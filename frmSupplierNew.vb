﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS


Public Class frmSupplierNew
#Region "Variables"
    Public NewSupplierAdded As Boolean = False
    Dim MBData As New DataExtractsClass
    Public AutoClose As Boolean = False
#End Region
    Private Sub Button_AddSupplier_Click(sender As System.Object, e As System.EventArgs) Handles Button_AddSupplier.Click
        Try
            If Me.CategoryDefault_ComboBox.Text = "" Or Me.DetailDefault2_ComboBox.Text = "" Or Me.DetailDefault_ComboBox.Text = "" Then
                MessageBox.Show("Please enter all three Category defaults", "Default Categories", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                CategoryDefault_ComboBox.Focus()
                Exit Sub
            End If

            If Me.SupplierName_TextBox.Text = "" Then
                MessageBox.Show("Please enter the supplier name.", "Supplier name", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                SupplierName_TextBox.Focus()
                Beep()
                Exit Sub
            End If

            Dim SupplierName As String = (SupplierName_TextBox.Text.Trim)
            Dim RefID As String = CStr(RefID_TextBox.Text.Trim)

            If RefID = "" Then
                MessageBox.Show("Please enter the Supplier's Ref ID.", "Supplier RefID", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                RefID_TextBox.Focus()
                Exit Sub
            End If

            If Not String.IsNullOrEmpty(MBData.CheckSupplierName(SupplierName)) Then
                MessageBox.Show("The supplier you're attempting to add already exits in the database for this company, please supply a different name", "Supplier name", MessageBoxButtons.OK, MessageBoxIcon.Information)
                SupplierName_TextBox.Focus()
                Exit Sub
            End If

            If Not String.IsNullOrEmpty(MBData.CheckSupplierRef(RefID)) Then
                MessageBox.Show("The ref id you supplied is already linked to a supplier, please supply a different reference", "Ref Id", MessageBoxButtons.OK, MessageBoxIcon.Information)
                RefID_TextBox.Focus()
                Exit Sub
            End If

            If Get_MB_GL_Account(CategoryDefault_ComboBox.Text, DetailDefault_ComboBox.Text, DetailDefault2_ComboBox.Text) = "NO ACCOUNT" Then
                MessageBox.Show("The category levels supplied do no relate to an account, please supply a different accounts from the list.", "Category Levels", MessageBoxButtons.OK, MessageBoxIcon.Information)
                CategoryDefault_ComboBox.Focus()
                Exit Sub
            End If


            Dim oBalance As String = "0"
            Dim oContact_No As String = Me.ContactNo_TextBox.Text
            Dim oContact_Person As String = Me.ContactNo_TextBox.Text
            Dim oModified As String = Now.Date.ToString("dd MMMM yyyy")
            Dim oVAT_Default As Integer = CInt(Me.cmbVAT.SelectedValue.ToString)
            Dim oAccount_Default As String = ""
            Dim oCategory_Default As String = Me.CategoryDefault_ComboBox.Text
            Dim oDetail_Default As String = Me.DetailDefault_ComboBox.Text
            Dim oDetail_Default2 As String = Me.DetailDefault2_ComboBox.Text
            Dim oAddress As String = Me.Address_TextBox.Text
            Dim oEmail_Address As String = Me.EmailAddress_TextBox.Text
            Dim oFax As String = Me.Fax_TextBox.Text
            Dim oActive As String = "Active"
            Dim oSearch As String = ""
            Dim oTerms As Integer = CInt(NudTerms.Value)
            Dim oIntegrationCode As String = Lbl_IntegrationCode.Text

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Call AppendSupplier(SQLConvert(SupplierName), RefID, oBalance, oContact_No, oContact_Person, oModified, oVAT_Default, oAccount_Default, oCategory_Default, _
                        oDetail_Default, oDetail_Default2, oAddress, oEmail_Address, oFax, oActive, oSearch, oTerms, SQLConvert(oIntegrationCode))
            'Reload the DataGridView with table data
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

            If AutoClose = True Then
                Me.Close()
            Else
                System.Threading.Thread.Sleep(1000)
                Call Form_Suppliers.Fill_Supplier_DGV(False)
                Call frmInvcCapture.Fill_Supplier_Combobox()

                Call oClearBoxes()

                NewSupplierAdded = True

            End If

            
            ''Call Form_InvoiceCapture.oFormLoad() 'Reload Invoice Capture

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured while adding a new supplier : " & ex.Message, "New Supplier", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub AppendSupplier(ByVal oSupplierName As String, ByVal oRefID As String, oBalance As Decimal, oContact_No As String, _
               ByVal oContact_Person As String, ByVal oModified As String, ByVal oVAT_Default As Integer, ByVal oAccount_Default As String, ByVal oCategory_Default As String, _
               ByVal oDetail_Default As String, ByVal oDetail_Default2 As String, ByVal oAddress As String, ByVal oEmail_Address As String, ByVal oFax As String, ByVal oActive As String,
               ByVal oSearch As String, ByVal oTerms As Integer, oIntegrationCode As String)
        Try
            'ONLY ENTER SPECIFIC COMPANY IF SPECIFIED
            Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####

            If My.Settings.DBType = "Access" Then

                Dim sSQL As String

                sSQL = "INSERT INTO Suppliers ( SupplierName, RefID, Balance, Contact_No, Contact_Person, Modified, VAT_Default, Account_Default, Category_Default, Detail_Default, "
                sSQL = sSQL & "Detail_Default2, Address, Email_Address, Fax, Active, Search, Terms,"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT "
                sSQL = sSQL & "'" & SQLConvert(oSupplierName) & "' as Expr1, "
                sSQL = sSQL & "'" & oRefID & "' as Expr2, "
                sSQL = sSQL & oBalance & " as Expr3, "
                sSQL = sSQL & "'" & SQLConvert(oContact_No) & "' as Expr4, "
                sSQL = sSQL & "'" & SQLConvert(oContact_Person) & "' as Expr5, "
                sSQL = sSQL & "#" & oModified & "# as Expr6, "
                sSQL = sSQL & oVAT_Default & " as Expr7, "
                sSQL = sSQL & "'" & SQLConvert(oAccount_Default) & "' as Expr8, "
                sSQL = sSQL & "'" & SQLConvert(oCategory_Default) & "' as Expr9, "
                sSQL = sSQL & "'" & SQLConvert(oDetail_Default) & "' as Expr10, "
                sSQL = sSQL & "'" & SQLConvert(oDetail_Default2) & "' as Expr10b, "
                sSQL = sSQL & "'" & SQLConvert(oAddress) & "' as Expr11, "
                sSQL = sSQL & "'" & SQLConvert(oEmail_Address) & "' as Expr12, "
                sSQL = sSQL & "'" & oFax & "' as Expr13, "
                sSQL = sSQL & "'" & oActive & "' as Expr14, "
                sSQL = sSQL & "'" & SQLConvert(oSearch) & "' as Expr15, "
                sSQL = sSQL & SQLConvert(oTerms) & " as Expr16"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr17"
                End If
                sSQL = sSQL & SQLConvert(oTerms) & " as Expr18"

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String

                sSQL = "INSERT INTO Suppliers ( SupplierName, RefID, Balance, Contact_No, Contact_Person, Modified, VAT_Default, Account_Default, Category_Default, Detail_Default, "
                sSQL = sSQL & "Detail_Default2, Address, Email_Address, Fax, Active, Search, Terms,Co_ID,IntegrationCode) "
                sSQL = sSQL & "SELECT "
                sSQL = sSQL & "'" & SQLConvert(oSupplierName) & "' as Expr1, "
                sSQL = sSQL & "'" & oRefID & "' as Expr2, "
                sSQL = sSQL & oBalance & " as Expr3, "
                sSQL = sSQL & "'" & SQLConvert(oContact_No) & "' as Expr4, "
                sSQL = sSQL & "'" & SQLConvert(oContact_Person) & "' as Expr5, "
                sSQL = sSQL & "'" & oModified & "' as Expr6, "
                sSQL = sSQL & oVAT_Default & " as Expr7, "
                sSQL = sSQL & "'" & SQLConvert(oAccount_Default) & "' as Expr8, "
                sSQL = sSQL & "'" & SQLConvert(oCategory_Default) & "' as Expr9, "
                sSQL = sSQL & "'" & SQLConvert(oDetail_Default) & "' as Expr10, "
                sSQL = sSQL & "'" & SQLConvert(oDetail_Default2) & "' as Expr10b, "
                sSQL = sSQL & "'" & SQLConvert(oAddress) & "' as Expr11, "
                sSQL = sSQL & "'" & SQLConvert(oEmail_Address) & "' as Expr12, "
                sSQL = sSQL & "'" & oFax & "' as Expr13, "
                sSQL = sSQL & "'" & oActive & "' as Expr14, "
                sSQL = sSQL & "'" & SQLConvert(oSearch) & "' as Expr15, "
                sSQL = sSQL & SQLConvert(oTerms) & " as Expr16 "
                sSQL = sSQL & ", " & oCompanyID & " as Expr17"
                sSQL = sSQL & ", '" & oIntegrationCode & "' as Expr18"


                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
                MessageBox.Show("New supplier succesfully added.", "Suppliers", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Cursor = Cursors.Arrow
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 198")
        End Try
    End Sub

    Sub oClearBoxes()

        Me.SupplierName_TextBox.Text = ""
        Me.RefID_TextBox.Text = ""
        Me.ContactNo_TextBox.Text = ""
        Me.ContactPerson_TextBox.Text = ""
        Me.cmbVAT.Text = ""
        'Me.AccountDefault_TextBox.Text = ""
        Me.CategoryDefault_ComboBox.Text = ""
        Me.DetailDefault_ComboBox.Text = ""
        Me.DetailDefault2_ComboBox.Text = ""
        Me.Address_TextBox.Text = ""
        Me.EmailAddress_TextBox.Text = ""
        Me.Fax_TextBox.Text = ""
        CategoryDefault_ComboBox.SelectedIndex = -1
        DetailDefault_ComboBox.SelectedIndex = -1
        DetailDefault2_ComboBox.SelectedIndex = -1
        cmbVAT.SelectedIndex = -1
        NudTerms.Value = 0
        'Me.Active_TextBox.Text = ""
        'Me.Search_TextBox.Text = ""

    End Sub
    Private Sub SupplierNew_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            LockForm(Me)
            Enabled = False
            Cursor = Cursors.AppStarting

            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> New Supplier -> Version : " & My.Settings.Setting_Version
            Call Fill_Category1_Combobox()
            Call LoadTax()
            cmbVAT.SelectedIndex = 0
            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "An error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Sub Fill_Category1_Combobox()
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.CategoryDefault_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                        Me.CategoryDefault_ComboBox.Items.Add(datareader("Segment 1 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.CategoryDefault_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                        Me.CategoryDefault_ComboBox.Items.Add(datareader("Segment 1 Desc"))
                    End If
                End While
                connection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 199")
        End Try

    End Sub

    Private Sub CategoryDefault_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CategoryDefault_ComboBox.SelectedIndexChanged
        If Me.CategoryDefault_ComboBox.Text = "" Then Exit Sub
        'First clear other two boxes
        Me.DetailDefault_ComboBox.Items.Clear()
        Me.DetailDefault_ComboBox.Text = ""
        Me.DetailDefault2_ComboBox.Items.Clear()
        Me.DetailDefault2_ComboBox.Text = ""
        'Then fill next box
        Call Fill_Detail_Combobox(Me.CategoryDefault_ComboBox.Text)
    End Sub

    Sub Fill_Detail_Combobox(Category1 As String)
        Try


            If My.Settings.DBType = "Access" Then

                Dim sSQL As String

                sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)

                Dim cmd As New OleDbCommand(sSQL, connection)
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Me.DetailDefault_ComboBox.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault_ComboBox.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String

                sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                Me.DetailDefault_ComboBox.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault_ComboBox.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 200")
        End Try

    End Sub


    Private Sub DetailDefault_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles DetailDefault_ComboBox.SelectedIndexChanged
        If Me.DetailDefault_ComboBox.Text = "" Then Exit Sub
        Call Fill_DetailDefault2_Combobox()
    End Sub

    Sub Fill_DetailDefault2_Combobox()
        Try

            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Me.DetailDefault_ComboBox.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryDefault_ComboBox.Text) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.DetailDefault2_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault2_ComboBox.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Me.DetailDefault_ComboBox.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryDefault_ComboBox.Text) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.DetailDefault2_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault2_ComboBox.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 201")
        End Try

    End Sub
    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Function LoadTax()

        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        cmbVAT.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            If ds.Tables(0).Rows.Count > 0 Then
                cmbVAT.DataSource = ds.Tables(0)
                cmbVAT.ValueMember = "Value"
                cmbVAT.DisplayMember = "Display"
                cmbVAT.SelectedIndex = 0
            Else
                MsgBox("There are no TAX types setup." & vbNewLine & "Go to settings and configure TAX")
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox("Can not open connection ! " & ex.Message)
        End Try


    End Function
End Class