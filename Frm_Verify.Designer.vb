﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Verify
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Verify))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TreeImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GRP_Selections = New System.Windows.Forms.GroupBox()
        Me.CB_ShowVerified = New System.Windows.Forms.CheckBox()
        Me.lbl_Payment = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.LBL_Incl_Amount = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LBL_VAT_Amount = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LBL_Amount_Excl = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Btn_Edit = New System.Windows.Forms.Button()
        Me.LBL_TxID = New System.Windows.Forms.Label()
        Me.LBL_InvDAte = New System.Windows.Forms.Label()
        Me.Lbl_InvRef = New System.Windows.Forms.Label()
        Me.LBL_SupplierName = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TB_VerifyNotes = New System.Windows.Forms.TextBox()
        Me.Btn_Verified = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Btn_Search = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DT_To = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DT_From = New System.Windows.Forms.DateTimePicker()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRP_Selections.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 106)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TreeView1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1637, 554)
        Me.SplitContainer1.SplitterDistance = 324
        Me.SplitContainer1.TabIndex = 0
        '
        'TreeView1
        '
        Me.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView1.ImageIndex = 0
        Me.TreeView1.ImageList = Me.TreeImageList
        Me.TreeView1.Location = New System.Drawing.Point(0, 0)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.SelectedImageIndex = 4
        Me.TreeView1.Size = New System.Drawing.Size(324, 554)
        Me.TreeView1.TabIndex = 0
        '
        'TreeImageList
        '
        Me.TreeImageList.ImageStream = CType(resources.GetObject("TreeImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.TreeImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.TreeImageList.Images.SetKeyName(0, "Home32X32.png")
        Me.TreeImageList.Images.SetKeyName(1, "Truck(Blue)32X32.png")
        Me.TreeImageList.Images.SetKeyName(2, "NoteArrow(Red)32X32.png")
        Me.TreeImageList.Images.SetKeyName(3, "NoteArrow(Green)32X32.png")
        Me.TreeImageList.Images.SetKeyName(4, "DoubleArrow(Blue)32X32.png")
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(1309, 554)
        Me.DataGridView1.TabIndex = 0
        '
        'GRP_Selections
        '
        Me.GRP_Selections.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GRP_Selections.Controls.Add(Me.CB_ShowVerified)
        Me.GRP_Selections.Controls.Add(Me.lbl_Payment)
        Me.GRP_Selections.Controls.Add(Me.Label9)
        Me.GRP_Selections.Controls.Add(Me.LBL_Incl_Amount)
        Me.GRP_Selections.Controls.Add(Me.Label10)
        Me.GRP_Selections.Controls.Add(Me.LBL_VAT_Amount)
        Me.GRP_Selections.Controls.Add(Me.Label8)
        Me.GRP_Selections.Controls.Add(Me.LBL_Amount_Excl)
        Me.GRP_Selections.Controls.Add(Me.Label6)
        Me.GRP_Selections.Controls.Add(Me.Btn_Edit)
        Me.GRP_Selections.Controls.Add(Me.LBL_TxID)
        Me.GRP_Selections.Controls.Add(Me.LBL_InvDAte)
        Me.GRP_Selections.Controls.Add(Me.Lbl_InvRef)
        Me.GRP_Selections.Controls.Add(Me.LBL_SupplierName)
        Me.GRP_Selections.Controls.Add(Me.Label5)
        Me.GRP_Selections.Controls.Add(Me.TB_VerifyNotes)
        Me.GRP_Selections.Controls.Add(Me.Btn_Verified)
        Me.GRP_Selections.Controls.Add(Me.Label4)
        Me.GRP_Selections.Controls.Add(Me.Label3)
        Me.GRP_Selections.Controls.Add(Me.Btn_Search)
        Me.GRP_Selections.Controls.Add(Me.Label2)
        Me.GRP_Selections.Controls.Add(Me.DT_To)
        Me.GRP_Selections.Controls.Add(Me.Label1)
        Me.GRP_Selections.Controls.Add(Me.DT_From)
        Me.GRP_Selections.Location = New System.Drawing.Point(12, 0)
        Me.GRP_Selections.Name = "GRP_Selections"
        Me.GRP_Selections.Size = New System.Drawing.Size(1637, 100)
        Me.GRP_Selections.TabIndex = 1
        Me.GRP_Selections.TabStop = False
        '
        'CB_ShowVerified
        '
        Me.CB_ShowVerified.AutoSize = True
        Me.CB_ShowVerified.Location = New System.Drawing.Point(182, 73)
        Me.CB_ShowVerified.Name = "CB_ShowVerified"
        Me.CB_ShowVerified.Size = New System.Drawing.Size(116, 21)
        Me.CB_ShowVerified.TabIndex = 26
        Me.CB_ShowVerified.Text = "Show Verified"
        Me.CB_ShowVerified.UseVisualStyleBackColor = True
        '
        'lbl_Payment
        '
        Me.lbl_Payment.AutoSize = True
        Me.lbl_Payment.Location = New System.Drawing.Point(1037, 22)
        Me.lbl_Payment.Name = "lbl_Payment"
        Me.lbl_Payment.Size = New System.Drawing.Size(16, 17)
        Me.lbl_Payment.TabIndex = 25
        Me.lbl_Payment.Text = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(937, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(103, 17)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Total Payment:"
        '
        'LBL_Incl_Amount
        '
        Me.LBL_Incl_Amount.AutoSize = True
        Me.LBL_Incl_Amount.Location = New System.Drawing.Point(854, 66)
        Me.LBL_Incl_Amount.Name = "LBL_Incl_Amount"
        Me.LBL_Incl_Amount.Size = New System.Drawing.Size(16, 17)
        Me.LBL_Incl_Amount.TabIndex = 23
        Me.LBL_Incl_Amount.Text = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(768, 66)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(75, 17)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Total (Incl)"
        '
        'LBL_VAT_Amount
        '
        Me.LBL_VAT_Amount.AutoSize = True
        Me.LBL_VAT_Amount.Location = New System.Drawing.Point(854, 44)
        Me.LBL_VAT_Amount.Name = "LBL_VAT_Amount"
        Me.LBL_VAT_Amount.Size = New System.Drawing.Size(16, 17)
        Me.LBL_VAT_Amount.TabIndex = 21
        Me.LBL_VAT_Amount.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(768, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 17)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "VAT"
        '
        'LBL_Amount_Excl
        '
        Me.LBL_Amount_Excl.AutoSize = True
        Me.LBL_Amount_Excl.Location = New System.Drawing.Point(854, 22)
        Me.LBL_Amount_Excl.Name = "LBL_Amount_Excl"
        Me.LBL_Amount_Excl.Size = New System.Drawing.Size(16, 17)
        Me.LBL_Amount_Excl.TabIndex = 19
        Me.LBL_Amount_Excl.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(768, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 17)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Total (Excl)"
        '
        'Btn_Edit
        '
        Me.Btn_Edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Edit.Location = New System.Drawing.Point(1548, 16)
        Me.Btn_Edit.Name = "Btn_Edit"
        Me.Btn_Edit.Size = New System.Drawing.Size(83, 78)
        Me.Btn_Edit.TabIndex = 17
        Me.Btn_Edit.Tag = "S_Invoice_Edit"
        Me.Btn_Edit.Text = "Edit Invoice"
        Me.Btn_Edit.UseVisualStyleBackColor = True
        '
        'LBL_TxID
        '
        Me.LBL_TxID.AutoSize = True
        Me.LBL_TxID.Location = New System.Drawing.Point(656, 16)
        Me.LBL_TxID.Name = "LBL_TxID"
        Me.LBL_TxID.Size = New System.Drawing.Size(16, 17)
        Me.LBL_TxID.TabIndex = 16
        Me.LBL_TxID.Text = "0"
        '
        'LBL_InvDAte
        '
        Me.LBL_InvDAte.AutoSize = True
        Me.LBL_InvDAte.Location = New System.Drawing.Point(463, 70)
        Me.LBL_InvDAte.Name = "LBL_InvDAte"
        Me.LBL_InvDAte.Size = New System.Drawing.Size(13, 17)
        Me.LBL_InvDAte.TabIndex = 15
        Me.LBL_InvDAte.Text = "-"
        '
        'Lbl_InvRef
        '
        Me.Lbl_InvRef.AutoSize = True
        Me.Lbl_InvRef.Location = New System.Drawing.Point(463, 39)
        Me.Lbl_InvRef.Name = "Lbl_InvRef"
        Me.Lbl_InvRef.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_InvRef.TabIndex = 14
        Me.Lbl_InvRef.Text = "-"
        '
        'LBL_SupplierName
        '
        Me.LBL_SupplierName.AutoSize = True
        Me.LBL_SupplierName.Location = New System.Drawing.Point(463, 11)
        Me.LBL_SupplierName.Name = "LBL_SupplierName"
        Me.LBL_SupplierName.Size = New System.Drawing.Size(115, 17)
        Me.LBL_SupplierName.TabIndex = 13
        Me.LBL_SupplierName.Text = "Select an Invoice"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(325, 70)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(90, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Invoice Date:"
        '
        'TB_VerifyNotes
        '
        Me.TB_VerifyNotes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TB_VerifyNotes.Location = New System.Drawing.Point(1317, 16)
        Me.TB_VerifyNotes.Multiline = True
        Me.TB_VerifyNotes.Name = "TB_VerifyNotes"
        Me.TB_VerifyNotes.Size = New System.Drawing.Size(225, 78)
        Me.TB_VerifyNotes.TabIndex = 9
        '
        'Btn_Verified
        '
        Me.Btn_Verified.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Verified.Location = New System.Drawing.Point(1184, 16)
        Me.Btn_Verified.Name = "Btn_Verified"
        Me.Btn_Verified.Size = New System.Drawing.Size(127, 78)
        Me.Btn_Verified.TabIndex = 8
        Me.Btn_Verified.Tag = "S_Allow_Verify"
        Me.Btn_Verified.Text = "Verify Invoice"
        Me.Btn_Verified.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(325, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(126, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Invoice Reference:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(325, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Suppler Name:"
        '
        'Btn_Search
        '
        Me.Btn_Search.Location = New System.Drawing.Point(10, 63)
        Me.Btn_Search.Name = "Btn_Search"
        Me.Btn_Search.Size = New System.Drawing.Size(166, 31)
        Me.Btn_Search.TabIndex = 5
        Me.Btn_Search.Tag = "S_Allow_Search"
        Me.Btn_Search.Text = "Search"
        Me.Btn_Search.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "To Date:"
        '
        'DT_To
        '
        Me.DT_To.CustomFormat = "dd MMMM yyyy"
        Me.DT_To.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_To.Location = New System.Drawing.Point(89, 39)
        Me.DT_To.Name = "DT_To"
        Me.DT_To.Size = New System.Drawing.Size(209, 22)
        Me.DT_To.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "From Date:"
        '
        'DT_From
        '
        Me.DT_From.CustomFormat = "dd MMMM yyyy"
        Me.DT_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_From.Location = New System.Drawing.Point(89, 11)
        Me.DT_From.Name = "DT_From"
        Me.DT_From.Size = New System.Drawing.Size(209, 22)
        Me.DT_From.TabIndex = 0
        '
        'Frm_Verify
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(1661, 670)
        Me.Controls.Add(Me.GRP_Selections)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Frm_Verify"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "S_Invoice_Verify"
        Me.Text = "Frm_Verify"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRP_Selections.ResumeLayout(False)
        Me.GRP_Selections.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents GRP_Selections As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DT_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DT_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Btn_Search As System.Windows.Forms.Button
    Friend WithEvents LBL_InvDAte As System.Windows.Forms.Label
    Friend WithEvents Lbl_InvRef As System.Windows.Forms.Label
    Friend WithEvents LBL_SupplierName As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TB_VerifyNotes As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Verified As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TreeImageList As System.Windows.Forms.ImageList
    Friend WithEvents LBL_TxID As System.Windows.Forms.Label
    Friend WithEvents Btn_Edit As System.Windows.Forms.Button
    Friend WithEvents LBL_Incl_Amount As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LBL_VAT_Amount As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents LBL_Amount_Excl As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lbl_Payment As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents CB_ShowVerified As System.Windows.Forms.CheckBox
End Class
