﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.IO

Public Class frmCompanyDetails

#Region "Variables"
    Dim dbadp As OleDbDataAdapter '*
    Dim dTable As New DataTable '*
    Dim dbadp2 As SqlDataAdapter '*
    Dim dTable2 As New DataTable '*

#End Region

#Region "Methods"

#End Region

    Private Sub Company_Details_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Company Details -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            If My.Settings.Setting_CompanyID <> String.Empty Or My.Settings.Setting_CompanyID <> Nothing Then
                GetEditCompany(CInt(My.Settings.Setting_CompanyID))
            Else
                MessageBox.Show("Please set a company in Settings screen in order to continue", "Workspace not set", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Me.Close()
            End If
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("There is a database update required for 'Company details', please report to the support team", "DB Update", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    'Sub Fill_DGV() '*
    '    Try '*
    '        Dim sSQL As String '*

    '        sSQL = "SELECT * FROM [Company Details]" '*
    '        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '            sSQL = sSQL & " Where ID = " & My.Settings.Setting_CompanyID
    '        End If
    '        Dim connection As New OleDbConnection(My.Settings.CS_Setting) '*
    '        dbadp = New OleDbDataAdapter(sSQL, connection) '*

    '        dbadp.Fill(dTable) '*
    '        Me.GrdCompanyDetails.DataSource = dTable '*

    '    Catch ex As Exception '*
    '        MsgBox(ex.Message & " 084") '*
    '    End Try '*
    'End Sub

    'Sub Fill_DGV2() '*
    '    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
    '    Try '*
    '        GrdCompanyDetails.DataSource = Nothing
    '        Dim sSQL As String '*

    '        sSQL = "SELECT * FROM [Company Details]" '*
    '        'If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '        ' sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
    '        'End If
    '        '*
    '        dbadp2 = New SqlDataAdapter(sSQL, connection) '*
    '        dTable2.Clear()
    '        dbadp2.Fill(dTable2) '*
    '        Me.GrdCompanyDetails.DataSource = dTable2 '*
    '    Catch ex As Exception '*
    '        If connection.State = ConnectionState.Open Then
    '            connection.Dispose()
    '        End If
    '        MsgBox(ex.Message & " 085") '*
    '    End Try '*
    'End Sub
    Private Sub Button_Save_Click(sender As System.Object, e As System.EventArgs) Handles Button_Save.Click '*

        Try

            If txtCompanyName.Text.Trim.Length = 0 Then
                MessageBox.Show("Please supply Company name!", "Company name", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtCompanyName.Focus()
                Exit Sub
            End If

            If txtLegalEntity.Text.Trim.Length = 0 Then
                MessageBox.Show("Please supply Company legal name!", "Company legal name", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtLegalEntity.Focus()
                Exit Sub
            End If

            If txtTelphone1.Text.Trim.Length = 0 Or Not IsNumeric(txtTelphone1.Text.Trim) Then
                MessageBox.Show("Please supply company primary telephone number in a correct format!", "Company tel. no.", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtTelphone1.Focus()
                Exit Sub
            End If

            If txtTelephone2.Text.Trim.Length <> 0 And Not IsNumeric(txtTelephone2.Text.Trim) Then
                MessageBox.Show("Please supply telephone line 2 in a correct format!", "Company tel. no.", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtTelephone2.Focus()
                Exit Sub
            End If

            If txtEmailAddress1.Text.Trim.Length = 0 Then
                MessageBox.Show("Please supply Company primary email adress", "Company email address", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtEmailAddress1.Focus()
                Exit Sub
            End If

            If txtEmailAddress1.Text.Trim.Length <> 0 And txtEmailAddress1.Text.Contains("@") = False Then
                MessageBox.Show("Please supply a valid email adress", "Company email address", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtEmailAddress1.Focus()
                Exit Sub
            End If

            Enabled = False
            Cursor = Cursors.AppStarting
            UpdateCompany()
            Enabled = Enabled
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = Enabled
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub UpdateCompany()

        Dim sSQL As String
        Dim cn1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        If My.Settings.Setting_CompanyID = Nothing Or My.Settings.Setting_CompanyID = String.Empty Then
            MessageBox.Show("Please Ensure that you're connected to a company", "Company Details editing", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error)
            Exit Sub
        End If

        Me.Enabled = False
        Me.Cursor = Cursors.AppStarting

        Try
            If My.Settings.DBType = "Access" Then

                sSQL = "UPDATE [Company Details] SET "
                sSQL = sSQL & "[CompanyName] = '" & SQLConvert(txtCompanyName.Text) & "', "
                sSQL = sSQL & "[LegalEntityName] = '" & SQLConvert(txtLegalEntity.Text) & "', "
                sSQL = sSQL & "[CoReg] = '" & SQLConvert(txtRegNo.Text) & "', "
                sSQL = sSQL & "[CoVat] = '" & SQLConvert(txtCompanyVat.Text) & "',"
                sSQL = sSQL & "[Address1] = '" & SQLConvert(txtPAddress1.Text) & "',"
                sSQL = sSQL & "[Address2] = '" & SQLConvert(txtPAddress2.Text) & "',"
                sSQL = sSQL & "[Address3] = '" & SQLConvert(txtPAddress3.Text) & "', "
                sSQL = sSQL & "[Address4] = '" & SQLConvert(txtPAddress4.Text) & "', "
                sSQL = sSQL & "[TelephoneLine1] = '" & SQLConvert(txtTelphone1.Text) & "', "
                sSQL = sSQL & "[TelephoneLine2] ='" & SQLConvert(txtTelephone2.Text) & "', "
                sSQL = sSQL & "[EmailAddress1] = '" & SQLConvert(txtEmailAddress1.Text) & "', "
                sSQL = sSQL & "[EmailAddress2] = '" & SQLConvert(txtEmailAddress2.Text) & "', "
                sSQL = sSQL & "[Directors] = '" & SQLConvert(txtDirectors.Text) & "' "
                sSQL = sSQL & "Where ID = " & My.Settings.Setting_CompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "UPDATE [Company Details] SET "
                sSQL = sSQL & "[CompanyName] = '" & SQLConvert(txtCompanyName.Text) & "', "
                sSQL = sSQL & "[LegalEntityName] = '" & SQLConvert(txtLegalEntity.Text) & "', "
                sSQL = sSQL & "[CoReg] = '" & SQLConvert(txtRegNo.Text) & "', "
                sSQL = sSQL & "[CoVat] = '" & SQLConvert(txtCompanyVat.Text) & "',"
                sSQL = sSQL & "[Address1] = '" & SQLConvert(txtPAddress1.Text) & "',"
                sSQL = sSQL & "[Address2] = '" & SQLConvert(txtPAddress2.Text) & "',"
                sSQL = sSQL & "[Address3] = '" & SQLConvert(txtPAddress3.Text) & "', "
                sSQL = sSQL & "[Address4] = '" & SQLConvert(txtPAddress4.Text) & "', "
                sSQL = sSQL & "[TelephoneLine1] = '" & SQLConvert(txtTelphone1.Text) & "', "
                sSQL = sSQL & "[TelephoneLine2] ='" & SQLConvert(txtTelephone2.Text) & "', "
                sSQL = sSQL & "[EmailAddress1] = '" & SQLConvert(txtEmailAddress1.Text) & "', "
                sSQL = sSQL & "[EmailAddress2] = '" & SQLConvert(txtEmailAddress2.Text) & "', "
                sSQL = sSQL & "[Directors] = '" & SQLConvert(txtDirectors.Text) & "' "
                sSQL = sSQL & "Where ID = " & My.Settings.Setting_CompanyID

                Dim cmd As New SqlCommand(sSQL, cn1)
                cmd.CommandTimeout = 300
                Using cn1
                    cn1.Open()
                    cmd.ExecuteNonQuery()
                End Using
            End If

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("Changes to company details succesfully saved")

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("Error :" & ex.Message)
        End Try
    End Sub
    Public Sub Clear()
        With Me
            txtCompanyName.Text = ""
            txtLegalEntity.Text = ""
            txtRegNo.Text = ""
            txtCompanyVat.Text = ""
            txtTelphone1.Text = ""
            txtTelephone2.Text = ""
            txtEmailAddress1.Text = ""
            txtEmailAddress2.Text = ""
            txtPAddress1.Text = ""
            txtPAddress2.Text = ""
            txtPAddress3.Text = ""
            txtPAddress4.Text = ""
            txtDirectors.Text = ""
        End With
    End Sub
    Private Sub GetEditCompany(ByVal CompanyID As Integer)

        Dim sSQL As String = "SELECT * FROM [Company Details] " '*
        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " Where ID = " & CompanyID
        End If

        Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, connection)
        cmd.CommandTimeout = 300

        Using connection
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            While datareader.Read
                'Company Name
                If Not datareader("CompanyName").Equals(DBNull.Value) Then
                    txtCompanyName.Text = datareader("CompanyName").ToString
                End If
                'Legal Entity Name
                If Not datareader("LegalEntityName").Equals(DBNull.Value) Then
                    txtLegalEntity.Text = datareader("LegalEntityName").ToString
                End If
                'CoReg
                If Not datareader("CoReg").Equals(DBNull.Value) Then
                    txtRegNo.Text = datareader("CoReg").ToString
                End If
                'CoVat
                If Not datareader("CoVat").Equals(DBNull.Value) Then
                    txtCompanyVat.Text = datareader("CoVat").ToString
                End If
                'Address1
                If Not datareader("Address1").Equals(DBNull.Value) Then
                    txtPAddress1.Text = datareader("Address1").ToString
                End If
                'Address2
                If Not datareader("Address2").Equals(DBNull.Value) Then
                    txtPAddress2.Text = datareader("Address2").ToString
                End If
                'Address3
                If Not datareader("Address3").Equals(DBNull.Value) Then
                    txtPAddress3.Text = datareader("Address3").ToString
                End If
                'Address4
                If Not datareader("Address4").Equals(DBNull.Value) Then
                    txtPAddress4.Text = datareader("Address4").ToString
                End If
                'Telephoneline1
                If Not datareader("Telephoneline1").Equals(DBNull.Value) Then
                    txtTelphone1.Text = datareader("Telephoneline1").ToString
                End If
                'Telephoneline2
                If Not datareader("Telephoneline2").Equals(DBNull.Value) Then
                    txtTelephone2.Text = datareader("Telephoneline2").ToString
                End If
                'EmailAddress1
                If Not datareader("EmailAddress1").Equals(DBNull.Value) Then
                    txtEmailAddress1.Text = datareader("EmailAddress1").ToString
                End If
                'EmailAddress2
                If Not datareader("EmailAddress2").Equals(DBNull.Value) Then
                    txtEmailAddress2.Text = datareader("EmailAddress2").ToString
                End If
                'Directors
                If Not datareader("Directors").Equals(DBNull.Value) Then
                    txtDirectors.Text = datareader("Directors").ToString
                End If
            End While
        End Using

    End Sub
    Private Sub btnNewCompany_Click_1(sender As Object, e As EventArgs) Handles btnNewCompany.Click
        'Dim frmNewCo As New frmAddNewCompany
        'frmNewCo.ShowDialog()
        MessageBox.Show("Dear User," & vbCrLf & vbCrLf & "Please contact the MagicBox team to assist with adding a new company." & _
                        vbCrLf & vbCrLf & "Regards," & vbCrLf & "MB Dev Team.", "System Administration", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub Button_Close_Click(sender As Object, e As EventArgs) Handles Button_Close.Click
        Me.Close()
    End Sub
End Class