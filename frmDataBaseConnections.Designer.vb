﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataBaseConnections
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataBaseConnections))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnSaveCS = New System.Windows.Forms.Button()
        Me.btnDeleteCS = New System.Windows.Forms.Button()
        Me.btnEditCS = New System.Windows.Forms.Button()
        Me.btnAddCS = New System.Windows.Forms.Button()
        Me.ListViewCS = New System.Windows.Forms.ListView()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.btnSaveCS)
        Me.GroupBox1.Controls.Add(Me.btnDeleteCS)
        Me.GroupBox1.Controls.Add(Me.btnEditCS)
        Me.GroupBox1.Controls.Add(Me.btnAddCS)
        Me.GroupBox1.Controls.Add(Me.ListViewCS)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Navy
        Me.GroupBox1.Location = New System.Drawing.Point(9, 11)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(448, 190)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Connections"
        '
        'btnSaveCS
        '
        Me.btnSaveCS.Location = New System.Drawing.Point(344, 141)
        Me.btnSaveCS.Name = "btnSaveCS"
        Me.btnSaveCS.Size = New System.Drawing.Size(97, 40)
        Me.btnSaveCS.TabIndex = 92
        Me.btnSaveCS.Text = "&Save changes"
        Me.btnSaveCS.UseVisualStyleBackColor = True
        '
        'btnDeleteCS
        '
        Me.btnDeleteCS.Location = New System.Drawing.Point(346, 97)
        Me.btnDeleteCS.Name = "btnDeleteCS"
        Me.btnDeleteCS.Size = New System.Drawing.Size(97, 38)
        Me.btnDeleteCS.TabIndex = 91
        Me.btnDeleteCS.Text = "&Delete selected"
        Me.btnDeleteCS.UseVisualStyleBackColor = True
        '
        'btnEditCS
        '
        Me.btnEditCS.Location = New System.Drawing.Point(344, 55)
        Me.btnEditCS.Name = "btnEditCS"
        Me.btnEditCS.Size = New System.Drawing.Size(97, 36)
        Me.btnEditCS.TabIndex = 90
        Me.btnEditCS.Text = "&Edit Selected"
        Me.btnEditCS.UseVisualStyleBackColor = True
        '
        'btnAddCS
        '
        Me.btnAddCS.Location = New System.Drawing.Point(344, 18)
        Me.btnAddCS.Name = "btnAddCS"
        Me.btnAddCS.Size = New System.Drawing.Size(98, 31)
        Me.btnAddCS.TabIndex = 89
        Me.btnAddCS.Text = "&Add New"
        Me.btnAddCS.UseVisualStyleBackColor = True
        '
        'ListViewCS
        '
        Me.ListViewCS.ForeColor = System.Drawing.Color.Navy
        Me.ListViewCS.FullRowSelect = True
        Me.ListViewCS.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.ListViewCS.HideSelection = False
        Me.ListViewCS.Location = New System.Drawing.Point(8, 18)
        Me.ListViewCS.Name = "ListViewCS"
        Me.ListViewCS.Size = New System.Drawing.Size(330, 163)
        Me.ListViewCS.TabIndex = 80
        Me.ListViewCS.UseCompatibleStateImageBehavior = False
        Me.ListViewCS.View = System.Windows.Forms.View.Details
        '
        'frmDataBaseConnections
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 210)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(478, 248)
        Me.MinimumSize = New System.Drawing.Size(478, 248)
        Me.Name = "frmDataBaseConnections"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Connections strings"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSaveCS As System.Windows.Forms.Button
    Friend WithEvents btnDeleteCS As System.Windows.Forms.Button
    Friend WithEvents btnEditCS As System.Windows.Forms.Button
    Friend WithEvents btnAddCS As System.Windows.Forms.Button
    Friend WithEvents ListViewCS As System.Windows.Forms.ListView
End Class
