﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class frmEmployee_New
#Region "Variables"
    ' Dim MBData As MagicBoxData
    Public isEditEmployee As Boolean = Nothing
    Public EditEmployeeID As Integer = Nothing
    Public Saved As Boolean = False
#End Region
#Region "Methods"

    Sub Add_Employee()
        Try
            Dim sSQL As String
            Dim oCommissionPerc As String
            If IsNumeric(Me.txtCommission.Text) = True Then
                oCommissionPerc = Me.txtCommission.Text
            Else
                oCommissionPerc = "0"
            End If

            Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString

            Enabled = False
            Cursor = Cursors.AppStarting

            If My.Settings.DBType = "Access" Then

                sSQL = "Insert Into Employees ([First_Name], [Last_Name], [IDNumber], [Employee_Type], [Active], [PayrollNumber], [Waiter Number], [Payment Type], "
                sSQL = sSQL & "[Commission_Perc], [Wage_Salary], [Contact Number], [Contact 2], [Start Date], [Contract End], "
                sSQL = sSQL & "[Loan], [Address], [Emp_BankName], [Emp_BankAccHolder], [Emp_BankAccNo], [Emp_BankBranch], [IsSchedulerContact]"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", [Co_ID]"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & " Values ("
                '##### NEW #####
                sSQL = sSQL & "'" & SQLConvert(Me.txtName.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.txtSurname.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.txtIDNumber.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.cmbEmployeeType.Text) & "', "
                sSQL = sSQL & "'" & cmbStatus.Text & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtPayrollNumber.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtWaiterNumber.Text) & "',"
                sSQL = sSQL & "'" & cmbPaymentType.Text & "',"
                sSQL = sSQL & "" & oCommissionPerc & ","
                sSQL = sSQL & "'" & txtWage.Text & "', "
                sSQL = sSQL & "'" & Me.txtContactNumber1.Text & "', "
                sSQL = sSQL & "'" & Me.txtContact2.Text & "', "
                sSQL = sSQL & "#" & Me.dtpDateSarted.Value.Date.ToString("dd MMMM yyyy") & "#, "
                sSQL = sSQL & "#" & dtpContractEnds.Value.Date.ToString("dd MMMM yyyy") & "#, "
                sSQL = sSQL & "'" & txtLoanAmount.Text & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.txtAddress.Text) & "',"
                'Banking Details
                sSQL = sSQL & "'" & SQLConvert(Me.txtBankName.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtAccountHolder.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtAccNumber.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtBranchNumber.Text) & "',"
                'Is scheduler contact
                If ckbIsScheduler.Checked = True Then
                    sSQL = sSQL & "'1'"
                Else
                    sSQL = sSQL & "'0'"
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID
                End If
                sSQL = sSQL & ") "

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Insert Into Employees ([First_Name], [Last_Name], [IDNumber], [Employee_Type], [Active], [PayrollNumber], [Waiter Number], [Payment Type], "
                sSQL = sSQL & "[Commission_Perc], [Wage_Salary], [Contact Number], [Contact 2], [Start Date], [Contract End], "
                sSQL = sSQL & "[Loan], [Address], [Emp_BankName], [Emp_BankAccHolder], [Emp_BankAccNo], [Emp_BankBranch], [IsSchedulerContact]"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", [Co_ID]"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & " Values ("
                '##### NEW #####
                sSQL = sSQL & "'" & SQLConvert(Me.txtName.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.txtSurname.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.txtIDNumber.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.cmbEmployeeType.Text) & "', "
                sSQL = sSQL & "'" & cmbStatus.Text & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtPayrollNumber.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtWaiterNumber.Text) & "',"
                sSQL = sSQL & "'" & cmbPaymentType.Text & "',"
                sSQL = sSQL & "" & oCommissionPerc & ","
                sSQL = sSQL & "'" & txtWage.Text & "', "
                sSQL = sSQL & "'" & Me.txtContactNumber1.Text & "', "
                sSQL = sSQL & "'" & Me.txtContact2.Text & "', "
                sSQL = sSQL & "'" & Me.dtpDateSarted.Value.Date.ToString("dd MMMM yyyy") & "', "
                sSQL = sSQL & "'" & dtpContractEnds.Value.Date.ToString("dd MMMM yyyy") & "', "
                sSQL = sSQL & "'" & txtLoanAmount.Text & "', "
                sSQL = sSQL & "'" & SQLConvert(Me.txtAddress.Text) & "',"
                'Banking Details
                sSQL = sSQL & "'" & SQLConvert(Me.txtBankName.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtAccountHolder.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtAccNumber.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(Me.txtBranchNumber.Text) & "',"
                'Is scheduler contact
                If ckbIsScheduler.Checked = True Then
                    sSQL = sSQL & "'1'"
                Else
                    sSQL = sSQL & "'0'"
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID
                End If
                sSQL = sSQL & ") "

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

            Enabled = True
            Cursor = Cursors.Arrow
            Saved = True
            MessageBox.Show("New  Employee successfully added.", "New employee", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ClearForm()

        Catch ex As Exception
            Enabled = False
            Cursor = Cursors.AppStarting
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub Update_Employee(ByVal EmployeeID As Integer)
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim sSQL As String

            If My.Settings.DBType = "Access" Then

                sSQL = "UPDATE Employees SET "
                sSQL = sSQL & "[First_Name] = '" & SQLConvert(txtName.Text) & "', "
                sSQL = sSQL & "[Last_Name] = '" & SQLConvert(txtSurname.Text) & "', "
                sSQL = sSQL & "[Employee_Type] = '" & SQLConvert(cmbEmployeeType.Text) & "', "
                sSQL = sSQL & "[Active] = '" & SQLConvert(cmbStatus.Text) & "',"
                sSQL = sSQL & "[Waiter Number] = '" & SQLConvert(txtWaiterNumber.Text) & "',"
                sSQL = sSQL & "[Payment Type] = '" & cmbPaymentType.Text & "',"
                If txtCommission.Text = "" Then
                    sSQL = sSQL & "[Commission_Perc] = 0 ,"
                Else
                    sSQL = sSQL & "[Commission_Perc] = " & SQLConvert(txtCommission.Text) & ","
                End If
                sSQL = sSQL & "[Wage_Salary] = '" & SQLConvert(txtWage.Text) & "', "
                sSQL = sSQL & "[Contact Number] = '" & SQLConvert(txtContactNumber1.Text) & "', "
                sSQL = sSQL & "[Contact 2] = '" & SQLConvert(txtContact2.Text) & "', "
                sSQL = sSQL & "[Start Date] =#" & dtpDateSarted.Value.Date.ToString("dd MMMM yyyy") & "#, "
                sSQL = sSQL & "[Contract End] = #" & dtpContractEnds.Value.Date.ToString("dd MMMM yyyy") & "#, "
                sSQL = sSQL & "[Loan] = '" & SQLConvert(txtLoanAmount.Text) & "', "
                sSQL = sSQL & "[Address] = '" & SQLConvert(txtAddress.Text) & "', "
                sSQL = sSQL & "[Emp_BankName] = '" & SQLConvert(txtBankName.Text) & "', "
                sSQL = sSQL & "[Emp_BankAccHolder] = '" & SQLConvert(txtAccountHolder.Text) & "', "
                sSQL = sSQL & "[Emp_BankAccNo] = '" & SQLConvert(txtAccNumber.Text) & "', "
                sSQL = sSQL & "[Emp_BankBranch] = '" & SQLConvert(txtBranchNumber.Text) & "', "
                If ckbIsScheduler.Checked = True Then
                    sSQL = sSQL & "[IsSchedulerContact] = '1', "
                Else
                    sSQL = sSQL & " [IsSchedulerContact] = '0', "
                End If
                sSQL = sSQL & "[PayrollNumber] = '" & SQLConvert(txtPayrollNumber.Text) & "' "
                sSQL = sSQL & "Where EmployeeID = " & EmployeeID

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If


                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "UPDATE Employees SET "
                sSQL = sSQL & "[First_Name] = '" & SQLConvert(txtName.Text) & "', "
                sSQL = sSQL & "[Last_Name] = '" & SQLConvert(txtSurname.Text) & "', "
                sSQL = sSQL & "[Employee_Type] = '" & SQLConvert(cmbEmployeeType.Text) & "', "
                sSQL = sSQL & "[Active] = '" & SQLConvert(cmbStatus.Text) & "',"
                sSQL = sSQL & "[Waiter Number] = '" & SQLConvert(txtWaiterNumber.Text) & "',"
                sSQL = sSQL & "[Payment Type] = '" & cmbPaymentType.Text & "',"
                If txtCommission.Text = "" Then
                    sSQL = sSQL & "[Commission_Perc] = 0 ,"
                Else
                    sSQL = sSQL & "[Commission_Perc] = " & SQLConvert(txtCommission.Text) & ","
                End If
                sSQL = sSQL & "[Wage_Salary] = '" & SQLConvert(txtWage.Text) & "', "
                sSQL = sSQL & "[Contact Number] = '" & SQLConvert(txtContactNumber1.Text) & "', "
                sSQL = sSQL & "[Contact 2] = '" & SQLConvert(txtContact2.Text) & "', "
                sSQL = sSQL & "[Start Date] ='" & dtpDateSarted.Value.Date.ToString("dd MMMM yyyy") & "', "
                sSQL = sSQL & "[Contract End] = '" & dtpContractEnds.Value.Date.ToString("dd MMMM yyyy") & "', "
                sSQL = sSQL & "[Loan] = '" & SQLConvert(txtLoanAmount.Text) & "', "
                sSQL = sSQL & "[Address] = '" & SQLConvert(txtAddress.Text) & "', "
                sSQL = sSQL & "[Emp_BankName] = '" & SQLConvert(txtBankName.Text) & "', "
                sSQL = sSQL & "[Emp_BankAccHolder] = '" & SQLConvert(txtAccountHolder.Text) & "', "
                sSQL = sSQL & "[Emp_BankAccNo] = '" & SQLConvert(txtAccNumber.Text) & "', "
                sSQL = sSQL & "[Emp_BankBranch] = '" & SQLConvert(txtBranchNumber.Text) & "', "
                If ckbIsScheduler.Checked = True Then
                    sSQL = sSQL & "[IsSchedulerContact] = '1', "
                Else
                    sSQL = sSQL & " [IsSchedulerContact] = '0', "
                End If
                sSQL = sSQL & "[PayrollNumber] = '" & SQLConvert(txtPayrollNumber.Text) & "' "
                sSQL = sSQL & "Where EmployeeID = " & EmployeeID

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using
            End If

            Enabled = True
            Cursor = Cursors.Arrow
            Saved = True
            MessageBox.Show("Employee successfully updated.", "Employee Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub GetEditEmployee(ByVal EmployeeID As Integer)

        Dim sSQL As String = "SELECT * FROM Employees WHERE EmployeeID = " & EmployeeID
        Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, connection)
        cmd.CommandTimeout = 300

        Using connection

            connection.Open()

            Dim datareader As SqlDataReader = cmd.ExecuteReader
            While datareader.Read
                'Employee Type
                If Not datareader("Employee_Type").Equals(DBNull.Value) Then
                    cmbEmployeeType.SelectedItem = datareader("Employee_Type").ToString
                End If
                'First Name
                If Not datareader("First_Name").Equals(DBNull.Value) Then
                    txtName.Text = datareader("First_Name").ToString
                End If
                'Surname
                If Not datareader("Last_Name").Equals(DBNull.Value) Then
                    txtSurname.Text = datareader("Last_Name").ToString
                End If
                'ID Number
                If Not datareader("IDNumber").Equals(DBNull.Value) Then
                    txtIDNumber.Text = datareader("IDNumber").ToString
                End If
                'Status
                If Not datareader("Active").Equals(DBNull.Value) Then
                    cmbStatus.SelectedItem = datareader("Active").ToString
                End If
                'Contact No1
                If Not datareader("Contact Number").Equals(DBNull.Value) Then
                    txtContactNumber1.Text = datareader("Contact Number").ToString
                End If
                'Contact No2
                If Not datareader("Contact 2").Equals(DBNull.Value) Then
                    txtContact2.Text = datareader("Contact 2").ToString
                End If
                'Start Date
                If Not datareader("Start Date").Equals(DBNull.Value) Then
                    dtpDateSarted.Value = CDate(datareader("Start Date")).Date
                End If
                'End Date
                If Not datareader("Contract End").Equals(DBNull.Value) Then
                    dtpContractEnds.Value = CDate(datareader("Contract End")).Date
                End If
                'Address
                If Not datareader("Address").Equals(DBNull.Value) Then
                    txtAddress.Text = datareader("Address").ToString
                End If
                'Is Scheduler Contact
                If Not datareader("IsSchedulerContact").Equals(DBNull.Value) Then
                    ckbIsScheduler.Checked = CBool(datareader("IsSchedulerContact"))
                End If
                'Payroll Number
                If Not datareader("PayrollNumber").Equals(DBNull.Value) Then
                    txtPayrollNumber.Text = datareader("PayrollNumber").ToString
                End If
                'Waiter Number
                If Not datareader("Waiter Number").Equals(DBNull.Value) Then
                    txtWaiterNumber.Text = datareader("Waiter Number").ToString
                End If
                'Payment Type
                If Not datareader("Payment Type").Equals(DBNull.Value) Then
                    cmbPaymentType.SelectedItem = datareader("Payment Type").ToString
                End If
                'Commission
                If Not datareader("Commission_Perc").Equals(DBNull.Value) Then
                    txtCommission.Text = datareader("Commission_Perc").ToString
                End If
                'Wage
                If Not datareader("Wage_Salary").Equals(DBNull.Value) Then
                    txtWage.Text = datareader("Wage_Salary").ToString
                End If
                'Wage
                If Not datareader("Loan").Equals(DBNull.Value) Then
                    txtLoanAmount.Text = datareader("Loan").ToString
                End If
                'Account Holder
                If Not datareader("Emp_BankAccHolder").Equals(DBNull.Value) Then
                    txtAccountHolder.Text = datareader("Emp_BankAccHolder").ToString
                End If
                'Bank Name
                If Not datareader("Emp_BankName").Equals(DBNull.Value) Then
                    txtBankName.Text = datareader("Emp_BankName").ToString
                End If
                'Acc No.
                If Not datareader("Emp_BankAccNo").Equals(DBNull.Value) Then
                    txtAccNumber.Text = datareader("Emp_BankAccNo").ToString
                End If
                'Branch No.
                If Not datareader("Emp_BankBranch").Equals(DBNull.Value) Then
                    txtBranchNumber.Text = datareader("Emp_BankBranch").ToString
                End If
            End While
        End Using

    End Sub
#End Region

    Private Sub frmEmployee_New_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Employees -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
        If isEditEmployee Then
            GetEditEmployee(EditEmployeeID)
        Else
            isEditEmployee = False
        End If
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClearBasic.Click
        cmbEmployeeType.Text = ""
        txtIDNumber.Text = ""
        txtName.Text = ""
        txtSurname.Text = ""
        cmbStatus.Text = ""
        txtContactNumber1.Text = ""
        txtContact2.Text = ""
        dtpContractEnds.Refresh()
        dtpDateSarted.Refresh()
        txtAddress.Text = ""
        ckbIsScheduler.Checked = False
        cmbEmployeeType.Focus()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If Me.cmbEmployeeType.Text = "" Or Me.txtName.Text = "" Then
            MsgBox("Please add at a minimum, Employee Name and Employee Type!")
            Exit Sub
        End If
        'Waiter's details validation
        If cmbEmployeeType.Text = "WAITER" Then

            If txtCommission.Text = "" Then
                MsgBox("Please supply waiter's commission !")
                txtCommission.Focus()
                Exit Sub
            End If

            'If txtIDNumber.Text = "" Then
            '    MsgBox("Please supply waiter's ID Number in a correct format !")
            '    tbBasicDetails.Focus()
            '    txtIDNumber.Focus()
            '    Exit Sub
            'End If

            'If cmbPaymentType.SelectedIndex = -1 Or cmbPaymentType.Text = "" Then
            '    MsgBox("Please supply waiter's payment code !")
            '    cmbPaymentType.Focus()
            '    Exit Sub
            'End If

        End If

        'Grill's details validation
        If cmbEmployeeType.Text = "GRILL" Then

            'If txtWage.Text = "" Or Not IsNumeric(txtWage.Text) Then
            '    MsgBox("Please grill's wages in a correct format!")
            '    txtWage.Focus()
            '    Exit Sub
            'End If

            'If txtIDNumber.Text = "" Then
            '    MsgBox("Please supply waiter's ID Number in a correct format !")
            '    tbBasicDetails.Focus()
            '    txtIDNumber.Focus()
            '    Exit Sub
            'End If

            'If cmbPaymentType.SelectedIndex = -1 Or cmbPaymentType.Text = "" Then
            '    MsgBox("Please supply waiter's payment code !")
            '    cmbPaymentType.Focus()
            '    Exit Sub
            'End If

        End If

        If txtContactNumber1.Text.Trim.Length <> 10 Or Not IsNumeric(txtContactNumber1.Text.Trim) Then
            MsgBox("Please supply contact number 1 in a correct format!")
            txtContactNumber1.Focus()
            Exit Sub
        End If

        'If txtContact2.Text.Trim.Length <> 10 Or Not IsNumeric(txtContact2.Text.Trim) Then
        '    MsgBox("Please supply contact number 2 in a correct format!")
        '    txtContact2.Focus()
        '    Exit Sub
        'End If

        'If txtPayrollNumber.Text.Trim.Length = 0 Then
        '    MsgBox("Please type in Payroll Number.")
        '    txtPayrollNumber.Focus()
        '    Exit Sub
        'End If

        'If IsNumeric(txtAccountHolder.Text.Trim) Then
        '    MsgBox("Please supply Account holder in a correct format!")
        '    txtAccountHolder.Focus()
        '    Exit Sub
        'End If

        'If Not IsNumeric(txtAccNumber.Text.Trim) Then
        '    MsgBox("Please supply Account number in a correct format!")
        '    txtAccNumber.Focus()
        '    Exit Sub
        'End If

        If isEditEmployee Then
            Update_Employee(EditEmployeeID)
        Else
            Add_Employee()
        End If

    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Private Sub ClearForm()
        With Me
            'Basic Details
            .cmbEmployeeType.Text = ""
            .txtIDNumber.Text = ""
            .txtName.Text = ""
            .txtSurname.Text = ""
            .cmbStatus.Text = ""
            .txtContactNumber1.Text = ""
            .txtContact2.Text = ""
            dtpContractEnds.Refresh()
            dtpDateSarted.Refresh()
            txtAddress.Text = ""
            'Paye Details
            .txtPayrollNumber.Text = ""
            .txtWaiterNumber.Text = ""
            .cmbPaymentType.Text = ""
            .txtCommission.Text = ""
            .txtWage.Text = ""
            .txtLoanAmount.Text = ""
            'Banking Details
            .txtAccNumber.Text = ""
            .txtBankName.Text = ""
            .txtAccountHolder.Text = ""
            .txtBranchNumber.Text = ""
            .cmbEmployeeType.Focus()

        End With
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnClearPaye.Click
        txtWaiterNumber.Text = ""
        cmbPaymentType.Text = ""
        txtCommission.Text = ""
        txtWage.Text = ""
        txtLoanAmount.Text = ""
        txtWaiterNumber.Focus()
    End Sub
    Private Sub btnClearBanking_Click(sender As Object, e As EventArgs) Handles btnClearBanking.Click
        txtAccNumber.Text = ""
        txtBankName.Text = ""
        txtAccountHolder.Text = ""
        txtBranchNumber.Text = ""
        txtAccountHolder.Focus()
    End Sub
End Class