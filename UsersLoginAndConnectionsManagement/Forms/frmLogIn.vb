﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class frmLogIn
#Region "Variables"
    Private Logic As New DataMethods
    Dim MBData As New DataExtractsClass
    Dim PassSecurity As New PasswordSecurity
#End Region

    Private Sub btnLogIn_Click(sender As Object, e As EventArgs) Handles btnLogIn.Click

        Try

            Dim UserName As String = ""
            Dim Password As String = ""

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            If txtUserName.TextLength = 0 Or IsNumeric(txtUserName.Text) Then
                MsgBox("Please type in your username in a correct format", MsgBoxStyle.Information)
                txtUserName.Focus()
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If
            If txtPassword.TextLength = 0 Then
                MsgBox("Please type in your password", MsgBoxStyle.Information)
                txtPassword.Focus()
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            UserName = CStr(txtUserName.Text.Trim)
            Password = CStr(txtPassword.Text.Trim)
            Password = PassSecurity.EncryptString(Password).ToString

            Dim UserOnLogIn As Tenant = Nothing
            Try
                UserOnLogIn = Logic.GetUserLogin(UserName, Password)
            Catch ex As Exception
                MessageBox.Show("Logging in Error", "MagicBox Security", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            If Not IsNothing(UserOnLogIn) Then

                If (UserOnLogIn.Password <> "" And UserOnLogIn.Password = Password) _
                 And (UserOnLogIn.TenantName <> "" And UserOnLogIn.TenantName = UserName) Then

                    Dim UserGroupID As Integer = UserOnLogIn.TenantGroup
                    Dim UserID As Integer = UserOnLogIn.TenantID
                    Dim FirstName As String = UserOnLogIn.FirstName
                    Dim Surname As String = UserOnLogIn.Surname
                    Dim Uname As String = UserOnLogIn.TenantName

                    My.Settings.GroupID = UserGroupID
                    My.Settings.UserID = UserID
                    My.Settings.Firstname = FirstName
                    My.Settings.Surname = Surname
                    My.Settings.Username = Uname
                    My.Settings.Save()
                    Dim Count As Integer = Logic.CountStores(UserGroupID)

                    If Count <> 0 Then
                        Dim GroupConnString As String = Logic.GetGroupConnString(UserGroupID)
                        If GroupConnString <> "" Then
                            My.Settings.CS_Setting = GroupConnString
                            My.Settings.Save()

                            Dim GroupStores = Logic.GetGroupStores(UserGroupID)
                            'For Each Store In GroupStores
                            '     MBData.UpdateGroupCompanies(Store)
                            'Next

                        Else
                            MessageBox.Show("The Group's connection details are invalid, The system cannot continue, please contact your Systems administrator to assist with this.", "Sever connection details", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.Close()
                            Exit Sub
                        End If
                    Else
                        MessageBox.Show("There are no stores under your group, The system cannot continue, please contact your Systems administrator to assist with this.", "No stores", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Me.Close()
                        Exit Sub
                    End If

                    Me.Close()

                    Dim frmDefWorkSpace As New frmDefaultWorkspace
                    frmDefWorkSpace.TenID = UserOnLogIn.TenantID
                    frmDefWorkSpace.ShowDialog()

                    If frmDefWorkSpace.StoresCount <> 0 Then

                        Globals.Ribbons.Ribbon1.btnMainLogIn.Label = "Log Out"
                        'Globals.Ribbons.Ribbon1.grpDailyProcessing.Visible = True
                        Globals.Ribbons.Ribbon1.grpMaintanance.Visible = True
                        Globals.Ribbons.Ribbon1.grpSuppliers.Visible = True
                        'Globals.Ribbons.Ribbon1.grpCustomers.Visible = True
                        'Globals.Ribbons.Ribbon1.grpCustomers.Visible = True
                        Globals.Ribbons.Ribbon1.grpGeneral.Visible = True
                        Globals.Ribbons.Ribbon1.grpIntelligence.Visible = True
                        'Globals.Ribbons.Ribbon1.grpModes.Visible = True
                        Globals.Ribbons.Ribbon1.grpAdmin.Visible = True
                        Globals.Ribbons.Ribbon1.GrpAccounting.Visible = True
                        Globals.Ribbons.Ribbon1.grpApprovals.Visible = True

                    End If

                Else
                    MessageBox.Show("Invalid log in details", "MagicBox Security", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtUserName.Clear()
                    txtPassword.Clear()
                    txtUserName.Focus()
                End If
            Else
                MessageBox.Show("Invalid log in details", "MagicBox Security", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtUserName.Clear()
                txtPassword.Clear()
                txtUserName.Focus()
            End If

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow


        Catch ex As Exception
            MessageBox.Show("An error occured with details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try

    End Sub
    Private Sub txtPassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            btnLogIn_Click(sender, e)
            e.Handled = True
        End If
    End Sub

    Private Sub btnLogIn_KeyPress(sender As Object, e As KeyPressEventArgs) Handles btnLogIn.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            btnLogIn_Click(sender, e)
            e.Handled = True
        End If
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub frmLogIn_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class