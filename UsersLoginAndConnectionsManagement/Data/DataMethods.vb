﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Collections

Public Class DataMethods

#Region "Variables"
    Dim MBData As New DataExtractsClass
    Dim UsersManagementContext As New LoginAndConnectionDataContext(My.Settings.MagicBoxCentral)
    Dim PassSecurity As New PasswordSecurity
#End Region

#Region "Methods"
    Public Function GetUserLogin(ByVal UserName As String, ByVal Password As String) As Tenant
        Return UsersManagementContext.Tenants.Where(Function(us) us.TenantName = UserName And us.Password = Password).FirstOrDefault
    End Function
    Public Function GetUserTypes() As IQueryable(Of SystemUserType)
        Return UsersManagementContext.SystemUserTypes.OrderBy(Function(fn) fn.Description)
    End Function
    Public Function GetGroups() As IQueryable(Of Group)
        Return UsersManagementContext.Groups.OrderBy(Function(fn) fn.DateModified)
    End Function
    Public Function GetTenants() As IQueryable(Of Tenant)
        Return UsersManagementContext.Tenants.OrderBy(Function(fn) fn.ModifiedDate)
    End Function
    Public Function GetGroupTenants(ByVal GroupID As Integer) As IQueryable(Of Tenant)
        Return (From gTenant In UsersManagementContext.Tenants
               Where gTenant.TenantGroup = GroupID
               Select gTenant)
    End Function
    Public Function GetStores() As IQueryable(Of GroupStore)
        Return UsersManagementContext.GroupStores.OrderBy(Function(fn) fn.DateModified)
    End Function
    Public Function CountStores(ByVal GroupID As Integer)
        Dim GStores = (From Gs In UsersManagementContext.GroupStores
        Where Gs.GroupID = GroupID
        Select Gs).Count()
        Return GStores
    End Function
    Public Function GetGroupConnString(ByVal GroupID As Integer) As String

        Dim Grp As Group = (From gps In UsersManagementContext.Groups
                Where gps.GroupID = GroupID
                Select gps).FirstOrDefault

        If IsNothing(Grp) Then
            Return ""
            Exit Function
        End If

        Try

            Dim Servername As String = Grp.GroupServerAddress.ToString
            Dim DatabaseName As String = Grp.DatabaseName.ToString
            Dim Username As String = Grp.DatabaseUserName.ToString
            Dim Password As String = Grp.DatabasePassword.ToString
            Password = PassSecurity.DecryptString(Password).ToString

            Dim ConnString As String = ""

            ConnString = "Server=" & Servername & ";Database=" & DatabaseName & ";Uid=" & Username & ";Pwd=" & Password

            Try
                Dim cn As New SqlConnection(ConnString)
                Using cn
                    cn.Open()
                    If cn.State = Data.ConnectionState.Open Then
                        ConnString = ConnString
                    Else
                        ConnString = ""
                    End If
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Return ConnString

        Catch ex As Exception
            Return ""
        End Try

    End Function
    Public Function GetTenantStores(ByVal TenantID As Integer) As IQueryable(Of TenantStore)
        Return (From TenStores In UsersManagementContext.TenantStores
               Where TenStores.TenantID = TenantID
               Select TenStores)
    End Function
    Public Function GetTenantStoreNames(ByVal TenantID As Integer) As List(Of String)
        Dim listStoreNames As List(Of String) = (From TenStores In UsersManagementContext.TenantStores
               Where TenStores.TenantID = TenantID
               Select TenStores.GroupStore.StoreName).ToList

        Return listStoreNames
    End Function
    Public Function StoreNameIDPair(ByVal TenantID As Integer)

        Dim StoreIDName = (From TenStores In UsersManagementContext.TenantStores
               Where TenStores.TenantID = TenantID And TenStores.IsActive = True
               Select TenStores.GroupStore Order By GroupStore.StoreName)

        Dim TenantStoreIDName As New Hashtable()
        For Each store In StoreIDName
            Dim StoreID As Integer = CInt(MBData.GetCompanyID(store.StoreName))
            TenantStoreIDName.Add(StoreID, store.StoreName)
        Next

        Return TenantStoreIDName

    End Function
    Public Function GetGroupStores(ByVal GroupID As Integer) As List(Of String)
        Dim listStores As List(Of String) = (From Stores In UsersManagementContext.GroupStores
               Where Stores.GroupID = GroupID
               Select Stores.StoreName).ToList

        Return listStores
    End Function
    Public Function GetStoresByGroup(ByVal GroupID As Integer) As IQueryable(Of GroupStore)
        Return UsersManagementContext.GroupStores.Where(Function(fn) fn.GroupID = GroupID)
    End Function
    Public Function GetSpecificUserType(ByVal UserTypeID As Integer) As SystemUserType
        Return (From Utype In UsersManagementContext.SystemUserTypes
               Where Utype.SystemUserTypeID = UserTypeID
               Select Utype).FirstOrDefault
    End Function
    Public Function CheckIfUserProfileExist(ByVal Username As String, ByVal Password As String,
                                            ByVal EmailAddress As String, ByVal FirstName As String,
                                            ByVal Surname As String) As Boolean
        Dim UserProfile As Tenant
        UserProfile = (From UserPro In UsersManagementContext.Tenants
        Where (UserPro.TenantName = Username And UserPro.Password = Password) _
        Or (UserPro.TenantName = Username And UserPro.Email = EmailAddress) _
        Or (UserPro.FirstName = FirstName And UserPro.Surname = Surname)
                         Select UserPro).FirstOrDefault
        If IsNothing(UserProfile) Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Sub AddUserType(ByVal Description As String, ByVal Details As String)
        Dim NewUserType As New SystemUserType
        With NewUserType
            .Description = Description
            .DetailDescription = Details
            .DateCreated = CDate(Date.Now)
        End With
        UsersManagementContext.SystemUserTypes.InsertOnSubmit(NewUserType)
        SaveChanges()
    End Sub
    Public Sub AddUserStore(ByVal TenantID As Integer, ByVal StoreID As Integer,
                            ByVal IsAdmin As Boolean, ByVal IsActive As Boolean)
        Dim NewUserStore As New TenantStore
        With NewUserStore
            .TenantID = TenantID
            .StoreID = StoreID
            .isAdmin = IsAdmin
            .IsActive = IsActive
            .DateCreated = CDate(Date.Now)
            .Comment = "User stores created by computer :" & My.Computer.Name & ", on " & Date.Now.ToString
        End With
        UsersManagementContext.TenantStores.InsertOnSubmit(NewUserStore)
        SaveChanges()
    End Sub
    Public Sub AddStore(ByVal StoreName As String, ByVal GroupID As Integer)
        Dim NewStore As New GroupStore
        With NewStore
            .GroupID = GroupID
            .StoreName = StoreName
            .DateCreated = CDate(Date.Now)
        End With
        UsersManagementContext.GroupStores.InsertOnSubmit(NewStore)
        SaveChanges()
    End Sub
    Public Function AddUser(ByVal UserName As String, ByVal Password As String,
                       ByVal EmailAddress As String, ByVal isActive As Boolean,
                       ByVal UserTypeID As Integer, ByVal GroupID As Integer,
                       ByVal FirstName As String, ByVal Surname As String) As Integer

        Dim NewUser As New Tenant
        With NewUser
            .FirstName = FirstName
            .Surname = Surname
            .TenantName = UserName
            .Password = Password
            .Email = EmailAddress
            .IsActive = isActive
            .UserTypeID = UserTypeID
            .TenantGroup = GroupID
            .CreateDate = CDate(Date.Now)
        End With
        UsersManagementContext.Tenants.InsertOnSubmit(NewUser)
        SaveChanges()

        Return NewUser.TenantID
    End Function
    Public Sub AddGroup(ByVal GroupName As String, ByVal LegalName As String, _
                        ByVal ServerAddress As String, ByVal DatabaseName As String, ByVal Username As String, _
                        ByVal Password As String, ByVal IsGroupActive As Boolean)

        Dim NewGroup As New Group
        With NewGroup
            .GroupName = GroupName
            .LegalName = LegalName
            .GroupServerAddress = ServerAddress
            .DatabaseName = DatabaseName
            .DatabaseUserName = Username
            .DatabasePassword = Password
            .DateCreated = CDate(Date.Now)
            .IsGroupActive = IsGroupActive
        End With
        UsersManagementContext.Groups.InsertOnSubmit(NewGroup)
        SaveChanges()

    End Sub
    Public Function GetSpecificGroup(ByVal GroupID As Integer) As Group
        Return (From SGroups In UsersManagementContext.Groups
               Where SGroups.GroupID = GroupID
               Select SGroups).FirstOrDefault
    End Function
    Public Function GetSpecificUserStore(ByVal TenantStoreID As Integer) As TenantStore
        Return (From Ustore In UsersManagementContext.TenantStores
               Where Ustore.TenantStoreID = TenantStoreID
               Select Ustore).FirstOrDefault
    End Function
    Public Sub DeleteGroup(ByVal GroupID As Integer)
        Dim GrouptoDelete As Group = GetSpecificGroup(GroupID)
        UsersManagementContext.Groups.DeleteOnSubmit(GrouptoDelete)
        SaveChanges()
    End Sub
    Public Sub DeleteUserStores(ByVal UserStoreID As Integer)
        Dim UserStoreToDelete As TenantStore = GetSpecificUserStore(UserStoreID)
        UsersManagementContext.TenantStores.DeleteOnSubmit(UserStoreToDelete)
        SaveChanges()
    End Sub
    Public Sub UpdateUserTypes(ByVal UserTypeId As Integer, ByVal Desc As String, ByVal Detail As String)

        Dim UserTypeOnEdit As SystemUserType = GetSpecificUserType(UserTypeId)
        UserTypeOnEdit.Description = Desc
        UserTypeOnEdit.DetailDescription = Detail
        UserTypeOnEdit.DateModified = CDate(Date.Now)
        SaveChanges()

    End Sub
    Public Sub SaveChanges()
        Try
            UsersManagementContext.SubmitChanges()
        Catch ex As Exception
            MessageBox.Show("A system error occured :" & ex.Message & vbCrLf & "Please report to system support with details", "System Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
End Class
