﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashupTools
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_CU_Dates = New System.Windows.Forms.DataGridView()
        Me.DGV_CU_Details = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TB_CU_Remover = New System.Windows.Forms.TabPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LB_Remove_Cashup = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        CType(Me.DGV_CU_Dates, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_CU_Details, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TB_CU_Remover.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGV_CU_Dates
        '
        Me.DGV_CU_Dates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_CU_Dates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_CU_Dates.Location = New System.Drawing.Point(0, 0)
        Me.DGV_CU_Dates.Name = "DGV_CU_Dates"
        Me.DGV_CU_Dates.RowTemplate.Height = 24
        Me.DGV_CU_Dates.Size = New System.Drawing.Size(201, 536)
        Me.DGV_CU_Dates.TabIndex = 1
        '
        'DGV_CU_Details
        '
        Me.DGV_CU_Details.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_CU_Details.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_CU_Details.Location = New System.Drawing.Point(0, 0)
        Me.DGV_CU_Details.Name = "DGV_CU_Details"
        Me.DGV_CU_Details.RowTemplate.Height = 24
        Me.DGV_CU_Details.Size = New System.Drawing.Size(967, 536)
        Me.DGV_CU_Details.TabIndex = 2
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TB_CU_Remover)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1186, 571)
        Me.TabControl1.TabIndex = 3
        '
        'TB_CU_Remover
        '
        Me.TB_CU_Remover.Controls.Add(Me.SplitContainer1)
        Me.TB_CU_Remover.Location = New System.Drawing.Point(4, 25)
        Me.TB_CU_Remover.Name = "TB_CU_Remover"
        Me.TB_CU_Remover.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_CU_Remover.Size = New System.Drawing.Size(1178, 542)
        Me.TB_CU_Remover.TabIndex = 0
        Me.TB_CU_Remover.Text = "Cash Up Remover"
        Me.TB_CU_Remover.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.DGV_CU_Dates)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DGV_CU_Details)
        Me.SplitContainer1.Size = New System.Drawing.Size(1172, 536)
        Me.SplitContainer1.SplitterDistance = 201
        Me.SplitContainer1.TabIndex = 3
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LB_Remove_Cashup, Me.ToolStripProgressBar1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 511)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(967, 25)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LB_Remove_Cashup
        '
        Me.LB_Remove_Cashup.Image = Global.MB.My.Resources.Resources.Off2
        Me.LB_Remove_Cashup.Name = "LB_Remove_Cashup"
        Me.LB_Remove_Cashup.Size = New System.Drawing.Size(192, 20)
        Me.LB_Remove_Cashup.Tag = "S_RemoveChashup"
        Me.LB_Remove_Cashup.Text = "Remove Selected Cashup"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 19)
        '
        'frmCashupTools
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1186, 571)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmCashupTools"
        Me.Tag = "S_CashupTools"
        Me.Text = "frmCashupTools"
        CType(Me.DGV_CU_Dates, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_CU_Details, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TB_CU_Remover.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DGV_CU_Dates As System.Windows.Forms.DataGridView
    Friend WithEvents DGV_CU_Details As System.Windows.Forms.DataGridView
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TB_CU_Remover As System.Windows.Forms.TabPage
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents LB_Remove_Cashup As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
End Class
