﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class frmBankingDetails
#Region "Variables"
    Public FirstName As String = ""
    Public LastName As String = ""
    Public BankSetting As String = ""

#End Region
    Private Sub frmBankingDetails_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Banking Details -> Version : " & My.Settings.Setting_Version
        txtEmpNames.Text = FirstName & " " & LastName
        LockForm(Me)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If txtAccountHolder.Text.Trim = "" Or IsNumeric(txtAccountHolder.Text) Then
            MsgBox("Please supply Account holder's name in a correct format!")
            txtAccountHolder.Focus()
            Exit Sub
        End If

        If txtBankName.Text.Trim = "" Or IsNumeric(txtBankName.Text) Then
            MsgBox("Please supply Bank name in a correct format!")
            txtBankName.Focus()
            Exit Sub
        End If

        If txtAccNumber.Text.Trim = "" Or Not IsNumeric(txtAccNumber.Text) Then
            MsgBox("Please supply Account number in a correct format!")
            txtAccNumber.Focus()
            Exit Sub
        End If

        If txtBranchNumber.Text.Trim = "" Then
            MsgBox("Please supply Branch name/Number!")
            txtBranchNumber.Focus()
            Exit Sub
        End If

        Dim AccHolder As String = txtAccountHolder.Text.ToString
        Dim BankName As String = txtBankName.Text.ToString
        Dim AccNumber As String = txtAccNumber.Text.Trim.ToString
        Dim Branch As String = txtBranchNumber.Text.Trim.ToString

        Add_Employee_BankingDetails(AccHolder, BankName, AccNumber, Branch)

    End Sub
    Sub Add_Employee_BankingDetails(ByVal AccHolder As String, ByVal BankName As String, ByVal AccNumber As String, ByVal BranchName As String)
        Try
            Dim sSQL As String = ""
            Dim oCompanyID As Integer = Nothing

            If My.Settings.Setting_CompanyID.ToString = "" Or My.Settings.Setting_CompanyID = Nothing Then
                MsgBox("Please set default company in order to continue with operations!")
                Exit Sub
            Else
                oCompanyID = My.Settings.Setting_CompanyID.ToString
            End If

            If My.Settings.DBType = "Access" Then

                sSQL = "UPDATE Employees set "
                sSQL = sSQL & "Emp_BankName = '" & BankName
                sSQL = sSQL & "',  Emp_BankAccHolder = '" & AccHolder
                sSQL = sSQL & "', Emp_BankAccNo = '" & AccNumber
                sSQL = sSQL & "', Emp_BankBranch = '" & BranchName
                sSQL = sSQL & "' WHERE (First_Name = '" & FirstName
                sSQL = sSQL & "' AND Last_Name = '" & LastName
                sSQL = sSQL & "') AND Co_ID = " & oCompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)

                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "UPDATE Employees set "
                sSQL = sSQL & "Emp_BankName = '" & BankName
                sSQL = sSQL & "',  Emp_BankAccHolder = '" & AccHolder
                sSQL = sSQL & "', Emp_BankAccNo = '" & AccNumber
                sSQL = sSQL & "', Emp_BankBranch = '" & BranchName
                sSQL = sSQL & "' WHERE (First_Name = '" & FirstName
                sSQL = sSQL & "' AND Last_Name = '" & LastName
                sSQL = sSQL & "') AND Co_ID = " & oCompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            End If

            If BankSetting = "ADD" Then
                MsgBox("Employee's banking details successfully added!")
            Else
                MsgBox("Employee's banking details successfully updated!")
            End If

            txtAccNumber.Text = ""
            txtAccountHolder.Text = ""
            txtBankName.Text = ""
            txtBranchNumber.Text = ""
            txtEmpNames.Text = ""
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
            'Me.Edit_ID_Label.Text = "None"
        End Try

    End Sub
End Class