﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEnquire
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Btn_Execute = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.TB_RefDesc = New System.Windows.Forms.TextBox()
        Me.RB_Desc = New System.Windows.Forms.RadioButton()
        Me.RB_Ref = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CB_FinCat = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CB_Employees = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CB_Suppliers = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CB_Accounts = New System.Windows.Forms.ComboBox()
        Me.RB_ByName = New System.Windows.Forms.RadioButton()
        Me.RB_ByCode = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DT_EndDate = New System.Windows.Forms.DateTimePicker()
        Me.DT_StartDate = New System.Windows.Forms.DateTimePicker()
        Me.DGV_Enquire = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LBL_Movement = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DGV_Enquire, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Btn_Execute)
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DT_EndDate)
        Me.GroupBox1.Controls.Add(Me.DT_StartDate)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 16)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1356, 145)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enquire Selection"
        '
        'Btn_Execute
        '
        Me.Btn_Execute.Location = New System.Drawing.Point(13, 87)
        Me.Btn_Execute.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Execute.Name = "Btn_Execute"
        Me.Btn_Execute.Size = New System.Drawing.Size(348, 28)
        Me.Btn_Execute.TabIndex = 7
        Me.Btn_Execute.Text = "Execute Enquire"
        Me.Btn_Execute.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.TB_RefDesc)
        Me.GroupBox4.Controls.Add(Me.RB_Desc)
        Me.GroupBox4.Controls.Add(Me.RB_Ref)
        Me.GroupBox4.Location = New System.Drawing.Point(369, 81)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Size = New System.Drawing.Size(468, 57)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Search Type"
        '
        'TB_RefDesc
        '
        Me.TB_RefDesc.Location = New System.Drawing.Point(216, 25)
        Me.TB_RefDesc.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_RefDesc.Name = "TB_RefDesc"
        Me.TB_RefDesc.Size = New System.Drawing.Size(243, 22)
        Me.TB_RefDesc.TabIndex = 2
        '
        'RB_Desc
        '
        Me.RB_Desc.AutoSize = True
        Me.RB_Desc.Checked = True
        Me.RB_Desc.Location = New System.Drawing.Point(104, 25)
        Me.RB_Desc.Margin = New System.Windows.Forms.Padding(4)
        Me.RB_Desc.Name = "RB_Desc"
        Me.RB_Desc.Size = New System.Drawing.Size(100, 21)
        Me.RB_Desc.TabIndex = 1
        Me.RB_Desc.TabStop = True
        Me.RB_Desc.Text = "Description"
        Me.RB_Desc.UseVisualStyleBackColor = True
        '
        'RB_Ref
        '
        Me.RB_Ref.AutoSize = True
        Me.RB_Ref.Location = New System.Drawing.Point(9, 25)
        Me.RB_Ref.Margin = New System.Windows.Forms.Padding(4)
        Me.RB_Ref.Name = "RB_Ref"
        Me.RB_Ref.Size = New System.Drawing.Size(95, 21)
        Me.RB_Ref.TabIndex = 0
        Me.RB_Ref.Text = "Reference"
        Me.RB_Ref.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.CB_FinCat)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.CB_Employees)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.CB_Suppliers)
        Me.GroupBox3.Location = New System.Drawing.Point(847, 23)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(412, 114)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Enitity"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 91)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 17)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Financial Category:"
        '
        'CB_FinCat
        '
        Me.CB_FinCat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.CB_FinCat.FormattingEnabled = True
        Me.CB_FinCat.Location = New System.Drawing.Point(147, 82)
        Me.CB_FinCat.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_FinCat.Name = "CB_FinCat"
        Me.CB_FinCat.Size = New System.Drawing.Size(251, 24)
        Me.CB_FinCat.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 58)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Employee:"
        '
        'CB_Employees
        '
        Me.CB_Employees.FormattingEnabled = True
        Me.CB_Employees.Location = New System.Drawing.Point(87, 53)
        Me.CB_Employees.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Employees.Name = "CB_Employees"
        Me.CB_Employees.Size = New System.Drawing.Size(311, 24)
        Me.CB_Employees.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 25)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Supplier:"
        '
        'CB_Suppliers
        '
        Me.CB_Suppliers.FormattingEnabled = True
        Me.CB_Suppliers.Location = New System.Drawing.Point(87, 20)
        Me.CB_Suppliers.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Suppliers.Name = "CB_Suppliers"
        Me.CB_Suppliers.Size = New System.Drawing.Size(311, 24)
        Me.CB_Suppliers.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CB_Accounts)
        Me.GroupBox2.Controls.Add(Me.RB_ByName)
        Me.GroupBox2.Controls.Add(Me.RB_ByCode)
        Me.GroupBox2.Location = New System.Drawing.Point(369, 23)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(468, 57)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Account Selection"
        '
        'CB_Accounts
        '
        Me.CB_Accounts.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.CB_Accounts.FormattingEnabled = True
        Me.CB_Accounts.Location = New System.Drawing.Point(216, 20)
        Me.CB_Accounts.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Accounts.Name = "CB_Accounts"
        Me.CB_Accounts.Size = New System.Drawing.Size(243, 24)
        Me.CB_Accounts.TabIndex = 2
        '
        'RB_ByName
        '
        Me.RB_ByName.AutoSize = True
        Me.RB_ByName.Location = New System.Drawing.Point(104, 25)
        Me.RB_ByName.Margin = New System.Windows.Forms.Padding(4)
        Me.RB_ByName.Name = "RB_ByName"
        Me.RB_ByName.Size = New System.Drawing.Size(86, 21)
        Me.RB_ByName.TabIndex = 1
        Me.RB_ByName.Text = "By Name"
        Me.RB_ByName.UseVisualStyleBackColor = True
        '
        'RB_ByCode
        '
        Me.RB_ByCode.AutoSize = True
        Me.RB_ByCode.Checked = True
        Me.RB_ByCode.Location = New System.Drawing.Point(9, 25)
        Me.RB_ByCode.Margin = New System.Windows.Forms.Padding(4)
        Me.RB_ByCode.Name = "RB_ByCode"
        Me.RB_ByCode.Size = New System.Drawing.Size(82, 21)
        Me.RB_ByCode.TabIndex = 0
        Me.RB_ByCode.TabStop = True
        Me.RB_ByCode.Text = "By Code"
        Me.RB_ByCode.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 63)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "End Date:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Start Date:"
        '
        'DT_EndDate
        '
        Me.DT_EndDate.CustomFormat = "dd MMMM yyyy"
        Me.DT_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_EndDate.Location = New System.Drawing.Point(95, 55)
        Me.DT_EndDate.Margin = New System.Windows.Forms.Padding(4)
        Me.DT_EndDate.Name = "DT_EndDate"
        Me.DT_EndDate.Size = New System.Drawing.Size(265, 22)
        Me.DT_EndDate.TabIndex = 1
        '
        'DT_StartDate
        '
        Me.DT_StartDate.CustomFormat = "dd MMMM yyyy"
        Me.DT_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_StartDate.Location = New System.Drawing.Point(95, 23)
        Me.DT_StartDate.Margin = New System.Windows.Forms.Padding(4)
        Me.DT_StartDate.Name = "DT_StartDate"
        Me.DT_StartDate.Size = New System.Drawing.Size(265, 22)
        Me.DT_StartDate.TabIndex = 0
        '
        'DGV_Enquire
        '
        Me.DGV_Enquire.AllowUserToAddRows = False
        Me.DGV_Enquire.AllowUserToDeleteRows = False
        Me.DGV_Enquire.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DGV_Enquire.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_Enquire.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_Enquire.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGV_Enquire.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.DGV_Enquire.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Enquire.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGV_Enquire.Location = New System.Drawing.Point(17, 187)
        Me.DGV_Enquire.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_Enquire.Name = "DGV_Enquire"
        Me.DGV_Enquire.Size = New System.Drawing.Size(1356, 379)
        Me.DGV_Enquire.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(873, 165)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 17)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Movement:"
        '
        'LBL_Movement
        '
        Me.LBL_Movement.AutoSize = True
        Me.LBL_Movement.Location = New System.Drawing.Point(961, 165)
        Me.LBL_Movement.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LBL_Movement.Name = "LBL_Movement"
        Me.LBL_Movement.Size = New System.Drawing.Size(16, 17)
        Me.LBL_Movement.TabIndex = 9
        Me.LBL_Movement.Text = "0"
        '
        'FrmEnquire
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1389, 599)
        Me.Controls.Add(Me.LBL_Movement)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DGV_Enquire)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FrmEnquire"
        Me.Tag = "S_Transaction&Enquire"
        Me.Text = "Loading..... Please Wait....."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DGV_Enquire, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents RB_Desc As System.Windows.Forms.RadioButton
    Friend WithEvents RB_Ref As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CB_Employees As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CB_Suppliers As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CB_Accounts As System.Windows.Forms.ComboBox
    Friend WithEvents RB_ByName As System.Windows.Forms.RadioButton
    Friend WithEvents RB_ByCode As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DT_EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents DT_StartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Btn_Execute As System.Windows.Forms.Button
    Friend WithEvents TB_RefDesc As System.Windows.Forms.TextBox
    Friend WithEvents DGV_Enquire As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CB_FinCat As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents LBL_Movement As System.Windows.Forms.Label
End Class
