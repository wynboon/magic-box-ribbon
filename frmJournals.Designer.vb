﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJournals
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJournals))
        Me.Date_DateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.EditButton = New System.Windows.Forms.Button()
        Me.DeleteButton = New System.Windows.Forms.Button()
        Me.Add_Button = New System.Windows.Forms.Button()
        Me.cmbJournalType = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Close_Button = New System.Windows.Forms.Button()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Process_Batch_Button = New System.Windows.Forms.Button()
        Me.Label_Processed = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label_Version = New System.Windows.Forms.Label()
        Me.Period = New System.Windows.Forms.TextBox()
        Me.DGV_Transactions = New System.Windows.Forms.DataGridView()
        Me.BatchID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Capture_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fin_Year = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAcc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tax_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accounting = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Void = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DR_CR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contra_Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbAccCat3 = New System.Windows.Forms.ComboBox()
        Me.cmbAccCat2 = New System.Windows.Forms.ComboBox()
        Me.cmbAccCat1 = New System.Windows.Forms.ComboBox()
        Me.lblCat1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblSegment4Account = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblSegment4Contra = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbContraCat3 = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbContraCat1 = New System.Windows.Forms.ComboBox()
        Me.cmbContraCat2 = New System.Windows.Forms.ComboBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblSelectedOption = New System.Windows.Forms.Label()
        Me.cmbJournalTypeValues = New System.Windows.Forms.ComboBox()
        Me.dgvPeriods = New System.Windows.Forms.DataGridView()
        Me.dgvSuppliers = New System.Windows.Forms.DataGridView()
        Me.dgvAccounts = New System.Windows.Forms.DataGridView()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdbExclusive = New System.Windows.Forms.RadioButton()
        Me.rdbInclusive = New System.Windows.Forms.RadioButton()
        Me.Reference = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Description = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbTax = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DC = New System.Windows.Forms.ComboBox()
        Me.txtInclusive = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtExclusive = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btn_Templates = New System.Windows.Forms.Button()
        Me.Ckb_StatTran = New System.Windows.Forms.CheckBox()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Date_DateTimePicker
        '
        Me.Date_DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Date_DateTimePicker.Location = New System.Drawing.Point(12, 190)
        Me.Date_DateTimePicker.Margin = New System.Windows.Forms.Padding(4)
        Me.Date_DateTimePicker.Name = "Date_DateTimePicker"
        Me.Date_DateTimePicker.Size = New System.Drawing.Size(200, 22)
        Me.Date_DateTimePicker.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label5.Location = New System.Drawing.Point(8, 119)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 15)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Period"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label1.Location = New System.Drawing.Point(8, 169)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 15)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Date"
        '
        'EditButton
        '
        Me.EditButton.ForeColor = System.Drawing.Color.MidnightBlue
        Me.EditButton.Location = New System.Drawing.Point(252, 298)
        Me.EditButton.Margin = New System.Windows.Forms.Padding(4)
        Me.EditButton.Name = "EditButton"
        Me.EditButton.Size = New System.Drawing.Size(192, 28)
        Me.EditButton.TabIndex = 7
        Me.EditButton.Text = "&Change Selected"
        Me.EditButton.UseVisualStyleBackColor = True
        '
        'DeleteButton
        '
        Me.DeleteButton.ForeColor = System.Drawing.Color.MidnightBlue
        Me.DeleteButton.Location = New System.Drawing.Point(132, 298)
        Me.DeleteButton.Margin = New System.Windows.Forms.Padding(4)
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(108, 28)
        Me.DeleteButton.TabIndex = 6
        Me.DeleteButton.Text = "&Delete"
        Me.DeleteButton.UseVisualStyleBackColor = True
        '
        'Add_Button
        '
        Me.Add_Button.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Add_Button.Location = New System.Drawing.Point(16, 298)
        Me.Add_Button.Margin = New System.Windows.Forms.Padding(4)
        Me.Add_Button.Name = "Add_Button"
        Me.Add_Button.Size = New System.Drawing.Size(108, 28)
        Me.Add_Button.TabIndex = 5
        Me.Add_Button.Tag = "S_AddJournal"
        Me.Add_Button.Text = "&Add"
        Me.Add_Button.UseVisualStyleBackColor = True
        '
        'cmbJournalType
        '
        Me.cmbJournalType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbJournalType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbJournalType.FormattingEnabled = True
        Me.cmbJournalType.Items.AddRange(New Object() {"General Journal", "Cash-Up Journal", "Supplier Journal", "Employee Journal"})
        Me.cmbJournalType.Location = New System.Drawing.Point(12, 39)
        Me.cmbJournalType.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbJournalType.Name = "cmbJournalType"
        Me.cmbJournalType.Size = New System.Drawing.Size(200, 24)
        Me.cmbJournalType.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label9.Location = New System.Drawing.Point(11, 20)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 15)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Type"
        '
        'Close_Button
        '
        Me.Close_Button.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Close_Button.Location = New System.Drawing.Point(1080, 534)
        Me.Close_Button.Margin = New System.Windows.Forms.Padding(4)
        Me.Close_Button.Name = "Close_Button"
        Me.Close_Button.Size = New System.Drawing.Size(112, 28)
        Me.Close_Button.TabIndex = 71
        Me.Close_Button.Text = "Cl&ose"
        Me.Close_Button.UseVisualStyleBackColor = True
        '
        'ListView1
        '
        Me.ListView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListView1.FullRowSelect = True
        Me.ListView1.HideSelection = False
        Me.ListView1.Location = New System.Drawing.Point(16, 335)
        Me.ListView1.Margin = New System.Windows.Forms.Padding(4)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(1175, 190)
        Me.ListView1.TabIndex = 72
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'Process_Batch_Button
        '
        Me.Process_Batch_Button.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Process_Batch_Button.Location = New System.Drawing.Point(16, 533)
        Me.Process_Batch_Button.Margin = New System.Windows.Forms.Padding(4)
        Me.Process_Batch_Button.Name = "Process_Batch_Button"
        Me.Process_Batch_Button.Size = New System.Drawing.Size(136, 30)
        Me.Process_Batch_Button.TabIndex = 73
        Me.Process_Batch_Button.Text = "&Process Batch"
        Me.Process_Batch_Button.UseVisualStyleBackColor = True
        '
        'Label_Processed
        '
        Me.Label_Processed.AutoSize = True
        Me.Label_Processed.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label_Processed.Location = New System.Drawing.Point(192, 651)
        Me.Label_Processed.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label_Processed.Name = "Label_Processed"
        Me.Label_Processed.Size = New System.Drawing.Size(0, 17)
        Me.Label_Processed.TabIndex = 80
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Brush Script MT", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label13.Location = New System.Drawing.Point(-1, 10)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(125, 37)
        Me.Label13.TabIndex = 81
        Me.Label13.Text = "Magic Box"
        '
        'Label_Version
        '
        Me.Label_Version.AutoSize = True
        Me.Label_Version.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Version.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label_Version.Location = New System.Drawing.Point(1069, 10)
        Me.Label_Version.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label_Version.Name = "Label_Version"
        Me.Label_Version.Size = New System.Drawing.Size(54, 17)
        Me.Label_Version.TabIndex = 82
        Me.Label_Version.Text = "version"
        '
        'Period
        '
        Me.Period.Enabled = False
        Me.Period.Location = New System.Drawing.Point(12, 139)
        Me.Period.Margin = New System.Windows.Forms.Padding(4)
        Me.Period.Name = "Period"
        Me.Period.Size = New System.Drawing.Size(200, 22)
        Me.Period.TabIndex = 1
        '
        'DGV_Transactions
        '
        Me.DGV_Transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Transactions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BatchID, Me.TransactionID, Me.LinkID, Me.Capture_Date, Me.Transaction_Date, Me.PPeriod, Me.Fin_Year, Me.GDC, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.AccNumber, Me.LinkAcc, Me.Amount, Me.TaxType, Me.Tax_Amount, Me.UserID, Me.SupplierID, Me.EmployeeID, Me.Description2, Me.Description3, Me.Description4, Me.Description5, Me.Posted, Me.Accounting, Me.Void, Me.Transaction_Type, Me.DR_CR, Me.Contra_Account, Me.Description_Code, Me.DocType, Me.SupplierName})
        Me.DGV_Transactions.Location = New System.Drawing.Point(20, 597)
        Me.DGV_Transactions.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_Transactions.Name = "DGV_Transactions"
        Me.DGV_Transactions.Size = New System.Drawing.Size(872, 32)
        Me.DGV_Transactions.TabIndex = 97
        Me.DGV_Transactions.Visible = False
        '
        'BatchID
        '
        Me.BatchID.HeaderText = "BatchID"
        Me.BatchID.Name = "BatchID"
        Me.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TransactionID
        '
        Me.TransactionID.HeaderText = "TransactionID"
        Me.TransactionID.Name = "TransactionID"
        '
        'LinkID
        '
        Me.LinkID.HeaderText = "LinkID"
        Me.LinkID.Name = "LinkID"
        '
        'Capture_Date
        '
        Me.Capture_Date.HeaderText = "Capture_Date"
        Me.Capture_Date.Name = "Capture_Date"
        '
        'Transaction_Date
        '
        Me.Transaction_Date.HeaderText = "Transaction_Date"
        Me.Transaction_Date.Name = "Transaction_Date"
        '
        'PPeriod
        '
        Me.PPeriod.HeaderText = "PPeriod"
        Me.PPeriod.Name = "PPeriod"
        '
        'Fin_Year
        '
        Me.Fin_Year.HeaderText = "Fin_Year"
        Me.Fin_Year.Name = "Fin_Year"
        '
        'GDC
        '
        Me.GDC.HeaderText = "GDC"
        Me.GDC.Name = "GDC"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Reference"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'AccNumber
        '
        Me.AccNumber.HeaderText = "AccNumber"
        Me.AccNumber.Name = "AccNumber"
        '
        'LinkAcc
        '
        Me.LinkAcc.HeaderText = "LinkAcc"
        Me.LinkAcc.Name = "LinkAcc"
        '
        'Amount
        '
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        '
        'TaxType
        '
        Me.TaxType.HeaderText = "TaxType"
        Me.TaxType.Name = "TaxType"
        '
        'Tax_Amount
        '
        Me.Tax_Amount.HeaderText = "Tax_Amount"
        Me.Tax_Amount.Name = "Tax_Amount"
        '
        'UserID
        '
        Me.UserID.HeaderText = "UserID"
        Me.UserID.Name = "UserID"
        '
        'SupplierID
        '
        Me.SupplierID.HeaderText = "SupplierID"
        Me.SupplierID.Name = "SupplierID"
        '
        'EmployeeID
        '
        Me.EmployeeID.HeaderText = "EmployeeID"
        Me.EmployeeID.Name = "EmployeeID"
        '
        'Description2
        '
        Me.Description2.HeaderText = "Description2"
        Me.Description2.Name = "Description2"
        '
        'Description3
        '
        Me.Description3.HeaderText = "Description3"
        Me.Description3.Name = "Description3"
        '
        'Description4
        '
        Me.Description4.HeaderText = "Description4"
        Me.Description4.Name = "Description4"
        '
        'Description5
        '
        Me.Description5.HeaderText = "Description5"
        Me.Description5.Name = "Description5"
        '
        'Posted
        '
        Me.Posted.HeaderText = "Posted"
        Me.Posted.Name = "Posted"
        '
        'Accounting
        '
        Me.Accounting.HeaderText = "Accounting"
        Me.Accounting.Name = "Accounting"
        '
        'Void
        '
        Me.Void.HeaderText = "Void"
        Me.Void.Name = "Void"
        '
        'Transaction_Type
        '
        Me.Transaction_Type.HeaderText = "Transaction_Type"
        Me.Transaction_Type.Name = "Transaction_Type"
        '
        'DR_CR
        '
        Me.DR_CR.HeaderText = "DR_CR"
        Me.DR_CR.Name = "DR_CR"
        '
        'Contra_Account
        '
        Me.Contra_Account.HeaderText = "Contra_Account"
        Me.Contra_Account.Name = "Contra_Account"
        '
        'Description_Code
        '
        Me.Description_Code.HeaderText = "Description_Code"
        Me.Description_Code.Name = "Description_Code"
        '
        'DocType
        '
        Me.DocType.HeaderText = "DocType"
        Me.DocType.Name = "DocType"
        '
        'SupplierName
        '
        Me.SupplierName.HeaderText = "SupplierName"
        Me.SupplierName.Name = "SupplierName"
        '
        'cmbAccCat3
        '
        Me.cmbAccCat3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbAccCat3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbAccCat3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAccCat3.FormattingEnabled = True
        Me.cmbAccCat3.Items.AddRange(New Object() {"ACCOUNTING & AUDITING"})
        Me.cmbAccCat3.Location = New System.Drawing.Point(11, 151)
        Me.cmbAccCat3.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbAccCat3.Name = "cmbAccCat3"
        Me.cmbAccCat3.Size = New System.Drawing.Size(259, 24)
        Me.cmbAccCat3.TabIndex = 2
        '
        'cmbAccCat2
        '
        Me.cmbAccCat2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbAccCat2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbAccCat2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAccCat2.FormattingEnabled = True
        Me.cmbAccCat2.Items.AddRange(New Object() {"ACCOUNTING & AUDITING", "BANK CHARGES", "CREDIT CARD CHARGES", "INSURANCE", "RENT + OPPS COSTS +RATES", "RENTALS AND LEASES", "FRANCHISE ROYALTY", "FRANCHISE MARKETING"})
        Me.cmbAccCat2.Location = New System.Drawing.Point(11, 98)
        Me.cmbAccCat2.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbAccCat2.Name = "cmbAccCat2"
        Me.cmbAccCat2.Size = New System.Drawing.Size(259, 24)
        Me.cmbAccCat2.TabIndex = 1
        '
        'cmbAccCat1
        '
        Me.cmbAccCat1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbAccCat1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbAccCat1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAccCat1.FormattingEnabled = True
        Me.cmbAccCat1.Items.AddRange(New Object() {"PURCHASES", "STAFF COSTS & WELFARE", "OPERATING COSTS", "UTILITIES", "NON CONTROLLABLE COSTS"})
        Me.cmbAccCat1.Location = New System.Drawing.Point(11, 47)
        Me.cmbAccCat1.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbAccCat1.Name = "cmbAccCat1"
        Me.cmbAccCat1.Size = New System.Drawing.Size(259, 24)
        Me.cmbAccCat1.TabIndex = 0
        '
        'lblCat1
        '
        Me.lblCat1.AutoSize = True
        Me.lblCat1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCat1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat1.Location = New System.Drawing.Point(16, 26)
        Me.lblCat1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCat1.Name = "lblCat1"
        Me.lblCat1.Size = New System.Drawing.Size(65, 15)
        Me.lblCat1.TabIndex = 101
        Me.lblCat1.Text = "Category 1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblSegment4Account)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cmbAccCat3)
        Me.GroupBox1.Controls.Add(Me.lblCat1)
        Me.GroupBox1.Controls.Add(Me.cmbAccCat1)
        Me.GroupBox1.Controls.Add(Me.cmbAccCat2)
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(612, 64)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(280, 220)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Account"
        '
        'lblSegment4Account
        '
        Me.lblSegment4Account.AutoSize = True
        Me.lblSegment4Account.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSegment4Account.ForeColor = System.Drawing.Color.Olive
        Me.lblSegment4Account.Location = New System.Drawing.Point(180, 16)
        Me.lblSegment4Account.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSegment4Account.Name = "lblSegment4Account"
        Me.lblSegment4Account.Size = New System.Drawing.Size(11, 15)
        Me.lblSegment4Account.TabIndex = 104
        Me.lblSegment4Account.Text = "-"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label14.Location = New System.Drawing.Point(16, 129)
        Me.Label14.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 15)
        Me.Label14.TabIndex = 103
        Me.Label14.Text = "Category 3"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label12.Location = New System.Drawing.Point(16, 78)
        Me.Label12.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 15)
        Me.Label12.TabIndex = 102
        Me.Label12.Text = "Category 2"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblSegment4Contra)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.cmbContraCat3)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.cmbContraCat1)
        Me.GroupBox3.Controls.Add(Me.cmbContraCat2)
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Location = New System.Drawing.Point(905, 64)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(283, 184)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Contra Account"
        '
        'lblSegment4Contra
        '
        Me.lblSegment4Contra.AutoSize = True
        Me.lblSegment4Contra.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSegment4Contra.ForeColor = System.Drawing.Color.Olive
        Me.lblSegment4Contra.Location = New System.Drawing.Point(189, 16)
        Me.lblSegment4Contra.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSegment4Contra.Name = "lblSegment4Contra"
        Me.lblSegment4Contra.Size = New System.Drawing.Size(11, 15)
        Me.lblSegment4Contra.TabIndex = 105
        Me.lblSegment4Contra.Text = "-"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label15.Location = New System.Drawing.Point(16, 130)
        Me.Label15.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(65, 15)
        Me.Label15.TabIndex = 103
        Me.Label15.Text = "Category 3"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label16.Location = New System.Drawing.Point(16, 79)
        Me.Label16.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(65, 15)
        Me.Label16.TabIndex = 102
        Me.Label16.Text = "Category 2"
        '
        'cmbContraCat3
        '
        Me.cmbContraCat3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbContraCat3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbContraCat3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbContraCat3.FormattingEnabled = True
        Me.cmbContraCat3.Items.AddRange(New Object() {"ACCOUNTING & AUDITING"})
        Me.cmbContraCat3.Location = New System.Drawing.Point(11, 153)
        Me.cmbContraCat3.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbContraCat3.Name = "cmbContraCat3"
        Me.cmbContraCat3.Size = New System.Drawing.Size(259, 24)
        Me.cmbContraCat3.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label17.Location = New System.Drawing.Point(16, 27)
        Me.Label17.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(65, 15)
        Me.Label17.TabIndex = 101
        Me.Label17.Text = "Category 1"
        '
        'cmbContraCat1
        '
        Me.cmbContraCat1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbContraCat1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbContraCat1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbContraCat1.FormattingEnabled = True
        Me.cmbContraCat1.Items.AddRange(New Object() {"PURCHASES", "STAFF COSTS & WELFARE", "OPERATING COSTS", "UTILITIES", "NON CONTROLLABLE COSTS"})
        Me.cmbContraCat1.Location = New System.Drawing.Point(11, 48)
        Me.cmbContraCat1.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbContraCat1.Name = "cmbContraCat1"
        Me.cmbContraCat1.Size = New System.Drawing.Size(259, 24)
        Me.cmbContraCat1.TabIndex = 0
        '
        'cmbContraCat2
        '
        Me.cmbContraCat2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbContraCat2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbContraCat2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbContraCat2.FormattingEnabled = True
        Me.cmbContraCat2.Items.AddRange(New Object() {"ACCOUNTING & AUDITING", "BANK CHARGES", "CREDIT CARD CHARGES", "INSURANCE", "RENT + OPPS COSTS +RATES", "RENTALS AND LEASES", "FRANCHISE ROYALTY", "FRANCHISE MARKETING"})
        Me.cmbContraCat2.Location = New System.Drawing.Point(11, 100)
        Me.cmbContraCat2.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbContraCat2.Name = "cmbContraCat2"
        Me.cmbContraCat2.Size = New System.Drawing.Size(259, 24)
        Me.cmbContraCat2.TabIndex = 1
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblSelectedOption)
        Me.GroupBox4.Controls.Add(Me.cmbJournalTypeValues)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.cmbJournalType)
        Me.GroupBox4.Controls.Add(Me.Date_DateTimePicker)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.Period)
        Me.GroupBox4.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox4.Location = New System.Drawing.Point(16, 64)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Size = New System.Drawing.Size(224, 220)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Journal"
        '
        'lblSelectedOption
        '
        Me.lblSelectedOption.AutoSize = True
        Me.lblSelectedOption.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectedOption.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblSelectedOption.Location = New System.Drawing.Point(11, 68)
        Me.lblSelectedOption.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSelectedOption.Name = "lblSelectedOption"
        Me.lblSelectedOption.Size = New System.Drawing.Size(33, 15)
        Me.lblSelectedOption.TabIndex = 72
        Me.lblSelectedOption.Text = "Type"
        '
        'cmbJournalTypeValues
        '
        Me.cmbJournalTypeValues.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbJournalTypeValues.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbJournalTypeValues.FormattingEnabled = True
        Me.cmbJournalTypeValues.Items.AddRange(New Object() {"General Journal", "Supplier Journal", "Employee Journal"})
        Me.cmbJournalTypeValues.Location = New System.Drawing.Point(12, 90)
        Me.cmbJournalTypeValues.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbJournalTypeValues.Name = "cmbJournalTypeValues"
        Me.cmbJournalTypeValues.Size = New System.Drawing.Size(200, 24)
        Me.cmbJournalTypeValues.TabIndex = 71
        '
        'dgvPeriods
        '
        Me.dgvPeriods.BackgroundColor = System.Drawing.SystemColors.Highlight
        Me.dgvPeriods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPeriods.Location = New System.Drawing.Point(711, 4)
        Me.dgvPeriods.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvPeriods.Name = "dgvPeriods"
        Me.dgvPeriods.RowTemplate.Height = 24
        Me.dgvPeriods.Size = New System.Drawing.Size(71, 50)
        Me.dgvPeriods.TabIndex = 120
        Me.dgvPeriods.Visible = False
        '
        'dgvSuppliers
        '
        Me.dgvSuppliers.BackgroundColor = System.Drawing.Color.MidnightBlue
        Me.dgvSuppliers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSuppliers.Location = New System.Drawing.Point(645, 4)
        Me.dgvSuppliers.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvSuppliers.Name = "dgvSuppliers"
        Me.dgvSuppliers.RowTemplate.Height = 24
        Me.dgvSuppliers.Size = New System.Drawing.Size(71, 50)
        Me.dgvSuppliers.TabIndex = 119
        Me.dgvSuppliers.Visible = False
        '
        'dgvAccounts
        '
        Me.dgvAccounts.BackgroundColor = System.Drawing.Color.Teal
        Me.dgvAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAccounts.Location = New System.Drawing.Point(575, 4)
        Me.dgvAccounts.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvAccounts.Name = "dgvAccounts"
        Me.dgvAccounts.RowTemplate.Height = 24
        Me.dgvAccounts.Size = New System.Drawing.Size(71, 50)
        Me.dgvAccounts.TabIndex = 118
        Me.dgvAccounts.Visible = False
        '
        'btnClear
        '
        Me.btnClear.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnClear.Location = New System.Drawing.Point(452, 298)
        Me.btnClear.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(108, 28)
        Me.btnClear.TabIndex = 8
        Me.btnClear.Text = "C&lear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdbExclusive)
        Me.GroupBox2.Controls.Add(Me.rdbInclusive)
        Me.GroupBox2.Controls.Add(Me.Reference)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Description)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cmbTax)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.DC)
        Me.GroupBox2.Controls.Add(Me.txtInclusive)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtExclusive)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(252, 64)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(348, 220)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detail"
        '
        'rdbExclusive
        '
        Me.rdbExclusive.AutoSize = True
        Me.rdbExclusive.Location = New System.Drawing.Point(176, 188)
        Me.rdbExclusive.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbExclusive.Name = "rdbExclusive"
        Me.rdbExclusive.Size = New System.Drawing.Size(87, 21)
        Me.rdbExclusive.TabIndex = 7
        Me.rdbExclusive.Text = "Exclusive"
        Me.rdbExclusive.UseVisualStyleBackColor = True
        '
        'rdbInclusive
        '
        Me.rdbInclusive.AutoSize = True
        Me.rdbInclusive.Checked = True
        Me.rdbInclusive.Location = New System.Drawing.Point(69, 188)
        Me.rdbInclusive.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbInclusive.Name = "rdbInclusive"
        Me.rdbInclusive.Size = New System.Drawing.Size(83, 21)
        Me.rdbInclusive.TabIndex = 6
        Me.rdbInclusive.TabStop = True
        Me.rdbInclusive.Text = "Inclusive"
        Me.rdbInclusive.UseVisualStyleBackColor = True
        '
        'Reference
        '
        Me.Reference.Location = New System.Drawing.Point(20, 47)
        Me.Reference.Margin = New System.Windows.Forms.Padding(4)
        Me.Reference.Name = "Reference"
        Me.Reference.Size = New System.Drawing.Size(319, 22)
        Me.Reference.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label3.Location = New System.Drawing.Point(16, 27)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 15)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Reference"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label4.Location = New System.Drawing.Point(16, 80)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 15)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Description"
        '
        'Description
        '
        Me.Description.Location = New System.Drawing.Point(20, 100)
        Me.Description.Margin = New System.Windows.Forms.Padding(4)
        Me.Description.Name = "Description"
        Me.Description.Size = New System.Drawing.Size(319, 22)
        Me.Description.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label6.Location = New System.Drawing.Point(16, 135)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(27, 15)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "Tax"
        '
        'cmbTax
        '
        Me.cmbTax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbTax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbTax.FormattingEnabled = True
        Me.cmbTax.Items.AddRange(New Object() {"0", "1"})
        Me.cmbTax.Location = New System.Drawing.Point(20, 155)
        Me.cmbTax.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbTax.Name = "cmbTax"
        Me.cmbTax.Size = New System.Drawing.Size(60, 24)
        Me.cmbTax.TabIndex = 2
        Me.cmbTax.Text = "1"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label8.Location = New System.Drawing.Point(83, 135)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 15)
        Me.Label8.TabIndex = 42
        Me.Label8.Text = "Inclusive"
        '
        'DC
        '
        Me.DC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.DC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.DC.FormattingEnabled = True
        Me.DC.Items.AddRange(New Object() {"Dr", "Cr"})
        Me.DC.Location = New System.Drawing.Point(292, 156)
        Me.DC.Margin = New System.Windows.Forms.Padding(4)
        Me.DC.Name = "DC"
        Me.DC.Size = New System.Drawing.Size(47, 24)
        Me.DC.TabIndex = 5
        '
        'txtInclusive
        '
        Me.txtInclusive.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInclusive.Location = New System.Drawing.Point(86, 157)
        Me.txtInclusive.Margin = New System.Windows.Forms.Padding(4)
        Me.txtInclusive.Name = "txtInclusive"
        Me.txtInclusive.Size = New System.Drawing.Size(80, 21)
        Me.txtInclusive.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label11.Location = New System.Drawing.Point(288, 137)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(24, 15)
        Me.Label11.TabIndex = 76
        Me.Label11.Text = "DC"
        '
        'txtExclusive
        '
        Me.txtExclusive.Enabled = False
        Me.txtExclusive.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExclusive.Location = New System.Drawing.Point(184, 157)
        Me.txtExclusive.Margin = New System.Windows.Forms.Padding(4)
        Me.txtExclusive.Name = "txtExclusive"
        Me.txtExclusive.Size = New System.Drawing.Size(92, 21)
        Me.txtExclusive.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label10.Location = New System.Drawing.Point(181, 138)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 15)
        Me.Label10.TabIndex = 75
        Me.Label10.Text = "Exclusive"
        '
        'btn_Templates
        '
        Me.btn_Templates.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btn_Templates.Location = New System.Drawing.Point(1028, 299)
        Me.btn_Templates.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Templates.Name = "btn_Templates"
        Me.btn_Templates.Size = New System.Drawing.Size(160, 28)
        Me.btn_Templates.TabIndex = 121
        Me.btn_Templates.Text = "Journal Templates"
        Me.btn_Templates.UseVisualStyleBackColor = True
        '
        'Ckb_StatTran
        '
        Me.Ckb_StatTran.AutoSize = True
        Me.Ckb_StatTran.Location = New System.Drawing.Point(916, 258)
        Me.Ckb_StatTran.Name = "Ckb_StatTran"
        Me.Ckb_StatTran.Size = New System.Drawing.Size(169, 21)
        Me.Ckb_StatTran.TabIndex = 0
        Me.Ckb_StatTran.Text = "Statistical Transaction"
        Me.Ckb_StatTran.UseVisualStyleBackColor = True
        '
        'frmJournals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1210, 642)
        Me.Controls.Add(Me.Ckb_StatTran)
        Me.Controls.Add(Me.btn_Templates)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.dgvPeriods)
        Me.Controls.Add(Me.dgvSuppliers)
        Me.Controls.Add(Me.dgvAccounts)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DGV_Transactions)
        Me.Controls.Add(Me.Label_Version)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label_Processed)
        Me.Controls.Add(Me.Process_Batch_Button)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.Close_Button)
        Me.Controls.Add(Me.EditButton)
        Me.Controls.Add(Me.DeleteButton)
        Me.Controls.Add(Me.Add_Button)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmJournals"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Journals"
        Me.Text = "Journals"
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Date_DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents EditButton As System.Windows.Forms.Button
    Friend WithEvents DeleteButton As System.Windows.Forms.Button
    Friend WithEvents Add_Button As System.Windows.Forms.Button
    Friend WithEvents cmbJournalType As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Close_Button As System.Windows.Forms.Button
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Process_Batch_Button As System.Windows.Forms.Button
    Friend WithEvents Label_Processed As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label_Version As System.Windows.Forms.Label
    Friend WithEvents Period As System.Windows.Forms.TextBox
    Friend WithEvents DGV_Transactions As System.Windows.Forms.DataGridView
    Friend WithEvents BatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Capture_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fin_Year As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAcc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tax_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accounting As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Void As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DR_CR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contra_Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbAccCat3 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbAccCat2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbAccCat1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCat1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmbContraCat3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cmbContraCat1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbContraCat2 As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPeriods As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSuppliers As System.Windows.Forms.DataGridView
    Friend WithEvents dgvAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents lblSegment4Account As System.Windows.Forms.Label
    Friend WithEvents lblSegment4Contra As System.Windows.Forms.Label
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbExclusive As System.Windows.Forms.RadioButton
    Friend WithEvents rdbInclusive As System.Windows.Forms.RadioButton
    Friend WithEvents Reference As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Description As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbTax As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DC As System.Windows.Forms.ComboBox
    Friend WithEvents txtInclusive As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtExclusive As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblSelectedOption As System.Windows.Forms.Label
    Friend WithEvents cmbJournalTypeValues As System.Windows.Forms.ComboBox
    Friend WithEvents btn_Templates As System.Windows.Forms.Button
    Friend WithEvents Ckb_StatTran As System.Windows.Forms.CheckBox
End Class
