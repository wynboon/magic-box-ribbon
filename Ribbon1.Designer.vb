﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'This call is required by the Component Designer.
        InitializeComponent()

    End Sub

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Ribbon1))
        Dim RibbonDropDownItemImpl1 As Microsoft.Office.Tools.Ribbon.RibbonDropDownItem = Me.Factory.CreateRibbonDropDownItem
        Dim RibbonDropDownItemImpl2 As Microsoft.Office.Tools.Ribbon.RibbonDropDownItem = Me.Factory.CreateRibbonDropDownItem
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.GrpAbout = Me.Factory.CreateRibbonGroup
        Me.Gallery1 = Me.Factory.CreateRibbonGallery
        Me.Button12 = Me.Factory.CreateRibbonButton
        Me.btnMainLogIn = Me.Factory.CreateRibbonButton
        Me.btn_LogOut = Me.Factory.CreateRibbonButton
        Me.Group_Home = Me.Factory.CreateRibbonGroup
        Me.btnOnOff = Me.Factory.CreateRibbonButton
        Me.DD_Group = Me.Factory.CreateRibbonDropDown
        Me.DD_Companies = Me.Factory.CreateRibbonDropDown
        Me.ButtonGroup1 = Me.Factory.CreateRibbonButtonGroup
        Me.btn_ChangeCo = Me.Factory.CreateRibbonButton
        Me.Btn_Cancel = Me.Factory.CreateRibbonButton
        Me.GrpLogin = Me.Factory.CreateRibbonGroup
        Me.Box5 = Me.Factory.CreateRibbonBox
        Me.Label2 = Me.Factory.CreateRibbonLabel
        Me.Box4 = Me.Factory.CreateRibbonBox
        Me.Label3 = Me.Factory.CreateRibbonLabel
        Me.Box1 = Me.Factory.CreateRibbonBox
        Me.btnLogin = Me.Factory.CreateRibbonButton
        Me.Box3 = Me.Factory.CreateRibbonBox
        Me.txtUserName = Me.Factory.CreateRibbonEditBox
        Me.Box2 = Me.Factory.CreateRibbonBox
        Me.txtPassword = Me.Factory.CreateRibbonEditBox
        Me.Box6 = Me.Factory.CreateRibbonBox
        Me.lblGroup = Me.Factory.CreateRibbonLabel
        Me.Label1 = Me.Factory.CreateRibbonLabel
        Me.lblUsername = Me.Factory.CreateRibbonLabel
        Me.grpDailyProcessing = Me.Factory.CreateRibbonGroup
        Me.Button_DayStart = Me.Factory.CreateRibbonButton
        Me.Button_ShiftHandover = Me.Factory.CreateRibbonButton
        Me.Button_Wages = Me.Factory.CreateRibbonButton
        Me.Button_WaiterCashup = Me.Factory.CreateRibbonButton
        Me.Button_RetailCashup = Me.Factory.CreateRibbonButton
        Me.Button_DayEnd = Me.Factory.CreateRibbonButton
        Me.grpMaintanance = Me.Factory.CreateRibbonGroup
        Me.Button_Settings = Me.Factory.CreateRibbonButton
        Me.Button_AccountingSync = Me.Factory.CreateRibbonButton
        Me.Button_Company = Me.Factory.CreateRibbonButton
        Me.btnEmpMaster = Me.Factory.CreateRibbonButton
        Me.btnSupplierMaster = Me.Factory.CreateRibbonButton
        Me.LBL_CoName = Me.Factory.CreateRibbonButton
        Me.grpSuppliers = Me.Factory.CreateRibbonGroup
        Me.Button_CaptureInvoice = Me.Factory.CreateRibbonButton
        Me.Button_CreditNotes = Me.Factory.CreateRibbonButton
        Me.Button2 = Me.Factory.CreateRibbonButton
        Me.Button6 = Me.Factory.CreateRibbonButton
        Me.btnTransactions = Me.Factory.CreateRibbonButton
        Me.Button9 = Me.Factory.CreateRibbonButton
        Me.Button10 = Me.Factory.CreateRibbonButton
        Me.grpApprovals = Me.Factory.CreateRibbonGroup
        Me.btnCreditNotes = Me.Factory.CreateRibbonButton
        Me.btnEFTApprovals = Me.Factory.CreateRibbonButton
        Me.btnAllocations = Me.Factory.CreateRibbonButton
        Me.grpCustomers = Me.Factory.CreateRibbonGroup
        Me.Button3 = Me.Factory.CreateRibbonButton
        Me.Button5 = Me.Factory.CreateRibbonButton
        Me.Button4 = Me.Factory.CreateRibbonButton
        Me.grpGeneral = Me.Factory.CreateRibbonGroup
        Me.Button1 = Me.Factory.CreateRibbonButton
        Me.btnCashSpotCheck = Me.Factory.CreateRibbonButton
        Me.chkCheckEmail = Me.Factory.CreateRibbonCheckBox
        Me.grpIntelligence = Me.Factory.CreateRibbonGroup
        Me.Btn_Reports = Me.Factory.CreateRibbonButton
        Me.Gal_BI = Me.Factory.CreateRibbonGallery
        Me.GrpAccounting = Me.Factory.CreateRibbonGroup
        Me.btnBankStatement = Me.Factory.CreateRibbonButton
        Me.btnCashbook = Me.Factory.CreateRibbonButton
        Me.Btn_Migrate = Me.Factory.CreateRibbonButton
        Me.Button7 = Me.Factory.CreateRibbonButton
        Me.Btn_Verify = Me.Factory.CreateRibbonButton
        Me.grpExtaTools = Me.Factory.CreateRibbonGroup
        Me.btnOtherModules = Me.Factory.CreateRibbonButton
        Me.Button8 = Me.Factory.CreateRibbonButton
        Me.Btn_InvIntegration = Me.Factory.CreateRibbonButton
        Me.grpModes = Me.Factory.CreateRibbonGroup
        Me.DropDown1 = Me.Factory.CreateRibbonDropDown
        Me.btnCheckBalance = Me.Factory.CreateRibbonButton
        Me.Btn_Tasks = Me.Factory.CreateRibbonButton
        Me.lblTimer = Me.Factory.CreateRibbonLabel
        Me.GrpOrderX = Me.Factory.CreateRibbonGroup
        Me.Btn_OrderX = Me.Factory.CreateRibbonButton
        Me.Gal_OrderX = Me.Factory.CreateRibbonGallery
        Me.grpAdmin = Me.Factory.CreateRibbonGroup
        Me.btnAdmin = Me.Factory.CreateRibbonButton
        Me.btnMaintenance = Me.Factory.CreateRibbonButton
        Me.Button11 = Me.Factory.CreateRibbonButton
        Me.test_report = Me.Factory.CreateRibbonButton
        Me.Tasks = Me.Factory.CreateRibbonButton
        Me.Btn_DBMaint = Me.Factory.CreateRibbonButton
        Me.btn_Update = Me.Factory.CreateRibbonButton
        Me.Button13 = Me.Factory.CreateRibbonButton
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.DB_BackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.Tmr_ServerCheck = New System.Windows.Forms.Timer(Me.components)
        Me.NI_ServerStatus = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.Tab1.SuspendLayout()
        Me.GrpAbout.SuspendLayout()
        Me.Group_Home.SuspendLayout()
        Me.ButtonGroup1.SuspendLayout()
        Me.GrpLogin.SuspendLayout()
        Me.Box5.SuspendLayout()
        Me.Box4.SuspendLayout()
        Me.Box1.SuspendLayout()
        Me.Box3.SuspendLayout()
        Me.Box2.SuspendLayout()
        Me.Box6.SuspendLayout()
        Me.grpDailyProcessing.SuspendLayout()
        Me.grpMaintanance.SuspendLayout()
        Me.grpSuppliers.SuspendLayout()
        Me.grpApprovals.SuspendLayout()
        Me.grpCustomers.SuspendLayout()
        Me.grpGeneral.SuspendLayout()
        Me.grpIntelligence.SuspendLayout()
        Me.GrpAccounting.SuspendLayout()
        Me.grpExtaTools.SuspendLayout()
        Me.grpModes.SuspendLayout()
        Me.GrpOrderX.SuspendLayout()
        Me.grpAdmin.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.Groups.Add(Me.GrpAbout)
        Me.Tab1.Groups.Add(Me.Group_Home)
        Me.Tab1.Groups.Add(Me.GrpLogin)
        Me.Tab1.Groups.Add(Me.grpDailyProcessing)
        Me.Tab1.Groups.Add(Me.grpMaintanance)
        Me.Tab1.Groups.Add(Me.grpSuppliers)
        Me.Tab1.Groups.Add(Me.grpApprovals)
        Me.Tab1.Groups.Add(Me.grpCustomers)
        Me.Tab1.Groups.Add(Me.grpGeneral)
        Me.Tab1.Groups.Add(Me.grpIntelligence)
        Me.Tab1.Groups.Add(Me.GrpAccounting)
        Me.Tab1.Groups.Add(Me.grpExtaTools)
        Me.Tab1.Groups.Add(Me.grpModes)
        Me.Tab1.Groups.Add(Me.GrpOrderX)
        Me.Tab1.Groups.Add(Me.grpAdmin)
        Me.Tab1.Label = "Magic Box"
        Me.Tab1.Name = "Tab1"
        '
        'GrpAbout
        '
        Me.GrpAbout.Items.Add(Me.Gallery1)
        Me.GrpAbout.Items.Add(Me.Button12)
        Me.GrpAbout.Items.Add(Me.btnMainLogIn)
        Me.GrpAbout.Items.Add(Me.btn_LogOut)
        Me.GrpAbout.Label = " "
        Me.GrpAbout.Name = "GrpAbout"
        '
        'Gallery1
        '
        Me.Gallery1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.Gallery1.Image = Global.MB.My.Resources.Resources.MB_64X64_Icon
        Me.Gallery1.Label = "Options"
        Me.Gallery1.Name = "Gallery1"
        Me.Gallery1.ScreenTip = "User access"
        Me.Gallery1.ShowImage = True
        Me.Gallery1.ShowItemSelection = True
        '
        'Button12
        '
        Me.Button12.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.Button12.Image = Global.MB.My.Resources.Resources.MB_64X64_Icon
        Me.Button12.Label = "Magic Box"
        Me.Button12.Name = "Button12"
        Me.Button12.ShowImage = True
        Me.Button12.Visible = False
        '
        'btnMainLogIn
        '
        Me.btnMainLogIn.Image = CType(resources.GetObject("btnMainLogIn.Image"), System.Drawing.Image)
        Me.btnMainLogIn.Label = "Log In"
        Me.btnMainLogIn.Name = "btnMainLogIn"
        Me.btnMainLogIn.ShowImage = True
        Me.btnMainLogIn.Visible = False
        '
        'btn_LogOut
        '
        Me.btn_LogOut.Image = CType(resources.GetObject("btn_LogOut.Image"), System.Drawing.Image)
        Me.btn_LogOut.Label = "Log Out"
        Me.btn_LogOut.Name = "btn_LogOut"
        Me.btn_LogOut.ShowImage = True
        Me.btn_LogOut.Visible = False
        '
        'Group_Home
        '
        Me.Group_Home.Items.Add(Me.btnOnOff)
        Me.Group_Home.Items.Add(Me.DD_Group)
        Me.Group_Home.Items.Add(Me.DD_Companies)
        Me.Group_Home.Items.Add(Me.ButtonGroup1)
        Me.Group_Home.Name = "Group_Home"
        Me.Group_Home.Visible = False
        '
        'btnOnOff
        '
        Me.btnOnOff.Image = CType(resources.GetObject("btnOnOff.Image"), System.Drawing.Image)
        Me.btnOnOff.Label = "Active"
        Me.btnOnOff.Name = "btnOnOff"
        Me.btnOnOff.ShowImage = True
        Me.btnOnOff.Visible = False
        '
        'DD_Group
        '
        Me.DD_Group.Label = "Group"
        Me.DD_Group.Name = "DD_Group"
        Me.DD_Group.Visible = False
        '
        'DD_Companies
        '
        Me.DD_Companies.Label = "Co"
        Me.DD_Companies.Name = "DD_Companies"
        '
        'ButtonGroup1
        '
        Me.ButtonGroup1.Items.Add(Me.btn_ChangeCo)
        Me.ButtonGroup1.Items.Add(Me.Btn_Cancel)
        Me.ButtonGroup1.Name = "ButtonGroup1"
        '
        'btn_ChangeCo
        '
        Me.btn_ChangeCo.Label = "Set Company"
        Me.btn_ChangeCo.Name = "btn_ChangeCo"
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.Label = "Cancel"
        Me.Btn_Cancel.Name = "Btn_Cancel"
        '
        'GrpLogin
        '
        Me.GrpLogin.Items.Add(Me.Box5)
        Me.GrpLogin.Items.Add(Me.Box4)
        Me.GrpLogin.Items.Add(Me.Box1)
        Me.GrpLogin.Items.Add(Me.Box3)
        Me.GrpLogin.Items.Add(Me.Box2)
        Me.GrpLogin.Items.Add(Me.Box6)
        Me.GrpLogin.Label = "Log in"
        Me.GrpLogin.Name = "GrpLogin"
        Me.GrpLogin.Visible = False
        '
        'Box5
        '
        Me.Box5.Items.Add(Me.Label2)
        Me.Box5.Name = "Box5"
        '
        'Label2
        '
        Me.Label2.Label = "Username"
        Me.Label2.Name = "Label2"
        '
        'Box4
        '
        Me.Box4.Items.Add(Me.Label3)
        Me.Box4.Name = "Box4"
        '
        'Label3
        '
        Me.Label3.Label = "Password"
        Me.Label3.Name = "Label3"
        '
        'Box1
        '
        Me.Box1.Items.Add(Me.btnLogin)
        Me.Box1.Name = "Box1"
        '
        'btnLogin
        '
        Me.btnLogin.Image = CType(resources.GetObject("btnLogin.Image"), System.Drawing.Image)
        Me.btnLogin.Label = "Log in"
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.ShowImage = True
        '
        'Box3
        '
        Me.Box3.Items.Add(Me.txtUserName)
        Me.Box3.Name = "Box3"
        '
        'txtUserName
        '
        Me.txtUserName.Label = ":"
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.SizeString = "xxxxxxxxxxxx"
        Me.txtUserName.Text = Nothing
        '
        'Box2
        '
        Me.Box2.Items.Add(Me.txtPassword)
        Me.Box2.Name = "Box2"
        '
        'txtPassword
        '
        Me.txtPassword.Label = ":"
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.SizeString = "xxxxxxxxxxxx"
        Me.txtPassword.Text = Nothing
        '
        'Box6
        '
        Me.Box6.Items.Add(Me.lblGroup)
        Me.Box6.Items.Add(Me.Label1)
        Me.Box6.Items.Add(Me.lblUsername)
        Me.Box6.Name = "Box6"
        '
        'lblGroup
        '
        Me.lblGroup.Label = "-"
        Me.lblGroup.Name = "lblGroup"
        '
        'Label1
        '
        Me.Label1.Label = ":"
        Me.Label1.Name = "Label1"
        '
        'lblUsername
        '
        Me.lblUsername.Label = "-"
        Me.lblUsername.Name = "lblUsername"
        '
        'grpDailyProcessing
        '
        Me.grpDailyProcessing.Items.Add(Me.Button_DayStart)
        Me.grpDailyProcessing.Items.Add(Me.Button_ShiftHandover)
        Me.grpDailyProcessing.Items.Add(Me.Button_Wages)
        Me.grpDailyProcessing.Items.Add(Me.Button_WaiterCashup)
        Me.grpDailyProcessing.Items.Add(Me.Button_RetailCashup)
        Me.grpDailyProcessing.Items.Add(Me.Button_DayEnd)
        Me.grpDailyProcessing.Label = "Daily Processing"
        Me.grpDailyProcessing.Name = "grpDailyProcessing"
        Me.grpDailyProcessing.Visible = False
        '
        'Button_DayStart
        '
        Me.Button_DayStart.Image = CType(resources.GetObject("Button_DayStart.Image"), System.Drawing.Image)
        Me.Button_DayStart.Label = "Day Start"
        Me.Button_DayStart.Name = "Button_DayStart"
        Me.Button_DayStart.ShowImage = True
        '
        'Button_ShiftHandover
        '
        Me.Button_ShiftHandover.Image = CType(resources.GetObject("Button_ShiftHandover.Image"), System.Drawing.Image)
        Me.Button_ShiftHandover.Label = "Shift Handover"
        Me.Button_ShiftHandover.Name = "Button_ShiftHandover"
        Me.Button_ShiftHandover.ShowImage = True
        '
        'Button_Wages
        '
        Me.Button_Wages.Image = CType(resources.GetObject("Button_Wages.Image"), System.Drawing.Image)
        Me.Button_Wages.Label = "Wages"
        Me.Button_Wages.Name = "Button_Wages"
        Me.Button_Wages.ShowImage = True
        '
        'Button_WaiterCashup
        '
        Me.Button_WaiterCashup.Image = CType(resources.GetObject("Button_WaiterCashup.Image"), System.Drawing.Image)
        Me.Button_WaiterCashup.Label = "Waiter Cashup"
        Me.Button_WaiterCashup.Name = "Button_WaiterCashup"
        Me.Button_WaiterCashup.ShowImage = True
        '
        'Button_RetailCashup
        '
        Me.Button_RetailCashup.Image = CType(resources.GetObject("Button_RetailCashup.Image"), System.Drawing.Image)
        Me.Button_RetailCashup.Label = "Retail Cashup"
        Me.Button_RetailCashup.Name = "Button_RetailCashup"
        Me.Button_RetailCashup.ShowImage = True
        '
        'Button_DayEnd
        '
        Me.Button_DayEnd.Image = CType(resources.GetObject("Button_DayEnd.Image"), System.Drawing.Image)
        Me.Button_DayEnd.Label = "Day End"
        Me.Button_DayEnd.Name = "Button_DayEnd"
        Me.Button_DayEnd.ShowImage = True
        '
        'grpMaintanance
        '
        Me.grpMaintanance.Items.Add(Me.Button_Settings)
        Me.grpMaintanance.Items.Add(Me.Button_AccountingSync)
        Me.grpMaintanance.Items.Add(Me.Button_Company)
        Me.grpMaintanance.Items.Add(Me.btnEmpMaster)
        Me.grpMaintanance.Items.Add(Me.btnSupplierMaster)
        Me.grpMaintanance.Items.Add(Me.LBL_CoName)
        Me.grpMaintanance.Label = "Maintenance"
        Me.grpMaintanance.Name = "grpMaintanance"
        '
        'Button_Settings
        '
        Me.Button_Settings.Image = CType(resources.GetObject("Button_Settings.Image"), System.Drawing.Image)
        Me.Button_Settings.Label = "Settings"
        Me.Button_Settings.Name = "Button_Settings"
        Me.Button_Settings.ShowImage = True
        '
        'Button_AccountingSync
        '
        Me.Button_AccountingSync.Image = CType(resources.GetObject("Button_AccountingSync.Image"), System.Drawing.Image)
        Me.Button_AccountingSync.Label = "Accounts"
        Me.Button_AccountingSync.Name = "Button_AccountingSync"
        Me.Button_AccountingSync.ShowImage = True
        '
        'Button_Company
        '
        Me.Button_Company.Image = CType(resources.GetObject("Button_Company.Image"), System.Drawing.Image)
        Me.Button_Company.Label = "Company"
        Me.Button_Company.Name = "Button_Company"
        Me.Button_Company.ShowImage = True
        '
        'btnEmpMaster
        '
        Me.btnEmpMaster.Image = CType(resources.GetObject("btnEmpMaster.Image"), System.Drawing.Image)
        Me.btnEmpMaster.Label = "Employee Master"
        Me.btnEmpMaster.Name = "btnEmpMaster"
        Me.btnEmpMaster.ShowImage = True
        '
        'btnSupplierMaster
        '
        Me.btnSupplierMaster.Image = CType(resources.GetObject("btnSupplierMaster.Image"), System.Drawing.Image)
        Me.btnSupplierMaster.Label = "Supplier Master"
        Me.btnSupplierMaster.Name = "btnSupplierMaster"
        Me.btnSupplierMaster.ShowImage = True
        '
        'LBL_CoName
        '
        Me.LBL_CoName.Image = Global.MB.My.Resources.Resources.Home32X32
        Me.LBL_CoName.Label = "CoName"
        Me.LBL_CoName.Name = "LBL_CoName"
        Me.LBL_CoName.ShowImage = True
        '
        'grpSuppliers
        '
        Me.grpSuppliers.Items.Add(Me.Button_CaptureInvoice)
        Me.grpSuppliers.Items.Add(Me.Button_CreditNotes)
        Me.grpSuppliers.Items.Add(Me.Button2)
        Me.grpSuppliers.Items.Add(Me.Button6)
        Me.grpSuppliers.Items.Add(Me.btnTransactions)
        Me.grpSuppliers.Items.Add(Me.Button9)
        Me.grpSuppliers.Items.Add(Me.Button10)
        Me.grpSuppliers.Label = "Suppliers"
        Me.grpSuppliers.Name = "grpSuppliers"
        '
        'Button_CaptureInvoice
        '
        Me.Button_CaptureInvoice.Image = CType(resources.GetObject("Button_CaptureInvoice.Image"), System.Drawing.Image)
        Me.Button_CaptureInvoice.Label = "Supplier Invoice"
        Me.Button_CaptureInvoice.Name = "Button_CaptureInvoice"
        Me.Button_CaptureInvoice.ShowImage = True
        '
        'Button_CreditNotes
        '
        Me.Button_CreditNotes.Image = CType(resources.GetObject("Button_CreditNotes.Image"), System.Drawing.Image)
        Me.Button_CreditNotes.Label = "Credit Notes"
        Me.Button_CreditNotes.Name = "Button_CreditNotes"
        Me.Button_CreditNotes.ShowImage = True
        '
        'Button2
        '
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.Label = "Supplier Payments"
        Me.Button2.Name = "Button2"
        Me.Button2.ShowImage = True
        '
        'Button6
        '
        Me.Button6.Image = CType(resources.GetObject("Button6.Image"), System.Drawing.Image)
        Me.Button6.Label = "Supplier Debits"
        Me.Button6.Name = "Button6"
        Me.Button6.ShowImage = True
        '
        'btnTransactions
        '
        Me.btnTransactions.Image = CType(resources.GetObject("btnTransactions.Image"), System.Drawing.Image)
        Me.btnTransactions.Label = "Transactions"
        Me.btnTransactions.Name = "btnTransactions"
        Me.btnTransactions.ShowImage = True
        Me.btnTransactions.Visible = False
        '
        'Button9
        '
        Me.Button9.Image = CType(resources.GetObject("Button9.Image"), System.Drawing.Image)
        Me.Button9.Label = "Pay Maint"
        Me.Button9.Name = "Button9"
        Me.Button9.ShowImage = True
        '
        'Button10
        '
        Me.Button10.Image = CType(resources.GetObject("Button10.Image"), System.Drawing.Image)
        Me.Button10.Label = "Enquire"
        Me.Button10.Name = "Button10"
        Me.Button10.ShowImage = True
        '
        'grpApprovals
        '
        Me.grpApprovals.Items.Add(Me.btnCreditNotes)
        Me.grpApprovals.Items.Add(Me.btnEFTApprovals)
        Me.grpApprovals.Items.Add(Me.btnAllocations)
        Me.grpApprovals.Label = "Approvals"
        Me.grpApprovals.Name = "grpApprovals"
        '
        'btnCreditNotes
        '
        Me.btnCreditNotes.Image = CType(resources.GetObject("btnCreditNotes.Image"), System.Drawing.Image)
        Me.btnCreditNotes.Label = "Credit Notes"
        Me.btnCreditNotes.Name = "btnCreditNotes"
        Me.btnCreditNotes.ShowImage = True
        '
        'btnEFTApprovals
        '
        Me.btnEFTApprovals.Image = CType(resources.GetObject("btnEFTApprovals.Image"), System.Drawing.Image)
        Me.btnEFTApprovals.Label = "EFT Approvals"
        Me.btnEFTApprovals.Name = "btnEFTApprovals"
        Me.btnEFTApprovals.ShowImage = True
        '
        'btnAllocations
        '
        Me.btnAllocations.Image = CType(resources.GetObject("btnAllocations.Image"), System.Drawing.Image)
        Me.btnAllocations.Label = "Allocations"
        Me.btnAllocations.Name = "btnAllocations"
        Me.btnAllocations.ShowImage = True
        '
        'grpCustomers
        '
        Me.grpCustomers.Items.Add(Me.Button3)
        Me.grpCustomers.Items.Add(Me.Button5)
        Me.grpCustomers.Items.Add(Me.Button4)
        Me.grpCustomers.Label = "Customers"
        Me.grpCustomers.Name = "grpCustomers"
        Me.grpCustomers.Visible = False
        '
        'Button3
        '
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.Label = "Invoice Customer"
        Me.Button3.Name = "Button3"
        Me.Button3.ShowImage = True
        '
        'Button5
        '
        Me.Button5.Image = CType(resources.GetObject("Button5.Image"), System.Drawing.Image)
        Me.Button5.Label = "Customer Payments"
        Me.Button5.Name = "Button5"
        Me.Button5.ShowImage = True
        '
        'Button4
        '
        Me.Button4.Label = "Products"
        Me.Button4.Name = "Button4"
        Me.Button4.ShowImage = True
        '
        'grpGeneral
        '
        Me.grpGeneral.Items.Add(Me.Button1)
        Me.grpGeneral.Items.Add(Me.btnCashSpotCheck)
        Me.grpGeneral.Items.Add(Me.chkCheckEmail)
        Me.grpGeneral.Label = "General"
        Me.grpGeneral.Name = "grpGeneral"
        Me.grpGeneral.Visible = False
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Label = "Import"
        Me.Button1.Name = "Button1"
        Me.Button1.ShowImage = True
        Me.Button1.Visible = False
        '
        'btnCashSpotCheck
        '
        Me.btnCashSpotCheck.Image = CType(resources.GetObject("btnCashSpotCheck.Image"), System.Drawing.Image)
        Me.btnCashSpotCheck.Label = "Cash Spot Check"
        Me.btnCashSpotCheck.Name = "btnCashSpotCheck"
        Me.btnCashSpotCheck.ShowImage = True
        Me.btnCashSpotCheck.Visible = False
        '
        'chkCheckEmail
        '
        Me.chkCheckEmail.Label = "Check Emails"
        Me.chkCheckEmail.Name = "chkCheckEmail"
        Me.chkCheckEmail.Visible = False
        '
        'grpIntelligence
        '
        Me.grpIntelligence.Items.Add(Me.Btn_Reports)
        Me.grpIntelligence.Items.Add(Me.Gal_BI)
        Me.grpIntelligence.Label = "Intelligence"
        Me.grpIntelligence.Name = "grpIntelligence"
        '
        'Btn_Reports
        '
        Me.Btn_Reports.Label = "Display"
        Me.Btn_Reports.Name = "Btn_Reports"
        '
        'Gal_BI
        '
        Me.Gal_BI.Image = Global.MB.My.Resources.Resources.Reports
        Me.Gal_BI.Label = "More"
        Me.Gal_BI.Name = "Gal_BI"
        Me.Gal_BI.ScreenTip = "User access"
        Me.Gal_BI.ShowImage = True
        '
        'GrpAccounting
        '
        Me.GrpAccounting.Items.Add(Me.btnBankStatement)
        Me.GrpAccounting.Items.Add(Me.btnCashbook)
        Me.GrpAccounting.Items.Add(Me.Btn_Migrate)
        Me.GrpAccounting.Items.Add(Me.Button7)
        Me.GrpAccounting.Items.Add(Me.Btn_Verify)
        Me.GrpAccounting.Label = "Accounting"
        Me.GrpAccounting.Name = "GrpAccounting"
        '
        'btnBankStatement
        '
        Me.btnBankStatement.Image = CType(resources.GetObject("btnBankStatement.Image"), System.Drawing.Image)
        Me.btnBankStatement.Label = "Bank Import"
        Me.btnBankStatement.Name = "btnBankStatement"
        Me.btnBankStatement.ShowImage = True
        '
        'btnCashbook
        '
        Me.btnCashbook.Image = CType(resources.GetObject("btnCashbook.Image"), System.Drawing.Image)
        Me.btnCashbook.Label = "Cash book"
        Me.btnCashbook.Name = "btnCashbook"
        Me.btnCashbook.ShowImage = True
        '
        'Btn_Migrate
        '
        Me.Btn_Migrate.Image = CType(resources.GetObject("Btn_Migrate.Image"), System.Drawing.Image)
        Me.Btn_Migrate.Label = "Migrate Transactions"
        Me.Btn_Migrate.Name = "Btn_Migrate"
        Me.Btn_Migrate.ShowImage = True
        '
        'Button7
        '
        Me.Button7.Image = CType(resources.GetObject("Button7.Image"), System.Drawing.Image)
        Me.Button7.Label = "Journal"
        Me.Button7.Name = "Button7"
        Me.Button7.ShowImage = True
        '
        'Btn_Verify
        '
        Me.Btn_Verify.Image = CType(resources.GetObject("Btn_Verify.Image"), System.Drawing.Image)
        Me.Btn_Verify.Label = "Verify"
        Me.Btn_Verify.Name = "Btn_Verify"
        Me.Btn_Verify.ShowImage = True
        '
        'grpExtaTools
        '
        Me.grpExtaTools.Items.Add(Me.btnOtherModules)
        Me.grpExtaTools.Items.Add(Me.Button8)
        Me.grpExtaTools.Items.Add(Me.Btn_InvIntegration)
        Me.grpExtaTools.Label = "Extra tools"
        Me.grpExtaTools.Name = "grpExtaTools"
        '
        'btnOtherModules
        '
        Me.btnOtherModules.Image = CType(resources.GetObject("btnOtherModules.Image"), System.Drawing.Image)
        Me.btnOtherModules.Label = "Other Modules"
        Me.btnOtherModules.Name = "btnOtherModules"
        Me.btnOtherModules.ShowImage = True
        '
        'Button8
        '
        Me.Button8.Image = CType(resources.GetObject("Button8.Image"), System.Drawing.Image)
        Me.Button8.Label = "Customer Care"
        Me.Button8.Name = "Button8"
        Me.Button8.ShowImage = True
        Me.Button8.Visible = False
        '
        'Btn_InvIntegration
        '
        Me.Btn_InvIntegration.Image = Global.MB.My.Resources.Resources.Bank_Transfer
        Me.Btn_InvIntegration.Label = "Inv Integration"
        Me.Btn_InvIntegration.Name = "Btn_InvIntegration"
        Me.Btn_InvIntegration.ShowImage = True
        '
        'grpModes
        '
        Me.grpModes.Items.Add(Me.DropDown1)
        Me.grpModes.Items.Add(Me.btnCheckBalance)
        Me.grpModes.Items.Add(Me.Btn_Tasks)
        Me.grpModes.Items.Add(Me.lblTimer)
        Me.grpModes.Label = "Modes"
        Me.grpModes.Name = "grpModes"
        '
        'DropDown1
        '
        RibbonDropDownItemImpl1.Label = "Fast (data loaded once)"
        RibbonDropDownItemImpl2.Label = "Medium (direct to database)"
        Me.DropDown1.Items.Add(RibbonDropDownItemImpl1)
        Me.DropDown1.Items.Add(RibbonDropDownItemImpl2)
        Me.DropDown1.Label = "Cloud"
        Me.DropDown1.Name = "DropDown1"
        '
        'btnCheckBalance
        '
        Me.btnCheckBalance.Label = "System Balance"
        Me.btnCheckBalance.Name = "btnCheckBalance"
        '
        'Btn_Tasks
        '
        Me.Btn_Tasks.Label = "Tasks"
        Me.Btn_Tasks.Name = "Btn_Tasks"
        '
        'lblTimer
        '
        Me.lblTimer.Label = "-"
        Me.lblTimer.Name = "lblTimer"
        '
        'GrpOrderX
        '
        Me.GrpOrderX.Items.Add(Me.Btn_OrderX)
        Me.GrpOrderX.Items.Add(Me.Gal_OrderX)
        Me.GrpOrderX.Label = "OrderX"
        Me.GrpOrderX.Name = "GrpOrderX"
        Me.GrpOrderX.Visible = False
        '
        'Btn_OrderX
        '
        Me.Btn_OrderX.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.Btn_OrderX.Image = Global.MB.My.Resources.Resources.Administrator
        Me.Btn_OrderX.Label = "User Panel"
        Me.Btn_OrderX.Name = "Btn_OrderX"
        Me.Btn_OrderX.ShowImage = True
        '
        'Gal_OrderX
        '
        Me.Gal_OrderX.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.Gal_OrderX.Image = Global.MB.My.Resources.Resources.Statement
        Me.Gal_OrderX.Label = "Quick Order"
        Me.Gal_OrderX.Name = "Gal_OrderX"
        Me.Gal_OrderX.ShowImage = True
        '
        'grpAdmin
        '
        Me.grpAdmin.Items.Add(Me.btnAdmin)
        Me.grpAdmin.Items.Add(Me.btnMaintenance)
        Me.grpAdmin.Items.Add(Me.Button11)
        Me.grpAdmin.Items.Add(Me.test_report)
        Me.grpAdmin.Items.Add(Me.Tasks)
        Me.grpAdmin.Items.Add(Me.Btn_DBMaint)
        Me.grpAdmin.Items.Add(Me.btn_Update)
        Me.grpAdmin.Items.Add(Me.Button13)
        Me.grpAdmin.Label = "Admin"
        Me.grpAdmin.Name = "grpAdmin"
        '
        'btnAdmin
        '
        Me.btnAdmin.Image = CType(resources.GetObject("btnAdmin.Image"), System.Drawing.Image)
        Me.btnAdmin.Label = "Admin"
        Me.btnAdmin.Name = "btnAdmin"
        Me.btnAdmin.ShowImage = True
        '
        'btnMaintenance
        '
        Me.btnMaintenance.Image = CType(resources.GetObject("btnMaintenance.Image"), System.Drawing.Image)
        Me.btnMaintenance.Label = "Tx Maint"
        Me.btnMaintenance.Name = "btnMaintenance"
        Me.btnMaintenance.ShowImage = True
        '
        'Button11
        '
        Me.Button11.Image = CType(resources.GetObject("Button11.Image"), System.Drawing.Image)
        Me.Button11.Label = "NewCo"
        Me.Button11.Name = "Button11"
        Me.Button11.ShowImage = True
        '
        'test_report
        '
        Me.test_report.Image = CType(resources.GetObject("test_report.Image"), System.Drawing.Image)
        Me.test_report.Label = "Report"
        Me.test_report.Name = "test_report"
        Me.test_report.ShowImage = True
        '
        'Tasks
        '
        Me.Tasks.Image = CType(resources.GetObject("Tasks.Image"), System.Drawing.Image)
        Me.Tasks.Label = "Tasks"
        Me.Tasks.Name = "Tasks"
        Me.Tasks.ShowImage = True
        '
        'Btn_DBMaint
        '
        Me.Btn_DBMaint.Image = Global.MB.My.Resources.Resources.Suppliers
        Me.Btn_DBMaint.Label = "DB Maint"
        Me.Btn_DBMaint.Name = "Btn_DBMaint"
        Me.Btn_DBMaint.ShowImage = True
        '
        'btn_Update
        '
        Me.btn_Update.Image = CType(resources.GetObject("btn_Update.Image"), System.Drawing.Image)
        Me.btn_Update.Label = "Update"
        Me.btn_Update.Name = "btn_Update"
        Me.btn_Update.ShowImage = True
        '
        'Button13
        '
        Me.Button13.Image = CType(resources.GetObject("Button13.Image"), System.Drawing.Image)
        Me.Button13.Label = "OX"
        Me.Button13.Name = "Button13"
        Me.Button13.ShowImage = True
        '
        'BackgroundWorker1
        '
        '
        'DB_BackgroundWorker
        '
        '
        'Tmr_ServerCheck
        '
        '
        'NI_ServerStatus
        '
        Me.NI_ServerStatus.Icon = CType(resources.GetObject("NI_ServerStatus.Icon"), System.Drawing.Icon)
        Me.NI_ServerStatus.Text = "NotifyIcon1"
        Me.NI_ServerStatus.Visible = True
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.GrpAbout.ResumeLayout(False)
        Me.GrpAbout.PerformLayout()
        Me.Group_Home.ResumeLayout(False)
        Me.Group_Home.PerformLayout()
        Me.ButtonGroup1.ResumeLayout(False)
        Me.ButtonGroup1.PerformLayout()
        Me.GrpLogin.ResumeLayout(False)
        Me.GrpLogin.PerformLayout()
        Me.Box5.ResumeLayout(False)
        Me.Box5.PerformLayout()
        Me.Box4.ResumeLayout(False)
        Me.Box4.PerformLayout()
        Me.Box1.ResumeLayout(False)
        Me.Box1.PerformLayout()
        Me.Box3.ResumeLayout(False)
        Me.Box3.PerformLayout()
        Me.Box2.ResumeLayout(False)
        Me.Box2.PerformLayout()
        Me.Box6.ResumeLayout(False)
        Me.Box6.PerformLayout()
        Me.grpDailyProcessing.ResumeLayout(False)
        Me.grpDailyProcessing.PerformLayout()
        Me.grpMaintanance.ResumeLayout(False)
        Me.grpMaintanance.PerformLayout()
        Me.grpSuppliers.ResumeLayout(False)
        Me.grpSuppliers.PerformLayout()
        Me.grpApprovals.ResumeLayout(False)
        Me.grpApprovals.PerformLayout()
        Me.grpCustomers.ResumeLayout(False)
        Me.grpCustomers.PerformLayout()
        Me.grpGeneral.ResumeLayout(False)
        Me.grpGeneral.PerformLayout()
        Me.grpIntelligence.ResumeLayout(False)
        Me.grpIntelligence.PerformLayout()
        Me.GrpAccounting.ResumeLayout(False)
        Me.GrpAccounting.PerformLayout()
        Me.grpExtaTools.ResumeLayout(False)
        Me.grpExtaTools.PerformLayout()
        Me.grpModes.ResumeLayout(False)
        Me.grpModes.PerformLayout()
        Me.GrpOrderX.ResumeLayout(False)
        Me.GrpOrderX.PerformLayout()
        Me.grpAdmin.ResumeLayout(False)
        Me.grpAdmin.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents Group_Home As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents btnOnOff As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpDailyProcessing As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button_DayStart As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_ShiftHandover As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_WaiterCashup As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_RetailCashup As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_DayEnd As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpSuppliers As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button_CaptureInvoice As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_Wages As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_CreditNotes As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpMaintanance As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button_Settings As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_AccountingSync As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpExtaTools As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents grpIntelligence As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Btn_Reports As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpApprovals As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button2 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button_Company As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpCustomers As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button3 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button5 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button4 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnCreditNotes As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnEFTApprovals As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpGeneral As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button1 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button6 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnAllocations As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents GrpLogin As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents txtUserName As Microsoft.Office.Tools.Ribbon.RibbonEditBox
    Friend WithEvents txtPassword As Microsoft.Office.Tools.Ribbon.RibbonEditBox
    Friend WithEvents Box1 As Microsoft.Office.Tools.Ribbon.RibbonBox
    Friend WithEvents btnLogin As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents lblUsername As Microsoft.Office.Tools.Ribbon.RibbonLabel
    Friend WithEvents Label1 As Microsoft.Office.Tools.Ribbon.RibbonLabel
    Friend WithEvents lblGroup As Microsoft.Office.Tools.Ribbon.RibbonLabel
    Friend WithEvents Box5 As Microsoft.Office.Tools.Ribbon.RibbonBox
    Friend WithEvents Label2 As Microsoft.Office.Tools.Ribbon.RibbonLabel
    Friend WithEvents Box4 As Microsoft.Office.Tools.Ribbon.RibbonBox
    Friend WithEvents Label3 As Microsoft.Office.Tools.Ribbon.RibbonLabel
    Friend WithEvents Box3 As Microsoft.Office.Tools.Ribbon.RibbonBox
    Friend WithEvents Box2 As Microsoft.Office.Tools.Ribbon.RibbonBox
    Friend WithEvents Box6 As Microsoft.Office.Tools.Ribbon.RibbonBox
    Friend WithEvents btnCashSpotCheck As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents chkCheckEmail As Microsoft.Office.Tools.Ribbon.RibbonCheckBox
    Friend WithEvents grpAdmin As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents btnAdmin As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnMaintenance As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnOtherModules As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents GrpAccounting As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents btnBankStatement As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnCashbook As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button7 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button8 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents grpModes As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents DropDown1 As Microsoft.Office.Tools.Ribbon.RibbonDropDown
    Friend WithEvents btnCheckBalance As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents lblTimer As Microsoft.Office.Tools.Ribbon.RibbonLabel
    Friend WithEvents btnMainLogIn As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnTransactions As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnEmpMaster As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btnSupplierMaster As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Btn_Migrate As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button9 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents DB_BackgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents Button10 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Btn_Tasks As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents DD_Group As Microsoft.Office.Tools.Ribbon.RibbonDropDown
    Friend WithEvents DD_Companies As Microsoft.Office.Tools.Ribbon.RibbonDropDown
    Friend WithEvents btn_LogOut As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Tmr_ServerCheck As System.Windows.Forms.Timer
    Friend WithEvents NI_ServerStatus As System.Windows.Forms.NotifyIcon
    Friend WithEvents GrpAbout As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Button12 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Gallery1 As Microsoft.Office.Tools.Ribbon.RibbonGallery
    Friend WithEvents LBL_CoName As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btn_ChangeCo As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ButtonGroup1 As Microsoft.Office.Tools.Ribbon.RibbonButtonGroup
    Friend WithEvents Btn_Cancel As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Gal_BI As Microsoft.Office.Tools.Ribbon.RibbonGallery
    Friend WithEvents Btn_Verify As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button11 As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents test_report As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Btn_InvIntegration As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents GrpOrderX As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents Btn_OrderX As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Gal_OrderX As Microsoft.Office.Tools.Ribbon.RibbonGallery
    Friend WithEvents Tasks As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Btn_DBMaint As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents btn_Update As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents Button13 As Microsoft.Office.Tools.Ribbon.RibbonButton
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
