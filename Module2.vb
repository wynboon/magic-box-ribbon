﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS


Module Module2

    Public blnCRITICAL_ERROR As Boolean = False
    Public blnAppend_Failed As Boolean

    Public oBatch_or_Transaction_generation_Failed As Boolean


    Sub Append_HistoryHeader(ByVal DocumentType As Integer, ByVal DocumentNumber As String, ByVal CustomerCode As String, ByVal DocumentDate As Date, OrderNumber As String, _
                             ByVal Message01 As String, ByVal Message02 As String, ByVal DelAddress01 As String, ByVal DelAddress02 As String, ByVal DelAddress03 As String, _
                             ByVal Terms As Integer, ByVal ExtraCosts As Decimal, ByVal CostCode As String, ByVal PPeriod As Integer, ByVal ClosingDate As Date, _
                             ByVal Telephone As String, ByVal Fax As String, ByVal Contact As String, ByVal DiscountPercent As Decimal, ByVal Total As Decimal, _
                             ByVal FCurrTotal As Decimal, ByVal TotalTax As Decimal, ByVal FCurrTotalTax As Decimal, ByVal invDeleted As String, ByVal Emailed As String)

        Try

            Dim sSQL As String

            sSQL = "INSERT INTO HistoryHeader ( DocumentType, DocumentNumber, CustomerCode, DocumentDate, OrderNumber, Message01, Message02, DelAddress01, DelAddress02, DelAddress03, "
            sSQL = sSQL & "Terms, ExtraCosts, CostCode, PPeriod, ClosingDate, Telephone, Fax, Contact, DiscountPercent, Total, FCurrTotal, TotalTax, FCurrTotalTax, invDeleted, Emailed ) "
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & "" & DocumentType & " as Expr1, " & "'" & DocumentNumber & "' as Expr2, " & "'" & CustomerCode & "' as Expr3, "
            sSQL = sSQL & "#" & CStr(DocumentDate) & "# as Expr4, " & "'" & OrderNumber & "' as Expr5, " & "'" & Message01 & "' as Expr6, "
            sSQL = sSQL & "'" & Message02 & "' as Expr7, " & "'" & DelAddress01 & "' as Expr8, " & "'" & DelAddress02 & "' as Expr9, "
            sSQL = sSQL & "'" & DelAddress03 & "' as Expr10, " & "" & Terms & " as Expr11, " & "" & ExtraCosts & " as Expr12, "
            sSQL = sSQL & "'" & CostCode & "' as Expr13, " & "" & PPeriod & " as Expr14, " & "#" & CStr(ClosingDate) & "# as Expr15, "
            sSQL = sSQL & "'" & Telephone & "' as Expr16, " & "'" & Fax & "' as Expr17, " & "'" & Contact & "' as Expr18, "
            sSQL = sSQL & "" & DiscountPercent & " as Expr19, " & "" & Total & " as Expr20, " & "" & FCurrTotal & " as Expr21, "
            sSQL = sSQL & "" & TotalTax & " as Expr22, " & "" & FCurrTotalTax & " as Expr23, " & "'" & invDeleted & "' as Expr25, "
            sSQL = sSQL & "'" & Emailed & "' as Expr26"

            Dim cn As New OleDbConnection(My.Settings.CS_Setting)

            '// define the sql statement to execute
            Dim cmd As New OleDbCommand(sSQL, cn)

            '    '// open the connection
            cn.Open()

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message & " 168")
        End Try
    End Sub

    Sub Append_HistoryLines(ByVal DocumentType As Integer, ByVal DocumentNumber As String, ByVal ItemCode As String, ByVal CustomerCode As String, ByVal SearchType As Integer, _
                         ByVal PPeriod As Integer, ByVal DDate As Date, ByVal TaxType As Integer, ByVal DiscountType As Integer, ByVal DiscountPercentage As Decimal, _
                         ByVal Description As String, ByVal CostPrice As Decimal, ByVal Qty As Decimal, ByVal UnitPrice As Decimal, ByVal InclusivePrice As Decimal, _
                         ByVal FCurrUnitPrice As Decimal, ByVal FCurrInclPrice As Decimal, ByVal TaxAmt As Decimal, ByVal FCurrTaxAmount As Decimal, ByVal DiscountAmount As Decimal, _
                         ByVal FCDiscountAmount As Decimal, ByVal CostCode As String, ByVal DateTime As Date)

        Try

            Dim sSQL As String

            sSQL = "INSERT INTO HistoryLines ( DocumentType, DocumentNumber, ItemCode, CustomerCode, SearchType, PPeriod, DDate, TaxType, DiscountType, DiscountPercentage, "
            sSQL = sSQL & "Description, CostPrice, Qty, UnitPrice, InclusivePrice, FCurrUnitPrice, FCurrInclPrice, TaxAmt, FCurrTaxAmount, DiscountAmount, FCDiscountAmount, CostCode, [DateTime] ) "
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & "" & DocumentType & " as Expr1, " & "'" & DocumentNumber & "' as Expr2, " & "'" & ItemCode & "' as Expr3, "
            sSQL = sSQL & "'" & CustomerCode & "' as Expr4, " & "" & SearchType & " as Expr5, " & "" & PPeriod & " as Expr6, "
            sSQL = sSQL & "#" & CStr(DDate) & "# as Expr7, " & "" & TaxType & " as Expr8, " & "" & DiscountType & " as Expr9, "
            sSQL = sSQL & "" & DiscountPercentage & " as Expr10, " & "'" & Description & "' as Expr11, " & "" & CostPrice & " as Expr12, "
            sSQL = sSQL & "" & Qty & " as Expr13, " & "" & UnitPrice & " as Expr14, " & "" & InclusivePrice & " as Expr15, "
            sSQL = sSQL & "" & FCurrUnitPrice & " as Expr16, " & "" & FCurrInclPrice & " as Expr17, " & "" & TaxAmt & " as Expr18, "
            sSQL = sSQL & "" & FCurrTaxAmount & " as Expr19, " & "" & DiscountAmount & " as Expr20, " & "" & FCDiscountAmount & " as Expr21, "
            sSQL = sSQL & "'" & CostCode & "' as Expr22, " & "#" & CStr(DateTime) & "# as Expr23"


            Dim cn As New OleDbConnection(My.Settings.CS_Setting)

            '// define the sql statement to execute
            Dim cmd As New OleDbCommand(sSQL, cn)

            '    '// open the connection
            cn.Open()

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message & " 169")
        End Try
    End Sub



 

    Function Database_Lookup_String_return_String(ByVal oTable As String, oLookupField As String, ByVal WhereField As String, ByVal WhereValue As String) As String
        Try
            Dim oResult As String
            Dim sSQL As String

            sSQL = "SELECT DISTINCT [" & oLookupField & "] FROM " & oTable & " WHERE [" & WhereField & "] = '" & SQLConvert(WhereValue) & "'"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Database_Lookup_String_return_String = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                Database_Lookup_String_return_String = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up a database value " & oLookupField & " in table " & oTable & Err.Description)
        End Try
    End Function

    Function GetSetting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT Detail FROM Settings WHERE Description = '" & SQLConvert(sString) & "'"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetSetting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetSetting = cmd.ExecuteScalar().ToString
                connection.Close()
            End If


        Catch ex As Exception
            MsgBox("There was an error looking up an account!")
        End Try
    End Function


    Function Get_Segm2Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm2Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segm2Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up Segment 2 Desc in Accounting!")
        End Try
    End Function
    Function Get_AccountInfo_from_Seg4(ByVal LedgerCode As String, ByVal ReturnFieldName As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [" & ReturnFieldName & "] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(LedgerCode) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_AccountInfo_from_Seg4 = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_AccountInfo_from_Seg4 = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up Segment 2 Desc in Accounting!")
        End Try
    End Function
    Function Get_DisplayName_In_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Diplay Name] FROM Settings WHERE [GLACCOUNT] = '" & SQLConvert(sString) & "'"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_DisplayName_In_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_DisplayName_In_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up a display name in Accounting!")
        End Try
    End Function

    Public Function oCreateTransactionID() As String
        Try
            Dim oUserName As String = Environ("username")
            Dim x As String = CreateRandomPassword(8)

            Dim query As String = "Insert Into TransactionID (oUser, oDate, oCode) Select '" & oUserName & "' as Expr1, '" & Now.Date.ToString("dd MMM yyyy") & "' as Expr2, '" & x & "' as Expr3"
            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(query, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
        Catch ex As Exception
            MsgBox("Error creating transaction ID - Err:" & ex.Message)
        End Try
    End Function

    Public Function oCreateBatchID() As String

                Try
            Dim oUserName As String = Environ("username")
            Dim x As String = CreateRandomPassword(8)

            Dim query As String = "Insert Into BatchID (oUser, oDate, oCode) Select '" & oUserName & "' as Expr1, '" & Now.Date & "' as Expr2, '" & x & "' as Expr3"
            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(query, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
        Catch ex As Exception
            MsgBox("Error creating batch ID - Err:" & ex.Message)
        End Try
    End Function

    Public Function CreateRandomPassword(ByVal PasswordLength As Integer) As String
        Dim _allowedChars As String = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789*$-+?_&=!%{}/"
        Dim randomNumber As New Random()
        Dim chars(PasswordLength - 1) As Char
        Dim allowedCharCount As Integer = _allowedChars.Length
        For i As Integer = 0 To PasswordLength - 1
            chars(i) = _allowedChars.Chars(CInt(Fix((_allowedChars.Length) * randomNumber.NextDouble())))
        Next i
        Return New String(chars)
    End Function




    Function GetTransactionID_FromID(ByVal oID As Integer) As Integer
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT TransactionID FROM Transactions WHERE ID = " & oID
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetTransactionID_FromID = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetTransactionID_FromID = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the transaction ID related to an invoice!")
        End Try
    End Function



    Function SQLConvert(ByVal sString As String) As String
        SQLConvert = Replace(sString, "'", "''")
    End Function


    Function GetVATType_for_Product(ByVal Description As String) As String
        Try

            Dim VATType As Integer
            Dim sSQL As String

            sSQL = "Select Distinct [VAT Type] From Products WHERE Description = '" & Description & "'"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                VATType = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                VATType = cmd.ExecuteScalar().ToString
                connection.Close()
            End If


            If VATType = 0 Then
                GetVATType_for_Product = "Y"
            Else
                GetVATType_for_Product = "N"
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up a VAT type for a product!" & ex.Message)
        End Try
    End Function
    Function Check_If_Invoice_Header_Table_Exists() As Boolean


        Dim oCount As Integer
        Dim oConnectionString As String
        oConnectionString = My.Settings.CS_Setting
        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT COUNT(name) FROM sys.tables where name ='InvoiceHeader'"
                Dim cn As New OleDbConnection(oConnectionString)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Check_If_Invoice_Header_Table_Exists = True
                Else
                    Check_If_Invoice_Header_Table_Exists = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT COUNT(name) FROM sys.tables where name ='InvoiceHeader'"
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Check_If_Invoice_Header_Table_Exists = True
                Else
                    Check_If_Invoice_Header_Table_Exists = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error checking if the InvoiceHeader table exists! " & ex.Message & ".")
        End Try
    End Function
    Function Count_Lines_In_Invoice_Header_Table() As String
        'Herold's Fix - Co_ID filter on query
        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM InvoiceHeader"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()

                Count_Lines_In_Invoice_Header_Table = cmd.ExecuteScalar.ToString

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM InvoiceHeader"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()

                Count_Lines_In_Invoice_Header_Table = cmd.ExecuteScalar.ToString

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            Count_Lines_In_Invoice_Header_Table = "Error"
        End Try
    End Function

    Sub Approve_All_Credit_Notes_set_Accounting_to_true_for_all_transactions_in_Step6_CreditNotes()
        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            Dim sSQL As String = "Select * From Step6_Credit_Notes"
            Dim oTrans As Integer

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                While datareader.Read
                    If Not datareader("Trans").Equals(DBNull.Value) Then
                        oTrans = datareader("Trans")
                        Set_Accounting_to_True(oTrans)
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then


                'the following technique is supposed to speed up the data reader compared to the other boxes
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Trans").Equals(DBNull.Value) Then
                        oTrans = datareader("Trans")
                        Set_Accounting_to_True(oTrans)
                    End If
                End While
                connection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 174")
        End Try

    End Sub

    Sub Set_Accounting_to_True(ByVal oTransactionID As String)

        Try
            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = -1 WHERE TransactionID = " & oTransactionID & " And Accounting = 0"
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)

                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = 1 WHERE TransactionID = " & oTransactionID & " And Accounting = 0"
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()

            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 176")
        End Try

    End Sub

    Sub Approve_EFTs_set_Accounting_to_true_for_all_transactions_in_Step7_EFT()
        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            Dim sSQL As String = "Select * From Step7_EFT"
            Dim oTrans As Integer
            Dim oLinkID As Integer

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                While datareader.Read
                    If Not datareader("Invoice Transaction ID").Equals(DBNull.Value) Then
                        oTrans = datareader("Payment Transaction ID")
                        oLinkID = datareader("Invoice Transaction ID")
                        xSet_Accounting_to_True(oTrans, oLinkID)
                    End If
                End While

                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'the following technique is supposed to speed up the data reader compared to the other boxes
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Invoice Transaction ID").Equals(DBNull.Value) Then
                        oTrans = datareader("Payment Transaction ID")
                        oLinkID = datareader("Invoice Transaction ID")
                        xSet_Accounting_to_True(oTrans, oLinkID)
                    End If
                End While
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("Error automatically approving EFTs. Could be your connection string" & ex.Message & " 177")
        End Try

    End Sub

    Sub xSet_Accounting_to_True(ByVal oTransactionID As String, ByVal oLinkID As String)

        Try
            If My.Settings.DBType = "Access" Then
                'NB SQL False = 0, True = -1
                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = -1 WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0"
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                'NB SQL False = 0, True = 1
                sSQL = "Update Transactions Set Accounting = 1 WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0"
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 178")
        End Try

    End Sub
    Sub QuickApproval(ByVal oTransactionID As String)

        Try
            Dim sSQL As String
            sSQL = "Update Transactions Set Accounting = 1 WHERE TransactionID = " & oTransactionID & " And Accounting = 0 AND Info like '%Invoice Payment Requested%' AND INFO NOT like '%EFT%'"
            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message & " 178")
        End Try

    End Sub
End Module
