﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserAuthorization
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserAuthorization))
        Me.Label_Level = New System.Windows.Forms.Label()
        Me.lblLevel = New System.Windows.Forms.Label()
        Me.Button_Login = New System.Windows.Forms.Button()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtUserName = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dgvUsers = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAddNewUser = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.clbPaymentTypes = New System.Windows.Forms.CheckedListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.txtPassword_New = New System.Windows.Forms.TextBox()
        Me.txtUserName_New = New System.Windows.Forms.TextBox()
        Me.btnDeleteSelected = New System.Windows.Forms.Button()
        Me.ckbPassword = New System.Windows.Forms.CheckBox()
        Me.cmbUserLevel = New System.Windows.Forms.ComboBox()
        Me.UserBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UserIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsernameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PasswordDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LevelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConnectionStringDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApprovePaymentsTypesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnExit = New System.Windows.Forms.Button()
        CType(Me.dgvUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.UserBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label_Level
        '
        Me.Label_Level.AutoSize = True
        Me.Label_Level.ForeColor = System.Drawing.Color.Maroon
        Me.Label_Level.Location = New System.Drawing.Point(547, 23)
        Me.Label_Level.Name = "Label_Level"
        Me.Label_Level.Size = New System.Drawing.Size(0, 13)
        Me.Label_Level.TabIndex = 114
        '
        'lblLevel
        '
        Me.lblLevel.AutoSize = True
        Me.lblLevel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.ForeColor = System.Drawing.Color.SlateGray
        Me.lblLevel.Location = New System.Drawing.Point(494, 23)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(33, 13)
        Me.lblLevel.TabIndex = 113
        Me.lblLevel.Text = "Level"
        Me.lblLevel.Visible = False
        '
        'Button_Login
        '
        Me.Button_Login.BackColor = System.Drawing.Color.Lime
        Me.Button_Login.ForeColor = System.Drawing.Color.Navy
        Me.Button_Login.Location = New System.Drawing.Point(405, 18)
        Me.Button_Login.Name = "Button_Login"
        Me.Button_Login.Size = New System.Drawing.Size(83, 23)
        Me.Button_Login.TabIndex = 2
        Me.Button_Login.Text = "&Login"
        Me.Button_Login.UseVisualStyleBackColor = False
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(272, 20)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(127, 20)
        Me.txtPassword.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.SlateGray
        Me.Label17.Location = New System.Drawing.Point(212, 23)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(53, 13)
        Me.Label17.TabIndex = 109
        Me.Label17.Text = "Password"
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(80, 20)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(127, 20)
        Me.txtUserName.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.SlateGray
        Me.Label14.Location = New System.Drawing.Point(9, 23)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 107
        Me.Label14.Text = "Username"
        '
        'dgvUsers
        '
        Me.dgvUsers.AllowUserToAddRows = False
        Me.dgvUsers.AllowUserToDeleteRows = False
        Me.dgvUsers.AutoGenerateColumns = False
        Me.dgvUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvUsers.BackgroundColor = System.Drawing.Color.White
        Me.dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.UserIdDataGridViewTextBoxColumn, Me.UsernameDataGridViewTextBoxColumn, Me.PasswordDataGridViewTextBoxColumn, Me.LevelDataGridViewTextBoxColumn, Me.ConnectionStringDataGridViewTextBoxColumn, Me.ApprovePaymentsTypesDataGridViewTextBoxColumn, Me.CoIDDataGridViewTextBoxColumn})
        Me.dgvUsers.DataSource = Me.UserBindingSource
        Me.dgvUsers.Location = New System.Drawing.Point(12, 239)
        Me.dgvUsers.Name = "dgvUsers"
        Me.dgvUsers.ReadOnly = True
        Me.dgvUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUsers.Size = New System.Drawing.Size(591, 226)
        Me.dgvUsers.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.ForeColor = System.Drawing.Color.Navy
        Me.btnSave.Location = New System.Drawing.Point(352, 471)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(106, 23)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        Me.btnSave.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbUserLevel)
        Me.GroupBox1.Controls.Add(Me.ckbPassword)
        Me.GroupBox1.Controls.Add(Me.btnAddNewUser)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.clbPaymentTypes)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lblUsername)
        Me.GroupBox1.Controls.Add(Me.txtPassword_New)
        Me.GroupBox1.Controls.Add(Me.txtUserName_New)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Navy
        Me.GroupBox1.Location = New System.Drawing.Point(12, 47)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(591, 186)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "New User"
        '
        'btnAddNewUser
        '
        Me.btnAddNewUser.ForeColor = System.Drawing.Color.Navy
        Me.btnAddNewUser.Location = New System.Drawing.Point(328, 157)
        Me.btnAddNewUser.Name = "btnAddNewUser"
        Me.btnAddNewUser.Size = New System.Drawing.Size(148, 23)
        Me.btnAddNewUser.TabIndex = 4
        Me.btnAddNewUser.Text = "&Add New User"
        Me.btnAddNewUser.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 131)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 118
        Me.Label3.Text = "Level"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(325, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 13)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "Approve Payment Types"
        '
        'clbPaymentTypes
        '
        Me.clbPaymentTypes.FormattingEnabled = True
        Me.clbPaymentTypes.Location = New System.Drawing.Point(328, 31)
        Me.clbPaymentTypes.Name = "clbPaymentTypes"
        Me.clbPaymentTypes.Size = New System.Drawing.Size(148, 116)
        Me.clbPaymentTypes.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 114
        Me.Label1.Text = "Password"
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Location = New System.Drawing.Point(23, 34)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(54, 13)
        Me.lblUsername.TabIndex = 113
        Me.lblUsername.Text = "Username"
        '
        'txtPassword_New
        '
        Me.txtPassword_New.Location = New System.Drawing.Point(84, 66)
        Me.txtPassword_New.Name = "txtPassword_New"
        Me.txtPassword_New.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword_New.Size = New System.Drawing.Size(170, 19)
        Me.txtPassword_New.TabIndex = 1
        '
        'txtUserName_New
        '
        Me.txtUserName_New.Location = New System.Drawing.Point(83, 31)
        Me.txtUserName_New.Name = "txtUserName_New"
        Me.txtUserName_New.Size = New System.Drawing.Size(170, 19)
        Me.txtUserName_New.TabIndex = 0
        '
        'btnDeleteSelected
        '
        Me.btnDeleteSelected.ForeColor = System.Drawing.Color.Navy
        Me.btnDeleteSelected.Location = New System.Drawing.Point(12, 471)
        Me.btnDeleteSelected.Name = "btnDeleteSelected"
        Me.btnDeleteSelected.Size = New System.Drawing.Size(106, 23)
        Me.btnDeleteSelected.TabIndex = 6
        Me.btnDeleteSelected.Text = "&Delete Selected"
        Me.btnDeleteSelected.UseVisualStyleBackColor = True
        '
        'ckbPassword
        '
        Me.ckbPassword.AutoSize = True
        Me.ckbPassword.Location = New System.Drawing.Point(84, 97)
        Me.ckbPassword.Name = "ckbPassword"
        Me.ckbPassword.Size = New System.Drawing.Size(101, 17)
        Me.ckbPassword.TabIndex = 120
        Me.ckbPassword.Text = "Show password"
        Me.ckbPassword.UseVisualStyleBackColor = True
        '
        'cmbUserLevel
        '
        Me.cmbUserLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUserLevel.FormattingEnabled = True
        Me.cmbUserLevel.Items.AddRange(New Object() {"Basic User", "Super User"})
        Me.cmbUserLevel.Location = New System.Drawing.Point(83, 128)
        Me.cmbUserLevel.Name = "cmbUserLevel"
        Me.cmbUserLevel.Size = New System.Drawing.Size(170, 21)
        Me.cmbUserLevel.TabIndex = 2
        '
        'UserBindingSource
        '
        Me.UserBindingSource.DataSource = GetType(MB.User)
        '
        'UserIdDataGridViewTextBoxColumn
        '
        Me.UserIdDataGridViewTextBoxColumn.DataPropertyName = "UserId"
        Me.UserIdDataGridViewTextBoxColumn.HeaderText = "UserId"
        Me.UserIdDataGridViewTextBoxColumn.Name = "UserIdDataGridViewTextBoxColumn"
        Me.UserIdDataGridViewTextBoxColumn.ReadOnly = True
        Me.UserIdDataGridViewTextBoxColumn.Visible = False
        '
        'UsernameDataGridViewTextBoxColumn
        '
        Me.UsernameDataGridViewTextBoxColumn.DataPropertyName = "Username"
        Me.UsernameDataGridViewTextBoxColumn.HeaderText = "Username"
        Me.UsernameDataGridViewTextBoxColumn.Name = "UsernameDataGridViewTextBoxColumn"
        Me.UsernameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PasswordDataGridViewTextBoxColumn
        '
        Me.PasswordDataGridViewTextBoxColumn.DataPropertyName = "Password"
        Me.PasswordDataGridViewTextBoxColumn.HeaderText = "Password"
        Me.PasswordDataGridViewTextBoxColumn.Name = "PasswordDataGridViewTextBoxColumn"
        Me.PasswordDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LevelDataGridViewTextBoxColumn
        '
        Me.LevelDataGridViewTextBoxColumn.DataPropertyName = "Level"
        Me.LevelDataGridViewTextBoxColumn.HeaderText = "Level"
        Me.LevelDataGridViewTextBoxColumn.Name = "LevelDataGridViewTextBoxColumn"
        Me.LevelDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ConnectionStringDataGridViewTextBoxColumn
        '
        Me.ConnectionStringDataGridViewTextBoxColumn.DataPropertyName = "ConnectionString"
        Me.ConnectionStringDataGridViewTextBoxColumn.HeaderText = "ConnectionString"
        Me.ConnectionStringDataGridViewTextBoxColumn.Name = "ConnectionStringDataGridViewTextBoxColumn"
        Me.ConnectionStringDataGridViewTextBoxColumn.ReadOnly = True
        Me.ConnectionStringDataGridViewTextBoxColumn.Visible = False
        '
        'ApprovePaymentsTypesDataGridViewTextBoxColumn
        '
        Me.ApprovePaymentsTypesDataGridViewTextBoxColumn.DataPropertyName = "Approve_Payments_Types"
        Me.ApprovePaymentsTypesDataGridViewTextBoxColumn.HeaderText = "Payment Types"
        Me.ApprovePaymentsTypesDataGridViewTextBoxColumn.Name = "ApprovePaymentsTypesDataGridViewTextBoxColumn"
        Me.ApprovePaymentsTypesDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CoIDDataGridViewTextBoxColumn
        '
        Me.CoIDDataGridViewTextBoxColumn.DataPropertyName = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.HeaderText = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.Name = "CoIDDataGridViewTextBoxColumn"
        Me.CoIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.CoIDDataGridViewTextBoxColumn.Visible = False
        '
        'btnExit
        '
        Me.btnExit.ForeColor = System.Drawing.Color.Navy
        Me.btnExit.Location = New System.Drawing.Point(518, 471)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(85, 23)
        Me.btnExit.TabIndex = 115
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'frmUserAuthorization
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 497)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnDeleteSelected)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dgvUsers)
        Me.Controls.Add(Me.Label_Level)
        Me.Controls.Add(Me.lblLevel)
        Me.Controls.Add(Me.Button_Login)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtUserName)
        Me.Controls.Add(Me.Label14)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(630, 535)
        Me.MinimumSize = New System.Drawing.Size(630, 535)
        Me.Name = "frmUserAuthorization"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmUserAutorization"
        CType(Me.dgvUsers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.UserBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label_Level As System.Windows.Forms.Label
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents Button_Login As System.Windows.Forms.Button
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dgvUsers As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnDeleteSelected As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents txtPassword_New As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName_New As System.Windows.Forms.TextBox
    Friend WithEvents clbPaymentTypes As System.Windows.Forms.CheckedListBox
    Friend WithEvents btnAddNewUser As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ckbPassword As System.Windows.Forms.CheckBox
    Friend WithEvents cmbUserLevel As System.Windows.Forms.ComboBox
    Friend WithEvents UserIdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsernameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PasswordDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LevelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConnectionStringDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ApprovePaymentsTypesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnExit As System.Windows.Forms.Button
End Class
