﻿Imports System.Data.SqlClient
Imports System.Data

Module Mod_Comms
    Function Comm_SendComm(ComType As String, CommAddress As String, CommMessage As String)
        Try
            '2 is email
            Dim strSQL As String = ""
            strSQL = "Insert Into [Comms_Pending] ([CommsQUID],[CommType],[CommAddress],[CommMessage]) Values (" & _
                "'" & Guid.NewGuid.ToString & "','" & ComType & "','" & CommAddress & "','" & CommMessage & "')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error sending communication! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function CreateRemittanceDataTable() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("TransactionID")
        dt.Columns.Add("SupplierName")
        dt.Columns.Add("SupplierID")
        dt.Columns.Add("Transaction Date")
        dt.Columns.Add("Reference")
        dt.Columns.Add("Amount")
        dt.Columns.Add("PaymentType")
        Return dt

    End Function
    Function RemittanceDataTable_AddRow(TransactionID As Integer, SupplierName As String, SupplierID As Long, TransDate As String, Ref As String, Amount As Double, PaymentType As String)

        '**** there is a global table called RemittanceTable, this method is used to add rows to that datatable, and then at the end bulk Post
        With Globals.Ribbons.Ribbon1.RemittanceTable
            Dim R As DataRow = .NewRow
            R("TransactionID") = TransactionID
            R("SupplierID") = SupplierID
            R("SupplierName") = SupplierName
            R("Transaction Date") = TransDate
            R("Reference") = Ref
            R("Amount") = Math.Abs(Amount)
            R("PaymentType") = PaymentType
            .Rows.Add(R)
        End With
    End Function
    Function SendRemittance()
        Dim Body As String = ""
        Dim EmailTo As String
        Dim InvRef As String
        Dim InvDate As String
        Dim InvAmount As String
        Dim InvPayType As String
        Dim SuppID As Long

        Dim distinctDT As DataTable = Globals.Ribbons.Ribbon1.RemittanceTable.DefaultView.ToTable(True, "SupplierName")
        Globals.Ribbons.Ribbon1.RemittanceTable.DefaultView.Sort = "SupplierName"
        Dim SumAmt As Double
        For Each drSup As DataRow In distinctDT.Rows
            Body = "<h4>This is a notification email to confirm that the following payments have been made to your organisation:</h4><br>"
            For Each drInv As DataRow In Globals.Ribbons.Ribbon1.RemittanceTable.Rows
                If drSup.Item("SupplierName") = drInv.Item("SupplierName") Then
                    InvRef = drInv.Item("Reference").ToString
                    InvDate = drInv.Item("Transaction Date")
                    InvAmount = drInv.Item("Amount")
                    InvPayType = drInv.Item("PaymentType")
                    SuppID = drInv.Item("SupplierID")
                    Body = Body & vbCrLf & "Invoice Number: " & InvRef & ", Amount: " & InvAmount & ", Payment Type: " & InvPayType & "<br>"
                    SumAmt = SumAmt + drInv.Item("Amount")
                End If
            Next
            Body = Body & "A total amount of " & SumAmt & " has been paid.<br>"
            Body = Body & "Thank you," & vbNewLine & My.Settings.CurrentCompany
            If My.Settings.EmailDefault = "Yes" Then
                If My.Settings.DefaultEmailAddress <> "" Then
                    Comm_SendComm(2, My.Settings.DefaultEmailAddress, Body)
                End If
            ElseIf My.Settings.EmailAll = "Yes" Then
                EmailTo = Get_Supplier_Email(SuppID)
                If EmailTo <> "" Then
                    Comm_SendComm(2, EmailTo, Body)
                End If
                If My.Settings.DefaultEmailAddress <> "" Then
                    Comm_SendComm(2, My.Settings.DefaultEmailAddress, Body)
                End If
            End If
        Next
    End Function
End Module
