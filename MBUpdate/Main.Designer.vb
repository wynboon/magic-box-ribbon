﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TBTypes = New System.Windows.Forms.TabControl()
        Me.TB_Core = New System.Windows.Forms.TabPage()
        Me.TB_Modules = New System.Windows.Forms.TabPage()
        Me.TB_Beta = New System.Windows.Forms.TabPage()
        Me.TBTypes.SuspendLayout()
        Me.SuspendLayout()
        '
        'TBTypes
        '
        Me.TBTypes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBTypes.Controls.Add(Me.TB_Core)
        Me.TBTypes.Controls.Add(Me.TB_Modules)
        Me.TBTypes.Controls.Add(Me.TB_Beta)
        Me.TBTypes.Location = New System.Drawing.Point(12, 12)
        Me.TBTypes.Name = "TBTypes"
        Me.TBTypes.SelectedIndex = 0
        Me.TBTypes.Size = New System.Drawing.Size(888, 535)
        Me.TBTypes.TabIndex = 0
        '
        'TB_Core
        '
        Me.TB_Core.Location = New System.Drawing.Point(4, 25)
        Me.TB_Core.Name = "TB_Core"
        Me.TB_Core.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_Core.Size = New System.Drawing.Size(880, 506)
        Me.TB_Core.TabIndex = 0
        Me.TB_Core.Text = "Core"
        Me.TB_Core.UseVisualStyleBackColor = True
        '
        'TB_Modules
        '
        Me.TB_Modules.Location = New System.Drawing.Point(4, 25)
        Me.TB_Modules.Name = "TB_Modules"
        Me.TB_Modules.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_Modules.Size = New System.Drawing.Size(880, 506)
        Me.TB_Modules.TabIndex = 1
        Me.TB_Modules.Text = "Modules"
        Me.TB_Modules.UseVisualStyleBackColor = True
        '
        'TB_Beta
        '
        Me.TB_Beta.Location = New System.Drawing.Point(4, 25)
        Me.TB_Beta.Name = "TB_Beta"
        Me.TB_Beta.Size = New System.Drawing.Size(880, 506)
        Me.TB_Beta.TabIndex = 2
        Me.TB_Beta.Text = "Beta Versions"
        Me.TB_Beta.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(912, 559)
        Me.Controls.Add(Me.TBTypes)
        Me.Name = "Main"
        Me.Text = "Form1"
        Me.TBTypes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TBTypes As System.Windows.Forms.TabControl
    Friend WithEvents TB_Core As System.Windows.Forms.TabPage
    Friend WithEvents TB_Modules As System.Windows.Forms.TabPage
    Friend WithEvents TB_Beta As System.Windows.Forms.TabPage

End Class
