﻿Imports System.Data
Imports System.Windows.Forms

Public Class Frm_Verify
    Private Sub Frm_Verify_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Verification -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
    End Sub
    Function BuildTree(Optional FindString As String = "0")
        Try
            TreeView1.Nodes.Clear()
            Dim dt As New DataTable
            'Fill Treeview with summary and then detailed info
            dt = VerifiedSummary2(DT_From.Text, DT_To.Text)
            If dt.Rows.Count <= 0 Then
                Exit Function
            End If

            Dim dt_Summary As New DataTable
            Dim TobeDistinct As String() = {"Transaction Date", "SupplierID", "SupplierName"}
            dt_Summary = dt.DefaultView.ToTable(True, TobeDistinct)
            For Each dr As DataRow In dt_Summary.Rows
                AddNode(System.DateTime.Parse(dr("Transaction Date")).ToString("ddMMyyyy"), System.DateTime.Parse(dr("Transaction Date")).ToString("dd MMM yyyy"), _
                         "S~" & dr("SupplierID").ToString() & "D~" & System.DateTime.Parse(dr("Transaction Date")).ToString("ddMMyyyy"), _
                        dr("SupplierName").ToString(), "S~" & dr("SupplierID").ToString(), 1, True)
            Next

            'DoDetail
            TobeDistinct = {"Transaction Date", "SupplierID", "SupplierName", "TransactionID", "Verified"}
            dt_Summary = dt.DefaultView.ToTable(True, TobeDistinct)
            Dim CurrentDate As String
            Dim ImageKey As Integer
            dt.Select("", "Transaction Date", DataViewRowState.CurrentRows)
            CurrentDate = dt.Rows(0).Item("Transaction Date")
            Dim isVerified As Boolean = False
            For Each dr As DataRow In dt.Rows
                isVerified = False
                If IsDBNull(dr("Verified")) Then
                    ImageKey = 2
                ElseIf dr("Verified") Then
                    ImageKey = 3
                    isVerified = True
                Else
                    ImageKey = 2
                End If
                If CB_ShowVerified.Checked = False And isVerified = True Then
                Else
                    AddNode("S~" & dr("SupplierID").ToString() & "D~" & System.DateTime.Parse(dr("Transaction Date")).ToString("ddMMyyyy"), _
                             dr("SupplierName").ToString(), _
                               "Z~" & dr("TransactionID").ToString(), dr("Reference").ToString(), _
                            "Z~" & dr("TransactionID").ToString(), ImageKey, True)
                End If
            Next
            Application.DoEvents()
            If FindString <> "0" Then
                TreeView1.Nodes.Find("Z~" & FindString, True)
            End If
        Catch ex As Exception
            MsgBox("Error loading tree view: " & ex.Message, MsgBoxStyle.Critical, "Loading Error")
        End Try
    End Function
    Private Sub AddNode(ParentKey As String, parentText As String, ChildKey As String, ChildText As String, ChildTag As String, IMIndex As Integer, ExpNode As Boolean)
        Dim node As New List(Of TreeNode)
        Dim InsertNode As New TreeNode
        node.AddRange(TreeView1.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            node.Add(TreeView1.Nodes.Add(ParentKey, parentText))
        End If
        InsertNode.Name = ChildKey
        InsertNode.Text = ChildText
        InsertNode.Tag = ChildTag
        InsertNode.ImageIndex = IMIndex
        node(0).Nodes.Add(InsertNode)

        If ExpNode = True Then
            node(0).Expand()
        End If

    End Sub
    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If IsNothing(TreeView1.SelectedNode.Tag) Then Exit Sub
            Dim TheTag As String = TreeView1.SelectedNode.Tag
            Dim TransactionID As String
            If TheTag.Substring(0, 2) = "Z~" Then
                TransactionID = TheTag.Remove(0, 2)
                FillGrid(TransactionID)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Btn_Search_Click(sender As Object, e As EventArgs) Handles Btn_Search.Click
        BuildTree()
    End Sub
    Function FillGrid(TransactionID As String)
        Try
            Dim DT_Transactions As DataTable
            DT_Transactions = VerifiedDetail(TransactionID)
            DataGridView1.DataSource = DT_Transactions
            If DT_Transactions.Rows.Count > 0 Then
                DataGridView1.Columns("Amount").DefaultCellStyle.Format = "c"
                DataGridView1.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                DataGridView1.Columns("Tax Amount").DefaultCellStyle.Format = "c"
                DataGridView1.Columns("Tax Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                DataGridView1.Columns("Total").DefaultCellStyle.Format = "c"
                DataGridView1.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                DataGridView1.Columns("Verified").Visible = False
                DataGridView1.Columns("VerificationNotes").Visible = False
                Lbl_InvRef.Text = DT_Transactions.Rows(0).Item("Reference")
                LBL_InvDAte.Text = DT_Transactions.Rows(0).Item("Transaction Date")
                LBL_TxID.Text = DT_Transactions.Rows(0).Item("TransactionID")
                LBL_SupplierName.Text = DT_Transactions.Rows(0).Item("SupplierName")
                If IsDBNull(DT_Transactions.Rows(0).Item("Verified")) Then
                    Btn_Verified.Text = "Verify Invoice"
                    TB_VerifyNotes.Text = ""
                ElseIf DT_Transactions.Rows(0).Item("Verified") = False Then
                    Btn_Verified.Text = "Update"
                    TB_VerifyNotes.Text = DT_Transactions.Rows(0).Item("VerificationNotes")
                Else
                    Btn_Verified.Text = "Remove Verified Status"
                    TB_VerifyNotes.Text = DT_Transactions.Rows(0).Item("VerificationNotes")
                End If
                Dim PayAmount As Double = Get_PaymentAmount_by_TransactionID(TransactionID)
                lbl_Payment.Text = PayAmount
                If PayAmount <> 0 Then
                    MsgBox("There are payments linked. This invoice cannot be edited to a different total amount.")
                    Btn_Edit.Enabled = True
                Else
                    Btn_Edit.Enabled = True
                End If

            End If
            AddTotals()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Function AddTotals()
        Dim sum As Double = 0
        Dim TotalExcl As Double
        Dim TotalIncl As Double
        Dim TotalVAT As Double

        For i = 0 To DataGridView1.RowCount - 1
            sum += DataGridView1.Rows(i).Cells("Total").Value()
        Next
        LBL_Amount_Excl.Text = Decimal.Parse(sum).ToString("C")
        TotalExcl = sum
        sum = 0

        For i = 0 To DataGridView1.RowCount - 1
            sum += DataGridView1.Rows(i).Cells("Tax Amount").Value()
        Next
        LBL_VAT_Amount.Text = Decimal.Parse(sum).ToString("C")
        TotalVAT = sum
        sum = 0

        For i = 0 To DataGridView1.RowCount - 1
            sum += DataGridView1.Rows(i).Cells("Amount").Value()
        Next
        LBL_Incl_Amount.Text = Decimal.Parse(sum).ToString("C")
        TotalIncl = sum
        sum = 0



        LBL_Incl_Amount.Text = TotalExcl.ToString("C")
        LBL_VAT_Amount.Text = TotalVAT.ToString("C")
        LBL_Amount_Excl.Text = TotalIncl.ToString("C")


        sum = 0
    End Function
    Private Sub Btn_Verified_Click(sender As Object, e As EventArgs) Handles Btn_Verified.Click
        If Btn_Verified.Text = "Verify Invoice" Then
            'Insert verification
            InsertVerifivation(LBL_TxID.Text, True, TB_VerifyNotes.Text)
        ElseIf Btn_Verified.Text = "Update" Then
            'Update Verification true
            UpdateVerifivation(LBL_TxID.Text, True, TB_VerifyNotes.Text)
        ElseIf Btn_Verified.Text = "Remove Verified Status" Then
            'Update False
            UpdateVerifivation(LBL_TxID.Text, False, TB_VerifyNotes.Text)
        End If
        FillGrid(LBL_TxID.Text)
        BuildTree(Lbl_InvRef.Text)

    End Sub

    Private Sub Btn_Edit_Click(sender As Object, e As EventArgs) Handles Btn_Edit.Click
        Dim OriginalID As Long
        Dim Paid_Amount As Double
        OriginalID = LBL_TxID.Text
        Dim frmInvcEdit As New frmInvoiceEdit
        frmInvcEdit.Lbl_OrigTxID.Text = OriginalID
        frmInvcEdit.Lbl_OriginalAmount.Text = LBL_Incl_Amount.Text
        frmInvcEdit.CB_SupplierName.Text = LBL_SupplierName.Text
        frmInvcEdit.TB_InvNo.Text = Lbl_InvRef.Text
        frmInvcEdit.DTP_InvoiceDate.Text = LBL_InvDAte.Text
        If IsNumeric(lbl_Payment.Text) Then
            Paid_Amount = lbl_Payment.Text
            If Paid_Amount > 0 Then
                frmInvcEdit.OriginalPaid.Text = "Paid Invoice"
            Else
                frmInvcEdit.OriginalPaid.Text = "Unpaid Invoice"
            End If
        End If
        frmInvcEdit.oFormLoad()
            frmInvcEdit.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Invoice Editing -> Version : " & My.Settings.Setting_Version

            ' frmInvcEdit.BringToFront()

            'Populate_Invoice_Capture_Form()

            'frmInvcCapture.Save1.Visible = True
            'frmInvcCapture.lblEditInvoice.Visible = True
            'frmInvcCapture.lblEditInvoice_TransactionID.Text = CStr(oTransactionID)
            'frmInvcCapture.BringToFront()

            Enabled = True
            Cursor = Cursors.Arrow
            frmInvcEdit.ShowDialog()
            If ChangedTxID = "" Then
                ChangedTxID = OriginalID
            End If
            Call BuildTree(ChangedTxID)
            FillGrid(ChangedTxID)
    End Sub

    Private Sub DT_From_ValueChanged(sender As Object, e As EventArgs) Handles DT_From.ValueChanged
        DT_To.Text = DT_From.Text
    End Sub
End Class