﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOtherModules
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOtherModules))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvActiveModules = New System.Windows.Forms.DataGridView()
        Me.MBModulesInventoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpDateReleased = New System.Windows.Forms.DateTimePicker()
        Me.txtDetailDesc = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtModuleName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ModuleIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ModuleNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActualFileNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetailDescriptionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LaunchPathDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateCreatedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateModifiedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ModuleAuthorDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IsActiveDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CommentsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvActiveModules, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MBModulesInventoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvActiveModules)
        Me.Panel1.Location = New System.Drawing.Point(8, 130)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(778, 344)
        Me.Panel1.TabIndex = 0
        '
        'dgvActiveModules
        '
        Me.dgvActiveModules.AllowUserToAddRows = False
        Me.dgvActiveModules.AllowUserToDeleteRows = False
        Me.dgvActiveModules.AutoGenerateColumns = False
        Me.dgvActiveModules.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvActiveModules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvActiveModules.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ModuleIDDataGridViewTextBoxColumn, Me.ModuleNameDataGridViewTextBoxColumn, Me.ActualFileNameDataGridViewTextBoxColumn, Me.DetailDescriptionDataGridViewTextBoxColumn, Me.LaunchPathDataGridViewTextBoxColumn, Me.DateCreatedDataGridViewTextBoxColumn, Me.DateModifiedDataGridViewTextBoxColumn, Me.ModuleAuthorDataGridViewTextBoxColumn, Me.IsActiveDataGridViewTextBoxColumn, Me.CommentsDataGridViewTextBoxColumn})
        Me.dgvActiveModules.DataSource = Me.MBModulesInventoryBindingSource
        Me.dgvActiveModules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvActiveModules.Location = New System.Drawing.Point(0, 0)
        Me.dgvActiveModules.Name = "dgvActiveModules"
        Me.dgvActiveModules.ReadOnly = True
        Me.dgvActiveModules.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvActiveModules.Size = New System.Drawing.Size(778, 344)
        Me.dgvActiveModules.TabIndex = 0
        '
        'MBModulesInventoryBindingSource
        '
        Me.MBModulesInventoryBindingSource.DataSource = GetType(MB.MB_ModulesInventory)
        '
        'btnExit
        '
        Me.btnExit.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnExit.Location = New System.Drawing.Point(693, 480)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(93, 23)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.dtpDateReleased)
        Me.GroupBox1.Controls.Add(Me.txtDetailDesc)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtModuleName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(8, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(778, 119)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Module Details"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Date released"
        '
        'dtpDateReleased
        '
        Me.dtpDateReleased.Enabled = False
        Me.dtpDateReleased.Location = New System.Drawing.Point(26, 82)
        Me.dtpDateReleased.Name = "dtpDateReleased"
        Me.dtpDateReleased.Size = New System.Drawing.Size(313, 20)
        Me.dtpDateReleased.TabIndex = 6
        '
        'txtDetailDesc
        '
        Me.txtDetailDesc.BackColor = System.Drawing.Color.White
        Me.txtDetailDesc.Location = New System.Drawing.Point(383, 39)
        Me.txtDetailDesc.Multiline = True
        Me.txtDetailDesc.Name = "txtDetailDesc"
        Me.txtDetailDesc.ReadOnly = True
        Me.txtDetailDesc.Size = New System.Drawing.Size(378, 63)
        Me.txtDetailDesc.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(380, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Detail description :"
        '
        'txtModuleName
        '
        Me.txtModuleName.BackColor = System.Drawing.Color.White
        Me.txtModuleName.Location = New System.Drawing.Point(26, 39)
        Me.txtModuleName.Name = "txtModuleName"
        Me.txtModuleName.ReadOnly = True
        Me.txtModuleName.Size = New System.Drawing.Size(313, 20)
        Me.txtModuleName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Module name :"
        '
        'ModuleIDDataGridViewTextBoxColumn
        '
        Me.ModuleIDDataGridViewTextBoxColumn.DataPropertyName = "ModuleID"
        Me.ModuleIDDataGridViewTextBoxColumn.HeaderText = "ModuleID"
        Me.ModuleIDDataGridViewTextBoxColumn.Name = "ModuleIDDataGridViewTextBoxColumn"
        Me.ModuleIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.ModuleIDDataGridViewTextBoxColumn.Visible = False
        '
        'ModuleNameDataGridViewTextBoxColumn
        '
        Me.ModuleNameDataGridViewTextBoxColumn.DataPropertyName = "ModuleName"
        Me.ModuleNameDataGridViewTextBoxColumn.HeaderText = "Module name"
        Me.ModuleNameDataGridViewTextBoxColumn.Name = "ModuleNameDataGridViewTextBoxColumn"
        Me.ModuleNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ActualFileNameDataGridViewTextBoxColumn
        '
        Me.ActualFileNameDataGridViewTextBoxColumn.DataPropertyName = "ActualFileName"
        Me.ActualFileNameDataGridViewTextBoxColumn.HeaderText = "ActualFileName"
        Me.ActualFileNameDataGridViewTextBoxColumn.Name = "ActualFileNameDataGridViewTextBoxColumn"
        Me.ActualFileNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.ActualFileNameDataGridViewTextBoxColumn.Visible = False
        '
        'DetailDescriptionDataGridViewTextBoxColumn
        '
        Me.DetailDescriptionDataGridViewTextBoxColumn.DataPropertyName = "DetailDescription"
        Me.DetailDescriptionDataGridViewTextBoxColumn.HeaderText = "Details"
        Me.DetailDescriptionDataGridViewTextBoxColumn.Name = "DetailDescriptionDataGridViewTextBoxColumn"
        Me.DetailDescriptionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DetailDescriptionDataGridViewTextBoxColumn.Visible = False
        '
        'LaunchPathDataGridViewTextBoxColumn
        '
        Me.LaunchPathDataGridViewTextBoxColumn.DataPropertyName = "LaunchPath"
        Me.LaunchPathDataGridViewTextBoxColumn.HeaderText = "LaunchPath"
        Me.LaunchPathDataGridViewTextBoxColumn.Name = "LaunchPathDataGridViewTextBoxColumn"
        Me.LaunchPathDataGridViewTextBoxColumn.ReadOnly = True
        Me.LaunchPathDataGridViewTextBoxColumn.Visible = False
        '
        'DateCreatedDataGridViewTextBoxColumn
        '
        Me.DateCreatedDataGridViewTextBoxColumn.DataPropertyName = "DateCreated"
        Me.DateCreatedDataGridViewTextBoxColumn.HeaderText = "Date created"
        Me.DateCreatedDataGridViewTextBoxColumn.Name = "DateCreatedDataGridViewTextBoxColumn"
        Me.DateCreatedDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DateModifiedDataGridViewTextBoxColumn
        '
        Me.DateModifiedDataGridViewTextBoxColumn.DataPropertyName = "DateModified"
        Me.DateModifiedDataGridViewTextBoxColumn.HeaderText = "DateModified"
        Me.DateModifiedDataGridViewTextBoxColumn.Name = "DateModifiedDataGridViewTextBoxColumn"
        Me.DateModifiedDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateModifiedDataGridViewTextBoxColumn.Visible = False
        '
        'ModuleAuthorDataGridViewTextBoxColumn
        '
        Me.ModuleAuthorDataGridViewTextBoxColumn.DataPropertyName = "ModuleAuthor"
        Me.ModuleAuthorDataGridViewTextBoxColumn.HeaderText = "Module Author"
        Me.ModuleAuthorDataGridViewTextBoxColumn.Name = "ModuleAuthorDataGridViewTextBoxColumn"
        Me.ModuleAuthorDataGridViewTextBoxColumn.ReadOnly = True
        Me.ModuleAuthorDataGridViewTextBoxColumn.Visible = False
        '
        'IsActiveDataGridViewTextBoxColumn
        '
        Me.IsActiveDataGridViewTextBoxColumn.DataPropertyName = "IsActive"
        Me.IsActiveDataGridViewTextBoxColumn.HeaderText = "IsActive"
        Me.IsActiveDataGridViewTextBoxColumn.Name = "IsActiveDataGridViewTextBoxColumn"
        Me.IsActiveDataGridViewTextBoxColumn.ReadOnly = True
        Me.IsActiveDataGridViewTextBoxColumn.Visible = False
        '
        'CommentsDataGridViewTextBoxColumn
        '
        Me.CommentsDataGridViewTextBoxColumn.DataPropertyName = "Comments"
        Me.CommentsDataGridViewTextBoxColumn.HeaderText = "Comments"
        Me.CommentsDataGridViewTextBoxColumn.Name = "CommentsDataGridViewTextBoxColumn"
        Me.CommentsDataGridViewTextBoxColumn.ReadOnly = True
        Me.CommentsDataGridViewTextBoxColumn.Visible = False
        '
        'frmOtherModules
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(793, 513)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(809, 551)
        Me.MinimumSize = New System.Drawing.Size(809, 551)
        Me.Name = "frmOtherModules"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvActiveModules, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MBModulesInventoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvActiveModules As System.Windows.Forms.DataGridView
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents MBModulesInventoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpDateReleased As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtDetailDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtModuleName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ModuleIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModuleNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActualFileNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DetailDescriptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LaunchPathDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateCreatedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateModifiedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ModuleAuthorDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsActiveDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CommentsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
