﻿Imports System.Windows.Forms
Imports System.Collections

Public Class frmDefaultWorkspace
#Region "Variables"
    Public TenID As Integer = Nothing
    Public StoresCount As Integer = Nothing
    Dim Roles As New DataMethods
    Dim MBData As New DataExtractsClass
    Dim onLoad As Boolean = True
#End Region
#Region "Methods"
    Private Sub InitialLoad()

        If TenID <> Nothing Then

            TenantEnabledStores = Roles.StoreNameIDPair(TenID)
            If TenantEnabledStores.Count <> 0 Then

                My.Settings.CurrentCompany = ""
                My.Settings.Setting_CompanyID = ""


                cmbCompany.DataSource = New BindingSource(TenantEnabledStores, Nothing)
                cmbCompany.DisplayMember = "Value"
                cmbCompany.ValueMember = "Key"
                StoresCount = CInt(cmbCompany.Items.Count)
                cmbCompany.SelectedIndex = -1
                onLoad = False
            Else
                MessageBox.Show("You do not have stores linked to your username, Please contact the MagicBox team for assistance", "User stores", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            End If


        End If

    End Sub
#End Region

    Private Sub frmDefaultWorkspace_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If TenantEnabledStores.Count <> 0 Then
            If My.Settings.CurrentCompany = String.Empty Or My.Settings.CurrentCompany = Nothing Then
                MessageBox.Show("You did not select default a workspace, first store in the list will be automatically set as your default workspace.", "Default workspace", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Dim Storename As String = CStr(cmbCompany.Text)
                Dim Co_ID As Integer = CInt(cmbCompany.SelectedValue)
                My.Settings.CurrentCompany = Storename
                My.Settings.Setting_CompanyID = Co_ID
                My.Settings.Save()
            End If
        End If
    End Sub
    Private Sub frmDefaultWorkspace_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitialLoad()
    End Sub
    Private Sub cmbCompany_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbCompany.SelectedValueChanged
        If onLoad = False Then
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Dim Storename As String = CStr(cmbCompany.Text)
            Dim Co_ID As Integer = CInt(cmbCompany.SelectedValue)
            My.Settings.CurrentCompany = Storename
            My.Settings.Setting_CompanyID = Co_ID
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("Default workspace set to :'" & cmbCompany.Text, "' Default workspace", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        End If
    End Sub
End Class