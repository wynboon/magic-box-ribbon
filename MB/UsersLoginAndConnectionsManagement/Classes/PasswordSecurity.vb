﻿Imports System.Security.Cryptography
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient

Public Class PasswordSecurity

#Region "Varialbles"
    Const PasswordEncryptionKey As String = "UserRolesPOC"
#End Region

#Region "Methods"

    Public Function EncryptString(Message As String) As String
        Dim Results As Byte()
        Dim UTF8 As New System.Text.UTF8Encoding()
        Dim HashProvider As New MD5CryptoServiceProvider()
        Dim TDESKey As Byte() = HashProvider.ComputeHash(UTF8.GetBytes(PasswordEncryptionKey))
        Dim TDESAlgorithm As New TripleDESCryptoServiceProvider()
        TDESAlgorithm.Key = TDESKey
        TDESAlgorithm.Mode = CipherMode.ECB
        TDESAlgorithm.Padding = PaddingMode.PKCS7
        Dim DataToEncrypt As Byte() = UTF8.GetBytes(Message)
        Try
            Dim Encryptor As ICryptoTransform = TDESAlgorithm.CreateEncryptor()
            Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length)
        Finally
            TDESAlgorithm.Clear()
            HashProvider.Clear()
        End Try
        Return Convert.ToBase64String(Results)
    End Function
    Public Function DecryptString(Message As String) As String
        Dim Results As Byte()
        Dim UTF8 As New System.Text.UTF8Encoding()
        Dim HashProvider As New MD5CryptoServiceProvider()
        Dim TDESKey As Byte() = HashProvider.ComputeHash(UTF8.GetBytes(PasswordEncryptionKey))
        Dim TDESAlgorithm As New TripleDESCryptoServiceProvider()
        TDESAlgorithm.Key = TDESKey
        TDESAlgorithm.Mode = CipherMode.ECB
        TDESAlgorithm.Padding = PaddingMode.PKCS7
        Dim DataToDecrypt As Byte() = Convert.FromBase64String(Message)
        Try
            Dim Decryptor As ICryptoTransform = TDESAlgorithm.CreateDecryptor()
            Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length)
        Finally
            TDESAlgorithm.Clear()
            HashProvider.Clear()
        End Try
        Return UTF8.GetString(Results)

    End Function
    Public Function TestServer(ByVal Servername As String, Databasename As String, ByVal Username As String, ByVal Password As String)

        Dim ConnString As String = ""
        Dim ConnectionStatus As Boolean = False

        ConnString = "Server=" & Servername & ";Database=" & Databasename & ";Uid=" & Username & ";Pwd=" & Password

        'Testing connection String
        Try
            Dim cn As New SqlConnection(ConnString)
            Using cn
                cn.Open()
                If cn.State = Data.ConnectionState.Open Then
                    ConnectionStatus = True
                Else
                    ConnectionStatus = False
                End If
            End Using
        Catch ex As Exception
            ConnectionStatus = False
        End Try
        Return ConnectionStatus
    End Function

#End Region

End Class
