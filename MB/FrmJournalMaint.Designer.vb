﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmJournalMaint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGV_Lines = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Btn_New = New System.Windows.Forms.Button()
        Me.Btn_LoadJ = New System.Windows.Forms.Button()
        Me.Btn_removeTemplate = New System.Windows.Forms.Button()
        Me.lbl_TemplateID = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lbl_HeaderName = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TB_Desc = New System.Windows.Forms.TextBox()
        Me.TB_Amount = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CB_Account = New System.Windows.Forms.ComboBox()
        Me.CB_DrCr = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CB_Tax = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Grp_LineInfo = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RB_AmtIncl = New System.Windows.Forms.RadioButton()
        Me.RB_AmtExcl = New System.Windows.Forms.RadioButton()
        Me.TB_Incl = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_Tax = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.RB_ContraByDesc = New System.Windows.Forms.RadioButton()
        Me.RB_ContraByCode = New System.Windows.Forms.RadioButton()
        Me.CB_Contra = New System.Windows.Forms.ComboBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RB_AccByDesc = New System.Windows.Forms.RadioButton()
        Me.RB_AccByCode = New System.Windows.Forms.RadioButton()
        Me.Lbl_RemoveLine = New System.Windows.Forms.Button()
        Me.lbl_AddLine = New System.Windows.Forms.Button()
        Me.lsv_Names = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        CType(Me.DGV_Lines, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Grp_LineInfo.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGV_Lines
        '
        Me.DGV_Lines.AllowUserToAddRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DGV_Lines.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_Lines.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_Lines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Lines.Location = New System.Drawing.Point(225, 179)
        Me.DGV_Lines.Name = "DGV_Lines"
        Me.DGV_Lines.RowTemplate.Height = 24
        Me.DGV_Lines.Size = New System.Drawing.Size(1536, 590)
        Me.DGV_Lines.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Btn_New)
        Me.GroupBox1.Controls.Add(Me.Btn_LoadJ)
        Me.GroupBox1.Controls.Add(Me.Btn_removeTemplate)
        Me.GroupBox1.Controls.Add(Me.lbl_TemplateID)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.lbl_HeaderName)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(225, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1535, 68)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Header Information"
        '
        'Btn_New
        '
        Me.Btn_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_New.Location = New System.Drawing.Point(1126, 15)
        Me.Btn_New.Name = "Btn_New"
        Me.Btn_New.Size = New System.Drawing.Size(132, 40)
        Me.Btn_New.TabIndex = 14
        Me.Btn_New.Text = "New Template"
        Me.Btn_New.UseVisualStyleBackColor = True
        '
        'Btn_LoadJ
        '
        Me.Btn_LoadJ.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_LoadJ.Location = New System.Drawing.Point(1399, 15)
        Me.Btn_LoadJ.Name = "Btn_LoadJ"
        Me.Btn_LoadJ.Size = New System.Drawing.Size(132, 40)
        Me.Btn_LoadJ.TabIndex = 13
        Me.Btn_LoadJ.Text = "Load Journal"
        Me.Btn_LoadJ.UseVisualStyleBackColor = True
        '
        'Btn_removeTemplate
        '
        Me.Btn_removeTemplate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_removeTemplate.Location = New System.Drawing.Point(1261, 15)
        Me.Btn_removeTemplate.Name = "Btn_removeTemplate"
        Me.Btn_removeTemplate.Size = New System.Drawing.Size(132, 40)
        Me.Btn_removeTemplate.TabIndex = 12
        Me.Btn_removeTemplate.Text = "Remove Template"
        Me.Btn_removeTemplate.UseVisualStyleBackColor = True
        '
        'lbl_TemplateID
        '
        Me.lbl_TemplateID.AutoSize = True
        Me.lbl_TemplateID.Location = New System.Drawing.Point(1265, 22)
        Me.lbl_TemplateID.Name = "lbl_TemplateID"
        Me.lbl_TemplateID.Size = New System.Drawing.Size(16, 17)
        Me.lbl_TemplateID.TabIndex = 4
        Me.lbl_TemplateID.Text = "0"
        Me.lbl_TemplateID.Visible = False
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.Location = New System.Drawing.Point(503, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(617, 23)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(939, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "ccc"
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(403, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 23)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Descriptoin:"
        '
        'lbl_HeaderName
        '
        Me.lbl_HeaderName.Location = New System.Drawing.Point(64, 22)
        Me.lbl_HeaderName.Name = "lbl_HeaderName"
        Me.lbl_HeaderName.Size = New System.Drawing.Size(316, 23)
        Me.lbl_HeaderName.TabIndex = 1
        Me.lbl_HeaderName.Text = "-"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(7, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 23)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Name:"
        '
        'TB_Desc
        '
        Me.TB_Desc.Location = New System.Drawing.Point(15, 60)
        Me.TB_Desc.Name = "TB_Desc"
        Me.TB_Desc.Size = New System.Drawing.Size(229, 22)
        Me.TB_Desc.TabIndex = 0
        '
        'TB_Amount
        '
        Me.TB_Amount.Location = New System.Drawing.Point(6, 44)
        Me.TB_Amount.Name = "TB_Amount"
        Me.TB_Amount.Size = New System.Drawing.Size(114, 22)
        Me.TB_Amount.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Description:"
        '
        'CB_Account
        '
        Me.CB_Account.FormattingEnabled = True
        Me.CB_Account.Location = New System.Drawing.Point(6, 45)
        Me.CB_Account.Name = "CB_Account"
        Me.CB_Account.Size = New System.Drawing.Size(262, 24)
        Me.CB_Account.TabIndex = 4
        '
        'CB_DrCr
        '
        Me.CB_DrCr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_DrCr.FormattingEnabled = True
        Me.CB_DrCr.Items.AddRange(New Object() {"Dr", "Cr"})
        Me.CB_DrCr.Location = New System.Drawing.Point(533, 60)
        Me.CB_DrCr.Name = "CB_DrCr"
        Me.CB_DrCr.Size = New System.Drawing.Size(74, 24)
        Me.CB_DrCr.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(530, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Sign:"
        '
        'CB_Tax
        '
        Me.CB_Tax.FormattingEnabled = True
        Me.CB_Tax.Location = New System.Drawing.Point(619, 59)
        Me.CB_Tax.Name = "CB_Tax"
        Me.CB_Tax.Size = New System.Drawing.Size(74, 24)
        Me.CB_Tax.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(616, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 17)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Tax Type:"
        '
        'Grp_LineInfo
        '
        Me.Grp_LineInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Grp_LineInfo.Controls.Add(Me.GroupBox2)
        Me.Grp_LineInfo.Controls.Add(Me.GroupBox4)
        Me.Grp_LineInfo.Controls.Add(Me.GroupBox3)
        Me.Grp_LineInfo.Controls.Add(Me.Lbl_RemoveLine)
        Me.Grp_LineInfo.Controls.Add(Me.lbl_AddLine)
        Me.Grp_LineInfo.Controls.Add(Me.Label5)
        Me.Grp_LineInfo.Controls.Add(Me.CB_Tax)
        Me.Grp_LineInfo.Controls.Add(Me.Label4)
        Me.Grp_LineInfo.Controls.Add(Me.CB_DrCr)
        Me.Grp_LineInfo.Controls.Add(Me.Label1)
        Me.Grp_LineInfo.Controls.Add(Me.TB_Desc)
        Me.Grp_LineInfo.Enabled = False
        Me.Grp_LineInfo.Location = New System.Drawing.Point(225, 74)
        Me.Grp_LineInfo.Name = "Grp_LineInfo"
        Me.Grp_LineInfo.Size = New System.Drawing.Size(1535, 99)
        Me.Grp_LineInfo.TabIndex = 3
        Me.Grp_LineInfo.TabStop = False
        Me.Grp_LineInfo.Text = "Line Information"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RB_AmtIncl)
        Me.GroupBox2.Controls.Add(Me.RB_AmtExcl)
        Me.GroupBox2.Controls.Add(Me.TB_Incl)
        Me.GroupBox2.Controls.Add(Me.TB_Amount)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TB_Tax)
        Me.GroupBox2.Location = New System.Drawing.Point(699, 14)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(346, 74)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Values"
        '
        'RB_AmtIncl
        '
        Me.RB_AmtIncl.AutoSize = True
        Me.RB_AmtIncl.Location = New System.Drawing.Point(214, 20)
        Me.RB_AmtIncl.Name = "RB_AmtIncl"
        Me.RB_AmtIncl.Size = New System.Drawing.Size(116, 21)
        Me.RB_AmtIncl.TabIndex = 21
        Me.RB_AmtIncl.Text = "Amount (Incl):"
        Me.RB_AmtIncl.UseVisualStyleBackColor = True
        '
        'RB_AmtExcl
        '
        Me.RB_AmtExcl.AutoSize = True
        Me.RB_AmtExcl.Checked = True
        Me.RB_AmtExcl.Location = New System.Drawing.Point(6, 20)
        Me.RB_AmtExcl.Name = "RB_AmtExcl"
        Me.RB_AmtExcl.Size = New System.Drawing.Size(120, 21)
        Me.RB_AmtExcl.TabIndex = 20
        Me.RB_AmtExcl.TabStop = True
        Me.RB_AmtExcl.Text = "Amount (Excl):"
        Me.RB_AmtExcl.UseVisualStyleBackColor = True
        '
        'TB_Incl
        '
        Me.TB_Incl.Location = New System.Drawing.Point(214, 44)
        Me.TB_Incl.Name = "TB_Incl"
        Me.TB_Incl.Size = New System.Drawing.Size(116, 22)
        Me.TB_Incl.TabIndex = 18
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(132, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 17)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Tax"
        '
        'TB_Tax
        '
        Me.TB_Tax.Location = New System.Drawing.Point(126, 44)
        Me.TB_Tax.Name = "TB_Tax"
        Me.TB_Tax.ReadOnly = True
        Me.TB_Tax.Size = New System.Drawing.Size(82, 22)
        Me.TB_Tax.TabIndex = 16
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.RB_ContraByDesc)
        Me.GroupBox4.Controls.Add(Me.RB_ContraByCode)
        Me.GroupBox4.Controls.Add(Me.CB_Contra)
        Me.GroupBox4.Location = New System.Drawing.Point(1051, 14)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(268, 75)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Contra Account"
        '
        'RB_ContraByDesc
        '
        Me.RB_ContraByDesc.AutoSize = True
        Me.RB_ContraByDesc.Location = New System.Drawing.Point(145, 18)
        Me.RB_ContraByDesc.Name = "RB_ContraByDesc"
        Me.RB_ContraByDesc.Size = New System.Drawing.Size(61, 21)
        Me.RB_ContraByDesc.TabIndex = 6
        Me.RB_ContraByDesc.Text = "Desc"
        Me.RB_ContraByDesc.UseVisualStyleBackColor = True
        '
        'RB_ContraByCode
        '
        Me.RB_ContraByCode.AutoSize = True
        Me.RB_ContraByCode.Checked = True
        Me.RB_ContraByCode.Location = New System.Drawing.Point(24, 18)
        Me.RB_ContraByCode.Name = "RB_ContraByCode"
        Me.RB_ContraByCode.Size = New System.Drawing.Size(62, 21)
        Me.RB_ContraByCode.TabIndex = 5
        Me.RB_ContraByCode.TabStop = True
        Me.RB_ContraByCode.Text = "Code"
        Me.RB_ContraByCode.UseVisualStyleBackColor = True
        '
        'CB_Contra
        '
        Me.CB_Contra.FormattingEnabled = True
        Me.CB_Contra.Location = New System.Drawing.Point(6, 45)
        Me.CB_Contra.Name = "CB_Contra"
        Me.CB_Contra.Size = New System.Drawing.Size(262, 24)
        Me.CB_Contra.TabIndex = 4
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RB_AccByDesc)
        Me.GroupBox3.Controls.Add(Me.RB_AccByCode)
        Me.GroupBox3.Controls.Add(Me.CB_Account)
        Me.GroupBox3.Location = New System.Drawing.Point(250, 13)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(274, 75)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Account"
        '
        'RB_AccByDesc
        '
        Me.RB_AccByDesc.AutoSize = True
        Me.RB_AccByDesc.Location = New System.Drawing.Point(145, 18)
        Me.RB_AccByDesc.Name = "RB_AccByDesc"
        Me.RB_AccByDesc.Size = New System.Drawing.Size(61, 21)
        Me.RB_AccByDesc.TabIndex = 6
        Me.RB_AccByDesc.Text = "Desc"
        Me.RB_AccByDesc.UseVisualStyleBackColor = True
        '
        'RB_AccByCode
        '
        Me.RB_AccByCode.AutoSize = True
        Me.RB_AccByCode.Checked = True
        Me.RB_AccByCode.Location = New System.Drawing.Point(24, 18)
        Me.RB_AccByCode.Name = "RB_AccByCode"
        Me.RB_AccByCode.Size = New System.Drawing.Size(62, 21)
        Me.RB_AccByCode.TabIndex = 5
        Me.RB_AccByCode.TabStop = True
        Me.RB_AccByCode.Text = "Code"
        Me.RB_AccByCode.UseVisualStyleBackColor = True
        '
        'Lbl_RemoveLine
        '
        Me.Lbl_RemoveLine.Location = New System.Drawing.Point(1420, 49)
        Me.Lbl_RemoveLine.Name = "Lbl_RemoveLine"
        Me.Lbl_RemoveLine.Size = New System.Drawing.Size(109, 40)
        Me.Lbl_RemoveLine.TabIndex = 11
        Me.Lbl_RemoveLine.Text = "Remove Line"
        Me.Lbl_RemoveLine.UseVisualStyleBackColor = True
        '
        'lbl_AddLine
        '
        Me.lbl_AddLine.Location = New System.Drawing.Point(1325, 49)
        Me.lbl_AddLine.Name = "lbl_AddLine"
        Me.lbl_AddLine.Size = New System.Drawing.Size(89, 40)
        Me.lbl_AddLine.TabIndex = 10
        Me.lbl_AddLine.Text = "Add Line"
        Me.lbl_AddLine.UseVisualStyleBackColor = True
        '
        'lsv_Names
        '
        Me.lsv_Names.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lsv_Names.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsv_Names.Location = New System.Drawing.Point(4, 13)
        Me.lsv_Names.MultiSelect = False
        Me.lsv_Names.Name = "lsv_Names"
        Me.lsv_Names.Size = New System.Drawing.Size(215, 756)
        Me.lsv_Names.SmallImageList = Me.ImageList1
        Me.lsv_Names.StateImageList = Me.ImageList1
        Me.lsv_Names.TabIndex = 4
        Me.lsv_Names.UseCompatibleStateImageBehavior = False
        Me.lsv_Names.View = System.Windows.Forms.View.List
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 200
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'FrmJournalMaint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(1773, 782)
        Me.Controls.Add(Me.lsv_Names)
        Me.Controls.Add(Me.Grp_LineInfo)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DGV_Lines)
        Me.Name = "FrmJournalMaint"
        Me.Text = "Journal Templates"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DGV_Lines, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Grp_LineInfo.ResumeLayout(False)
        Me.Grp_LineInfo.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DGV_Lines As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_removeTemplate As System.Windows.Forms.Button
    Friend WithEvents lbl_TemplateID As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lbl_HeaderName As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TB_Desc As System.Windows.Forms.TextBox
    Friend WithEvents TB_Amount As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CB_Account As System.Windows.Forms.ComboBox
    Friend WithEvents CB_DrCr As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CB_Tax As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Grp_LineInfo As System.Windows.Forms.GroupBox
    Friend WithEvents Lbl_RemoveLine As System.Windows.Forms.Button
    Friend WithEvents lbl_AddLine As System.Windows.Forms.Button
    Friend WithEvents lsv_Names As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents RB_ContraByDesc As System.Windows.Forms.RadioButton
    Friend WithEvents RB_ContraByCode As System.Windows.Forms.RadioButton
    Friend WithEvents CB_Contra As System.Windows.Forms.ComboBox
    Friend WithEvents RB_AccByDesc As System.Windows.Forms.RadioButton
    Friend WithEvents RB_AccByCode As System.Windows.Forms.RadioButton
    Friend WithEvents Btn_LoadJ As System.Windows.Forms.Button
    Friend WithEvents TB_Incl As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_Tax As System.Windows.Forms.TextBox
    Friend WithEvents Btn_New As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RB_AmtIncl As System.Windows.Forms.RadioButton
    Friend WithEvents RB_AmtExcl As System.Windows.Forms.RadioButton
End Class
