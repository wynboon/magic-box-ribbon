﻿
Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Diagnostics


Public Class frmPay_ACB_Files
    Private chckboxHeader As CheckBox
    Private ACB_File As frmPayment
    Private xSQLTable_Suppliers As DataTable
    Dim AccNo As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.AppIcon
        Me.Text = "MagicBox -> " & CStr(My.Settings.CurrentCompany) & " -> Bank Export -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
        LoadAll()
    End Sub
    Function LoadAll()
        LoadAccount()
        Fill_DGV_Suppliers()
    End Function
    Function LoadAccount()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [BankAccountNo] as Value, [segment 3 desc] as Display FROM Accounting"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID & " and FinCatCode = 60"
            sSQL = sSQL & " Order By [segment 3 desc]"
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_BankAccount.DataSource = ds.Tables(0)
            CB_BankAccount.ValueMember = "Value"
            CB_BankAccount.DisplayMember = "Display"
            CB_BankAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            CB_BankAccount.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Sub Fill_DGV_Suppliers()
        Try
            Dim xSQLAdapter_Suppliers As SqlDataAdapter
            xSQLTable_Suppliers = New DataTable()
            Dim sSQL As String

            'Columns: TransactionID,[Transaction Date],SuppID,SuppName
            sSQL = CStr("SELECT distinct RequestID,TransactionID,[Transaction date],Suppliers.SupplierID,Suppliers.SupplierName,SupplierBank.DefaultRef as [Supplier Ref],  Transactions.reference as [Company Ref], " & _
                    "   [Company Details].ID as CompanyID, [Company Details].CompanyName, " & _
                    "   SupplierBank.AccountNumber, SupplierBank.AccountName,SupplierBank.Branch, " & _
                    "   RequestedAmount,RequestedAmount as [Pay Amount]" & _
            "FROM [PaymentsRequests] " & _
            "left Join Transactions " & _
            "On transactions.TransactionID = PaymentsRequests.InvoiceTransactionID " & _
            "Left Join Suppliers " & _
            "on Suppliers.SupplierID = Transactions.SupplierID " & _
            "left join SupplierBank  on SupplierBank.SupplierID = Suppliers.SupplierID " & _
            "left join [Company Details]   on [Company Details].ID = PaymentsRequests.co_ID " & _
            "where [PaymentsRequests].Co_ID = " & My.Settings.Setting_CompanyID & _
            " and BankExported = 'False' and approvedamount <> RequestedAmount " & _
            "Order by RequestID,TransactionID,[Transaction date],Suppliers.SupplierID,Suppliers.SupplierName,SupplierBank.DefaultRef,  Transactions.reference, " & _
                    "   [Company Details].ID, [Company Details].CompanyName, " & _
                    "   SupplierBank.AccountNumber, SupplierBank.AccountName,SupplierBank.Branch, " & _
                    "   RequestedAmount")

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            xSQLAdapter_Suppliers = New SqlDataAdapter(sSQL, connection)
            xSQLAdapter_Suppliers.Fill(xSQLTable_Suppliers)

            If xSQLTable_Suppliers.Rows.Count = 0 Then
                MsgBox("There is no data to display")
                Exit Sub
            End If

            Me.DataGridView1.DataSource = xSQLTable_Suppliers

            DataGridView1.Columns("TransactionID").Visible = False
            DataGridView1.Columns("TransactionID").Visible = False
            DataGridView1.Columns("Transaction Date").DefaultCellStyle.Format = "dd MMM yyyy"
            DataGridView1.Columns("SupplierID").Visible = False
            DataGridView1.Columns("CompanyID").Visible = False
            DataGridView1.Columns("CompanyName").Visible = False
            DataGridView1.Columns("Branch").Visible = False
            DataGridView1.Columns("AccountName").Visible = False
            DataGridView1.Columns("AccountNumber").Visible = False
            DataGridView1.Rows(0).Selected = True

            Dim BadSuppliers =
                (
                        From Rows In DataGridView1.Rows.Cast(Of DataGridViewRow)()
                        Where IsDBNull(Rows.Cells("AccountName").Value)
                 ).ToList

            If Not BadSuppliers.Count = 0 Then
                ' MessageBox.Show("No Amount Edited")
                'Else

                For Each row As DataGridViewRow In BadSuppliers
                    DataGridView1.Rows(row.Index).DefaultCellStyle.ForeColor = Color.Red
                    DataGridView1.Rows(row.Index).ReadOnly = True
                Next
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 051")

        End Try

    End Sub
    Function BankBranch(ByVal AccountNo As String) As String
        Try

            Dim sSQL As String

            sSQL = "Select BranchCode from Accounting Where [BankAccountNo] = '" & AccountNo & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                BankBranch = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function CreateSBO()
        Try
            Dim Header As String
            Dim Detail As String
            Dim Contra As String
            Dim Trailer As String
            Dim i As Long
            Dim TxAmount As Decimal
            Dim TotalAmount As String
            Dim RowIndex As String

            Dim CheckedRows = (
                                From Rows In DataGridView1.Rows.Cast(Of DataGridViewRow)()
                                Where CBool(Rows.Cells("chk").Value) = True
                                ).ToList

            'WRITE THE HEADER
            Header = "SB" & DT_EffectDate.Value.ToString("yyyyMMdd") & "MagicBoxImport".ToString.PadRight(133, " ")

            If CheckedRows.Count = 0 Then
                MessageBox.Show("You have Not Chose the Invoice to be Paid Yet")
            Else

                i = 0
                TotalAmount = 0
                'LOOP AND WRITE THE DETAIL

                Dim distinctsuppliers() As String = (From row As DataGridViewRow In DataGridView1.Rows.Cast(Of DataGridViewRow)() _
                     Where row.Cells("chk").Value = True _
                     Select CStr(row.Cells("SupplierID").Value)).Distinct.ToArray

                For Each distinctsupplier In distinctsuppliers
                    TxAmount = 0
                    i = i + 1
                    RowIndex = 0
                    'Calculate the amount
                    For z As Integer = 0 To DataGridView1.RowCount - 1
                        If DataGridView1.Rows(z).Cells("SupplierID").Value = distinctsupplier And CBool(DataGridView1.Rows(z).Cells("chk").Value) = True Then
                            TxAmount += DataGridView1.Rows(z).Cells("Pay Amount").Value
                            RowIndex = z
                        End If
                    Next
                    TotalAmount = TxAmount.ToString("N2")
                    TotalAmount = Replace(Replace(Replace(TotalAmount, ".", ""), ",", ""), " ", "")
                    Dim Ref As String
                    Dim Branch As String = Trim(DataGridView1.Rows(RowIndex).Cells("Branch").Value.ToString.Substring(0, 6)).PadLeft(6, "0")
                    Dim strAccNo As String = Trim(DataGridView1.Rows(RowIndex).Cells("AccountNumber").Value.ToString.Substring(0, 13)).PadLeft(13, "0")
                    Dim strAccName As String = Trim(DataGridView1.Rows(RowIndex).Cells("AccountName").Value.ToString.Substring(0, 30)).PadRight(30, " ")
                    Ref = DataGridView1.Rows(RowIndex).Cells("Supplier Ref").Value.ToString
                    Ref = Ref.Substring(0, 30).PadRight(30, " ")
                    If Detail <> "" Then
                        Detail = Detail & vbNewLine
                    End If
                    Detail = Detail & "SD" & "001" & i.ToString.PadLeft(10, "0") & "C" & Branch & _
                        strAccNo & strAccName & _
                    " ".PadRight(16, " ") & TotalAmount.ToString.PadLeft(15, "0") & Ref & "N" & " ".ToString.PadRight(16, " ")

                Next

                'Write the Contra
                Dim BranchCode As String = BankBranch(CB_BankAccount.SelectedValue)
                Dim AccNo As String = CB_BankAccount.SelectedValue.ToString.PadLeft(13, "0")
                Contra = "SC" & "001" & " ".ToString.PadRight(30, " ") & "D" & BranchCode & AccNo & My.Settings.CurrentCompany.PadRight(30, " ") & CB_BankAccount.Text.PadRight(30, " ") & " ".ToString.PadRight(28, " ")

                'Write Trailer
                Trailer = "ST" & "0".ToString.PadRight(7, "0") & i.ToString.PadLeft(7, "0") & "001" & "0".ToString.PadRight(15, "0") & TotalAmount.ToString.PadLeft(15, "0") & " ".ToString.PadRight(94, " ")

                'For Each row As DataGridViewRow In CheckedRows
                ' UpdateRequest2Exported(CInt(row.Cells("RequestID").Value))
                ' Next

                Dim myStream As String
                Dim saveFileDialog1 As New SaveFileDialog()

                saveFileDialog1.Filter = "txt files (*.txt)|*.txt"
                saveFileDialog1.FilterIndex = 1
                saveFileDialog1.RestoreDirectory = True

                If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                    myStream = saveFileDialog1.FileName
                    If (myStream Is Nothing) Then
                        ' Code to write the stream goes here.
                        MsgBox("You did not select a valid path.")
                    End If
                End If

                Dim FILE_NAME As String = myStream
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME)
                objWriter.WriteLine(Header)
                objWriter.WriteLine(Detail)
                objWriter.WriteLine(Contra)
                objWriter.WriteLine(Trailer)

                objWriter.Close()
                If MsgBox("Payment(s) are Complete." & vbNewLine & "Would you like to open an ACB file" & " at: " & myStream & vbNewLine, _
                      MsgBoxStyle.YesNo) = MsgBoxResult.Yes And System.IO.File.Exists(myStream) Then
                    Process.Start(myStream)
                ElseIf Not System.IO.File.Exists(myStream) Then
                    MsgBox("File Does Not Exist Yet! Click Payment Transaction then enter required datails then Click Create ACB file")
                End If

                For Each row As DataGridViewRow In CheckedRows
                    ' Call UpdatePmtRequest(row.Cells("RequestID").Value, True, row.Cells("Pay Amount").Value, My.Settings.FirstName, 0, _
                    '                       True, row.Cells("RequestedAmount").Value, "Bank Export")
                    DataGridView1.Rows.RemoveAt(row.Index)
                Next

            End If

        Catch ex As Exception
            MsgBox("Error creating Standard Bank Online file. Error was: " & ex.Message)
        End Try
    End Function
    Function CreateACB2()
        Try
            Dim InstallHeader As String
            Dim UserHeader As String
            Dim Detail As String
            Dim Contra As String
            Dim UserTrailer As String
            Dim InstallTrailer As String
            Dim i As Long
            Dim TxAmount As Double
            Dim TotalAmount As String
            Dim RowIndex As String

            Dim CheckedRows = (
                                From Rows In DataGridView1.Rows.Cast(Of DataGridViewRow)()
                                Where CBool(Rows.Cells("chk").Value) = True
                                ).ToList

            If CheckedRows.Count = 0 Then
                MessageBox.Show("You have Not Chose the Invoice to be Paid Yet")
            Else
                'WRITE THE HEADER
                InstallHeader = "02".ToString.PadRight(180, "0")
                UserHeader = "04".ToString.PadRight(180, "0")

                i = 0
                TotalAmount = 0
                'LOOP AND WRITE THE DETAIL
                Dim distinctsuppliers() As String = (From row As DataGridViewRow In DataGridView1.Rows.Cast(Of DataGridViewRow)() _
                     Where CBool(row.Cells("chk").Value) = True
                     Select CStr(row.Cells("SupplierID").Value)).Distinct.ToArray

                For Each distinctsupplier In distinctsuppliers
                    TxAmount = 0
                    i = i + 1
                    RowIndex = 0
                    'Calculate the amount
                    For z As Integer = 0 To DataGridView1.RowCount - 1
                        If DataGridView1.Rows(z).Cells("SupplierID").Value = distinctsupplier And CBool(DataGridView1.Rows(z).Cells("chk").Value) = True Then
                            TxAmount += DataGridView1.Rows(z).Cells("Pay Amount").Value
                            RowIndex = z
                        End If
                        TotalAmount = TxAmount.ToString("N2")
                        TotalAmount = Replace(Replace(Replace(TotalAmount, ".", ""), ",", ""), " ", "")

                    Next
                    Dim RecIdent As String = "10"
                    Dim UserBranch As String = BankBranch(CB_BankAccount.SelectedValue).Substring(0, 6).PadLeft(6, "0")
                    Dim UserAccount As String = AccNo.ToString.PadLeft(11, "0").Substring(0, 11).PadLeft(11, "0")
                    Dim UserFiller As String = "0".ToString.PadRight(4, "0")
                    Dim UserSeq As String = i.ToString.PadLeft(6, "0")
                    Dim RecipBranch As String = Trim(DataGridView1.Rows(RowIndex).Cells("Branch").Value.ToString.Substring(0, 6)).PadLeft(6, "0")
                    Dim RecipAccount As String = Trim(DataGridView1.Rows(RowIndex).Cells("AccountNumber").Value.ToString.Substring(0, 11)).PadLeft(11, "0")
                    Dim AccType As String = "1"
                    Dim Amount As String = TotalAmount.ToString.PadLeft(11, "0")
                    Dim BefRefFiller As String = "            "
                    Dim Ref As String
                    Ref = DataGridView1.Rows(RowIndex).Cells("Supplier Ref").Value.ToString
                    Ref = Ref.Substring(0, 20).PadRight(20, " ")
                    Dim AfterRefFiller As String = " ".ToString.PadRight(10, " ")
                    Dim RecipAccName As String = DataGridView1.Rows(RowIndex).Cells("SupplierName").Value.ToString.PadRight(15, " ").Substring(0, 15)
                    Dim PreFinalFiller As String = " ".ToString.PadRight(15, " ")
                    Dim FinalFiller As String = " ".ToString.PadRight(50, " ")

                        If Detail <> "" Then
                            Detail = Detail & vbNewLine
                        End If
                    Detail = Detail & RecIdent & UserBranch & UserAccount & UserFiller & UserSeq & RecipBranch _
                        & RecipAccount & AccType & Amount & BefRefFiller & Ref & AfterRefFiller & RecipAccName & PreFinalFiller & FinalFiller
                Next

                'Write the Contra
                Contra = "12".ToString.PadRight(58, "0") & DT_EffectDate.Value.ToString("yyMMdd") & "0".ToString.PadRight(116, "0")

                'Write UserTrailer
                UserTrailer = "92".ToString.PadRight(180, "0")

                'Write InstallTrailer
                InstallTrailer = "94".ToString.PadRight(180, "0")

                ' For Each row As DataGridViewRow In CheckedRows
                ' UpdateRequest2Exported(CInt(row.Cells("RequestID").Value))
                ' Next

                Dim myStream As String
                Dim saveFileDialog1 As New SaveFileDialog()

                saveFileDialog1.Filter = "txt files (*.txt)|*.txt"
                saveFileDialog1.FilterIndex = 1
                saveFileDialog1.RestoreDirectory = True

                If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                    myStream = saveFileDialog1.FileName
                    If (myStream Is Nothing) Then
                        ' Code to write the stream goes here.
                        MsgBox("You did not select a valid path.")
                    End If
                End If

                Dim FILE_NAME As String = myStream
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME)
                objWriter.WriteLine(InstallHeader)
                objWriter.WriteLine(UserHeader)
                objWriter.WriteLine(Detail)
                objWriter.WriteLine(Contra)
                objWriter.WriteLine(UserTrailer)
                objWriter.WriteLine(InstallTrailer)

                objWriter.Close()
                If MsgBox("Payment(s) are Complete." & vbNewLine & "Would you like to open an ACB file" & " at: " & myStream & vbNewLine, _
                      MsgBoxStyle.YesNo) = MsgBoxResult.Yes And System.IO.File.Exists(myStream) Then
                    Process.Start(myStream)
                ElseIf Not System.IO.File.Exists(myStream) Then
                    MsgBox("File Does Not Exist Yet! Click Payment Transaction then enter required datails then Click Create ACB file")
                End If

                For Each row As DataGridViewRow In CheckedRows
                    DataGridView1.Rows.RemoveAt(row.Index)
                Next
            End If
        Catch ex As Exception
            MsgBox("Error Creating ACB file. Error was: " & ex.Message)
        End Try

    End Function


    Function CreateACB()

        Const MAX_CONTRA As Integer = 200
        Dim Bulk(MAX_CONTRA) As STD_BulkTransactin

        Try
            ACB_File = New frmPayment(xSQLTable_Suppliers, 1)
            Dim EditedRows =
                (
                        From Rows In DataGridView1.Rows.Cast(Of DataGridViewRow)()
                        Where CInt(Rows.Cells("RequestedAmount").Value) <> CInt(Rows.Cells("Pay Amount").Value)
                 ).ToList

            If Not EditedRows.Count = 0 Then
                ' MessageBox.Show("No Amount Edited")
                'Else

                For Each row As DataGridViewRow In EditedRows
                    If DataGridView1.Rows(row.Index).Cells("chk").Value = True Then
                        DataGridView1.Rows(row.Index).DefaultCellStyle.ForeColor = Color.Red
                        DataGridView1.Rows(row.Index).DefaultCellStyle.Font = New Font("Broadway", 10, FontStyle.Regular)
                        DataGridView1.Rows(row.Index).Cells("chk").Value = False
                    End If
                Next
            End If
            Dim CheckedRows =
                            (
                                From Rows In DataGridView1.Rows.Cast(Of DataGridViewRow)()
                                Where CBool(Rows.Cells("chk").Value) = True
                            ).ToList
            If CheckedRows.Count = 0 Then
                MessageBox.Show("You have Not Chose the Invoice to be Paid Yet")
            Else
                Dim tot As Integer = 0
                Dim invCount As Integer = 0
                Dim sb As New System.Text.StringBuilder
                DataGridView1.CurrentCell = CheckedRows.First.Cells("chk")
                ACB_File.Headers_nTrailers_nContra(CDate(DT_EffectDate.Text))
                For Each row As DataGridViewRow In CheckedRows
                    Bulk(invCount) = New STD_BulkTransactin()
                    Bulk(invCount).UserAccountBranch = BankBranch(CB_BankAccount.SelectedValue)
                    Bulk(invCount).UserAccountNumber = CB_BankAccount.SelectedValue
                    Bulk(invCount).UserRef = "MBEFT" & row.Cells("TransactionID").Value.ToString & "| " & row.Cells("Supplier Ref").Value.ToString
                    Bulk(invCount).CreditorBranch = row.Cells("Branch").Value.ToString
                    Bulk(invCount).CreditorAccountNumber = row.Cells("AccountNumber").Value.ToString
                    Bulk(invCount).CreditorAccountType = "1"
                    Bulk(invCount).CreditorRef = row.Cells("AccountName").Value.ToString()
                    Bulk(invCount).PayAmount = row.Cells("Pay Amount").Value.ToString()

                    invCount += 1
                    tot += CInt(row.Cells("Pay Amount").Value)
                    sb.AppendLine(invCount & ". " & row.Cells("SupplierName").Value.ToString)
                    DataGridView1.Rows(row.Index).Selected = True
                    UpdateRequest2Exported(CInt(row.Cells("RequestID").Value))
                Next


                ACB_File.subCeateACB_File_Click(Bulk)

                For Each row As DataGridViewRow In CheckedRows
                    DataGridView1.Rows.RemoveAt(row.Index)
                Next
            End If
            If DataGridView1.Rows.Count = 0 Then Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Function
    Private cellInfo As String

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If DataGridView1.CurrentCell.Equals(DataGridView1.Rows(DataGridView1.CurrentCell.RowIndex).Cells("Pay Amount")) Then

            cellInfo = DataGridView1.CurrentCell.FormattedValue.ToString
            MsgBox("saved :) " & cellInfo)
        ElseIf DataGridView1.CurrentCell.Equals(DataGridView1.Rows(DataGridView1.CurrentCell.RowIndex).Cells("chk")) Then
            CheckBox1.CheckState = False
            DataGridView1.Rows(DataGridView1.CurrentCell.RowIndex).Selected = True
            Dim CheckedRows =
                           (
                               From Rows In DataGridView1.Rows.Cast(Of DataGridViewRow)()
                               Where CBool(Rows.Cells("chk").Value) = True
                           ).ToList
            For Each row As DataGridViewRow In CheckedRows
                DataGridView1.Rows(row.Index).Selected = True
            Next


        End If
    End Sub
    Private Sub DataGridView1_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If DataGridView1.Rows.Count = 0 Then Exit Sub
        If IsNothing(DataGridView1.CurrentCell) Then Exit Sub

        If DataGridView1.CurrentCell.Equals(DataGridView1.Rows(DataGridView1.CurrentCell.RowIndex).Cells("Pay Amount")) Then
            If CDbl(cellInfo) < CDbl(DataGridView1.CurrentCell.FormattedValue.ToString) Then
                MsgBox("Amount paid must be less o equal to amount Due ")
                DataGridView1.CurrentCell.Value = cellInfo
            End If
        End If
    End Sub

    Private Sub CB_BankAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_BankAccount.SelectedIndexChanged

        Dim LayoutType As String
        If CB_BankAccount.SelectedValue.ToString = "System.Data.DataRowView" Then Exit Sub
        If IsNothing(CB_BankAccount.SelectedValue) Then
        Else
            If IsDBNull(CB_BankAccount.SelectedValue) Then
                AccNo = ""
            Else
                AccNo = CB_BankAccount.SelectedValue
            End If
            If BankBranch(AccNo) = "" Then
                MsgBox("This bank account does not have a Branch code set up. Update the banking records in the Accounting screen and then return.", MsgBoxStyle.Critical, "No Branch Code")
                Me.Btn_Pay.Enabled = False
            Else
                Me.Btn_Pay.Enabled = True
            End If
            LayoutType = BankLayout_fromAccCode(AccNo)
            If Not IsDBNull(LayoutType) Then
                LBL_LayoutType.Text = LayoutType
            Else
                LBL_LayoutType.Text = ""
            End If
            If LBL_LayoutType.Text = "" Then
                MsgBox("Please note that there is no bank export layout set up for this account. Please edit the acount in the 'Accounting' screen.", MsgBoxStyle.Critical, "No Layout")
            End If

        End If
    End Sub

    Private Sub Btn_Accounts_Click(sender As Object, e As EventArgs) Handles Btn_Accounts.Click
        Dim FrmAccounts As New frmAccountMaintenance
        FrmAccounts.ShowDialog()
        LoadAll()
    End Sub

    Private Sub Btn_SupplierMaster_Click(sender As Object, e As EventArgs) Handles Btn_SupplierMaster.Click
        Dim FrmSuppliers As New frmSuppliers
        FrmSuppliers.ShowDialog()
        LoadAll()
    End Sub
    Private Sub Btn_Pay_Click(sender As Object, e As EventArgs) Handles Btn_Pay.Click
        If LBL_LayoutType.Text = "ACB" Then
            Call CreateACB2()
        ElseIf LBL_LayoutType.Text = "SBO" Then
            Call CreateSBO()
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If sender.Checked Then
            For Each row As DataGridViewRow In DataGridView1.Rows
                If row.Index <> DataGridView1.RowCount Then
                    DataGridView1.Rows(row.Index).Cells("chk").Value = True
                End If
            Next
        Else
            For Each row As DataGridViewRow In DataGridView1.Rows
                DataGridView1.Rows(row.Index).Cells("chk").Value = False
            Next
        End If
    End Sub
End Class

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''' <summary>
''' Class that create a ACB file
''' </summary>
''' <remarks></remarks>

Public Class frmPayment

    Private Instal_ As Installation
    Private User_ As User_Payer
    Private STD_Transaction As Payment_Transaction
    Private Contra As Contract_Date
    Private accInfo As DataTable
    Private selectedAcc As Integer
    Private requiredField As Boolean
    Const SIZE_ As Integer = 20
    Dim missedTxtBx(SIZE_) As Color
    Private FilePath_ As String

    Public Sub New(ByVal accInfo As DataTable, Optional chosenAcc As Integer = 0)
        '  InitializeComponent()
        selectedAcc = chosenAcc
        Me.accInfo = accInfo
        requiredField = False

        For Each col In missedTxtBx
            col = Color.Black
        Next

        Dim myStream As String
        Dim saveFileDialog1 As New SaveFileDialog()

        saveFileDialog1.Filter = "txt files (*.txt)|*.txt"
        saveFileDialog1.FilterIndex = 1
        saveFileDialog1.RestoreDirectory = True

        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
            myStream = saveFileDialog1.FileName
            If (myStream Is Nothing) Then
                ' Code to write the stream goes here.
                MsgBox("You did not select a valid path.")
            End If
        End If


        FilePath_ = myStream


    End Sub

    'implements the INSTALLATION HEADER AND TRAILER RECORD
    Private Structure Installation

        Private ID_Header As Integer ' 2 ID characters 
        Private ID_Trailer As Integer ' 2 ID characters
        Private Blanks_Header As String '179 blanks = 181 caharaters
        Private Blanks_Trailer As String '178 blanks = 181 caharaters

        'get records identifier for header and trailer 
        Public Sub New(ByVal Header_RecordID As Integer, ByVal Trailer_RecordID As Integer)
            ID_Header = Header_RecordID
            ID_Trailer = Trailer_RecordID
        End Sub

        'complete installation header to be posted on ACD File with 181 characters
        Public ReadOnly Property I_Header As String
            Get
                SetBlanks()
                Return ID_Header.ToString("D2") + Blanks_Header
            End Get
        End Property

        'complete installation trailler to be posted on ACD File with 181 characters
        Public ReadOnly Property I_Trailer As String
            Get
                SetBlanks()
                Return ID_Trailer.ToString("D2") + Blanks_Trailer
            End Get
        End Property

        Private Sub SetBlanks()
            Blanks_Header = StrDup(178, "0") '178 blanks for header
            Blanks_Trailer = StrDup(178, "0") '178 blanks for trailer
        End Sub

    End Structure ' the end of installation 

    'implements the USER HEADER AND TRAILER RECORD
    Private Structure User_Payer

        Private ID_Header As Integer ' 2 ID characters 
        Private ID_Trailer As Integer ' 2 ID characters
        Private HashTotal As Integer ' 12 digits
        Private H_BlankSize As Integer '178 zeros to fill blanks 
        Private BlanksBeforeHash As Integer  '70 zeros to fill blanks
        Private BlanksAfterHash As Integer  '96 zeros to fill blanks

        'get records identifier for header and trailer 
        Public Sub New(ByVal zerosFillsBlanks As Boolean, Optional ByVal Header_RecordID As Integer = 4, Optional ByVal Trailer_RecordID As Integer = 92)
            ID_Header = Header_RecordID
            ID_Trailer = Trailer_RecordID

            H_BlankSize = 178 '178 zeros to fill blanks 
            BlanksBeforeHash = 70 '70 zeros to fill blanks
            BlanksAfterHash = 96 '96 zeros to fill blanks
        End Sub

        'complete installation header to be posted on ACD File with 181 characters
        Public ReadOnly Property Header As String
            Get
                Return ID_Header.ToString("D2") + addBlanks(H_BlankSize)
            End Get
        End Property

        'complete installation trailler to be posted on ACD File with 181 characters
        Public ReadOnly Property Trailer As String
            Get
                Return ID_Trailer.ToString("D2") + addBlanks(BlanksBeforeHash) _
                    + generate_HashTotal(0).ToString("D12") + addBlanks(BlanksAfterHash)
            End Get
        End Property

        Private Function generate_HashTotal(ByRef hashTot As Long) As Long
            Return hashTot
        End Function

        Private Function addBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, "0") 'zeros to fiil up the blanks
        End Function

    End Structure ' the end of user 

    'implements the STANDARD TRANSACTION RECORD
    Private Structure Payment_Transaction

        Private ID As Integer
        Private Branch As Integer
        Private Account As ULong
        Private ZerosBeforeUSN As Integer
        Private USN As Integer
        Private HBranch As Integer
        Private HAccount As ULong
        Private HAccType As Char
        Private Amount As Double
        Private ZerosBeforeTax As Integer
        Private Tax As Integer
        Private ZerosAfterTax As Integer
        Private ZerosBeforeReci_Ref As Integer
        Private Recipient_Ref As String
        Private SpacesBtwnRefs As Integer
        Private Owner_Ref As String
        Private SpacesBeforeNonSTD_Acc As Integer
        Private NonSTD_Acc As ULong
        Private ZerosAfterNonSTD_Acc As Integer

        'get transaction details 1--3---3----2-----2-----1-->>>12 parameters
        Public Sub New(ByVal tranID As Integer _
            , ByVal Branch As Integer, ByVal Account As ULong, ByVal USN As Integer _
            , ByVal HBranch As Integer, ByVal HAccount As ULong, ByVal HAccType As Char _
            , ByVal Amount As Double, ByVal Tax As Integer _
            , ByVal Recipient_Ref As String, ByVal Owner_Ref As String _
            , ByVal NonSTD_Acc As ULong) 'parameters


            Me.ID = tranID
            Me.Branch = Branch
            Me.Account = Account
            Me.USN = USN
            Me.HBranch = HBranch
            Me.HAccount = HAccount
            Me.HAccType = HAccType
            Me.Amount = Amount
            Me.Tax = Tax
            Me.Recipient_Ref = Recipient_Ref
            Me.Owner_Ref = Owner_Ref
            Me.NonSTD_Acc = NonSTD_Acc

            ZerosBeforeUSN = 4 'number of zeros to fill blanks
            ZerosBeforeTax = 8 'number of zeros to fill blanks
            ZerosAfterTax = 2 'number of zeros to fill blanks
            ZerosBeforeReci_Ref = 1 'number of zeros to fill blanks
            SpacesBtwnRefs = 10 'number of spaces to fill blanks
            SpacesBeforeNonSTD_Acc = 15 'number of spaces to fill blanks
            ZerosAfterNonSTD_Acc = 30 'number of zeros to fill blanks

        End Sub



        'keep the size a fixed length
        Private Function strSizeControl(ByRef setance As String, ByVal size As Integer) As String

            If setance.Length > size Then ' cut other charater above given size
                setance = setance.Remove(size, setance.Length - size)
            Else 'fill the extra space with blank space character
                setance = setance & StrDup(size - setance.Length, " ")
            End If

            Return setance
        End Function

        'it generates Zeros (0) to fill up blanks
        Private Function addZerosBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, "0") 'zeros to fiil up the blanks
        End Function

        'it generates spaces to fill up blanks
        Private Function addSpaceBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, " ") 'space character to fiil up the blanks
        End Function

        'removes the dot in a double number and returns a string with both
        Private Function removeDotForCent(ByRef AmountDbl As Double) As String
            Dim parts As String() = (AmountDbl.ToString("f2")).Split(New Char() {"."c})
            Return (CInt(parts(0))).ToString("D9") & parts(1)
        End Function

        'complete installation contra details to be posted on ACD File with 180 characters
        Public Overrides Function ToString() As String
            '-------
            'Size***2***6,11,[4],6***6,11,1***11***[8],1,[2]*****[1],20*****[10],"15******[15],"20,[30]
            '-------1-------4------3--------1--------3---------2--------2-------3
            Return ID.ToString("D2") &
               strSizeControl(Branch.ToString("D6"), 6) & Account.ToString("D11") & addZerosBlanks(ZerosBeforeUSN) & USN.ToString("D6") &
                HBranch.ToString("D6") & HAccount.ToString("D11") & HAccType.ToString() &
                removeDotForCent(Amount) &
                addZerosBlanks(ZerosBeforeTax) & Tax.ToString("D1") & addZerosBlanks(ZerosAfterTax) &
                addZerosBlanks(ZerosBeforeReci_Ref) & strSizeControl(Recipient_Ref, 20) &
                addSpaceBlanks(SpacesBtwnRefs) & strSizeControl(Owner_Ref, 15) &
                addSpaceBlanks(SpacesBeforeNonSTD_Acc) & NonSTD_Acc.ToString("D20") & addZerosBlanks(ZerosAfterNonSTD_Acc)
        End Function

    End Structure ' the end of transaction

    'implements the CONTRA RECORD
    Private Structure Contract_Date

        Private ID As Integer ' 2 ID characters 
        Private Action_Date As Date ' 6 date characters 
        Private BlanksBeforeDate As Integer  '56 zeros to fill blanks
        Private BlanksAfterDate As Integer  '116 zeros to fill blanks

        'get records identifier and action date 
        Public Sub New(ByVal zerosFillsBlanks As Boolean, ByVal TransDate As Date, Optional ByVal RecordID As Integer = 12)
            ID = RecordID
            Action_Date = TransDate

            BlanksBeforeDate = 56 '56 zeros to fill blanks
            BlanksAfterDate = 116 '116 zeros to fill blanks
        End Sub

        'it generates Zeros (0) to fill up blanks
        Private Function addBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, "0") 'zeros to fiil up the blanks
        End Function

        'convert date into numeric yyMMdd -> 991231
        Private Function strDate_yymmdd(ByRef dateFormat As Date) As String
            If dateFormat.ToString = "" Then
                Return (Date.Now).ToString("yyMMdd") 'if no transcation date, then use today
            End If

            Return dateFormat.ToString("yyMMdd") 'yy & mm & dd 'with a required date format

        End Function

        'complete installation contra details to be posted on ACD File with 180 characters
        Public Overrides Function ToString() As String
            Return ID.ToString("D2") + addBlanks(BlanksBeforeDate) +
            strDate_yymmdd(Action_Date) + addBlanks(BlanksAfterDate)
        End Function
    End Structure ' the end of contra 


    Private Sub createTextFile()

        Dim FILE_NAME As String = FilePath_


        Dim objWriter As New System.IO.StreamWriter(FILE_NAME)

        objWriter.WriteLine(Instal_.I_Header)

        objWriter.WriteLine(User_.Header)

        'objWriter.WriteLine(STD_Transaction)

        objWriter.WriteLine(bulk)

        objWriter.WriteLine(Contra)

        objWriter.WriteLine(User_.Trailer)

        objWriter.WriteLine(Instal_.I_Trailer)


        objWriter.Close()
        OpenACD_File()

    End Sub
    Public Sub Headers_nTrailers_nContra(ByVal ActionDate As Date)

        Instal_ = New Installation(2, 94)
        User_ = New User_Payer(True)
        Contra = New Contract_Date(False, ActionDate)
    End Sub

    Private bulk As String
    Public Sub subCeateACB_File_Click(ByVal banch() As STD_BulkTransactin)
        'STD_Transaction = vbNull

        For Each trans In banch
            STD_Transaction = New Payment_Transaction(10, CInt(EmptyTxtBoxControl(trans.UserAccountBranch, 1)), CULng(EmptyTxtBoxControl(trans.UserAccountNumber, 0)), 1, _
                            CInt(EmptyTxtBoxControl(trans.CreditorBranch, 3)), CULng(EmptyTxtBoxControl(trans.CreditorAccountNumber, 2)), "1", _
                            CDbl(EmptyTxtBoxControl(trans.PayAmount, 4)), 0, _
                            EmptyTxtBoxControlTxt(trans.CreditorRef), EmptyTxtBoxControlTxt(trans.UserRef), 0L)


            If trans.Equals(banch.Last) Then
                Exit For
            ElseIf trans.Equals(banch.First) Then
                bulk = STD_Transaction.ToString
            Else
                bulk = bulk & vbNewLine & STD_Transaction.ToString
            End If

        Next
        createTextFile()
    End Sub

    Private Function EmptyTxtBoxControlTxt(ByVal txtBx As String) As String
        If txtBx = vbNullString Then
            Return " "
        End If
        Return txtBx
    End Function

    Private Function EmptyTxtBoxControl(ByVal txtBx As String, ByVal index As Integer) As String
        If txtBx = vbNullString Then
            missedTxtBx(index) = Color.Red
            requiredField = True
            Return "0"
        End If
        Return txtBx
    End Function

    Public Sub OpenACD_File()

        If MsgBox("Payment(s) are Complete." & vbNewLine & "Would you like to open an ACB file" & " at: " & FilePath_ & vbNewLine, _
                  MsgBoxStyle.YesNo) = MsgBoxResult.Yes And System.IO.File.Exists(FilePath_) Then
            Process.Start(FilePath_)
        ElseIf Not System.IO.File.Exists(FilePath_) Then
            MsgBox("File Does Not Exist Yet! Click Payment Transaction then enter required datails then Click Create ACB file")
        End If
    End Sub
End Class