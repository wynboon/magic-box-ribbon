﻿
Imports Microsoft.Office.Interop.Outlook

Module modEmail

    Public Sub oSendEmail(ByVal oTo As String, ByVal oCc As String, ByVal oSubject As String, ByVal oBody As String, ByVal oAttachment As String)

        Try
            ' Create an Outlook application.
            Dim oOutApp As New Microsoft.Office.Interop.Outlook.Application
            oOutApp = New Microsoft.Office.Interop.Outlook.Application

            Dim ns As Microsoft.Office.Interop.Outlook.NameSpace = oOutApp.GetNamespace("MAPI")

            Dim f As Microsoft.Office.Interop.Outlook.MAPIFolder
            f = ns.GetDefaultFolder(OlDefaultFolders.olFolderInbox)

            System.Threading.Thread.Sleep(3000) 'a bit of startup grace time.

            ' Create a new MailItem.
            Dim oMsg As Microsoft.Office.Interop.Outlook.MailItem
            oMsg = oOutApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem)

            oMsg.Subject = oSubject
            oMsg.Body = oBody

            ' TODO: Replace with a valid e-mail address.
            oMsg.To = oTo
            oMsg.CC = oCc

            ' Add an attachment
            Dim sSource As String = oAttachment 'eg "C:\Temp\Hello.txt"

            'Dim sBodyLen As String = oMsg.Body.Length
            Dim sBodyLen As Integer = oBody.Length


            Dim oAttachs As Microsoft.Office.Interop.Outlook.Attachments
            Dim oAttach As Microsoft.Office.Interop.Outlook.Attachment
            'only attach if there is something there
            If oAttachment <> "" And oAttachment <> Nothing Then
                oAttachs = oMsg.Attachments
                If oAttachment <> Nothing And oAttachment <> "" And System.IO.File.Exists(oAttachment) = True Then
                    oAttach = oAttachs.Add(sSource, , sBodyLen + 1)
                    'oAttach = oAttachs.Add(sSource, , sBodyLen + 1, sDisplayName)
                End If
            End If

            ' Send
            If Globals.Ribbons.Ribbon1.chkCheckEmail.Checked = True Then
                'Manual
                oMsg.Display()
                'Note no cleanup code
            Else
                oMsg.Send()
                oOutApp = Nothing
                oMsg = Nothing
                oAttach = Nothing
                oAttachs = Nothing

            End If

            ' Clean up

        Catch
            MsgBox("An error occurred in trying to send an email! " & Err.Description)
        End Try
    End Sub
End Module
