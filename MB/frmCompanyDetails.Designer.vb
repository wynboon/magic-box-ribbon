﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompanyDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompanyDetails))
        Me.Button_Close = New System.Windows.Forms.Button()
        Me.Button_Save = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnNewCompany = New System.Windows.Forms.Button()
        Me.txtLegalEntity = New System.Windows.Forms.TextBox()
        Me.txtCompanyVat = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtCompanyName = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDirectors = New System.Windows.Forms.TextBox()
        Me.txtRegNo = New System.Windows.Forms.TextBox()
        Me.txtPAddress4 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtPAddress3 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPAddress2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPAddress1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtEmailAddress2 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtEmailAddress1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTelephone2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTelphone1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button_Close
        '
        Me.Button_Close.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_Close.ForeColor = System.Drawing.Color.Navy
        Me.Button_Close.Location = New System.Drawing.Point(437, 578)
        Me.Button_Close.Name = "Button_Close"
        Me.Button_Close.Size = New System.Drawing.Size(95, 23)
        Me.Button_Close.TabIndex = 0
        Me.Button_Close.Text = "&Close"
        Me.Button_Close.UseVisualStyleBackColor = True
        '
        'Button_Save
        '
        Me.Button_Save.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_Save.ForeColor = System.Drawing.Color.Navy
        Me.Button_Save.Location = New System.Drawing.Point(385, 497)
        Me.Button_Save.Name = "Button_Save"
        Me.Button_Save.Size = New System.Drawing.Size(113, 23)
        Me.Button_Save.TabIndex = 13
        Me.Button_Save.Text = "&Save Changes"
        Me.Button_Save.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Navy
        Me.Label14.Location = New System.Drawing.Point(12, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(190, 20)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Company Details Editing"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Navy
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Navy
        Me.Label13.Location = New System.Drawing.Point(84, 36)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(461, 1)
        Me.Label13.TabIndex = 17
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnNewCompany)
        Me.GroupBox1.Controls.Add(Me.txtLegalEntity)
        Me.GroupBox1.Controls.Add(Me.txtCompanyVat)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtCompanyName)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtDirectors)
        Me.GroupBox1.Controls.Add(Me.txtRegNo)
        Me.GroupBox1.Controls.Add(Me.Button_Save)
        Me.GroupBox1.Controls.Add(Me.txtPAddress4)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtPAddress3)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtPAddress2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtPAddress1)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtEmailAddress2)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtEmailAddress1)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtTelephone2)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtTelphone1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Navy
        Me.GroupBox1.Location = New System.Drawing.Point(19, 46)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(513, 526)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Details"
        '
        'btnNewCompany
        '
        Me.btnNewCompany.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNewCompany.ForeColor = System.Drawing.Color.Navy
        Me.btnNewCompany.Location = New System.Drawing.Point(6, 503)
        Me.btnNewCompany.Name = "btnNewCompany"
        Me.btnNewCompany.Size = New System.Drawing.Size(69, 23)
        Me.btnNewCompany.TabIndex = 40
        Me.btnNewCompany.Text = "&New Company"
        Me.btnNewCompany.UseVisualStyleBackColor = True
        Me.btnNewCompany.Visible = False
        '
        'txtLegalEntity
        '
        Me.txtLegalEntity.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtLegalEntity.ForeColor = System.Drawing.Color.Black
        Me.txtLegalEntity.Location = New System.Drawing.Point(165, 42)
        Me.txtLegalEntity.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtLegalEntity.Name = "txtLegalEntity"
        Me.txtLegalEntity.Size = New System.Drawing.Size(333, 24)
        Me.txtLegalEntity.TabIndex = 1
        '
        'txtCompanyVat
        '
        Me.txtCompanyVat.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtCompanyVat.ForeColor = System.Drawing.Color.Black
        Me.txtCompanyVat.Location = New System.Drawing.Point(165, 84)
        Me.txtCompanyVat.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtCompanyVat.Name = "txtCompanyVat"
        Me.txtCompanyVat.Size = New System.Drawing.Size(333, 24)
        Me.txtCompanyVat.TabIndex = 3
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Navy
        Me.Label15.Location = New System.Drawing.Point(6, 449)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(78, 20)
        Me.Label15.TabIndex = 39
        Me.Label15.Text = "Directors."
        '
        'txtCompanyName
        '
        Me.txtCompanyName.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtCompanyName.ForeColor = System.Drawing.Color.Black
        Me.txtCompanyName.Location = New System.Drawing.Point(165, 21)
        Me.txtCompanyName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(333, 24)
        Me.txtCompanyName.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(6, 377)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(119, 20)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "P. Adress Line 4"
        '
        'txtDirectors
        '
        Me.txtDirectors.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtDirectors.ForeColor = System.Drawing.Color.Black
        Me.txtDirectors.Location = New System.Drawing.Point(165, 448)
        Me.txtDirectors.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtDirectors.Multiline = True
        Me.txtDirectors.Name = "txtDirectors"
        Me.txtDirectors.Size = New System.Drawing.Size(333, 43)
        Me.txtDirectors.TabIndex = 12
        '
        'txtRegNo
        '
        Me.txtRegNo.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtRegNo.ForeColor = System.Drawing.Color.Black
        Me.txtRegNo.Location = New System.Drawing.Point(165, 63)
        Me.txtRegNo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtRegNo.Name = "txtRegNo"
        Me.txtRegNo.Size = New System.Drawing.Size(333, 24)
        Me.txtRegNo.TabIndex = 2
        '
        'txtPAddress4
        '
        Me.txtPAddress4.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtPAddress4.ForeColor = System.Drawing.Color.Black
        Me.txtPAddress4.Location = New System.Drawing.Point(165, 383)
        Me.txtPAddress4.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPAddress4.Multiline = True
        Me.txtPAddress4.Name = "txtPAddress4"
        Me.txtPAddress4.Size = New System.Drawing.Size(333, 63)
        Me.txtPAddress4.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(6, 318)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(119, 20)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "P. Adress Line 3"
        '
        'txtPAddress3
        '
        Me.txtPAddress3.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtPAddress3.ForeColor = System.Drawing.Color.Black
        Me.txtPAddress3.Location = New System.Drawing.Point(165, 318)
        Me.txtPAddress3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPAddress3.Multiline = True
        Me.txtPAddress3.Name = "txtPAddress3"
        Me.txtPAddress3.Size = New System.Drawing.Size(333, 63)
        Me.txtPAddress3.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Navy
        Me.Label7.Location = New System.Drawing.Point(6, 253)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(119, 20)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "P. Adress Line 2"
        '
        'txtPAddress2
        '
        Me.txtPAddress2.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtPAddress2.ForeColor = System.Drawing.Color.Black
        Me.txtPAddress2.Location = New System.Drawing.Point(165, 253)
        Me.txtPAddress2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPAddress2.Multiline = True
        Me.txtPAddress2.Name = "txtPAddress2"
        Me.txtPAddress2.Size = New System.Drawing.Size(333, 63)
        Me.txtPAddress2.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Navy
        Me.Label6.Location = New System.Drawing.Point(6, 192)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 20)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "P. Adress Line 1"
        '
        'txtPAddress1
        '
        Me.txtPAddress1.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtPAddress1.ForeColor = System.Drawing.Color.Black
        Me.txtPAddress1.Location = New System.Drawing.Point(165, 189)
        Me.txtPAddress1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPAddress1.Multiline = True
        Me.txtPAddress1.Name = "txtPAddress1"
        Me.txtPAddress1.Size = New System.Drawing.Size(333, 63)
        Me.txtPAddress1.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Navy
        Me.Label10.Location = New System.Drawing.Point(6, 171)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(110, 20)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Email Adress 2"
        '
        'txtEmailAddress2
        '
        Me.txtEmailAddress2.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtEmailAddress2.ForeColor = System.Drawing.Color.Black
        Me.txtEmailAddress2.Location = New System.Drawing.Point(165, 168)
        Me.txtEmailAddress2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtEmailAddress2.Name = "txtEmailAddress2"
        Me.txtEmailAddress2.Size = New System.Drawing.Size(333, 24)
        Me.txtEmailAddress2.TabIndex = 7
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Navy
        Me.Label17.Location = New System.Drawing.Point(6, 150)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(110, 20)
        Me.Label17.TabIndex = 28
        Me.Label17.Text = "Email Adress 1"
        '
        'txtEmailAddress1
        '
        Me.txtEmailAddress1.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtEmailAddress1.ForeColor = System.Drawing.Color.Black
        Me.txtEmailAddress1.Location = New System.Drawing.Point(165, 147)
        Me.txtEmailAddress1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtEmailAddress1.Name = "txtEmailAddress1"
        Me.txtEmailAddress1.Size = New System.Drawing.Size(333, 24)
        Me.txtEmailAddress1.TabIndex = 6
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Navy
        Me.Label12.Location = New System.Drawing.Point(6, 129)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(130, 20)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Telephone Line 2"
        '
        'txtTelephone2
        '
        Me.txtTelephone2.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtTelephone2.ForeColor = System.Drawing.Color.Black
        Me.txtTelephone2.Location = New System.Drawing.Point(165, 126)
        Me.txtTelephone2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtTelephone2.MaxLength = 10
        Me.txtTelephone2.Name = "txtTelephone2"
        Me.txtTelephone2.Size = New System.Drawing.Size(333, 24)
        Me.txtTelephone2.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Navy
        Me.Label11.Location = New System.Drawing.Point(6, 108)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(130, 20)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Telephone Line 1"
        '
        'txtTelphone1
        '
        Me.txtTelphone1.Font = New System.Drawing.Font("Century Gothic", 8.0!)
        Me.txtTelphone1.ForeColor = System.Drawing.Color.Black
        Me.txtTelphone1.Location = New System.Drawing.Point(165, 105)
        Me.txtTelphone1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtTelphone1.MaxLength = 10
        Me.txtTelphone1.Name = "txtTelphone1"
        Me.txtTelphone1.Size = New System.Drawing.Size(333, 24)
        Me.txtTelphone1.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(6, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(129, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Company Name"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(6, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(139, 20)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Legal Entity Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Navy
        Me.Label3.Location = New System.Drawing.Point(6, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(123, 20)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Registration No."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(6, 87)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 20)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Company VAT."
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(512, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(33, 32)
        Me.PictureBox1.TabIndex = 27
        Me.PictureBox1.TabStop = False
        '
        'frmCompanyDetails
        '
        Me.ClientSize = New System.Drawing.Size(547, 596)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button_Close)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(565, 643)
        Me.MinimumSize = New System.Drawing.Size(565, 643)
        Me.Name = "frmCompanyDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Company&Details"
        Me.Text = "Company Details"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_Close As System.Windows.Forms.Button
    Friend WithEvents Button_Save As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCompanyName As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCompanyVat As System.Windows.Forms.TextBox
    Friend WithEvents txtRegNo As System.Windows.Forms.TextBox
    Friend WithEvents txtLegalEntity As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtEmailAddress2 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtEmailAddress1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTelephone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtTelphone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPAddress4 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPAddress3 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPAddress2 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPAddress1 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtDirectors As System.Windows.Forms.TextBox
    Friend WithEvents btnNewCompany As System.Windows.Forms.Button
End Class
