﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmSuppliers

#Region "Variables"
    Dim MBData As New DataExtractsClass
#End Region

#Region "Methods"

    Sub oClearBoxes()

        Me.SupplierName_TextBox.Text = ""
        Me.RefID_TextBox.Text = ""
        Me.ContactNo_TextBox.Text = ""
        Me.ContactPerson_TextBox.Text = ""
        Me.VATDefault_ComboBox.Text = ""
        'Me.AccountDefault_TextBox.Text = ""
        Me.CategoryDefault_ComboBox.Text = ""
        Me.DetailDefault_ComboBox.Text = ""
        Me.DetailDefault2_ComboBox.Text = ""
        Me.Address_TextBox.Text = ""
        Me.EmailAddress_TextBox.Text = ""
        Me.Fax_TextBox.Text = ""
        'Me.Active_TextBox.Text = ""
        'Me.Search_TextBox.Text = ""

    End Sub

#End Region
    Sub Fill_Supplier_DGV(ShowDeactivated As Boolean)
        Try
            Me.Supplier_DataGridView.DataSource = Nothing
            Dim sSQL As String

            sSQL = "Select Suppliers.* , Case When Accounting.ID is null Then 0 else 1 end as HasAccount FRom Suppliers " & _
                    "Left Join Accounting " & _
                    "on Suppliers.[Category_Default] = Accounting.[Segment 1 Desc] " & _
                    "and Suppliers.Detail_Default = Accounting.[Segment 2 Desc] " & _
                    "And Suppliers.Detail_Default2 = Accounting.[Segment 3 Desc] " & _
                    "And Suppliers.Co_ID = Accounting.Co_ID "
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Suppliers.Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If ShowDeactivated = True Then
                sSQL = sSQL & " and Active <> 'Active'"
            Else
                sSQL = sSQL & " and Active <> ('Deactivated')"
            End If



            'Old 
            'oSQLTable_Suppliers.Clear()
            'Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            'oSQLAdapter_Suppliers = New SqlDataAdapter(sSQL, connection)
            'oSQLAdapter_Suppliers.Fill(oSQLTable_Suppliers)
            'Me.Supplier_DataGridView.DataSource = oSQLTable_Suppliers

            'New
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Suppliers")
            connection.Close()
            Supplier_DataGridView.DataSource = ds
            Supplier_DataGridView.DataMember = "Suppliers"

            For Each row As DataGridViewRow In Supplier_DataGridView.Rows
                If row.Cells("HasAccount").Value = 0 Then
                    row.DefaultCellStyle.BackColor = Color.IndianRed
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message & " 202")
        End Try
    End Sub
    Private Sub Suppliers_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Suppliers -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            If Not CheckIFTermsExisrs() Then
                AddTermsColumn()
            End If
            Cursor = Cursors.AppStarting
            Call Fill_Supplier_DGV(Cb_ShowDeactivated.Checked)
            Call LoadTax()
            Fill_Category1_Combobox()
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Suppliers error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ConfirmChanges_Button_Click(sender As System.Object, e As System.EventArgs) Handles ConfirmChanges_Button.Click
        Try

            If Me.Edit_ID_Label.Text = "None" Or Me.Edit_ID_Label.Text = "" Then
                MessageBox.Show("Please select a row to edit by clicking 'Open Selected' first.", "Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                btnEditSupplier.Focus()
                Exit Sub
            End If

            Dim oSupplierName As String = Me.SupplierName_TextBox.Text
            Dim oRefID As String = Me.RefID_TextBox.Text

            Dim oBalance As String = "0"

            Dim oContact_No As String = Me.ContactNo_TextBox.Text
            Dim oContact_Person As String = Me.ContactPerson_TextBox.Text
            Dim oModified As String = Now.Date.ToString("dd MMMM yyyy")
            Dim oVAT_Default As Integer = Nothing
            'Check Vat
            If IsNothing(cmbVAT.SelectedValue) Then
                MessageBox.Show("Please supply valid value for VAT type", "Vat Default", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbVAT.Focus()
                Exit Sub
            End If
            If IsNumeric(cmbVAT.SelectedValue.ToString) Then
                oVAT_Default = cmbVAT.SelectedValue.ToString
            Else
                MessageBox.Show("Please supply valid value for VAT type", "Vat Default", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbVAT.Focus()
                Exit Sub
            End If

            If Lbl_Edit_SuppName.Text <> oSupplierName Then
                If Not String.IsNullOrEmpty(MBData.CheckSupplierName(oSupplierName)) Then
                    MessageBox.Show("The supplier you're attempting to edit with already exits in the database for this company, please supply a different name", "Supplier name", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    SupplierName_TextBox.Focus()
                    Exit Sub
                End If
            End If

            If Lbl_Edit_SuppRef.Text <> oRefID Then
                If Not String.IsNullOrEmpty(MBData.CheckSupplierRef(oRefID)) Then
                    MessageBox.Show("The ref id you supplied is already linked to a supplier, please supply a different reference", "Ref Id", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    RefID_TextBox.Focus()
                    Exit Sub
                End If
            End If


            Dim oAccount_Default As String = ""
            Dim oCategory_Default As String = Me.CategoryDefault_ComboBox.Text
            Dim oDetail_Default As String = Me.DetailDefault_ComboBox.Text
            Dim oDetail_Default2 As String = Me.DetailDefault2_ComboBox.Text
            Dim oAddress As String = Me.Address_TextBox.Text
            Dim oEmail_Address As String = Me.EmailAddress_TextBox.Text
            Dim oFax As String = Me.Fax_TextBox.Text
            Dim oTerms As Integer = CInt(NudTerms.Value)
            Dim oIntegrationCode As String = TB_IntegrationCode.Text
            Dim oActive As String = ""
            If ckbIsActive.Checked = True Then
                oActive = "Active"
            Else
                oActive = "Deactivated"
            End If
            Dim oSearch As String = ""

            Dim sSQL As String

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim oID As Integer = CInt(Me.Edit_ID_Label.Text)


            If My.Settings.DBType = "SQL" Then

                sSQL = "UPDATE Suppliers SET "
                sSQL = sSQL & "SupplierName = " & "'" & SQLConvert(oSupplierName) & "', "
                sSQL = sSQL & "RefID = " & "'" & oRefID & "', "
                sSQL = sSQL & "Balance = " & oBalance & ", "
                sSQL = sSQL & "Contact_No = " & "'" & oContact_No & "', "
                sSQL = sSQL & "Contact_Person = " & "'" & oContact_Person & "', "
                sSQL = sSQL & "Modified = " & "'" & oModified & "', "
                sSQL = sSQL & "VAT_Default = " & oVAT_Default & ", "
                sSQL = sSQL & "Account_Default = " & "'" & oAccount_Default & "', "
                sSQL = sSQL & "Category_Default = " & "'" & oCategory_Default & "', "
                sSQL = sSQL & "Detail_Default = " & "'" & oDetail_Default & "', "
                sSQL = sSQL & "Detail_Default2 = " & "'" & oDetail_Default2 & "', "
                sSQL = sSQL & "Address  = " & "'" & oAddress & "', "
                sSQL = sSQL & "Email_Address = " & "'" & oEmail_Address & "', "
                sSQL = sSQL & "Fax = " & "'" & oFax & "', "
                sSQL = sSQL & "Active = " & "'" & oActive & "', "
                sSQL = sSQL & "Search = " & "'" & oSearch & "', "
                sSQL = sSQL & "Terms = " & oTerms & ","
                sSQL = sSQL & "IntegrationCode = '" & oIntegrationCode & "' "
                sSQL = sSQL & "WHERE SupplierID= " & oID & " "

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

            Me.Edit_ID_Label.Text = "None"
            System.Threading.Thread.Sleep(1000)
            Suppliers_Load(sender, e)

            MessageBox.Show("Supplier successfully updated!", _
            "Suppliers", MessageBoxButtons.OK, MessageBoxIcon.Information)

            With Me
                .SupplierName_TextBox.Enabled = False
                .RefID_TextBox.Enabled = False
                .ContactNo_TextBox.Enabled = False
                .CategoryDefault_ComboBox.Enabled = False
                .DetailDefault_ComboBox.Enabled = False
                .DetailDefault2_ComboBox.Enabled = False
                .ContactPerson_TextBox.Enabled = False
                .cmbVAT.Enabled = False
                .Address_TextBox.Enabled = False
                .EmailAddress_TextBox.Enabled = False
                .Fax_TextBox.Enabled = False
                .ConfirmChanges_Button.Enabled = False
                NudTerms.Enabled = False
                ckbIsActive.Enabled = False
                TB_IntegrationCode.Enabled = False
                Btn_SuppBank.Enabled = False
            End With

            'Form_Supplier_Payments.oLoadData() 'Reload invoice capture
            'frmInvcCapture.oFormLoad()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("There was an error editing supplier information! " & Err.Description)
        End Try
    End Sub
    Private Function CheckIFTermsExisrs() As Boolean

        Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            Dim ds As New DataSet

            sSQL = "SELECT TOP 1 * FROM Suppliers"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Using connection
                connection.Open()
                Dim cmd As New SqlDataAdapter(sSQL, connection)
                cmd.Fill(ds, "Suppliers")
                connection.Close()
            End Using

            Dim Suppliers As DataTable
            Suppliers = ds.Tables(0)

            If Suppliers.Columns.Contains("Terms") Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
            MessageBox.Show("An error occured with details :" & ex.Message, "Suppliers error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function
    Private Sub AddTermsColumn()
        Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            Dim comm As New SqlCommand("ALTER TABLE Suppliers ADD Terms INT", connection)
            comm.CommandTimeout = 300
            Using connection
                connection.Open()
                comm.ExecuteNonQuery()
                connection.Close()
            End Using

        Catch ex As Exception
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
            MessageBox.Show("An error occured with details :" & ex.Message, "Suppliers error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub Get_Supplier_Data_using_ID(ByVal oID As Integer)

        Dim oSupplierName As String
        Dim oRefID As String
        Dim oBalance As String
        Dim oContact_No As String
        Dim oContact_Person As String
        Dim oModified As Date
        Dim oVAT_Default As Integer
        Dim oAccount_Default As String
        Dim oCategory_Default As String
        Dim oDetail_Default As String
        Dim oDetail_Default2 As String
        Dim oAddress As String
        Dim oEmail_Address As String
        Dim oFax As String
        Dim oActive As String
        Dim oSearch As String
        Dim oTerms As Integer
        Dim oIntegrationCode As String


        Try

            Dim sSQL As String

            sSQL = "SELECT * FROM Suppliers WHERE SupplierID = " & oID

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                While datareader.Read
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        oID = datareader("SupplierID")
                    Else
                        MsgBox("The supplier at the line you clicked does not exist")
                        connection.Close()
                        Exit Sub
                    End If

                    If Not datareader("SupplierName").Equals(DBNull.Value) Then
                        oSupplierName = datareader("SupplierName")
                    Else
                        oSupplierName = ""
                    End If

                    If Not datareader("RefID").Equals(DBNull.Value) Then
                        oRefID = datareader("RefID")
                    Else
                        oRefID = ""
                    End If

                    If Not datareader("Balance").Equals(DBNull.Value) Then
                        oBalance = datareader("Balance")
                    Else
                        oBalance = ""
                    End If
                    If Not datareader("Contact_No").Equals(DBNull.Value) Then
                        oContact_No = datareader("Contact_No")
                    Else
                        oContact_No = ""
                    End If
                    If Not datareader("Contact_Person").Equals(DBNull.Value) Then
                        oContact_Person = datareader("Contact_Person")
                    Else
                        oContact_Person = ""
                    End If
                    If Not datareader("Modified").Equals(DBNull.Value) Then
                        oModified = CDate(datareader("Modified"))
                    Else
                        oModified = ""
                    End If
                    If Not datareader("VAT_Default").Equals(DBNull.Value) Then
                        oVAT_Default = datareader("VAT_Default")
                    Else
                        oVAT_Default = ""
                    End If
                    If Not datareader("Account_Default").Equals(DBNull.Value) Then
                        oAccount_Default = datareader("Account_Default")
                    Else
                        oCategory_Default = ""
                    End If
                    If Not datareader("Category_Default").Equals(DBNull.Value) Then
                        oCategory_Default = datareader("Category_Default")
                    Else

                    End If
                    If Not datareader("Detail_Default").Equals(DBNull.Value) Then
                        oDetail_Default = datareader("Detail_Default")
                    Else
                        oDetail_Default = ""
                    End If
                    If Not datareader("Detail_Default2").Equals(DBNull.Value) Then
                        oDetail_Default2 = datareader("Detail_Default2")
                    Else
                        oDetail_Default2 = ""
                    End If
                    If Not datareader("Address").Equals(DBNull.Value) Then
                        oAddress = datareader("Address")
                    Else
                        oAddress = ""
                    End If
                    If Not datareader("Email_Address").Equals(DBNull.Value) Then
                        oEmail_Address = datareader("Email_Address")
                    Else
                        oEmail_Address = ""
                    End If
                    If Not datareader("Fax").Equals(DBNull.Value) Then
                        oFax = datareader("Fax")
                    Else
                        oFax = ""
                    End If
                    If Not datareader("Active").Equals(DBNull.Value) Then
                        oActive = datareader("Active")
                    Else
                        oActive = ""
                    End If
                    If Not datareader("Search").Equals(DBNull.Value) Then
                        oSearch = datareader("Search")
                    Else
                        oSearch = ""
                    End If
                    If Not datareader("Terms").Equals(DBNull.Value) Then
                        oTerms = datareader("Terms")
                    Else
                        oTerms = Nothing
                    End If
                    If Not datareader("IntegrationCode").Equals(DBNull.Value) Then
                        oIntegrationCode = datareader("IntegrationCode")
                    Else
                        oIntegrationCode = Nothing
                    End If
                    If Not datareader("Active").Equals(DBNull.Value) Then
                        If datareader("Active") = "Active" Then
                            ckbIsActive.Checked = True
                        ElseIf datareader("Active") = "Deactivated" Then
                            ckbIsActive.Checked = False
                        ElseIf datareader("Active") = "" Then
                            ckbIsActive.Checked = False
                        End If
                    Else
                        ckbIsActive.Checked = False
                    End If
                End While

                connection.Close()
            End If

            Me.SupplierName_TextBox.Text = oSupplierName
            Me.RefID_TextBox.Text = oRefID
            'Me.Balance_TextBox.Text = oBalance
            Me.ContactNo_TextBox.Text = oContact_No
            Me.ContactPerson_TextBox.Text = oContact_Person
            'Me.Modified_TextBox.Text = oModified
            Me.cmbVAT.SelectedValue = oVAT_Default
            'Me.AccountDefault_TextBox.Text = oAccount_Default
            Me.CategoryDefault_ComboBox.SelectedItem = oCategory_Default
            Me.DetailDefault_ComboBox.SelectedItem = oDetail_Default
            Me.DetailDefault2_ComboBox.SelectedItem = oDetail_Default2
            Me.Address_TextBox.Text = oAddress
            Me.EmailAddress_TextBox.Text = oEmail_Address
            NudTerms.Value = oTerms
            TB_IntegrationCode.Text = oIntegrationCode
            'Me.Active_TextBox.Text = oActive
            'Me.Search_TextBox.Text = oSearch


        Catch ex As Exception
            MsgBox(ex.Message & " 203")
        End Try
    End Sub

    Sub Fill_Category1_Combobox()
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.CategoryDefault_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                        Me.CategoryDefault_ComboBox.Items.Add(datareader("Segment 1 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300

                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.CategoryDefault_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                        Me.CategoryDefault_ComboBox.Items.Add(datareader("Segment 1 Desc"))
                    End If
                End While
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 204")
        End Try

    End Sub

    Private Sub CategoryDefault_ComboBox_GotFocus(sender As Object, e As System.EventArgs) Handles CategoryDefault_ComboBox.GotFocus
        Me.CategoryDefault_ComboBox.DroppedDown = True
    End Sub

    Private Sub CategoryDefault_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CategoryDefault_ComboBox.SelectedIndexChanged
        If Me.CategoryDefault_ComboBox.Text = "" Then Exit Sub
        'First clear other two boxes
        Me.DetailDefault_ComboBox.Items.Clear()
        Me.DetailDefault_ComboBox.Text = ""
        Me.DetailDefault2_ComboBox.Items.Clear()
        Me.DetailDefault2_ComboBox.Text = ""
        'Then fill next box
        Call Fill_Detail_Combobox(Me.CategoryDefault_ComboBox.Text)
    End Sub

    Sub Fill_Detail_Combobox(Category1 As String)
        Try


            If My.Settings.DBType = "Access" Then

                Dim sSQL As String

                sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)

                Dim cmd As New OleDbCommand(sSQL, connection)
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Me.DetailDefault_ComboBox.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault_ComboBox.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String

                sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                Me.DetailDefault_ComboBox.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault_ComboBox.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 206")
        End Try

    End Sub


    Private Sub DetailDefault_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles DetailDefault_ComboBox.SelectedIndexChanged
        If Me.DetailDefault_ComboBox.Text = "" Then Exit Sub
        Call Fill_DetailDefault2_Combobox()
    End Sub

    Sub Fill_DetailDefault2_Combobox()
        Try

            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Me.DetailDefault_ComboBox.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryDefault_ComboBox.Text) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.DetailDefault2_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault2_ComboBox.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Me.DetailDefault_ComboBox.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryDefault_ComboBox.Text) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.DetailDefault2_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.DetailDefault2_ComboBox.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 207")
        End Try

    End Sub


    Private Sub Delete_Button_Click(sender As System.Object, e As System.EventArgs) Handles Delete_Button.Click
        Try

            If Me.Supplier_DataGridView.SelectedCells.Count < 1 Then
                MsgBox("Please select a row containing an invoice!")
                Exit Sub
            End If
            If Me.Supplier_DataGridView.SelectedRows.Count < 1 Then
                Me.Supplier_DataGridView.Rows(Supplier_DataGridView.SelectedCells(0).RowIndex).Selected = True
            End If

            Dim oSelectedRowIndex As Integer

            oSelectedRowIndex = Me.Supplier_DataGridView.SelectedCells(0).RowIndex

            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                Dim oID As Integer = Me.Supplier_DataGridView.Rows(oSelectedRowIndex).Cells(0).Value
                sSQL = "Delete From Suppliers Where SupplierID = " & oID
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                Dim oID As Integer = Me.Supplier_DataGridView.Rows(oSelectedRowIndex).Cells(0).Value
                sSQL = "Delete From Suppliers Where SupplierID = " & oID
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

            System.Threading.Thread.Sleep(1500)
            Call Fill_Supplier_DGV(Cb_ShowDeactivated.Checked)

        Catch ex As Exception
            MsgBox("There was an error deleting supplier information! " & Err.Description)
            'Me.Edit_ID_Label.Text = "None"
        End Try
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            Dim Form_SupplierNew As New frmSupplierNew
            Form_SupplierNew.ShowDialog()
            If Form_SupplierNew.NewSupplierAdded = True Then
                Enabled = False
                Cursor = Cursors.AppStarting
                Fill_Supplier_DGV(Cb_ShowDeactivated.Checked)
                Fill_Category1_Combobox()
                Enabled = True
                Cursor = Cursors.Arrow
            End If
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 208")
        End Try
    End Sub

    Private Sub Supplier_DataGridView_AllowUserToOrderColumnsChanged(sender As Object, e As EventArgs) Handles Supplier_DataGridView.AllowUserToOrderColumnsChanged

    End Sub
    Private Sub Supplier_DataGridView_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Supplier_DataGridView.CellClick
        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            Dim oID As Integer = Supplier_DataGridView.Rows(Supplier_DataGridView.SelectedCells(0).RowIndex).Cells(0).Value
            Dim oSuppName As String = Supplier_DataGridView.Rows(Supplier_DataGridView.SelectedCells(0).RowIndex).Cells(1).Value
            Dim oRef As String
            If Not IsDBNull(Supplier_DataGridView.Rows(Supplier_DataGridView.SelectedCells(0).RowIndex).Cells(2).Value) Then
                oRef = Supplier_DataGridView.Rows(Supplier_DataGridView.SelectedCells(0).RowIndex).Cells(2).Value
            End If

            Edit_ID_Label.Text = CStr(oID)
            Get_Supplier_Data_using_ID(oID)
            Lbl_Edit_SuppName.Text = oSuppName
            Lbl_Edit_SuppRef.Text = oRef
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, _
                            "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Private Sub btnEditSupplier_Click(sender As Object, e As EventArgs) Handles btnEditSupplier.Click
        SupplierName_TextBox.Enabled = True
        RefID_TextBox.Enabled = True
        ContactNo_TextBox.Enabled = True
        CategoryDefault_ComboBox.Enabled = True
        DetailDefault_ComboBox.Enabled = True
        DetailDefault2_ComboBox.Enabled = True
        ContactPerson_TextBox.Enabled = True
        cmbVAT.Enabled = True
        Address_TextBox.Enabled = True
        EmailAddress_TextBox.Enabled = True
        Fax_TextBox.Enabled = True
        ConfirmChanges_Button.Enabled = True
        NudTerms.Enabled = True
        ckbIsActive.Enabled = True
        TB_IntegrationCode.Enabled = True
        Btn_SuppBank.Enabled = True
    End Sub
    Private Function LockFields()
        SupplierName_TextBox.Enabled = False
        RefID_TextBox.Enabled = False
        ContactNo_TextBox.Enabled = False
        CategoryDefault_ComboBox.Enabled = False
        DetailDefault_ComboBox.Enabled = False
        DetailDefault2_ComboBox.Enabled = False
        ContactPerson_TextBox.Enabled = False
        cmbVAT.Enabled = False
        Address_TextBox.Enabled = False
        EmailAddress_TextBox.Enabled = False
        Fax_TextBox.Enabled = False
        ConfirmChanges_Button.Enabled = False
        NudTerms.Enabled = False
        ckbIsActive.Enabled = False
        TB_IntegrationCode.Enabled = False
        Btn_SuppBank.Enabled = False
    End Function

    Private Sub Supplier_DataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles Supplier_DataGridView.CellContentClick
        Call LockFields()
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub Btn_Search_Click(sender As Object, e As EventArgs)
        Call Fill_Supplier_DGV(Cb_ShowDeactivated.Checked)
    End Sub

    Private Sub Btn_SuppBank_Click(sender As Object, e As EventArgs) Handles Btn_SuppBank.Click
        Dim frmSupplierBank As New FrmSupplierBank
        frmSupplierBank.LBL_SupplierID.Text = Edit_ID_Label.Text
        frmSupplierBank.LBL_SupplierName.Text = SupplierName_TextBox.Text
        frmSupplierBank.LBL_SupplierName.Text = SupplierName_TextBox.Text
        frmSupplierBank.BankID.Text = GetSupplierBankID(Edit_ID_Label.Text)
        frmSupplierBank.Show()
    End Sub
    Function LoadTax()

        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        VATDefault_ComboBox.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            If ds.Tables(0).Rows.Count > 0 Then
                cmbVAT.DataSource = ds.Tables(0)
                cmbVAT.ValueMember = "Value"
                cmbVAT.DisplayMember = "Display"
                cmbVAT.SelectedIndex = 0
            Else
                MsgBox("There are no TAX types setup." & vbNewLine & "Go to settings and configure TAX")
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox("Can not open connection ! " & ex.Message)
        End Try


    End Function

    Private Sub Cb_ShowAll_CheckedChanged(sender As Object, e As EventArgs) Handles Cb_ShowDeactivated.CheckedChanged
        Call Fill_Supplier_DGV(Cb_ShowDeactivated.Checked)
    End Sub
End Class