﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoice_Capture
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInvoice_Capture))
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbSupplier = New System.Windows.Forms.ComboBox()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Cat3 = New System.Windows.Forms.ComboBox()
        Me.txtNonVATAmount = New System.Windows.Forms.TextBox()
        Me.LabelExVAT = New System.Windows.Forms.Label()
        Me.lblCat3 = New System.Windows.Forms.Label()
        Me.txtVATAmount = New System.Windows.Forms.TextBox()
        Me.LabelVATAmt = New System.Windows.Forms.Label()
        Me.Cat2 = New System.Windows.Forms.ComboBox()
        Me.cmbVATType = New System.Windows.Forms.ComboBox()
        Me.LabelVATType = New System.Windows.Forms.Label()
        Me.lblCat2 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Cat1 = New System.Windows.Forms.ComboBox()
        Me.InvoiceTotal_TextBox = New System.Windows.Forms.TextBox()
        Me.txtInvoiceNumber = New System.Windows.Forms.TextBox()
        Me.lblCat1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.InvoiceDate_DateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Notes_RichTextBox = New System.Windows.Forms.RichTextBox()
        Me.CaptureDate_DateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ComboBox_Status = New System.Windows.Forms.ComboBox()
        Me.txtGrossAmount = New System.Windows.Forms.TextBox()
        Me.VATAmount_TextBox = New System.Windows.Forms.TextBox()
        Me.MultipleLines_CheckBox = New System.Windows.Forms.CheckBox()
        Me.CategoryType3_ComboBox = New System.Windows.Forms.ComboBox()
        Me.CategoryType2_ComboBox = New System.Windows.Forms.ComboBox()
        Me.lblC3 = New System.Windows.Forms.Label()
        Me.NonVAT_Amount_TextBox = New System.Windows.Forms.TextBox()
        Me.lblC2 = New System.Windows.Forms.Label()
        Me.CategoryType1_ComboBox = New System.Windows.Forms.ComboBox()
        Me.lblC1 = New System.Windows.Forms.Label()
        Me.lblVT = New System.Windows.Forms.Label()
        Me.VATType_ComboBox = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DatePaid_DateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.cmbPaymentType = New System.Windows.Forms.ComboBox()
        Me.AmountPaid_TextBox = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.btnPayMultiple = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DateTimePicker_To = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker_From = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Save1 = New System.Windows.Forms.Button()
        Me.Result_Label = New System.Windows.Forms.Label()
        Me.ID_Label = New System.Windows.Forms.Label()
        Me.Delete_Invoice_Button = New System.Windows.Forms.Button()
        Me.lblMagicBox_Account_Segment4 = New System.Windows.Forms.Label()
        Me.EditInvoices_CheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.AddRow_Button = New System.Windows.Forms.Button()
        Me.DeleteButton = New System.Windows.Forms.Button()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Save2 = New System.Windows.Forms.Button()
        Me.Edit_Supplier_Combo = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.FindInvoice_GroupBox = New System.Windows.Forms.GroupBox()
        Me.ComboBox_Status_Delete_Invoice = New System.Windows.Forms.ComboBox()
        Me.FindButton = New System.Windows.Forms.Button()
        Me.Invoice_Numbers_Combo = New System.Windows.Forms.ComboBox()
        Me.VATTotal_TextBox = New System.Windows.Forms.TextBox()
        Me.Label_Version = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtUserName = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Button_Activate = New System.Windows.Forms.Button()
        Me.Label_Level = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label_Success = New System.Windows.Forms.Label()
        Me.DGV_Transactions = New System.Windows.Forms.DataGridView()
        Me.BatchID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Capture_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fin_Year = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reference = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAcc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tax_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accounting = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Void = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DR_CR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contra_Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label_Error_Batches = New System.Windows.Forms.Label()
        Me.DataGridView_Unpaid_Invoices = New System.Windows.Forms.DataGridView()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBox_Supplier = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Date_To = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Date_From = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnPay = New System.Windows.Forms.Button()
        Me.chkCredit = New System.Windows.Forms.CheckBox()
        Me.txtCreditAmount = New System.Windows.Forms.TextBox()
        Me.GroupBox_Credit = New System.Windows.Forms.GroupBox()
        Me.txtCreditExVAT = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtCreditVAT = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblVATSplit = New System.Windows.Forms.Label()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.dgvAccounts = New System.Windows.Forms.DataGridView()
        Me.dgvSuppliers = New System.Windows.Forms.DataGridView()
        Me.dgvPeriods = New System.Windows.Forms.DataGridView()
        Me.txtCredit = New System.Windows.Forms.TextBox()
        Me.lblCredit = New System.Windows.Forms.Label()
        Me.txtCrVAT = New System.Windows.Forms.TextBox()
        Me.lblCrVAT = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.chkNotes = New System.Windows.Forms.CheckBox()
        Me.TotalAmount = New System.Windows.Forms.TextBox()
        Me.lblEditInvoice = New System.Windows.Forms.Label()
        Me.lblEditInvoice_TransactionID = New System.Windows.Forms.Label()
        Me.PositionSingle = New System.Windows.Forms.Label()
        Me.PositionMulti = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FindInvoice_GroupBox.SuspendLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView_Unpaid_Invoices, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox_Credit.SuspendLayout()
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Brush Script MT", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label9.Location = New System.Drawing.Point(9, 7)
        Me.Label9.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(125, 37)
        Me.Label9.TabIndex = 39
        Me.Label9.Text = "Magic Box"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbSupplier)
        Me.GroupBox2.Controls.Add(Me.txtSearch)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.Cat3)
        Me.GroupBox2.Controls.Add(Me.txtNonVATAmount)
        Me.GroupBox2.Controls.Add(Me.LabelExVAT)
        Me.GroupBox2.Controls.Add(Me.lblCat3)
        Me.GroupBox2.Controls.Add(Me.txtVATAmount)
        Me.GroupBox2.Controls.Add(Me.LabelVATAmt)
        Me.GroupBox2.Controls.Add(Me.Cat2)
        Me.GroupBox2.Controls.Add(Me.cmbVATType)
        Me.GroupBox2.Controls.Add(Me.LabelVATType)
        Me.GroupBox2.Controls.Add(Me.lblCat2)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Cat1)
        Me.GroupBox2.Controls.Add(Me.InvoiceTotal_TextBox)
        Me.GroupBox2.Controls.Add(Me.txtInvoiceNumber)
        Me.GroupBox2.Controls.Add(Me.lblCat1)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.InvoiceDate_DateTimePicker)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(20, 62)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(5)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(5)
        Me.GroupBox2.Size = New System.Drawing.Size(761, 247)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSupplier.Location = New System.Drawing.Point(123, 82)
        Me.cmbSupplier.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(317, 24)
        Me.cmbSupplier.TabIndex = 1
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(463, 215)
        Me.txtSearch.Margin = New System.Windows.Forms.Padding(5)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(277, 22)
        Me.txtSearch.TabIndex = 10
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label29.Location = New System.Drawing.Point(459, 194)
        Me.Label29.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(162, 17)
        Me.Label29.TabIndex = 45
        Me.Label29.Text = "Search Category Type 3"
        '
        'Cat3
        '
        Me.Cat3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.Cat3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Cat3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cat3.FormattingEnabled = True
        Me.Cat3.Items.AddRange(New Object() {"ACCOUNTING & AUDITING"})
        Me.Cat3.Location = New System.Drawing.Point(459, 160)
        Me.Cat3.Margin = New System.Windows.Forms.Padding(5)
        Me.Cat3.Name = "Cat3"
        Me.Cat3.Size = New System.Drawing.Size(281, 24)
        Me.Cat3.TabIndex = 9
        '
        'txtNonVATAmount
        '
        Me.txtNonVATAmount.Enabled = False
        Me.txtNonVATAmount.Location = New System.Drawing.Point(328, 207)
        Me.txtNonVATAmount.Margin = New System.Windows.Forms.Padding(5)
        Me.txtNonVATAmount.Name = "txtNonVATAmount"
        Me.txtNonVATAmount.Size = New System.Drawing.Size(112, 22)
        Me.txtNonVATAmount.TabIndex = 6
        '
        'LabelExVAT
        '
        Me.LabelExVAT.AutoSize = True
        Me.LabelExVAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelExVAT.ForeColor = System.Drawing.Color.MidnightBlue
        Me.LabelExVAT.Location = New System.Drawing.Point(249, 214)
        Me.LabelExVAT.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.LabelExVAT.Name = "LabelExVAT"
        Me.LabelExVAT.Size = New System.Drawing.Size(53, 17)
        Me.LabelExVAT.TabIndex = 43
        Me.LabelExVAT.Text = "ex VAT"
        '
        'lblCat3
        '
        Me.lblCat3.AutoSize = True
        Me.lblCat3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat3.Location = New System.Drawing.Point(459, 137)
        Me.lblCat3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCat3.Name = "lblCat3"
        Me.lblCat3.Size = New System.Drawing.Size(113, 17)
        Me.lblCat3.TabIndex = 43
        Me.lblCat3.Text = "Category Type 3"
        '
        'txtVATAmount
        '
        Me.txtVATAmount.Location = New System.Drawing.Point(123, 207)
        Me.txtVATAmount.Margin = New System.Windows.Forms.Padding(5)
        Me.txtVATAmount.Name = "txtVATAmount"
        Me.txtVATAmount.Size = New System.Drawing.Size(115, 22)
        Me.txtVATAmount.TabIndex = 4
        '
        'LabelVATAmt
        '
        Me.LabelVATAmt.AutoSize = True
        Me.LabelVATAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelVATAmt.ForeColor = System.Drawing.Color.MidnightBlue
        Me.LabelVATAmt.Location = New System.Drawing.Point(28, 210)
        Me.LabelVATAmt.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.LabelVATAmt.Name = "LabelVATAmt"
        Me.LabelVATAmt.Size = New System.Drawing.Size(62, 17)
        Me.LabelVATAmt.TabIndex = 40
        Me.LabelVATAmt.Text = "VAT amt"
        '
        'Cat2
        '
        Me.Cat2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.Cat2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Cat2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cat2.FormattingEnabled = True
        Me.Cat2.Items.AddRange(New Object() {"ACCOUNTING & AUDITING", "BANK CHARGES", "CREDIT CARD CHARGES", "INSURANCE", "RENT + OPPS COSTS +RATES", "RENTALS AND LEASES", "FRANCHISE ROYALTY", "FRANCHISE MARKETING"})
        Me.Cat2.Location = New System.Drawing.Point(460, 103)
        Me.Cat2.Margin = New System.Windows.Forms.Padding(5)
        Me.Cat2.Name = "Cat2"
        Me.Cat2.Size = New System.Drawing.Size(281, 24)
        Me.Cat2.TabIndex = 8
        '
        'cmbVATType
        '
        Me.cmbVATType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbVATType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbVATType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVATType.FormattingEnabled = True
        Me.cmbVATType.Items.AddRange(New Object() {"0", "1"})
        Me.cmbVATType.Location = New System.Drawing.Point(328, 171)
        Me.cmbVATType.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbVATType.Name = "cmbVATType"
        Me.cmbVATType.Size = New System.Drawing.Size(112, 24)
        Me.cmbVATType.TabIndex = 5
        '
        'LabelVATType
        '
        Me.LabelVATType.AutoSize = True
        Me.LabelVATType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelVATType.ForeColor = System.Drawing.Color.MidnightBlue
        Me.LabelVATType.Location = New System.Drawing.Point(249, 175)
        Me.LabelVATType.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.LabelVATType.Name = "LabelVATType"
        Me.LabelVATType.Size = New System.Drawing.Size(66, 17)
        Me.LabelVATType.TabIndex = 35
        Me.LabelVATType.Text = "VAT type"
        '
        'lblCat2
        '
        Me.lblCat2.AutoSize = True
        Me.lblCat2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat2.Location = New System.Drawing.Point(459, 80)
        Me.lblCat2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCat2.Name = "lblCat2"
        Me.lblCat2.Size = New System.Drawing.Size(113, 17)
        Me.lblCat2.TabIndex = 33
        Me.lblCat2.Text = "Category Type 2"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label19.Location = New System.Drawing.Point(27, 171)
        Me.Label19.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 17)
        Me.Label19.TabIndex = 34
        Me.Label19.Text = "Inv Total"
        '
        'Cat1
        '
        Me.Cat1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.Cat1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Cat1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cat1.FormattingEnabled = True
        Me.Cat1.Items.AddRange(New Object() {"PURCHASES", "STAFF COSTS & WELFARE", "OPERATING COSTS", "UTILITIES", "NON CONTROLLABLE COSTS"})
        Me.Cat1.Location = New System.Drawing.Point(460, 46)
        Me.Cat1.Margin = New System.Windows.Forms.Padding(5)
        Me.Cat1.Name = "Cat1"
        Me.Cat1.Size = New System.Drawing.Size(281, 24)
        Me.Cat1.TabIndex = 7
        '
        'InvoiceTotal_TextBox
        '
        Me.InvoiceTotal_TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InvoiceTotal_TextBox.Location = New System.Drawing.Point(123, 171)
        Me.InvoiceTotal_TextBox.Margin = New System.Windows.Forms.Padding(5)
        Me.InvoiceTotal_TextBox.Name = "InvoiceTotal_TextBox"
        Me.InvoiceTotal_TextBox.Size = New System.Drawing.Size(115, 22)
        Me.InvoiceTotal_TextBox.TabIndex = 3
        '
        'txtInvoiceNumber
        '
        Me.txtInvoiceNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceNumber.Location = New System.Drawing.Point(123, 126)
        Me.txtInvoiceNumber.Margin = New System.Windows.Forms.Padding(5)
        Me.txtInvoiceNumber.Name = "txtInvoiceNumber"
        Me.txtInvoiceNumber.Size = New System.Drawing.Size(317, 22)
        Me.txtInvoiceNumber.TabIndex = 2
        '
        'lblCat1
        '
        Me.lblCat1.AutoSize = True
        Me.lblCat1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat1.Location = New System.Drawing.Point(455, 21)
        Me.lblCat1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCat1.Name = "lblCat1"
        Me.lblCat1.Size = New System.Drawing.Size(113, 17)
        Me.lblCat1.TabIndex = 31
        Me.lblCat1.Text = "Category Type 1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label4.Location = New System.Drawing.Point(27, 86)
        Me.Label4.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 17)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Supplier"
        '
        'InvoiceDate_DateTimePicker
        '
        Me.InvoiceDate_DateTimePicker.CustomFormat = "dd MMM yyyy"
        Me.InvoiceDate_DateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InvoiceDate_DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.InvoiceDate_DateTimePicker.Location = New System.Drawing.Point(123, 46)
        Me.InvoiceDate_DateTimePicker.Margin = New System.Windows.Forms.Padding(5)
        Me.InvoiceDate_DateTimePicker.Name = "InvoiceDate_DateTimePicker"
        Me.InvoiceDate_DateTimePicker.Size = New System.Drawing.Size(317, 22)
        Me.InvoiceDate_DateTimePicker.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label5.Location = New System.Drawing.Point(29, 42)
        Me.Label5.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 17)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Inv Date"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label1.Location = New System.Drawing.Point(27, 126)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 17)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Invoice No."
        '
        'Notes_RichTextBox
        '
        Me.Notes_RichTextBox.BackColor = System.Drawing.SystemColors.MenuBar
        Me.Notes_RichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Notes_RichTextBox.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Notes_RichTextBox.Location = New System.Drawing.Point(733, 347)
        Me.Notes_RichTextBox.Margin = New System.Windows.Forms.Padding(5)
        Me.Notes_RichTextBox.Name = "Notes_RichTextBox"
        Me.Notes_RichTextBox.Size = New System.Drawing.Size(201, 56)
        Me.Notes_RichTextBox.TabIndex = 49
        Me.Notes_RichTextBox.Text = ""
        Me.Notes_RichTextBox.Visible = False
        '
        'CaptureDate_DateTimePicker
        '
        Me.CaptureDate_DateTimePicker.Location = New System.Drawing.Point(1524, 20)
        Me.CaptureDate_DateTimePicker.Margin = New System.Windows.Forms.Padding(5)
        Me.CaptureDate_DateTimePicker.Name = "CaptureDate_DateTimePicker"
        Me.CaptureDate_DateTimePicker.Size = New System.Drawing.Size(112, 22)
        Me.CaptureDate_DateTimePicker.TabIndex = 5
        Me.CaptureDate_DateTimePicker.Visible = False
        '
        'ComboBox_Status
        '
        Me.ComboBox_Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox_Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox_Status.FormattingEnabled = True
        Me.ComboBox_Status.Items.AddRange(New Object() {"UNPAID", "PAID", "PAID LOCK"})
        Me.ComboBox_Status.Location = New System.Drawing.Point(108, 49)
        Me.ComboBox_Status.Margin = New System.Windows.Forms.Padding(5)
        Me.ComboBox_Status.Name = "ComboBox_Status"
        Me.ComboBox_Status.Size = New System.Drawing.Size(180, 24)
        Me.ComboBox_Status.TabIndex = 0
        '
        'txtGrossAmount
        '
        Me.txtGrossAmount.Location = New System.Drawing.Point(20, 448)
        Me.txtGrossAmount.Margin = New System.Windows.Forms.Padding(5)
        Me.txtGrossAmount.Name = "txtGrossAmount"
        Me.txtGrossAmount.Size = New System.Drawing.Size(163, 22)
        Me.txtGrossAmount.TabIndex = 15
        '
        'VATAmount_TextBox
        '
        Me.VATAmount_TextBox.Enabled = False
        Me.VATAmount_TextBox.Location = New System.Drawing.Point(299, 446)
        Me.VATAmount_TextBox.Margin = New System.Windows.Forms.Padding(5)
        Me.VATAmount_TextBox.Name = "VATAmount_TextBox"
        Me.VATAmount_TextBox.Size = New System.Drawing.Size(131, 22)
        Me.VATAmount_TextBox.TabIndex = 18
        '
        'MultipleLines_CheckBox
        '
        Me.MultipleLines_CheckBox.AutoSize = True
        Me.MultipleLines_CheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MultipleLines_CheckBox.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MultipleLines_CheckBox.Location = New System.Drawing.Point(24, 319)
        Me.MultipleLines_CheckBox.Margin = New System.Windows.Forms.Padding(5)
        Me.MultipleLines_CheckBox.Name = "MultipleLines_CheckBox"
        Me.MultipleLines_CheckBox.Size = New System.Drawing.Size(107, 20)
        Me.MultipleLines_CheckBox.TabIndex = 43
        Me.MultipleLines_CheckBox.Text = "multiple lines"
        Me.MultipleLines_CheckBox.UseVisualStyleBackColor = True
        '
        'CategoryType3_ComboBox
        '
        Me.CategoryType3_ComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.CategoryType3_ComboBox.FormattingEnabled = True
        Me.CategoryType3_ComboBox.Items.AddRange(New Object() {"ACCOUNTING & AUDITING"})
        Me.CategoryType3_ComboBox.Location = New System.Drawing.Point(1103, 446)
        Me.CategoryType3_ComboBox.Margin = New System.Windows.Forms.Padding(5)
        Me.CategoryType3_ComboBox.Name = "CategoryType3_ComboBox"
        Me.CategoryType3_ComboBox.Size = New System.Drawing.Size(239, 23)
        Me.CategoryType3_ComboBox.TabIndex = 23
        '
        'CategoryType2_ComboBox
        '
        Me.CategoryType2_ComboBox.FormattingEnabled = True
        Me.CategoryType2_ComboBox.Items.AddRange(New Object() {"ACCOUNTING & AUDITING", "BANK CHARGES", "CREDIT CARD CHARGES", "INSURANCE", "RENT + OPPS COSTS +RATES", "RENTALS AND LEASES", "FRANCHISE ROYALTY", "FRANCHISE MARKETING"})
        Me.CategoryType2_ComboBox.Location = New System.Drawing.Point(859, 444)
        Me.CategoryType2_ComboBox.Margin = New System.Windows.Forms.Padding(5)
        Me.CategoryType2_ComboBox.Name = "CategoryType2_ComboBox"
        Me.CategoryType2_ComboBox.Size = New System.Drawing.Size(239, 24)
        Me.CategoryType2_ComboBox.TabIndex = 22
        '
        'lblC3
        '
        Me.lblC3.AutoSize = True
        Me.lblC3.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblC3.Location = New System.Drawing.Point(1112, 421)
        Me.lblC3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblC3.Name = "lblC3"
        Me.lblC3.Size = New System.Drawing.Size(113, 17)
        Me.lblC3.TabIndex = 33
        Me.lblC3.Text = "Category Type 3"
        '
        'NonVAT_Amount_TextBox
        '
        Me.NonVAT_Amount_TextBox.Enabled = False
        Me.NonVAT_Amount_TextBox.Location = New System.Drawing.Point(508, 444)
        Me.NonVAT_Amount_TextBox.Margin = New System.Windows.Forms.Padding(5)
        Me.NonVAT_Amount_TextBox.Name = "NonVAT_Amount_TextBox"
        Me.NonVAT_Amount_TextBox.Size = New System.Drawing.Size(104, 22)
        Me.NonVAT_Amount_TextBox.TabIndex = 20
        '
        'lblC2
        '
        Me.lblC2.AutoSize = True
        Me.lblC2.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblC2.Location = New System.Drawing.Point(860, 421)
        Me.lblC2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblC2.Name = "lblC2"
        Me.lblC2.Size = New System.Drawing.Size(113, 17)
        Me.lblC2.TabIndex = 10
        Me.lblC2.Text = "Category Type 2"
        '
        'CategoryType1_ComboBox
        '
        Me.CategoryType1_ComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CategoryType1_ComboBox.FormattingEnabled = True
        Me.CategoryType1_ComboBox.Items.AddRange(New Object() {"PURCHASES", "STAFF COSTS & WELFARE", "OPERATING COSTS", "UTILITIES", "NON CONTROLLABLE COSTS"})
        Me.CategoryType1_ComboBox.Location = New System.Drawing.Point(615, 444)
        Me.CategoryType1_ComboBox.Margin = New System.Windows.Forms.Padding(5)
        Me.CategoryType1_ComboBox.Name = "CategoryType1_ComboBox"
        Me.CategoryType1_ComboBox.Size = New System.Drawing.Size(239, 23)
        Me.CategoryType1_ComboBox.TabIndex = 21
        '
        'lblC1
        '
        Me.lblC1.AutoSize = True
        Me.lblC1.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblC1.Location = New System.Drawing.Point(611, 420)
        Me.lblC1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblC1.Name = "lblC1"
        Me.lblC1.Size = New System.Drawing.Size(113, 17)
        Me.lblC1.TabIndex = 30
        Me.lblC1.Text = "Category Type 1"
        '
        'lblVT
        '
        Me.lblVT.AutoSize = True
        Me.lblVT.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVT.ForeColor = System.Drawing.Color.Blue
        Me.lblVT.Location = New System.Drawing.Point(202, 426)
        Me.lblVT.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblVT.Name = "lblVT"
        Me.lblVT.Size = New System.Drawing.Size(57, 15)
        Me.lblVT.TabIndex = 39
        Me.lblVT.Text = "VAT Type"
        Me.lblVT.Visible = False
        '
        'VATType_ComboBox
        '
        Me.VATType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.VATType_ComboBox.FormattingEnabled = True
        Me.VATType_ComboBox.Items.AddRange(New Object() {"0", "1"})
        Me.VATType_ComboBox.Location = New System.Drawing.Point(193, 446)
        Me.VATType_ComboBox.Margin = New System.Windows.Forms.Padding(5)
        Me.VATType_ComboBox.Name = "VATType_ComboBox"
        Me.VATType_ComboBox.Size = New System.Drawing.Size(99, 24)
        Me.VATType_ComboBox.TabIndex = 17
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Blue
        Me.Label15.Location = New System.Drawing.Point(316, 425)
        Me.Label15.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(87, 17)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "VAT Amount"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.Blue
        Me.Label16.Location = New System.Drawing.Point(27, 427)
        Me.Label16.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(98, 17)
        Me.Label16.TabIndex = 28
        Me.Label16.Text = "Gross Amount"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.DataGridView2)
        Me.GroupBox3.Controls.Add(Me.DatePaid_DateTimePicker)
        Me.GroupBox3.Controls.Add(Me.cmbPaymentType)
        Me.GroupBox3.Controls.Add(Me.AmountPaid_TextBox)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.ComboBox_Status)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.btnPayMultiple)
        Me.GroupBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox3.Location = New System.Drawing.Point(792, 64)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(5)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(5)
        Me.GroupBox3.Size = New System.Drawing.Size(320, 236)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Payment"
        '
        'DataGridView2
        '
        Me.DataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DataGridView2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView2.Location = New System.Drawing.Point(-165, 158)
        Me.DataGridView2.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(385, 107)
        Me.DataGridView2.TabIndex = 127
        Me.DataGridView2.Visible = False
        '
        'DatePaid_DateTimePicker
        '
        Me.DatePaid_DateTimePicker.CustomFormat = "dd MMM yyyy"
        Me.DatePaid_DateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DatePaid_DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DatePaid_DateTimePicker.Location = New System.Drawing.Point(108, 150)
        Me.DatePaid_DateTimePicker.Margin = New System.Windows.Forms.Padding(5)
        Me.DatePaid_DateTimePicker.Name = "DatePaid_DateTimePicker"
        Me.DatePaid_DateTimePicker.Size = New System.Drawing.Size(180, 22)
        Me.DatePaid_DateTimePicker.TabIndex = 2
        '
        'cmbPaymentType
        '
        Me.cmbPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPaymentType.FormattingEnabled = True
        Me.cmbPaymentType.Location = New System.Drawing.Point(107, 98)
        Me.cmbPaymentType.Margin = New System.Windows.Forms.Padding(5)
        Me.cmbPaymentType.Name = "cmbPaymentType"
        Me.cmbPaymentType.Size = New System.Drawing.Size(181, 24)
        Me.cmbPaymentType.TabIndex = 1
        '
        'AmountPaid_TextBox
        '
        Me.AmountPaid_TextBox.Enabled = False
        Me.AmountPaid_TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AmountPaid_TextBox.Location = New System.Drawing.Point(107, 201)
        Me.AmountPaid_TextBox.Margin = New System.Windows.Forms.Padding(5)
        Me.AmountPaid_TextBox.Name = "AmountPaid_TextBox"
        Me.AmountPaid_TextBox.Size = New System.Drawing.Size(181, 22)
        Me.AmountPaid_TextBox.TabIndex = 3
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(27, 201)
        Me.Label20.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(56, 17)
        Me.Label20.TabIndex = 33
        Me.Label20.Text = "Amount"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(27, 103)
        Me.Label21.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(40, 17)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Type"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(27, 156)
        Me.Label22.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(38, 17)
        Me.Label22.TabIndex = 29
        Me.Label22.Text = "Date"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(27, 53)
        Me.Label23.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(48, 17)
        Me.Label23.TabIndex = 28
        Me.Label23.Text = "Status"
        '
        'btnPayMultiple
        '
        Me.btnPayMultiple.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPayMultiple.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayMultiple.ForeColor = System.Drawing.Color.Gray
        Me.btnPayMultiple.Location = New System.Drawing.Point(181, 12)
        Me.btnPayMultiple.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPayMultiple.Name = "btnPayMultiple"
        Me.btnPayMultiple.Size = New System.Drawing.Size(108, 28)
        Me.btnPayMultiple.TabIndex = 11
        Me.btnPayMultiple.Text = "Pay multiple"
        Me.btnPayMultiple.UseVisualStyleBackColor = True
        Me.btnPayMultiple.Visible = False
        '
        'DataGridView1
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView1.Location = New System.Drawing.Point(17, 852)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(5)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(1153, 166)
        Me.DataGridView1.TabIndex = 44
        '
        'DateTimePicker_To
        '
        Me.DateTimePicker_To.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_To.Location = New System.Drawing.Point(1072, 37)
        Me.DateTimePicker_To.Margin = New System.Windows.Forms.Padding(5)
        Me.DateTimePicker_To.Name = "DateTimePicker_To"
        Me.DateTimePicker_To.Size = New System.Drawing.Size(181, 22)
        Me.DateTimePicker_To.TabIndex = 46
        '
        'DateTimePicker_From
        '
        Me.DateTimePicker_From.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_From.Location = New System.Drawing.Point(829, 36)
        Me.DateTimePicker_From.Margin = New System.Windows.Forms.Padding(5)
        Me.DateTimePicker_From.Name = "DateTimePicker_From"
        Me.DateTimePicker_From.Size = New System.Drawing.Size(187, 22)
        Me.DateTimePicker_From.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1028, 39)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 17)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "To"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(768, 37)
        Me.Label10.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 17)
        Me.Label10.TabIndex = 47
        Me.Label10.Text = "From"
        '
        'Save1
        '
        Me.Save1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Save1.Location = New System.Drawing.Point(947, 329)
        Me.Save1.Margin = New System.Windows.Forms.Padding(5)
        Me.Save1.Name = "Save1"
        Me.Save1.Size = New System.Drawing.Size(128, 34)
        Me.Save1.TabIndex = 2
        Me.Save1.Text = "Save"
        Me.Save1.UseVisualStyleBackColor = True
        '
        'Result_Label
        '
        Me.Result_Label.AutoSize = True
        Me.Result_Label.ForeColor = System.Drawing.Color.Maroon
        Me.Result_Label.Location = New System.Drawing.Point(1609, 1044)
        Me.Result_Label.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Result_Label.Name = "Result_Label"
        Me.Result_Label.Size = New System.Drawing.Size(0, 17)
        Me.Result_Label.TabIndex = 51
        '
        'ID_Label
        '
        Me.ID_Label.AutoSize = True
        Me.ID_Label.Location = New System.Drawing.Point(1305, 1498)
        Me.ID_Label.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.ID_Label.Name = "ID_Label"
        Me.ID_Label.Size = New System.Drawing.Size(0, 17)
        Me.ID_Label.TabIndex = 54
        '
        'Delete_Invoice_Button
        '
        Me.Delete_Invoice_Button.Location = New System.Drawing.Point(1181, 986)
        Me.Delete_Invoice_Button.Margin = New System.Windows.Forms.Padding(5)
        Me.Delete_Invoice_Button.Name = "Delete_Invoice_Button"
        Me.Delete_Invoice_Button.Size = New System.Drawing.Size(135, 33)
        Me.Delete_Invoice_Button.TabIndex = 50
        Me.Delete_Invoice_Button.Text = "Delete"
        Me.Delete_Invoice_Button.UseVisualStyleBackColor = True
        '
        'lblMagicBox_Account_Segment4
        '
        Me.lblMagicBox_Account_Segment4.AutoSize = True
        Me.lblMagicBox_Account_Segment4.ForeColor = System.Drawing.SystemColors.ScrollBar
        Me.lblMagicBox_Account_Segment4.Location = New System.Drawing.Point(479, 36)
        Me.lblMagicBox_Account_Segment4.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblMagicBox_Account_Segment4.Name = "lblMagicBox_Account_Segment4"
        Me.lblMagicBox_Account_Segment4.Size = New System.Drawing.Size(12, 17)
        Me.lblMagicBox_Account_Segment4.TabIndex = 59
        Me.lblMagicBox_Account_Segment4.Text = "."
        '
        'EditInvoices_CheckBox
        '
        Me.EditInvoices_CheckBox.AutoSize = True
        Me.EditInvoices_CheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditInvoices_CheckBox.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EditInvoices_CheckBox.Location = New System.Drawing.Point(24, 348)
        Me.EditInvoices_CheckBox.Margin = New System.Windows.Forms.Padding(5)
        Me.EditInvoices_CheckBox.Name = "EditInvoices_CheckBox"
        Me.EditInvoices_CheckBox.Size = New System.Drawing.Size(94, 20)
        Me.EditInvoices_CheckBox.TabIndex = 60
        Me.EditInvoices_CheckBox.Text = "edit/delete"
        Me.EditInvoices_CheckBox.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.Blue
        Me.Label11.Location = New System.Drawing.Point(504, 420)
        Me.Label11.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 17)
        Me.Label11.TabIndex = 61
        Me.Label11.Text = "ex VAT"
        '
        'AddRow_Button
        '
        Me.AddRow_Button.ForeColor = System.Drawing.Color.MediumBlue
        Me.AddRow_Button.Location = New System.Drawing.Point(1103, 368)
        Me.AddRow_Button.Margin = New System.Windows.Forms.Padding(5)
        Me.AddRow_Button.Name = "AddRow_Button"
        Me.AddRow_Button.Size = New System.Drawing.Size(119, 34)
        Me.AddRow_Button.TabIndex = 62
        Me.AddRow_Button.Text = "Add"
        Me.AddRow_Button.UseVisualStyleBackColor = True
        Me.AddRow_Button.Visible = False
        '
        'DeleteButton
        '
        Me.DeleteButton.ForeColor = System.Drawing.Color.MediumBlue
        Me.DeleteButton.Location = New System.Drawing.Point(1224, 368)
        Me.DeleteButton.Margin = New System.Windows.Forms.Padding(5)
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(119, 34)
        Me.DeleteButton.TabIndex = 63
        Me.DeleteButton.Text = "Delete"
        Me.DeleteButton.UseVisualStyleBackColor = True
        Me.DeleteButton.Visible = False
        '
        'ListView1
        '
        Me.ListView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListView1.FullRowSelect = True
        Me.ListView1.HideSelection = False
        Me.ListView1.Location = New System.Drawing.Point(19, 478)
        Me.ListView1.Margin = New System.Windows.Forms.Padding(5)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(1321, 144)
        Me.ListView1.TabIndex = 64
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'Save2
        '
        Me.Save2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Save2.Location = New System.Drawing.Point(948, 330)
        Me.Save2.Margin = New System.Windows.Forms.Padding(5)
        Me.Save2.Name = "Save2"
        Me.Save2.Size = New System.Drawing.Size(127, 34)
        Me.Save2.TabIndex = 3
        Me.Save2.Text = "Save"
        Me.Save2.UseVisualStyleBackColor = True
        Me.Save2.Visible = False
        '
        'Edit_Supplier_Combo
        '
        Me.Edit_Supplier_Combo.FormattingEnabled = True
        Me.Edit_Supplier_Combo.Location = New System.Drawing.Point(203, 33)
        Me.Edit_Supplier_Combo.Margin = New System.Windows.Forms.Padding(5)
        Me.Edit_Supplier_Combo.Name = "Edit_Supplier_Combo"
        Me.Edit_Supplier_Combo.Size = New System.Drawing.Size(276, 24)
        Me.Edit_Supplier_Combo.TabIndex = 60
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(215, 38)
        Me.Label12.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 17)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = "Supplier"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label18.Location = New System.Drawing.Point(1265, 39)
        Me.Label18.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(106, 17)
        Me.Label18.TabIndex = 68
        Me.Label18.Text = "Invoice Number"
        '
        'FindInvoice_GroupBox
        '
        Me.FindInvoice_GroupBox.Controls.Add(Me.ComboBox_Status_Delete_Invoice)
        Me.FindInvoice_GroupBox.Controls.Add(Me.FindButton)
        Me.FindInvoice_GroupBox.Controls.Add(Me.DateTimePicker_To)
        Me.FindInvoice_GroupBox.Controls.Add(Me.Invoice_Numbers_Combo)
        Me.FindInvoice_GroupBox.Controls.Add(Me.Label2)
        Me.FindInvoice_GroupBox.Controls.Add(Me.Edit_Supplier_Combo)
        Me.FindInvoice_GroupBox.Controls.Add(Me.Label18)
        Me.FindInvoice_GroupBox.Controls.Add(Me.DateTimePicker_From)
        Me.FindInvoice_GroupBox.Controls.Add(Me.Label12)
        Me.FindInvoice_GroupBox.Controls.Add(Me.Label10)
        Me.FindInvoice_GroupBox.Location = New System.Drawing.Point(17, 800)
        Me.FindInvoice_GroupBox.Margin = New System.Windows.Forms.Padding(5)
        Me.FindInvoice_GroupBox.Name = "FindInvoice_GroupBox"
        Me.FindInvoice_GroupBox.Padding = New System.Windows.Forms.Padding(5)
        Me.FindInvoice_GroupBox.Size = New System.Drawing.Size(1589, 10)
        Me.FindInvoice_GroupBox.TabIndex = 69
        Me.FindInvoice_GroupBox.TabStop = False
        Me.FindInvoice_GroupBox.Text = "Find invoice"
        '
        'ComboBox_Status_Delete_Invoice
        '
        Me.ComboBox_Status_Delete_Invoice.FormattingEnabled = True
        Me.ComboBox_Status_Delete_Invoice.Items.AddRange(New Object() {"[ALL]", "UNPAID", "PAID"})
        Me.ComboBox_Status_Delete_Invoice.Location = New System.Drawing.Point(507, 33)
        Me.ComboBox_Status_Delete_Invoice.Margin = New System.Windows.Forms.Padding(5)
        Me.ComboBox_Status_Delete_Invoice.Name = "ComboBox_Status_Delete_Invoice"
        Me.ComboBox_Status_Delete_Invoice.Size = New System.Drawing.Size(235, 24)
        Me.ComboBox_Status_Delete_Invoice.TabIndex = 71
        Me.ComboBox_Status_Delete_Invoice.Text = "[ALL]"
        '
        'FindButton
        '
        Me.FindButton.Location = New System.Drawing.Point(12, 31)
        Me.FindButton.Margin = New System.Windows.Forms.Padding(5)
        Me.FindButton.Name = "FindButton"
        Me.FindButton.Size = New System.Drawing.Size(160, 33)
        Me.FindButton.TabIndex = 70
        Me.FindButton.Text = "Find/Refresh"
        Me.FindButton.UseVisualStyleBackColor = True
        '
        'Invoice_Numbers_Combo
        '
        Me.Invoice_Numbers_Combo.FormattingEnabled = True
        Me.Invoice_Numbers_Combo.Location = New System.Drawing.Point(1413, 36)
        Me.Invoice_Numbers_Combo.Margin = New System.Windows.Forms.Padding(5)
        Me.Invoice_Numbers_Combo.Name = "Invoice_Numbers_Combo"
        Me.Invoice_Numbers_Combo.Size = New System.Drawing.Size(152, 24)
        Me.Invoice_Numbers_Combo.TabIndex = 69
        '
        'VATTotal_TextBox
        '
        Me.VATTotal_TextBox.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.VATTotal_TextBox.Enabled = False
        Me.VATTotal_TextBox.ForeColor = System.Drawing.SystemColors.Info
        Me.VATTotal_TextBox.Location = New System.Drawing.Point(185, 379)
        Me.VATTotal_TextBox.Margin = New System.Windows.Forms.Padding(5)
        Me.VATTotal_TextBox.Name = "VATTotal_TextBox"
        Me.VATTotal_TextBox.Size = New System.Drawing.Size(91, 22)
        Me.VATTotal_TextBox.TabIndex = 70
        Me.VATTotal_TextBox.Visible = False
        '
        'Label_Version
        '
        Me.Label_Version.AutoSize = True
        Me.Label_Version.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Version.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label_Version.Location = New System.Drawing.Point(892, 11)
        Me.Label_Version.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label_Version.Name = "Label_Version"
        Me.Label_Version.Size = New System.Drawing.Size(54, 17)
        Me.Label_Version.TabIndex = 71
        Me.Label_Version.Text = "version"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.Blue
        Me.Label6.Location = New System.Drawing.Point(24, 815)
        Me.Label6.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 17)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Invoices"
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(1371, 878)
        Me.txtUserName.Margin = New System.Windows.Forms.Padding(5)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(177, 22)
        Me.txtUserName.TabIndex = 76
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label14.Location = New System.Drawing.Point(1251, 889)
        Me.Label14.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(73, 17)
        Me.Label14.TabIndex = 75
        Me.Label14.Text = "Username"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(1371, 917)
        Me.txtPassword.Margin = New System.Windows.Forms.Padding(5)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(177, 22)
        Me.txtPassword.TabIndex = 78
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label17.Location = New System.Drawing.Point(1251, 923)
        Me.Label17.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(69, 17)
        Me.Label17.TabIndex = 77
        Me.Label17.Text = "Password"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(1251, 838)
        Me.Label24.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(91, 17)
        Me.Label24.TabIndex = 79
        Me.Label24.Text = "Activate level"
        '
        'Button_Activate
        '
        Me.Button_Activate.Location = New System.Drawing.Point(1443, 954)
        Me.Button_Activate.Margin = New System.Windows.Forms.Padding(5)
        Me.Button_Activate.Name = "Button_Activate"
        Me.Button_Activate.Size = New System.Drawing.Size(107, 34)
        Me.Button_Activate.TabIndex = 80
        Me.Button_Activate.Text = "Activate"
        Me.Button_Activate.UseVisualStyleBackColor = True
        '
        'Label_Level
        '
        Me.Label_Level.AutoSize = True
        Me.Label_Level.ForeColor = System.Drawing.Color.Maroon
        Me.Label_Level.Location = New System.Drawing.Point(1448, 1001)
        Me.Label_Level.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label_Level.Name = "Label_Level"
        Me.Label_Level.Size = New System.Drawing.Size(0, 17)
        Me.Label_Level.TabIndex = 81
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label25.Location = New System.Drawing.Point(1381, 999)
        Me.Label25.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(42, 17)
        Me.Label25.TabIndex = 82
        Me.Label25.Text = "Level"
        '
        'Label_Success
        '
        Me.Label_Success.AutoSize = True
        Me.Label_Success.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Success.Location = New System.Drawing.Point(973, 302)
        Me.Label_Success.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label_Success.Name = "Label_Success"
        Me.Label_Success.Size = New System.Drawing.Size(0, 16)
        Me.Label_Success.TabIndex = 94
        '
        'DGV_Transactions
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Transactions.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DGV_Transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Transactions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BatchID, Me.TransactionID, Me.LinkID, Me.Capture_Date, Me.Transaction_Date, Me.PPeriod, Me.Fin_Year, Me.GDC, Me.Reference, Me.Description, Me.AccNumber, Me.LinkAcc, Me.Amount, Me.TaxType, Me.Tax_Amount, Me.UserID, Me.SupplierID, Me.EmployeeID, Me.Description2, Me.Description3, Me.Description4, Me.Description5, Me.Posted, Me.Accounting, Me.Void, Me.Transaction_Type, Me.DR_CR, Me.Contra_Account, Me.Description_Code, Me.DocType, Me.SupplierName, Me.Info, Me.Info2, Me.Info3})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_Transactions.DefaultCellStyle = DataGridViewCellStyle6
        Me.DGV_Transactions.Location = New System.Drawing.Point(16, 649)
        Me.DGV_Transactions.Margin = New System.Windows.Forms.Padding(5)
        Me.DGV_Transactions.Name = "DGV_Transactions"
        Me.DGV_Transactions.RowTemplate.Height = 24
        Me.DGV_Transactions.Size = New System.Drawing.Size(875, 141)
        Me.DGV_Transactions.TabIndex = 96
        Me.DGV_Transactions.Visible = False
        '
        'BatchID
        '
        Me.BatchID.HeaderText = "BatchID"
        Me.BatchID.Name = "BatchID"
        Me.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TransactionID
        '
        Me.TransactionID.HeaderText = "TransactionID"
        Me.TransactionID.Name = "TransactionID"
        '
        'LinkID
        '
        Me.LinkID.HeaderText = "LinkID"
        Me.LinkID.Name = "LinkID"
        '
        'Capture_Date
        '
        Me.Capture_Date.HeaderText = "Capture_Date"
        Me.Capture_Date.Name = "Capture_Date"
        '
        'Transaction_Date
        '
        Me.Transaction_Date.HeaderText = "Transaction_Date"
        Me.Transaction_Date.Name = "Transaction_Date"
        '
        'PPeriod
        '
        Me.PPeriod.HeaderText = "PPeriod"
        Me.PPeriod.Name = "PPeriod"
        '
        'Fin_Year
        '
        Me.Fin_Year.HeaderText = "Fin_Year"
        Me.Fin_Year.Name = "Fin_Year"
        '
        'GDC
        '
        Me.GDC.HeaderText = "GDC"
        Me.GDC.Name = "GDC"
        '
        'Reference
        '
        Me.Reference.HeaderText = "Reference"
        Me.Reference.Name = "Reference"
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        '
        'AccNumber
        '
        Me.AccNumber.HeaderText = "AccNumber"
        Me.AccNumber.Name = "AccNumber"
        '
        'LinkAcc
        '
        Me.LinkAcc.HeaderText = "LinkAcc"
        Me.LinkAcc.Name = "LinkAcc"
        '
        'Amount
        '
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        '
        'TaxType
        '
        Me.TaxType.HeaderText = "TaxType"
        Me.TaxType.Name = "TaxType"
        '
        'Tax_Amount
        '
        Me.Tax_Amount.HeaderText = "Tax_Amount"
        Me.Tax_Amount.Name = "Tax_Amount"
        '
        'UserID
        '
        Me.UserID.HeaderText = "UserID"
        Me.UserID.Name = "UserID"
        '
        'SupplierID
        '
        Me.SupplierID.HeaderText = "SupplierID"
        Me.SupplierID.Name = "SupplierID"
        '
        'EmployeeID
        '
        Me.EmployeeID.HeaderText = "EmployeeID"
        Me.EmployeeID.Name = "EmployeeID"
        '
        'Description2
        '
        Me.Description2.HeaderText = "Description2"
        Me.Description2.Name = "Description2"
        '
        'Description3
        '
        Me.Description3.HeaderText = "Description3"
        Me.Description3.Name = "Description3"
        '
        'Description4
        '
        Me.Description4.HeaderText = "Description4"
        Me.Description4.Name = "Description4"
        '
        'Description5
        '
        Me.Description5.HeaderText = "Description5"
        Me.Description5.Name = "Description5"
        '
        'Posted
        '
        Me.Posted.HeaderText = "Posted"
        Me.Posted.Name = "Posted"
        '
        'Accounting
        '
        Me.Accounting.HeaderText = "Accounting"
        Me.Accounting.Name = "Accounting"
        '
        'Void
        '
        Me.Void.HeaderText = "Void"
        Me.Void.Name = "Void"
        '
        'Transaction_Type
        '
        Me.Transaction_Type.HeaderText = "Transaction_Type"
        Me.Transaction_Type.Name = "Transaction_Type"
        '
        'DR_CR
        '
        Me.DR_CR.HeaderText = "DR_CR"
        Me.DR_CR.Name = "DR_CR"
        '
        'Contra_Account
        '
        Me.Contra_Account.HeaderText = "Contra_Account"
        Me.Contra_Account.Name = "Contra_Account"
        '
        'Description_Code
        '
        Me.Description_Code.HeaderText = "Description_Code"
        Me.Description_Code.Name = "Description_Code"
        '
        'DocType
        '
        Me.DocType.HeaderText = "DocType"
        Me.DocType.Name = "DocType"
        '
        'SupplierName
        '
        Me.SupplierName.HeaderText = "SupplierName"
        Me.SupplierName.Name = "SupplierName"
        '
        'Info
        '
        Me.Info.HeaderText = "Info"
        Me.Info.Name = "Info"
        '
        'Info2
        '
        Me.Info2.HeaderText = "Info2"
        Me.Info2.Name = "Info2"
        '
        'Info3
        '
        Me.Info3.HeaderText = "Info3"
        Me.Info3.Name = "Info3"
        '
        'Label_Error_Batches
        '
        Me.Label_Error_Batches.AutoSize = True
        Me.Label_Error_Batches.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Error_Batches.ForeColor = System.Drawing.Color.Silver
        Me.Label_Error_Batches.Location = New System.Drawing.Point(1691, 20)
        Me.Label_Error_Batches.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label_Error_Batches.Name = "Label_Error_Batches"
        Me.Label_Error_Batches.Size = New System.Drawing.Size(12, 17)
        Me.Label_Error_Batches.TabIndex = 97
        Me.Label_Error_Batches.Text = "."
        '
        'DataGridView_Unpaid_Invoices
        '
        Me.DataGridView_Unpaid_Invoices.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView_Unpaid_Invoices.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridView_Unpaid_Invoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView_Unpaid_Invoices.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridView_Unpaid_Invoices.Location = New System.Drawing.Point(964, 38)
        Me.DataGridView_Unpaid_Invoices.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView_Unpaid_Invoices.Name = "DataGridView_Unpaid_Invoices"
        Me.DataGridView_Unpaid_Invoices.RowTemplate.Height = 24
        Me.DataGridView_Unpaid_Invoices.Size = New System.Drawing.Size(45, 27)
        Me.DataGridView_Unpaid_Invoices.TabIndex = 98
        Me.DataGridView_Unpaid_Invoices.Visible = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.CheckBox1.Location = New System.Drawing.Point(1825, 16)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(105, 21)
        Me.CheckBox1.TabIndex = 99
        Me.CheckBox1.Text = "pay multiple"
        Me.CheckBox1.UseVisualStyleBackColor = True
        Me.CheckBox1.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox_Supplier)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Date_To)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Date_From)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(40, 38)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(141, 60)
        Me.GroupBox1.TabIndex = 100
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Visible = False
        '
        'ComboBox_Supplier
        '
        Me.ComboBox_Supplier.FormattingEnabled = True
        Me.ComboBox_Supplier.Location = New System.Drawing.Point(9, 42)
        Me.ComboBox_Supplier.Margin = New System.Windows.Forms.Padding(5)
        Me.ComboBox_Supplier.Name = "ComboBox_Supplier"
        Me.ComboBox_Supplier.Size = New System.Drawing.Size(288, 24)
        Me.ComboBox_Supplier.TabIndex = 69
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.MediumBlue
        Me.Label13.Location = New System.Drawing.Point(9, 16)
        Me.Label13.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(103, 17)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Select Supplier"
        '
        'Date_To
        '
        Me.Date_To.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Date_To.Location = New System.Drawing.Point(491, 42)
        Me.Date_To.Margin = New System.Windows.Forms.Padding(5)
        Me.Date_To.Name = "Date_To"
        Me.Date_To.Size = New System.Drawing.Size(159, 22)
        Me.Date_To.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Blue
        Me.Label3.Location = New System.Drawing.Point(487, 18)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 17)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "To Date"
        '
        'Date_From
        '
        Me.Date_From.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Date_From.Location = New System.Drawing.Point(309, 43)
        Me.Date_From.Margin = New System.Windows.Forms.Padding(5)
        Me.Date_From.Name = "Date_From"
        Me.Date_From.Size = New System.Drawing.Size(169, 22)
        Me.Date_From.TabIndex = 65
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.Blue
        Me.Label8.Location = New System.Drawing.Point(312, 16)
        Me.Label8.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 17)
        Me.Label8.TabIndex = 67
        Me.Label8.Text = "From Date"
        '
        'btnPay
        '
        Me.btnPay.ForeColor = System.Drawing.Color.Blue
        Me.btnPay.Location = New System.Drawing.Point(801, 18)
        Me.btnPay.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(92, 33)
        Me.btnPay.TabIndex = 101
        Me.btnPay.Text = "Pay"
        Me.btnPay.UseVisualStyleBackColor = True
        Me.btnPay.Visible = False
        '
        'chkCredit
        '
        Me.chkCredit.AutoSize = True
        Me.chkCredit.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCredit.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkCredit.Location = New System.Drawing.Point(167, 319)
        Me.chkCredit.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCredit.Name = "chkCredit"
        Me.chkCredit.Size = New System.Drawing.Size(63, 20)
        Me.chkCredit.TabIndex = 105
        Me.chkCredit.Text = "credit"
        Me.chkCredit.UseVisualStyleBackColor = True
        '
        'txtCreditAmount
        '
        Me.txtCreditAmount.Location = New System.Drawing.Point(53, 26)
        Me.txtCreditAmount.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCreditAmount.Name = "txtCreditAmount"
        Me.txtCreditAmount.Size = New System.Drawing.Size(85, 22)
        Me.txtCreditAmount.TabIndex = 106
        Me.txtCreditAmount.Text = "0"
        '
        'GroupBox_Credit
        '
        Me.GroupBox_Credit.Controls.Add(Me.txtCreditExVAT)
        Me.GroupBox_Credit.Controls.Add(Me.Label27)
        Me.GroupBox_Credit.Controls.Add(Me.txtCreditVAT)
        Me.GroupBox_Credit.Controls.Add(Me.Label26)
        Me.GroupBox_Credit.Controls.Add(Me.txtCreditAmount)
        Me.GroupBox_Credit.ForeColor = System.Drawing.Color.Navy
        Me.GroupBox_Credit.Location = New System.Drawing.Point(335, 334)
        Me.GroupBox_Credit.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox_Credit.Name = "GroupBox_Credit"
        Me.GroupBox_Credit.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox_Credit.Size = New System.Drawing.Size(389, 70)
        Me.GroupBox_Credit.TabIndex = 110
        Me.GroupBox_Credit.TabStop = False
        Me.GroupBox_Credit.Text = "Credit"
        Me.GroupBox_Credit.Visible = False
        '
        'txtCreditExVAT
        '
        Me.txtCreditExVAT.BackColor = System.Drawing.SystemColors.Control
        Me.txtCreditExVAT.Enabled = False
        Me.txtCreditExVAT.Location = New System.Drawing.Point(292, 26)
        Me.txtCreditExVAT.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCreditExVAT.Name = "txtCreditExVAT"
        Me.txtCreditExVAT.Size = New System.Drawing.Size(85, 22)
        Me.txtCreditExVAT.TabIndex = 110
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.Color.Maroon
        Me.Label27.Location = New System.Drawing.Point(144, 30)
        Me.Label27.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(35, 17)
        Me.Label27.TabIndex = 109
        Me.Label27.Text = "VAT"
        '
        'txtCreditVAT
        '
        Me.txtCreditVAT.Location = New System.Drawing.Point(197, 26)
        Me.txtCreditVAT.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCreditVAT.Name = "txtCreditVAT"
        Me.txtCreditVAT.Size = New System.Drawing.Size(85, 22)
        Me.txtCreditVAT.TabIndex = 108
        Me.txtCreditVAT.Text = "0"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.Maroon
        Me.Label26.Location = New System.Drawing.Point(9, 30)
        Me.Label26.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(32, 17)
        Me.Label26.TabIndex = 107
        Me.Label26.Text = "Amt"
        '
        'lblVATSplit
        '
        Me.lblVATSplit.AutoSize = True
        Me.lblVATSplit.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblVATSplit.Location = New System.Drawing.Point(257, 320)
        Me.lblVATSplit.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblVATSplit.Name = "lblVATSplit"
        Me.lblVATSplit.Size = New System.Drawing.Size(66, 17)
        Me.lblVATSplit.TabIndex = 112
        Me.lblVATSplit.Text = "VAT Split"
        Me.lblVATSplit.Visible = False
        '
        'btnClear
        '
        Me.btnClear.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnClear.Location = New System.Drawing.Point(948, 369)
        Me.btnClear.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(127, 33)
        Me.btnClear.TabIndex = 4
        Me.btnClear.Text = "Clear/Cancel"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'dgvAccounts
        '
        Me.dgvAccounts.BackgroundColor = System.Drawing.Color.Teal
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAccounts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAccounts.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgvAccounts.Location = New System.Drawing.Point(1204, 7)
        Me.dgvAccounts.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvAccounts.Name = "dgvAccounts"
        Me.dgvAccounts.RowTemplate.Height = 24
        Me.dgvAccounts.Size = New System.Drawing.Size(71, 50)
        Me.dgvAccounts.TabIndex = 115
        Me.dgvAccounts.Visible = False
        '
        'dgvSuppliers
        '
        Me.dgvSuppliers.BackgroundColor = System.Drawing.Color.MidnightBlue
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSuppliers.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvSuppliers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSuppliers.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvSuppliers.Location = New System.Drawing.Point(1275, 7)
        Me.dgvSuppliers.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvSuppliers.Name = "dgvSuppliers"
        Me.dgvSuppliers.RowTemplate.Height = 24
        Me.dgvSuppliers.Size = New System.Drawing.Size(71, 50)
        Me.dgvSuppliers.TabIndex = 116
        Me.dgvSuppliers.Visible = False
        '
        'dgvPeriods
        '
        Me.dgvPeriods.BackgroundColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPeriods.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvPeriods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPeriods.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgvPeriods.Location = New System.Drawing.Point(1337, 17)
        Me.dgvPeriods.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvPeriods.Name = "dgvPeriods"
        Me.dgvPeriods.RowTemplate.Height = 24
        Me.dgvPeriods.Size = New System.Drawing.Size(71, 50)
        Me.dgvPeriods.TabIndex = 117
        Me.dgvPeriods.Visible = False
        '
        'txtCredit
        '
        Me.txtCredit.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtCredit.Location = New System.Drawing.Point(127, 448)
        Me.txtCredit.Margin = New System.Windows.Forms.Padding(5)
        Me.txtCredit.Name = "txtCredit"
        Me.txtCredit.Size = New System.Drawing.Size(62, 22)
        Me.txtCredit.TabIndex = 16
        Me.txtCredit.Visible = False
        '
        'lblCredit
        '
        Me.lblCredit.AutoSize = True
        Me.lblCredit.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblCredit.Location = New System.Drawing.Point(129, 421)
        Me.lblCredit.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCredit.Name = "lblCredit"
        Me.lblCredit.Size = New System.Drawing.Size(45, 17)
        Me.lblCredit.TabIndex = 119
        Me.lblCredit.Text = "Credit"
        Me.lblCredit.Visible = False
        '
        'txtCrVAT
        '
        Me.txtCrVAT.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtCrVAT.Location = New System.Drawing.Point(404, 446)
        Me.txtCrVAT.Margin = New System.Windows.Forms.Padding(5)
        Me.txtCrVAT.Name = "txtCrVAT"
        Me.txtCrVAT.Size = New System.Drawing.Size(99, 22)
        Me.txtCrVAT.TabIndex = 19
        Me.txtCrVAT.Visible = False
        '
        'lblCrVAT
        '
        Me.lblCrVAT.AutoSize = True
        Me.lblCrVAT.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblCrVAT.Location = New System.Drawing.Point(437, 421)
        Me.lblCrVAT.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblCrVAT.Name = "lblCrVAT"
        Me.lblCrVAT.Size = New System.Drawing.Size(53, 17)
        Me.lblCrVAT.TabIndex = 121
        Me.lblCrVAT.Text = "Cr VAT"
        Me.lblCrVAT.Visible = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(883, 305)
        Me.ProgressBar1.Margin = New System.Windows.Forms.Padding(4)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(228, 16)
        Me.ProgressBar1.TabIndex = 122
        Me.ProgressBar1.Visible = False
        '
        'chkNotes
        '
        Me.chkNotes.AutoSize = True
        Me.chkNotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNotes.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkNotes.Location = New System.Drawing.Point(167, 348)
        Me.chkNotes.Margin = New System.Windows.Forms.Padding(4)
        Me.chkNotes.Name = "chkNotes"
        Me.chkNotes.Size = New System.Drawing.Size(63, 20)
        Me.chkNotes.TabIndex = 124
        Me.chkNotes.Text = "notes"
        Me.chkNotes.UseVisualStyleBackColor = True
        '
        'TotalAmount
        '
        Me.TotalAmount.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.TotalAmount.Enabled = False
        Me.TotalAmount.ForeColor = System.Drawing.SystemColors.Info
        Me.TotalAmount.Location = New System.Drawing.Point(83, 380)
        Me.TotalAmount.Margin = New System.Windows.Forms.Padding(5)
        Me.TotalAmount.Name = "TotalAmount"
        Me.TotalAmount.Size = New System.Drawing.Size(91, 22)
        Me.TotalAmount.TabIndex = 125
        Me.TotalAmount.Visible = False
        '
        'lblEditInvoice
        '
        Me.lblEditInvoice.AutoSize = True
        Me.lblEditInvoice.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblEditInvoice.Location = New System.Drawing.Point(253, 37)
        Me.lblEditInvoice.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEditInvoice.Name = "lblEditInvoice"
        Me.lblEditInvoice.Size = New System.Drawing.Size(80, 17)
        Me.lblEditInvoice.TabIndex = 128
        Me.lblEditInvoice.Text = "Edit Invoice"
        Me.lblEditInvoice.Visible = False
        '
        'lblEditInvoice_TransactionID
        '
        Me.lblEditInvoice_TransactionID.AutoSize = True
        Me.lblEditInvoice_TransactionID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblEditInvoice_TransactionID.Location = New System.Drawing.Point(377, 37)
        Me.lblEditInvoice_TransactionID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEditInvoice_TransactionID.Name = "lblEditInvoice_TransactionID"
        Me.lblEditInvoice_TransactionID.Size = New System.Drawing.Size(0, 17)
        Me.lblEditInvoice_TransactionID.TabIndex = 129
        Me.lblEditInvoice_TransactionID.Visible = False
        '
        'PositionSingle
        '
        Me.PositionSingle.AutoSize = True
        Me.PositionSingle.Location = New System.Drawing.Point(1061, 416)
        Me.PositionSingle.Name = "PositionSingle"
        Me.PositionSingle.Size = New System.Drawing.Size(97, 17)
        Me.PositionSingle.TabIndex = 130
        Me.PositionSingle.Text = "PositionSingle"
        Me.PositionSingle.Visible = False
        '
        'PositionMulti
        '
        Me.PositionMulti.AutoSize = True
        Me.PositionMulti.Location = New System.Drawing.Point(1282, 636)
        Me.PositionMulti.Name = "PositionMulti"
        Me.PositionMulti.Size = New System.Drawing.Size(87, 17)
        Me.PositionMulti.TabIndex = 131
        Me.PositionMulti.Text = "PositionMulti"
        Me.PositionMulti.Visible = False
        '
        'frmInvoice_Capture
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1229, 755)
        Me.Controls.Add(Me.PositionMulti)
        Me.Controls.Add(Me.PositionSingle)
        Me.Controls.Add(Me.lblEditInvoice_TransactionID)
        Me.Controls.Add(Me.lblEditInvoice)
        Me.Controls.Add(Me.TotalAmount)
        Me.Controls.Add(Me.chkNotes)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.lblCrVAT)
        Me.Controls.Add(Me.txtCrVAT)
        Me.Controls.Add(Me.lblCredit)
        Me.Controls.Add(Me.txtCredit)
        Me.Controls.Add(Me.dgvPeriods)
        Me.Controls.Add(Me.Label_Success)
        Me.Controls.Add(Me.dgvSuppliers)
        Me.Controls.Add(Me.GroupBox_Credit)
        Me.Controls.Add(Me.dgvAccounts)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.lblVATSplit)
        Me.Controls.Add(Me.btnPay)
        Me.Controls.Add(Me.chkCredit)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.DataGridView_Unpaid_Invoices)
        Me.Controls.Add(Me.Label_Error_Batches)
        Me.Controls.Add(Me.DGV_Transactions)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label_Level)
        Me.Controls.Add(Me.Button_Activate)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtUserName)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label_Version)
        Me.Controls.Add(Me.VATTotal_TextBox)
        Me.Controls.Add(Me.Delete_Invoice_Button)
        Me.Controls.Add(Me.FindInvoice_GroupBox)
        Me.Controls.Add(Me.Save2)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.DeleteButton)
        Me.Controls.Add(Me.AddRow_Button)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Save1)
        Me.Controls.Add(Me.MultipleLines_CheckBox)
        Me.Controls.Add(Me.EditInvoices_CheckBox)
        Me.Controls.Add(Me.lblMagicBox_Account_Segment4)
        Me.Controls.Add(Me.CategoryType3_ComboBox)
        Me.Controls.Add(Me.CategoryType2_ComboBox)
        Me.Controls.Add(Me.lblC3)
        Me.Controls.Add(Me.NonVAT_Amount_TextBox)
        Me.Controls.Add(Me.CaptureDate_DateTimePicker)
        Me.Controls.Add(Me.lblC2)
        Me.Controls.Add(Me.ID_Label)
        Me.Controls.Add(Me.CategoryType1_ComboBox)
        Me.Controls.Add(Me.lblC1)
        Me.Controls.Add(Me.Notes_RichTextBox)
        Me.Controls.Add(Me.lblVT)
        Me.Controls.Add(Me.Result_Label)
        Me.Controls.Add(Me.VATType_ComboBox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.VATAmount_TextBox)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.txtGrossAmount)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label9)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "frmInvoice_Capture"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "S_Supplier&Invoice&Capture"
        Me.Text = "Invoice Capture"
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        Me.GroupBox3.ResumeLayout(false)
        Me.GroupBox3.PerformLayout
        CType(Me.DataGridView2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DataGridView1,System.ComponentModel.ISupportInitialize).EndInit
        Me.FindInvoice_GroupBox.ResumeLayout(false)
        Me.FindInvoice_GroupBox.PerformLayout
        CType(Me.DGV_Transactions,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DataGridView_Unpaid_Invoices,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.GroupBox_Credit.ResumeLayout(false)
        Me.GroupBox_Credit.PerformLayout
        CType(Me.dgvAccounts,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.dgvSuppliers,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.dgvPeriods,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents InvoiceDate_DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CaptureDate_DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtInvoiceNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtGrossAmount As System.Windows.Forms.TextBox
    Friend WithEvents VATAmount_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblC3 As System.Windows.Forms.Label
    Friend WithEvents CategoryType3_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents lblC1 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents CategoryType1_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents lblC2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents AmountPaid_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents VATType_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents lblVT As System.Windows.Forms.Label
    Friend WithEvents NonVAT_Amount_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CategoryType2_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPaymentType As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DateTimePicker_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Notes_RichTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents Save1 As System.Windows.Forms.Button
    Friend WithEvents Result_Label As System.Windows.Forms.Label
    Friend WithEvents ID_Label As System.Windows.Forms.Label
    Friend WithEvents Delete_Invoice_Button As System.Windows.Forms.Button
    Friend WithEvents lblMagicBox_Account_Segment4 As System.Windows.Forms.Label
    Friend WithEvents MultipleLines_CheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents EditInvoices_CheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents AddRow_Button As System.Windows.Forms.Button
    Friend WithEvents DeleteButton As System.Windows.Forms.Button
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Save2 As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Edit_Supplier_Combo As System.Windows.Forms.ComboBox
    Friend WithEvents FindInvoice_GroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Invoice_Numbers_Combo As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents InvoiceTotal_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents FindButton As System.Windows.Forms.Button
    Friend WithEvents VATTotal_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label_Version As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Status_Delete_Invoice As System.Windows.Forms.ComboBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Button_Activate As System.Windows.Forms.Button
    Friend WithEvents Label_Level As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label_Success As System.Windows.Forms.Label
    Friend WithEvents DGV_Transactions As System.Windows.Forms.DataGridView
    Friend WithEvents Label_Error_Batches As System.Windows.Forms.Label
    Friend WithEvents DataGridView_Unpaid_Invoices As System.Windows.Forms.DataGridView
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Date_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Date_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Supplier As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnPay As System.Windows.Forms.Button
    Friend WithEvents chkCredit As System.Windows.Forms.CheckBox
    Friend WithEvents txtCreditAmount As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox_Credit As System.Windows.Forms.GroupBox
    Friend WithEvents txtCreditExVAT As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtCreditVAT As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lblVATSplit As System.Windows.Forms.Label
    Friend WithEvents btnPayMultiple As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents dgvAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSuppliers As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPeriods As System.Windows.Forms.DataGridView
    Friend WithEvents txtCredit As System.Windows.Forms.TextBox
    Friend WithEvents lblCredit As System.Windows.Forms.Label
    Friend WithEvents txtCrVAT As System.Windows.Forms.TextBox
    Friend WithEvents lblCrVAT As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents lblCat1 As System.Windows.Forms.Label
    Friend WithEvents Cat3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCat3 As System.Windows.Forms.Label
    Friend WithEvents Cat2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCat2 As System.Windows.Forms.Label
    Friend WithEvents Cat1 As System.Windows.Forms.ComboBox
    Friend WithEvents chkNotes As System.Windows.Forms.CheckBox
    Friend WithEvents txtNonVATAmount As System.Windows.Forms.TextBox
    Friend WithEvents LabelExVAT As System.Windows.Forms.Label
    Friend WithEvents txtVATAmount As System.Windows.Forms.TextBox
    Friend WithEvents LabelVATAmt As System.Windows.Forms.Label
    Friend WithEvents cmbVATType As System.Windows.Forms.ComboBox
    Friend WithEvents LabelVATType As System.Windows.Forms.Label
    Friend WithEvents TotalAmount As System.Windows.Forms.TextBox
    Friend WithEvents BatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Capture_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fin_Year As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Reference As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAcc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tax_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accounting As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Void As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DR_CR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contra_Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents lblEditInvoice As System.Windows.Forms.Label
    Friend WithEvents lblEditInvoice_TransactionID As System.Windows.Forms.Label
    Friend WithEvents DatePaid_DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents PositionSingle As System.Windows.Forms.Label
    Friend WithEvents PositionMulti As System.Windows.Forms.Label
End Class
