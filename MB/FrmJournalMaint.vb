﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Data

Public Class FrmJournalMaint
    Dim TaxPerc As Double
    Dim TaxCalc As Double
    Private Sub FrmJournalMaint_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_TemplateID.Text = 0
        Call LoadHeaders()
        Call LoadTax()
        CB_DrCr.SelectedIndex = 0
        CB_Tax.SelectedIndex = 0
    End Sub
    Function LoadTax()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        '  cmbVATType.Items.Clear()
        '  VATType_ComboBox.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Tax.DataSource = ds.Tables(0)
            CB_Tax.ValueMember = "Value"
            CB_Tax.DisplayMember = "Display"
            CB_Tax.SelectedIndex = 0
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Function LoadAccounts()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        If RB_AccByCode.Checked = True Then
            sSQL = "SELECT [Segment 4] as Value,[Segment 4] + ' :~: ' + [Segment 3 Desc] as Display FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
                sSQL = sSQL & " Order By [Segment 4] "
            End If
        Else
            sSQL = "SELECT [Segment 4] as Value,[Segment 3 Desc] + ' :~: ' + [Segment 4] as Display FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
                sSQL = sSQL & " Order By [Segment 3 Desc]"
            End If
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Account.DataSource = ds.Tables(0)
            CB_Account.ValueMember = "Value"
            CB_Account.DisplayMember = "Display"
            CB_Account.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        Catch ex As Exception
            MsgBox("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Function LoadContras()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        If RB_ContraByCode.Checked = True Then
            sSQL = "SELECT [Segment 4] as Value,[Segment 4] + ' :~: ' + [Segment 3 Desc] as Display FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
                sSQL = sSQL & " Order By [Segment 4] "
            End If
        Else
            sSQL = "SELECT [Segment 4] as Value,[Segment 3 Desc] + ' :~: ' + [Segment 4] as Display FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
                sSQL = sSQL & " Order By [Segment 3 Desc]"
            End If
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Contra.DataSource = ds.Tables(0)
            CB_Contra.ValueMember = "Value"
            CB_Contra.DisplayMember = "Display"
            CB_Contra.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        Catch ex As Exception
            MsgBox("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Function LoadHeaders()
        Dim SecureEnc As New PasswordSecurity
        Try
            '' Dim tmpCol As ColumnHeader
            ' tmpCol = lsv_Names.Columns.Add("B", 50, HorizontalAlignment.Left)
            ' tmpCol.Width = 100



            Dim sSQL As String = "SELECT Distinct HeaderID,JournalName From Journal_Header Where Co_ID =" & My.Settings.Setting_CompanyID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            lsv_Names.Items.Clear()
            While datareader.Read
                Dim lvi As New ListViewItem("t")
                lvi.Tag = datareader("HeaderID")
                lvi.Text = datareader("JournalName")
                lvi.Name = datareader("JournalName")
                Dim ImageKey As String = Guid.NewGuid().ToString()
                ImageList1.Images.Add(ImageKey, My.Resources.journal)
                lvi.ImageKey = ImageKey
                lsv_Names.Items.Add(lvi)
            End While
            connection.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub lsv_Names_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsv_Names.SelectedIndexChanged
        For Each item As ListViewItem In lsv_Names.Items
            If item.Selected = True Then
                lbl_TemplateID.Text = item.Tag.ToString
                LoadJournal()
                Grp_LineInfo.Enabled = True
            End If
        Next

    End Sub

    Function LoadJournal()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            'sSQL = CStr("Select HeaderID,DetailID,LineDesc,LineAccount,LineAccountDesc,LineAccountContra,LineAccountDescContra,LineDRCR," & _
            '    "Case When LineDRCR = 'Cr' then LineAmount * -1 else LineAmount End As AmountExcl, " & _
            '    "Case When LineDRCR = 'Cr' then (LineAmount * (TaxType/100)) *-1 else LineAmount * (TaxType/100) End As Tax, " & _
            '    "Case When LineDRCR = 'Cr' then (LineAmount * (1+ (TaxType/100))*-1)  else LineAmount * (1+ (TaxType/100)) End As AmountIncl " & _
            '    "From Journal_Lines Where HeaderID =  " & lbl_TemplateID.Text)

            sSQL = CStr("Select HeaderID,DetailID,LineDesc,LineAccount,LineAccountDesc,LineAccountContra,LineAccountDescContra,LineDRCR," & _
                           "LineAmount As AmountExcl, " & _
                            "LineAmount * (TaxType/100) As Tax, " & _
                            "LineAmount * (1+ (TaxType/100)) As AmountIncl " & _
                            "From Journal_Lines Where HeaderID =  " & lbl_TemplateID.Text)

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Journals")
            connection.Close()
            DGV_Lines.DataSource = ds
            DGV_Lines.DataMember = "Journals"
            DGV_Lines.Columns("HeaderID").Visible = False
            DGV_Lines.Columns("DetailID").Visible = False
            DGV_Lines.Columns("AmountExcl").DefaultCellStyle.Format = "C"
            DGV_Lines.Columns("Tax").DefaultCellStyle.Format = "C"
            DGV_Lines.Columns("AmountIncl").DefaultCellStyle.Format = "C"
            DGV_Lines.AutoResizeColumns()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Function


    Private Sub lbl_AddLine_Click(sender As Object, e As EventArgs) Handles lbl_AddLine.Click
        Try
            Dim strSQL As String = ""
            strSQL = strSQL & "Insert Into [Journal_Lines] ([HeaderID],[LineDesc],[LineAccount],[LineAccountDesc],[LineAccountContra]" & _
                              ",[LineAccountDescContra],[LineDRCR],[LineAmount],[TaxType]) Values (" & lbl_TemplateID.Text & _
                              ",'" & TB_Desc.Text & "','" & CB_Account.SelectedValue.ToString & "','" & CB_Account.Text & _
                              "','" & CB_Contra.SelectedValue.ToString & "','" & CB_Contra.Text & _
                              "','" & CB_DrCr.Text & "','" & TB_Amount.Text & "','" & CB_Tax.SelectedValue & "')"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            LoadJournal()
        Catch ex As Exception
            MsgBox("There was an error adding the new journal line! Error: " & ex.Message)
        End Try
    End Sub

    Private Sub RB_AccByCode_CheckedChanged(sender As Object, e As EventArgs) Handles RB_AccByCode.CheckedChanged
        Call LoadAccounts()
    End Sub

    Private Sub RB_AccByDesc_CheckedChanged(sender As Object, e As EventArgs) Handles RB_AccByDesc.CheckedChanged
        Call LoadAccounts()
    End Sub

    Private Sub RB_ContraByCode_CheckedChanged(sender As Object, e As EventArgs) Handles RB_ContraByCode.CheckedChanged
        Call LoadContras()
    End Sub

    Private Sub RB_ContraByDesc_CheckedChanged(sender As Object, e As EventArgs) Handles RB_ContraByDesc.CheckedChanged
        Call LoadContras()
    End Sub

    Private Sub lbl_TemplateID_Click(sender As Object, e As EventArgs) Handles lbl_TemplateID.Click
        If lbl_TemplateID.Text = "0" Then
            Grp_LineInfo.Enabled = False
        Else
            Grp_LineInfo.Enabled = True
        End If
    End Sub

    Private Sub TB_Amount_TextChanged(sender As Object, e As EventArgs) Handles TB_Amount.TextChanged
        CalcAmount()
    End Sub
    Private Sub CB_Tax_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Tax.SelectedIndexChanged
        CalcAmount()
    End Sub
    Private Sub TB_Incl_TextChanged(sender As Object, e As EventArgs) Handles TB_Incl.TextChanged
        CalcAmount()
    End Sub

    Private Sub Btn_removeTemplate_Click(sender As Object, e As EventArgs) Handles Btn_removeTemplate.Click
        If MsgBox("Are you sure you want to remove the journal template?", MsgBoxStyle.YesNo, "Template Delete") = MsgBoxResult.Yes Then
            Try
                Dim strSQL As String = ""
                strSQL = "Delete From  [Journal_Lines] Where HeaderID = '" & lbl_TemplateID.Text & "' " & vbNewLine & _
                         "Delete From  [Journal_Header] Where HeaderID = '" & lbl_TemplateID.Text & "'"


                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(strSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                cmd.ExecuteNonQuery()
                connection.Close()
                LoadHeaders()
                lbl_TemplateID.Text = 0
                LoadJournal()
            Catch ex As Exception
                MsgBox("There was an error! Error: " & ex.Message)
                LoadHeaders()
            End Try
        End If
    End Sub

    Private Sub Btn_New_Click(sender As Object, e As EventArgs) Handles Btn_New.Click
        Dim TemplateName As String

        TemplateName = InputBox("Template Name:", "Create New", "")
        If TemplateName = "" Then
            MsgBox("No name given, process aborted")
        Else
            Try
                Dim strSQL As String = ""
                strSQL = "INSERT INTO [Journal_Header] ([JournalName],[JournalDescription],[Co_ID]) VALUES (" & _
                         "'" & TemplateName & "',''," & My.Settings.Setting_CompanyID & ")"
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(strSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                cmd.ExecuteNonQuery()
                connection.Close()
                LoadHeaders()
                lbl_TemplateID.Text = 0
                LoadJournal()
            Catch ex As Exception
                MsgBox("There was an error! Error: " & ex.Message)
            End Try
        End If
    End Sub

    Private Sub RB_AmtExcl_CheckedChanged(sender As Object, e As EventArgs) Handles RB_AmtExcl.CheckedChanged
        If RB_AmtExcl.Checked = True Then
            TB_Incl.Enabled = False
            TB_Amount.Enabled = True
        Else
            TB_Amount.Enabled = False
            TB_Incl.Enabled = True
        End If
        CalcAmount()
    End Sub
    Function CalcAmount()
        If IsNothing(CB_Tax.SelectedValue) Then Exit Function
        If CB_Tax.SelectedValue.ToString = "System.Data.DataRowView" Then Exit Function
        TaxPerc = CB_Tax.SelectedValue.ToString
        TaxCalc = 1 + (TaxPerc / 100)
        If RB_AmtExcl.Checked = True Then
            If Val(TaxPerc) <> 0 Then
                TB_Tax.Text = Math.Round((Val(TB_Amount.Text) * TaxCalc) - Val(TB_Amount.Text), 2)
                TB_Incl.Text = Val(TB_Amount.Text) + Val(TB_Tax.Text)
            Else
                TB_Tax.Text = 0
                TB_Incl.Text = Val(TB_Amount.Text)
            End If
        Else
            If TaxPerc <> 0 Then
                TB_Tax.Text = Math.Round((Val(TB_Incl.Text - (TB_Incl.Text) / TaxCalc)), 2)
                TB_Amount.Text = Val(TB_Incl.Text) - Val(TB_Tax.Text)
            Else
                TB_Tax.Text = 0
                TB_Amount.Text = Val(TB_Incl.Text)
            End If
        End If
    End Function

    Private Sub Lbl_RemoveLine_Click(sender As Object, e As EventArgs) Handles Lbl_RemoveLine.Click
        Dim value As Object = DGV_Lines.Rows(DGV_Lines.CurrentCell.RowIndex).Cells("DetailID").Value

        If Not IsDBNull(value) Then
            If MsgBox("Are you sure you want to remove the journal line template?", MsgBoxStyle.YesNo, "Template Line Delete") = MsgBoxResult.Yes Then
                Try
                    Dim strSQL As String = ""
                    strSQL = "Delete From  [Journal_Lines] Where DetailID = '" & CType(value, String) & "' "


                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(strSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    cmd.ExecuteNonQuery()
                    connection.Close()
                    LoadJournal()
                Catch ex As Exception
                    MsgBox("There was an error! Error: " & ex.Message)
                    LoadHeaders()
                End Try
            End If
        End If

    End Sub

    Private Sub Btn_LoadJ_Click(sender As Object, e As EventArgs) Handles Btn_LoadJ.Click
        Dim JDS As New DataTable
        JDS.Columns.Add("Period")
        JDS.Columns.Add("TransactionDate")
        JDS.Columns.Add("Reference")
        JDS.Columns.Add("Description")
        JDS.Columns.Add("Tax")
        JDS.Columns.Add("Inclusive")
        JDS.Columns.Add("Exclusive")
        JDS.Columns.Add("DefaultCat")
        JDS.Columns.Add("Cat1")
        JDS.Columns.Add("Cat2")
        JDS.Columns.Add("Cat3")
        JDS.Columns.Add("ContraCat1")
        JDS.Columns.Add("ContraCat2")
        JDS.Columns.Add("ContraCat3")
        JDS.Columns.Add("Segment4Account")
        JDS.Columns.Add("Segment4Contra")
        JDS.Columns.Add("SupplierID")
        JDS.Columns.Add("SupplierName")
        JDS.Columns.Add("EmployeeID")
        JDS.Columns.Add("JournalType")
        JDS.Columns.Add("Accounting")

        Dim Tax As Long

        For i As Integer = 0 To DGV_Lines.Rows.Count - 1
            Dim str(20) As String
            str(0) = GetPeriod(Now.ToString("dd MMM yyyy")) 'Period
            str(1) = Now.ToString("dd MMM yyyy") 'TransactionDate
            str(2) = DGV_Lines.Rows(i).Cells("LineDesc").Value 'Reference
            str(3) = DGV_Lines.Rows(i).Cells("LineDesc").Value 'Description
            Tax = DGV_Lines.Rows(i).Cells("Tax").Value
            str(4) = Tax 'Tax
            str(5) = DGV_Lines.Rows(i).Cells("AmountIncl").Value 'Inclusive
            str(6) = DGV_Lines.Rows(i).Cells("AmountExcl").Value 'Exclusive
            str(7) = DGV_Lines.Rows(i).Cells("LineDRCR").Value 'DefaultCat
            str(8) = Get_AccountInfo_from_Seg4(DGV_Lines.Rows(i).Cells("LineAccount").Value, "Segment 1 Desc")
            str(9) = Get_AccountInfo_from_Seg4(DGV_Lines.Rows(i).Cells("LineAccount").Value, "Segment 2 Desc")
            str(10) = Get_AccountInfo_from_Seg4(DGV_Lines.Rows(i).Cells("LineAccount").Value, "Segment 3 Desc")
            str(11) = Get_AccountInfo_from_Seg4(DGV_Lines.Rows(i).Cells("LineAccountContra").Value, "Segment 1 Desc")
            str(12) = Get_AccountInfo_from_Seg4(DGV_Lines.Rows(i).Cells("LineAccountContra").Value, "Segment 2 Desc")
            str(13) = Get_AccountInfo_from_Seg4(DGV_Lines.Rows(i).Cells("LineAccountContra").Value, "Segment 3 Desc")
            str(14) = DGV_Lines.Rows(i).Cells("LineAccount").Value     'Segment4Account
            str(15) = DGV_Lines.Rows(i).Cells("LineAccountContra").Value 'Segment4Contra
            str(16) = "0" 'SupplierID
            str(17) = ""  'SupplierName
            str(18) = 0  'EmployeeID
            str(19) = "Genral Journal" 'JournalType
            str(20) = 1  'Accounting

            JDS.Rows.Add(str(0), str(1), str(2), str(3), str(4), str(5), str(6), str(7), str(8), str(9), str(10), str(11), str(12), str(13), str(14) _
                         , str(15), str(16), str(17), str(18), str(19), str(20))
        Next
        Globals.Ribbons.Ribbon1.JournalTemplate = JDS
        Me.Close()
    End Sub
End Class