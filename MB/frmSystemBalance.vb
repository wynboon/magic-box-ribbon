﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmSystemBalance

    Private Sub btnGetBalance_Click(sender As System.Object, e As System.EventArgs) Handles btnGetBalance.Click
        oGetBalances()
    End Sub

    Sub oGetBalances()
        Try
            Me.txtTotalDebits.Text = Get_Total_Dr()
            Me.txtTotalCredits.Text = Get_Total_Cr()
            Me.lblBalance.Text = CDec(Me.txtTotalDebits.Text) - CDec(Me.txtTotalCredits.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function Get_Total_Dr() As String
        Try

            Dim sSQL As String

            Dim oResult As String
            If My.Settings.DBType = "Access" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where DR_CR = 'Dr' And Accounting = True and Void = FALSE"
                If rdbCurrentCompany.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                ElseIf Me.rdbSpecificCoID.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & Me.NumericUpDown1.Value.ToString
                Else
                    'do nothing
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Dr = "0"
                Else
                    Get_Total_Dr = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where DR_CR = 'Dr' And Accounting = 1 and Void = 0"
                If rdbCurrentCompany.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                ElseIf Me.rdbSpecificCoID.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & Me.NumericUpDown1.Value.ToString
                Else
                    'do nothing
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Dr = "0"
                Else
                    Get_Total_Dr = oResult
                End If
                connection.Close()

            End If

        Catch ex As Exception
            MsgBox("Error getting the total Debits" & ex.Message)
        End Try
    End Function

    Function Get_Total_Cr() As String
        Try

            Dim sSQL As String

            Dim oResult As String
            If My.Settings.DBType = "Access" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where DR_CR = 'Dr' And Accounting = True and Void = FALSE"
                If rdbCurrentCompany.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                ElseIf Me.rdbSpecificCoID.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & Me.NumericUpDown1.Value.ToString
                Else
                    'do nothing
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Cr = "0"
                Else
                    Get_Total_Cr = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where DR_CR = 'Dr' And Accounting = 1 and Void = 0"
                If rdbCurrentCompany.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                ElseIf Me.rdbSpecificCoID.Checked = True Then
                    sSQL = sSQL & " And Co_ID = " & Me.NumericUpDown1.Value.ToString
                Else
                    'do nothing
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Cr = "0"
                Else
                    Get_Total_Cr = oResult
                End If
                connection.Close()

            End If

        Catch ex As Exception
            MsgBox("Error getting the total Debits" & ex.Message)
        End Try
    End Function
    Private Sub frmSystemBalance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> System Balances -> Version : " & My.Settings.Setting_Version
        oGetBalances()
    End Sub
End Class