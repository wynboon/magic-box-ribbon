﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Administration
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Administration))
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TB_ContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TS_RemoveUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TAB_Group = New System.Windows.Forms.TabPage()
        Me.Btn_NewGrp = New System.Windows.Forms.Button()
        Me.Btn_ActiveateNew = New System.Windows.Forms.Button()
        Me.Btn_Update = New System.Windows.Forms.Button()
        Me.LBL_GroupID = New System.Windows.Forms.Label()
        Me.Lbl_lastModified = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Lbl_DateCreated = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.LBL_GUID = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Chk_Active = New System.Windows.Forms.CheckBox()
        Me.TB_DBPass = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TB_DBUser = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TB_Database = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TB_ServerAddress = New System.Windows.Forms.TextBox()
        Me.lbl_ServerAddress = New System.Windows.Forms.Label()
        Me.TB_LegalName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_GroupName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TAB_Store = New System.Windows.Forms.TabPage()
        Me.TB_POSID = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Btn_AddNew = New System.Windows.Forms.Button()
        Me.lbl_StoreID = New System.Windows.Forms.Label()
        Me.TB_LinkedCOID = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TB_ShortName = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TB_StoreName = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TAB_User = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_UpdateGroup = New System.Windows.Forms.Button()
        Me.btn_UpdateRolesAssignments = New System.Windows.Forms.Button()
        Me.ltv_Roles = New System.Windows.Forms.ListView()
        Me.Lbl_U_SelectedGroupID = New System.Windows.Forms.Label()
        Me.Lbl_U_SelectedGroup = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Ckb_U_active = New System.Windows.Forms.CheckBox()
        Me.lbl_DateModified = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lbl_U_dateCreated = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TB_U_eMailAddress = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.LBL_U_ID = New System.Windows.Forms.Label()
        Me.TB_U_Surname = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TB_U_FirstName = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TB_U_TenantName = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TAB_UserMapping = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Btn_RemoveUser = New System.Windows.Forms.Button()
        Me.Btn_AddUser = New System.Windows.Forms.Button()
        Me.DGV_UserMapping = New System.Windows.Forms.DataGridView()
        Me.Map = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.TAB_RoleDef = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Btn = New System.Windows.Forms.Button()
        Me.Btn_ExclRemove = New System.Windows.Forms.Button()
        Me.Btn_ExclAdd = New System.Windows.Forms.Button()
        Me.TB_AddExclusion = New System.Windows.Forms.TextBox()
        Me.LstB_Exclusions = New System.Windows.Forms.ListBox()
        Me.lbl_Paytypes = New System.Windows.Forms.Label()
        Me.clb_PayTypes = New System.Windows.Forms.CheckedListBox()
        Me.btn_RoleMap = New System.Windows.Forms.Button()
        Me.lbl_R_GrpID = New System.Windows.Forms.Label()
        Me.Btn_AddRole = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.CB_Forms = New System.Windows.Forms.ComboBox()
        Me.TV_Role = New System.Windows.Forms.TreeView()
        Me.CM_RoleTask = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RemoveRoleTaskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.cb_AutoInvoice = New System.Windows.Forms.CheckBox()
        Me.CB_AutoGRN = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btn_RefreshAuto = New System.Windows.Forms.Button()
        Me.lbl_AutoLoaderID = New System.Windows.Forms.Label()
        Me.TB_ContextMenu.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TAB_Group.SuspendLayout()
        Me.TAB_Store.SuspendLayout()
        Me.TAB_User.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TAB_UserMapping.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DGV_UserMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TAB_RoleDef.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.CM_RoleTask.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeView1
        '
        Me.TreeView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TreeView1.ContextMenuStrip = Me.TB_ContextMenu
        Me.TreeView1.ImageIndex = 0
        Me.TreeView1.ImageList = Me.TreeImageList
        Me.TreeView1.Location = New System.Drawing.Point(13, 13)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.SelectedImageIndex = 0
        Me.TreeView1.Size = New System.Drawing.Size(327, 628)
        Me.TreeView1.TabIndex = 0
        '
        'TB_ContextMenu
        '
        Me.TB_ContextMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TB_ContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TS_RemoveUser})
        Me.TB_ContextMenu.Name = "TB_ContextMenu"
        Me.TB_ContextMenu.Size = New System.Drawing.Size(166, 28)
        '
        'TS_RemoveUser
        '
        Me.TS_RemoveUser.Name = "TS_RemoveUser"
        Me.TS_RemoveUser.Size = New System.Drawing.Size(165, 24)
        Me.TS_RemoveUser.Text = "Remove User"
        '
        'TreeImageList
        '
        Me.TreeImageList.ImageStream = CType(resources.GetObject("TreeImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.TreeImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.TreeImageList.Images.SetKeyName(0, "Usersm.jpg")
        Me.TreeImageList.Images.SetKeyName(1, "Menu.jpg")
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TAB_Group)
        Me.TabControl1.Controls.Add(Me.TAB_Store)
        Me.TabControl1.Controls.Add(Me.TAB_User)
        Me.TabControl1.Controls.Add(Me.TAB_UserMapping)
        Me.TabControl1.Controls.Add(Me.TAB_RoleDef)
        Me.TabControl1.Location = New System.Drawing.Point(358, 13)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1199, 628)
        Me.TabControl1.TabIndex = 1
        '
        'TAB_Group
        '
        Me.TAB_Group.Controls.Add(Me.Btn_NewGrp)
        Me.TAB_Group.Controls.Add(Me.Btn_ActiveateNew)
        Me.TAB_Group.Controls.Add(Me.Btn_Update)
        Me.TAB_Group.Controls.Add(Me.LBL_GroupID)
        Me.TAB_Group.Controls.Add(Me.Lbl_lastModified)
        Me.TAB_Group.Controls.Add(Me.Label11)
        Me.TAB_Group.Controls.Add(Me.Lbl_DateCreated)
        Me.TAB_Group.Controls.Add(Me.Label9)
        Me.TAB_Group.Controls.Add(Me.LBL_GUID)
        Me.TAB_Group.Controls.Add(Me.Label1)
        Me.TAB_Group.Controls.Add(Me.Chk_Active)
        Me.TAB_Group.Controls.Add(Me.TB_DBPass)
        Me.TAB_Group.Controls.Add(Me.Label6)
        Me.TAB_Group.Controls.Add(Me.TB_DBUser)
        Me.TAB_Group.Controls.Add(Me.Label4)
        Me.TAB_Group.Controls.Add(Me.TB_Database)
        Me.TAB_Group.Controls.Add(Me.Label5)
        Me.TAB_Group.Controls.Add(Me.TB_ServerAddress)
        Me.TAB_Group.Controls.Add(Me.lbl_ServerAddress)
        Me.TAB_Group.Controls.Add(Me.TB_LegalName)
        Me.TAB_Group.Controls.Add(Me.Label3)
        Me.TAB_Group.Controls.Add(Me.TB_GroupName)
        Me.TAB_Group.Controls.Add(Me.Label2)
        Me.TAB_Group.Location = New System.Drawing.Point(4, 25)
        Me.TAB_Group.Name = "TAB_Group"
        Me.TAB_Group.Padding = New System.Windows.Forms.Padding(3)
        Me.TAB_Group.Size = New System.Drawing.Size(1191, 599)
        Me.TAB_Group.TabIndex = 0
        Me.TAB_Group.Text = "Group"
        Me.TAB_Group.UseVisualStyleBackColor = True
        '
        'Btn_NewGrp
        '
        Me.Btn_NewGrp.Location = New System.Drawing.Point(21, 413)
        Me.Btn_NewGrp.Name = "Btn_NewGrp"
        Me.Btn_NewGrp.Size = New System.Drawing.Size(343, 32)
        Me.Btn_NewGrp.TabIndex = 23
        Me.Btn_NewGrp.Text = "Save New Group"
        Me.Btn_NewGrp.UseVisualStyleBackColor = True
        Me.Btn_NewGrp.Visible = False
        '
        'Btn_ActiveateNew
        '
        Me.Btn_ActiveateNew.Location = New System.Drawing.Point(21, 375)
        Me.Btn_ActiveateNew.Name = "Btn_ActiveateNew"
        Me.Btn_ActiveateNew.Size = New System.Drawing.Size(343, 32)
        Me.Btn_ActiveateNew.TabIndex = 22
        Me.Btn_ActiveateNew.Text = "Create a new group"
        Me.Btn_ActiveateNew.UseVisualStyleBackColor = True
        '
        'Btn_Update
        '
        Me.Btn_Update.Location = New System.Drawing.Point(21, 337)
        Me.Btn_Update.Name = "Btn_Update"
        Me.Btn_Update.Size = New System.Drawing.Size(343, 32)
        Me.Btn_Update.TabIndex = 21
        Me.Btn_Update.Text = "Update"
        Me.Btn_Update.UseVisualStyleBackColor = True
        '
        'LBL_GroupID
        '
        Me.LBL_GroupID.AutoSize = True
        Me.LBL_GroupID.Location = New System.Drawing.Point(370, 50)
        Me.LBL_GroupID.Name = "LBL_GroupID"
        Me.LBL_GroupID.Size = New System.Drawing.Size(13, 17)
        Me.LBL_GroupID.TabIndex = 20
        Me.LBL_GroupID.Text = "-"
        '
        'Lbl_lastModified
        '
        Me.Lbl_lastModified.AutoSize = True
        Me.Lbl_lastModified.Location = New System.Drawing.Point(152, 307)
        Me.Lbl_lastModified.Name = "Lbl_lastModified"
        Me.Lbl_lastModified.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_lastModified.TabIndex = 19
        Me.Lbl_lastModified.Text = "-"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(18, 307)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(92, 17)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Last Modified"
        '
        'Lbl_DateCreated
        '
        Me.Lbl_DateCreated.AutoSize = True
        Me.Lbl_DateCreated.Location = New System.Drawing.Point(152, 280)
        Me.Lbl_DateCreated.Name = "Lbl_DateCreated"
        Me.Lbl_DateCreated.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_DateCreated.TabIndex = 17
        Me.Lbl_DateCreated.Text = "-"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(18, 280)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(92, 17)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Date Created"
        '
        'LBL_GUID
        '
        Me.LBL_GUID.AutoSize = True
        Me.LBL_GUID.Location = New System.Drawing.Point(152, 253)
        Me.LBL_GUID.Name = "LBL_GUID"
        Me.LBL_GUID.Size = New System.Drawing.Size(13, 17)
        Me.LBL_GUID.TabIndex = 15
        Me.LBL_GUID.Text = "-"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 253)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 17)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Group GUID"
        '
        'Chk_Active
        '
        Me.Chk_Active.AutoSize = True
        Me.Chk_Active.Location = New System.Drawing.Point(21, 216)
        Me.Chk_Active.Name = "Chk_Active"
        Me.Chk_Active.Size = New System.Drawing.Size(116, 21)
        Me.Chk_Active.TabIndex = 13
        Me.Chk_Active.Text = " Group Active"
        Me.Chk_Active.UseVisualStyleBackColor = True
        '
        'TB_DBPass
        '
        Me.TB_DBPass.Location = New System.Drawing.Point(155, 185)
        Me.TB_DBPass.Name = "TB_DBPass"
        Me.TB_DBPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TB_DBPass.Size = New System.Drawing.Size(209, 22)
        Me.TB_DBPass.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 185)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(134, 17)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Database Password"
        '
        'TB_DBUser
        '
        Me.TB_DBUser.Location = New System.Drawing.Point(155, 157)
        Me.TB_DBUser.Name = "TB_DBUser"
        Me.TB_DBUser.Size = New System.Drawing.Size(209, 22)
        Me.TB_DBUser.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 157)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(103, 17)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Database User"
        '
        'TB_Database
        '
        Me.TB_Database.Location = New System.Drawing.Point(155, 129)
        Me.TB_Database.Name = "TB_Database"
        Me.TB_Database.Size = New System.Drawing.Size(209, 22)
        Me.TB_Database.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 129)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 17)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Database Name"
        '
        'TB_ServerAddress
        '
        Me.TB_ServerAddress.Location = New System.Drawing.Point(155, 101)
        Me.TB_ServerAddress.Name = "TB_ServerAddress"
        Me.TB_ServerAddress.Size = New System.Drawing.Size(209, 22)
        Me.TB_ServerAddress.TabIndex = 6
        '
        'lbl_ServerAddress
        '
        Me.lbl_ServerAddress.AutoSize = True
        Me.lbl_ServerAddress.Location = New System.Drawing.Point(18, 101)
        Me.lbl_ServerAddress.Name = "lbl_ServerAddress"
        Me.lbl_ServerAddress.Size = New System.Drawing.Size(106, 17)
        Me.lbl_ServerAddress.TabIndex = 5
        Me.lbl_ServerAddress.Text = "Server Address"
        '
        'TB_LegalName
        '
        Me.TB_LegalName.Location = New System.Drawing.Point(155, 73)
        Me.TB_LegalName.Name = "TB_LegalName"
        Me.TB_LegalName.Size = New System.Drawing.Size(209, 22)
        Me.TB_LegalName.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Legal Name"
        '
        'TB_GroupName
        '
        Me.TB_GroupName.Location = New System.Drawing.Point(155, 45)
        Me.TB_GroupName.Name = "TB_GroupName"
        Me.TB_GroupName.Size = New System.Drawing.Size(209, 22)
        Me.TB_GroupName.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Group Name"
        '
        'TAB_Store
        '
        Me.TAB_Store.Controls.Add(Me.lbl_AutoLoaderID)
        Me.TAB_Store.Controls.Add(Me.btn_RefreshAuto)
        Me.TAB_Store.Controls.Add(Me.Button1)
        Me.TAB_Store.Controls.Add(Me.Label21)
        Me.TAB_Store.Controls.Add(Me.ComboBox1)
        Me.TAB_Store.Controls.Add(Me.CheckBox3)
        Me.TAB_Store.Controls.Add(Me.CB_AutoGRN)
        Me.TAB_Store.Controls.Add(Me.cb_AutoInvoice)
        Me.TAB_Store.Controls.Add(Me.TB_POSID)
        Me.TAB_Store.Controls.Add(Me.Label17)
        Me.TAB_Store.Controls.Add(Me.Button2)
        Me.TAB_Store.Controls.Add(Me.Btn_AddNew)
        Me.TAB_Store.Controls.Add(Me.lbl_StoreID)
        Me.TAB_Store.Controls.Add(Me.TB_LinkedCOID)
        Me.TAB_Store.Controls.Add(Me.Label10)
        Me.TAB_Store.Controls.Add(Me.TB_ShortName)
        Me.TAB_Store.Controls.Add(Me.Label12)
        Me.TAB_Store.Controls.Add(Me.TB_StoreName)
        Me.TAB_Store.Controls.Add(Me.Label13)
        Me.TAB_Store.Location = New System.Drawing.Point(4, 25)
        Me.TAB_Store.Name = "TAB_Store"
        Me.TAB_Store.Padding = New System.Windows.Forms.Padding(3)
        Me.TAB_Store.Size = New System.Drawing.Size(1191, 599)
        Me.TAB_Store.TabIndex = 1
        Me.TAB_Store.Text = "Store"
        Me.TAB_Store.UseVisualStyleBackColor = True
        '
        'TB_POSID
        '
        Me.TB_POSID.Location = New System.Drawing.Point(155, 130)
        Me.TB_POSID.Name = "TB_POSID"
        Me.TB_POSID.Size = New System.Drawing.Size(209, 22)
        Me.TB_POSID.TabIndex = 33
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(18, 130)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(54, 17)
        Me.Label17.TabIndex = 32
        Me.Label17.Text = "POS ID"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(200, 306)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(164, 39)
        Me.Button2.TabIndex = 31
        Me.Button2.Text = "Update Current"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Btn_AddNew
        '
        Me.Btn_AddNew.Location = New System.Drawing.Point(21, 306)
        Me.Btn_AddNew.Name = "Btn_AddNew"
        Me.Btn_AddNew.Size = New System.Drawing.Size(173, 39)
        Me.Btn_AddNew.TabIndex = 30
        Me.Btn_AddNew.Text = "Add New"
        Me.Btn_AddNew.UseVisualStyleBackColor = True
        '
        'lbl_StoreID
        '
        Me.lbl_StoreID.AutoSize = True
        Me.lbl_StoreID.Location = New System.Drawing.Point(370, 46)
        Me.lbl_StoreID.Name = "lbl_StoreID"
        Me.lbl_StoreID.Size = New System.Drawing.Size(13, 17)
        Me.lbl_StoreID.TabIndex = 29
        Me.lbl_StoreID.Text = "-"
        '
        'TB_LinkedCOID
        '
        Me.TB_LinkedCOID.Location = New System.Drawing.Point(155, 102)
        Me.TB_LinkedCOID.Name = "TB_LinkedCOID"
        Me.TB_LinkedCOID.Size = New System.Drawing.Size(209, 22)
        Me.TB_LinkedCOID.TabIndex = 26
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(18, 102)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(84, 17)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Company ID"
        '
        'TB_ShortName
        '
        Me.TB_ShortName.Location = New System.Drawing.Point(155, 74)
        Me.TB_ShortName.Name = "TB_ShortName"
        Me.TB_ShortName.Size = New System.Drawing.Size(209, 22)
        Me.TB_ShortName.TabIndex = 24
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(18, 74)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(83, 17)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Short Name"
        '
        'TB_StoreName
        '
        Me.TB_StoreName.Location = New System.Drawing.Point(155, 46)
        Me.TB_StoreName.Name = "TB_StoreName"
        Me.TB_StoreName.Size = New System.Drawing.Size(209, 22)
        Me.TB_StoreName.TabIndex = 22
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(18, 46)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(83, 17)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "Store Name"
        '
        'TAB_User
        '
        Me.TAB_User.Controls.Add(Me.GroupBox1)
        Me.TAB_User.Controls.Add(Me.Ckb_U_active)
        Me.TAB_User.Controls.Add(Me.lbl_DateModified)
        Me.TAB_User.Controls.Add(Me.Label18)
        Me.TAB_User.Controls.Add(Me.lbl_U_dateCreated)
        Me.TAB_User.Controls.Add(Me.Label20)
        Me.TAB_User.Controls.Add(Me.TB_U_eMailAddress)
        Me.TAB_User.Controls.Add(Me.Label16)
        Me.TAB_User.Controls.Add(Me.LBL_U_ID)
        Me.TAB_User.Controls.Add(Me.TB_U_Surname)
        Me.TAB_User.Controls.Add(Me.Label8)
        Me.TAB_User.Controls.Add(Me.TB_U_FirstName)
        Me.TAB_User.Controls.Add(Me.Label14)
        Me.TAB_User.Controls.Add(Me.TB_U_TenantName)
        Me.TAB_User.Controls.Add(Me.Label15)
        Me.TAB_User.Location = New System.Drawing.Point(4, 25)
        Me.TAB_User.Name = "TAB_User"
        Me.TAB_User.Padding = New System.Windows.Forms.Padding(3)
        Me.TAB_User.Size = New System.Drawing.Size(1191, 599)
        Me.TAB_User.TabIndex = 2
        Me.TAB_User.Text = "Users"
        Me.TAB_User.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_UpdateGroup)
        Me.GroupBox1.Controls.Add(Me.btn_UpdateRolesAssignments)
        Me.GroupBox1.Controls.Add(Me.ltv_Roles)
        Me.GroupBox1.Controls.Add(Me.Lbl_U_SelectedGroupID)
        Me.GroupBox1.Controls.Add(Me.Lbl_U_SelectedGroup)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 261)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(550, 318)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "User Role"
        '
        'btn_UpdateGroup
        '
        Me.btn_UpdateGroup.Location = New System.Drawing.Point(13, 248)
        Me.btn_UpdateGroup.Name = "btn_UpdateGroup"
        Me.btn_UpdateGroup.Size = New System.Drawing.Size(324, 29)
        Me.btn_UpdateGroup.TabIndex = 9
        Me.btn_UpdateGroup.Text = "Update Selected Role"
        Me.btn_UpdateGroup.UseVisualStyleBackColor = True
        '
        'btn_UpdateRolesAssignments
        '
        Me.btn_UpdateRolesAssignments.Location = New System.Drawing.Point(13, 213)
        Me.btn_UpdateRolesAssignments.Name = "btn_UpdateRolesAssignments"
        Me.btn_UpdateRolesAssignments.Size = New System.Drawing.Size(324, 29)
        Me.btn_UpdateRolesAssignments.TabIndex = 8
        Me.btn_UpdateRolesAssignments.Text = "Update Role Assignments"
        Me.btn_UpdateRolesAssignments.UseVisualStyleBackColor = True
        '
        'ltv_Roles
        '
        Me.ltv_Roles.CheckBoxes = True
        Me.ltv_Roles.Location = New System.Drawing.Point(13, 51)
        Me.ltv_Roles.Name = "ltv_Roles"
        Me.ltv_Roles.Size = New System.Drawing.Size(324, 156)
        Me.ltv_Roles.TabIndex = 7
        Me.ltv_Roles.UseCompatibleStateImageBehavior = False
        Me.ltv_Roles.View = System.Windows.Forms.View.List
        '
        'Lbl_U_SelectedGroupID
        '
        Me.Lbl_U_SelectedGroupID.AutoSize = True
        Me.Lbl_U_SelectedGroupID.Location = New System.Drawing.Point(278, 22)
        Me.Lbl_U_SelectedGroupID.Name = "Lbl_U_SelectedGroupID"
        Me.Lbl_U_SelectedGroupID.Size = New System.Drawing.Size(59, 17)
        Me.Lbl_U_SelectedGroupID.TabIndex = 2
        Me.Lbl_U_SelectedGroupID.Text = "Label17"
        Me.Lbl_U_SelectedGroupID.Visible = False
        '
        'Lbl_U_SelectedGroup
        '
        Me.Lbl_U_SelectedGroup.AutoSize = True
        Me.Lbl_U_SelectedGroup.Location = New System.Drawing.Point(129, 22)
        Me.Lbl_U_SelectedGroup.Name = "Lbl_U_SelectedGroup"
        Me.Lbl_U_SelectedGroup.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_U_SelectedGroup.TabIndex = 1
        Me.Lbl_U_SelectedGroup.Text = "-"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 17)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Selected Group: "
        '
        'Ckb_U_active
        '
        Me.Ckb_U_active.AutoSize = True
        Me.Ckb_U_active.Location = New System.Drawing.Point(17, 154)
        Me.Ckb_U_active.Name = "Ckb_U_active"
        Me.Ckb_U_active.Size = New System.Drawing.Size(116, 21)
        Me.Ckb_U_active.TabIndex = 43
        Me.Ckb_U_active.Text = "User is Active"
        Me.Ckb_U_active.UseVisualStyleBackColor = True
        '
        'lbl_DateModified
        '
        Me.lbl_DateModified.AutoSize = True
        Me.lbl_DateModified.Location = New System.Drawing.Point(148, 223)
        Me.lbl_DateModified.Name = "lbl_DateModified"
        Me.lbl_DateModified.Size = New System.Drawing.Size(13, 17)
        Me.lbl_DateModified.TabIndex = 42
        Me.lbl_DateModified.Text = "-"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(14, 223)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(92, 17)
        Me.Label18.TabIndex = 41
        Me.Label18.Text = "Last Modified"
        '
        'lbl_U_dateCreated
        '
        Me.lbl_U_dateCreated.AutoSize = True
        Me.lbl_U_dateCreated.Location = New System.Drawing.Point(148, 196)
        Me.lbl_U_dateCreated.Name = "lbl_U_dateCreated"
        Me.lbl_U_dateCreated.Size = New System.Drawing.Size(13, 17)
        Me.lbl_U_dateCreated.TabIndex = 40
        Me.lbl_U_dateCreated.Text = "-"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(14, 196)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(92, 17)
        Me.Label20.TabIndex = 39
        Me.Label20.Text = "Date Created"
        '
        'TB_U_eMailAddress
        '
        Me.TB_U_eMailAddress.Location = New System.Drawing.Point(151, 117)
        Me.TB_U_eMailAddress.Name = "TB_U_eMailAddress"
        Me.TB_U_eMailAddress.Size = New System.Drawing.Size(209, 22)
        Me.TB_U_eMailAddress.TabIndex = 38
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(14, 117)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(102, 17)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "e-Mail Address"
        '
        'LBL_U_ID
        '
        Me.LBL_U_ID.AutoSize = True
        Me.LBL_U_ID.Location = New System.Drawing.Point(366, 33)
        Me.LBL_U_ID.Name = "LBL_U_ID"
        Me.LBL_U_ID.Size = New System.Drawing.Size(13, 17)
        Me.LBL_U_ID.TabIndex = 36
        Me.LBL_U_ID.Text = "-"
        '
        'TB_U_Surname
        '
        Me.TB_U_Surname.Location = New System.Drawing.Point(151, 89)
        Me.TB_U_Surname.Name = "TB_U_Surname"
        Me.TB_U_Surname.Size = New System.Drawing.Size(209, 22)
        Me.TB_U_Surname.TabIndex = 35
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 89)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 17)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Surname"
        '
        'TB_U_FirstName
        '
        Me.TB_U_FirstName.Location = New System.Drawing.Point(151, 61)
        Me.TB_U_FirstName.Name = "TB_U_FirstName"
        Me.TB_U_FirstName.Size = New System.Drawing.Size(209, 22)
        Me.TB_U_FirstName.TabIndex = 33
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(14, 61)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(76, 17)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "First Name"
        '
        'TB_U_TenantName
        '
        Me.TB_U_TenantName.Location = New System.Drawing.Point(151, 33)
        Me.TB_U_TenantName.Name = "TB_U_TenantName"
        Me.TB_U_TenantName.Size = New System.Drawing.Size(209, 22)
        Me.TB_U_TenantName.TabIndex = 31
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(14, 33)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(79, 17)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "User Name"
        '
        'TAB_UserMapping
        '
        Me.TAB_UserMapping.Controls.Add(Me.GroupBox2)
        Me.TAB_UserMapping.Controls.Add(Me.DGV_UserMapping)
        Me.TAB_UserMapping.Location = New System.Drawing.Point(4, 25)
        Me.TAB_UserMapping.Name = "TAB_UserMapping"
        Me.TAB_UserMapping.Size = New System.Drawing.Size(1191, 599)
        Me.TAB_UserMapping.TabIndex = 3
        Me.TAB_UserMapping.Text = "UserMapping"
        Me.TAB_UserMapping.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Btn_RemoveUser)
        Me.GroupBox2.Controls.Add(Me.Btn_AddUser)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox2.Location = New System.Drawing.Point(0, 499)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1191, 100)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options"
        '
        'Btn_RemoveUser
        '
        Me.Btn_RemoveUser.Location = New System.Drawing.Point(165, 22)
        Me.Btn_RemoveUser.Name = "Btn_RemoveUser"
        Me.Btn_RemoveUser.Size = New System.Drawing.Size(140, 72)
        Me.Btn_RemoveUser.TabIndex = 1
        Me.Btn_RemoveUser.Text = "Remove User"
        Me.Btn_RemoveUser.UseVisualStyleBackColor = True
        '
        'Btn_AddUser
        '
        Me.Btn_AddUser.Location = New System.Drawing.Point(6, 21)
        Me.Btn_AddUser.Name = "Btn_AddUser"
        Me.Btn_AddUser.Size = New System.Drawing.Size(141, 73)
        Me.Btn_AddUser.TabIndex = 0
        Me.Btn_AddUser.Text = "New User"
        Me.Btn_AddUser.UseVisualStyleBackColor = True
        '
        'DGV_UserMapping
        '
        Me.DGV_UserMapping.AllowUserToAddRows = False
        Me.DGV_UserMapping.AllowUserToDeleteRows = False
        Me.DGV_UserMapping.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_UserMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_UserMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Map})
        Me.DGV_UserMapping.Location = New System.Drawing.Point(0, 0)
        Me.DGV_UserMapping.Name = "DGV_UserMapping"
        Me.DGV_UserMapping.RowTemplate.Height = 24
        Me.DGV_UserMapping.Size = New System.Drawing.Size(863, 493)
        Me.DGV_UserMapping.TabIndex = 0
        '
        'Map
        '
        Me.Map.HeaderText = "Map"
        Me.Map.Name = "Map"
        Me.Map.Text = "<<Map"
        Me.Map.UseColumnTextForButtonValue = True
        '
        'TAB_RoleDef
        '
        Me.TAB_RoleDef.Controls.Add(Me.GroupBox3)
        Me.TAB_RoleDef.Controls.Add(Me.lbl_Paytypes)
        Me.TAB_RoleDef.Controls.Add(Me.clb_PayTypes)
        Me.TAB_RoleDef.Controls.Add(Me.btn_RoleMap)
        Me.TAB_RoleDef.Controls.Add(Me.lbl_R_GrpID)
        Me.TAB_RoleDef.Controls.Add(Me.Btn_AddRole)
        Me.TAB_RoleDef.Controls.Add(Me.Label19)
        Me.TAB_RoleDef.Controls.Add(Me.CB_Forms)
        Me.TAB_RoleDef.Controls.Add(Me.TV_Role)
        Me.TAB_RoleDef.Controls.Add(Me.ListBox1)
        Me.TAB_RoleDef.Location = New System.Drawing.Point(4, 25)
        Me.TAB_RoleDef.Name = "TAB_RoleDef"
        Me.TAB_RoleDef.Size = New System.Drawing.Size(1191, 599)
        Me.TAB_RoleDef.TabIndex = 5
        Me.TAB_RoleDef.Text = "Roles"
        Me.TAB_RoleDef.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Btn)
        Me.GroupBox3.Controls.Add(Me.Btn_ExclRemove)
        Me.GroupBox3.Controls.Add(Me.Btn_ExclAdd)
        Me.GroupBox3.Controls.Add(Me.TB_AddExclusion)
        Me.GroupBox3.Controls.Add(Me.LstB_Exclusions)
        Me.GroupBox3.Location = New System.Drawing.Point(771, 56)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(315, 373)
        Me.GroupBox3.TabIndex = 13
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Payment Exclusion Types"
        '
        'Btn
        '
        Me.Btn.Location = New System.Drawing.Point(7, 295)
        Me.Btn.Name = "Btn"
        Me.Btn.Size = New System.Drawing.Size(292, 51)
        Me.Btn.TabIndex = 16
        Me.Btn.Text = "Save"
        Me.Btn.UseVisualStyleBackColor = True
        '
        'Btn_ExclRemove
        '
        Me.Btn_ExclRemove.Location = New System.Drawing.Point(6, 63)
        Me.Btn_ExclRemove.Name = "Btn_ExclRemove"
        Me.Btn_ExclRemove.Size = New System.Drawing.Size(292, 23)
        Me.Btn_ExclRemove.TabIndex = 15
        Me.Btn_ExclRemove.Text = "Remove Selected"
        Me.Btn_ExclRemove.UseVisualStyleBackColor = True
        '
        'Btn_ExclAdd
        '
        Me.Btn_ExclAdd.Location = New System.Drawing.Point(175, 33)
        Me.Btn_ExclAdd.Name = "Btn_ExclAdd"
        Me.Btn_ExclAdd.Size = New System.Drawing.Size(75, 23)
        Me.Btn_ExclAdd.TabIndex = 14
        Me.Btn_ExclAdd.Text = "Add"
        Me.Btn_ExclAdd.UseVisualStyleBackColor = True
        '
        'TB_AddExclusion
        '
        Me.TB_AddExclusion.Location = New System.Drawing.Point(7, 35)
        Me.TB_AddExclusion.Name = "TB_AddExclusion"
        Me.TB_AddExclusion.Size = New System.Drawing.Size(161, 22)
        Me.TB_AddExclusion.TabIndex = 13
        '
        'LstB_Exclusions
        '
        Me.LstB_Exclusions.FormattingEnabled = True
        Me.LstB_Exclusions.ItemHeight = 16
        Me.LstB_Exclusions.Location = New System.Drawing.Point(7, 95)
        Me.LstB_Exclusions.Name = "LstB_Exclusions"
        Me.LstB_Exclusions.Size = New System.Drawing.Size(292, 180)
        Me.LstB_Exclusions.TabIndex = 12
        '
        'lbl_Paytypes
        '
        Me.lbl_Paytypes.AutoSize = True
        Me.lbl_Paytypes.Location = New System.Drawing.Point(307, 555)
        Me.lbl_Paytypes.Name = "lbl_Paytypes"
        Me.lbl_Paytypes.Size = New System.Drawing.Size(13, 17)
        Me.lbl_Paytypes.TabIndex = 11
        Me.lbl_Paytypes.Text = "-"
        '
        'clb_PayTypes
        '
        Me.clb_PayTypes.FormattingEnabled = True
        Me.clb_PayTypes.Location = New System.Drawing.Point(307, 306)
        Me.clb_PayTypes.Name = "clb_PayTypes"
        Me.clb_PayTypes.Size = New System.Drawing.Size(435, 242)
        Me.clb_PayTypes.TabIndex = 10
        '
        'btn_RoleMap
        '
        Me.btn_RoleMap.Location = New System.Drawing.Point(242, 251)
        Me.btn_RoleMap.Name = "btn_RoleMap"
        Me.btn_RoleMap.Size = New System.Drawing.Size(59, 23)
        Me.btn_RoleMap.TabIndex = 9
        Me.btn_RoleMap.Text = ">>"
        Me.btn_RoleMap.UseVisualStyleBackColor = True
        '
        'lbl_R_GrpID
        '
        Me.lbl_R_GrpID.AutoSize = True
        Me.lbl_R_GrpID.Location = New System.Drawing.Point(477, 21)
        Me.lbl_R_GrpID.Name = "lbl_R_GrpID"
        Me.lbl_R_GrpID.Size = New System.Drawing.Size(16, 17)
        Me.lbl_R_GrpID.TabIndex = 7
        Me.lbl_R_GrpID.Text = "0"
        '
        'Btn_AddRole
        '
        Me.Btn_AddRole.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_AddRole.Location = New System.Drawing.Point(307, 56)
        Me.Btn_AddRole.Name = "Btn_AddRole"
        Me.Btn_AddRole.Size = New System.Drawing.Size(435, 32)
        Me.Btn_AddRole.TabIndex = 5
        Me.Btn_AddRole.Text = "Add Role"
        Me.Btn_AddRole.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(10, 21)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(120, 17)
        Me.Label19.TabIndex = 4
        Me.Label19.Text = "Application Forms"
        '
        'CB_Forms
        '
        Me.CB_Forms.FormattingEnabled = True
        Me.CB_Forms.Location = New System.Drawing.Point(136, 14)
        Me.CB_Forms.Name = "CB_Forms"
        Me.CB_Forms.Size = New System.Drawing.Size(321, 24)
        Me.CB_Forms.TabIndex = 3
        '
        'TV_Role
        '
        Me.TV_Role.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.TV_Role.ContextMenuStrip = Me.CM_RoleTask
        Me.TV_Role.Location = New System.Drawing.Point(307, 91)
        Me.TV_Role.Name = "TV_Role"
        Me.TV_Role.Size = New System.Drawing.Size(435, 208)
        Me.TV_Role.TabIndex = 1
        '
        'CM_RoleTask
        '
        Me.CM_RoleTask.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CM_RoleTask.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RemoveRoleTaskToolStripMenuItem})
        Me.CM_RoleTask.Name = "CM_RoleTask"
        Me.CM_RoleTask.Size = New System.Drawing.Size(198, 28)
        '
        'RemoveRoleTaskToolStripMenuItem
        '
        Me.RemoveRoleTaskToolStripMenuItem.Name = "RemoveRoleTaskToolStripMenuItem"
        Me.RemoveRoleTaskToolStripMenuItem.Size = New System.Drawing.Size(197, 24)
        Me.RemoveRoleTaskToolStripMenuItem.Text = "Remove Role Task"
        '
        'ListBox1
        '
        Me.ListBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Location = New System.Drawing.Point(13, 56)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(223, 500)
        Me.ListBox1.TabIndex = 0
        '
        'cb_AutoInvoice
        '
        Me.cb_AutoInvoice.AutoSize = True
        Me.cb_AutoInvoice.Location = New System.Drawing.Point(21, 202)
        Me.cb_AutoInvoice.Name = "cb_AutoInvoice"
        Me.cb_AutoInvoice.Size = New System.Drawing.Size(107, 21)
        Me.cb_AutoInvoice.TabIndex = 34
        Me.cb_AutoInvoice.Text = "Auto Invoice"
        Me.cb_AutoInvoice.UseVisualStyleBackColor = True
        '
        'CB_AutoGRN
        '
        Me.CB_AutoGRN.AutoSize = True
        Me.CB_AutoGRN.Location = New System.Drawing.Point(21, 230)
        Me.CB_AutoGRN.Name = "CB_AutoGRN"
        Me.CB_AutoGRN.Size = New System.Drawing.Size(94, 21)
        Me.CB_AutoGRN.TabIndex = 35
        Me.CB_AutoGRN.Text = "Auto GRN"
        Me.CB_AutoGRN.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(21, 257)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(100, 21)
        Me.CheckBox3.TabIndex = 36
        Me.CheckBox3.Text = "CheckBox3"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(155, 158)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(209, 24)
        Me.ComboBox1.TabIndex = 37
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(18, 165)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(73, 17)
        Me.Label21.TabIndex = 38
        Me.Label21.Text = "POS Type"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(182, 202)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(182, 59)
        Me.Button1.TabIndex = 39
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btn_RefreshAuto
        '
        Me.btn_RefreshAuto.Location = New System.Drawing.Point(182, 267)
        Me.btn_RefreshAuto.Name = "btn_RefreshAuto"
        Me.btn_RefreshAuto.Size = New System.Drawing.Size(182, 23)
        Me.btn_RefreshAuto.TabIndex = 40
        Me.btn_RefreshAuto.Text = "Refresh"
        Me.btn_RefreshAuto.UseVisualStyleBackColor = True
        '
        'lbl_AutoLoaderID
        '
        Me.lbl_AutoLoaderID.AutoSize = True
        Me.lbl_AutoLoaderID.Location = New System.Drawing.Point(373, 202)
        Me.lbl_AutoLoaderID.Name = "lbl_AutoLoaderID"
        Me.lbl_AutoLoaderID.Size = New System.Drawing.Size(16, 17)
        Me.lbl_AutoLoaderID.TabIndex = 41
        Me.lbl_AutoLoaderID.Text = "0"
        '
        'Frm_Administration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1565, 653)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TreeView1)
        Me.Name = "Frm_Administration"
        Me.Text = "Administration"
        Me.TB_ContextMenu.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TAB_Group.ResumeLayout(False)
        Me.TAB_Group.PerformLayout()
        Me.TAB_Store.ResumeLayout(False)
        Me.TAB_Store.PerformLayout()
        Me.TAB_User.ResumeLayout(False)
        Me.TAB_User.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TAB_UserMapping.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DGV_UserMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TAB_RoleDef.ResumeLayout(False)
        Me.TAB_RoleDef.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.CM_RoleTask.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TAB_Group As System.Windows.Forms.TabPage
    Friend WithEvents TAB_Store As System.Windows.Forms.TabPage
    Friend WithEvents TAB_User As System.Windows.Forms.TabPage
    Friend WithEvents TAB_UserMapping As System.Windows.Forms.TabPage
    Friend WithEvents LBL_GroupID As System.Windows.Forms.Label
    Friend WithEvents Lbl_lastModified As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Lbl_DateCreated As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents LBL_GUID As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Chk_Active As System.Windows.Forms.CheckBox
    Friend WithEvents TB_DBPass As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TB_DBUser As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TB_Database As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TB_ServerAddress As System.Windows.Forms.TextBox
    Friend WithEvents lbl_ServerAddress As System.Windows.Forms.Label
    Friend WithEvents TB_LegalName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_GroupName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Btn_NewGrp As System.Windows.Forms.Button
    Friend WithEvents Btn_ActiveateNew As System.Windows.Forms.Button
    Friend WithEvents Btn_Update As System.Windows.Forms.Button
    Friend WithEvents lbl_StoreID As System.Windows.Forms.Label
    Friend WithEvents TB_LinkedCOID As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TB_ShortName As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TB_StoreName As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents DGV_UserMapping As System.Windows.Forms.DataGridView
    Friend WithEvents lbl_DateModified As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lbl_U_dateCreated As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TB_U_eMailAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents LBL_U_ID As System.Windows.Forms.Label
    Friend WithEvents TB_U_Surname As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TB_U_FirstName As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TB_U_TenantName As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Ckb_U_active As System.Windows.Forms.CheckBox
    Friend WithEvents TAB_RoleDef As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Lbl_U_SelectedGroup As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Lbl_U_SelectedGroupID As System.Windows.Forms.Label
    Friend WithEvents TV_Role As System.Windows.Forms.TreeView
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents CB_Forms As System.Windows.Forms.ComboBox
    Friend WithEvents Btn_AddRole As System.Windows.Forms.Button
    Friend WithEvents lbl_R_GrpID As System.Windows.Forms.Label
    Friend WithEvents btn_RoleMap As System.Windows.Forms.Button
    Friend WithEvents Btn_AddNew As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ltv_Roles As System.Windows.Forms.ListView
    Friend WithEvents btn_UpdateRolesAssignments As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_AddUser As System.Windows.Forms.Button
    Friend WithEvents clb_PayTypes As System.Windows.Forms.CheckedListBox
    Friend WithEvents lbl_Paytypes As System.Windows.Forms.Label
    Friend WithEvents btn_UpdateGroup As System.Windows.Forms.Button
    Friend WithEvents TB_ContextMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TS_RemoveUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Btn_RemoveUser As System.Windows.Forms.Button
    Friend WithEvents CM_RoleTask As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents RemoveRoleTaskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TreeImageList As System.Windows.Forms.ImageList
    Friend WithEvents Map As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents TB_POSID As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn As System.Windows.Forms.Button
    Friend WithEvents Btn_ExclRemove As System.Windows.Forms.Button
    Friend WithEvents Btn_ExclAdd As System.Windows.Forms.Button
    Friend WithEvents TB_AddExclusion As System.Windows.Forms.TextBox
    Friend WithEvents LstB_Exclusions As System.Windows.Forms.ListBox
    Friend WithEvents Label21 As Windows.Forms.Label
    Friend WithEvents ComboBox1 As Windows.Forms.ComboBox
    Friend WithEvents CheckBox3 As Windows.Forms.CheckBox
    Friend WithEvents CB_AutoGRN As Windows.Forms.CheckBox
    Friend WithEvents cb_AutoInvoice As Windows.Forms.CheckBox
    Friend WithEvents btn_RefreshAuto As Windows.Forms.Button
    Friend WithEvents Button1 As Windows.Forms.Button
    Friend WithEvents lbl_AutoLoaderID As Windows.Forms.Label
End Class
