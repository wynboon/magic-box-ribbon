﻿Imports System.Data
Imports MB.PasswordSecurity
Imports System.Data.SqlClient
Imports System.Windows.Forms

Module Mod_Secure
    Public LoginUsername As String
    Public GroupID As Long
    Public MagicBox_CommonDB As String = "Server=41.72.128.100;Database=MagicBox_Central;Uid=sa; Pwd=Bu1ldSm@rt"
    Function ValidateLogon(Username As String, Password As String) As DataSet
        Dim SecureP As New PasswordSecurity
        Dim EncryptPass As String
        Dim sSQL As String

        EncryptPass = SecureP.EncryptString(Password)
        sSQL = "Select Distinct * From [UserLogin] where [TenantName] =  '" & Username & "' and " & _
        "[Password] = '" & EncryptPass & "'"

        Dim cn As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "CashPay")
        cn.Close()
        Return ds
    End Function
    Function GetTenantINFO(TenantID As String) As DataSet
        Dim sSQL As String

        sSQL = "Select Distinct * From [Tenants] where [TenantID] =  '" & TenantID & "'"

        Dim cn As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "CashPay")
        cn.Close()
        Return ds
    End Function
    Function GetGroupInfo(GroupID As String) As String()
        Dim sSQL As String

        sSQL = "SELECT TOP 1 [GroupServerAddress],[DatabaseName],[DatabaseUserName],[DatabasePassword] FROM [Groups] Where GroupID =  '" & GroupID & "'"

        Dim cn As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "CashPay")
        cn.Close()
        Return {ds.Tables(0).Rows(0).Item(0), ds.Tables(0).Rows(0).Item(1), ds.Tables(0).Rows(0).Item(2), ds.Tables(0).Rows(0).Item(3)}
    End Function
    Function TenantRoles(TenantID As Long) As DataSet
        Dim sSQL As String

        sSQL = "Select * From [UserRoles] where [TenantID] =  '" & TenantID & "'"

        Dim cn As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "Roles")
        cn.Close()
        Return ds
    End Function
    Function GetTaxTables() As DataSet
        Dim sSQL As String

        sSQL = "Select * From [TaxTypes] where [Co_ID] =  '" & My.Settings.Setting_CompanyID & "'"

        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "TT")
        cn.Close()
        Return ds
    End Function
    Function GetAccountsTables() As DataSet
        Dim sSQL As String

        sSQL = "Select * From [Accounting] where [Co_ID] =  '" & My.Settings.Setting_CompanyID & "'"

        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "AT")
        cn.Close()
        Return ds
    End Function
    Function AddGroupStore(ByVal StoreName As String, ShortName As String, Co_ID As Long, GroupID As Long, POS_ID As String) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = strSQL & "Insert Into [GroupStores] ([StoreName],[ShortName],[DateCreated],[DateModified],[LinkCo_ID],[GroupID],[LinkPOS_ID]) Values (" & _
                "'" & StoreName & "','" & ShortName & "','" & Now.ToString("dd MMM yyyy") & "','" & Now.ToString("dd MMM yyyy") & "','" & Co_ID & "','" & GroupID & "','" & POS_ID & "')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error adding the store! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function AddTenant(ByVal TenantName As String, FirstName As String, Surname As String, eMail As String, DefaultGroupID As Long) As Long
        Try

            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer
            Dim strSQL As String = ""
            strSQL = strSQL & "Insert Into [Tenants] ([TenantName],[FirstName],[Surname],[Email],[CreateDate],[ModifiedDate],[IsActive],[ResetPassword],[DefaultGroup]) Values (" & _
                "'" & TenantName & "','" & FirstName & "','" & Surname & "','" & eMail & "','" & Now.ToString("dd MMM yyyy") & "','" & Now.ToString("dd MMM yyyy") & "','TRUE','TRUE'," & DefaultGroupID & ")"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.CommandText = strSQL
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            connection.Close()
            Return ID
        Catch ex As Exception
            MsgBox("There was an error adding the user! Error: " & ex.Message)
            Return 0
        End Try
    End Function
    Function AddGroup(ByVal GroupName As String, LegalName As String, GroupServerAddress As String, DatabaseName As String, DatabaseUserName As String, DatabasePassword As String, isActive As Boolean) As Boolean
        Try
            Dim SecureP As New PasswordSecurity
            Dim EncryptPass As String
            EncryptPass = SecureP.EncryptString(DatabasePassword)
            Dim strSQL As String = ""
            strSQL = strSQL & "Insert Into [Groups] ([GroupName],[LegalName],[GroupServerAddress],[DatabaseName],[DatabaseUserName],[DatabasePassword]" & _
      ",[DateCreated],[DateModified],[IsGroupActive],[ContainerID],[GroupGUID]) Values (" & _
                "'" & GroupName & "','" & LegalName & "','" & GroupServerAddress & "','" & DatabaseName & "'," & _
                "'" & DatabaseUserName & "','" & EncryptPass & "','" & Now.ToString("dd MMM yyyy") & "','" & Now.ToString("dd MMM yyyy") & "','TRUE','0','" & Guid.NewGuid.ToString & "')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error adding the Group! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function ValidateAddress(ByVal EmailAddress As String) As String
        Try
            Dim strSQL As String = ""
            strSQL = "Select top 1 TenantID From [Tenants] where [email] = '" & EmailAddress & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            ValidateAddress = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error looking up the email address! Error: " & ex.Message)
        End Try
    End Function
    Function ValidateRoleName(ByVal RoleName As String, GroupID As Long) As Boolean
        Try
            Dim RName As String
            Dim strSQL As String = ""
            strSQL = "Select top 1 RoleID From [Roles_Header] where [RoleName] = '" & RoleName & "' and GroupID = " & GroupID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            RName = cmd.ExecuteScalar()
            connection.Close()
            If RName = "" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the email address! Error: " & ex.Message)
        End Try
    End Function
    Function ChangePassword(ByVal TenantID As Integer, Password As String, ForceNewPassword As Boolean) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Update [Tenants] set Password = '" & Password & "', ResetPassword = '" & ForceNewPassword & "' Where [TenantID] = '" & TenantID & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            ChangePassword = True
        Catch ex As Exception
            ChangePassword = False
            MsgBox("There was an error updating tenant password! Error: " & ex.Message)
        End Try
    End Function
    Function StoreSettings(StoreID As String) As DataSet
        Dim SecureP As New PasswordSecurity
        Dim EncryptPass As String
        Dim sSQL As String


        sSQL = "Select Top 1 * From [GroupStore_Settings] where [StoreID] =  '" & StoreID
        Dim cn As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "CashPay")
        cn.Close()
        Return ds
    End Function
    Function IsUserinStore(UserID As Long, StoreID As Long)
        Dim RName As String
        Dim strSQL As String = ""
        strSQL = "Select top 1 TenantID From [TenantStores] where [TenantID] = '" & UserID & "' and StoreID = " & StoreID

        Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(strSQL, connection)
        cmd.CommandTimeout = 300
        connection.Open()
        RName = cmd.ExecuteScalar()
        If IsDBNull(RName) Then RName = ""
        connection.Close()
        If RName = "" Then
            Return False
        Else
            Return True
        End If
    End Function
    Function MapUser2Group(ByVal TenantID As Integer, StoreID As Long) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Insert Into [TenantStores] ([TenantID],[StoreID],[isAdmin],[DateCreated],[DateModified],[IsActive],[Comment]) Values (" & _
                "'" & TenantID & "','" & StoreID & "','FALSE','" & Now.ToString("dd MMM yyyy") & "','" & Now.ToString("dd MMM yyyy") & "','True','')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error looking up the email address! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function MapUser2Role(ByVal TenantID As Integer, RoleID As Long) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Insert Into [Roles_ToTenant] ([TenantID],[RoleID]) Values (" & _
                "'" & TenantID & "','" & RoleID & "')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error looking up the email address! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function MapUser2RoleClear(ByVal TenantID As Integer, GroupID As Long) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Delete R2T FROM [Roles_ToTenant] R2T Left Join Roles_Header on R2T.RoleID = Roles_Header.RoleID " & _
                    "Where TenantID = " & TenantID & " and GroupID = " & GroupID
            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error clearing roles! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function MapRoleGontrol(ByVal RoleID As Integer, FormName As String, Tag As String) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Insert Into [Roles_ControlToRole] ([RoleID],[FormName],[ControlTag]) Values (" & _
                "'" & RoleID & "','" & FormName & "','S_" & Tag & "')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error looking up the email address! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function UpdateGroup(ByVal GroupID As Integer, GroupName As String, LegalName As String, ServerAddress As String, Database As String, DBUser As String, DBPass As String _
                      , isactive As Boolean) As Boolean
        Try
            Dim SecPass As New PasswordSecurity
            Dim strSQL As String = ""
            strSQL = "Update [Groups] set GroupName = '" & GroupName & "', LegalName = '" & LegalName & "', " & _
                "GroupServerAddress = '" & ServerAddress & "', DatabaseName = '" & Database & "', DatabaseUserName = '" & DBUser & "', DatabasePassword = '" & SecPass.EncryptString(DBPass) & "'," & _
                "IsGroupActive = '" & isactive & "',DateModified = '" & Now.ToString("dd MMM yyyy") & "'  Where [GroupID] = '" & GroupID & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error looking up the email address! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function UpdateStore(ByVal StoreID As Integer, StoreName As String, ShortName As String, LinkCo_ID As String, POS_ID As String) As Boolean
        Try
            Dim SecPass As New PasswordSecurity
            Dim strSQL As String = ""
            strSQL = "Update [GroupStores] set StoreName = '" & StoreName & "', ShortName = '" & ShortName & "', " & _
                "DateModified = '" & Now.ToString("dd MMM yyyy") & "', LinkCo_ID = " & LinkCo_ID & ", LinkPOS_ID = '" & POS_ID & "' Where [StoreID] = '" & StoreID & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error updating the store! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function UpdateLastGroup(ByVal TenantID As Integer, GroupID As String, StoreID As String) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Update [Tenants] set LastGroupID = '" & GroupID & "', LastCoID = '" & StoreID & "', " & _
                "LastDate = '" & Now().ToString("dd MMM yyyy") & "'  Where [TenantID] = '" & TenantID & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error updateing last entries! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function ResetPassword(ByVal TenantID As Integer, SecureNewPassword As String) As Boolean
        Try
            Dim strSQL As String = ""
            strSQL = "Update [Tenants] set Password = '" & SecureNewPassword & "',ResetPassword = 'False' Where [TenantID] = '" & TenantID & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error updateing updating password! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function NewPassword(TenantID As Long, email As String)
        Try
            Dim PassSecure As New PasswordSecurity
            Dim strNewPassword As String
            Dim SecurePassword As String
            strNewPassword = CreateRandomPassword(8)
            SecurePassword = PassSecure.EncryptString(strNewPassword)
            ChangePassword(TenantID, SecurePassword, True)
            Comm_SendComm(2, email, "Your new Magic Box Password is: " & strNewPassword)
        Catch ex As Exception
            MsgBox("Error with new password ,Error: " & ex.Message)
        End Try
    End Function
    Function RemoveTenantFromOrg(TenantID As Long, StoreID As Long)
        Try
            Dim strSQL As String = ""
            strSQL = "Delete From [TenantStores] Where TenantID = '" & TenantID & "' and StoreID = '" & StoreID & "'"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error removing user from store! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function RemoveTenantFull(TenantID As Long)
        Try
            Dim strSQL As String = ""
            strSQL = "Delete From [TenantStores] Where TenantID = " & TenantID & vbNewLine & _
                "Delete From [Tenants] Where TenantID = " & TenantID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error removing user! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function IsControlOpen(Control_Tag As String, FormName_Tag As String) As Boolean
        For Each row As DataRow In Globals.Ribbons.Ribbon1.LoggedUsersRoles.Rows
            ' MsgBox(row("GroupID") & " " & Globals.Ribbons.Ribbon1.CurrentGroupID)
            If row("GroupID") = Globals.Ribbons.Ribbon1.CurrentGroupID Then
                Dim RoleControlTag As String = If(IsDBNull(row("ControlTag")), "", row("ControlTag"))
                If FormName_Tag = "S_" & row("FormName").ToString.Replace(" ", "&") And Control_Tag = RoleControlTag Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function

    Private Sub DoAction(c As Control, Form_Tag As String)
        If IsNothing(c.Tag) Then
            c.Enabled = True
        Else
            If IsControlOpen(c.Tag.ToString, Form_Tag) = True Then
                c.Enabled = True
            Else
                c.Enabled = False
            End If
        End If
    End Sub
    Public Function GetGroupUsers() As DataSet
        Dim sSQL As String

        sSQL = "SELECT Distinct TenantID as Value, [FirstName] + ' ' + [Surname] as Display FROM [UserLogin] Where GroupID = " & My.Settings.GroupID

        Dim cn As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=300")
        Dim cmd As New SqlCommand(sSQL, cn)
        Dim ds As New DataSet()
        Dim DA As New SqlDataAdapter(sSQL, cn)
        cn.Open()
        DA.Fill(ds, "AT")
        cn.Close()
        Return ds
    End Function
    Function LockForm(f As Form)
        If IsNothing(f.Tag) Then Exit Function
        For Each control As Control In f.Controls
            LoopLockForm(control, f)
        Next
    End Function
    Private Sub LoopLockForm(c As Control, f As Form)
        DoAction(c, f.Tag.ToString.ToString)
        For Each control As Control In c.Controls
            LoopLockForm(control, f)
        Next
    End Sub
    Function UpdatePaytypeExclusions(RoleID As Long, PayExclusions As String)
        Try
            Dim strSQL As String = ""
            If GetPaytypeExclusionsID(RoleID) = 0 Then
                strSQL = "Insert Into [Role_PayTypes] (RoleID,PaymentExclusion) Values ('" & RoleID & "','" & PayExclusions & "')"
            Else
                strSQL = "Update [Role_PayTypes] Set PaymentExclusion = '" & PayExclusions & "' Where RoleID = " & RoleID
            End If

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            connection.Close()
            MsgBox("Saved")
        Catch ex As Exception
            MsgBox("There was an error removing user! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function GetPaytypeExclusionsID(RoleID As Long) As Long
        Try
            Dim strSQL As String = ""
            Dim Res
            strSQL = "Select Top 1 RoleID From [Role_PayTypes] Where RoleID = " & RoleID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Res = cmd.ExecuteScalar()
            connection.Close()
            If Res = Nothing Then
                Return 0
            Else
                Return Res.ToString
            End If

        Catch ex As Exception
            MsgBox("There was an error removing GetPaytypeExclusionsID! Error: " & ex.Message)
            Return False
        End Try
    End Function
    Function GetPaytypeExclusionsByID(RoleID As Long) As String()
        Try
            Dim strSQL As String = ""
            Dim Res
            strSQL = "Select Top 1 PaymentExclusion From [Role_PayTypes] Where RoleID = " & RoleID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Res = cmd.ExecuteScalar()
            connection.Close()
            If Res = Nothing Then
                Return {""}
            Else
                Return Split(Res.ToString, "|")
            End If

        Catch ex As Exception
            MsgBox("There was an error removing GetPaytypeExclusionsByID! Error: " & ex.Message)
            Return {""}
        End Try
    End Function
End Module
