﻿Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient

Public Class Frm_Administration
    Private Sub Frm_Administration_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BuildTree()
        Call LoadUserMapping()
    End Sub
    Function BuildTree()
        TreeView1.Nodes.Clear()
        Dim dt As New DataTable
        'Fill Groups
        dt = GroupsFillDT()
        For Each dr As DataRow In dt.Rows
            AddNode("Home", "Home", "G" & dr("GroupID").ToString, dr("GroupName").ToString, "G" & dr("GroupID").ToString, 0, True)
        Next
        'Fill Stores
        dt = StoresFillDT()
        For Each dr As DataRow In dt.Rows
            AddNode("G" & dr("GroupID").ToString, dr("GroupName").ToString, "S" & dr("StoreID").ToString, dr("StoreName").ToString, "S" & dr("StoreID").ToString, 1, False)

        Next
        'Fill Users
        dt = TenantFillDT()
        For Each dr As DataRow In dt.Rows
            AddNode("S" & dr("StoreID").ToString, dr("StoreName").ToString, "T" & dr("TenantID").ToString, dr("TenantName").ToString, "T" & dr("TenantID").ToString, 2, False)
        Next

    End Function
    Private Sub AddNode(ParentKey As String, parentText As String, ChildKey As String, ChildText As String, ChildTag As String, IMIndex As Integer, ExpNode As Boolean)
        Dim node As New List(Of TreeNode)
        Dim InsertNode As New TreeNode
        node.AddRange(TreeView1.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            node.Add(TreeView1.Nodes.Add(ParentKey, parentText))
        End If
        InsertNode.Name = ChildKey
        InsertNode.Text = ChildText
        InsertNode.Tag = ChildTag
        InsertNode.ImageIndex = IMIndex
        node(0).Nodes.Add(InsertNode)
        If ExpNode = True Then
            node(0).Expand()
        End If

    End Sub
    Private Sub AddRoleNode(ParentKey As String, parentText As String, ChildKey As String, ChildText As String, ChildTag As String, imIndex As Integer)
        Dim node As New List(Of TreeNode)
        Dim InsertNode As New TreeNode
        node.AddRange(TV_Role.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            node.Add(TV_Role.Nodes.Add(ParentKey, parentText))
        End If
        InsertNode.Name = ChildKey
        InsertNode.Text = ChildText
        InsertNode.Tag = ChildTag
        InsertNode.ImageIndex = imIndex
        node(0).Nodes.Add(InsertNode)

    End Sub
    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If IsNothing(TreeView1.SelectedNode.Tag) Then Exit Sub
            Dim TheTag As String = TreeView1.SelectedNode.Tag
            If TheTag.Substring(0, 1) = "G" Then
                LoadGroup(TheTag.Remove(0, 1))
                If TabControl1.SelectedTab.Name <> "TAB_UserMapping" Then
                    TabControl1.SelectTab("TAB_Group")
                End If
                lbl_R_GrpID.Text = TheTag.Remove(0, 1)
                If lbl_R_GrpID.Text = "0" Then
                    MsgBox("No group selected")
                End If
                BuildRollTree()
                ListForms()
            End If
            If TheTag.Substring(0, 1) = "S" Then
                LoadStore(TheTag.Remove(0, 1))
                If TabControl1.SelectedTab.Name <> "TAB_UserMapping" Then
                    TabControl1.SelectTab("TAB_Store")
                End If
            End If
            If TheTag.Substring(0, 1) = "T" Then
                LoadUser(TheTag.Remove(0, 1))
                Lbl_U_SelectedGroup.Text = TreeView1.SelectedNode.Parent.Parent.Text
                Lbl_U_SelectedGroupID.Text = TreeView1.SelectedNode.Parent.Parent.Tag.ToString
                LoadUserRoules(Lbl_U_SelectedGroupID.Text.Remove(0, 1))
                If TabControl1.SelectedTab.Name <> "TAB_UserMapping" Then
                    TabControl1.SelectTab("TAB_User")
                End If

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function GroupsFillDT() As DataTable
        Try
            Dim strSql As String = "select Distinct Groups.[GroupID],Groups.[GroupName] " & _
            "FROM Groups left join GroupStores On Groups.GroupID = GroupStores.GroupID " & _
            "left Join TenantStores On TenantStores.StoreID = GroupStores.StoreID " & _
            "Left Join Tenants on Tenants.TenantID = TenantStores.TenantID " & _
            "Where Groups.[GroupID] is not null order by GroupName"

            Dim dtb As New DataTable
            Using cnn As New SqlConnection(MagicBox_CommonDB)
                cnn.Open()
                Using dad As New SqlDataAdapter(strSql, cnn)
                    dad.Fill(dtb)
                    GroupsFillDT = dtb
                End Using
                cnn.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function StoresFillDT() As DataTable
        Try
            Dim strSql As String = "select Distinct Groups.[GroupID],Groups.[GroupName],GroupStores.Storename,GroupStores.StoreID " & _
            "FROM Groups left join GroupStores On Groups.GroupID = GroupStores.GroupID " & _
            "left Join TenantStores On TenantStores.StoreID = GroupStores.StoreID " & _
            "Left Join Tenants on Tenants.TenantID = TenantStores.TenantID " & _
            "Where GroupStores.StoreID is not null order by GroupName"
            Dim dtb As New DataTable
            Using cnn As New SqlConnection(MagicBox_CommonDB)
                cnn.Open()
                Using dad As New SqlDataAdapter(strSql, cnn)
                    dad.Fill(dtb)
                    StoresFillDT = dtb
                End Using
                cnn.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function RolesFillDT(GroupID As Integer) As DataTable
        Try
            Dim strSql As String = "SELECT * from Roles_Header left join Roles_ToTenant on Roles_Header.RoleID = Roles_ToTenant.RoleID " & _
    "left Join Tenants on Tenants.TenantID = Roles_ToTenant.TenantID Where Roles_Header.GroupID =" & GroupID
            Dim dtb As New DataTable
            Using cnn As New SqlConnection(MagicBox_CommonDB)
                cnn.Open()
                Using dad As New SqlDataAdapter(strSql, cnn)
                    dad.Fill(dtb)
                    RolesFillDT = dtb
                End Using
                cnn.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function TenantFillDT() As DataTable
        Try
            Dim strSql As String = "select  GroupStores.Storename,GroupStores.StoreID,Tenants.TenantID, Tenants.FirstName + ' ' + Tenants.Surname as TenantName  " & _
            "FROM Groups left join GroupStores On Groups.GroupID = GroupStores.GroupID " & _
            "left Join TenantStores On TenantStores.StoreID = GroupStores.StoreID " & _
            "Left Join Tenants on Tenants.TenantID = TenantStores.TenantID " & _
            "Where Tenants.TenantID is not null"
            Dim dtb As New DataTable
            Using cnn As New SqlConnection(MagicBox_CommonDB)
                cnn.Open()
                Using dad As New SqlDataAdapter(strSql, cnn)
                    dad.Fill(dtb)
                    TenantFillDT = dtb
                End Using
                cnn.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Function LoadGroup(GroupID As Integer)
        Dim SecureEnc As New PasswordSecurity
        Try
            Dim sSQL As String

            sSQL = "SELECT Top 1 * FROM Groups WHERE GroupID = " & GroupID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            LBL_GroupID.Text = GroupID

            While datareader.Read
                If Not datareader("GroupName").Equals(DBNull.Value) Then
                    TB_GroupName.Text = datareader("GroupName")
                Else
                    TB_GroupName.Text = ""
                End If
                If Not datareader("LegalName").Equals(DBNull.Value) Then
                    TB_LegalName.Text = datareader("LegalName")
                Else
                    TB_LegalName.Text = ""
                End If
                If Not datareader("GroupServerAddress").Equals(DBNull.Value) Then
                    TB_ServerAddress.Text = datareader("GroupServerAddress")
                Else
                    TB_ServerAddress.Text = ""
                End If
                If Not datareader("DatabaseName").Equals(DBNull.Value) Then
                    TB_Database.Text = datareader("DatabaseName")
                Else
                    TB_Database.Text = ""
                End If
                If Not datareader("DatabaseUserName").Equals(DBNull.Value) Then
                    TB_DBUser.Text = datareader("DatabaseUserName")
                Else
                    TB_DBUser.Text = ""
                End If
                If Not datareader("DatabasePassword").Equals(DBNull.Value) Then
                    TB_DBPass.Text = SecureEnc.DecryptString(datareader("DatabasePassword"))
                Else
                    TB_DBPass.Text = ""
                End If
                If Not datareader("IsGroupActive").Equals(DBNull.Value) Then
                    Chk_Active.Checked = datareader("IsGroupActive")
                Else
                    Chk_Active.Checked = False
                End If
                If Not datareader("DateCreated").Equals(DBNull.Value) Then
                    Lbl_DateCreated.Text = datareader("DateCreated")
                Else
                    Lbl_DateCreated.Text = ""
                End If
                If Not datareader("DateModified").Equals(DBNull.Value) Then
                    Lbl_lastModified.Text = datareader("DateModified")
                Else
                    Lbl_lastModified.Text = ""
                End If
                If Not datareader("GroupGUID").Equals(DBNull.Value) Then
                    LBL_GUID.Text = datareader("GroupGUID")
                Else
                    LBL_GUID.Text = ""
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Function LoadStore(StoreID As Integer)
        Dim SecureEnc As New PasswordSecurity
        Try
            Dim sSQL As String

            sSQL = "SELECT Top 1 * FROM GroupStores WHERE StoreID = " & StoreID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            lbl_StoreID.Text = StoreID

            While datareader.Read
                If Not datareader("StoreName").Equals(DBNull.Value) Then
                    TB_StoreName.Text = datareader("StoreName")
                Else
                    TB_StoreName.Text = ""
                End If
                If Not datareader("ShortName").Equals(DBNull.Value) Then
                    TB_ShortName.Text = datareader("ShortName")
                Else
                    TB_ShortName.Text = ""
                End If
                If Not datareader("LinkCo_ID").Equals(DBNull.Value) Then
                    TB_LinkedCOID.Text = datareader("LinkCo_ID")
                Else
                    TB_LinkedCOID.Text = ""
                End If
                If Not datareader("LinkPOS_ID").Equals(DBNull.Value) Then
                    TB_POSID.Text = datareader("LinkPOS_ID")
                Else
                    TB_POSID.Text = ""
                End If

            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Function LoadUser(UserID As Integer)
        Dim SecureEnc As New PasswordSecurity
        Try
            Dim sSQL As String
            sSQL = "SELECT Top 1 * FROM Tenants WHERE TenantID = " & UserID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            LBL_U_ID.Text = UserID

            While datareader.Read
                If Not datareader("FirstName").Equals(DBNull.Value) Then
                    TB_U_FirstName.Text = datareader("FirstName")
                Else
                    TB_U_FirstName.Text = ""
                End If
                If Not datareader("Surname").Equals(DBNull.Value) Then
                    TB_U_Surname.Text = datareader("Surname")
                Else
                    TB_U_Surname.Text = ""
                End If
                If Not datareader("TenantName").Equals(DBNull.Value) Then
                    TB_U_TenantName.Text = datareader("TenantName")
                Else
                    TB_U_TenantName.Text = ""
                End If
                If Not datareader("Email").Equals(DBNull.Value) Then
                    TB_U_eMailAddress.Text = datareader("Email")
                Else
                    TB_U_eMailAddress.Text = ""
                End If
                If Not datareader("IsActive").Equals(DBNull.Value) Then
                    Ckb_U_active.Checked = datareader("IsActive")
                Else
                    Ckb_U_active.Checked = False
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Function LoadUserRoules(GroupID As Integer)
        Dim SecureEnc As New PasswordSecurity
        Dim ischecked As Boolean
        Try
            Dim sSQL As String = "SELECT Distinct Roles_Header.RoleID,Roles_Header.RoleName,Tenants.TenantID,GroupID from Roles_Header left join Roles_ToTenant on Roles_Header.RoleID = Roles_ToTenant.RoleID " & _
"left Join Tenants on Tenants.TenantID = Roles_ToTenant.TenantID Where Roles_Header.GroupID =" & GroupID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            ltv_Roles.Items.Clear()
            While datareader.Read
                If ltv_Roles.Items.ToString.Contains(datareader("RoleName")) = False Then
                    Dim lvi As New ListViewItem("t")
                    lvi.Tag = datareader("RoleID")
                    lvi.Text = datareader("RoleName")
                    lvi.Name = datareader("RoleName")

                    If Not IsDBNull(datareader("TenantID")) Then
                        If datareader("TenantID") = LBL_U_ID.Text And datareader("GroupID") = Lbl_U_SelectedGroupID.Text.Remove(0, 1) Then
                            ischecked = True
                        Else
                            ischecked = False
                        End If
                    Else
                        ischecked = False
                    End If
                    lvi.Checked = ischecked
                    ltv_Roles.Items.Add(lvi)
                End If
            End While
            connection.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Private Sub Btn_ActiveateNew_Click(sender As Object, e As EventArgs) Handles Btn_ActiveateNew.Click
        TB_Database.Text = ""
        TB_DBPass.Text = ""
        TB_DBUser.Text = ""
        TB_GroupName.Text = ""
        TB_LegalName.Text = ""
        TB_ServerAddress.Text = ""
        Chk_Active.Checked = True
        Lbl_DateCreated.Text = ""
        LBL_GroupID.Text = ""
        LBL_GUID.Text = ""
        Btn_ActiveateNew.Visible = False
        Btn_NewGrp.Visible = True
        Btn_Update.Visible = False
    End Sub
    Private Sub CB_U_RoleSelection_SelectedIndexChanged(sender As Object, e As EventArgs)
        ' If MsgBox("are you sure you want to change the users role?", MsgBoxStyle.YesNo, "User Role Change") = MsgBoxResult.Yes Then
        ' Call MapUser2Role(LBL_U_ID.Text, CB_U_RoleSelection.SelectedItem.Value)
        ' End If
    End Sub
    Function LoadUserMapping()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select * From Tenants Where isActive = 'True'")

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "UserMapping")
            connection.Close()
            DGV_UserMapping.DataSource = ds
            DGV_UserMapping.DataMember = "UserMapping"
            DGV_UserMapping.Columns("Password").Visible = False
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Function
    Private Sub DGV_UserMapping_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_UserMapping.CellContentClick
        Try
            Dim colName As String = DGV_UserMapping.Columns(e.ColumnIndex).Name
            If colName = "Map" Then
                If IsDBNull(TreeView1.SelectedNode.Tag) Then Exit Sub
                If IsNothing(TreeView1.SelectedNode.Tag) Then Exit Sub
                If TreeView1.SelectedNode.Tag.ToString.Substring(0, 1) <> "S" Then
                    MsgBox("A store must be selected in the tree view on the left", MsgBoxStyle.Critical, "User Error")
                    Exit Sub
                End If
                Dim TenantID As Long = DGV_UserMapping.Rows(e.RowIndex).Cells("TenantID").Value
                Dim StoreID As Long = TreeView1.SelectedNode.Tag.ToString.Remove(0, 1)
                If IsUserinStore(TenantID, StoreID) Then
                    MsgBox("User already allocated to store!, Process Aborting")
                    Exit Sub
                Else
                    If MapUser2Group(TenantID, StoreID) = True Then
                        BuildTree()
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox("Selection not allowed")
        End Try
    End Sub
    Function ListForms()

        On Error Resume Next
        Dim DS As New DataTable
        DS.Columns.Add("DisplayName", GetType(String))
        DS.Columns.Add("Value", GetType(String))
        CB_Forms.Items.Clear()
        Dim DisplayName As String
        Dim ValMem As String
        Dim Forms As New List(Of Form)()
        For Each t As Type In Me.GetType().Assembly.GetTypes()
            If t.BaseType.Name = "Form" Then
                Dim frmt As New Form
                frmt = CType(Activator.CreateInstance(t), Form)
                If Not IsNothing(frmt.Tag) Then
                    If frmt.Tag.ToString.Substring(0, 2) = "S_" Then
                        DisplayName = frmt.Tag.ToString.Remove(0, 2)
                        DisplayName = DisplayName.Replace("&", " ")
                        ValMem = frmt.Name
                        DS.Rows.Add(DisplayName, ValMem)
                    End If
                End If
                frmt.Dispose()
            End If
        Next
        DS = DS.Select("", "DisplayName").CopyToDataTable
        CB_Forms.DataSource = DS
        CB_Forms.DisplayMember = DS.Columns("DisplayName").ToString
        CB_Forms.ValueMember = DS.Columns("Value").ToString

    End Function
    Function ListControls(FrmName As String)
        Dim FullTypeName As String = "MB." & FrmName
        Dim FormInstanceType As Type = Type.GetType(FullTypeName, True, True)
        Dim objForm As Form = CType(Activator.CreateInstance(FormInstanceType), Form)
        Dim aControl As Control
        Dim bControl As Control
        Dim cControl As Control
        Dim dControl As Control
        Dim eControl As Control

        ListBox1.Items.Clear()

        For Each aControl In objForm.Controls
            'Set the a's
            If Not IsNothing(aControl.Tag) Then
                If Len(aControl.Tag.ToString) > 2 Then
                    If aControl.Tag.ToString.Substring(0, 2) = "S_" Then
                        ListBox1.Items.Add(aControl.Tag.ToString.Remove(0, 2))
                    End If
                End If
            End If
            For Each bControl In aControl.Controls
                If Not IsNothing(bControl.Tag) Then
                    If Len(bControl.Tag.ToString) > 2 Then
                        If bControl.Tag.ToString.Substring(0, 2) = "S_" Then
                            ListBox1.Items.Add(bControl.Tag.ToString.Remove(0, 2))
                        End If
                    End If
                End If
                For Each cControl In bControl.Controls
                    If Not IsNothing(cControl.Tag) Then
                        If Len(cControl.Tag.ToString) > 2 Then
                            If cControl.Tag.ToString.Substring(0, 2) = "S_" Then
                                ListBox1.Items.Add(cControl.Tag.ToString.Remove(0, 2))
                            End If
                        End If
                    End If
                    For Each dControl In cControl.Controls
                        If Not IsNothing(dControl.Tag) Then
                            If Len(dControl.Tag.ToString) > 2 Then
                                If dControl.Tag.ToString.Substring(0, 2) = "S_" Then
                                    ListBox1.Items.Add(dControl.Tag.ToString.Remove(0, 2))
                                End If
                            End If
                        End If
                        For Each eControl In dControl.Controls
                            If Not IsNothing(eControl.Tag) Then
                                If Len(eControl.Tag.ToString) > 2 Then
                                    If eControl.Tag.ToString.Substring(0, 2) = "S_" Then
                                        ListBox1.Items.Add(eControl.Tag.ToString.Remove(0, 2))
                                    End If
                                End If
                            End If
                        Next eControl
                    Next dControl
                Next cControl
            Next bControl
        Next aControl
    End Function
    Private Sub CB_Forms_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Forms.SelectedIndexChanged
        If CB_Forms.SelectedValue.ToString = "System.Data.DataRowView" Then Exit Sub
        Call ListControls(CB_Forms.SelectedValue.ToString)
    End Sub
    Private Sub Btn_AddRole_Click(sender As Object, e As EventArgs) Handles Btn_AddRole.Click
        Dim frm_Addrole As New Frm_AddRole
        frm_Addrole.Lbl_GrgID.Text = LBL_GroupID.Text
        frm_Addrole.ShowDialog()
        BuildRollTree()
    End Sub
    Function BuildRollTree(Optional FindNode As String = "")
        On Error Resume Next
        Dim ChildNodeTag As String
        Dim SearchNodeTag As String
        Dim dt As New DataTable
        TV_Role.Nodes.Clear()
        'Fill Roles
        dt = RolesFillDT(lbl_R_GrpID.Text)
        For Each dr As DataRow In dt.Rows
            AddRoleNode("Roles", "Roles", "R_" & dr("RoleID").ToString, dr("RoleName").ToString & " (Role)", "R_" & dr("RoleID").ToString, 0)
        Next
        'Fill Forms
        dt = RoleFormsFillDT(lbl_R_GrpID.Text)
        For Each dr As DataRow In dt.Rows
            ChildNodeTag = "F_" & dr("FormName").ToString & "R_" & dr("RoleID").ToString
            AddRoleNode("R_" & dr("RoleID").ToString, dr("RoleName").ToString & " (Role)", ChildNodeTag, dr("FormName").ToString & " (Screen)", ChildNodeTag, 1)
        Next
        'Fill Tenants
        dt = RoleControlsFillDT(lbl_R_GrpID.Text)
        For Each dr As DataRow In dt.Rows
            ChildNodeTag = "C_" & dr("ControlID").ToString
            SearchNodeTag = "F_" & dr("FormName").ToString & "R_" & dr("RoleID").ToString
            AddRoleNode(SearchNodeTag, dr("FormName").ToString & " (Screen)", ChildNodeTag, dr("ControlTag").ToString & " (Action)", ChildNodeTag, 2)
        Next
        TV_Role.ExpandAll()
        If FindNode <> "" Then
            TV_Role.Nodes.Find(FindNode, True)
        End If
    End Function
    Function RolesFillDT(GroupID As String) As DataTable
        Dim strSql As String = "select distinct RoleID,RoleName " & _
        "FROM Roles_Header Where GroupID = " & GroupID
        Dim dtb As New DataTable
        Using cnn As New SqlConnection(MagicBox_CommonDB)
            cnn.Open()
            Using dad As New SqlDataAdapter(strSql, cnn)
                dad.Fill(dtb)
                RolesFillDT = dtb
            End Using
            cnn.Close()
        End Using
    End Function
    Function RoleControlsFillDT(GroupID As String) As DataTable
        Dim strSql As String = "select distinct Roles_Header.RoleID,RoleName,ControlID,ControlTag,FormName " & _
        "FROM Roles_Header left Join [Roles_ControlToRole] on [Roles_ControlToRole].RoleID = Roles_Header.RoleID Where GroupID = " & GroupID
        Dim dtb As New DataTable
        Using cnn As New SqlConnection(MagicBox_CommonDB)
            cnn.Open()
            Using dad As New SqlDataAdapter(strSql, cnn)
                dad.Fill(dtb)
                RoleControlsFillDT = dtb
            End Using
            cnn.Close()
        End Using
    End Function
    Function RoleFormsFillDT(GroupID As String) As DataTable
        Dim strSql As String = "select distinct Roles_Header.RoleID,RoleName,FormName " & _
        "FROM Roles_Header left Join [Roles_ControlToRole] on [Roles_ControlToRole].RoleID = Roles_Header.RoleID Where GroupID = " & GroupID
        Dim dtb As New DataTable
        Using cnn As New SqlConnection(MagicBox_CommonDB)
            cnn.Open()
            Using dad As New SqlDataAdapter(strSql, cnn)
                dad.Fill(dtb)
                RoleFormsFillDT = dtb
            End Using
            cnn.Close()
        End Using
    End Function
    Private Sub btn_RoleMap_Click(sender As Object, e As EventArgs) Handles btn_RoleMap.Click
        Dim SelectedNodeKey As String

        If IsNothing(ListBox1.SelectedItem) Then
            MsgBox("There is no selected form control", MsgBoxStyle.Critical, "User Error")
            Exit Sub
        End If
        If ListBox1.SelectedItem.ToString = "" Then
            MsgBox("There is no selected form control", MsgBoxStyle.Critical, "User Error")
            Exit Sub
        End If
        If IsNothing(TV_Role.SelectedNode) Then
            MsgBox("There is no role selected", MsgBoxStyle.Critical, "User Error")
            Exit Sub
        End If
        If IsNothing(TV_Role.SelectedNode.Tag) Then
            MsgBox("There is no role selected", MsgBoxStyle.Critical, "User Error")
            Exit Sub
        Else
            SelectedNodeKey = TV_Role.SelectedNode.Tag.ToString
        End If
        If TV_Role.SelectedNode.Tag.ToString.Substring(0, 2) <> "R_" Then
            MsgBox("There is no role selected", MsgBoxStyle.Critical, "User Error")
            Exit Sub
        End If
        MapRoleGontrol(TV_Role.SelectedNode.Tag.ToString.Remove(0, 2), CB_Forms.Text, ListBox1.SelectedItem.ToString)
        BuildRollTree()
        Application.DoEvents()
        TV_Role.Nodes.Find(SelectedNodeKey, True)
    End Sub
    Private Sub Btn_Update_Click(sender As Object, e As EventArgs) Handles Btn_Update.Click
        If UpdateGroup(LBL_GroupID.Text, TB_GroupName.Text, TB_LegalName.Text, TB_ServerAddress.Text, TB_Database.Text, TB_DBUser.Text, TB_DBPass.Text, Chk_Active.Checked) = True Then
            MsgBox("Updated")
            BuildTree()
        End If
    End Sub
    Private Sub Btn_AddNew_Click(sender As Object, e As EventArgs) Handles Btn_AddNew.Click
        Dim frm_new As New Frm_AddStore
        frm_new.Show()
        BuildTree()
    End Sub
    Private Sub Btn_NewGrp_Click(sender As Object, e As EventArgs) Handles Btn_NewGrp.Click
        If AddGroup(TB_GroupName.Text, TB_LegalName.Text, TB_ServerAddress.Text, TB_Database.Text, TB_DBUser.Text, TB_DBPass.Text, True) = True Then
            MsgBox("Added")
        End If
        BuildTree()
        Btn_NewGrp.Visible = False
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If UpdateStore(lbl_StoreID.Text, TB_StoreName.Text, TB_ShortName.Text, TB_LinkedCOID.Text, TB_POSID.Text) = True Then
            MsgBox("Store Linked", MsgBoxStyle.Information, "Completed")
            BuildTree()
        End If
    End Sub
    Private Sub btn_UpdateRolesAssignments_Click(sender As Object, e As EventArgs) Handles btn_UpdateRolesAssignments.Click
        Call MapUser2RoleClear(LBL_U_ID.Text, Lbl_U_SelectedGroupID.Text.Remove(0, 1))
        For Each item As ListViewItem In ltv_Roles.Items
            If item.Checked = True Then
                Call MapUser2Role(LBL_U_ID.Text, item.Tag.ToString)
            End If
        Next
    End Sub
    Function LoadPayPypes(GroupID As Integer)
        Dim SecureEnc As New PasswordSecurity
        Dim ischecked As Boolean
        Try
            Dim sSQL As String = "SELECT * from Roles_Header left join Roles_ToTenant on Roles_Header.RoleID = Roles_ToTenant.RoleID " & _
"left Join Tenants on Tenants.TenantID = Roles_ToTenant.TenantID Where Roles_Header.GroupID =" & GroupID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            ltv_Roles.Items.Clear()
            While datareader.Read
                Dim lvi As New ListViewItem("t")
                lvi.Tag = datareader("RoleID")
                lvi.Text = datareader("RoleName")

                If Not IsDBNull(datareader("TenantID")) Then
                    If datareader("TenantID") = LBL_U_ID.Text And datareader("GroupID") = Lbl_U_SelectedGroupID.Text.Remove(0, 1) Then
                        ischecked = True
                    Else
                        ischecked = False
                    End If
                Else
                    ischecked = False
                End If
                lvi.Checked = ischecked
                ltv_Roles.Items.Add(lvi)
            End While
            connection.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Private Sub Btn_AddUser_Click(sender As Object, e As EventArgs) Handles Btn_AddUser.Click
        Dim Frm_NewUser As New Frm_AddTenant
        Frm_NewUser.ShowDialog()
        TenantFillDT()
    End Sub

    Private Sub btn_UpdateGroup_Click(sender As Object, e As EventArgs) Handles btn_UpdateGroup.Click
        For Each item As ListViewItem In ltv_Roles.Items
            If item.Selected = True Then
                Dim frm_AddRole As New Frm_AddRole
                frm_AddRole.lbl_RoleID.Text = item.Tag.ToString
                frm_AddRole.ShowDialog()
            End If
        Next
    End Sub

    Private Sub TS_RemoveUser_Click(sender As Object, e As EventArgs) Handles TS_RemoveUser.Click
        Dim UserID As Long

        Dim lngUserID As String = TreeView1.SelectedNode.Tag
        Dim lngStoreID As String = TreeView1.SelectedNode.Parent.Tag
        If lngUserID.Substring(0, 1) = "T" Then
            If MsgBox("Are you sure you want to remove the user from this Company?", vbYesNo, "Confirmation") = vbYes Then
                RemoveTenantFromOrg(lngUserID.Remove(0, 1), lngStoreID.Remove(0, 1))
                BuildTree()
            End If
        End If

    End Sub

    Private Sub TreeView1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TreeView1.MouseDown
        Try
            Dim NodeClicked As TreeNode
            ' Get the node clicked on
            NodeClicked = Me.TreeView1.GetNodeAt(e.X, e.Y)

            If IsNothing(NodeClicked.Tag) Then Exit Sub
            Dim TheTag As String = NodeClicked.Tag
            If TheTag.Substring(0, 1) = "T" Then
                If e.Button = Windows.Forms.MouseButtons.Right Then
                    Dim point As New Drawing.Point(e.X, e.Y)
                    Me.TreeView1.SelectedNode = NodeClicked
                    TB_ContextMenu.Show(TreeView1, point)
                End If
                BuildRollTree()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Btn_RemoveUser_Click(sender As Object, e As EventArgs) Handles Btn_RemoveUser.Click
        Dim TenantID As Long = DGV_UserMapping.Rows(DGV_UserMapping.CurrentRow.Index).Cells("TenantID").Value
        If MsgBox("Are you sure you want to delete user " & TenantID, MsgBoxStyle.YesNo, "Delete User?") = MsgBoxResult.Yes Then
            RemoveTenantFull(TenantID)
            TenantFillDT()
            DGV_UserMapping.Refresh()
        End If
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub
    Private Sub RemoveRoleTaskToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveRoleTaskToolStripMenuItem.Click
        If IsNothing(TV_Role.SelectedNode) Then Exit Sub
        If TV_Role.SelectedNode.Tag.ToString.Substring(0, 2) <> "C_" Then
            MsgBox("YOu may only remove role tasks, please select a tash and then retry")
            Exit Sub
        End If

        If MsgBox("Area you sure you want to remove role task " & TV_Role.SelectedNode.Text.ToString, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            DeleteRoleTask(TV_Role.SelectedNode.Tag.ToString.Remove(0, 2))
            BuildRollTree()
        End If
    End Sub

    Function DeleteRoleTask(ControlID As Long)
        Try
            Dim strSQL As String = ""
            strSQL = "Delete From [Roles_ControlToRole] Where ControlID = " & ControlID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            MsgBox("Role task has been removed!")
            connection.Close()
        Catch ex As Exception
            MsgBox("There was an error removing role: " & ex.Message)
        End Try
    End Function

    Private Sub TV_Role_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TV_Role.AfterSelect
        Try
            LstB_Exclusions.Items.Clear()
            If TV_Role.SelectedNode.Tag.ToString.Substring(0, 2) = "R_" Then
                Dim Exclusions As String()
                Exclusions = GetPaytypeExclusionsByID(TV_Role.SelectedNode.Tag.ToString.Remove(0, 2))
                For Each i In Exclusions
                    LstB_Exclusions.Items.Add(i.ToString)
                Next
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error getting exclusions: " & ex.Message)
        End Try
    End Sub
    Private Sub Btn_Click(sender As Object, e As EventArgs) Handles Btn.Click
        If IsNothing(TV_Role.SelectedNode) Then
            MsgBox("Please Ensure you select a role to the left")
            Exit Sub
        End If
        If TV_Role.SelectedNode.Tag.ToString.Substring(0, 2) <> "R_" Then
            MsgBox("Please ensure there is a role name highlighted on the left")
            Exit Sub
        End If

        Dim RoleID As Long
        Dim ExclusionsList As String = ""
        RoleID = TV_Role.SelectedNode.Tag.ToString.Remove(0, 2)
        For Each i As String In LstB_Exclusions.Items
            ExclusionsList = ExclusionsList & i.ToString & "|"
        Next
        ExclusionsList = ExclusionsList.TrimEnd(CChar("|"))
        UpdatePaytypeExclusions(RoleID, ExclusionsList)

    End Sub

    Private Sub Btn_ExclAdd_Click(sender As Object, e As EventArgs) Handles Btn_ExclAdd.Click
        If TB_AddExclusion.Text = "" Then Exit Sub
        LstB_Exclusions.Items.Add(TB_AddExclusion.Text)
    End Sub

    Private Sub Btn_ExclRemove_Click(sender As Object, e As EventArgs) Handles Btn_ExclRemove.Click
        If IsNothing(LstB_Exclusions.SelectedItem) Then Exit Sub
        LstB_Exclusions.Items.Remove(LstB_Exclusions.SelectedItem)
    End Sub

    Private Sub btn_RefreshAuto_Click(sender As Object, e As EventArgs) Handles btn_RefreshAuto.Click
        Dim sSQL As String

        sSQL = "SELECT Top 1 * FROM AutoLoader WHERE StoreID = " & lbl_StoreID.Text

        Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

        Dim cmd As New SqlCommand(sSQL, connection)
        cmd.CommandTimeout = 300
        connection.Open()
        Dim datareader As SqlDataReader = cmd.ExecuteReader

        While datareader.Read
            lbl_AutoLoaderID.Text = datareader.Item("ID")
            If datareader.Item("AutoGRN") = True Then
                CB_AutoGRN.Checked = True
            Else
                CB_AutoGRN.Checked = False
            End If
        End While
    End Sub
End Class

