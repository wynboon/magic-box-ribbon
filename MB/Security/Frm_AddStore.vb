﻿Imports System.Data.SqlClient

Public Class Frm_AddStore

    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        If AddGroupStore(TB_StoreName.Text, TB_ShortName.Text, TB_Co_ID.Text, CB_GroupID.SelectedItem.Value.ToString, TB_POSID.Text) = True Then
            MsgBox("Added")
        End If
    End Sub

    Private Sub Frm_AddStore_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadGroups()
    End Sub
    Function LoadGroups()
        Dim SecureEnc As New PasswordSecurity
        Try
            Dim sSQL As String = "SELECT Distinct *  from [Groups] Order By GroupName"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            CB_GroupID.Items.Clear()
            While datareader.Read
                CB_GroupID.Items.Add(New ValueDescriptionPair(datareader("GroupID"), datareader("GroupName")))
            End While
            connection.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
End Class