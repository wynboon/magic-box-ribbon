﻿Public Class Frm_AddTenant

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim TenantID As Long
        If TB_eMail.Text = "" Then
            MsgBox("A new user must have an email address!")
            Exit Sub
        End If
        If TB_TenantName.Text = "" Then
            MsgBox("A new user must have a user name!")
            Exit Sub
        End If
        If T_FirstName.Text = "" Then
            MsgBox("A new user must have a first name!")
            Exit Sub
        End If
        TenantID = AddTenant(TB_TenantName.Text, T_FirstName.Text, TB_Surname.Text, TB_eMail.Text, Globals.Ribbons.Ribbon1.CurrentGroupID)
        If TenantID <> 0 Then
            NewPassword(TenantID, TB_eMail.Text)
        End If
        Me.Close()
    End Sub
End Class