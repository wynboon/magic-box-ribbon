﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_AddTenant
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TB_TenantName = New System.Windows.Forms.TextBox()
        Me.T_FirstName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_Surname = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_eMail = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Usename"
        '
        'TB_TenantName
        '
        Me.TB_TenantName.Location = New System.Drawing.Point(115, 30)
        Me.TB_TenantName.Name = "TB_TenantName"
        Me.TB_TenantName.Size = New System.Drawing.Size(219, 22)
        Me.TB_TenantName.TabIndex = 1
        '
        'T_FirstName
        '
        Me.T_FirstName.Location = New System.Drawing.Point(115, 58)
        Me.T_FirstName.Name = "T_FirstName"
        Me.T_FirstName.Size = New System.Drawing.Size(219, 22)
        Me.T_FirstName.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(30, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "First Name"
        '
        'TB_Surname
        '
        Me.TB_Surname.Location = New System.Drawing.Point(115, 86)
        Me.TB_Surname.Name = "TB_Surname"
        Me.TB_Surname.Size = New System.Drawing.Size(219, 22)
        Me.TB_Surname.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(30, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Surename"
        '
        'TB_eMail
        '
        Me.TB_eMail.Location = New System.Drawing.Point(115, 114)
        Me.TB_eMail.Name = "TB_eMail"
        Me.TB_eMail.Size = New System.Drawing.Size(219, 22)
        Me.TB_eMail.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(30, 119)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "eMail"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(33, 150)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(301, 23)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Frm_AddTenant
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(426, 198)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TB_eMail)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TB_Surname)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.T_FirstName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TB_TenantName)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Frm_AddTenant"
        Me.Text = "Add New Tenant"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TB_TenantName As System.Windows.Forms.TextBox
    Friend WithEvents T_FirstName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB_Surname As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_eMail As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
