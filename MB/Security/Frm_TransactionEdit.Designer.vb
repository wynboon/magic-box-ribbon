﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_TransactionEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DGV_Tx = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BatchIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CaptureDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriodDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinYearDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReferenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAccDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxAmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PostedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccountingDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VoidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DRCRDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContraAccountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InfoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TxBinding = New System.Windows.Forms.BindingSource(Me.components)
        Me.Btn_Load = New System.Windows.Forms.Button()
        Me.TB_TxID = New System.Windows.Forms.TextBox()
        Me.Save = New System.Windows.Forms.Button()
        Me.TB_Total = New System.Windows.Forms.TextBox()
        Me.Btn_NonBal = New System.Windows.Forms.Button()
        CType(Me.DGV_Tx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxBinding, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Tx
        '
        Me.DGV_Tx.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_Tx.AutoGenerateColumns = False
        Me.DGV_Tx.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Tx.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.BatchIDDataGridViewTextBoxColumn, Me.TransactionIDDataGridViewTextBoxColumn, Me.LinkIDDataGridViewTextBoxColumn, Me.CaptureDateDataGridViewTextBoxColumn, Me.TransactionDateDataGridViewTextBoxColumn, Me.PPeriodDataGridViewTextBoxColumn, Me.FinYearDataGridViewTextBoxColumn, Me.GDCDataGridViewTextBoxColumn, Me.ReferenceDataGridViewTextBoxColumn, Me.DescriptionDataGridViewTextBoxColumn, Me.AccNumberDataGridViewTextBoxColumn, Me.LinkAccDataGridViewTextBoxColumn, Me.AmountDataGridViewTextBoxColumn, Me.TaxTypeDataGridViewTextBoxColumn, Me.TaxAmountDataGridViewTextBoxColumn, Me.UserIDDataGridViewTextBoxColumn, Me.SupplierIDDataGridViewTextBoxColumn, Me.EmployeeIDDataGridViewTextBoxColumn, Me.Description2DataGridViewTextBoxColumn, Me.Description3DataGridViewTextBoxColumn, Me.Description4DataGridViewTextBoxColumn, Me.Description5DataGridViewTextBoxColumn, Me.PostedDataGridViewTextBoxColumn, Me.AccountingDataGridViewTextBoxColumn, Me.VoidDataGridViewTextBoxColumn, Me.TransactionTypeDataGridViewTextBoxColumn, Me.DRCRDataGridViewTextBoxColumn, Me.ContraAccountDataGridViewTextBoxColumn, Me.DescriptionCodeDataGridViewTextBoxColumn, Me.DocTypeDataGridViewTextBoxColumn, Me.SupplierNameDataGridViewTextBoxColumn, Me.CoIDDataGridViewTextBoxColumn, Me.InfoDataGridViewTextBoxColumn, Me.Info2DataGridViewTextBoxColumn, Me.Info3DataGridViewTextBoxColumn})
        Me.DGV_Tx.DataSource = Me.TxBinding
        Me.DGV_Tx.Location = New System.Drawing.Point(13, 73)
        Me.DGV_Tx.Name = "DGV_Tx"
        Me.DGV_Tx.RowTemplate.Height = 24
        Me.DGV_Tx.Size = New System.Drawing.Size(1002, 272)
        Me.DGV_Tx.TabIndex = 0
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        '
        'BatchIDDataGridViewTextBoxColumn
        '
        Me.BatchIDDataGridViewTextBoxColumn.DataPropertyName = "BatchID"
        Me.BatchIDDataGridViewTextBoxColumn.HeaderText = "BatchID"
        Me.BatchIDDataGridViewTextBoxColumn.Name = "BatchIDDataGridViewTextBoxColumn"
        '
        'TransactionIDDataGridViewTextBoxColumn
        '
        Me.TransactionIDDataGridViewTextBoxColumn.DataPropertyName = "TransactionID"
        Me.TransactionIDDataGridViewTextBoxColumn.HeaderText = "TransactionID"
        Me.TransactionIDDataGridViewTextBoxColumn.Name = "TransactionIDDataGridViewTextBoxColumn"
        '
        'LinkIDDataGridViewTextBoxColumn
        '
        Me.LinkIDDataGridViewTextBoxColumn.DataPropertyName = "LinkID"
        Me.LinkIDDataGridViewTextBoxColumn.HeaderText = "LinkID"
        Me.LinkIDDataGridViewTextBoxColumn.Name = "LinkIDDataGridViewTextBoxColumn"
        '
        'CaptureDateDataGridViewTextBoxColumn
        '
        Me.CaptureDateDataGridViewTextBoxColumn.DataPropertyName = "Capture_Date"
        Me.CaptureDateDataGridViewTextBoxColumn.HeaderText = "Capture_Date"
        Me.CaptureDateDataGridViewTextBoxColumn.Name = "CaptureDateDataGridViewTextBoxColumn"
        '
        'TransactionDateDataGridViewTextBoxColumn
        '
        Me.TransactionDateDataGridViewTextBoxColumn.DataPropertyName = "Transaction_Date"
        Me.TransactionDateDataGridViewTextBoxColumn.HeaderText = "Transaction_Date"
        Me.TransactionDateDataGridViewTextBoxColumn.Name = "TransactionDateDataGridViewTextBoxColumn"
        '
        'PPeriodDataGridViewTextBoxColumn
        '
        Me.PPeriodDataGridViewTextBoxColumn.DataPropertyName = "PPeriod"
        Me.PPeriodDataGridViewTextBoxColumn.HeaderText = "PPeriod"
        Me.PPeriodDataGridViewTextBoxColumn.Name = "PPeriodDataGridViewTextBoxColumn"
        '
        'FinYearDataGridViewTextBoxColumn
        '
        Me.FinYearDataGridViewTextBoxColumn.DataPropertyName = "Fin_Year"
        Me.FinYearDataGridViewTextBoxColumn.HeaderText = "Fin_Year"
        Me.FinYearDataGridViewTextBoxColumn.Name = "FinYearDataGridViewTextBoxColumn"
        '
        'GDCDataGridViewTextBoxColumn
        '
        Me.GDCDataGridViewTextBoxColumn.DataPropertyName = "GDC"
        Me.GDCDataGridViewTextBoxColumn.HeaderText = "GDC"
        Me.GDCDataGridViewTextBoxColumn.Name = "GDCDataGridViewTextBoxColumn"
        '
        'ReferenceDataGridViewTextBoxColumn
        '
        Me.ReferenceDataGridViewTextBoxColumn.DataPropertyName = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.HeaderText = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.Name = "ReferenceDataGridViewTextBoxColumn"
        '
        'DescriptionDataGridViewTextBoxColumn
        '
        Me.DescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.HeaderText = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.Name = "DescriptionDataGridViewTextBoxColumn"
        '
        'AccNumberDataGridViewTextBoxColumn
        '
        Me.AccNumberDataGridViewTextBoxColumn.DataPropertyName = "AccNumber"
        Me.AccNumberDataGridViewTextBoxColumn.HeaderText = "AccNumber"
        Me.AccNumberDataGridViewTextBoxColumn.Name = "AccNumberDataGridViewTextBoxColumn"
        '
        'LinkAccDataGridViewTextBoxColumn
        '
        Me.LinkAccDataGridViewTextBoxColumn.DataPropertyName = "LinkAcc"
        Me.LinkAccDataGridViewTextBoxColumn.HeaderText = "LinkAcc"
        Me.LinkAccDataGridViewTextBoxColumn.Name = "LinkAccDataGridViewTextBoxColumn"
        '
        'AmountDataGridViewTextBoxColumn
        '
        Me.AmountDataGridViewTextBoxColumn.DataPropertyName = "Amount"
        Me.AmountDataGridViewTextBoxColumn.HeaderText = "Amount"
        Me.AmountDataGridViewTextBoxColumn.Name = "AmountDataGridViewTextBoxColumn"
        '
        'TaxTypeDataGridViewTextBoxColumn
        '
        Me.TaxTypeDataGridViewTextBoxColumn.DataPropertyName = "TaxType"
        Me.TaxTypeDataGridViewTextBoxColumn.HeaderText = "TaxType"
        Me.TaxTypeDataGridViewTextBoxColumn.Name = "TaxTypeDataGridViewTextBoxColumn"
        '
        'TaxAmountDataGridViewTextBoxColumn
        '
        Me.TaxAmountDataGridViewTextBoxColumn.DataPropertyName = "Tax_Amount"
        Me.TaxAmountDataGridViewTextBoxColumn.HeaderText = "Tax_Amount"
        Me.TaxAmountDataGridViewTextBoxColumn.Name = "TaxAmountDataGridViewTextBoxColumn"
        '
        'UserIDDataGridViewTextBoxColumn
        '
        Me.UserIDDataGridViewTextBoxColumn.DataPropertyName = "UserID"
        Me.UserIDDataGridViewTextBoxColumn.HeaderText = "UserID"
        Me.UserIDDataGridViewTextBoxColumn.Name = "UserIDDataGridViewTextBoxColumn"
        '
        'SupplierIDDataGridViewTextBoxColumn
        '
        Me.SupplierIDDataGridViewTextBoxColumn.DataPropertyName = "SupplierID"
        Me.SupplierIDDataGridViewTextBoxColumn.HeaderText = "SupplierID"
        Me.SupplierIDDataGridViewTextBoxColumn.Name = "SupplierIDDataGridViewTextBoxColumn"
        '
        'EmployeeIDDataGridViewTextBoxColumn
        '
        Me.EmployeeIDDataGridViewTextBoxColumn.DataPropertyName = "EmployeeID"
        Me.EmployeeIDDataGridViewTextBoxColumn.HeaderText = "EmployeeID"
        Me.EmployeeIDDataGridViewTextBoxColumn.Name = "EmployeeIDDataGridViewTextBoxColumn"
        '
        'Description2DataGridViewTextBoxColumn
        '
        Me.Description2DataGridViewTextBoxColumn.DataPropertyName = "Description_2"
        Me.Description2DataGridViewTextBoxColumn.HeaderText = "Description_2"
        Me.Description2DataGridViewTextBoxColumn.Name = "Description2DataGridViewTextBoxColumn"
        '
        'Description3DataGridViewTextBoxColumn
        '
        Me.Description3DataGridViewTextBoxColumn.DataPropertyName = "Description_3"
        Me.Description3DataGridViewTextBoxColumn.HeaderText = "Description_3"
        Me.Description3DataGridViewTextBoxColumn.Name = "Description3DataGridViewTextBoxColumn"
        '
        'Description4DataGridViewTextBoxColumn
        '
        Me.Description4DataGridViewTextBoxColumn.DataPropertyName = "Description_4"
        Me.Description4DataGridViewTextBoxColumn.HeaderText = "Description_4"
        Me.Description4DataGridViewTextBoxColumn.Name = "Description4DataGridViewTextBoxColumn"
        '
        'Description5DataGridViewTextBoxColumn
        '
        Me.Description5DataGridViewTextBoxColumn.DataPropertyName = "Description_5"
        Me.Description5DataGridViewTextBoxColumn.HeaderText = "Description_5"
        Me.Description5DataGridViewTextBoxColumn.Name = "Description5DataGridViewTextBoxColumn"
        '
        'PostedDataGridViewTextBoxColumn
        '
        Me.PostedDataGridViewTextBoxColumn.DataPropertyName = "Posted"
        Me.PostedDataGridViewTextBoxColumn.HeaderText = "Posted"
        Me.PostedDataGridViewTextBoxColumn.Name = "PostedDataGridViewTextBoxColumn"
        '
        'AccountingDataGridViewTextBoxColumn
        '
        Me.AccountingDataGridViewTextBoxColumn.DataPropertyName = "Accounting"
        Me.AccountingDataGridViewTextBoxColumn.HeaderText = "Accounting"
        Me.AccountingDataGridViewTextBoxColumn.Name = "AccountingDataGridViewTextBoxColumn"
        '
        'VoidDataGridViewTextBoxColumn
        '
        Me.VoidDataGridViewTextBoxColumn.DataPropertyName = "Void"
        Me.VoidDataGridViewTextBoxColumn.HeaderText = "Void"
        Me.VoidDataGridViewTextBoxColumn.Name = "VoidDataGridViewTextBoxColumn"
        '
        'TransactionTypeDataGridViewTextBoxColumn
        '
        Me.TransactionTypeDataGridViewTextBoxColumn.DataPropertyName = "Transaction_Type"
        Me.TransactionTypeDataGridViewTextBoxColumn.HeaderText = "Transaction_Type"
        Me.TransactionTypeDataGridViewTextBoxColumn.Name = "TransactionTypeDataGridViewTextBoxColumn"
        '
        'DRCRDataGridViewTextBoxColumn
        '
        Me.DRCRDataGridViewTextBoxColumn.DataPropertyName = "DR_CR"
        Me.DRCRDataGridViewTextBoxColumn.HeaderText = "DR_CR"
        Me.DRCRDataGridViewTextBoxColumn.Name = "DRCRDataGridViewTextBoxColumn"
        '
        'ContraAccountDataGridViewTextBoxColumn
        '
        Me.ContraAccountDataGridViewTextBoxColumn.DataPropertyName = "Contra_Account"
        Me.ContraAccountDataGridViewTextBoxColumn.HeaderText = "Contra_Account"
        Me.ContraAccountDataGridViewTextBoxColumn.Name = "ContraAccountDataGridViewTextBoxColumn"
        '
        'DescriptionCodeDataGridViewTextBoxColumn
        '
        Me.DescriptionCodeDataGridViewTextBoxColumn.DataPropertyName = "Description_Code"
        Me.DescriptionCodeDataGridViewTextBoxColumn.HeaderText = "Description_Code"
        Me.DescriptionCodeDataGridViewTextBoxColumn.Name = "DescriptionCodeDataGridViewTextBoxColumn"
        '
        'DocTypeDataGridViewTextBoxColumn
        '
        Me.DocTypeDataGridViewTextBoxColumn.DataPropertyName = "DocType"
        Me.DocTypeDataGridViewTextBoxColumn.HeaderText = "DocType"
        Me.DocTypeDataGridViewTextBoxColumn.Name = "DocTypeDataGridViewTextBoxColumn"
        '
        'SupplierNameDataGridViewTextBoxColumn
        '
        Me.SupplierNameDataGridViewTextBoxColumn.DataPropertyName = "SupplierName"
        Me.SupplierNameDataGridViewTextBoxColumn.HeaderText = "SupplierName"
        Me.SupplierNameDataGridViewTextBoxColumn.Name = "SupplierNameDataGridViewTextBoxColumn"
        '
        'CoIDDataGridViewTextBoxColumn
        '
        Me.CoIDDataGridViewTextBoxColumn.DataPropertyName = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.HeaderText = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.Name = "CoIDDataGridViewTextBoxColumn"
        '
        'InfoDataGridViewTextBoxColumn
        '
        Me.InfoDataGridViewTextBoxColumn.DataPropertyName = "Info"
        Me.InfoDataGridViewTextBoxColumn.HeaderText = "Info"
        Me.InfoDataGridViewTextBoxColumn.Name = "InfoDataGridViewTextBoxColumn"
        '
        'Info2DataGridViewTextBoxColumn
        '
        Me.Info2DataGridViewTextBoxColumn.DataPropertyName = "Info2"
        Me.Info2DataGridViewTextBoxColumn.HeaderText = "Info2"
        Me.Info2DataGridViewTextBoxColumn.Name = "Info2DataGridViewTextBoxColumn"
        '
        'Info3DataGridViewTextBoxColumn
        '
        Me.Info3DataGridViewTextBoxColumn.DataPropertyName = "Info3"
        Me.Info3DataGridViewTextBoxColumn.HeaderText = "Info3"
        Me.Info3DataGridViewTextBoxColumn.Name = "Info3DataGridViewTextBoxColumn"
        '
        'TxBinding
        '
        Me.TxBinding.DataSource = GetType(MB.Transaction)
        '
        'Btn_Load
        '
        Me.Btn_Load.Location = New System.Drawing.Point(13, 12)
        Me.Btn_Load.Name = "Btn_Load"
        Me.Btn_Load.Size = New System.Drawing.Size(113, 55)
        Me.Btn_Load.TabIndex = 1
        Me.Btn_Load.Text = "Load"
        Me.Btn_Load.UseVisualStyleBackColor = True
        '
        'TB_TxID
        '
        Me.TB_TxID.Location = New System.Drawing.Point(133, 44)
        Me.TB_TxID.Name = "TB_TxID"
        Me.TB_TxID.Size = New System.Drawing.Size(100, 22)
        Me.TB_TxID.TabIndex = 2
        '
        'Save
        '
        Me.Save.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Save.Location = New System.Drawing.Point(13, 351)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(101, 28)
        Me.Save.TabIndex = 3
        Me.Save.Text = "Save"
        Me.Save.UseVisualStyleBackColor = True
        '
        'TB_Total
        '
        Me.TB_Total.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TB_Total.Enabled = False
        Me.TB_Total.Location = New System.Drawing.Point(860, 45)
        Me.TB_Total.Name = "TB_Total"
        Me.TB_Total.Size = New System.Drawing.Size(155, 22)
        Me.TB_Total.TabIndex = 4
        '
        'Btn_NonBal
        '
        Me.Btn_NonBal.Location = New System.Drawing.Point(240, 44)
        Me.Btn_NonBal.Name = "Btn_NonBal"
        Me.Btn_NonBal.Size = New System.Drawing.Size(231, 24)
        Me.Btn_NonBal.TabIndex = 5
        Me.Btn_NonBal.Text = "Next non Zero Transaction"
        Me.Btn_NonBal.UseVisualStyleBackColor = True
        '
        'Frm_TransactionEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1027, 391)
        Me.Controls.Add(Me.Btn_NonBal)
        Me.Controls.Add(Me.TB_Total)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.TB_TxID)
        Me.Controls.Add(Me.Btn_Load)
        Me.Controls.Add(Me.DGV_Tx)
        Me.Name = "Frm_TransactionEdit"
        Me.Text = "Transaction Edit"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DGV_Tx,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TxBinding,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents DGV_Tx As System.Windows.Forms.DataGridView
    Friend WithEvents TxBinding As System.Windows.Forms.BindingSource
    Friend WithEvents Btn_Load As System.Windows.Forms.Button
    Friend WithEvents TB_TxID As System.Windows.Forms.TextBox
    Friend WithEvents Save As System.Windows.Forms.Button
    Friend WithEvents TB_Total As System.Windows.Forms.TextBox
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BatchIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CaptureDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriodDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FinYearDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReferenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAccDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxAmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PostedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccountingDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VoidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DRCRDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContraAccountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InfoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Btn_NonBal As System.Windows.Forms.Button
End Class
