﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class frmPeriods2

    Private Sub frmPeriods2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadTree()
    End Sub
    Function LoadTree()

        TreeView1.Nodes.Clear()

        Dim dt As New DataTable
        dt = FinYear()
        For Each dr As DataRow In dt.Rows
            AddNode(dr("FinYear").ToString, dr("FinYear").ToString, dr("FinYear").ToString, dr("FinYear").ToString, dr("FinYear").ToString)
        Next
        ' dt = Periods()
        ' For Each dr As DataRow In dt.Rows
        ' AddNode(dr("FinYear").ToString, dr("Display").ToString, dr("Tag").ToString, dr("FinYear").ToString, "0")
        ' Next

        TreeView1.CollapseAll()
    End Function
    Function FinYear() As DataTable
        Dim strSql As String = "select Distinct [FinYear] from Periods Where Co_ID = " & My.Settings.Setting_CompanyID & " order by FinYear"
        Dim dtb As New DataTable
        Using cnn As New SqlConnection(My.Settings.CS_Setting)
            cnn.Open()
            Using dad As New SqlDataAdapter(strSql, cnn)
                dad.Fill(dtb)
                FinYear = dtb
            End Using
            cnn.Close()
        End Using
    End Function
    Function Periods() As DataTable
        Dim strSql As String = "select top 1 [FinYear], Cast(Period as VARCHAR) + '(' + CONVERT(VARCHAR(11),[Start Date],106) + ' ~ ' + CONVERT(VARCHAR(11),[End Date],106) + ')' as Display, ID as Tag from Periods Where Co_ID = " & My.Settings.Setting_CompanyID & " order by FinYear"
        Dim dtb As New DataTable
        Using cnn As New SqlConnection(My.Settings.CS_Setting)
            cnn.Open()
            Using dad As New SqlDataAdapter(strSql, cnn)
                dad.Fill(dtb)
                Periods = dtb
            End Using
            cnn.Close()
        End Using
    End Function
    Private Sub AddNode(parentNode As String, nodeText As String, ChildKeyKey As String, ParentKey As String, NodeTage As String)
        Dim node As New List(Of TreeNode)
        Dim InsertNode As New TreeNode
        node.AddRange(TreeView1.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            node.Add(TreeView1.Nodes.Add(ParentKey, parentNode))
        End If
        InsertNode.Name = ChildKeyKey
        InsertNode.Text = nodeText
        InsertNode.Tag = NodeTage
        node(0).Nodes.Add(InsertNode)

    End Sub
End Class