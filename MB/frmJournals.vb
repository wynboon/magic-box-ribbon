﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmJournals

#Region "Variables"
    Dim IsVat As Boolean = Nothing
    Dim Inclusive As String = Nothing
    Dim MBData As New DataExtractsClass
#End Region

    Dim xAccessAdapter_Accounts As OleDbDataAdapter
    Dim xAccessTable_Accounts As New DataTable
    Dim xSQLAdapter_Accounts As SqlDataAdapter
    Dim xSQLTable_Accounts As New DataTable

    Dim xAccessAdapter_Suppliers As OleDbDataAdapter
    Dim xAccessTable_Suppliers As New DataTable
    Dim xSQLAdapter_Suppliers As SqlDataAdapter
    Dim xSQLTable_Suppliers As New DataTable

    Dim xAccessAdapter_Periods As OleDbDataAdapter
    Dim xAccessTable_Periods As New DataTable
    Dim xSQLAdapter_Periods As SqlDataAdapter
    Dim xSQLTable_Periods As New DataTable


    Dim oInvoiceAmount As Decimal
    Dim oProportion As Decimal

    Dim oBatchID As Integer
    Dim oTransactionID As Long
    Dim oLinkID As Integer
    Dim sTransactionDate As Date
    Dim sCaptureDate As Date
    Dim sPPeriod As String
    Dim sFinYear As String
    Dim sGDC As String
    Dim sInvoiceNumber As String
    Dim sDescription As String
    Dim sAccountNumber As String
    Dim sContraAccount As String
    Dim sLinkAcc As String
    Dim oAmount As Decimal
    Dim oTaxType As Integer
    Dim oTaxAmount As Decimal
    Dim sUserID As String
    Dim oSupplierID As Integer
    Dim oEmployeeID As Integer
    Dim sDescription2 As String
    Dim sDescription3 As String
    Dim sDescription4 As String
    Dim sDescription5 As String
    Dim blnPosted As Boolean
    Dim blnAccounting As Boolean
    Dim blnVoid As Boolean
    Dim TransactionType As Integer
    Dim sDRCR As String
    Dim sDescriptionCode As String
    Dim oDocType As String
    Dim oSupplierName As String

    Dim oJournalAmount As String

    Dim TaxPerc As Double
    Dim TaxCalc As Double

    Public Sub Add_Transaction_DGV(ByVal oWhich_DGV As String, ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                     ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                     ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                     ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                     ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                     ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                     ByVal oDocType As String, ByVal oSupplierName As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)
            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName


            If oWhich_DGV = "DGV_Transactions" Then
                Me.DGV_Transactions.Rows.Add(newRow)
            Else
                MsgBox("ERROR ON DGV CLEANUP")
            End If

        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox(ex.Message & " 048")
        End Try


    End Sub

    Sub AppendTransactions(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####



        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value

                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value

                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName]"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr29"  '##### NEW #####
                End If

                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 283")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####


        Dim i As Integer
        Dim cmd As SqlCommand

        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value
                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value
                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName]"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr29"  '##### NEW #####
                End If

                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 284")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub
    Function Check_Integrity(ByVal oDataGridView As DataGridView) As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To oDataGridView.RowCount - 2 'extra row
                If oDataGridView.Rows(i).Cells(26).Value = "Dr" Then
                    oDebits = oDebits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                ElseIf oDataGridView.Rows(i).Cells(26).Value = "Cr" Then
                    oCredits = oCredits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                End If '
                Dim V = CDec(oDataGridView.Rows(i).Cells(14).Value)
            Next




            If oDebits = oCredits Then
                Check_Integrity = True
            Else
                Check_Integrity = False
            End If


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox("There was an error checking integrity " & ex.Message & " 285")
        End Try
    End Function
    Private Sub Journals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Journals -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            Me.Label_Version.Text = "Version " & My.Settings.Setting_Version
            Call LoadTax()
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting


            Dim oResponse As MsgBoxResult
            If Fill_DGV_Accounts() = False Then
                oResponse = MsgBox("Problem connecting to database, Would you like to continue?", MsgBoxStyle.YesNo)
                If oResponse = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If

            Call Fill_DGV_Suppliers()
            Call Fill_DGV_Periods()

            Call Fill_Category1_Combobox()

            Me.cmbJournalType.Text = "General Journal"

            Me.Period.Text = GetPeriod(CStr(Me.Date_DateTimePicker.Value.Date.ToString("dd MMMM yyyy")))

            Me.ListView1.Columns.Add("Period", 50, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Date", 105, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Reference", 130, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Description", 100, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Tax Type", 100, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Inclusive", 45, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Exclusive", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Dr/Cr", 80, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Account Category 1", 120, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Account Category 2", 120, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Account Category 3", 120, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Contra Category 1", 120, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Contra Category 2", 120, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Contra Category 3", 120, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Account", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Contra Account", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Supplier ID", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Supplier Name", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("Employee ID", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("TransType", 90, HorizontalAlignment.Left)
            Me.ListView1.Columns.Add("IsStat", 5, HorizontalAlignment.Left)

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            rdbInclusive.Checked = True
        Catch ex As Exception
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            MessageBox.Show("An error occured while loading the journals screen : " & _
                            ex.Message, "Journals Screen", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Function Fill_DGV_Accounts() As Boolean
        Try
            Dim sSQL As String

            sSQL = "Select * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Accounts = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Accounts.Fill(xAccessTable_Accounts)
                Me.dgvAccounts.DataSource = xAccessTable_Accounts
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Accounts.Fill(xSQLTable_Accounts)
                Me.dgvAccounts.DataSource = xSQLTable_Accounts
            End If
            Fill_DGV_Accounts = True
        Catch ex As Exception
            MsgBox(ex.Message & " 287")
            Fill_DGV_Accounts = False
        End Try
    End Function

    Sub Fill_DGV_Suppliers()
        Try
            Dim sSQL As String

            sSQL = "Select * From Suppliers"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Suppliers = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Suppliers.Fill(xAccessTable_Suppliers)
                Me.dgvSuppliers.DataSource = xAccessTable_Suppliers
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Suppliers = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Suppliers.Fill(xSQLTable_Suppliers)
                Me.dgvSuppliers.DataSource = xSQLTable_Suppliers
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 288")
        End Try
    End Sub
    Sub Fill_DGV_Periods()
        Try
            Dim sSQL As String

            sSQL = "Select * From Periods"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Periods = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Periods.Fill(xAccessTable_Periods)
                Me.dgvPeriods.DataSource = xAccessTable_Periods
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Periods = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Periods.Fill(xSQLTable_Periods)
                Me.dgvPeriods.DataSource = xSQLTable_Periods
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 289")
        End Try
    End Sub
    Private Sub Add_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Add_Button.Click

        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            If Me.DC.Text = "" Then
                MessageBox.Show("Please select the transaction type ('Dr' or 'Cr')", "Transaction type", MessageBoxButtons.OK, MessageBoxIcon.Information)
                DC.Focus()
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.cmbJournalType.Text = "" Then
                MessageBox.Show("Please select a journal type", "Journal Type", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbJournalType.Focus()
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.lblSegment4Account.Text = "" Then
                MessageBox.Show("Please select the corresponding account", "Account", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Enabled = True
                Cursor = Cursors.Arrow
                cmbAccCat1.Focus()
                Exit Sub
            End If

            If txtInclusive.Text = "" And txtExclusive.Text = "" Then
                MessageBox.Show("Please enter the amount.", "Amount", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            Dim Period As String = Me.Period.Text
            Dim TransactionDate As String = Me.Date_DateTimePicker.Value.Date.ToString("dd MMMM yyyy")
            Dim Reference As String = Me.Reference.Text
            Dim Description As String = Me.Description.Text
            Dim Tax As String = Me.cmbTax.SelectedValue.ToString
            Dim Inclusive As String = Me.txtInclusive.Text
            Dim Exclusive As String = Me.txtExclusive.Text
            Dim DefaultCat As String = Me.DC.Text
            Dim Cat1 As String = Me.cmbAccCat1.Text
            Dim Cat2 As String = Me.cmbAccCat2.Text
            Dim Cat3 As String = Me.cmbAccCat3.Text
            Dim ContraCat1 As String = Me.cmbContraCat1.Text
            Dim ContraCat2 As String = Me.cmbContraCat2.Text
            Dim ContraCat3 As String = Me.cmbContraCat3.Text
            Dim Segment4Account As String = Me.lblSegment4Account.Text
            Dim Segment4Contra As String = Me.lblSegment4Contra.Text

            Dim SupplierID As Integer = Nothing
            Dim SupplierName As String = ""
            Dim EmployeeID As String = Nothing

            If lblSelectedOption.Text = "Supplier Journal" Then
                SupplierID = CInt(cmbJournalTypeValues.SelectedValue)
                SupplierName = CStr(cmbJournalTypeValues.Text)
            End If

            If lblSelectedOption.Text = "Employee Journal" Then
                EmployeeID = CInt(cmbJournalTypeValues.SelectedValue)
            End If

            Dim str(20) As String
            Dim itm As ListViewItem
            str(0) = Period
            str(1) = TransactionDate
            str(2) = Reference
            str(3) = Description
            str(4) = Tax
            str(5) = Inclusive
            str(6) = Exclusive
            str(7) = DefaultCat
            str(8) = Cat1
            str(9) = Cat2
            str(10) = Cat3
            str(11) = ContraCat1
            str(12) = ContraCat2
            str(13) = ContraCat3
            str(14) = Segment4Account
            str(15) = Segment4Contra

            If lblSelectedOption.Text = "Supplier Journal" Then
                str(16) = SupplierID
                str(17) = SupplierName
            Else
                str(16) = 0
                str(17) = ""
            End If

            If lblSelectedOption.Text = "Employee Journal" Then
                str(18) = EmployeeID
            Else
                str(18) = 0
            End If

            str(19) = cmbJournalType.Text
            If Ckb_StatTran.Checked Then
                str(20) = False
            Else
                str(20) = True
            End If

            itm = New ListViewItem(str)
            Me.ListView1.Items.Add(itm)
            oClear()

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub DeleteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteButton.Click
        Try

            If Me.ListView1.SelectedItems.Count > 1 Then
                MsgBox("Please only delete one line at a time!")
                Exit Sub
            End If

            Me.Refresh()

            Dim items(Me.ListView1.SelectedItems.Count - 1) As Object 'this code needed as multiselect is true
            Me.ListView1.SelectedItems.CopyTo(items, 0)
            For Each item As ListViewItem In items
                Me.ListView1.Items.Remove(item)
            Next

        Catch ex As Exception
            MsgBox("There was an error deleting rows " & ex.Message & " 292")
        End Try
    End Sub

    Private Sub EditButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditButton.Click
        Try

            If Me.ListView1.SelectedIndices.Count = 0 Then
                MsgBox("Please select a line in the list box. This will be edited with the values in the entry boxes.")
            Else


                Dim Period As String = Me.Period.Text
                Dim TransactionDate As String = Me.Date_DateTimePicker.Value.Date.ToString("dd MMMM yyyy")
                Dim Reference As String = Me.Reference.Text
                Dim Description As String = Me.Description.Text
                Dim Tax As String = Me.cmbTax.Text
                Dim Inclusive As String = Me.txtInclusive.Text
                Dim Exclusive As String = Me.txtExclusive.Text
                Dim DefaultCat As String = Me.DC.Text
                Dim Cat1 As String = Me.cmbAccCat1.Text
                Dim Cat2 As String = Me.cmbAccCat2.Text
                Dim Cat3 As String = Me.cmbAccCat3.Text
                Dim ContraCat1 As String = Me.cmbContraCat1.Text
                Dim ContraCat2 As String = Me.cmbContraCat2.Text
                Dim ContraCat3 As String = Me.cmbContraCat3.Text
                Dim Segment4Account As String = Me.lblSegment4Account.Text
                Dim Segment4Contra As String = Me.lblSegment4Contra.Text

                Dim SupplierID As Integer = Nothing
                Dim SupplierName As String = ""
                Dim EmployeeID As Integer = Nothing

                If lblSelectedOption.Text = "Supplier Journal" Then
                    SupplierID = CInt(cmbJournalTypeValues.SelectedValue)
                    SupplierName = CStr(cmbJournalTypeValues.Text)
                End If

                If lblSelectedOption.Text = "Employee Journal" Then
                    EmployeeID = CInt(cmbJournalTypeValues.SelectedValue)
                End If

                With Me.ListView1.SelectedItems(0)
                    .SubItems(0).Text = Period
                    .SubItems(1).Text = TransactionDate
                    .SubItems(2).Text = Reference
                    .SubItems(3).Text = Description
                    .SubItems(4).Text = Tax
                    .SubItems(5).Text = Inclusive
                    .SubItems(6).Text = Exclusive
                    .SubItems(7).Text = DefaultCat
                    .SubItems(8).Text = Cat1
                    .SubItems(9).Text = Cat2
                    .SubItems(10).Text = Cat3
                    .SubItems(11).Text = ContraCat1
                    .SubItems(12).Text = ContraCat2
                    .SubItems(13).Text = ContraCat3
                    .SubItems(14).Text = Segment4Account
                    .SubItems(15).Text = Segment4Contra

                    If lblSelectedOption.Text = "Supplier Journal" Then
                        .SubItems(16).Text = SupplierID
                        .SubItems(17).Text = SupplierName
                    Else
                        .SubItems(16).Text = 0
                        .SubItems(17).Text = ""
                    End If

                    If lblSelectedOption.Text = "Employee Journal" Then
                        .SubItems(18).Text = EmployeeID
                    Else
                        .SubItems(18).Text = 0
                    End If

                End With
            End If

        Catch ex As Exception
            MsgBox(Err.Description & " 293")
        End Try
    End Sub
    Private Sub Close_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Close_Button.Click
        Me.Close()
    End Sub
    Private Sub JournalType_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbJournalType.SelectedIndexChanged

        Dim CompanyID As Integer = Nothing
        If My.Settings.Setting_CompanyID <> Nothing Then
            CompanyID = CInt(My.Settings.Setting_CompanyID)
        Else
            MessageBox.Show("Please set a default company in order to continue.", "Default Company", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        With cmbJournalType

            If .Text = "General Journal" Then

                lblSelectedOption.Text = "General Journal"
                cmbJournalTypeValues.Enabled = False
                cmbJournalTypeValues.SelectedIndex = -1
                lblSegment4Contra.Text = ""
                cmbContraCat1.SelectedIndex = -1
                cmbContraCat2.SelectedIndex = -1
                cmbContraCat3.SelectedIndex = -1
                GroupBox3.Enabled = True
            ElseIf .Text = "Cash-Up Journal" Then
                lblSelectedOption.Text = "Cash-Up Journal"
                cmbJournalTypeValues.Enabled = False
                cmbJournalTypeValues.SelectedIndex = -1
                lblSegment4Contra.Text = ""
                cmbContraCat1.SelectedIndex = -1
                cmbContraCat2.SelectedIndex = -1
                cmbContraCat3.SelectedIndex = -1
                GroupBox3.Enabled = True

            ElseIf .Text = "Supplier Journal" Then

                lblSegment4Contra.Text = ""
                cmbContraCat1.SelectedIndex = -1
                cmbContraCat2.SelectedIndex = -1
                cmbContraCat3.SelectedIndex = -1

                Enabled = False
                Cursor = Cursors.AppStarting
                lblSelectedOption.Text = "Supplier Journal"
                cmbJournalTypeValues.Enabled = True
                cmbJournalTypeValues.DataSource = MBData.SupplierList(CompanyID)
                cmbJournalTypeValues.DisplayMember = "SupplierName"
                cmbJournalTypeValues.ValueMember = "SupplierID"

                If cmbContraCat1.Items.Contains("CONTROL1") Then
                    cmbContraCat1.SelectedItem = "CONTROL1"
                Else
                    Enabled = True
                    Cursor = Cursors.Arrow
                    cmbContraCat1.SelectedIndex = -1
                    cmbJournalTypeValues.SelectedIndex = -1
                    MessageBox.Show("There is no Supplier 'CREDITORS CONTROL' account for the current company therefore you cannot continue with adding a journal.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                If cmbContraCat2.Items.Contains("POS CREDITORS CONTROL") Then
                    cmbContraCat2.SelectedItem = "POS CREDITORS CONTROL"
                Else
                    Enabled = True
                    Cursor = Cursors.Arrow
                    cmbContraCat1.SelectedIndex = -1
                    cmbContraCat2.SelectedIndex = -1
                    cmbJournalTypeValues.SelectedIndex = -1
                    MessageBox.Show("There is no Supplier 'CREDITORS CONTROL' category 2, please rectify before you continue.", "Missing Category", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                If cmbContraCat3.Items.Contains("POS CREDITORS CONTROL") Then
                    cmbContraCat3.SelectedItem = "POS CREDITORS CONTROL"
                Else
                    Enabled = True
                    Cursor = Cursors.Arrow
                    cmbContraCat1.SelectedIndex = -1
                    cmbContraCat2.SelectedIndex = -1
                    cmbContraCat3.SelectedIndex = -1
                    cmbJournalTypeValues.SelectedIndex = -1
                    MessageBox.Show("There is no Supplier 'CREDITORS CONTROL' for category 3, please rectify before you continue.", "Missing Category", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                Enabled = True
                Cursor = Cursors.Arrow
                GroupBox3.Enabled = False

            ElseIf .Text = "Employee Journal" Then

                lblSegment4Contra.Text = ""
                cmbContraCat1.SelectedIndex = -1
                cmbContraCat2.SelectedIndex = -1
                cmbContraCat3.SelectedIndex = -1

                Enabled = False
                Cursor = Cursors.AppStarting
                lblSelectedOption.Text = "Employee Journal"
                cmbJournalTypeValues.Enabled = True
                cmbJournalTypeValues.DataSource = MBData.EmployeeList(CompanyID)
                cmbJournalTypeValues.DisplayMember = "EmployeeNames"
                cmbJournalTypeValues.ValueMember = "ID"

                If cmbContraCat1.Items.Contains("CONTROL1") Then
                    cmbContraCat1.SelectedItem = "CONTROL1"
                Else
                    Enabled = True
                    Cursor = Cursors.Arrow
                    cmbContraCat1.SelectedIndex = -1
                    cmbJournalTypeValues.SelectedIndex = -1
                    MessageBox.Show("There is no 'EMPLOYEE CONTROL' account for the current company therefore you cannot continue with adding a journal.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                If cmbContraCat2.Items.Contains("EMPLOYEE CONTROL") Then
                    cmbContraCat2.SelectedItem = "EMPLOYEE CONTROL"
                Else
                    Enabled = True
                    Cursor = Cursors.Arrow
                    cmbContraCat1.SelectedIndex = -1
                    cmbContraCat2.SelectedIndex = -1
                    cmbJournalTypeValues.SelectedIndex = -1
                    MessageBox.Show("There is no 'EMPLOYEE CONTROL' category 2, please rectify before you continue.", "Missing Category", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                If cmbContraCat3.Items.Contains("EMPLOYEE CONTROL") Then
                    cmbContraCat3.SelectedItem = "EMPLOYEE CONTROL"
                Else
                    Enabled = True
                    Cursor = Cursors.Arrow
                    cmbContraCat1.SelectedIndex = -1
                    cmbContraCat2.SelectedIndex = -1
                    cmbContraCat3.SelectedIndex = -1
                    cmbJournalTypeValues.SelectedIndex = -1
                    MessageBox.Show("There is no 'EMPLOYEE CONTROL' for category 3, please rectify before you continue.", "Missing Category", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                Enabled = True
                Cursor = Cursors.Arrow
                GroupBox3.Enabled = False

            End If
        End With

    End Sub
    Private Sub Process_Batch_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Process_Batch_Button.Click
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim oInclusive, oExclusive As String
            Dim oInclusive_or_Exclusive As String
            Dim Original_Amount As Decimal

            Dim oBatchID As Integer
            Dim oTransactionID As Integer

            Dim oLinkID As Integer = 0
            Dim sTransactionDate As String = Me.Date_DateTimePicker.Value.Date.ToString("dd MMMM yyyy") 'in loop below
            Dim sCaptureDate As String = Now.Date.ToString("dd MMMM yyyy")

            Dim sPPeriod As String 'in loop below
            'Dim sFinYear As String = My.Settings.Setting_Year 'Set this once
            sFinYear = GetFinYear(sTransactionDate)
            If IsNumeric(sFinYear) = False Or CInt(sFinYear) < 1 Then
                MsgBox("There was an error retrieving the financial year. Process aborted!")
                Exit Sub
            End If

            Dim sGDC As String 'in loop below
            Dim sReference As String 'in loop below
            Dim sDescription As String 'in loop below
            Dim sAccountNumber As String 'in loop below
            Dim sLinkAcc = CStr(oBatchID)
            Dim oAmount As Decimal 'in loop below
            Dim oTaxType As String 'in loop below
            Dim oTaxAmount As Decimal 'calculated in loop
            Dim sUserID As String = ""
            Dim sSupplierName As String
            Dim oSupplierID As Integer
            Dim oAccount As String 'This holds different things (Acc Num for General, Supplier Name for Debtors etc
            Dim oEmployeeID As String = "0"
            Dim sDescription2 As String = ""
            Dim sDescription3 As String = ""
            Dim sDescription4 As String = ""
            Dim sDescription5 As String = ""
            Dim blnPosted As Boolean = False 'becomes true when posted to Pastel
            Dim blnAccounting As Boolean = True
            Dim TransactionType As Integer
            Dim blnVoid As Boolean = False
            Dim sDRCR As String

            Dim oContraAccount As String
            Dim sContraAccount As String
            Dim sDescriptionCode As String
            Dim sDocType As String

            'now loop thru ListBox
            For i As Integer = 0 To Me.ListView1.Items.Count - 1

                '=======++++++++++++ First Generate TransactionID and BatchID =========================== 
                oBatch_or_Transaction_generation_Failed = False
                oBatchID = CLng(oCreateBatchID())
                oTransactionID = CLng(oCreateTransactionID()) 'adds a new record to the TransactionID table thus creating a new ID number that is used as the Transaction ID
                '=========================================================================================
                'First check if it is a Supplier Journal Credit - if so then must be done against an existing invoice
                Dim oSupplier_Invoice_Reference As String
                Dim oTransaction_relating_to_Reference As Integer

                sPPeriod = CDec(Me.ListView1.Items(i).SubItems(0).Text)
                sTransactionDate = Me.ListView1.Items(i).SubItems(1).Text
                oSupplierID = Nothing

                If CInt(ListView1.Items(i).SubItems(16).Text) <> 0 Then
                    oSupplierID = CInt(ListView1.Items(i).SubItems(16).Text)
                    sSupplierName = CStr(ListView1.Items(i).SubItems(17).Text)
                Else
                    oSupplierID = 0
                    sSupplierName = ""
                End If

                If CInt(ListView1.Items(i).SubItems(18).Text) <> 0 Then
                    oEmployeeID = CInt(ListView1.Items(i).SubItems(18).Text)
                Else
                    oEmployeeID = 0
                End If

                sDocType = ListView1.Items(i).SubItems(19).Text

                sGDC = "G"
                If sDocType = "Cash-Up Journal" Then
                    TransactionType = 146
                Else
                    TransactionType = 20

                End If

                sReference = Me.ListView1.Items(i).SubItems(2).Text
                oTaxType = Me.ListView1.Items(i).SubItems(4).Text
                blnAccounting = Me.ListView1.Items(i).SubItems(20).Text

                If oTaxType <> 0 Then
                    IsVat = True
                End If

                If IsVat Then
                    oTaxAmount = CDec(Me.ListView1.Items(i).SubItems(5).Text) - CDec(Me.ListView1.Items(i).SubItems(6).Text)
                    oAmount = CDec(Me.ListView1.Items(i).SubItems(5).Text - oTaxAmount)
                    oTaxType = oTaxType

                    sDRCR = (Me.ListView1.Items(i).SubItems(7).Text)
                    'Load Categories 1, 2, 3 into following three descriptions as with Invoice Capture
                    sDescription = Me.ListView1.Items(i).SubItems(8).Text
                    sDescription2 = Me.ListView1.Items(i).SubItems(9).Text
                    sDescription3 = Me.ListView1.Items(i).SubItems(10).Text
                    '*** NB Load the Journal Description into Description 4
                    sDescription4 = Me.ListView1.Items(i).SubItems(3).Text '???
                    sAccountNumber = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(14).Text)
                    sContraAccount = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(15).Text)
                    sDescriptionCode = Me.ListView1.Items(i).SubItems(14).Text 'NEW [Segment 4] code in Account Column

                    'Add the main line
                    Call Add_Transaction_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sReference, _
                        sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, _
                        sDescription5, blnPosted, blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

                    oAmount = Me.ListView1.Items(i).SubItems(5).Text
                    oTaxAmount = 0
                    oTaxType = "0"

                    'Now do the reverse Dr or Cr by switching the Contra account and Account and Dr/Cr
                    If sDRCR = "Dr" Then
                        sDRCR = "Cr"
                    ElseIf sDRCR = "Cr" Then
                        sDRCR = "Dr"
                    End If

                    sDescription = Me.ListView1.Items(i).SubItems(11).Text
                    sDescription2 = Me.ListView1.Items(i).SubItems(12).Text
                    sDescription3 = Me.ListView1.Items(i).SubItems(13).Text

                    'see also Contra and Acc swapped in call below

                    sDescriptionCode = Me.ListView1.Items(i).SubItems(15).Text 'NEW [Segment 4] code in Contra
                    sAccountNumber = Get_GLACCOUNT_in_Accounting_From_Segment4(sDescriptionCode)
                    sContraAccount = Get_GLACCOUNT_in_Accounting_From_Segment4(sDescriptionCode)
                    'Add Contra
                    Call Add_Transaction_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sReference, _
                    sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, _
                    sDescription5, blnPosted, blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)


                    'Add Tax Line
                        oAmount = CDec(Me.ListView1.Items(i).SubItems(5).Text) - CDec(Me.ListView1.Items(i).SubItems(6).Text)
                        oTaxAmount = 0
                        oTaxType = "0"

                        'Now do the reverse Dr or Cr by switching the Contra account and Account and Dr/Cr
                        If sDRCR = "Dr" Then
                            sDRCR = "Cr"
                        ElseIf sDRCR = "Cr" Then
                            sDRCR = "Dr"
                        End If

                        sDescription = "VAT CONTROL"
                        sDescription2 = "VAT CONTROL"
                        sDescription3 = "VAT CONTROL"

                        'see also Contra and Acc swapped in call below

                        sDescriptionCode = Get_Segment4_Using_Type_in_Accounting("VAT CONTROL")
                        sAccountNumber = Get_GLACCOUNT_in_Accounting_From_Segment4(sDescriptionCode)
                        sContraAccount = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(15).Text)
                    'Add Tax Line
                        Call Add_Transaction_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sReference, _
                        sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, _
                        sDescription5, blnPosted, blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

                    Else

                        oAmount = Me.ListView1.Items(i).SubItems(6).Text
                        sDRCR = (Me.ListView1.Items(i).SubItems(7).Text)
                        'Load Categories 1, 2, 3 into following three descriptions as with Invoice Capture
                        sDescription = Me.ListView1.Items(i).SubItems(8).Text
                        sDescription2 = Me.ListView1.Items(i).SubItems(9).Text
                        sDescription3 = Me.ListView1.Items(i).SubItems(10).Text
                        '*** NB Load the Journal Description into Description 4
                        sDescription4 = Me.ListView1.Items(i).SubItems(3).Text '???
                        sAccountNumber = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(14).Text)
                        sContraAccount = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(15).Text)
                        sDescriptionCode = Me.ListView1.Items(i).SubItems(14).Text 'NEW [Segment 4] code in Account Column

                    'Add the main line
                        Call Add_Transaction_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sReference, _
                            sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, _
                            sDescription5, blnPosted, blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)


                    'Add Contra
                    If blnAccounting = True Then
                        'Now do the reverse Dr or Cr by switching the Contra account and Account and Dr/Cr
                        If sDRCR = "Dr" Then
                            sDRCR = "Cr"
                        ElseIf sDRCR = "Cr" Then
                            sDRCR = "Dr"
                        End If

                        sDescription = Me.ListView1.Items(i).SubItems(11).Text
                        sDescription2 = Me.ListView1.Items(i).SubItems(12).Text
                        sDescription3 = Me.ListView1.Items(i).SubItems(13).Text

                        'see also Contra and Acc swapped in call below

                        sDescriptionCode = Me.ListView1.Items(i).SubItems(15).Text 'NEW [Segment 4] code in Contra
                        sAccountNumber = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(15).Text)
                        sContraAccount = Get_GLACCOUNT_in_Accounting_From_Segment4(Me.ListView1.Items(i).SubItems(14).Text)


                        Call Add_Transaction_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sReference, _
                        sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, _
                        sDescription5, blnPosted, blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)
                    End If
                    End If
Jump:
            Next

            If Me.DGV_Transactions.RowCount > 1 Then 'always more than 1 debit/credit
                'If Check_Integrity(Me.DGV_Transactions) = False Then
                '    MsgBox("The numbers are out of balance and cannot be saved!")
                'Else
                If My.Settings.DBType = "Access" Then
                    Call AppendTransactions(Me.DGV_Transactions, "Transactions")
                ElseIf My.Settings.DBType = "SQL" Then
                    Call AppendTransactions_SQL(Me.DGV_Transactions, "Transactions")
                End If

                ' End If
            End If

            Me.DGV_Transactions.Rows.Clear()
            ListView1.Items.Clear()
            oClear()
            cmbJournalType.SelectedIndex = -1
            cmbJournalTypeValues.Enabled = False
            cmbJournalTypeValues.Text = ""

            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("Journal batch succesfully processed", "Journal Batch", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show("An error occured with detais : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub Inclusive_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInclusive.TextChanged
        Try
            TaxPerc = cmbTax.SelectedValue.ToString
            If rdbInclusive.Checked = True Then
                If TaxPerc = 0 Then
                    txtExclusive.Text = txtInclusive.Text
                Else
                    If txtInclusive.Text.Trim.Length <> 0 Then
                        TaxCalc = 1 + (TaxPerc / 100)
                        txtExclusive.Text = Math.Round((txtInclusive.Text / TaxCalc), 2)
                    Else
                        txtExclusive.Text = ""
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Exclusive_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExclusive.TextChanged
        Try
            TaxPerc = cmbTax.SelectedValue.ToString

            If rdbExclusive.Checked = True Then
                If TaxPerc = 0 Then
                    txtInclusive.Text = txtExclusive.Text
                Else
                    TaxCalc = 1 + (TaxPerc / 100)
                    If txtExclusive.Text.Trim.Length <> 0 Then
                        txtInclusive.Text = Math.Round((txtExclusive.Text * TaxCalc))
                    Else
                        txtInclusive.Text = ""
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Date_DateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_DateTimePicker.ValueChanged
        Try
            Me.Period.Text = GetPeriod(CStr(Me.Date_DateTimePicker.Value.Date.ToString("dd MMMM yyyy")))
        Catch ex As Exception
            MsgBox("There was an error automatically finding the period! " & ex.Message)
        End Try

    End Sub

    Function Get_TransactionID_from_Reference(ByVal oReference As String, ByVal oSupplierName As String) As Integer
        Try

            Dim sSQL As String
            sSQL = "Select Distinct TransactionID From Transactions Where Reference = '" & oReference & "' And SupplierName = '" & oSupplierName & "' And DocType = 'Invoice'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Get_TransactionID_from_Reference = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Get_TransactionID_from_Reference = cmd.ExecuteScalar().ToString
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox("There was an error retrieving the BatchID for an invoice " & ex.Message & "298")
        End Try
    End Function

    Function Get_Amount_from_Reference(ByVal oReference As String, ByVal oSupplierName As String) As Decimal
        Try

            Dim sSQL As String


            sSQL = "Select Distinct Amount From Transactions Where Reference = '" & oReference & "' And SupplierName = '" & oSupplierName & "' And DocType = 'Invoice'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Get_Amount_from_Reference = CDec(cmd.ExecuteScalar().ToString)
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
                connection.Open()
                Get_Amount_from_Reference = CDec(cmd.ExecuteScalar().ToString)
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox("There was an error retrieving the BatchID for an invoice " & ex.Message & " 274")
        End Try
    End Function
    Sub Loop_Through_Transactions_With_Same_TransactionID(ByVal oTransactionID As Integer)
        Try

            Dim oID As Integer
            Dim sSQL As String

            Dim blnFirstRow As Boolean = True 'set to false after first read

            sSQL = "Select * From Transactions Where TransactionID = " & oTransactionID
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            sSQL = sSQL & " Order By ID"
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not IsDBNull(datareader("TransactionID")) Then
                        oID = datareader("TransactionID")
                        Load_Invoice_Data_From_ID_to_Variables(oID)
                        AddMirror()
                        blnFirstRow = False
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "Access" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not IsDBNull(datareader("TransactionID")) Then
                        oID = datareader("TransactionID")
                        Load_Invoice_Data_From_ID_to_Variables(oID)
                        AddMirror()
                        blnFirstRow = False
                    End If
                End While
                connection.Close()

            End If



        Catch ex As Exception
            MsgBox(ex.Message & " 299")
        Finally

        End Try

    End Sub
    Sub Load_Invoice_Data_From_ID_to_Variables(ByVal oID As Integer)

        Try

            'Dim ConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"
            Dim sSQL As String

            sSQL = "SELECT * FROM Transactions WHERE ID = " & oID
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If


            If My.Settings.DBType = "Access" Then

                Dim xConnection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, xConnection)
                xConnection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                While datareader.Read

                    oBatchID = datareader("BatchID")
                    oTransactionID = datareader("TransactionID")
                    oLinkID = datareader("LinkID")
                    sTransactionDate = datareader("Transaction Date")
                    sCaptureDate = datareader("Capture Date")
                    sPPeriod = datareader("PPeriod")
                    sFinYear = datareader("Fin Year")
                    sGDC = datareader("GDC")
                    sInvoiceNumber = datareader("Reference")
                    sDescription = datareader("Description")
                    sAccountNumber = datareader("AccNumber")
                    sContraAccount = datareader("Contra Account")
                    sLinkAcc = datareader("LinkAcc")
                    oAmount = datareader("Amount")
                    oTaxType = datareader("TaxType")
                    oTaxAmount = datareader("Tax Amount")
                    sUserID = datareader("UserID")
                    oSupplierID = datareader("SupplierID")
                    oEmployeeID = datareader("EmployeeID")
                    sDescription2 = datareader("Description 2")
                    sDescription3 = datareader("Description 3")
                    sDescription4 = datareader("Description 4")
                    sDescription5 = datareader("Description 5")
                    blnPosted = datareader("Posted")
                    blnAccounting = datareader("Accounting")
                    blnVoid = datareader("Void")
                    TransactionType = datareader("Transaction Type")
                    sDRCR = datareader("DR_CR")
                    sDescriptionCode = datareader("Description Code")
                    oDocType = datareader("DocType")
                    oSupplierName = datareader("SupplierName")
                End While
                xConnection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim xConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, xConnection)
                cmd.CommandTimeout = 300
                xConnection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    oBatchID = datareader("BatchID")
                    oTransactionID = datareader("TransactionID")
                    oLinkID = datareader("LinkID")
                    sTransactionDate = datareader("Transaction Date")
                    sCaptureDate = datareader("Capture Date")
                    sPPeriod = datareader("PPeriod")
                    sFinYear = datareader("Fin Year")
                    sGDC = datareader("GDC")
                    sInvoiceNumber = datareader("Reference")
                    sDescription = datareader("Description")
                    sAccountNumber = datareader("AccNumber")
                    sContraAccount = datareader("Contra Account")
                    sLinkAcc = datareader("LinkAcc")
                    oAmount = datareader("Amount")
                    oTaxType = datareader("TaxType")
                    oTaxAmount = datareader("Tax Amount")
                    sUserID = datareader("UserID")
                    oSupplierID = datareader("SupplierID")
                    oEmployeeID = datareader("EmployeeID")
                    sDescription2 = datareader("Description 2")
                    sDescription3 = datareader("Description 3")
                    sDescription4 = datareader("Description 4")
                    sDescription5 = datareader("Description 5")
                    blnPosted = datareader("Posted")
                    blnAccounting = datareader("Accounting")
                    blnVoid = datareader("Void")
                    TransactionType = datareader("Transaction Type")
                    sDRCR = datareader("DR_CR")
                    sDescriptionCode = datareader("Description Code")
                    oDocType = datareader("DocType")
                    oSupplierName = datareader("SupplierName")
                End While
                xConnection.Close()
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 300")
        End Try

    End Sub

    Sub AddMirror()
        Try


            Dim oProportionateAmount As Decimal
            oProportionateAmount = oAmount * oProportion

            If sDRCR = "Dr" Then
                sDRCR = "Cr"
            ElseIf sDRCR = "Cr" Then
                sDRCR = "Dr"
            End If

            sGDC = "D"

            Call Add_Transaction_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
        sDescription, sAccountNumber, sLinkAcc, oProportionateAmount, oTaxType, oTaxAmount, sUserID, _
        oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
        blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, oDocType, oSupplierName)
        Catch ex As Exception
            MsgBox(ex.Message & " 301")
        End Try
    End Sub

    Function Database_Lookup_String_return_Integer(ByVal oTable As String, ByVal oLookupField As String, ByVal WhereField As String, ByVal WhereValue As String, ByVal DataType As String) As Integer
        Try
            Dim oResult As Object
            Dim sSQL As String

            sSQL = "SELECT DISTINCT " & oLookupField & " FROM " & oTable & " WHERE " & WhereField & " = '" & SQLConvert(WhereValue) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Database_Lookup_String_return_Integer = CInt(cmd.ExecuteScalar().ToString)
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    Database_Lookup_String_return_Integer = 0
                Else
                    Database_Lookup_String_return_Integer = oResult
                End If
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up a database value " & oLookupField & " in table " & oTable & Err.Description & " 302")

        End Try
    End Function

    Function GetPeriod(ByVal sTransactionDate As String) As String
        Try

            Dim sSQL As String

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= Format('" & sTransactionDate & "',""Short Date"")"
                sSQL = sSQL & " AND [End Date] >= Format('" & sTransactionDate & "',""Short Date"")"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetPeriod = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= CAST('" & sTransactionDate & "' as date)"
                sSQL = sSQL & " AND [End Date] >= CAST('" & sTransactionDate & "' as date)"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetPeriod = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error getting the accounting period!" & ex.Message & " 303")
            GetPeriod = "NA"
        End Try
    End Function

    Function Get_GLACCOUNT_Using_Type_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            'Dim My.Settings.CS_Setting As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"


            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Type] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_GLACCOUNT_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_GLACCOUNT_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account based on Type in Accounting!" & " 304")
        End Try
    End Function

    Function Get_GLACCOUNT_in_Accounting_From_Segment4(ByVal sString As String) As String
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_GLACCOUNT_in_Accounting_From_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_GLACCOUNT_in_Accounting_From_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account in Accounting!" & " 307")
        End Try
    End Function

    Private Sub Cat1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAccCat1.SelectedIndexChanged
        If Me.cmbAccCat1.Text = "" Then Exit Sub
        'First clear other two boxes
        Me.cmbAccCat2.Items.Clear()
        Me.cmbAccCat2.Text = ""
        Me.cmbAccCat3.Items.Clear()
        Me.cmbAccCat3.Text = ""
        'Then fill next box
        Call Fill_Cat2(Me.cmbAccCat1.Text)
    End Sub

    Sub Fill_Category1_Combobox()
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            Me.cmbAccCat1.Items.Clear()
            Me.cmbContraCat1.Items.Clear()

            While datareader.Read
                If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                    Me.cmbAccCat1.Items.Add(datareader("Segment 1 Desc"))
                    Me.cmbContraCat1.Items.Add(datareader("Segment 1 Desc"))
                End If
            End While
            connection.Close()

        Catch ex As Exception
            MsgBox(ex.Message & " 308")
        End Try

    End Sub
    Sub Fill_Cat2(ByVal Cat1 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Cat1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            With Me.cmbAccCat2.Items
                .Clear()
                While datareader.Read
                    .Add(datareader("Segment 2 Desc"))
                End While
            End With
            connection.Close()

        Catch ex As Exception
            MsgBox(ex.Message & " 309")
        End Try

    End Sub

    Function AlreadInCombo(ByVal oComboBox As ComboBox, ByVal oText As String) As Boolean
        AlreadInCombo = False
        For i As Integer = 0 To oComboBox.Items.Count - 1
            If oComboBox.Items(i).ToString = oText Then
                AlreadInCombo = True
            End If
        Next
    End Function

    Private Sub Cat2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAccCat2.SelectedIndexChanged
        If Me.cmbAccCat2.Text = "" Then Exit Sub
        Me.cmbAccCat3.Text = ""
        Call Fill_Cat3(Me.cmbAccCat2.Text)
    End Sub

    Sub Fill_Cat3(ByVal Cat2 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Cat2) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.cmbAccCat1.Text) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            Me.cmbAccCat3.Items.Clear()
            While datareader.Read
                If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                    Me.cmbAccCat3.Items.Add(datareader("Segment 3 Desc"))
                End If
            End While
            connection.Close()

        Catch ex As Exception
            MsgBox(ex.Message & " 310")
        End Try

    End Sub

    Private Sub ContraCat1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbContraCat1.SelectedIndexChanged
        'First clear other two boxes
        Me.cmbContraCat2.Items.Clear()
        Me.cmbContraCat2.Text = ""
        Me.cmbContraCat3.Items.Clear()
        Me.cmbContraCat3.Text = ""
        Fill_ContraCat2(Me.cmbContraCat1.Text)
    End Sub

    Sub Fill_ContraCat2(ByVal ContraCat1 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(ContraCat1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            With Me.cmbContraCat2.Items
                .Clear()
                While datareader.Read
                    .Add(datareader("Segment 2 Desc"))
                End While
            End With
            connection.Close()

        Catch ex As Exception
            MsgBox(ex.Message & " 311")
        End Try

    End Sub

    Private Sub ContraCat2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbContraCat2.SelectedIndexChanged
        If Me.cmbContraCat2.Text = "" Then Exit Sub
        Me.cmbContraCat3.Text = ""
        Call Fill_ContraCat3(Me.cmbContraCat2.Text)
    End Sub

    Sub Fill_ContraCat3(ByVal ContraCat2 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(ContraCat2) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.cmbContraCat1.Text) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            Me.cmbContraCat3.Items.Clear()
            While datareader.Read
                If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                    Me.cmbContraCat3.Items.Add(datareader("Segment 3 Desc"))
                End If
            End While
            connection.Close()

        Catch ex As Exception
            MsgBox(ex.Message & " 312")
        End Try

    End Sub

    Private Sub Cat3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAccCat3.SelectedIndexChanged
        If Me.cmbAccCat3.Text = "" Then Exit Sub
        Me.lblSegment4Account.Text = Get_MB_GL_Account(Me.cmbAccCat1.Text, Me.cmbAccCat2.Text, Me.cmbAccCat3.Text)
        If Me.lblSegment4Account.Text = "## NO ACCOUNT ##" Then
            MsgBox("There is no account for this category!")
            Me.lblSegment4Account.Text = ""
        End If
    End Sub

    Function Get_GL_MB_Account(ByVal oCategory1 As String, ByVal oCategory2 As String, ByVal oCategory3 As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(oCategory3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(oCategory2) & "' And [Segment 1 Desc] = '" & SQLConvert(oCategory1) & "'"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            While datareader.Read
                If Not datareader("GL ACCOUNT").Equals(DBNull.Value) Then
                    Get_GL_MB_Account = datareader("GL ACCOUNT")
                Else
                    Get_GL_MB_Account = "## NO ACCOUNT ##"
                End If
            End While
            connection.Close()

        Catch ex As Exception

            MsgBox("Error fetching the Magic Box account " & ex.Message & " 313")
        End Try

    End Function

    Private Sub ContraCat3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbContraCat3.SelectedIndexChanged
        Try
            If Me.cmbContraCat3.Text = "" Then Exit Sub
            Me.lblSegment4Contra.Text = Get_MB_GL_Account(Me.cmbContraCat1.Text, Me.cmbContraCat2.Text, Me.cmbContraCat3.Text)
            If Me.lblSegment4Contra.Text = "## NO ACCOUNT ##" Then
                MsgBox("There is no account for this category!")
                Me.lblSegment4Contra.Text = ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 304")
        End Try
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        Try

            Dim oCol1 As String
            Dim oCol2 As String
            Dim oCol3 As String
            Dim oCol4 As String
            Dim oCol5 As String
            Dim oCol6 As String
            Dim oCol7 As String
            Dim oCol8 As String
            Dim oCol9 As String
            Dim oCol10 As String
            Dim oCol11 As String
            Dim oCol12 As String
            Dim oCol13 As String
            Dim oCol14 As String
            Dim oCol15 As String
            Dim oCol16 As String


            With Me.ListView1.SelectedItems(0)
                oCol1 = .SubItems(0).Text
                oCol2 = .SubItems(1).Text
                oCol3 = .SubItems(2).Text
                oCol4 = .SubItems(3).Text
                oCol5 = .SubItems(4).Text
                oCol6 = .SubItems(5).Text
                oCol7 = .SubItems(6).Text
                oCol8 = .SubItems(7).Text
                oCol9 = .SubItems(8).Text
                oCol10 = .SubItems(9).Text
                oCol11 = .SubItems(10).Text
                oCol12 = .SubItems(11).Text
                oCol13 = .SubItems(12).Text
                oCol14 = .SubItems(13).Text
                oCol15 = .SubItems(14).Text
                oCol16 = .SubItems(15).Text
            End With

            Me.Period.Text = oCol1
            Me.Date_DateTimePicker.Value = oCol2
            Me.Reference.Text = oCol3
            Me.Description.Text = oCol4
            Me.cmbTax.Text = oCol5
            Me.txtInclusive.Text = oCol6
            Me.txtExclusive.Text = oCol7
            Me.DC.Text = oCol8
            Me.cmbAccCat1.Text = oCol9
            Me.cmbAccCat2.Text = oCol10
            Me.cmbAccCat3.Text = oCol11
            Me.cmbContraCat1.Text = oCol12
            Me.cmbContraCat2.Text = oCol13
            Me.cmbContraCat3.Text = oCol14
            Me.lblSegment4Account.Text = oCol15
            Me.lblSegment4Contra.Text = oCol16

        Catch ex As Exception
            'don't show error
        End Try
    End Sub

    Sub Balance_Debits_and_Credits()
        ''On Error Resume Next
        ''Dim oTotal As Decimal = 0
        ''For i As Integer = 0 To DataGridView_Supplier.Rows.Count - 1
        ''oTotal = oTotal + Me.DataGridView_Supplier.Rows(i).Cells(1).Value
        ''Next
        ''Me.TextBox_TotBalanced.Text = CStr(oTotal)
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        oClear()
    End Sub
    Sub oClear()

        Me.Reference.Text = ""
        Me.Description.Text = ""
        Me.txtInclusive.Text = ""
        Me.txtExclusive.Text = ""
        Me.cmbAccCat1.Text = ""
        Me.cmbAccCat2.Text = ""
        Me.cmbAccCat3.Text = ""
        Me.lblSegment4Account.Text = ""

        If cmbJournalType.Text = "General Journal" Then
            Me.cmbContraCat1.Text = ""
            Me.cmbContraCat2.Text = ""
            Me.cmbContraCat3.Text = ""
        Else
            cmbJournalTypeValues.SelectedIndex = -1
        End If
        Ckb_StatTran.Checked = False
    End Sub
    Private Sub rdbInclusive_CheckedChanged(sender As Object, e As EventArgs) Handles rdbInclusive.CheckedChanged
        If rdbInclusive.Checked = True Then
            Inclusive = "INCLUSIVE"
            txtInclusive.Enabled = True
            txtExclusive.Enabled = False
            txtInclusive.Focus()
            Try
                TaxPerc = cmbTax.SelectedValue.ToString
                If TaxPerc = 0 Then
                    txtExclusive.Text = txtInclusive.Text
                Else
                    TaxCalc = 1 + (TaxPerc / 100)
                    If txtInclusive.Text.Trim.Length <> 0 Then
                        txtExclusive.Text = Math.Round((txtInclusive.Text / TaxCalc), 2)
                    Else
                        txtExclusive.Text = ""
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub rdbExclusive_CheckedChanged(sender As Object, e As EventArgs) Handles rdbExclusive.CheckedChanged
        If rdbExclusive.Checked = True Then
            Inclusive = "EXCLUSIVE"
            txtInclusive.Enabled = False
            txtExclusive.Enabled = True
            txtExclusive.Focus()
            Try
                TaxPerc = cmbTax.SelectedValue.ToString
                If TaxPerc = 0 Then
                    txtInclusive.Text = txtExclusive.Text
                Else
                    TaxCalc = 1 + (TaxPerc / 100)
                    If txtExclusive.Text.Trim.Length <> 0 Then
                        txtInclusive.Text = Math.Round((txtExclusive.Text * TaxCalc))
                    Else
                        txtInclusive.Text = ""
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub cmbTax_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTax.SelectedIndexChanged
        On Error Resume Next
        If IsNothing(cmbTax.SelectedValue) Then Exit Sub
        TaxPerc = cmbTax.SelectedValue.ToString
        Dim isStat As Boolean = Ckb_StatTran.Checked
        If TaxPerc > 0 And isStat Then
            MsgBox("Statistical transaction has been activated, and you may not have TAX on a statistical transacction.", MsgBoxStyle.Exclamation, "Statistical Transaction")
            cmbTax.SelectedValue = 0
        End If



        If TaxPerc = 0 Then
            IsVat = False
            If rdbExclusive.Checked = True Then
                txtInclusive.Text = txtExclusive.Text
            End If
            If rdbInclusive.Checked = True Then
                txtExclusive.Text = txtInclusive.Text
            End If
        Else
            TaxCalc = 1 + (TaxPerc / 100)
            IsVat = True
            If rdbExclusive.Checked = True Then
                If txtExclusive.Text.Trim.Length <> 0 Then
                    txtInclusive.Text = Math.Round((txtExclusive.Text * TaxCalc))
                Else
                    txtInclusive.Text = ""
                End If
            End If
            If rdbInclusive.Checked = True Then
                If txtInclusive.Text.Trim.Length <> 0 Then
                    txtExclusive.Text = Math.Round((txtInclusive.Text / TaxCalc), 2)
                Else
                    txtExclusive.Text = ""
                End If
            End If
        End If
    End Sub
    Function Get_Segment4_Using_Type_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Type] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If


            If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                Dim oResult As String
                xSQLTable_Accounts.DefaultView.RowFilter = "Type='" & SQLConvert(sString) & "'"
                oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                Get_Segment4_Using_Type_in_Accounting = oResult

            Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment4_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

            End If


        Catch ex As Exception
            MsgBox("There was an error looking up the Segment 4 Account based on Type in Accounting!")
        End Try
    End Function

    Private Sub btn_Templates_Click(sender As Object, e As EventArgs) Handles btn_Templates.Click
        Try
            Dim frm_JournalTemplates As New FrmJournalMaint
            frm_JournalTemplates.ShowDialog()
            If IsNothing(Globals.Ribbons.Ribbon1.JournalTemplate) Then Exit Sub
            If Globals.Ribbons.Ribbon1.JournalTemplate.Rows.Count > 0 Then
                For Each row As DataRow In Globals.Ribbons.Ribbon1.JournalTemplate.Rows
                    Dim str(20) As String
                    Dim itm As ListViewItem
                    str(0) = row.Item(0) 'Period
                    str(1) = row.Item(1) 'TransactionDate
                    str(2) = row.Item(2) 'Reference
                    str(3) = row.Item(3) 'Description
                    str(4) = row.Item(4) 'Tax
                    str(5) = row.Item(5) 'Inclusive
                    str(6) = row.Item(6) 'Exclusive
                    str(7) = row.Item(7) 'DefaultCat
                    str(8) = row.Item(8) 'Line Segment 1 Desc
                    str(9) = row.Item(9) 'Line Segment 2 Desc
                    str(10) = row.Item(10) 'Line Segment 3 Desc
                    str(11) = row.Item(11) 'Contra Segment 1 Desc
                    str(12) = row.Item(12) 'Contra Segment 2 Desc
                    str(13) = row.Item(13) 'Contra Segment 3 Desc
                    str(14) = row.Item(14) 'Line Segment4Account
                    str(15) = row.Item(15) 'Contra Segment4Account
                    str(16) = row.Item(16) 'SupplierID
                    str(17) = row.Item(17) 'SupplierName
                    str(18) = row.Item(18) 'EmployeeID
                    str(19) = row.Item(19) 'JournalType
                    str(20) = row.Item(20) 'Accounting
                    itm = New ListViewItem(str)
                    Me.ListView1.Items.Add(itm)
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function LoadTax()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        '  cmbVATType.Items.Clear()
        '  VATType_ComboBox.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            cmbTax.DataSource = ds.Tables(0)
            cmbTax.ValueMember = "Value"
            cmbTax.DisplayMember = "Display"
            cmbTax.SelectedIndex = 0
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Sub Ckb_StatTran_CheckedChanged(sender As Object, e As EventArgs) Handles Ckb_StatTran.CheckedChanged
        If Ckb_StatTran.Checked = True And cmbTax.SelectedValue.ToString() <> 0 Then
            cmbTax.SelectedValue = 0
            MsgBox("Statistical transactions cannot have a VAT included, changing VAT to Zero", MsgBoxStyle.Exclamation, "Statistical Transation")
        End If
    End Sub
End Class