﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Collections


Public Class frmCreditNotes
#Region "Variables"
    Dim oBatchID As Integer
    Dim oTransactionID As Long
    Dim oLinkID As Integer
    Dim dTransactionDate As Date
    Dim dCaptureDate As Date
    Dim sPPeriod As String
    Dim sFinYear As String
    Dim sGDC As String
    Dim sInvoiceNumber As String
    Dim sDescription As String
    Dim sAccountNumber As String
    Dim sContraAccount As String
    Dim sLinkAcc As String
    Dim oAmount As Decimal
    Dim oTaxType As Integer
    Dim oTaxAmount As Decimal
    Dim sUserID As String
    Dim oSupplierID As Integer
    Dim oEmployeeID As Integer
    Dim sDescription2 As String
    Dim sDescription3 As String
    Dim sDescription4 As String
    Dim sDescription5 As String
    Dim blnPosted As Boolean
    Dim blnAccounting As Boolean
    Dim blnVoid As Boolean
    Dim TransactionType As Integer
    Dim oDocType As String
    Dim oSupplierName As String
    Dim sDRCR As String
    Dim sInfo As String
    Dim sInfo2 As String
    Dim sInfo3 As String

    Dim BatchID_Check As Integer
    Public oNumberPayments As Integer
    Public arrID(10000) As Integer '0
    Public arrSupplierName(10000) As String '1
    Public arrReference(10000) As String '2
    Public arrDateOfPayment(10000) As String '3
    Public arrPaymentAmount(10000) As Decimal '4
    Public arrPaymentType(10000) As String ' 5

    Dim sTransactionDate As String

    Dim xAccessAdapter_Accounts As OleDbDataAdapter
    Dim xAccessTable_Accounts As New DataTable
    Dim xSQLAdapter_Accounts As SqlDataAdapter
    Dim xSQLTable_Accounts As New DataTable

    Dim xAccessAdapter_Suppliers As OleDbDataAdapter
    Dim xAccessTable_Suppliers As New DataTable
    Dim xSQLAdapter_Suppliers As SqlDataAdapter
    Dim xSQLTable_Suppliers As New DataTable

    Dim xAccessAdapter_Periods As OleDbDataAdapter
    Dim xAccessTable_Periods As New DataTable
    Dim xSQLAdapter_Periods As SqlDataAdapter
    Dim xSQLTable_Periods As New DataTable

    Dim oProgressBar As ProgressBar

    Dim blnAt_Least_One_Payment_Request_Paid As Boolean

    Private Property blnFormLoading As Boolean
    Private blnFirstRun As Boolean
    Private blnForm_Hiding As Boolean
    Dim blnSuccessOrFailure
    Dim oDifference_between_Drs_and_Crs As String
    Dim MBData As New DataExtractsClass
#End Region
    Private Sub frmCreditNotes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Credit Notes -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            FillSupplierCombobox()
            Me.dtpFrom.Value = System.DateTime.Now.AddYears(-1)

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with detaills :" & ex.Message)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub FindButton_Click(sender As System.Object, e As System.EventArgs) Handles FindButton.Click
        Try
            Me.Cursor = Cursors.AppStarting
            Me.Enabled = False
            LoadInvoices(CInt(cmbSupplier.SelectedValue), dtpFrom.Value, dtpTo.Value)
            dgvInvoices.Columns("Amount").DefaultCellStyle.Format = "c"
            dgvInvoices.Columns("Due").DefaultCellStyle.Format = "c"

            dgvInvoices.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("Due").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("Reference").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("Transaction Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("SupplierName").Visible = False

            Me.Cursor = Cursors.Arrow
            Me.Enabled = True

        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    'Sub GetInvoiceDetailPerSupplier2()

    '    Try

    '        Me.Enabled = False
    '        Me.Cursor = Cursors.AppStarting

    '        Dim oSupplier As String
    '        oSupplier = Me.cmbSupplier.Text
    '        Dim sSQL_Supplier As String = ""

    '        If oSupplier = "[ALL]" Or oSupplier = "" Then
    '            'do nothing
    '        Else
    '            oSupplier = Me.cmbSupplier.Text
    '            sSQL_Supplier = " And SupplierName = '" & SQLConvert(oSupplier) & "'"
    '        End If

    '        Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
    '        Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")
    '        'Dim sDateCriteria As String = " And ([Transaction Date] >= #" & oFrom.Month & "-" & oFrom.Day & "-" & oFrom.Year & "# And [Transaction Date] <= #" & oTo.Month & "-" & oTo.Day & "-" & oTo.Year & "#)"

    '        Dim sDateCriteria As String
    '        If My.Settings.DBType = "Access" Then
    '            sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
    '        Else
    '            sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
    '        End If

    '        Dim sSQL As String
    '        If Me.cmbStatus.Text = "[ALL]" Then
    '            sSQL = "Select * From [Step4b] Where Due >=0 And Invoiced > 0"
    '        ElseIf Me.cmbStatus.Text = "PAID" Then
    '            sSQL = "Select * From [Step4b] Where Due =0 And Invoiced > 0"
    '        ElseIf Me.cmbStatus.Text = "UNPAID" Then
    '            sSQL = "Select * From [Step4b] Where Due >0 And Invoiced > 0"
    '        End If

    '        sSQL = sSQL & sSQL_Supplier
    '        sSQL = sSQL & sDateCriteria

    '        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '        End If

    '        'First clear DGV
    '        DataGridView1.DataSource = Nothing

    '        System.Threading.Thread.Sleep(1000)

    '        If My.Settings.DBType = "Access" Then
    '            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
    '            Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
    '            Dim ds As New DataSet()
    '            connection.Open()
    '            dataadapter.Fill(ds, "Suppliers_table")
    '            connection.Close()
    '            DataGridView1.DataSource = ds
    '            DataGridView1.DataMember = "Suppliers_table"
    '        ElseIf My.Settings.DBType = "SQL" Then
    '            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
    '            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
    '            Dim ds As New DataSet()
    '            connection.Open()
    '            dataadapter.Fill(ds, "Suppliers_table")
    '            connection.Close()
    '            DataGridView1.DataSource = ds
    '            DataGridView1.DataMember = "Suppliers_table"
    '        End If

    '        DataGridView1.Columns(0).Visible = False
    '        DataGridView1.Columns("Co_ID").Visible = False

    '        Me.Enabled = True
    '        Me.Cursor = Cursors.Arrow

    '    Catch ex As Exception
    '        Me.Enabled = True
    '        Me.Cursor = Cursors.Arrow
    '        MsgBox(ex.Message & " 250")
    '    End Try

    'End Sub
    'Sub Load_Invoices_for_Supplier_to_ComboBox()
    '    Try
    '        If My.Settings.DBType = "Access then" Then
    '            Dim sSQL As String
    '            sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = False"


    '            Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
    '            Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")
    '            Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= #" & oFrom & "# And Transactions.[Transaction Date] <= #" & oTo & "#)"
    '            sSQL = sSQL & sDateCriteria
    '            Dim oSupplier As String
    '            oSupplier = Me.cmbSupplier.Text
    '            If oSupplier <> "[ALL]" Then
    '                oSupplier = Me.cmbSupplier.Text
    '                sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
    '            End If

    '            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '            End If

    '            sSQL = sSQL & " ORDER BY Transactions.Reference"
    '            Dim connection As New OleDbConnection(My.Settings.CS_Setting)

    '            Dim cmd As New OleDbCommand(sSQL, connection)
    '            connection.Open()

    '            Dim datareader As OleDbDataReader = cmd.ExecuteReader
    '            Dim oReference As String
    '            Try

    '                Me.Invoice_Numbers_Combo.Items.Clear()
    '                Me.Invoice_Numbers_Combo.Items.Add("[ALL]")
    '                While datareader.Read
    '                    If Not IsDBNull(datareader("Invoice Number")) = True Then
    '                        oReference = datareader("Invoice Number").ToString()
    '                        Me.Invoice_Numbers_Combo.Items.Add(oReference)
    '                    End If
    '                End While
    '                Me.Invoice_Numbers_Combo.SelectedIndex = 0

    '                connection.Close()

    '            Catch ex As Exception
    '                MsgBox(ex.Message & " 251")
    '                connection.Close()
    '            End Try
    '        ElseIf My.Settings.DBType = "SQL" Then
    '            Dim sSQL As String
    '            sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = 0"
    '            Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
    '            Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")
    '            Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= '" & oFrom & "' And Transactions.[Transaction Date] <= '" & oTo & "')"
    '            sSQL = sSQL & sDateCriteria
    '            Dim oSupplier As String
    '            oSupplier = Me.cmbSupplier.Text
    '            If oSupplier <> "[ALL]" Then
    '                oSupplier = Me.cmbSupplier.Text
    '                sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
    '            End If

    '            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '            End If

    '            sSQL = sSQL & " ORDER BY Transactions.Reference"
    '            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

    '            Dim cmd As New SqlCommand(sSQL, connection)
    '            connection.Open()

    '            Dim datareader As SqlDataReader = cmd.ExecuteReader

    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message & " 252")
    '    End Try
    'End Sub
    Private Sub Button_Activate_Click(sender As System.Object, e As System.EventArgs)
        Try
            Me.Label_Level.Text = CheckUserLevel(Me.txtUserName.Text, Me.txtPassword.Text)
        Catch ex As Exception
            MsgBox(ex.Message & " 253")
        End Try
    End Sub
    Private Sub Button_Activate_Click_1(sender As System.Object, e As System.EventArgs) Handles Button_Activate.Click
        Try
            Me.Label_Level.Text = CheckUserLevel(Me.txtUserName.Text, Me.txtPassword.Text)
        Catch ex As Exception
            MsgBox(ex.Message & " 154")
        End Try
    End Sub
    Sub FillSupplierCombobox()
        Try

            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Private Sub Delete_Invoice_Button_Click(sender As System.Object, e As System.EventArgs) Handles Delete_Invoice_Button.Click

        Try

            If DGV_Transactions.Rows.Count <> 0 Then
                DGV_Transactions.Rows.Clear()
            End If

            Me.lblCreditNotePassed.Visible = False

            If Me.txtCreditAmount.Text = "" Then
                MessageBox.Show("Please enter the amount to you want to credit on the invoice.", "Invoice credit note", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtCreditAmount.Focus()
                Exit Sub
            End If

            If CDec(Me.txtCreditAmount.Text) > Me.dgvInvoices.Rows(Me.dgvInvoices.SelectedCells(0).RowIndex).Cells(4).Value Then
                MessageBox.Show("Please enter an amount equal to or lower than the Due amount.", "Invoice credit note", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtCreditAmount.Focus()
                Exit Sub
            End If

            If Me.dgvInvoices.SelectedCells.Count < 1 Then
                MessageBox.Show("Please select an invoice in order to continue", "Invoice credit note", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                dgvInvoices.Focus()
                Exit Sub
            End If

            Dim TransactionID As String = Me.dgvInvoices.SelectedRows(0).Cells("TransactionID").Value

            If CheckForRequests(TransactionID) > 0 Then
                MessageBox.Show("There are payment(s) request against this invoice, please pay/remove the requests before you attempt to credit this invoice", _
                                "Invoice Movements", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.dgvInvoices.SelectedRows.Count < 1 Then
                Me.dgvInvoices.Rows(Me.dgvInvoices.SelectedCells(0).RowIndex).Selected = True
            End If

            Dim oFirstCreditAmount As Decimal

            Dim oRowIndex As Integer
            oRowIndex = Me.dgvInvoices.SelectedRows(0).Index

            Dim oAmount As Decimal
            oAmount = Me.dgvInvoices.SelectedRows(0).Cells(3).Value
            Dim oDue As Decimal
            oDue = Me.dgvInvoices.SelectedRows(0).Cells(4).Value
            ' If oDue <> oAmount Then
            ' If Me.Label_Level.Text <> "Super" Then
            ' MessageBox.Show("Payments have been made on this invoice. Please activate the required level to credit this invoice.", _
            '               "Credit Notes", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            'Exit Sub
            'End If
            'End If

            Dim oTransactionDate As Date = CDate(Me.dgvInvoices.SelectedRows(0).Cells(5).Value)
            ' If oTransactionDate < Now.Date Then
            ' If Me.Label_Level.Text <> "Super" Then
            ' MessageBox.Show("You need to activate the required user level to enter a credit note for an invoice invoice that wasn't entered into the system today!", _
            '                 "Credit Notes", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            'Exit Sub
            'End If
            'End If

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            'Now gather supplier ID and invoice number and see if any payments made on that invoice
            Dim sSupplierName As String = Me.dgvInvoices.SelectedRows(0).Cells(1).Value
            Dim oSupplierID As Integer
            oSupplierID = Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") 'Supplier Name

            Dim oTransactionID As Integer = Me.dgvInvoices.SelectedRows(0).Cells(0).Value   'GetTransactionID_FromID(oID)
            Dim oGrossAmount As Decimal = Get_Invoice_Gross_Amount(oTransactionID)

            Loop_Through_Transactions_With_Same_TransactionID(oTransactionID, oGrossAmount)

            If Me.DGV_Transactions.RowCount > 1 Then 'always one Dr  & more than 1 Credit
                If Check_Integrity(Me.DGV_Transactions) = False Then
                    'If Drs and Crs in DataGridView are out of balance then fill the gap
                    oFirstCreditAmount = Me.DGV_Transactions.Rows(1).Cells(12).Value
                    '******* NOTE ***********************************************************************************************
                    'Regardless of whether the difference is minus or positive always add it on. This is because if it is eg -0.01 then +-0.01
                    Me.DGV_Transactions.Rows(1).Cells(12).Value = oFirstCreditAmount + oDifference_between_Drs_and_Crs
                    '*************************************************************************************************************
                    'Now run the integrity check one last time
                    If Check_Integrity(Me.DGV_Transactions) = False Then
                        MsgBox("Balances are out. Transaction cannot be completed!")
                        Me.DGV_Transactions.Rows.Clear()
                        Me.Enabled = True
                        Me.Cursor = Cursors.Arrow
                        Exit Sub
                    Else
                        GoTo Jump
                    End If
                Else
Jump:
                    If My.Settings.DBType = "Access" Then
                        Call AppendTransactions(Me.DGV_Transactions, "Transactions")
                    ElseIf My.Settings.DBType = "SQL" Then
                        Call AppendTransactions_SQL(Me.DGV_Transactions, "Transactions")
                    End If
                End If
            End If

            dgvInvoices.DataSource = Nothing
            lblCreditNotePassed.Visible = False
            LoadInvoices(CInt(cmbSupplier.SelectedValue), dtpFrom.Value, dtpTo.Value)
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub Loop_Through_Transactions_With_Same_TransactionID(ByVal oTransactionID As Integer, ByVal oGrossAmount As Decimal)
        Try

            Dim oCreditAmount As Decimal = Me.txtCreditAmount.Text
            Dim oProportion As Decimal = oCreditAmount / oGrossAmount

            Dim xID As Integer
            Dim sSQL As String
            Dim oNewBatchID As Long = CLng(oCreateBatchID())
            Dim oNewTransactionID = CLng(oCreateTransactionID())
            Dim oNewLinkID As Long
            Dim BlnAccounting As Boolean

            If My.Settings.Auto_Approve_CreditNotes = "Yes" Or My.Settings.Auto_Approve_CreditNotes = "" Or My.Settings.Auto_Approve_CreditNotes = Nothing Then
                BlnAccounting = True
            Else
                BlnAccounting = False
            End If


            Dim blnFirstRow As Boolean = True 'set to false after first read
            'Must look at ones with 'Invoice' in 'Info2' otherwise includes credit notes
            sSQL = "Select * From Transactions Where TransactionID = " & oTransactionID '& " And Info = 'Invoice'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            sSQL = sSQL & " Order By ID"

            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Dim oNewAmount As Decimal
                While datareader.Read
                    If Not IsDBNull(datareader("ID")) Then
                        xID = datareader("ID") 'ID is unique ID or that line
                        Load_Invoice_Data_From_ID_to_Variables(xID)

                        oNewAmount = Math.Round(cAmount * oProportion, 2) 'MDB

                        If cDRCR = "Dr" Then
                            cDRCR = "Cr"
                        ElseIf cDRCR = "Cr" Then
                            cDRCR = "Dr"
                        End If
                        cInfo2 = "Credit Note" 'oNEW
                        cGDC = "D"
                        cTaxAmount = Math.Round(cTaxAmount * oProportion, 2)
                        '/// NOTE - Dates have to be converted to proper format otherwise FAIL
                        Dim strTransactionDate As String = cTransactionDate.ToString("dd MMMM yyyy")
                        Dim strCaptureDate As String = cCaptureDate.ToString("dd MMMM yyyy")


                        oNewLinkID = cTransactionID 'only the TransactionID must be same as Invoice LinkID

                        Dim Capdate As String = Date.Now.ToString("dd MMMM yyyy") & " " & Date.Now.ToLongTimeString

                        Call Add_Transaction_to_DGV("DGV_Transactions", oNewBatchID, oNewTransactionID, oNewLinkID, strTransactionDate, Capdate, cPPeriod, cFinYear, cGDC, cInvoiceNumber, _
    cDescription, cAccountNumber, cLinkAcc, oNewAmount, cTaxType, cTaxAmount, cUserID, _
    cSupplierID, cEmployeeID, cDescription2, cDescription3, cDescription4, cDescription5, cPosted, _
    BlnAccounting, cVoid, cTransactionType, cDRCR, cContraAccount, cDescriptionCode, cDocType, cSupplierName, cInfo, cInfo2, cInfo3)

                    End If
                End While
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 256")
        Finally

        End Try

    End Sub


    Public Sub Add_Transaction_to_DGV(ByVal oWhich_DGV As String, ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                     ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                     ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                     ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                     ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                     ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                     ByVal oDocType As String, ByVal oSupplierName As String, ByVal oInfo As String, ByVal oInfo2 As String, ByVal oInfo3 As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)

            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName
            newRow.Cells(31).Value = oInfo
            newRow.Cells(32).Value = oInfo2
            newRow.Cells(33).Value = oInfo3


            If oWhich_DGV = "DGV_Transactions" Then
                Me.DGV_Transactions.Rows.Add(newRow)
            Else
                MsgBox("ERROR ON DGV CLEANUP")
            End If
            'ElseIf oWhich_DGV = "DGV_Transactions_Credit" Then
            'Me.DGV_Transactions_Credit.Rows.Add(newRow)
            'ElseIf oWhich_DGV = "DGV_Transactions_EFT" Then
            'Me.DGV_Transactions_EFT.Rows.Add(newRow)
            'End If

            '


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox(ex.Message & " 257")
        End Try


    End Sub

    Sub AppendTransactions(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String

        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value



                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value

                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                'sSQL = sSQL & "#" & CYear & "-" & CMonth & "-" & CDay & "# As MyDate1, "
                'sSQL = sSQL & "#" & TYear & "-" & TMonth & "-" & TDay & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & "258")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String



        Dim i As Integer
        Dim cmd As SqlCommand


        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value
                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value
                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value

                If My.Settings.User_Control_On = "No" Then
                    If My.Settings.Auto_Approve_CreditNotes = "Yes" Then
                        blnAccounting = True
                    Else
                        blnAccounting = False
                    End If
                ElseIf My.Settings.User_Control_On = "Yes" Then
                    If My.Settings.Auto_Approve_CreditNotes = "Yes" Then
                        blnAccounting = True
                    Else
                        blnAccounting = False
                    End If
                ElseIf My.Settings.User_Control_On = "" Then
                    blnAccounting = False
                End If

                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                Dim AccString As String = "''"
                If blnAccounting = "True" Then
                    AccString = "'1'"
                ElseIf blnAccounting = "False" Then
                    AccString = "'0'"
                End If

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(AccString) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & "259")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Function Check_Integrity(ByVal oDataGridView As DataGridView) As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To oDataGridView.RowCount - 2 'extra row
                If oDataGridView.Rows(i).Cells(26).Value = "Dr" Then
                    oDebits = oDebits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                ElseIf oDataGridView.Rows(i).Cells(26).Value = "Cr" Then
                    oCredits = oCredits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                End If
            Next

            If oDebits = oCredits Then
                Check_Integrity = True
            Else
                Check_Integrity = False
                oDifference_between_Drs_and_Crs = oDebits - oCredits
            End If


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox("There was an error checking integrity " & ex.Message & " 260")
        End Try
    End Function
    Private Sub txtCreditAmount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCreditAmount.TextChanged
        If IsNumeric(Me.txtCreditAmount.Text) = False Then
            MsgBox("Please enter a numeric amount!")
            txtCreditAmount.Text = ""
            Exit Sub
        End If
    End Sub
    Function Get_Invoice_Gross_Amount(ByVal sTransactionID As String) As String
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT Amount FROM Transactions"
            sSQL = sSQL & " WHERE TransactionID = " & sTransactionID
            sSQL = sSQL & " AND DocType = 'Invoice'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Invoice_Gross_Amount = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Invoice_Gross_Amount = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error getting the gross amount of the invoice to be credited. " & ex.Message & " et500")
        End Try
    End Function
    Public Sub LoadInvoices(ByVal SupplierID As Integer, ByVal fromDate As Date, ByVal toDate As Date)

        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim d1 As String = fromDate.ToString("dd MMMM yyyy")
            Dim d2 As String = toDate.ToString("dd MMMM yyyy")

            Dim sSQL As String = ""
            sSQL = CStr("Select * from dbo.Func_GeneralInvoicesQuery(" & My.Settings.Setting_CompanyID) & ", " _
                & SupplierID & ",'" & d1 & "','" & d2 & "')"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "SupplierInvoices")
            connection.Close()
            dgvInvoices.DataSource = ds
            dgvInvoices.DataMember = "SupplierInvoices"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Public Function CheckForRequests(ByVal TransID As Integer) As Integer

        Dim CountRequests As Integer = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "SELECT Count(*) FROM PaymentsRequests WHERE InvoiceTransactionID = " & TransID
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()

            CountRequests = CInt(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return CountRequests

        Catch ex As Exception
            MsgBox("An error occured with details: " & ex.Message & ".")
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
                cn.Dispose()
            End If
        End Try
    End Function
End Class