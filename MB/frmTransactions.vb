﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmTransactions

#Region "Variables"
    Dim dbadp As OleDbDataAdapter
    Dim dTable As New DataTable
    Dim dbadp2 As SqlDataAdapter
    Dim dTable2 As New DataTable
    Dim MBData As New DataExtractsClass
#End Region


    Private Sub Transactions_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            e.Cancel = True
            Me.ShowInTaskbar = False
            Me.Hide()
        End If
    End Sub

    Private Sub Transactions_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Transactions -> Version : " & My.Settings.Setting_Version
            Me.Label_Version.Text = "Version " & My.Settings.Setting_Version
            Me.DateTimePicker_From.Value = System.DateTime.Now.AddDays(-14)
            Call FillComboBoxSuppliers()
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub
    Sub FillComboBoxSuppliers()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub GetTransactions()

        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            Dim oSupplier As String
            oSupplier = Me.cmbSupplier.Text
            Dim sSQL_Supplier As String = ""

            If oSupplier = "" Or oSupplier = "" Then
                'do nothing
            Else
                oSupplier = Me.cmbSupplier.Text
                sSQL_Supplier = " Where SupplierName = '" & SQLConvert(oSupplier) & "'"
            End If

            'First clear DGV
            DataGridView1.DataSource = Nothing

            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.DateTimePicker_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTimePicker_To.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String

            If sSQL_Supplier = "" Then

                If My.Settings.DBType = "Access" Then
                    sDateCriteria = " Where ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
                Else
                    sDateCriteria = " Where ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
                End If

            Else

                If My.Settings.DBType = "Access" Then
                    sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
                Else
                    sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
                End If

            End If

            Dim sSQL As String

            sSQL = "Select * From Transactions"

            sSQL = sSQL & sSQL_Supplier

            sSQL = sSQL & sDateCriteria

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND [Description Code] = 'HAAAAA' And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If Reference.Text <> "" Then
                sSQL = sSQL & " And Reference = '" & Reference.Text & "'"
            End If

            DataGridView1.DataSource = Nothing

            If My.Settings.DBType = "Access" Then
                dTable.Clear()
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                dbadp = New OleDbDataAdapter(sSQL, connection)
                dbadp.Fill(dTable)
                Me.DataGridView1.DataSource = dTable
            ElseIf My.Settings.DBType = "SQL" Then
                dTable2.Clear()
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                dbadp2 = New SqlDataAdapter(sSQL, connection)
                dbadp2.Fill(dTable2)
                Me.DataGridView1.DataSource = dTable2
            End If


            If DataGridView1.ColumnCount > 0 Then
                Me.DataGridView1.Columns(0).Visible = False

                Me.DataGridView1.Columns(1).Visible = False
                Me.DataGridView1.Columns(2).Visible = False
                Me.DataGridView1.Columns(3).Visible = False
                Me.DataGridView1.Columns("UserID").Visible = False
                Me.DataGridView1.Columns("supplierID").Visible = False
                Me.DataGridView1.Columns("EmployeeID").Visible = False
                Me.DataGridView1.Columns("Co_ID").Visible = False
                Me.DataGridView1.Columns("Amount").DefaultCellStyle.Format = "C"
                Me.DataGridView1.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                Me.DataGridView1.Columns("Tax Amount").DefaultCellStyle.Format = "C"
                Me.DataGridView1.Columns("Tax Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                'Me.DataGridView1.Columns(3).DefaultCellStyle.Format = "n2"
                'Me.DataGridView1.Columns(4).DefaultCellStyle.Format = "n2"
                'Me.DataGridView1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                'Me.DataGridView1.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 319")
        End Try

    End Sub


    Sub Fill_DGV()
        Try
            Dim sSQL As String

            sSQL = "Select * From Transactions"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                dbadp = New OleDbDataAdapter(sSQL, connection)
                dbadp.Fill(dTable)
                Me.DataGridView1.DataSource = dTable
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                dbadp2 = New SqlDataAdapter(sSQL, connection)
                dbadp2.Fill(dTable2)
                Me.DataGridView1.DataSource = dTable2
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 209")
        End Try
    End Sub

    Sub oSave_Access()
        Try
            Dim builder As New OleDbCommandBuilder(dbadp)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            dbadp.Update(dTable)
            dbadp.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub
    Sub oSave_SQL()
        Try
            Dim builder As New SqlCommandBuilder(dbadp2)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            dbadp2.Update(dTable2)
            dbadp2.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Sub Load_Invoices_for_Supplier_to_ComboBox()
        Try
            If My.Settings.DBType = "Access then" Then
                Dim sSQL As String
                sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = False"

                Dim oFrom As String = Me.DateTimePicker_From.Value.ToString
                Dim oTo As String = Me.DateTimePicker_To.Value.ToString
                Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= #" & oFrom & "# And Transactions.[Transaction Date] <= #" & oTo & "#)"
                sSQL = sSQL & sDateCriteria
                Dim oSupplier As String
                oSupplier = Me.cmbSupplier.Text
                If oSupplier <> "[ALL]" Then
                    oSupplier = Me.cmbSupplier.Text
                    sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
                End If

                If Me.Reference.Text <> "" Then
                    sSQL = sSQL & " And Reference = " & Me.Reference.Text
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                sSQL = sSQL & " ORDER BY Transactions.Reference"
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)

                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()

                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Dim oReference As String
                Try

                    Me.Reference.Items.Clear()
                    Me.Reference.Items.Add("[ALL]")
                    While datareader.Read
                        If Not IsDBNull(datareader("Invoice Number")) = True Then
                            oReference = datareader("Invoice Number").ToString()
                            Me.Reference.Items.Add(oReference)
                        End If
                    End While
                    Me.Reference.SelectedIndex = 0

                    connection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message & " 033b")
                    connection.Close()
                End Try
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = 0"
                Dim oFrom As String = Me.DateTimePicker_From.Value.ToString
                Dim oTo As String = Me.DateTimePicker_To.Value.ToString
                Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= '" & oFrom & "' And Transactions.[Transaction Date] <= '" & oTo & "')"
                sSQL = sSQL & sDateCriteria
                Dim oSupplier As String
                oSupplier = Me.cmbSupplier.Text
                If oSupplier <> "[ALL]" Then
                    oSupplier = Me.cmbSupplier.Text
                    sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
                End If

                If Me.Reference.Text <> "" Then
                    sSQL = sSQL & " And Reference = " & Me.Reference.Text
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                sSQL = sSQL & " ORDER BY Transactions.Reference"
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()

                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Dim oReference As String
                Try
                    Me.Reference.Items.Clear()
                    Me.Reference.Items.Add("[ALL]")
                    While datareader.Read
                        If Not IsDBNull(datareader("Invoice Number")) = True Then
                            oReference = datareader("Invoice Number").ToString()
                            Me.Reference.Items.Add(oReference)
                        End If
                    End While
                    Me.Reference.SelectedIndex = 0
                    connection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message & " 034b")
                    connection.Close()
                End Try
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 035b")
        End Try
    End Sub


    Private Sub btnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRun.Click
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Call GetTransactions()
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured :" & ex.Message, "Transactions", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub Reference_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Reference.SelectedIndexChanged

    End Sub

    Private Sub chkAlter_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAlter.CheckedChanged
        Try
            If Me.chkAlter.Checked = True Then
                Me.Panel1.Visible = True
            Else
                Me.Panel1.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        Try
            If txtUserName.Text = "s" And txtPassword.Text = "x" Then
                'If txtUserName.Text = "SuperUser" And txtPassword.Text = "zxcvbnm,./" Then
                Me.lblLoggedIn.Visible = True
                Me.btnSaveChanges.Visible = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveChanges_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveChanges.Click
        Try
            If Check_Integrity() = False Then
                MsgBox("Cannot Save Changes! Balance out! Please make sure Dr Amounts equal Cr Amounts")
                Exit Sub
            End If

            If My.Settings.DBType = "Access" Then
                Call oSave_Access()
            ElseIf My.Settings.DBType = "SQL" Then
                Call oSave_SQL()
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Function Check_Integrity() As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To Me.DataGridView1.RowCount - 2 'extra row
                If Me.DataGridView1.Rows(i).Cells(27).Value = "Dr" Then
                    oDebits = oDebits + CDec(Me.DataGridView1.Rows(i).Cells(13).Value)
                ElseIf Me.DataGridView1.Rows(i).Cells(27).Value = "Cr" Then
                    oCredits = oCredits + CDec(Me.DataGridView1.Rows(i).Cells(13).Value)
                End If
            Next

            If oDebits = oCredits Then
                Check_Integrity = True
            Else
                Check_Integrity = False
            End If


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox("There was an error checking integrity " & ex.Message & " 024")
        End Try
    End Function

    Private Sub btnEditSelectedTransaction_Click(sender As System.Object, e As System.EventArgs) Handles btnEditSelectedTransaction.Click
        Try

            If Me.DataGridView1.SelectedCells.Count < 1 Then
                MsgBox("Please select a Transaction")
                Exit Sub
            End If

            Dim oSelectedCellRowIndex As Integer
            oSelectedCellRowIndex = Me.DataGridView1.SelectedCells(0).RowIndex

            Dim oTransactionID As String
            oTransactionID = Me.DataGridView1.Rows(oSelectedCellRowIndex).Cells(2).Value

            Dim T2 As New frmTransactions2
            T2.lblTransactionID.Text = oTransactionID

            T2.Show()
            T2.BringToFront()
            Me.SendToBack()
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class