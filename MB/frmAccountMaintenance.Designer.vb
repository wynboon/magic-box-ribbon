﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountMaintenance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountMaintenance))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCode1 = New System.Windows.Forms.TextBox()
        Me.txtDesc1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ckbIsNew = New System.Windows.Forms.CheckBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDesc3 = New System.Windows.Forms.TextBox()
        Me.txtCode3 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDesc2 = New System.Windows.Forms.TextBox()
        Me.txtCode2 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAccount = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtAccountDescription = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtReportingCategory = New System.Windows.Forms.TextBox()
        Me.cmbFinancialCategory = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtDisplayName = New System.Windows.Forms.TextBox()
        Me.txtType = New System.Windows.Forms.TextBox()
        Me.btnUpdateChanges = New System.Windows.Forms.Button()
        Me.btnMakeAPayout = New System.Windows.Forms.Button()
        Me.btnDeleteAccount = New System.Windows.Forms.Button()
        Me.rdbIncomeStatement = New System.Windows.Forms.RadioButton()
        Me.rdbBalanceSheet = New System.Windows.Forms.RadioButton()
        Me.lblID = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblSegment4CountInTransactions = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnMigrateTransactions = New System.Windows.Forms.Button()
        Me.txtBankAccNo = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Btn_MakeTender = New System.Windows.Forms.Button()
        Me.TB_GLAcc = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtBankBranchCode = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.CB_BankLayout = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Location = New System.Drawing.Point(4, 4)
        Me.ListBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(285, 324)
        Me.ListBox1.TabIndex = 0
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 16
        Me.ListBox2.Location = New System.Drawing.Point(299, 4)
        Me.ListBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(292, 324)
        Me.ListBox2.TabIndex = 1
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.ItemHeight = 16
        Me.ListBox3.Location = New System.Drawing.Point(600, 4)
        Me.ListBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(292, 324)
        Me.ListBox3.TabIndex = 2
        '
        'btnNew
        '
        Me.btnNew.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnNew.Location = New System.Drawing.Point(4, 351)
        Me.btnNew.Margin = New System.Windows.Forms.Padding(4)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(889, 38)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(15, 37)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Code 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(15, 75)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Desc 1"
        '
        'txtCode1
        '
        Me.txtCode1.Enabled = False
        Me.txtCode1.Location = New System.Drawing.Point(92, 33)
        Me.txtCode1.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCode1.Name = "txtCode1"
        Me.txtCode1.Size = New System.Drawing.Size(89, 22)
        Me.txtCode1.TabIndex = 0
        '
        'txtDesc1
        '
        Me.txtDesc1.Location = New System.Drawing.Point(92, 71)
        Me.txtDesc1.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesc1.Name = "txtDesc1"
        Me.txtDesc1.Size = New System.Drawing.Size(301, 22)
        Me.txtDesc1.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ckbIsNew)
        Me.GroupBox1.Controls.Add(Me.btnAdd)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtDesc3)
        Me.GroupBox1.Controls.Add(Me.txtCode3)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtDesc2)
        Me.GroupBox1.Controls.Add(Me.txtCode2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDesc1)
        Me.GroupBox1.Controls.Add(Me.txtCode1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(901, 4)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(403, 361)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "New Account"
        '
        'ckbIsNew
        '
        Me.ckbIsNew.AutoSize = True
        Me.ckbIsNew.Location = New System.Drawing.Point(92, 300)
        Me.ckbIsNew.Margin = New System.Windows.Forms.Padding(4)
        Me.ckbIsNew.Name = "ckbIsNew"
        Me.ckbIsNew.Size = New System.Drawing.Size(83, 21)
        Me.ckbIsNew.TabIndex = 6
        Me.ckbIsNew.Tag = "S_IsNew"
        Me.ckbIsNew.Text = "Is New ?"
        Me.ckbIsNew.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Enabled = False
        Me.btnAdd.Location = New System.Drawing.Point(191, 289)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(204, 39)
        Me.btnAdd.TabIndex = 7
        Me.btnAdd.Text = "&Save New"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(15, 224)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Code 3"
        '
        'txtDesc3
        '
        Me.txtDesc3.Location = New System.Drawing.Point(92, 258)
        Me.txtDesc3.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesc3.Name = "txtDesc3"
        Me.txtDesc3.Size = New System.Drawing.Size(301, 22)
        Me.txtDesc3.TabIndex = 5
        '
        'txtCode3
        '
        Me.txtCode3.Enabled = False
        Me.txtCode3.Location = New System.Drawing.Point(92, 220)
        Me.txtCode3.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCode3.Name = "txtCode3"
        Me.txtCode3.Size = New System.Drawing.Size(89, 22)
        Me.txtCode3.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(15, 262)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 17)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Desc 3"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(15, 132)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Code 2"
        '
        'txtDesc2
        '
        Me.txtDesc2.Location = New System.Drawing.Point(92, 166)
        Me.txtDesc2.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesc2.Name = "txtDesc2"
        Me.txtDesc2.Size = New System.Drawing.Size(301, 22)
        Me.txtDesc2.TabIndex = 3
        '
        'txtCode2
        '
        Me.txtCode2.Enabled = False
        Me.txtCode2.Location = New System.Drawing.Point(92, 128)
        Me.txtCode2.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCode2.Name = "txtCode2"
        Me.txtCode2.Size = New System.Drawing.Size(89, 22)
        Me.txtCode2.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(15, 170)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 17)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Desc 2"
        '
        'txtAccount
        '
        Me.txtAccount.Enabled = False
        Me.txtAccount.Location = New System.Drawing.Point(196, 405)
        Me.txtAccount.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAccount.Name = "txtAccount"
        Me.txtAccount.Size = New System.Drawing.Size(356, 22)
        Me.txtAccount.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(4, 405)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 17)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Account"
        '
        'txtAccountDescription
        '
        Me.txtAccountDescription.Enabled = False
        Me.txtAccountDescription.Location = New System.Drawing.Point(196, 440)
        Me.txtAccountDescription.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAccountDescription.Name = "txtAccountDescription"
        Me.txtAccountDescription.Size = New System.Drawing.Size(356, 22)
        Me.txtAccountDescription.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(0, 444)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(138, 17)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Account Description:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(0, 503)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(129, 17)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Financial Category:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(0, 543)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(135, 17)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Reporting Category:"
        '
        'txtReportingCategory
        '
        Me.txtReportingCategory.Location = New System.Drawing.Point(196, 540)
        Me.txtReportingCategory.Margin = New System.Windows.Forms.Padding(4)
        Me.txtReportingCategory.Name = "txtReportingCategory"
        Me.txtReportingCategory.Size = New System.Drawing.Size(356, 22)
        Me.txtReportingCategory.TabIndex = 10
        '
        'cmbFinancialCategory
        '
        Me.cmbFinancialCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbFinancialCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbFinancialCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFinancialCategory.FormattingEnabled = True
        Me.cmbFinancialCategory.Location = New System.Drawing.Point(196, 503)
        Me.cmbFinancialCategory.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbFinancialCategory.Name = "cmbFinancialCategory"
        Me.cmbFinancialCategory.Size = New System.Drawing.Size(356, 24)
        Me.cmbFinancialCategory.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(4, 581)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 17)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Display Name:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(4, 616)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 17)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Type:"
        '
        'txtDisplayName
        '
        Me.txtDisplayName.Location = New System.Drawing.Point(196, 574)
        Me.txtDisplayName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDisplayName.Name = "txtDisplayName"
        Me.txtDisplayName.Size = New System.Drawing.Size(356, 22)
        Me.txtDisplayName.TabIndex = 11
        '
        'txtType
        '
        Me.txtType.Location = New System.Drawing.Point(196, 610)
        Me.txtType.Margin = New System.Windows.Forms.Padding(4)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(356, 22)
        Me.txtType.TabIndex = 12
        '
        'btnUpdateChanges
        '
        Me.btnUpdateChanges.ForeColor = System.Drawing.Color.Navy
        Me.btnUpdateChanges.Location = New System.Drawing.Point(600, 405)
        Me.btnUpdateChanges.Margin = New System.Windows.Forms.Padding(4)
        Me.btnUpdateChanges.Name = "btnUpdateChanges"
        Me.btnUpdateChanges.Size = New System.Drawing.Size(293, 39)
        Me.btnUpdateChanges.TabIndex = 14
        Me.btnUpdateChanges.Tag = "S_UpdateChanges"
        Me.btnUpdateChanges.Text = "&Update Changes"
        Me.btnUpdateChanges.UseVisualStyleBackColor = True
        '
        'btnMakeAPayout
        '
        Me.btnMakeAPayout.ForeColor = System.Drawing.Color.Navy
        Me.btnMakeAPayout.Location = New System.Drawing.Point(600, 456)
        Me.btnMakeAPayout.Margin = New System.Windows.Forms.Padding(4)
        Me.btnMakeAPayout.Name = "btnMakeAPayout"
        Me.btnMakeAPayout.Size = New System.Drawing.Size(293, 39)
        Me.btnMakeAPayout.TabIndex = 15
        Me.btnMakeAPayout.Text = "&Make a Payout"
        Me.btnMakeAPayout.UseVisualStyleBackColor = True
        '
        'btnDeleteAccount
        '
        Me.btnDeleteAccount.ForeColor = System.Drawing.Color.Navy
        Me.btnDeleteAccount.Location = New System.Drawing.Point(600, 546)
        Me.btnDeleteAccount.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDeleteAccount.Name = "btnDeleteAccount"
        Me.btnDeleteAccount.Size = New System.Drawing.Size(293, 39)
        Me.btnDeleteAccount.TabIndex = 16
        Me.btnDeleteAccount.Text = "&Delete Account"
        Me.btnDeleteAccount.UseVisualStyleBackColor = True
        '
        'rdbIncomeStatement
        '
        Me.rdbIncomeStatement.AutoSize = True
        Me.rdbIncomeStatement.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbIncomeStatement.ForeColor = System.Drawing.Color.Blue
        Me.rdbIncomeStatement.Location = New System.Drawing.Point(196, 474)
        Me.rdbIncomeStatement.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbIncomeStatement.Name = "rdbIncomeStatement"
        Me.rdbIncomeStatement.Size = New System.Drawing.Size(128, 19)
        Me.rdbIncomeStatement.TabIndex = 7
        Me.rdbIncomeStatement.Text = "Income Statement"
        Me.rdbIncomeStatement.UseVisualStyleBackColor = True
        '
        'rdbBalanceSheet
        '
        Me.rdbBalanceSheet.AutoSize = True
        Me.rdbBalanceSheet.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbBalanceSheet.ForeColor = System.Drawing.Color.Blue
        Me.rdbBalanceSheet.Location = New System.Drawing.Point(383, 474)
        Me.rdbBalanceSheet.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbBalanceSheet.Name = "rdbBalanceSheet"
        Me.rdbBalanceSheet.Size = New System.Drawing.Size(108, 19)
        Me.rdbBalanceSheet.TabIndex = 8
        Me.rdbBalanceSheet.Text = "Balance Sheet"
        Me.rdbBalanceSheet.UseVisualStyleBackColor = True
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.ForeColor = System.Drawing.Color.Maroon
        Me.lblID.Location = New System.Drawing.Point(611, 641)
        Me.lblID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(563, 17)
        Me.lblID.TabIndex = 26
        Me.lblID.Text = "This holds the ID on Cat3 selection - and is then used in ""Update Changes"" and ""D" & _
    "elete"""
        Me.lblID.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(584, 554)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(0, 17)
        Me.Label13.TabIndex = 27
        '
        'DataGridView1
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Location = New System.Drawing.Point(901, 372)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(403, 225)
        Me.DataGridView1.TabIndex = 28
        Me.DataGridView1.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.Gray
        Me.Label14.Location = New System.Drawing.Point(596, -20)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(126, 17)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "click on one to edit"
        '
        'lblSegment4CountInTransactions
        '
        Me.lblSegment4CountInTransactions.AutoSize = True
        Me.lblSegment4CountInTransactions.ForeColor = System.Drawing.Color.Gray
        Me.lblSegment4CountInTransactions.Location = New System.Drawing.Point(989, 657)
        Me.lblSegment4CountInTransactions.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSegment4CountInTransactions.Name = "lblSegment4CountInTransactions"
        Me.lblSegment4CountInTransactions.Size = New System.Drawing.Size(13, 17)
        Me.lblSegment4CountInTransactions.TabIndex = 30
        Me.lblSegment4CountInTransactions.Text = "-"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(1080, 657)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(215, 17)
        Me.Label15.TabIndex = 31
        Me.Label15.Text = "transactions against this account"
        '
        'btnMigrateTransactions
        '
        Me.btnMigrateTransactions.ForeColor = System.Drawing.Color.Navy
        Me.btnMigrateTransactions.Location = New System.Drawing.Point(600, 593)
        Me.btnMigrateTransactions.Margin = New System.Windows.Forms.Padding(4)
        Me.btnMigrateTransactions.Name = "btnMigrateTransactions"
        Me.btnMigrateTransactions.Size = New System.Drawing.Size(293, 39)
        Me.btnMigrateTransactions.TabIndex = 17
        Me.btnMigrateTransactions.Text = "M&igrate Transactions"
        Me.btnMigrateTransactions.UseVisualStyleBackColor = True
        '
        'txtBankAccNo
        '
        Me.txtBankAccNo.Location = New System.Drawing.Point(195, 666)
        Me.txtBankAccNo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBankAccNo.Name = "txtBankAccNo"
        Me.txtBankAccNo.Size = New System.Drawing.Size(145, 22)
        Me.txtBankAccNo.TabIndex = 13
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(3, 672)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(153, 17)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "Bank Account Number:"
        '
        'Btn_MakeTender
        '
        Me.Btn_MakeTender.ForeColor = System.Drawing.Color.Navy
        Me.Btn_MakeTender.Location = New System.Drawing.Point(600, 503)
        Me.Btn_MakeTender.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_MakeTender.Name = "Btn_MakeTender"
        Me.Btn_MakeTender.Size = New System.Drawing.Size(293, 39)
        Me.Btn_MakeTender.TabIndex = 34
        Me.Btn_MakeTender.Text = "&Make a Tender"
        Me.Btn_MakeTender.UseVisualStyleBackColor = True
        '
        'TB_GLAcc
        '
        Me.TB_GLAcc.Location = New System.Drawing.Point(196, 638)
        Me.TB_GLAcc.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_GLAcc.Name = "TB_GLAcc"
        Me.TB_GLAcc.Size = New System.Drawing.Size(356, 22)
        Me.TB_GLAcc.TabIndex = 35
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(4, 641)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(92, 17)
        Me.Label17.TabIndex = 36
        Me.Label17.Text = "Output Code:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(350, 674)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(130, 17)
        Me.Label18.TabIndex = 37
        Me.Label18.Text = "Bank Branch Code:"
        '
        'txtBankBranchCode
        '
        Me.txtBankBranchCode.Location = New System.Drawing.Point(491, 669)
        Me.txtBankBranchCode.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBankBranchCode.Name = "txtBankBranchCode"
        Me.txtBankBranchCode.Size = New System.Drawing.Size(60, 22)
        Me.txtBankBranchCode.TabIndex = 38
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(4, 701)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(127, 17)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "Bank Layout Type:"
        '
        'CB_BankLayout
        '
        Me.CB_BankLayout.FormattingEnabled = True
        Me.CB_BankLayout.Items.AddRange(New Object() {"ACB", "SBO"})
        Me.CB_BankLayout.Location = New System.Drawing.Point(196, 701)
        Me.CB_BankLayout.Name = "CB_BankLayout"
        Me.CB_BankLayout.Size = New System.Drawing.Size(121, 24)
        Me.CB_BankLayout.TabIndex = 41
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.Controls.Add(Me.ListBox1)
        Me.Panel1.Controls.Add(Me.CB_BankLayout)
        Me.Panel1.Controls.Add(Me.ListBox2)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.ListBox3)
        Me.Panel1.Controls.Add(Me.txtBankBranchCode)
        Me.Panel1.Controls.Add(Me.btnNew)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.TB_GLAcc)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txtAccount)
        Me.Panel1.Controls.Add(Me.Btn_MakeTender)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtBankAccNo)
        Me.Panel1.Controls.Add(Me.txtAccountDescription)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.btnMigrateTransactions)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.txtReportingCategory)
        Me.Panel1.Controls.Add(Me.lblSegment4CountInTransactions)
        Me.Panel1.Controls.Add(Me.cmbFinancialCategory)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.DataGridView1)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txtDisplayName)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.txtType)
        Me.Panel1.Controls.Add(Me.rdbBalanceSheet)
        Me.Panel1.Controls.Add(Me.btnUpdateChanges)
        Me.Panel1.Controls.Add(Me.rdbIncomeStatement)
        Me.Panel1.Controls.Add(Me.btnMakeAPayout)
        Me.Panel1.Controls.Add(Me.btnDeleteAccount)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1348, 796)
        Me.Panel1.TabIndex = 42
        '
        'frmAccountMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1348, 796)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAccountMaintenance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Account&Maint"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCode1 As System.Windows.Forms.TextBox
    Friend WithEvents txtDesc1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDesc3 As System.Windows.Forms.TextBox
    Friend WithEvents txtCode3 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDesc2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCode2 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtAccount As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtAccountDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtReportingCategory As System.Windows.Forms.TextBox
    Friend WithEvents cmbFinancialCategory As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtDisplayName As System.Windows.Forms.TextBox
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateChanges As System.Windows.Forms.Button
    Friend WithEvents btnMakeAPayout As System.Windows.Forms.Button
    Friend WithEvents btnDeleteAccount As System.Windows.Forms.Button
    Friend WithEvents rdbIncomeStatement As System.Windows.Forms.RadioButton
    Friend WithEvents rdbBalanceSheet As System.Windows.Forms.RadioButton
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblSegment4CountInTransactions As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnMigrateTransactions As System.Windows.Forms.Button
    Friend WithEvents ckbIsNew As System.Windows.Forms.CheckBox
    Friend WithEvents txtBankAccNo As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Btn_MakeTender As System.Windows.Forms.Button
    Friend WithEvents TB_GLAcc As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtBankBranchCode As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents CB_BankLayout As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
