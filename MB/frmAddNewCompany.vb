﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data

Public Class frmAddNewCompany
#Region "Variables"

#End Region
#Region "Methods"
    Sub CreateCompany()

        Dim cn1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            Dim sSQL As String

            Dim oCompanyID As Integer = Nothing
            If My.Settings.Setting_CompanyID <> "" Or My.Settings.Setting_CompanyID <> Nothing Then
                oCompanyID = CInt(My.Settings.Setting_CompanyID)
            End If

            If My.Settings.DBType = "Access" Then

                sSQL = "Insert Into [Company Details] ([CompanyName], [LegalEntityName], [CoReg], [CoVat], [Address1], [Address2], [Address3], "
                sSQL = sSQL & "[Address4], [TelephoneLine1], [TelephoneLine2], [EmailAdress1], [EmailAdress2], [Directors]"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", [Co_ID]"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & " Values ("
                '##### NEW #####
                sSQL = sSQL & "'" & SQLConvert(txtCompanyName.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtLegalName.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtRegNo.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtVat.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtPAdd1.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtPAdd2.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtPAdd3.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtPAdd4.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtTel1.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtTel2.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtEmailAdd1.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtEmailAdd1.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtDirectorNames.Text) & "' "
                If oCompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID
                End If
                sSQL = sSQL & ") "

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Insert Into [Company Details] ([CompanyName], [LegalEntityName], [CoReg], [CoVat], [Address1], [Address2], [Address3], "
                sSQL = sSQL & "[Address4], [TelephoneLine1], [TelephoneLine2], [EmailAdress1], [EmailAdress2], [Directors]"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", [Co_ID]"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & " Values ("
                '##### NEW #####
                sSQL = sSQL & "'" & SQLConvert(txtCompanyName.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtLegalName.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtRegNo.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtVat.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtPAdd1.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtPAdd2.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtPAdd3.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtPAdd4.Text) & "',"
                sSQL = sSQL & "'" & SQLConvert(txtTel1.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtTel2.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtEmailAdd1.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtEmailAdd1.Text) & "', "
                sSQL = sSQL & "'" & SQLConvert(txtDirectorNames.Text) & "' "
                If oCompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID
                End If
                sSQL = sSQL & ") "


                Dim cmd As New SqlCommand(sSQL, cn1)
                cmd.CommandTimeout = 300
                Using cn1
                    cn1.Open()
                    cmd.ExecuteNonQuery()
                    cn1.Close()
                End Using
            End If

            MsgBox("Company successfully created!")
            ClearForm()

        Catch ex As Exception
            If cn1.State = ConnectionState.Open Then
                cn1.Dispose()
            End If
            MsgBox("There was an error editing supplier information! " & Err.Description)
            'Me.Edit_ID_Label.Text = "None"
        End Try

    End Sub
    Private Sub ClearForm()

        txtCompanyName.Text = ""
        txtLegalName.Text = ""
        txtRegNo.Text = ""
        txtVat.Text = ""
        txtPAdd1.Text = ""
        txtPAdd2.Text = ""
        txtPAdd3.Text = ""
        txtPAdd4.Text = ""
        txtTel1.Text = ""
        txtTel2.Text = ""
        txtEmailAdd1.Text = ""
        txtEmailAdd2.Text = ""
        txtDirectorNames.Text = ""
        txtCompanyName.Focus()

    End Sub
#End Region
    Private Sub frmAddNewCompany_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> New Company -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtCompanyName.Text.Trim.Length = 0 Then
            MessageBox.Show("Please supply Company name!", "Company name", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtCompanyName.Focus()
            Exit Sub
        End If

        If txtLegalName.Text.Trim.Length = 0 Then
            MessageBox.Show("Please supply Company legal name!", "Company legal name", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtLegalName.Focus()
            Exit Sub
        End If

        If txtTel1.Text.Trim.Length = 0 Then
            MessageBox.Show("Please supply company primary telephone number!", "Company tel. no.", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtTel1.Focus()
            Exit Sub
        End If

        If txtEmailAdd1.Text.Trim.Length = 0 Then
            MessageBox.Show("Please supply Company primary email adress", "Company email address", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtEmailAdd1.Focus()
            Exit Sub
        End If

        CreateCompany()

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        ClearForm()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class