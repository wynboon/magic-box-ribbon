﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransactions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransactions))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label_Version = New System.Windows.Forms.Label()
        Me.DateTimePicker_To = New System.Windows.Forms.DateTimePicker()
        Me.cmbSupplier = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DateTimePicker_From = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.chkAlter = New System.Windows.Forms.CheckBox()
        Me.btnSaveChanges = New System.Windows.Forms.Button()
        Me.lblUserName = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblLoggedIn = New System.Windows.Forms.Label()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUserName = New System.Windows.Forms.TextBox()
        Me.btnEditSelectedTransaction = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Reference = New System.Windows.Forms.ComboBox()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(11, 69)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(2)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(890, 419)
        Me.DataGridView1.TabIndex = 0
        '
        'Label_Version
        '
        Me.Label_Version.AutoSize = True
        Me.Label_Version.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Version.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label_Version.Location = New System.Drawing.Point(825, 7)
        Me.Label_Version.Name = "Label_Version"
        Me.Label_Version.Size = New System.Drawing.Size(41, 13)
        Me.Label_Version.TabIndex = 84
        Me.Label_Version.Text = "version"
        '
        'DateTimePicker_To
        '
        Me.DateTimePicker_To.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_To.Location = New System.Drawing.Point(387, 43)
        Me.DateTimePicker_To.Name = "DateTimePicker_To"
        Me.DateTimePicker_To.Size = New System.Drawing.Size(165, 20)
        Me.DateTimePicker_To.TabIndex = 86
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.FormattingEnabled = True
        Me.cmbSupplier.Location = New System.Drawing.Point(11, 43)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(191, 21)
        Me.cmbSupplier.TabIndex = 87
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(8, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Select Supplier"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(384, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 92
        Me.Label5.Text = "To Date"
        '
        'DateTimePicker_From
        '
        Me.DateTimePicker_From.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_From.Location = New System.Drawing.Point(208, 44)
        Me.DateTimePicker_From.Name = "DateTimePicker_From"
        Me.DateTimePicker_From.Size = New System.Drawing.Size(173, 20)
        Me.DateTimePicker_From.TabIndex = 85
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(205, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 91
        Me.Label3.Text = "From Date"
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(558, 41)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(174, 23)
        Me.btnRun.TabIndex = 95
        Me.btnRun.Text = "&Load Transactions"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'chkAlter
        '
        Me.chkAlter.AutoSize = True
        Me.chkAlter.ForeColor = System.Drawing.Color.DarkGray
        Me.chkAlter.Location = New System.Drawing.Point(31, 522)
        Me.chkAlter.Name = "chkAlter"
        Me.chkAlter.Size = New System.Drawing.Size(47, 17)
        Me.chkAlter.TabIndex = 96
        Me.chkAlter.Text = "Alter"
        Me.chkAlter.UseVisualStyleBackColor = True
        Me.chkAlter.Visible = False
        '
        'btnSaveChanges
        '
        Me.btnSaveChanges.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSaveChanges.Location = New System.Drawing.Point(565, 8)
        Me.btnSaveChanges.Name = "btnSaveChanges"
        Me.btnSaveChanges.Size = New System.Drawing.Size(100, 23)
        Me.btnSaveChanges.TabIndex = 97
        Me.btnSaveChanges.Text = "Save Changes"
        Me.btnSaveChanges.UseVisualStyleBackColor = True
        Me.btnSaveChanges.Visible = False
        '
        'lblUserName
        '
        Me.lblUserName.AutoSize = True
        Me.lblUserName.ForeColor = System.Drawing.Color.Maroon
        Me.lblUserName.Location = New System.Drawing.Point(3, 11)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(60, 13)
        Me.lblUserName.TabIndex = 98
        Me.lblUserName.Text = "User Name"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblLoggedIn)
        Me.Panel1.Controls.Add(Me.btnLogin)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtUserName)
        Me.Panel1.Controls.Add(Me.lblUserName)
        Me.Panel1.Controls.Add(Me.btnSaveChanges)
        Me.Panel1.Location = New System.Drawing.Point(12, 493)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(689, 37)
        Me.Panel1.TabIndex = 99
        Me.Panel1.Visible = False
        '
        'lblLoggedIn
        '
        Me.lblLoggedIn.AutoSize = True
        Me.lblLoggedIn.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLoggedIn.Location = New System.Drawing.Point(494, 13)
        Me.lblLoggedIn.Name = "lblLoggedIn"
        Me.lblLoggedIn.Size = New System.Drawing.Size(55, 13)
        Me.lblLoggedIn.TabIndex = 103
        Me.lblLoggedIn.Text = "Logged In"
        Me.lblLoggedIn.Visible = False
        '
        'btnLogin
        '
        Me.btnLogin.ForeColor = System.Drawing.Color.Maroon
        Me.btnLogin.Location = New System.Drawing.Point(359, 6)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(100, 23)
        Me.btnLogin.TabIndex = 102
        Me.btnLogin.Text = "Log in"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(244, 8)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(100, 20)
        Me.txtPassword.TabIndex = 101
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Maroon
        Me.Label2.Location = New System.Drawing.Point(186, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 100
        Me.Label2.Text = "Password"
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(69, 8)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(100, 20)
        Me.txtUserName.TabIndex = 99
        '
        'btnEditSelectedTransaction
        '
        Me.btnEditSelectedTransaction.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnEditSelectedTransaction.Location = New System.Drawing.Point(738, 41)
        Me.btnEditSelectedTransaction.Name = "btnEditSelectedTransaction"
        Me.btnEditSelectedTransaction.Size = New System.Drawing.Size(163, 23)
        Me.btnEditSelectedTransaction.TabIndex = 100
        Me.btnEditSelectedTransaction.Text = "&Edit Selected Transaction"
        Me.btnEditSelectedTransaction.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Navy
        Me.Label18.Location = New System.Drawing.Point(482, 22)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(57, 13)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "Reference"
        Me.Label18.Visible = False
        '
        'Reference
        '
        Me.Reference.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Reference.FormattingEnabled = True
        Me.Reference.Location = New System.Drawing.Point(469, 43)
        Me.Reference.Margin = New System.Windows.Forms.Padding(4)
        Me.Reference.Name = "Reference"
        Me.Reference.Size = New System.Drawing.Size(126, 21)
        Me.Reference.TabIndex = 94
        Me.Reference.Visible = False
        '
        'btnClose
        '
        Me.btnClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnClose.Location = New System.Drawing.Point(786, 496)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(115, 23)
        Me.btnClose.TabIndex = 101
        Me.btnClose.Text = "&Exit"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmTransactions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(912, 527)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnEditSelectedTransaction)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.chkAlter)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.Reference)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.DateTimePicker_To)
        Me.Controls.Add(Me.cmbSupplier)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.DateTimePicker_From)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label_Version)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(928, 565)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(928, 565)
        Me.Name = "frmTransactions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Transactions"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label_Version As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents chkAlter As System.Windows.Forms.CheckBox
    Friend WithEvents btnSaveChanges As System.Windows.Forms.Button
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents lblLoggedIn As System.Windows.Forms.Label
    Friend WithEvents btnEditSelectedTransaction As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Reference As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
End Class
