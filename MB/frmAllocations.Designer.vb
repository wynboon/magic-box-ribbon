﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAllocations
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAllocations))
        Me.cmbStatus1 = New System.Windows.Forms.ComboBox()
        Me.DateTo1 = New System.Windows.Forms.DateTimePicker()
        Me.lblStatus1 = New System.Windows.Forms.Label()
        Me.cmbSupplier1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DateFrom1 = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtUpdateOriginalRef = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DateTo2 = New System.Windows.Forms.DateTimePicker()
        Me.cmbStatus2 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DateFrom2 = New System.Windows.Forms.DateTimePicker()
        Me.cmbSupplier2 = New System.Windows.Forms.ComboBox()
        Me.lblStatus2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Index1 = New System.Windows.Forms.TextBox()
        Me.A0 = New System.Windows.Forms.TextBox()
        Me.A1 = New System.Windows.Forms.TextBox()
        Me.A2 = New System.Windows.Forms.TextBox()
        Me.A3 = New System.Windows.Forms.TextBox()
        Me.A4 = New System.Windows.Forms.TextBox()
        Me.A5 = New System.Windows.Forms.TextBox()
        Me.A6 = New System.Windows.Forms.TextBox()
        Me.B6 = New System.Windows.Forms.TextBox()
        Me.B5 = New System.Windows.Forms.TextBox()
        Me.B4 = New System.Windows.Forms.TextBox()
        Me.B3 = New System.Windows.Forms.TextBox()
        Me.B2 = New System.Windows.Forms.TextBox()
        Me.B1 = New System.Windows.Forms.TextBox()
        Me.B0 = New System.Windows.Forms.TextBox()
        Me.Index2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbAllocate = New System.Windows.Forms.ComboBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbStatus1
        '
        Me.cmbStatus1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbStatus1.FormattingEnabled = True
        Me.cmbStatus1.Items.AddRange(New Object() {"[ALL]", "UNPAID", "PAID"})
        Me.cmbStatus1.Location = New System.Drawing.Point(107, 150)
        Me.cmbStatus1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbStatus1.Name = "cmbStatus1"
        Me.cmbStatus1.Size = New System.Drawing.Size(113, 24)
        Me.cmbStatus1.TabIndex = 3
        Me.cmbStatus1.Text = "UNPAID"
        '
        'DateTo1
        '
        Me.DateTo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTo1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTo1.Location = New System.Drawing.Point(165, 107)
        Me.DateTo1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTo1.Name = "DateTo1"
        Me.DateTo1.Size = New System.Drawing.Size(120, 22)
        Me.DateTo1.TabIndex = 2
        '
        'lblStatus1
        '
        Me.lblStatus1.AutoSize = True
        Me.lblStatus1.BackColor = System.Drawing.Color.Transparent
        Me.lblStatus1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus1.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblStatus1.Location = New System.Drawing.Point(15, 154)
        Me.lblStatus1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStatus1.Name = "lblStatus1"
        Me.lblStatus1.Size = New System.Drawing.Size(41, 15)
        Me.lblStatus1.TabIndex = 74
        Me.lblStatus1.Text = "Status"
        '
        'cmbSupplier1
        '
        Me.cmbSupplier1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier1.FormattingEnabled = True
        Me.cmbSupplier1.Location = New System.Drawing.Point(19, 44)
        Me.cmbSupplier1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbSupplier1.Name = "cmbSupplier1"
        Me.cmbSupplier1.Size = New System.Drawing.Size(267, 24)
        Me.cmbSupplier1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(19, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 15)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Supplier"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(173, 86)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 15)
        Me.Label5.TabIndex = 78
        Me.Label5.Text = "To Date"
        '
        'DateFrom1
        '
        Me.DateFrom1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateFrom1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateFrom1.Location = New System.Drawing.Point(19, 107)
        Me.DateFrom1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateFrom1.Name = "DateFrom1"
        Me.DateFrom1.Size = New System.Drawing.Size(137, 22)
        Me.DateFrom1.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(15, 86)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 15)
        Me.Label3.TabIndex = 77
        Me.Label3.Text = "From Date"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowDrop = True
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(307, 25)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(691, 191)
        Me.DataGridView1.TabIndex = 5
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtUpdateOriginalRef)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.DateTo1)
        Me.GroupBox1.Controls.Add(Me.cmbStatus1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Controls.Add(Me.DateFrom1)
        Me.GroupBox1.Controls.Add(Me.cmbSupplier1)
        Me.GroupBox1.Controls.Add(Me.lblStatus1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(20, 90)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1017, 223)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtUpdateOriginalRef
        '
        Me.txtUpdateOriginalRef.Location = New System.Drawing.Point(167, 188)
        Me.txtUpdateOriginalRef.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtUpdateOriginalRef.Name = "txtUpdateOriginalRef"
        Me.txtUpdateOriginalRef.Size = New System.Drawing.Size(119, 22)
        Me.txtUpdateOriginalRef.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Maroon
        Me.Label6.Location = New System.Drawing.Point(8, 192)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(115, 15)
        Me.Label6.TabIndex = 79
        Me.Label6.Text = "Update Original Ref"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DataGridView2)
        Me.GroupBox2.Controls.Add(Me.DateTo2)
        Me.GroupBox2.Controls.Add(Me.cmbStatus2)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.DateFrom2)
        Me.GroupBox2.Controls.Add(Me.cmbSupplier2)
        Me.GroupBox2.Controls.Add(Me.lblStatus2)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(20, 320)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(1017, 215)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowDrop = True
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(307, 20)
        Me.DataGridView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowTemplate.Height = 24
        Me.DataGridView2.Size = New System.Drawing.Size(691, 188)
        Me.DataGridView2.TabIndex = 4
        '
        'DateTo2
        '
        Me.DateTo2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTo2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTo2.Location = New System.Drawing.Point(165, 108)
        Me.DateTo2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTo2.Name = "DateTo2"
        Me.DateTo2.Size = New System.Drawing.Size(120, 22)
        Me.DateTo2.TabIndex = 2
        '
        'cmbStatus2
        '
        Me.cmbStatus2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbStatus2.FormattingEnabled = True
        Me.cmbStatus2.Items.AddRange(New Object() {"[ALL]", "UNPAID", "PAID"})
        Me.cmbStatus2.Location = New System.Drawing.Point(19, 170)
        Me.cmbStatus2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbStatus2.Name = "cmbStatus2"
        Me.cmbStatus2.Size = New System.Drawing.Size(113, 24)
        Me.cmbStatus2.TabIndex = 3
        Me.cmbStatus2.Text = "UNPAID"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(163, 87)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 15)
        Me.Label4.TabIndex = 78
        Me.Label4.Text = "To Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label2.Location = New System.Drawing.Point(8, 20)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 15)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Supplier"
        '
        'DateFrom2
        '
        Me.DateFrom2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateFrom2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateFrom2.Location = New System.Drawing.Point(8, 108)
        Me.DateFrom2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateFrom2.Name = "DateFrom2"
        Me.DateFrom2.Size = New System.Drawing.Size(148, 22)
        Me.DateFrom2.TabIndex = 1
        '
        'cmbSupplier2
        '
        Me.cmbSupplier2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier2.FormattingEnabled = True
        Me.cmbSupplier2.Location = New System.Drawing.Point(8, 39)
        Me.cmbSupplier2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbSupplier2.Name = "cmbSupplier2"
        Me.cmbSupplier2.Size = New System.Drawing.Size(277, 24)
        Me.cmbSupplier2.TabIndex = 0
        '
        'lblStatus2
        '
        Me.lblStatus2.AutoSize = True
        Me.lblStatus2.BackColor = System.Drawing.Color.Transparent
        Me.lblStatus2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus2.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblStatus2.Location = New System.Drawing.Point(8, 150)
        Me.lblStatus2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStatus2.Name = "lblStatus2"
        Me.lblStatus2.Size = New System.Drawing.Size(41, 15)
        Me.lblStatus2.TabIndex = 74
        Me.lblStatus2.Text = "Status"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label8.Location = New System.Drawing.Point(4, 87)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 15)
        Me.Label8.TabIndex = 77
        Me.Label8.Text = "From Date"
        '
        'Index1
        '
        Me.Index1.Location = New System.Drawing.Point(1129, 14)
        Me.Index1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Index1.Name = "Index1"
        Me.Index1.Size = New System.Drawing.Size(48, 22)
        Me.Index1.TabIndex = 83
        Me.Index1.Visible = False
        '
        'A0
        '
        Me.A0.Location = New System.Drawing.Point(1045, 46)
        Me.A0.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A0.Name = "A0"
        Me.A0.Size = New System.Drawing.Size(132, 22)
        Me.A0.TabIndex = 84
        Me.A0.Visible = False
        '
        'A1
        '
        Me.A1.Location = New System.Drawing.Point(1045, 80)
        Me.A1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A1.Name = "A1"
        Me.A1.Size = New System.Drawing.Size(132, 22)
        Me.A1.TabIndex = 85
        Me.A1.Visible = False
        '
        'A2
        '
        Me.A2.Location = New System.Drawing.Point(1045, 112)
        Me.A2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A2.Name = "A2"
        Me.A2.Size = New System.Drawing.Size(132, 22)
        Me.A2.TabIndex = 86
        Me.A2.Visible = False
        '
        'A3
        '
        Me.A3.Location = New System.Drawing.Point(1045, 144)
        Me.A3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A3.Name = "A3"
        Me.A3.Size = New System.Drawing.Size(132, 22)
        Me.A3.TabIndex = 87
        Me.A3.Visible = False
        '
        'A4
        '
        Me.A4.Location = New System.Drawing.Point(1045, 176)
        Me.A4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A4.Name = "A4"
        Me.A4.Size = New System.Drawing.Size(132, 22)
        Me.A4.TabIndex = 88
        Me.A4.Visible = False
        '
        'A5
        '
        Me.A5.Location = New System.Drawing.Point(1045, 210)
        Me.A5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A5.Name = "A5"
        Me.A5.Size = New System.Drawing.Size(132, 22)
        Me.A5.TabIndex = 89
        Me.A5.Visible = False
        '
        'A6
        '
        Me.A6.Location = New System.Drawing.Point(1045, 244)
        Me.A6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.A6.Name = "A6"
        Me.A6.Size = New System.Drawing.Size(132, 22)
        Me.A6.TabIndex = 90
        Me.A6.Visible = False
        '
        'B6
        '
        Me.B6.Location = New System.Drawing.Point(1045, 518)
        Me.B6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B6.Name = "B6"
        Me.B6.Size = New System.Drawing.Size(132, 22)
        Me.B6.TabIndex = 98
        Me.B6.Visible = False
        '
        'B5
        '
        Me.B5.Location = New System.Drawing.Point(1045, 485)
        Me.B5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B5.Name = "B5"
        Me.B5.Size = New System.Drawing.Size(132, 22)
        Me.B5.TabIndex = 97
        Me.B5.Visible = False
        '
        'B4
        '
        Me.B4.Location = New System.Drawing.Point(1045, 450)
        Me.B4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B4.Name = "B4"
        Me.B4.Size = New System.Drawing.Size(132, 22)
        Me.B4.TabIndex = 96
        Me.B4.Visible = False
        '
        'B3
        '
        Me.B3.Location = New System.Drawing.Point(1045, 418)
        Me.B3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B3.Name = "B3"
        Me.B3.Size = New System.Drawing.Size(132, 22)
        Me.B3.TabIndex = 95
        Me.B3.Visible = False
        '
        'B2
        '
        Me.B2.Location = New System.Drawing.Point(1045, 386)
        Me.B2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B2.Name = "B2"
        Me.B2.Size = New System.Drawing.Size(132, 22)
        Me.B2.TabIndex = 94
        Me.B2.Visible = False
        '
        'B1
        '
        Me.B1.Location = New System.Drawing.Point(1045, 354)
        Me.B1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B1.Name = "B1"
        Me.B1.Size = New System.Drawing.Size(132, 22)
        Me.B1.TabIndex = 93
        Me.B1.Visible = False
        '
        'B0
        '
        Me.B0.Location = New System.Drawing.Point(1045, 320)
        Me.B0.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.B0.Name = "B0"
        Me.B0.Size = New System.Drawing.Size(132, 22)
        Me.B0.TabIndex = 92
        Me.B0.Visible = False
        '
        'Index2
        '
        Me.Index2.Location = New System.Drawing.Point(1129, 288)
        Me.Index2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Index2.Name = "Index2"
        Me.Index2.Size = New System.Drawing.Size(48, 22)
        Me.Index2.TabIndex = 91
        Me.Index2.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label9.Location = New System.Drawing.Point(1045, 17)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 15)
        Me.Label9.TabIndex = 80
        Me.Label9.Text = "Row Index"
        Me.Label9.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(1045, 290)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 15)
        Me.Label10.TabIndex = 99
        Me.Label10.Text = "Row Index"
        Me.Label10.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.cmbAllocate)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 5)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(1017, 65)
        Me.GroupBox3.TabIndex = 100
        Me.GroupBox3.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label11.Location = New System.Drawing.Point(29, 30)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 17)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Allocate"
        '
        'cmbAllocate
        '
        Me.cmbAllocate.FormattingEnabled = True
        Me.cmbAllocate.Items.AddRange(New Object() {"Supplier Debits to Invoices"})
        Me.cmbAllocate.Location = New System.Drawing.Point(137, 26)
        Me.cmbAllocate.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbAllocate.Name = "cmbAllocate"
        Me.cmbAllocate.Size = New System.Drawing.Size(475, 24)
        Me.cmbAllocate.TabIndex = 72
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.BackColor = System.Drawing.Color.Transparent
        Me.lblDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.6!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblDescription.Location = New System.Drawing.Point(323, 539)
        Me.lblDescription.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(11, 16)
        Me.lblDescription.TabIndex = 102
        Me.lblDescription.Text = "."
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(8, 79)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 28)
        Me.Button1.TabIndex = 103
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'frmAllocations
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1336, 566)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.B6)
        Me.Controls.Add(Me.B5)
        Me.Controls.Add(Me.B4)
        Me.Controls.Add(Me.B3)
        Me.Controls.Add(Me.B2)
        Me.Controls.Add(Me.B1)
        Me.Controls.Add(Me.B0)
        Me.Controls.Add(Me.Index2)
        Me.Controls.Add(Me.A6)
        Me.Controls.Add(Me.A5)
        Me.Controls.Add(Me.A4)
        Me.Controls.Add(Me.A3)
        Me.Controls.Add(Me.A2)
        Me.Controls.Add(Me.A1)
        Me.Controls.Add(Me.A0)
        Me.Controls.Add(Me.Index1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MinimumSize = New System.Drawing.Size(1085, 595)
        Me.Name = "frmAllocations"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Allocations"
        Me.Text = "Allocations"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbStatus1 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTo1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStatus1 As System.Windows.Forms.Label
    Friend WithEvents cmbSupplier1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DateFrom1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTo2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbStatus2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateFrom2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbSupplier2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Index1 As System.Windows.Forms.TextBox
    Friend WithEvents A0 As System.Windows.Forms.TextBox
    Friend WithEvents A1 As System.Windows.Forms.TextBox
    Friend WithEvents A2 As System.Windows.Forms.TextBox
    Friend WithEvents A3 As System.Windows.Forms.TextBox
    Friend WithEvents A4 As System.Windows.Forms.TextBox
    Friend WithEvents A5 As System.Windows.Forms.TextBox
    Friend WithEvents A6 As System.Windows.Forms.TextBox
    Friend WithEvents B6 As System.Windows.Forms.TextBox
    Friend WithEvents B5 As System.Windows.Forms.TextBox
    Friend WithEvents B4 As System.Windows.Forms.TextBox
    Friend WithEvents B3 As System.Windows.Forms.TextBox
    Friend WithEvents B2 As System.Windows.Forms.TextBox
    Friend WithEvents B1 As System.Windows.Forms.TextBox
    Friend WithEvents B0 As System.Windows.Forms.TextBox
    Friend WithEvents Index2 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbAllocate As System.Windows.Forms.ComboBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtUpdateOriginalRef As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
