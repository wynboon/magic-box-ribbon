﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing

Public Class frmCashPayRemoveV2

    Private Sub frmCashPayRemove_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Payment Remover -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            Me.Cursor = Cursors.AppStarting
            Me.Enabled = False
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True

        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub LoadCashPayments()

        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select TransactionID,[Transaction Date] as Date,Reference,Suppliers.SupplierName, Amount " & _
            "From Transactions Inner Join Suppliers on Suppliers.SupplierID = Transactions.SupplierID " & _
            "Where [Transaction date] >= '" & DTP_FromDate.Text & "' and [Transaction date] <= '" & DTP_ToDate.Text & "' " & _
            "and Transactions.Co_ID = " & My.Settings.Setting_CompanyID & _
            "and [Transaction Type] = 1 and [Description Code] = 'HAAAAA'")

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "CashPay")
            connection.Close()
            DGV_Payments.DataSource = ds
            DGV_Payments.DataMember = "CashPay"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            DGV_Payments.Columns("Amount").DefaultCellStyle.Format = "c"
            DGV_Payments.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            DGV_Payments.Columns("Reference").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'DGV_Payments.Columns("Show").Visible = False
            DGV_Payments.Columns("TransactionID").Visible = False


            'Get the cashup days, extract ds, loop through and if there is a day with it, disable

            Dim dsCashupDays As New DataTable
            dsCashupDays = CashUpDays(DTP_FromDate.Text, DTP_ToDate.Text)

            For Each dsRow As DataRow In dsCashupDays.Rows
                For Each row As DataGridViewRow In DGV_Payments.Rows
                    If DGV_Payments.Rows(row.Index).Cells("Date").ToString("dd MMM yyyy") = row.Cells("Transaction Date").Value.ToString("dd MMM yyyy") Then
                        DGV_Payments.Rows(row.Index).DefaultCellStyle.ForeColor = Color.Red
                        DGV_Payments.Rows(row.Index).ReadOnly = True
                    Else
                        DGV_Payments.Rows(row.Index).DefaultCellStyle.ForeColor = Color.Green
                        DGV_Payments.Rows(row.Index).ReadOnly = False
                    End If
                Next
            Next

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Sub DeleteInvoicePaymentsCreditsDebits(ByVal LinkID As String)

        Me.Enabled = False
        Me.Cursor = Cursors.AppStarting

        Dim sSQL As String

        Try
            If LinkID = "" Then
                MsgBox("There was an issue with the LinkID not being valid, please contact support", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            If LinkID = "0" Then
                MsgBox("There was an issue with the LinkID not being valid, please contact support", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True, Info3 = 'Voided on " & Now.ToShortDateString & "' WHERE TransactionID = " & LinkID
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET Void = 1, Info3 = 'Voided on " & Now.ToShortDateString & "'  WHERE TransactionID = " & LinkID & " and Co_ID = " & My.Settings.Setting_CompanyID & vbNewLine & _
                    "Delete from PaymentsRequests WHERE [InvoiceTransactionID] = " & LinkID & " and Co_ID = " & My.Settings.Setting_CompanyID
                '
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If


            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            End If

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("There was a problem deleting payments relating to an invoice " & ex.Message & " 029")
        End Try

    End Sub


    Private Sub btn_Void_Click(sender As Object, e As EventArgs) Handles btn_Void.Click
        Dim MoveCount As Integer
        For Each row As DataGridViewRow In DGV_Payments.Rows

            If CBool(row.Cells("Void").Value) = True Then
                'row.Cells(12).Value = GLAccount
                DeleteInvoicePaymentsCreditsDebits(row.Cells("TransactionID").Value)
                MoveCount += 1
            End If
        Next
        MsgBox("Payments(" & MoveCount & ") have been voided")
        Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Close()
    End Sub

    Private Sub Btn_Search_Click(sender As Object, e As EventArgs) Handles Btn_Search.Click
        Call LoadCashPayments()
    End Sub

    Private Sub DGV_Payments_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Payments.CellContentClick

    End Sub
End Class