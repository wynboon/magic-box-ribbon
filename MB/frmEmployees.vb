﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Windows.Forms


Public Class frmEmployees

    Dim oAccessAdapter As OleDbDataAdapter
    Dim oAccessTable As New DataTable
    Dim oSQLAdapter As SqlDataAdapter
    Dim oSQLTable As New DataTable
    Dim EmployeeID As Integer
    Dim ShowInactive As Boolean

    Private Sub Employees_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Employees -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
        Me.dgvEmployees.CellBorderStyle = Windows.Forms.DataGridViewCellBorderStyle.None
        Fill_Employees_DGV()

    End Sub
    Public Sub Fill_Employees_DGV_After_AddingNew()
        Try

            Me.dgvEmployees.DataSource = Nothing
            Dim sSQL As String

            sSQL = "Select * From Employees"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
                If ShowInactive = False Then
                    sSQL = sSQL & " and [Active] = 'ACTIVE' "
                End If
                sSQL = sSQL & " order by EmployeeID DESC "
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            oSQLTable.Clear() 'MUST CLEAR DATA TABLE
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            oSQLAdapter = New SqlDataAdapter(sSQL, connection)
            oSQLAdapter.Fill(oSQLTable)
            Me.dgvEmployees.DataSource = oSQLTable
            Me.dgvEmployees.Refresh()
            'Me.DataGridView1.ColumnHeadersDefaultCellStyle.Font = System.Drawing.F
            'Me.DataGridView1.Columns(0).HeaderCell.

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception

            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub Fill_Employees_DGV()
        Try

            Me.dgvEmployees.DataSource = Nothing

            Dim sSQL As String

            sSQL = "Select * From Employees "
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & "WHERE Co_ID = " & My.Settings.Setting_CompanyID
                If ShowInactive = False Then
                    sSQL = sSQL & " and [Active] = 'ACTIVE' "
                End If
            End If

            If My.Settings.DBType = "Access" Then
                oAccessTable.Clear()
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                oAccessAdapter = New OleDbDataAdapter(sSQL, connection)
                oAccessAdapter.Fill(oAccessTable)
                Me.dgvEmployees.DataSource = oAccessTable
            ElseIf My.Settings.DBType = "SQL" Then
                oSQLTable.Clear() 'MUST CLEAR DATA TABLE
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                oSQLAdapter = New SqlDataAdapter(sSQL, connection)
                oSQLAdapter.Fill(oSQLTable)
                Me.dgvEmployees.DataSource = oSQLTable
            End If
            Me.dgvEmployees.Refresh()
            'Me.DataGridView1.ColumnHeadersDefaultCellStyle.Font = System.Drawing.F
            'Me.DataGridView1.Columns(0).HeaderCell.
        Catch ex As Exception
            MsgBox(ex.Message & " 087")
        End Try
    End Sub
    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles btnEditEmp.Click
        Try

            If Me.dgvEmployees.SelectedCells.Count < 1 Then
                MessageBox.Show("Please select an Employee record in order to continue with editing", "Employee editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            Dim EmployeeID As Integer = CInt(dgvEmployees.Rows(dgvEmployees.CurrentRow.Index).Cells(0).Value)

            Dim frmEditEmployee As New frmEmployee_New
            If EmployeeID <> Nothing Then
                frmEditEmployee.isEditEmployee = True
                frmEditEmployee.EditEmployeeID = EmployeeID
                frmEditEmployee.ShowDialog()
            End If

            Me.dgvEmployees.CellBorderStyle = Windows.Forms.DataGridViewCellBorderStyle.None

            If frmEditEmployee.Saved Then
                Me.Enabled = False
                Me.Cursor = Cursors.AppStarting
                Call Fill_Employees_DGV()
                Me.dgvEmployees.Refresh()
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
            End If

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured :" & ex.Message, "Employees error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        'Call Update_Employee()
        'Me.DataGridView1.DataSource = Nothing
        'Me.DataGridView1.Rows.Clear()

        'Me.DataGridView1.Refresh()
        'System.Threading.Thread.Sleep(1500)
        'Call Fill_Employees_DGV()

        'Call oDataGridViewClicked()
    End Sub
    'Sub Update_Employee()
    '    Try

    '        Dim sSQL As String

    '        If My.Settings.DBType = "Access" Then

    '            sSQL = "UPDATE Employees SET "
    '            sSQL = sSQL & "[First_Name] = '" & SQLConvert(Me.txtName.Text) & "', "
    '            sSQL = sSQL & "[Last_Name] = '" & SQLConvert(Me.Surname.Text) & "', "
    '            sSQL = sSQL & "[Employee_Type] = '" & SQLConvert(Me.EmployeeType.Text) & "', "
    '            sSQL = sSQL & "[Active] = '" & Status.Text & "',"
    '            sSQL = sSQL & "[PayrollNumber] = '" & SQLConvert(Me.txtPayrollNumber.Text) & "',"
    '            sSQL = sSQL & "[Waiter Number] = '" & SQLConvert(Me.WaiterID.Text) & "',"
    '            sSQL = sSQL & "[Payment Type] = '" & PaymentType.Text & "',"
    '            If Commission.Text = "" Then
    '                sSQL = sSQL & "[Commission_Perc] = 0 ,"
    '            Else
    '                sSQL = sSQL & "[Commission_Perc] = " & Commission.Text & ","
    '            End If
    '            sSQL = sSQL & "[Wage_Salary] = '" & Wage.Text & "', "
    '            sSQL = sSQL & "[Contact Number] = '" & Me.ContactNumber1.Text & "', "
    '            sSQL = sSQL & "[Contact 2] = '" & Me.Contact2.Text & "', "
    '            sSQL = sSQL & "[Start Date] =#" & Me.DateSarted.Value.Date.ToString("dd MMMM yyyy") & "#, "
    '            sSQL = sSQL & "[Contract End] = #" & ContractEnds.Value.Date.ToString("dd MMMM yyyy") & "#, "
    '            sSQL = sSQL & "[Loan] = '" & LoanAmount.Text & "', "
    '            sSQL = sSQL & "[Address] = '" & SQLConvert(Me.Address.Text) & "', "
    '            If ckbIsScheduler.Checked = True Then
    '                sSQL = sSQL & "[IsSchedulerContact] = '1' "
    '            Else
    '                sSQL = sSQL & " [IsSchedulerContact] = '0' "
    '            End If
    '            sSQL = sSQL & "Where EmployeeID = " & EmployeeID

    '            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '            End If


    '            Dim cn As New OleDbConnection(My.Settings.CS_Setting)
    '            Dim cmd As New OleDbCommand(sSQL, cn)
    '            cn.Open()
    '            cmd.ExecuteNonQuery()

    '        ElseIf My.Settings.DBType = "SQL" Then

    '            sSQL = "UPDATE Employees SET "
    '            sSQL = sSQL & "[First_Name] = '" & SQLConvert(Me.txtName.Text) & "', "
    '            sSQL = sSQL & "[Last_Name] = '" & SQLConvert(Me.Surname.Text) & "', "
    '            sSQL = sSQL & "[Employee_Type] = '" & SQLConvert(Me.EmployeeType.Text) & "', "
    '            sSQL = sSQL & "[Active] = '" & Status.Text & "',"
    '            sSQL = sSQL & "[PayrollNumber] = '" & SQLConvert(Me.txtPayrollNumber.Text) & "',"
    '            sSQL = sSQL & "[Waiter Number] = '" & SQLConvert(Me.WaiterID.Text) & "',"
    '            sSQL = sSQL & "[Payment Type] = '" & PaymentType.Text & "',"
    '            If Commission.Text = "" Then
    '                sSQL = sSQL & "[Commission_Perc] = 0 ,"
    '            Else
    '                sSQL = sSQL & "[Commission_Perc] = " & Commission.Text & ","
    '            End If
    '            sSQL = sSQL & "[Wage_Salary] = '" & Wage.Text & "', "
    '            sSQL = sSQL & "[Contact Number] = '" & Me.ContactNumber1.Text & "', "
    '            sSQL = sSQL & "[Contact 2] = '" & Me.Contact2.Text & "', "
    '            sSQL = sSQL & "[Start Date] ='" & Me.DateSarted.Value.Date.ToString("dd MMMM yyyy") & "', "
    '            sSQL = sSQL & "[Contract End] = '" & ContractEnds.Value.Date.ToString("dd MMMM yyyy") & "', "
    '            sSQL = sSQL & "[Loan] = '" & LoanAmount.Text & "', "
    '            sSQL = sSQL & "[Address] = '" & SQLConvert(Me.Address.Text) & "', "
    '            If ckbIsScheduler.Checked = True Then
    '                sSQL = sSQL & "[IsSchedulerContact] = '1' "
    '            Else
    '                sSQL = sSQL & " [IsSchedulerContact] = '0' "
    '            End If
    '            sSQL = sSQL & "Where EmployeeID = " & EmployeeID

    '            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
    '                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '            End If

    '            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
    '            Dim cmd As New SqlCommand(sSQL, cn)
    '            cn.Open()
    '            cmd.ExecuteNonQuery()
    '        End If


    '        'FINALLY - set Edit label back to None



    '        MsgBox("Employee updated successfully!")


    '    Catch ex As Exception
    '        MsgBox("There was an error editing supplier information! " & Err.Description)
    '        'Me.Edit_ID_Label.Text = "None"
    '    End Try

    'End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnAddEmp.Click
        Try

            Dim frmEmployeeNew As New frmEmployee_New
            frmEmployeeNew.isEditEmployee = False
            frmEmployeeNew.EditEmployeeID = Nothing
            frmEmployeeNew.ShowDialog()
            Me.dgvEmployees.CellBorderStyle = Windows.Forms.DataGridViewCellBorderStyle.None

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Call Fill_Employees_DGV()
            Me.dgvEmployees.Refresh()
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured :" & ex.Message, "Employees error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs)
        Fill_Employees_DGV()
    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnTerminateEmp.Click

        Try

            If Me.dgvEmployees.SelectedRows.Count <> 1 Then
                MessageBox.Show("Please select one employee record in order to continue", "Employee editing", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                dgvEmployees.Focus()
                Exit Sub
            End If

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            ActivateEmployee()
            Fill_Employees_DGV()
            Me.dgvEmployees.Refresh()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured :" & ex.Message, "Employees error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Sub ActivateEmployee()
        Try

            Dim oSelectedRowIndex As Integer
            If Me.dgvEmployees.SelectedCells.Count < 1 Then
                MsgBox("Please select at least one cell or row!")
                Exit Sub
            End If
            oSelectedRowIndex = Me.dgvEmployees.SelectedCells(0).RowIndex

            Dim sSQL As String = ""


            If btnTerminateEmp.Text = "&Deactivate Employee" Then
                sSQL = "UPDATE Employees SET Active = 'INACTIVE' and [Waiter Number] = '' Where EmployeeID = " & Me.dgvEmployees.Rows(oSelectedRowIndex).Cells(0).Value
            ElseIf btnTerminateEmp.Text = "&Activate Employee" Then
                sSQL = "UPDATE Employees SET Active = 'ACTIVE' Where EmployeeID = " & Me.dgvEmployees.Rows(oSelectedRowIndex).Cells(0).Value
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300

            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
            End Using


            MessageBox.Show("Employee succesfully deactivated", "Employee Active Status", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MsgBox("There was an error deleting an employee information! " & Err.Description)
            'Me.Edit_ID_Label.Text = "None"
        End Try

    End Sub
    Private Sub EmpBankDetails_Click(sender As Object, e As EventArgs) Handles EmpBankDetails.Click

        Dim oSelectedRowIndex As Integer
        oSelectedRowIndex = Me.dgvEmployees.SelectedCells(0).RowIndex

        Dim FirstName = dgvEmployees.Rows(oSelectedRowIndex).Cells(1).Value
        Dim Surname As String = ""
        If dgvEmployees.Rows(oSelectedRowIndex).Cells(2).Value.ToString <> Nothing Then
            Surname = dgvEmployees.Rows(oSelectedRowIndex).Cells(2).Value.ToString
        End If
        Dim BankName As String = ""
        If dgvEmployees.Rows(oSelectedRowIndex).Cells(16).Value.ToString <> Nothing Then
            BankName = dgvEmployees.Rows(oSelectedRowIndex).Cells(16).Value.ToString
        End If
        Dim BankAccHolder As String = ""
        If dgvEmployees.Rows(oSelectedRowIndex).Cells(17).Value.ToString <> Nothing Then
            BankAccHolder = dgvEmployees.Rows(oSelectedRowIndex).Cells(17).Value.ToString
        End If
        Dim BankAccNumber As String = ""
        If dgvEmployees.Rows(oSelectedRowIndex).Cells(18).Value.ToString <> Nothing Then
            BankAccNumber = dgvEmployees.Rows(oSelectedRowIndex).Cells(18).Value.ToString
        End If
        Dim BankBranch As String = ""
        If dgvEmployees.Rows(oSelectedRowIndex).Cells(19).Value.ToString <> Nothing Then
            BankBranch = dgvEmployees.Rows(oSelectedRowIndex).Cells(19).Value.ToString
        End If

        Dim EmpBankDetails As New frmBankingDetails

        EmpBankDetails.FirstName = FirstName
        EmpBankDetails.LastName = Surname

        If BankName = "" And BankAccHolder = "" And BankAccNumber = "" And BankBranch = "" Then

            EmpBankDetails.btnExit.Text = "&Save"
        Else
            EmpBankDetails.txtBankName.Text = BankName
            EmpBankDetails.txtAccountHolder.Text = BankAccHolder
            EmpBankDetails.txtAccNumber.Text = BankAccNumber
            EmpBankDetails.txtBranchNumber.Text = BankBranch
            EmpBankDetails.btnSave.Text = "&Save Changes"
        End If

        EmpBankDetails.ShowDialog()
        Fill_Employees_DGV_After_AddingNew()

    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Private Sub dgvEmployees_SelectionChanged(sender As Object, e As EventArgs) Handles dgvEmployees.SelectionChanged

        Try

            If Not IsNothing(dgvEmployees.CurrentRow) Then

                Dim ActiveStatus As String = CStr(dgvEmployees.CurrentRow.Cells(5).Value)
                ActiveStatus = ActiveStatus.ToUpper

                If ActiveStatus = "ACTIVE" Then
                    btnTerminateEmp.Text = "&Deactivate Employee"
                ElseIf ActiveStatus = "INACTIVE" Then
                    btnTerminateEmp.Text = "&Activate Employee"
                End If

            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Chk_ShowInactive_CheckedChanged(sender As Object, e As EventArgs) Handles Chk_ShowInactive.CheckedChanged
        If Chk_ShowInactive.Checked = True Then
            ShowInactive = True
        Else
            ShowInactive = False
        End If
        Fill_Employees_DGV()
    End Sub
End Class