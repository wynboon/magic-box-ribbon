﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.SupplierMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ds_SupplierMaster = New MB.ds_SupplierMaster()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        CType(Me.SupplierMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ds_SupplierMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SupplierMasterBindingSource
        '
        Me.SupplierMasterBindingSource.DataMember = "SupplierMaster"
        Me.SupplierMasterBindingSource.DataSource = Me.ds_SupplierMaster
        '
        'ds_SupplierMaster
        '
        Me.ds_SupplierMaster.DataSetName = "ds_SupplierMaster"
        Me.ds_SupplierMaster.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "SupplierMasters"
        ReportDataSource1.Value = Me.SupplierMasterBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "MB.Suppliers_Master.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(858, 523)
        Me.ReportViewer1.TabIndex = 0
        '
        'frmReportHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 523)
        Me.Controls.Add(Me.ReportViewer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmReportHome"
        Me.Text = "frmReportHome"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.SupplierMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ds_SupplierMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents SupplierMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ds_SupplierMaster As MB.ds_SupplierMaster
End Class
