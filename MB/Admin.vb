﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS


Public Class Admin

    Private Sub Admin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Admin -> Version : " & My.Settings.Setting_Version
        Catch ex As Exception
        End Try
    End Sub



    Sub Create_New_Suppliers(ByVal New_CoID As String)
        Try
            'ONLY ENTER SPECIFIC COMPANY IF SPECIFIED
            Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString

            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "INSERT INTO Suppliers ( SupplierName, RefID, Balance, Contact_No, Contact_Person, "
                sSQL = sSQL & "Modified, VAT_Default, Account_Default, Category_Default, Detail_Default, Detail_Default2,"
                sSQL = sSQL & "Payment_Default, Address, Email_Address, Fax, Active, Search, Co_ID ) "
                sSQL = sSQL & "SELECT SupplierName, RefID, Balance, "
                sSQL = sSQL & "Contact_No, Contact_Person, Modified, VAT_Default, "
                sSQL = sSQL & "Account_Default, Category_Default, Detail_Default, "
                sSQL = sSQL & "Detail_Default2, Payment_Default, Address, Email_Address, "
                sSQL = sSQL & "Fax, Active, Search, " & New_CoID & " AS Expr1"
                sSQL = sSQL & " FROM Suppliers Where Co_ID = " & oCompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            ElseIf My.Settings.DBType = "SQL" Then


                Dim sSQL As String
                sSQL = "INSERT INTO Suppliers ( SupplierName, RefID, Balance, Contact_No, Contact_Person, "
                sSQL = sSQL & "Modified, VAT_Default, Account_Default, Category_Default, Detail_Default, Detail_Default2,"
                sSQL = sSQL & "Payment_Default, Address, Email_Address, Fax, Active, Search, Co_ID ) "
                sSQL = sSQL & "SELECT SupplierName, RefID, Balance, "
                sSQL = sSQL & "Contact_No, Contact_Person, Modified, VAT_Default, "
                sSQL = sSQL & "Account_Default, Category_Default, Detail_Default, "
                sSQL = sSQL & "Detail_Default2, Payment_Default, Address, Email_Address, "
                sSQL = sSQL & "Fax, Active, Search, " & New_CoID & " AS Expr1"
                sSQL = sSQL & " FROM Suppliers Where Co_ID = " & oCompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 402")
        End Try
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Me.lblUpgrade.Visible = False
            If IsNumeric(Me.txtCoID.Text) = False Then
                MsgBox("Please enter a numeric Company ID")
                Exit Sub
            End If
            Call Create_New_Suppliers(Me.txtCoID.Text)
            Me.lblUpgrade.Visible = True
            Me.lblUpgrade.Text = "Upgrade Complete.."
        Catch ex As Exception
            Me.lblUpgrade.Visible = True
            Me.lblUpgrade.Text = "Upgrade Fail !!!"
            MsgBox(ex.Message)
        End Try
    End Sub


    Sub Create_New_Accounting(ByVal New_CoID As String)
        Try
            'ONLY ENTER SPECIFIC COMPANY IF SPECIFIED
            Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString

            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "INSERT INTO Accounting ( [Segment 1], [Segment 1 Desc], [Segment 2], "
                sSQL = sSQL & "[Segment 2 Desc], [Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT], "
                sSQL = sSQL & "[Account Type], [Report Category], Type, [Display Name], FinCatCode, Co_ID  )"
                sSQL = sSQL & "SELECT [Segment 1], [Segment 1 Desc], [Segment 2], [Segment 2 Desc], "
                sSQL = sSQL & "[Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT],[Account Type], "
                sSQL = sSQL & "[Report Category], Type, [Display Name], FinCatCode, " & New_CoID & " AS Expr1"
                sSQL = sSQL & " FROM Accounting Where Co_ID = " & oCompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection

                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "INSERT INTO Accounting ( [Segment 1], [Segment 1 Desc], [Segment 2], "
                sSQL = sSQL & "[Segment 2 Desc], [Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT], "
                sSQL = sSQL & "[Account Type], [Report Category], Type, [Display Name], FinCatCode, Co_ID  )"
                sSQL = sSQL & "SELECT [Segment 1], [Segment 1 Desc], [Segment 2], [Segment 2 Desc], "
                sSQL = sSQL & "[Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT],[Account Type], "
                sSQL = sSQL & "[Report Category], Type, [Display Name], FinCatCode, " & New_CoID & " AS Expr1"
                sSQL = sSQL & " FROM Accounting Where Co_ID = " & oCompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 403")
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Me.lblUpgrade.Visible = False
            If IsNumeric(Me.txtCoID2.Text) = False Then
                MsgBox("Please enter a numeric Company ID")
                Exit Sub
            End If
            Call Create_New_Accounting(Me.txtCoID2.Text)
            Me.lblUpgrade.Visible = True
            Me.lblUpgrade.Text = "Upgrade Complete.."
        Catch ex As Exception
            Me.lblUpgrade.Visible = True
            Me.lblUpgrade.Text = "Upgrade Fail !!!"
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Create_New_Periods(ByVal New_CoID As String)
        Try
            'ONLY ENTER SPECIFIC COMPANY IF SPECIFIED
            Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString

            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "INSERT INTO Periods ( Period, [Start Date], [End Date], Co_ID ) "
                sSQL = sSQL & "SELECT Period, [Start Date], [End Date], " & New_CoID & " AS Expr1"
                sSQL = sSQL & " FROM Periods Where Co_ID = " & oCompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection

                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "INSERT INTO Periods ( Period, [Start Date], [End Date], Co_ID ) "
                sSQL = sSQL & "SELECT Period, [Start Date], [End Date], " & New_CoID & " AS Expr1"
                sSQL = sSQL & " FROM Periods Where Co_ID = " & oCompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection

                Using cn
                    cn.Open()
                    cmd.ExecuteNonQuery()
                End Using

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 403")
        End Try
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Me.lblUpgrade.Visible = False
            If IsNumeric(Me.txtCoID3.Text) = False Then
                MsgBox("Please enter a numeric Company ID")
                Exit Sub
            End If
            Call Create_New_Periods(Me.txtCoID3.Text)
            Me.lblUpgrade.Visible = True
            Me.lblUpgrade.Text = "Upgrade Complete.."
        Catch ex As Exception
            Me.lblUpgrade.Visible = True
            Me.lblUpgrade.Text = "Upgrade Fail !!!"
            MsgBox(ex.Message)
        End Try
    End Sub
End Class