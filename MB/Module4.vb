﻿

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS


Module Module4

    Dim oID As Long
    Dim oInvoiceDate As Date
    Dim oSupplier As String
    Dim oInvoiceNumber As String
    Dim oInvoiceTotal As Object
    Dim oPaymentStatus As String
    Dim oPaymentType As String
    Dim oDatePaid As String
    Dim oAmountPaid As String
    Dim oInvoice_Header_Error As String
    Dim Invoice_Header_Data_Error As String
    Dim oNotes As String
    Dim blnLineError As Boolean
    Dim blnMasterError As Boolean


    Private Sub Loop_through_invoice_headers_table()

        Try

            blnMasterError = False

            oSet_InvoivceHeader_Errors_to_nothing()
            oSet_InvoivceLines_Errors_to_nothing()

            Invoice_Header_Data_Error = ""

            'Dim sInvoice_Header_Rows_Not_Added As String: sInvoice_Header_Rows_Not_Added = ""

            Dim oMessageHeader As String
            Dim sMessage As String
            Dim oResult As Object

            Dim oRowCount As Long
            oRowCount = 0

            Dim lRunningSum As Long
            lRunningSum = 0

            Dim oLineError As String

            Dim sSQL As String
            sSQL = "SELECT * FROM [Invoice Header]"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                While datareader.Read

                    oRowCount = oRowCount + 1
                    oLineError = ""
                    oInvoice_Header_Error = ""
                    Invoice_Header_Data_Error = ""

                    If Not datareader("ID").Equals(DBNull.Value) Then
                        oID = datareader("ID")
                    Else
                        Exit Sub 'Shortcircuit process if null ID
                    End If

                    If Not datareader("Invoice Date").Equals(DBNull.Value) Then
                        oID = datareader("Invoice Date")
                    Else
                        oLineError = oLineError & "[Invoice Date]"
                    End If

                    If Not datareader("Supplier").Equals(DBNull.Value) Then
                        oID = datareader("Supplier")
                    Else
                        oLineError = oLineError & "[Supplier]"
                    End If

                    If Not datareader("Invoice Number").Equals(DBNull.Value) Then
                        oID = datareader("Invoice Number")
                    Else
                        oLineError = oLineError & "[Invoice Number]"
                    End If

                    If Not datareader("Invoice Total").Equals(DBNull.Value) Then
                        oID = datareader("Invoice Total")
                    Else
                        oLineError = oLineError & "[Invoice Total]"
                    End If

                    If Not datareader("Payment Status").Equals(DBNull.Value) Then
                        oID = datareader("Payment Status")
                    Else
                        oLineError = oLineError & "[Payment Status]"
                    End If

                    If Not datareader("Payment Type").Equals(DBNull.Value) Then
                        oID = datareader("Payment Type")
                    Else
                        oLineError = oLineError & "[Payment Type]"
                    End If

                    If Not datareader("Date Paid").Equals(DBNull.Value) Then
                        oID = datareader("Date Paid")
                    Else
                        oLineError = oLineError & "[Date Paid]"
                    End If

                    If Not datareader("Amount Paid").Equals(DBNull.Value) Then
                        oID = datareader("Amount Paid")
                    Else
                        oLineError = oLineError & "[Amount Paid]"
                    End If

                    If Not datareader("Notes").Equals(DBNull.Value) Then
                        oNotes = datareader("Notes")
                    End If

                    If oLineError <> "" Then
                        blnMasterError = True
                        oInvoice_Header_Error = "Mandatory fields missing data: " & oLineError
                    End If

                    Call oCreateTransactions(oRowCount)

                    If Invoice_Header_Data_Error <> "" Then
                        oInvoice_Header_Error = oInvoice_Header_Error & " Data errors: " & Invoice_Header_Data_Error
                    End If
                    If oInvoice_Header_Error <> "" Then
                        Update_Record_Error_with_new_Error(oID, oInvoice_Header_Error)
                    End If

                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                While datareader.Read

                    oRowCount = oRowCount + 1
                    oLineError = ""
                    oInvoice_Header_Error = ""
                    Invoice_Header_Data_Error = ""

                    If Not datareader("ID").Equals(DBNull.Value) Then
                        oID = datareader("ID")
                    Else
                        Exit Sub 'Shortcircuit process if null ID
                    End If

                    If Not datareader("Invoice Date").Equals(DBNull.Value) Then
                        oID = datareader("Invoice Date")
                    Else
                        oLineError = oLineError & "[Invoice Date]"
                    End If

                    If Not datareader("Supplier").Equals(DBNull.Value) Then
                        oID = datareader("Supplier")
                    Else
                        oLineError = oLineError & "[Supplier]"
                    End If

                    If Not datareader("Invoice Number").Equals(DBNull.Value) Then
                        oID = datareader("Invoice Number")
                    Else
                        oLineError = oLineError & "[Invoice Number]"
                    End If

                    If Not datareader("Invoice Total").Equals(DBNull.Value) Then
                        oID = datareader("Invoice Total")
                    Else
                        oLineError = oLineError & "[Invoice Total]"
                    End If

                    If Not datareader("Payment Status").Equals(DBNull.Value) Then
                        oID = datareader("Payment Status")
                    Else
                        oLineError = oLineError & "[Payment Status]"
                    End If

                    If Not datareader("Payment Type").Equals(DBNull.Value) Then
                        oID = datareader("Payment Type")
                    Else
                        oLineError = oLineError & "[Payment Type]"
                    End If

                    If Not datareader("Date Paid").Equals(DBNull.Value) Then
                        oID = datareader("Date Paid")
                    Else
                        oLineError = oLineError & "[Date Paid]"
                    End If

                    If Not datareader("Amount Paid").Equals(DBNull.Value) Then
                        oID = datareader("Amount Paid")
                    Else
                        oLineError = oLineError & "[Amount Paid]"
                    End If

                    If Not datareader("Notes").Equals(DBNull.Value) Then
                        oNotes = datareader("Notes")
                    End If


                    If oLineError <> "" Then
                        blnMasterError = True
                        oInvoice_Header_Error = "Mandatory fields missing data: " & oLineError
                    End If

                    Call oCreateTransactions(oRowCount)

                    If Invoice_Header_Data_Error <> "" Then
                        oInvoice_Header_Error = oInvoice_Header_Error & " Data errors: " & Invoice_Header_Data_Error
                    End If
                    If oInvoice_Header_Error <> "" Then
                        Update_Record_Error_with_new_Error(oID, oInvoice_Header_Error)
                    End If

                End While
                connection.Close()
            End If


        Catch ex As Exception
            MsgBox("Error in Loop_through_invoice_headers_table" & ex.Message)
        End Try

    End Sub


    Sub oCreateTransactions(ByVal oRow As Long)

        On Error GoTo EH

        Dim oBatchID As Long
        Dim oTransactionID As Long
        Dim oLinkID As Long
        Dim sTransactionDate As String
        Dim sCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sInvoiceNumber As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sContraAccount As String
        Dim sLinkAcc As String
        Dim oAmount As Single
        Dim oTaxType As Integer
        Dim oTaxAmount As Single
        Dim sUserID As String
        Dim sSupplierName As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As Integer
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim TransactionType As String
        Dim sDRCR As String
        Dim sDescriptionCode As String
        Dim sDocType As String

        Dim Credit_Note_DRCR As String
        Dim oDGV As String

        Dim Full_Credit_Amount As Single
        Dim Credit_Amount As Single
        Dim oCredit_List_Box_RowIndex As Integer
        oCredit_List_Box_RowIndex = -1
        Dim Credit_TaxAmount As Single
        Dim Credit_ex_VAT As Single

        Dim sMessage As String
        Dim lRunningSum As Object


        sContraAccount = Get_Segment4_Using_Type_in_Accounting("Contra Account")
        If sContraAccount = "NO ACCOUNT" Then
            Invoice_Header_Data_Error = "No GL Account matching categories"
        End If

        '==================  First Generate TransactionID and BatchID ===========================

        oBatchID = oCreateBatchID()

        oTransactionID = oCreateTransactionID() 'adds a new record to the TransactionID table thus creating a new ID number that is used as the Transaction ID

        Dim oPaid_TransacionID As Long
        If oPaymentStatus = "PAID" Then
            oPaid_TransacionID = oCreateTransactionID
        End If

        '=========================================================================================
        '======= CR Creditors Control =========
        oLinkID = 0
        sTransactionDate = CStr(Format(oInvoiceDate, "dd MMMM yyyy")) 'ALWAYS GET DATE OTHERWISE INCLUDES TIME OF DAY
        'sCaptureDate = Now.Date.ToString("dd MMMM yyyy")
        sCaptureDate = CStr(Format(Now, "dd MMMM yyyy"))

        sPPeriod = GetPeriod(sTransactionDate)
        If IsNumeric(sPPeriod) = False Or CInt(sPPeriod) < 1 Then
            Invoice_Header_Data_Error = Invoice_Header_Data_Error & " No period found for transaction date"
        End If

        sFinYear = Get_Year_From_Period(sPPeriod)

        sGDC = "G" 'General/Debtors/Creditorsoice
        sInvoiceNumber = oInvoiceNumber

        sLinkAcc = CStr(oBatchID) '$-NEW

        sUserID = "" 'for later - they should log on to use this
        sSupplierName = oSupplier
        'oSupplierID = DLookup("[SupplierID] ", "Suppliers", " [SupplierName] = '" & SQLConvert(oSupplier) & "'")
        oSupplierID = Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") '
        sDescription4 = oNotes
        sDescription5 = ""
        oEmployeeID = 0 'not required
        sDescriptionCode = ""



        oAmount = oInvoiceTotal

        oTaxAmount = 0
        sDescription = "CREDITORS CONTROL" '[Description] field
        sDescription2 = "CREDITORS CONTROL" '[Description2] field
        sDescription3 = "CREDITORS CONTROL" '[Description3] field


        blnPosted = False 'becomes true when posted to Pastel

        TransactionType = 12
        oTaxType = 0
        sDRCR = "Cr"

        Dim sCreditorsControl_Account As String
        sCreditorsControl_Account = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
        If sCreditorsControl_Account = "NO ACCOUNT" Then
            Invoice_Header_Data_Error = Invoice_Header_Data_Error & "No GL Account found for 'Creditors Control'"
        End If
        sAccountNumber = sCreditorsControl_Account
        sDescriptionCode = Get_Segment4_from_GLACCOUNT_in_Accounting(sAccountNumber)
        If sDescriptionCode = "NO SEGMENT" Then
            Invoice_Header_Data_Error = Invoice_Header_Data_Error & "No Segment 4 code found for 'Creditors Control'"
        End If
        sDocType = "Invoice"

        blnAccounting = True

        If Invoice_Header_Data_Error <> "" Then blnMasterError = True

        If blnMasterError = False Then

            Call Add_Transaction(oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
       sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
      oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
      blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

        End If



        Dim oRowNumber As Long
        oRowNumber = 0
        Dim oMessageHeader As String

        Dim oInvoiceLineID As Long
        Dim oInvoiceLine_InvoiceHeaderID As Long
        Dim oGrossAmount As Object
        Dim oVAT_Type As Integer
        Dim oVAT_Amount As Object
        Dim exVAT As Object
        Dim oCategory1 As String
        Dim oCategory2 As String
        Dim oCategory3 As String

        'Dim blnMultipleLines As Boolean

        'Dim oLineCount As Integer: oLineCount = oCount_Invoice_Lines(oID)

        'If oLineCount > 1 Then
        'blnMultipleLines = True
        'End If

        Dim oLineError As String
        Dim oDataError As String
        Dim sDataError As String


        lRunningSum = 0

        'Set rs = db.OpenRecordset("SELECT * FROM [Invoice Lines] Where [Invoice Header ID] = 2")

        Dim sSQL As String
        sSQL = "SELECT * FROM [Invoice Lines] Where [Invoice Header ID] = " & oID

        If My.Settings.DBType = "Access" Then
            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, connection)
            connection.Open()
            Dim datareader As OleDbDataReader = cmd.ExecuteReader

            While datareader.Read


                oRowNumber = oRowNumber + 1

                sDataError = ""
                oLineError = ""

                If Not datareader("ID").Equals(DBNull.Value) Then
                    oInvoiceLineID = datareader("ID")
                Else
                    Exit Sub 'Shortcircuit process if null ID
                End If

                If Not datareader("Invoice Header ID").Equals(DBNull.Value) Then
                    oInvoiceLine_InvoiceHeaderID = datareader("Invoice Header ID")
                Else
                    oLineError = oLineError & "[Invoice Header ID]"
                End If

                If Not datareader("Gross Amount").Equals(DBNull.Value) Then
                    oGrossAmount = datareader("Gross Amount")
                Else
                    oLineError = oLineError & "[Gross Amount]"
                End If

                If Not datareader("VAT Type").Equals(DBNull.Value) Then
                    oVAT_Type = datareader("VAT Type")
                Else
                    oLineError = oLineError & "[VAT Type]"
                End If

                If Not datareader("VAT Amount").Equals(DBNull.Value) Then
                    oVAT_Amount = datareader("VAT Amount")
                Else
                    oLineError = oLineError & "[VAT Amount]"
                End If

                If Not datareader("ex VAT").Equals(DBNull.Value) Then
                    exVAT = datareader("ex VAT")
                Else
                    oLineError = oLineError & "[ex VAT]"
                End If

                If Not datareader("Category 1").Equals(DBNull.Value) Then
                    oCategory1 = datareader("Category 1")
                Else
                    oLineError = oLineError & "[Category 1]"
                End If

                If Not datareader("Category 2").Equals(DBNull.Value) Then
                    oCategory2 = datareader("Category 2")
                Else
                    oLineError = oLineError & "[Category 2]"
                End If

                If Not datareader("Category 3").Equals(DBNull.Value) Then
                    oCategory3 = datareader("Category 3")
                Else
                    oLineError = oLineError & "[Category 3]"
                End If






                '======= DR Supplier =========

                oAmount = exVAT 'Amount before VAT $-NEW
                oTaxType = oVAT_Type
                oTaxAmount = oVAT_Amount

                sDescription = oCategory1 '[Description] field
                sDescription2 = oCategory2 '[Description2] field
                sDescription3 = oCategory3 '[Description3] field

                blnPosted = False 'becomes true when posted to Pastel

                blnVoid = False
                TransactionType = 12
                sDRCR = "Dr"
                sAccountNumber = Get_MB_GL_Account(oCategory1, oCategory2, oCategory3) 'gleamed from the Accouning table
                If sAccountNumber = "NO ACCOUNT" Then
                    sDataError = sDataError & " Account number not found for categories!"
                End If
                sDescriptionCode = Get_Segment4(sDescription, sDescription2, sDescription3)
                'sDescriptionCode = Get_Segment4_from_Categories(sDescription, sDescription2, sDescription3) 'Picks up [Segment 4] value from Accounting table based on the three category selections
                If sDescriptionCode = "NO SEGMENT" Then
                    sDataError = sDataError & " Segment 4 not found for given categories!"
                End If
                'Set DocType to blank so not picked up in queries
                sDocType = ""
                blnAccounting = True

                If oLineError <> "" Or sDataError <> "" Then blnMasterError = True

                If blnMasterError = False Then

                    Call Add_Transaction(oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
        sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
       oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
       blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

                End If
                '======= DR VAT =========

                oAmount = oVAT_Amount
                oTaxType = 1
                oTaxAmount = 0
                sDescription = sSupplierName
                sDescription2 = "VAT CONTROL"
                sDescription3 = "VAT CONTROL"

                TransactionType = 12
                oTaxType = 0
                sDRCR = "Dr"

                Dim sVAT_Account As String
                sVAT_Account = Get_Segment4_Using_Type_in_Accounting("VAT Control")
                sAccountNumber = sVAT_Account
                If sAccountNumber = "NO ACCOUNT" Then
                    sDataError = "Account number not found for VAT Control!"
                End If

                sDescriptionCode = Get_Segment4_from_GLACCOUNT_in_Accounting(sAccountNumber)
                If sDescriptionCode = "NO SEGMENT" Then
                    sDataError = sDataError & " Segment 4 not found for given categories!"
                End If

                blnAccounting = True

                If oLineError <> "" Or sDataError <> "" Then blnMasterError = True

                If blnMasterError = False Then

                    Call Add_Transaction(oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
    sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
    blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

                End If

                If oPaymentStatus = "PAID" Then 'this is gathered in the invoice header data in first procedure

                    '========== PAYMENT MADE ==============


                    oLinkID = oTransactionID 'Need the linkID to equal the TransactionID of the original invoice
                    oTransactionID = oPaid_TransacionID 'generated in first procedure for invoice header
                    sTransactionDate = oDatePaid

                    'Only set Accounting to true if cash is paid
                    If oPaymentType = "EFT" Then
                        blnAccounting = False
                    ElseIf oPaymentType = "CHEQUE" Then
                        blnAccounting = False
                    Else
                        blnAccounting = True
                    End If

                    sDescription = sSupplierName
                    TransactionType = 1
                    oTaxType = 0 '[Tax Type] field
                    oTaxAmount = 0
                    oAmount = oAmountPaid
                    sDRCR = "Dr"

                    sDescription2 = "CREDITORS CONTROL" '[Description2] field
                    sDescription3 = "CREDITORS CONTROL" '[Description3] field
                    sCreditorsControl_Account = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
                    If sCreditorsControl_Account = "NO ACCOUNT" Then
                        sDataError = sDataError & "No GL Account found for 'Creditors Control'"
                    End If
                    sAccountNumber = sCreditorsControl_Account
                    sDescriptionCode = Get_Segment4_from_GLACCOUNT_in_Accounting(sAccountNumber)
                    If sDescriptionCode = "NO SEGMENT" Then
                        sDataError = sDataError & " Segment 4 not found for given categories!"
                    End If
                    sDocType = "Payment"

                    If oLineError <> "" Or sDataError <> "" Then blnMasterError = True

                    If blnMasterError = False Then

                        Call Add_Transaction(oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
        sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
       oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
       blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

                    End If

                    sDescription = Get_Segm1Desc_From_DisplayName_in_Accounting(oPaymentType)
                    If sDescription = "NO DESCRIPTION" Then
                        sDataError = sDataError & "No Segment 1 Desc found for payment display name"
                    End If
                    sDescription2 = Get_Segm2Desc_From_DisplayName_in_Accounting(oPaymentType)
                    If sDescription2 = "NO DESCRIPTION" Then
                        sDataError = sDataError & "No Segment 2 Desc found for payment display name"
                    End If
                    sDescription3 = Get_Segm3Desc_From_DisplayName_in_Accounting(oPaymentType)
                    If sDescription3 = "NO DESCRIPTION" Then
                        sDataError = sDataError & "No Segment 3 Desc found for payment display name"
                    End If
                    sAccountNumber = Get_GLACCOUNT_from_DisplayName_in_Accounting(oPaymentType)
                    If sAccountNumber = "NO ACCOUNT" Then
                        sDataError = sDataError & "No GL Account found for Display Name"
                    End If
                    sDescriptionCode = Get_Segment4_from_GLACCOUNT_in_Accounting(sAccountNumber)

                    TransactionType = 1
                    oTaxType = 0
                    oTaxAmount = 0
                    oAmount = oAmountPaid
                    sDRCR = "Cr"
                    'Set the DocType to blank again to avoid being picked up in the query
                    sDocType = ""

                    If oLineError <> "" Or sDataError <> "" Then blnMasterError = True

                    If blnMasterError = False Then

                        Call Add_Transaction(oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
        sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
       oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
       blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName)

                    End If


                End If


                'Error test
                'oLineError = "[Field1][Field2]"
                'oDataError = "my data error"


                sMessage = ""

                If oLineError <> "" Then
                    sMessage = "Mandatory fields missing data: " & oLineError
                End If
                If oDataError <> "" Then
                    sMessage = sMessage & " Data Errors: " & oDataError
                End If

                If sMessage <> "" Then
                    Update_Invoice_Line_with_new_Error(oInvoiceLineID, sMessage)
                End If




            End While
        Else

        End If

        Exit Sub
EH:
        MsgBox(Err.Description)

    End Sub


    Function Get_MB_GL_Account(ByVal Category1 As String, ByVal Category2 As String, ByVal Category3 As String) As String
        Try

            Dim oResult As String

            Dim sSQL As String
            sSQL = "Select [Segment 4] From Accounting Where [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                  SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_MB_GL_Account = "NO ACCOUNT"
                Else
                    Get_MB_GL_Account = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = String.Empty
                oResult = cmd.ExecuteScalar()
                If IsNothing(oResult) Then
                    Get_MB_GL_Account = "NO ACCOUNT"
                ElseIf oResult = "" Then
                    Get_MB_GL_Account = "NO ACCOUNT"
                Else
                    Get_MB_GL_Account = oResult
                End If
                connection.Close()
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 176")
        End Try


    End Function



    Function Get_Segment4_Using_Type_in_Accounting(ByVal sString As String) As String
        Try
            Dim sSQL As String
            'Dim My.Settings.CS_Setting As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"

            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Type] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment4_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                'Dim oResult As String
                'xSQLTable_Accounts.DefaultView.RowFilter = "Type='" & SQLConvert(sString) & "'"

                'oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("GL ACCOUNT").ToString

                'Get_GLACCOUNT_Using_Type_in_Accounting = oResult

                'Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment4_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

                'End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account based on Type in Accounting!")
        End Try
    End Function


    Function Database_Lookup_String_return_Integer(ByVal oTable As String, oLookupField As String, ByVal WhereField As String, ByVal WhereValue As String, ByVal DataType As String) As Integer
        Try
            Dim oResult As Object
            Dim sSQL As String

            sSQL = "SELECT DISTINCT " & oLookupField & " FROM " & oTable & " WHERE " & WhereField & " = '" & SQLConvert(WhereValue) & "'"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Database_Lookup_String_return_Integer = CInt(cmd.ExecuteScalar().ToString)
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                'xSQLTable_Suppliers.DefaultView.RowFilter = WhereField & "='" & SQLConvert(WhereValue) & "'"

                'oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item(oLookupField).ToString

                'Database_Lookup_String_return_Integer = oResult

                'Else

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    Database_Lookup_String_return_Integer = 0
                Else
                    Database_Lookup_String_return_Integer = oResult
                End If
                connection.Close()
            End If
            'End If
        Catch ex As Exception
            MsgBox("There was an error looking up a database value " & oLookupField & " in table " & oTable & Err.Description)

        End Try
    End Function





    Function GetPeriod(ByVal sTransactionDate As String) As String
        Try

            Dim sSQL As String
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= '" & sTransactionDate & "'"
                sSQL = sSQL & " AND [End Date] >= '" & sTransactionDate & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetPeriod = cmd.ExecuteScalar().ToString
                connection.Close()
                'End If

        Catch ex As Exception
            MsgBox("There was an error getting the accounting period! " & vbNewLine & ex.Message)
            GetPeriod = "NA"
        End Try
    End Function

    Function GetFinYear(ByVal sTransactionDate As String) As String
        Try

            Dim sSQL As String

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                sSQL = "SELECT DISTINCT FinYear FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= Format('" & sTransactionDate & "',""Short Date"")"
                sSQL = sSQL & " AND [End Date] >= Format('" & sTransactionDate & "',""Short Date"")"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetFinYear = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                sSQL = "SELECT DISTINCT FinYear FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= CAST('" & sTransactionDate & "' as date)"
                sSQL = sSQL & " AND [End Date] >= CAST('" & sTransactionDate & "' as date)"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetFinYear = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error getting the 'financial year!" & ex.Message & " 045bb. Please make sure that Co_ID and other data is correct in the periods table! ")
            GetFinYear = "NA"
        End Try
    End Function

    Function Get_Segment4_from_GLACCOUNT_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [GL ACCOUNT] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment4_from_GLACCOUNT_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                'Dim oResult As String
                'xSQLTable_Accounts.DefaultView.RowFilter = "[GL ACCOUNT]='" & SQLConvert(sString) & "'"

                'oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString

                'Get_Segment4_from_GLACCOUNT_in_Accounting = oResult

                'Else

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment4_from_GLACCOUNT_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

                'End If

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account in Accounting!")
        End Try
    End Function

    Function Get_Year_From_Period(ByVal sPeriod As String) As String
        Dim oResult As String
        Dim sSQL As String
        sSQL = "SELECT DISTINCT [Start Date] FROM Periods WHERE [Period] = " & sPeriod
        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
        End If

        If My.Settings.DBType = "Access" Then
            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, connection)
            connection.Open()
            oResult = cmd.ExecuteScalar().ToString
            Dim sYear = Year(CDate(oResult))
            If oResult = "" Then
                Get_Year_From_Period = "NO YEAR"
            Else
                Get_Year_From_Period = sYear
            End If
            connection.Close()
        ElseIf My.Settings.DBType = "SQL" Then
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            oResult = cmd.ExecuteScalar().ToString
            Dim sYear = Year(CDate(oResult))
            If oResult = "" Then
                Get_Year_From_Period = "NO YEAR"
            Else
                Get_Year_From_Period = sYear
            End If
            connection.Close()
        End If

    End Function


    Function Get_Segm1Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm1Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                'Dim oResult As String
                'xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                'oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 1 Desc").ToString
                'End If
                'Get_Segm1Desc_From_DisplayName_in_Accounting = oResult
                'Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segm1Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

                ' End If

            End If


        Catch ex As Exception
            MsgBox("There was an error looking up Segment 1 Desc in Accounting! " & ex.Message)
        End Try
    End Function



    Function Get_Segm3Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm3Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                'Dim oResult As String
                'xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                'oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 3 Desc").ToString
                'End If
                'Get_Segm3Desc_From_DisplayName_in_Accounting = oResult
                'Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segm3Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
                'End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up Segment 3 Desc in Accounting!")
        End Try
    End Function

    Function Get_GLACCOUNT_from_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_GLACCOUNT_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                'Dim oResult As String
                'xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                'oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("GL ACCOUNT").ToString
                'If
                'Get_GLACCOUNT_from_DisplayName_in_Accounting = oResult
                'Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_GLACCOUNT_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
                'End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account in Accounting!")
        End Try
    End Function

    Function oCount_Invoice_Lines(ByVal sInvoiceHeaderID As Long) As Integer
        Try

            Dim sSQL As String
            sSQL = "Select Count(*) from [Invoice Lines] Where [Invoice Header ID] = " & sInvoiceHeaderID

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oCount_Invoice_Lines = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oCount_Invoice_Lines = cmd.ExecuteScalar().ToString
                connection.Close()
            End If


        Catch ex As Exception
            MsgBox("There was an error looking up an account!")
        End Try
    End Function

    Function Get_Segment4(ByVal Category1 As String, ByVal Category2 As String, ByVal Category3 As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4 = datareader("Segment 4")
                    Else
                        Get_Segment4 = ""
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                'Dim oResult As String
                'Dim oCriteria As String
                'oCriteria = "[Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                'SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
                'xSQLTable_Accounts.DefaultView.RowFilter = oCriteria
                'If xSQLTable_Accounts.DefaultView.Count > 0 Then
                'oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                'Get_Segment4 = oResult
                'Else
                'Get_Segment4 = ""
                'End If

                'Else

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4 = datareader("Segment 4")
                    Else
                        Get_Segment4 = ""
                    End If
                End While
                connection.Close()
                'End If
            End If
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try


    End Function

    Sub Add_Transaction(ByVal oBatchID As Long, ByVal oTransactionID As Long, ByVal oLinkID As Long, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                         ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                         ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Single, ByVal sUserID As String, _
                         ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                         ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                         ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                         ByVal oDocType As String, ByVal oSupplierName As String)


        Dim sSQL As String
        sSQL = "INSERT INTO Transactions_Imported_Loader ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
        sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
        sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName] ) "
        sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
        sSQL = sSQL & "#" & sCaptureDate & "# As MyDate1, #" & sTransactionDate & "# As MyDate2, "
        sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
        sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
        sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
        sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
        sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
        sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
        sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
        sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
        sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
        sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
        sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
        sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28"

        'DoCmd.RunSQL(sSQL)

    End Sub

    Sub oSet_InvoivceHeader_Errors_to_nothing()

        Try
            Dim sSQL As String
            sSQL = "Update [Invoice Header] Set [Errors] = ''"
            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 177")
        End Try
    End Sub

    Sub oSet_InvoivceLines_Errors_to_nothing()

        Try
            Dim sSQL As String
            sSQL = "Update [Invoice Lines] Set [Errors] = ''"
            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 177")
        End Try
    End Sub


    Sub Update_Record_Error_with_new_Error(ByVal oID As Long, ByVal oNewError As String)

        Try
            Dim sSQL As String
            sSQL = "Update [Invoice Header] Set [Errors] = '" & oNewError & "' Where ID = " & oID
            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 180")
        End Try
    End Sub

    Sub Update_Invoice_Line_with_new_Error(ByVal oID As Long, ByVal oNewError As String)

        Try
            Dim sSQL As String
            sSQL = "Update [Invoice Lines] Set [Errors] = '" & oNewError & "' Where ID = " & oID
            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 181")
        End Try
    End Sub

End Module
