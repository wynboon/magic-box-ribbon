﻿

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmPeriods

    Private Sub Periods_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Periods -> Version : " & My.Settings.Setting_Version
            Me.TopMost = True
            GetPeriods()
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub GetPeriods()
        Try

            Dim sSQL As String

            sSQL = "Select [Start Date], [End Date], FinYear 'Financial Year' from Periods"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "My_table")
                connection.Close()
                Me.dgvPeriods.DataSource = ds
                Me.dgvPeriods.DataMember = "My_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "My_table")
                connection.Close()
                Me.dgvPeriods.DataSource = ds
                Me.dgvPeriods.DataMember = "My_table"
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 182")
        End Try

    End Sub



    Private Sub Button_EditSelectedDate_Click(sender As System.Object, e As System.EventArgs) Handles Button_EditSelectedDate.Click
        Call ExecuteUpdate()
    End Sub

    Sub ExecuteUpdate()

        Try

            'First make sure that only one cell is selected
            If Me.dgvPeriods.SelectedCells.Count < 1 Then
                MsgBox("Please select one date cell!")
                Exit Sub
            End If

            If Me.dgvPeriods.SelectedCells.Count > 1 Then
                MsgBox("Please select only one date cell!")
                Exit Sub
            End If

            If Me.dgvPeriods.SelectedCells.Count = 1 Then
                If Me.dgvPeriods.SelectedCells(0).ColumnIndex = 0 Then
                    MsgBox("Please select only one date cell!")
                    Exit Sub
                End If
            End If


            Dim oColumnIndex As Integer = Me.dgvPeriods.SelectedCells(0).ColumnIndex
            Dim oRowIndex As Integer = Me.dgvPeriods.SelectedCells(0).RowIndex
            Dim oID As Integer = Me.dgvPeriods.Rows(oRowIndex).Cells(0).Value

            Dim sSQL As String
            Dim oDate As Date = Me.dgvPeriods.SelectedCells(0).Value
            If My.Settings.DBType = "Access" Then

                If oColumnIndex = 1 Then 'From Date
                    sSQL = "UPDATE Periods SET "
                    sSQL = sSQL & "[Start Date] = #" & Me.dgvPeriods.SelectedCells(0).Value & "# "
                    sSQL = sSQL & "WHERE Period = " & oID & " "
                Else 'To Date
                    sSQL = "UPDATE Periods SET "
                    sSQL = sSQL & "[End Date] = #" & Me.dgvPeriods.SelectedCells(0).Value & "# "
                    sSQL = sSQL & "WHERE Period = " & oID & " "
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()

            ElseIf My.Settings.DBType = "SQL" Then

                If oColumnIndex = 1 Then 'From Date
                    sSQL = "UPDATE Periods SET "
                    sSQL = sSQL & "[Start Date] = '" & Me.dgvPeriods.SelectedCells(0).Value & "' "
                    sSQL = sSQL & "WHERE Period = " & oID & " "
                Else 'To Date
                    sSQL = "UPDATE Periods SET "
                    sSQL = sSQL & "[End Date] = '" & Me.dgvPeriods.SelectedCells(0).Value & "' "
                    sSQL = sSQL & "WHERE Period = " & oID & " "
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()

            End If

        Catch ex As Exception
            MsgBox("There was an error editing supplier information! " & Err.Description)

        End Try
    End Sub

    Sub AppendAccounting(ByVal oSegment1 As String, ByVal oSeg1Desc As String, ByVal oSegment2 As String, ByVal oSeg2Desc As String, _
             ByVal oSegment3 As String, ByVal oSeg3Desc As String, ByVal oSegment4 As String, ByVal GLACCOUNT As String, ByVal AccountType As String)
        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String

                sSQL = "INSERT INTO Accounting ( [Segment 1], [Segment 1 Desc], [Segment 2], [Segment 2 Desc], [Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT], [Account Type] ) "
                sSQL = sSQL & "SELECT "
                sSQL = sSQL & "'" & oSegment1 & "' as Expr1, "
                sSQL = sSQL & "'" & oSeg1Desc & "' as Expr2, "
                sSQL = sSQL & "'" & oSegment2 & "' as Expr3, "
                sSQL = sSQL & "'" & oSeg2Desc & "' as Expr4, "
                sSQL = sSQL & "'" & oSegment3 & "' as Expr5, "
                sSQL = sSQL & "'" & oSeg3Desc & "' as Expr6, "
                sSQL = sSQL & "'" & oSegment4 & "' as Expr7, "
                sSQL = sSQL & "'" & GLACCOUNT & "' as Expr8, "
                sSQL = sSQL & "'" & AccountType & "' as Expr9"

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)

                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)

                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String

                sSQL = "INSERT INTO Accounting ( [Segment 1], [Segment 1 Desc], [Segment 2], [Segment 2 Desc], [Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT], [Account Type] ) "
                sSQL = sSQL & "SELECT "
                sSQL = sSQL & "'" & oSegment1 & "' as Expr1, "
                sSQL = sSQL & "'" & oSeg1Desc & "' as Expr2, "
                sSQL = sSQL & "'" & oSegment2 & "' as Expr3, "
                sSQL = sSQL & "'" & oSeg2Desc & "' as Expr4, "
                sSQL = sSQL & "'" & oSegment3 & "' as Expr5, "
                sSQL = sSQL & "'" & oSeg3Desc & "' as Expr6, "
                sSQL = sSQL & "'" & oSegment4 & "' as Expr7, "
                sSQL = sSQL & "'" & GLACCOUNT & "' as Expr8, "
                sSQL = sSQL & "'" & AccountType & "' as Expr9"

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()

            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 183")
        End Try
    End Sub

    Sub Delete_All_Accounting_Table()
        Try

            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "Delete * From Accounting"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "Delete * From Accounting"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 186")
        End Try
    End Sub



    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPeriods.CellContentClick

    End Sub

    Private Sub DataGridView1_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPeriods.CellValueChanged
        Call ExecuteUpdate()
    End Sub


End Class