﻿

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D


Public Class frmInvoiceDetail_Per_Supplier
#Region "Variables"
    Public SupplierName As String = String.Empty
    Dim blnSearchByDate As Boolean
#End Region
    Private Sub frmInvoiceDetail_Per_Supplier_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            e.Cancel = True
            Me.ShowInTaskbar = False
            Me.Hide()
        End If
    End Sub
    Private Sub frmInvoiceDetail_Per_Supplier_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Supplier Invoice Detail -> Version : " & My.Settings.Setting_Version
        txtSupplier.Text = SupplierName
        'DO NOTHING
    End Sub

    Sub GetInvoiceDetailPerSupplier()

        Try
            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            If SupplierName = "" Then
                Exit Sub
            End If
            Dim sSQL_Supplier As String = ""

            If SupplierName = "[ALL]" Or SupplierName = "" Then
                'do nothing
            Else
                ' oSupplier = Form_Supplier_Payments.ComboBox_Supplier.Text
                sSQL_Supplier = " And SupplierName = '" & SQLConvert(SupplierName) & "'"
            End If

            'First clear DGV
            DataGridView1.DataSource = Nothing

            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.DateTimePicker_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTimePicker_To.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String = ""
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If

            Dim sSQL As String

            sSQL = "Select Reference, [Transaction Date], DocType, Amount, Info From Transactions "
            sSQL = sSQL & " where DocType = 'Invoice' and [Description Code] = 'HAAAAA'AND Accounting = '1' and Void ='0' and LinkID = 0"

            sSQL = sSQL & sSQL_Supplier

            If blnSearchByDate = True Then
                sSQL = sSQL & sDateCriteria
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If


            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            End If
            If DataGridView1.ColumnCount > 0 Then

                Me.DataGridView1.Columns("Amount").DefaultCellStyle.Format = "n2"

                Me.DataGridView1.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                Me.DataGridView1.AutoResizeColumns()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 057b")
        End Try
    End Sub
    Private Sub btnSearchByDate_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchByDate.Click
        Try
            blnSearchByDate = True
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            GetInvoiceDetailPerSupplier()
            InvoiceDue()
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            MsgBox(ex.Message)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Public Sub InvoiceDue()
        Dim Due As Decimal
        For Each row As DataGridViewRow In DataGridView1.Rows
            If row.Cells("DocType").Value = "Invoice" Then
                Due += CDec(row.Cells("Amount").Value)
            ElseIf row.Cells("DocType").Value = "Payment" Then
                Due -= CDec(row.Cells("Amount").Value)
            End If
        Next
        txtAmount.TextAlign = HorizontalAlignment.Right
        txtAmount.Text = Due.ToString("c")
    End Sub
    
End Class