﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms


Module Module3


    Public Function CheckUserLevel(ByVal sUserName As String, ByVal sPassword As String) As String 'returns "Not User", "Wrong password", or the Level



        Dim sSQL As String
        sSQL = "SELECT * FROM Users"
        If My.Settings.Setting_CompanyID <> "" Or Not IsNothing(My.Settings.Setting_CompanyID) Then
            sSQL &= " Where Co_ID = " & CInt(My.Settings.Setting_CompanyID)
        End If

        Dim oUsername, oPassword, oLevel As String

        'Set to this in case no user found
        CheckUserLevel = "Not User"
        If My.Settings.DBType = "SQL" Then

            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            Try

                cn.Open()
                Using reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                    While reader.Read()
                        oUsername = reader("Username").ToString()
                        oPassword = reader("Password").ToString()
                        oLevel = reader("Level").ToString()

                        If sUserName = oUsername Then

                            If sPassword = oPassword Then
                                CheckUserLevel = oLevel
                            Else
                                CheckUserLevel = "Wrong Password"
                            End If

                        End If

                    End While
                End Using
            Catch ex As Exception
                MsgBox(ex.Message & " 173")
            Finally
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End Try
        End If

    End Function
End Module
