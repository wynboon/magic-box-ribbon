﻿Imports System.Collections

Module Mod_Alachemex
    Public Enum eAlchemexLaunchModes
        eReportManager = 1
        eAdministrator = 2
        eSecurityManager = 3
        eLicenseManager = 4
        eOLAPManager = 5
        eReportViewer = 6
        eCommunity = 7
    End Enum
    Function LaunchReportManager()
        LaunchAlchemexModule("eReportManager")
    End Function
    Function LaunchAdministrator()
        LaunchAlchemexModule("eAdministrator")
    End Function
    Function LaunchLicenseManager()
        LaunchAlchemexModule("eLicenseManager")
    End Function
    Function LaunchViewer()
        LaunchAlchemexModule("eReportViewer")
    End Function

    Public Sub LaunchAlchemexModule(eAlchemexLaunchModes As String)
        Try


            'Getting current system session settings.

            Dim ConnString As String() = My.Settings.CS_Setting.Split(New Char() {"="c})
            Dim ServerSplit() As String = ConnString(1).Split(New Char() {";"c})
            Dim DatabaseSplit() As String = ConnString(2).Split(New Char() {";"c})
            Dim UserSplit() As String = ConnString(3).Split(New Char() {";"c})

            'Create Alchemex COM Object

            Dim objObjFetcher As Object
            objObjFetcher = CreateObject("PLOBJFETCHER.CObjectBroker")
            Dim objSDK = objObjFetcher.CreateObj("Alchemex.COM4.SDK")
            'Adding environmental(Alchemex) variables/parametres

            objSDK.SDKAssemblyPath = CStr(AppDomain.CurrentDomain.BaseDirectory().ToString)
            objSDK.DatabaseServer = CStr(ServerSplit(0).ToString)
            objSDK.DatabaseCatalog = CStr(DatabaseSplit(0).ToString)
            objSDK.DatabaseUser = CStr(UserSplit(0).ToString)
            objSDK.DatabasePassword = CStr(ConnString(4).ToString)
            objSDK.TenantCode = "Local Tenant"
            objSDK.SystemUser = "Local User"

            ' Adding session variables

            objSDK.AddSessionVariable("@LANGUAGE@", "English")
            objSDK.AddSessionVariable("@CurrentCompanyID@", My.Settings.Setting_CompanyID)
            objSDK.AddSessionVariable("@CurrentCompanyName@", CStr(GetCompanyName()))
            objSDK.AddSessionVariable("@CurrentFinancialYear@", GetFinYear(Now.Date.ToString("dd MMMM yyyy")))
            objSDK.AddSessionVariable("@CurrentPeriod@", GetPeriod(Now.Date.ToString("dd MMMM yyyy")))
            objSDK.AddSessionVariable("@CurrentGroupID@", GroupID.ToString)


            'L

            Select Case eAlchemexLaunchModes

                Case "eReportManager"
                    objSDK.LaunchReportManager()
                Case "eAdministrator"
                    objSDK.LaunchAdministratorTool()
                Case "eSecurityManager"
                    objSDK.LaunchSecurityManager()
                Case "eLicenseManager"
                    objSDK.LaunchLicenseManager()
                Case "eOLAPManager"
                    objSDK.LaunchOLAPManager()
                Case "eCommunity"
                    objSDK.LaunchCommunity()
                Case "eReportViewer"
                    objSDK.LaunchViewer()
            End Select

            objSDK = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Module
