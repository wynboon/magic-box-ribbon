﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInvoiceEdit))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.OriginalPaid = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Lbl_OriginalAmount = New System.Windows.Forms.Label()
        Me.Lbl_SupplierID = New System.Windows.Forms.Label()
        Me.DTP_InvoiceDate = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TB_InvNo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CB_SupplierName = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Lbl_OrigTxID = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SupplierBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbl_TaxSplit = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TB_TaxAmount = New System.Windows.Forms.TextBox()
        Me.Lbl_RowIndex = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TB_AmountIncl_Edit = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.CB_Tax_Edit = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TB_AmountExcl_Edit = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TB_Desc_Edit = New System.Windows.Forms.TextBox()
        Me.CB_Seg3_Edit = New System.Windows.Forms.ComboBox()
        Me.CB_Seg2_Edit = New System.Windows.Forms.ComboBox()
        Me.CB_Seg1_Edit = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.BT_Edit = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.DGV_InvLines = New System.Windows.Forms.DataGridView()
        Me.LineNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Segment1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Segment2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Segment3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Lbl_Total_Excl = New System.Windows.Forms.Label()
        Me.Lbl_Total_VAT = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Lbl_Total_Incl = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.InvoiceBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Btn_UpdatePost = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Btn_VoidInvoice = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SupplierBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DGV_InvLines, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.OriginalPaid)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Lbl_OriginalAmount)
        Me.GroupBox1.Controls.Add(Me.Lbl_SupplierID)
        Me.GroupBox1.Controls.Add(Me.DTP_InvoiceDate)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TB_InvNo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.CB_SupplierName)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Lbl_OrigTxID)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1691, 63)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Invoice Header"
        '
        'OriginalPaid
        '
        Me.OriginalPaid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OriginalPaid.AutoSize = True
        Me.OriginalPaid.Location = New System.Drawing.Point(1397, 15)
        Me.OriginalPaid.Name = "OriginalPaid"
        Me.OriginalPaid.Size = New System.Drawing.Size(84, 17)
        Me.OriginalPaid.TabIndex = 11
        Me.OriginalPaid.Text = "Paid Invoice"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(1502, 32)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 17)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Original Amt"
        '
        'Lbl_OriginalAmount
        '
        Me.Lbl_OriginalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lbl_OriginalAmount.AutoSize = True
        Me.Lbl_OriginalAmount.Location = New System.Drawing.Point(1599, 32)
        Me.Lbl_OriginalAmount.Name = "Lbl_OriginalAmount"
        Me.Lbl_OriginalAmount.Size = New System.Drawing.Size(16, 17)
        Me.Lbl_OriginalAmount.TabIndex = 9
        Me.Lbl_OriginalAmount.Text = "0"
        '
        'Lbl_SupplierID
        '
        Me.Lbl_SupplierID.AutoSize = True
        Me.Lbl_SupplierID.Location = New System.Drawing.Point(335, 18)
        Me.Lbl_SupplierID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_SupplierID.Name = "Lbl_SupplierID"
        Me.Lbl_SupplierID.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_SupplierID.TabIndex = 8
        Me.Lbl_SupplierID.Text = "-"
        Me.Lbl_SupplierID.Visible = False
        '
        'DTP_InvoiceDate
        '
        Me.DTP_InvoiceDate.CustomFormat = "dd MMMM yyyy"
        Me.DTP_InvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTP_InvoiceDate.Location = New System.Drawing.Point(819, 15)
        Me.DTP_InvoiceDate.Margin = New System.Windows.Forms.Padding(4)
        Me.DTP_InvoiceDate.Name = "DTP_InvoiceDate"
        Me.DTP_InvoiceDate.Size = New System.Drawing.Size(204, 22)
        Me.DTP_InvoiceDate.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(720, 23)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Invoice Date"
        '
        'TB_InvNo
        '
        Me.TB_InvNo.Location = New System.Drawing.Point(473, 18)
        Me.TB_InvNo.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_InvNo.Name = "TB_InvNo"
        Me.TB_InvNo.Size = New System.Drawing.Size(236, 22)
        Me.TB_InvNo.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(356, 23)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Invoice Number"
        '
        'CB_SupplierName
        '
        Me.CB_SupplierName.FormattingEnabled = True
        Me.CB_SupplierName.Location = New System.Drawing.Point(113, 17)
        Me.CB_SupplierName.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_SupplierName.Name = "CB_SupplierName"
        Me.CB_SupplierName.Size = New System.Drawing.Size(207, 24)
        Me.CB_SupplierName.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 25)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Supplier Name"
        '
        'Lbl_OrigTxID
        '
        Me.Lbl_OrigTxID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lbl_OrigTxID.AutoSize = True
        Me.Lbl_OrigTxID.Location = New System.Drawing.Point(1599, 15)
        Me.Lbl_OrigTxID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_OrigTxID.Name = "Lbl_OrigTxID"
        Me.Lbl_OrigTxID.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_OrigTxID.TabIndex = 1
        Me.Lbl_OrigTxID.Text = "-"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1502, 15)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Original TxID"
        '
        'SupplierBindingSource
        '
        Me.SupplierBindingSource.DataSource = GetType(MB.Supplier)
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.lbl_TaxSplit)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.TB_TaxAmount)
        Me.GroupBox2.Controls.Add(Me.Lbl_RowIndex)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.TB_AmountIncl_Edit)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.CB_Tax_Edit)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.TB_AmountExcl_Edit)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TB_Desc_Edit)
        Me.GroupBox2.Controls.Add(Me.CB_Seg3_Edit)
        Me.GroupBox2.Controls.Add(Me.CB_Seg2_Edit)
        Me.GroupBox2.Controls.Add(Me.CB_Seg1_Edit)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.BT_Edit)
        Me.GroupBox2.Location = New System.Drawing.Point(17, 85)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(1690, 81)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Line Editor"
        '
        'lbl_TaxSplit
        '
        Me.lbl_TaxSplit.AutoSize = True
        Me.lbl_TaxSplit.BackColor = System.Drawing.Color.YellowGreen
        Me.lbl_TaxSplit.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_TaxSplit.Location = New System.Drawing.Point(1486, 19)
        Me.lbl_TaxSplit.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_TaxSplit.Name = "lbl_TaxSplit"
        Me.lbl_TaxSplit.Size = New System.Drawing.Size(62, 17)
        Me.lbl_TaxSplit.TabIndex = 20
        Me.lbl_TaxSplit.Text = "Tax Split"
        Me.lbl_TaxSplit.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1258, 20)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 17)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Tax Amount"
        '
        'TB_TaxAmount
        '
        Me.TB_TaxAmount.Location = New System.Drawing.Point(1262, 43)
        Me.TB_TaxAmount.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_TaxAmount.Name = "TB_TaxAmount"
        Me.TB_TaxAmount.Size = New System.Drawing.Size(91, 22)
        Me.TB_TaxAmount.TabIndex = 18
        '
        'Lbl_RowIndex
        '
        Me.Lbl_RowIndex.AutoSize = True
        Me.Lbl_RowIndex.Location = New System.Drawing.Point(1617, 21)
        Me.Lbl_RowIndex.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_RowIndex.Name = "Lbl_RowIndex"
        Me.Lbl_RowIndex.Size = New System.Drawing.Size(51, 17)
        Me.Lbl_RowIndex.TabIndex = 17
        Me.Lbl_RowIndex.Text = "Label7"
        Me.Lbl_RowIndex.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(1621, 44)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(70, 28)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "Remove"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(1358, 22)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(81, 17)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Amount Incl"
        '
        'TB_AmountIncl_Edit
        '
        Me.TB_AmountIncl_Edit.Location = New System.Drawing.Point(1361, 44)
        Me.TB_AmountIncl_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_AmountIncl_Edit.Name = "TB_AmountIncl_Edit"
        Me.TB_AmountIncl_Edit.Size = New System.Drawing.Size(120, 22)
        Me.TB_AmountIncl_Edit.TabIndex = 14
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(1142, 19)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(67, 17)
        Me.Label15.TabIndex = 13
        Me.Label15.Text = "Tax Type"
        '
        'CB_Tax_Edit
        '
        Me.CB_Tax_Edit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Tax_Edit.FormattingEnabled = True
        Me.CB_Tax_Edit.Items.AddRange(New Object() {"0", "1"})
        Me.CB_Tax_Edit.Location = New System.Drawing.Point(1146, 43)
        Me.CB_Tax_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Tax_Edit.Name = "CB_Tax_Edit"
        Me.CB_Tax_Edit.Size = New System.Drawing.Size(108, 24)
        Me.CB_Tax_Edit.TabIndex = 12
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(1010, 21)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(85, 17)
        Me.Label14.TabIndex = 11
        Me.Label14.Text = "Amount Excl"
        '
        'TB_AmountExcl_Edit
        '
        Me.TB_AmountExcl_Edit.Location = New System.Drawing.Point(1014, 44)
        Me.TB_AmountExcl_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_AmountExcl_Edit.Name = "TB_AmountExcl_Edit"
        Me.TB_AmountExcl_Edit.Size = New System.Drawing.Size(124, 22)
        Me.TB_AmountExcl_Edit.TabIndex = 10
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(765, 21)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 17)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Segment 3"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(535, 21)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 17)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Segment 2"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(303, 21)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 17)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Segment 1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 23)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 17)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Description"
        '
        'TB_Desc_Edit
        '
        Me.TB_Desc_Edit.Location = New System.Drawing.Point(8, 46)
        Me.TB_Desc_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_Desc_Edit.Name = "TB_Desc_Edit"
        Me.TB_Desc_Edit.Size = New System.Drawing.Size(291, 22)
        Me.TB_Desc_Edit.TabIndex = 5
        '
        'CB_Seg3_Edit
        '
        Me.CB_Seg3_Edit.FormattingEnabled = True
        Me.CB_Seg3_Edit.Location = New System.Drawing.Point(768, 44)
        Me.CB_Seg3_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Seg3_Edit.Name = "CB_Seg3_Edit"
        Me.CB_Seg3_Edit.Size = New System.Drawing.Size(220, 24)
        Me.CB_Seg3_Edit.TabIndex = 4
        '
        'CB_Seg2_Edit
        '
        Me.CB_Seg2_Edit.FormattingEnabled = True
        Me.CB_Seg2_Edit.Location = New System.Drawing.Point(535, 45)
        Me.CB_Seg2_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Seg2_Edit.Name = "CB_Seg2_Edit"
        Me.CB_Seg2_Edit.Size = New System.Drawing.Size(220, 24)
        Me.CB_Seg2_Edit.TabIndex = 3
        '
        'CB_Seg1_Edit
        '
        Me.CB_Seg1_Edit.FormattingEnabled = True
        Me.CB_Seg1_Edit.Location = New System.Drawing.Point(307, 45)
        Me.CB_Seg1_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_Seg1_Edit.Name = "CB_Seg1_Edit"
        Me.CB_Seg1_Edit.Size = New System.Drawing.Size(220, 24)
        Me.CB_Seg1_Edit.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1554, 44)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(61, 28)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Add"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'BT_Edit
        '
        Me.BT_Edit.Location = New System.Drawing.Point(1489, 44)
        Me.BT_Edit.Margin = New System.Windows.Forms.Padding(4)
        Me.BT_Edit.Name = "BT_Edit"
        Me.BT_Edit.Size = New System.Drawing.Size(61, 28)
        Me.BT_Edit.TabIndex = 0
        Me.BT_Edit.Text = "Edit"
        Me.BT_Edit.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.DGV_InvLines)
        Me.GroupBox3.Location = New System.Drawing.Point(17, 175)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(1690, 400)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Lines"
        '
        'DGV_InvLines
        '
        Me.DGV_InvLines.AllowUserToAddRows = False
        Me.DGV_InvLines.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.DGV_InvLines.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_InvLines.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_InvLines.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_InvLines.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGV_InvLines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_InvLines.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LineNo, Me.Description, Me.Segment1, Me.Segment2, Me.Segment3, Me.Amount, Me.TaxType, Me.TaxAmount, Me.Total})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_InvLines.DefaultCellStyle = DataGridViewCellStyle3
        Me.DGV_InvLines.Location = New System.Drawing.Point(4, 19)
        Me.DGV_InvLines.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_InvLines.Name = "DGV_InvLines"
        Me.DGV_InvLines.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_InvLines.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGV_InvLines.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_InvLines.Size = New System.Drawing.Size(1682, 377)
        Me.DGV_InvLines.TabIndex = 0
        '
        'LineNo
        '
        Me.LineNo.FillWeight = 20.0!
        Me.LineNo.HeaderText = "#"
        Me.LineNo.Name = "LineNo"
        Me.LineNo.ReadOnly = True
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        Me.Description.ReadOnly = True
        '
        'Segment1
        '
        Me.Segment1.HeaderText = "Segment1"
        Me.Segment1.Name = "Segment1"
        Me.Segment1.ReadOnly = True
        '
        'Segment2
        '
        Me.Segment2.HeaderText = "Segment2"
        Me.Segment2.Name = "Segment2"
        Me.Segment2.ReadOnly = True
        '
        'Segment3
        '
        Me.Segment3.HeaderText = "Segment3"
        Me.Segment3.Name = "Segment3"
        Me.Segment3.ReadOnly = True
        '
        'Amount
        '
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        Me.Amount.ReadOnly = True
        '
        'TaxType
        '
        Me.TaxType.HeaderText = "TaxType"
        Me.TaxType.Name = "TaxType"
        Me.TaxType.ReadOnly = True
        '
        'TaxAmount
        '
        Me.TaxAmount.HeaderText = "TaxAmount"
        Me.TaxAmount.Name = "TaxAmount"
        Me.TaxAmount.ReadOnly = True
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1489, 579)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 17)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Total Excl"
        '
        'Lbl_Total_Excl
        '
        Me.Lbl_Total_Excl.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lbl_Total_Excl.ImageAlign = System.Drawing.ContentAlignment.BottomRight
        Me.Lbl_Total_Excl.Location = New System.Drawing.Point(1566, 579)
        Me.Lbl_Total_Excl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_Total_Excl.Name = "Lbl_Total_Excl"
        Me.Lbl_Total_Excl.Size = New System.Drawing.Size(133, 22)
        Me.Lbl_Total_Excl.TabIndex = 4
        Me.Lbl_Total_Excl.Text = "0"
        Me.Lbl_Total_Excl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Lbl_Total_VAT
        '
        Me.Lbl_Total_VAT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lbl_Total_VAT.Location = New System.Drawing.Point(1564, 601)
        Me.Lbl_Total_VAT.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_Total_VAT.Name = "Lbl_Total_VAT"
        Me.Lbl_Total_VAT.Size = New System.Drawing.Size(135, 15)
        Me.Lbl_Total_VAT.TabIndex = 6
        Me.Lbl_Total_VAT.Text = "0"
        Me.Lbl_Total_VAT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(1489, 600)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 17)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Total Tax"
        '
        'Lbl_Total_Incl
        '
        Me.Lbl_Total_Incl.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lbl_Total_Incl.Location = New System.Drawing.Point(1562, 623)
        Me.Lbl_Total_Incl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_Total_Incl.Name = "Lbl_Total_Incl"
        Me.Lbl_Total_Incl.Size = New System.Drawing.Size(138, 12)
        Me.Lbl_Total_Incl.TabIndex = 8
        Me.Lbl_Total_Incl.Text = "0"
        Me.Lbl_Total_Incl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(1489, 623)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 17)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Total Incl"
        '
        'Btn_UpdatePost
        '
        Me.Btn_UpdatePost.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Btn_UpdatePost.Location = New System.Drawing.Point(17, 583)
        Me.Btn_UpdatePost.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_UpdatePost.Name = "Btn_UpdatePost"
        Me.Btn_UpdatePost.Size = New System.Drawing.Size(173, 53)
        Me.Btn_UpdatePost.TabIndex = 9
        Me.Btn_UpdatePost.Tag = "S_Allow_Invoice_Post"
        Me.Btn_UpdatePost.Text = "Post Updated Invoice"
        Me.Btn_UpdatePost.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 668)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1716, 25)
        Me.StatusStrip1.TabIndex = 10
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(15, 20)
        Me.ToolStripStatusLabel1.Text = "-"
        '
        'Btn_VoidInvoice
        '
        Me.Btn_VoidInvoice.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Btn_VoidInvoice.Location = New System.Drawing.Point(198, 583)
        Me.Btn_VoidInvoice.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_VoidInvoice.Name = "Btn_VoidInvoice"
        Me.Btn_VoidInvoice.Size = New System.Drawing.Size(173, 53)
        Me.Btn_VoidInvoice.TabIndex = 11
        Me.Btn_VoidInvoice.Tag = "S_Allow_Invoice_Void"
        Me.Btn_VoidInvoice.Text = "Void Invoice"
        Me.Btn_VoidInvoice.UseVisualStyleBackColor = True
        '
        'frmInvoiceEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(1716, 693)
        Me.Controls.Add(Me.Btn_VoidInvoice)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Btn_UpdatePost)
        Me.Controls.Add(Me.Lbl_Total_Incl)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Lbl_Total_VAT)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Lbl_Total_Excl)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmInvoiceEdit"
        Me.Tag = "S_Invoice_Edit"
        Me.Text = "frmInvoiceEdit"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.SupplierBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.DGV_InvLines, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DTP_InvoiceDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TB_InvNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CB_SupplierName As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Lbl_OrigTxID As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DGV_InvLines As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Lbl_Total_Excl As System.Windows.Forms.Label
    Friend WithEvents Lbl_Total_VAT As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Lbl_Total_Incl As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents SupplierBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvoiceBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TB_Desc_Edit As System.Windows.Forms.TextBox
    Friend WithEvents CB_Seg3_Edit As System.Windows.Forms.ComboBox
    Friend WithEvents CB_Seg2_Edit As System.Windows.Forms.ComboBox
    Friend WithEvents CB_Seg1_Edit As System.Windows.Forms.ComboBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents BT_Edit As System.Windows.Forms.Button
    Friend WithEvents Lbl_SupplierID As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TB_AmountIncl_Edit As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents CB_Tax_Edit As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TB_AmountExcl_Edit As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Lbl_RowIndex As System.Windows.Forms.Label
    Friend WithEvents Btn_UpdatePost As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TB_TaxAmount As System.Windows.Forms.TextBox
    Friend WithEvents lbl_TaxSplit As System.Windows.Forms.Label
    Friend WithEvents LineNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Segment1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Segment2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Segment3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lbl_OriginalAmount As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents OriginalPaid As System.Windows.Forms.Label
    Friend WithEvents Btn_VoidInvoice As Windows.Forms.Button
End Class
