﻿
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Windows.Forms


Public Class ThisAddIn
    Private OrderX As Microsoft.Office.Tools.CustomTaskPane
    Private Sub ThisAddIn_Startup() Handles Me.Startup
        Try
            Dim CurrenyDec As String
            CurrenyDec = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator.ToString
            If CurrenyDec <> "." Then
                Globals.Ribbons.Ribbon1.GrpAbout.Visible = False
                MsgBox("Magic Box requires the correct formatting of the currency and number decimal separator. Please update this in regional setting and restart the application.")
                Exit Sub
            End If
            Dim NumDec As String
            NumDec = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString
            If CurrenyDec <> "." Then
                Globals.Ribbons.Ribbon1.GrpAbout.Visible = False
                MsgBox("Magic Box requires the correct formatting of the currency and number decimal separator. Please update this in regional setting and restart the application.")
                Exit Sub
            End If

            Exit Sub
            My.Settings.Setting_CompanyID = ""
            My.Settings.Start_Up_Activation_Mode = "Activate"

            If My.Settings.CS_Setting <> "" Or My.Settings.Setting_CompanyID <> "" Then

                'Set button to inactive on load
                If My.Settings.Start_Up_Activation_Mode = "Activate" Then
                    '     Call oStartUp()
                    Globals.Ribbons.Ribbon1.btnOnOff.Label = "Active"
                    Globals.Ribbons.Ribbon1.btnOnOff.Image = My.Resources.On2



                    '   If My.Settings.Auto_Approve_CreditNotes = "" _
                    '       Or My.Settings.Auto_Approve_EFT = "" Then
                    '   MessageBox.Show("The system has detected that you haven't set some key settings for your daily processing, Please set at a minimun :" & vbCrLf & vbCrLf & _
                    '                   "1. 'Credit notes approving method'" & vbCrLf & "2. 'EFT payment approval method'", "Approvals", MessageBoxButtons.OK)
                    '   Dim fSettings As New frmSettings
                    '   fSettings.Label10.ForeColor = Drawing.Color.Red
                    '   fSettings.Label9.ForeColor = Drawing.Color.Red
                    '  fSettings.Label11.ForeColor = Drawing.Color.Red
                    '  fSettings.Label13.ForeColor = Drawing.Color.Red
                    ''  fSettings.ShowDialog()
                    ' End If

                Else
                    Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive"
                    Globals.Ribbons.Ribbon1.btnOnOff.Image = My.Resources.Off2
                End If
                'Rather Us mod_Application_Startup
                Exit Sub

                If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                'Load checks governs the InvoiceHeader and InvoiceLines table checks
                If My.Settings.Load_Checks = "No" Then
                    'do nothing
                Else
                    Dim oNumberRowsInInvoiceHeader = Count_Lines_In_Invoice_Header_Table()
                    If oNumberRowsInInvoiceHeader = "Error" Or oNumberRowsInInvoiceHeader = "0" Then
                        'Do nothing
                    Else
                        If IsNumeric(oNumberRowsInInvoiceHeader) = True Then
                            '  Form_ImportExport.Show()
                            '  Form_ImportExport.BringToFront()
                            '  Call Form_ImportExport.oCheckAll()
                        End If
                    End If
                End If


                '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                If My.Settings.Load_Checks = "No" Then
                    'do nothing
                Else

                    If My.Settings.CS_Setting = "" Or My.Settings.CS_Setting = Nothing Then
                        MsgBox("No database path or connection string has been set up. Please do this now in 'Settings'.")
                        Beep()
                    End If


                    Dim oHighestTransactionID_in_Transactions As Long = oGetHighestTransactionID_in_Transactions()

                    If oHighestTransactionID_in_Transactions = 0 Then Exit Sub

                    Dim oHighestID_in_TransactionID_Table As Long = oGetHighestID_in_TransactionID()
                    If oHighestTransactionID_in_Transactions > oHighestID_in_TransactionID_Table Then
                        If My.Settings.DBType = "Access" Then
                            Insert_New_ID_in_TransactionID(oHighestTransactionID_in_Transactions + 100)
                        ElseIf My.Settings.DBType = "SQL" Then
                            Drop_Transaction_ID_Table()
                            Create_New_Transaction_ID_Table(oHighestTransactionID_in_Transactions + 100)
                        End If
                    End If
                    Dim oHighestBatchID_in_Transactions As Long = oGetHighestBatchID_in_Transactions()
                    Dim oHighestID_in_BatchID_Table As Long = oGetHighestID_in_BatchID()
                    If oHighestBatchID_in_Transactions > oHighestID_in_BatchID_Table Then
                        If My.Settings.DBType = "Access" Then
                            Insert_New_ID_in_BatchID(oHighestBatchID_in_Transactions + 100)
                        ElseIf My.Settings.DBType = "SQL" Then
                            Drop_Batch_ID_Table()
                            Create_New_Batch_ID_Table(oHighestBatchID_in_Transactions + 100)
                        End If
                    End If
                End If
            Else
                '    MessageBox.Show("Pre-loading processes where ignored because there is no database connection details/default store set, set this in settings screen", "Pre-Loading", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message & ", please make sure that the database connection details are set correctly", "Pre-Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ThisAddIn_Shutdown() Handles Me.Shutdown

        'If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Active" Then

        '    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '    If My.Settings.Auto_Approve_CreditNotes = Nothing Or My.Settings.Auto_Approve_CreditNotes = "" Then
        '        My.Settings.Auto_Approve_CreditNotes = "Yes"
        '        My.Settings.Save()
        '    End If

        '    If My.Settings.Auto_Approve_CreditNotes = "Yes" Then
        '        Call Approve_All_Credit_Notes_set_Accounting_to_true_for_all_transactions_in_Step6_CreditNotes()
        '    End If

        '    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '    If My.Settings.Auto_Approve_EFT = Nothing Or My.Settings.Auto_Approve_EFT = "" Then
        '        My.Settings.Auto_Approve_EFT = "Yes"
        '        My.Settings.Save()
        '    End If
        '    If My.Settings.Auto_Approve_EFT = "Yes" Then
        '        Call Approve_EFTs_set_Accounting_to_true_for_all_transactions_in_Step7_EFT()
        '    End If
        'End If
        'dsfsdg
        RemoveOrderXTaskPane()
    End Sub

    Sub Create_New_Database()
        'Dim conStr As String = "Server=DIMENSION9100;Database=;Trusted_Connection = yes"
        'Dim objCon As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")(conStr)
        'Dim obj As SqlCommand
        'Dim strSQL As String
        'obj = objConn.CreateCommand()
        'strSQL = "CREATE DATABASE " & DatabaseName8.' Execute9.obj.CommandText = strSQL
        'obj.ExecuteNonQuery()
    End Sub
    Sub Drop_Transaction_ID_Table()
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            cn.Open()
            Dim sSQL As String
            sSQL = "DROP TABLE TransactionID"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            If cn.State = ConnectionState.Open Then
                cn.Dispose()
            End If
            MsgBox(ex.Message & "002")
        End Try
    End Sub
    Sub Create_New_Transaction_ID_Table(ByVal oIdentitySeed As String)
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            cn.Open()
            Dim sSQL As String
            sSQL = "CREATE TABLE TransactionID "
            sSQL = sSQL & "([ID] bigint identity(" & oIdentitySeed & ",1) not null primary key, [oUser] nvarchar(50), [oDate] nvarchar(50), [oCode] nvarchar(50))"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            If cn.State = ConnectionState.Open Then
                cn.Dispose()
            End If
            MsgBox(ex.Message & " 003")
        End Try
    End Sub

    Sub Drop_Batch_ID_Table()
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try

            cn.Open()
            Dim sSQL As String
            sSQL = "DROP TABLE BatchID"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            If cn.State = ConnectionState.Open Then
                cn.Dispose()
            End If
            MsgBox(ex.Message & " 004")
        End Try
    End Sub
    Sub Create_New_Batch_ID_Table(ByVal oIdentitySeed As String)
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            cn.Open()
            Dim sSQL As String
            sSQL = "CREATE TABLE BatchID "
            sSQL = sSQL & "([ID] bigint identity(" & oIdentitySeed & ",1) not null primary key, [oUser] nvarchar(50), [oDate] nvarchar(50), [oCode] nvarchar(50))"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            If cn.State = ConnectionState.Open Then
                cn.Dispose()
            End If
            MsgBox(ex.Message & " 005")
        End Try
    End Sub
    Public Sub Insert_New_ID_in_TransactionID(ByVal oNewAutoNumber As Long)
        Dim sSQL As String
        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Try
            sSQL = "ALTER TABLE TransactionID ALTER COLUMN ID Counter(" & oNewAutoNumber & ",1)"


            Dim cmd As New OleDbCommand(sSQL, cn)
            cn.Open()
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            If cn.State = ConnectionState.Open Then
                cn.Dispose()
            End If
            MsgBox("There was a problem setting a new ID in TransactionID " & ex.Message & " 006")
        End Try
    End Sub
    Public Sub Insert_New_ID_in_BatchID(ByVal oNewAutoNumber As Long)
        Dim sSQL As String
        Dim cn1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            sSQL = "ALTER TABLE BatchID ALTER COLUMN ID Counter(" & oNewAutoNumber & ",1)"

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim cmd As New SqlCommand(sSQL, cn1)
                cmd.CommandTimeout = 300
                cn1.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            If cn1.State = ConnectionState.Open Then
                cn1.Dispose()
            End If
            MsgBox("There was a problem setting a new ID in BatchID " & ex.Message & " 007")
        End Try
    End Sub
    Public Function oGetHighestTransactionID_in_Transactions() As Long
        Dim connection1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try

            Dim oResult As Object
            Dim sSQL As String
            sSQL = "Select Max(TransactionID) From Transactions"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestTransactionID_in_Transactions = 0
                Else
                    oGetHighestTransactionID_in_Transactions = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim cmd As New SqlCommand(sSQL, connection1)
                cmd.CommandTimeout = 300
                connection1.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestTransactionID_in_Transactions = 0
                Else
                    oGetHighestTransactionID_in_Transactions = oResult
                End If
                connection1.Close()
            End If
        Catch ex As Exception
            If connection1.State = ConnectionState.Open Then
                connection1.Dispose()
            End If
            MsgBox("There was an error looking up the highest TransactionID in Transactions! " & ex.Message & " 008")
        End Try
    End Function
    Public Function oGetHighestID_in_TransactionID() As Long
        Dim connection1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            Dim sSQL As String
            sSQL = "Select Max(ID) From TransactionID"
            Dim oResult As Object

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_TransactionID = 0
                Else
                    oGetHighestID_in_TransactionID = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cmd As New SqlCommand(sSQL, connection1)
                cmd.CommandTimeout = 300
                connection1.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_TransactionID = 0
                Else
                    oGetHighestID_in_TransactionID = oResult
                End If
                connection1.Close()
            End If
        Catch ex As Exception
            If connection1.State = ConnectionState.Open Then
                connection1.Dispose()
            End If
            MsgBox("There was an error getting the highest ID from the TRansactionID table! " & ex.Message & " 009")
        End Try
    End Function
    Public Function oGetHighestBatchID_in_Transactions() As Long
        Dim connection1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            Dim oResult As Object
            Dim sSQL As String
            sSQL = "Select Max(BatchID) From Transactions"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestBatchID_in_Transactions = 0
                Else
                    oGetHighestBatchID_in_Transactions = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim cmd As New SqlCommand(sSQL, connection1)
                cmd.CommandTimeout = 300
                connection1.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestBatchID_in_Transactions = 0
                Else
                    oGetHighestBatchID_in_Transactions = oResult
                End If
                connection1.Close()
            End If
        Catch ex As Exception
            If connection1.State = ConnectionState.Open Then
                connection1.Dispose()
            End If
            MsgBox("There was an error looking up the highest BatchID in Transactions! " & ex.Message & " 010")
        End Try
    End Function



    Public Function oGetHighestID_in_BatchID() As Long
        Dim connection1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            Dim oResult As Object
            Dim sSQL As String
            sSQL = "Select Max(ID) From BatchID"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_BatchID = 0
                Else
                    oGetHighestID_in_BatchID = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim cmd As New SqlCommand(sSQL, connection1)
                cmd.CommandTimeout = 300
                connection1.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_BatchID = 0
                Else
                    oGetHighestID_in_BatchID = oResult
                End If
                connection1.Close()
            End If

        Catch ex As Exception
            If connection1.State = ConnectionState.Open Then
                connection1.Dispose()
            End If
            MsgBox("There was an error getting the highest ID from the BatchID table! " & ex.Message & " 011")
        End Try
    End Function
    Public Sub AddOrderXTaskPane()
        OrderX = Me.CustomTaskPanes.Add(New UC_OrderX(), "OrderX")
        OrderX.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionLeft
        OrderX.Visible = True
    End Sub
    Public Sub RemoveOrderXTaskPane()
        '   Me.CustomTaskPanes.Remove(OrderX)
    End Sub
End Class
