﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettings))
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSaveDatabaseType = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.lblInvalidConnectionString = New System.Windows.Forms.Label()
        Me.lblLoad = New System.Windows.Forms.Label()
        Me.btnUseConnectionString = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ListViewCS = New System.Windows.Forms.ListView()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.cmbStartUp_Activation_Mode = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnSave_StartUp_Activation_Mode = New System.Windows.Forms.Button()
        Me.cmbCompany = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BtnExit = New System.Windows.Forms.Button()
        Me.Tab_Setting = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblIsFuturedate = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnDisable6 = New System.Windows.Forms.Button()
        Me.btnEnable6 = New System.Windows.Forms.Button()
        Me.lblPayInAdvance = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnDisable5 = New System.Windows.Forms.Button()
        Me.btnEnable5 = New System.Windows.Forms.Button()
        Me.lblAutoApproveEFTs = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnDisable4 = New System.Windows.Forms.Button()
        Me.btnEnable4 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.lblAutoApproveCreditNotes = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnEnable3 = New System.Windows.Forms.Button()
        Me.btnDisable3 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.ckbtoDefault = New System.Windows.Forms.CheckBox()
        Me.ckbToAll = New System.Windows.Forms.CheckBox()
        Me.txtDefaultEmail = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnSaveChanges = New System.Windows.Forms.Button()
        Me.TAB_Alchemex = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btn_TaxUpdate = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.CB_Tx_Default = New System.Windows.Forms.CheckBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TB_TaxPerc = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TB_TaxDesc = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lbl_TaxID = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Btn_LoadTax = New System.Windows.Forms.Button()
        Me.Btn_removeTax = New System.Windows.Forms.Button()
        Me.Btn_AddTax = New System.Windows.Forms.Button()
        Me.DGV_Tax = New System.Windows.Forms.DataGridView()
        Me.Tab_Periods = New System.Windows.Forms.TabPage()
        Me.Btn_PeriodRemove = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btn_Edit = New System.Windows.Forms.Button()
        Me.btn_AddNewPeriod = New System.Windows.Forms.Button()
        Me.PeriodID = New System.Windows.Forms.Label()
        Me.CB_Closed = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Cmb_FinYear = New System.Windows.Forms.ComboBox()
        Me.TB_EndDate = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TB_StartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TB_Period = New System.Windows.Forms.TextBox()
        Me.DGV_Periods = New System.Windows.Forms.DataGridView()
        Me.Tab_Update = New System.Windows.Forms.TabPage()
        Me.Btn_UpdateCheck = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Tab_Setting.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TAB_Alchemex.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.DGV_Tax, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tab_Periods.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.DGV_Periods, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tab_Update.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(7, 63)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(271, 28)
        Me.Button4.TabIndex = 77
        Me.Button4.Text = "Save Metadata Folder"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(7, 33)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(689, 22)
        Me.TextBox2.TabIndex = 76
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(706, 29)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(103, 28)
        Me.Button5.TabIndex = 75
        Me.Button5.Text = "Browse"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"SQL"})
        Me.ComboBox1.Location = New System.Drawing.Point(33, 55)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(256, 24)
        Me.ComboBox1.TabIndex = 78
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 17)
        Me.Label1.TabIndex = 79
        Me.Label1.Text = "Database type"
        '
        'btnSaveDatabaseType
        '
        Me.btnSaveDatabaseType.Location = New System.Drawing.Point(128, 87)
        Me.btnSaveDatabaseType.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSaveDatabaseType.Name = "btnSaveDatabaseType"
        Me.btnSaveDatabaseType.Size = New System.Drawing.Size(163, 28)
        Me.btnSaveDatabaseType.TabIndex = 80
        Me.btnSaveDatabaseType.Text = "Save Database Type"
        Me.btnSaveDatabaseType.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(11, 97)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 17)
        Me.Label8.TabIndex = 106
        Me.Label8.Text = "Company :"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(171, 126)
        Me.Button8.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(143, 28)
        Me.Button8.TabIndex = 107
        Me.Button8.Text = "&Save Company"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'lblInvalidConnectionString
        '
        Me.lblInvalidConnectionString.AutoSize = True
        Me.lblInvalidConnectionString.ForeColor = System.Drawing.Color.Red
        Me.lblInvalidConnectionString.Location = New System.Drawing.Point(59, 239)
        Me.lblInvalidConnectionString.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblInvalidConnectionString.Name = "lblInvalidConnectionString"
        Me.lblInvalidConnectionString.Size = New System.Drawing.Size(0, 17)
        Me.lblInvalidConnectionString.TabIndex = 113
        '
        'lblLoad
        '
        Me.lblLoad.AutoSize = True
        Me.lblLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoad.ForeColor = System.Drawing.Color.Maroon
        Me.lblLoad.Location = New System.Drawing.Point(8, 161)
        Me.lblLoad.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLoad.Name = "lblLoad"
        Me.lblLoad.Size = New System.Drawing.Size(12, 16)
        Me.lblLoad.TabIndex = 115
        Me.lblLoad.Text = "-"
        '
        'btnUseConnectionString
        '
        Me.btnUseConnectionString.Location = New System.Drawing.Point(131, 155)
        Me.btnUseConnectionString.Margin = New System.Windows.Forms.Padding(4)
        Me.btnUseConnectionString.Name = "btnUseConnectionString"
        Me.btnUseConnectionString.Size = New System.Drawing.Size(105, 28)
        Me.btnUseConnectionString.TabIndex = 116
        Me.btnUseConnectionString.Text = "Use Selected"
        Me.btnUseConnectionString.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ListViewCS)
        Me.GroupBox1.Controls.Add(Me.btnLoad)
        Me.GroupBox1.Controls.Add(Me.lblLoad)
        Me.GroupBox1.Controls.Add(Me.btnUseConnectionString)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.GroupBox1.Location = New System.Drawing.Point(635, 463)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(409, 191)
        Me.GroupBox1.TabIndex = 118
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Multiple Connection String Manager"
        Me.GroupBox1.Visible = False
        '
        'ListViewCS
        '
        Me.ListViewCS.ForeColor = System.Drawing.Color.SteelBlue
        Me.ListViewCS.FullRowSelect = True
        Me.ListViewCS.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.ListViewCS.HideSelection = False
        Me.ListViewCS.Location = New System.Drawing.Point(12, 22)
        Me.ListViewCS.Margin = New System.Windows.Forms.Padding(4)
        Me.ListViewCS.Name = "ListViewCS"
        Me.ListViewCS.Size = New System.Drawing.Size(387, 131)
        Me.ListViewCS.TabIndex = 118
        Me.ListViewCS.UseCompatibleStateImageBehavior = False
        Me.ListViewCS.View = System.Windows.Forms.View.Details
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(240, 155)
        Me.btnLoad.Margin = New System.Windows.Forms.Padding(4)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(160, 28)
        Me.btnLoad.TabIndex = 117
        Me.btnLoad.Text = "Manage Connections"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'cmbStartUp_Activation_Mode
        '
        Me.cmbStartUp_Activation_Mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStartUp_Activation_Mode.FormattingEnabled = True
        Me.cmbStartUp_Activation_Mode.Items.AddRange(New Object() {"Activate", "Don't Activate"})
        Me.cmbStartUp_Activation_Mode.Location = New System.Drawing.Point(131, 20)
        Me.cmbStartUp_Activation_Mode.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbStartUp_Activation_Mode.Name = "cmbStartUp_Activation_Mode"
        Me.cmbStartUp_Activation_Mode.Size = New System.Drawing.Size(183, 24)
        Me.cmbStartUp_Activation_Mode.TabIndex = 125
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(4, 23)
        Me.Label26.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(116, 17)
        Me.Label26.TabIndex = 126
        Me.Label26.Text = "Activation Mode :"
        '
        'btnSave_StartUp_Activation_Mode
        '
        Me.btnSave_StartUp_Activation_Mode.Location = New System.Drawing.Point(177, 53)
        Me.btnSave_StartUp_Activation_Mode.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnSave_StartUp_Activation_Mode.Name = "btnSave_StartUp_Activation_Mode"
        Me.btnSave_StartUp_Activation_Mode.Size = New System.Drawing.Size(137, 28)
        Me.btnSave_StartUp_Activation_Mode.TabIndex = 127
        Me.btnSave_StartUp_Activation_Mode.Text = "&Save Mode"
        Me.btnSave_StartUp_Activation_Mode.UseVisualStyleBackColor = True
        '
        'cmbCompany
        '
        Me.cmbCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompany.FormattingEnabled = True
        Me.cmbCompany.Location = New System.Drawing.Point(93, 94)
        Me.cmbCompany.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbCompany.Name = "cmbCompany"
        Me.cmbCompany.Size = New System.Drawing.Size(219, 24)
        Me.cmbCompany.TabIndex = 128
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbCompany)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.btnSave_StartUp_Activation_Mode)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Button8)
        Me.GroupBox2.Controls.Add(Me.cmbStartUp_Activation_Mode)
        Me.GroupBox2.Location = New System.Drawing.Point(306, 485)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(321, 191)
        Me.GroupBox2.TabIndex = 129
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Current workspace Defaults"
        Me.GroupBox2.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(5, 449)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(333, 28)
        Me.Button1.TabIndex = 129
        Me.Button1.Text = "Restore Prior Settings"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ComboBox1)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.btnSaveDatabaseType)
        Me.GroupBox3.Location = New System.Drawing.Point(55, 551)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(333, 119)
        Me.GroupBox3.TabIndex = 130
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Database Settings"
        Me.GroupBox3.Visible = False
        '
        'BtnExit
        '
        Me.BtnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnExit.Location = New System.Drawing.Point(308, 415)
        Me.BtnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnExit.Name = "BtnExit"
        Me.BtnExit.Size = New System.Drawing.Size(119, 28)
        Me.BtnExit.TabIndex = 131
        Me.BtnExit.Text = "&Exit"
        Me.BtnExit.UseVisualStyleBackColor = True
        '
        'Tab_Setting
        '
        Me.Tab_Setting.Controls.Add(Me.TabPage1)
        Me.Tab_Setting.Controls.Add(Me.TabPage2)
        Me.Tab_Setting.Controls.Add(Me.TAB_Alchemex)
        Me.Tab_Setting.Controls.Add(Me.TabPage3)
        Me.Tab_Setting.Controls.Add(Me.Tab_Periods)
        Me.Tab_Setting.Controls.Add(Me.Tab_Update)
        Me.Tab_Setting.Location = New System.Drawing.Point(5, 2)
        Me.Tab_Setting.Margin = New System.Windows.Forms.Padding(4)
        Me.Tab_Setting.Name = "Tab_Setting"
        Me.Tab_Setting.SelectedIndex = 0
        Me.Tab_Setting.Size = New System.Drawing.Size(827, 406)
        Me.Tab_Setting.TabIndex = 132
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lblIsFuturedate)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.btnDisable6)
        Me.TabPage1.Controls.Add(Me.btnEnable6)
        Me.TabPage1.Controls.Add(Me.lblPayInAdvance)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.btnDisable5)
        Me.TabPage1.Controls.Add(Me.btnEnable5)
        Me.TabPage1.Controls.Add(Me.lblAutoApproveEFTs)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.btnDisable4)
        Me.TabPage1.Controls.Add(Me.btnEnable4)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.lblAutoApproveCreditNotes)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.btnEnable3)
        Me.TabPage1.Controls.Add(Me.btnDisable3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage1.Size = New System.Drawing.Size(819, 377)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'lblIsFuturedate
        '
        Me.lblIsFuturedate.AutoSize = True
        Me.lblIsFuturedate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblIsFuturedate.Location = New System.Drawing.Point(203, 115)
        Me.lblIsFuturedate.Name = "lblIsFuturedate"
        Me.lblIsFuturedate.Size = New System.Drawing.Size(13, 17)
        Me.lblIsFuturedate.TabIndex = 138
        Me.lblIsFuturedate.Tag = ""
        Me.lblIsFuturedate.Text = "-"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Blue
        Me.Label15.Location = New System.Drawing.Point(497, 115)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(326, 17)
        Me.Label15.TabIndex = 137
        Me.Label15.Text = "(Allows capturing of future dated supplier Invoices)"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.Blue
        Me.Label17.Location = New System.Drawing.Point(13, 115)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(147, 17)
        Me.Label17.TabIndex = 136
        Me.Label17.Text = "Capture future Invoice"
        '
        'btnDisable6
        '
        Me.btnDisable6.Location = New System.Drawing.Point(396, 108)
        Me.btnDisable6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnDisable6.Name = "btnDisable6"
        Me.btnDisable6.Size = New System.Drawing.Size(96, 28)
        Me.btnDisable6.TabIndex = 135
        Me.btnDisable6.Text = "Disable"
        Me.btnDisable6.UseVisualStyleBackColor = True
        '
        'btnEnable6
        '
        Me.btnEnable6.Location = New System.Drawing.Point(295, 108)
        Me.btnEnable6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEnable6.Name = "btnEnable6"
        Me.btnEnable6.Size = New System.Drawing.Size(96, 28)
        Me.btnEnable6.TabIndex = 134
        Me.btnEnable6.Text = "Enable"
        Me.btnEnable6.UseVisualStyleBackColor = True
        '
        'lblPayInAdvance
        '
        Me.lblPayInAdvance.AutoSize = True
        Me.lblPayInAdvance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblPayInAdvance.Location = New System.Drawing.Point(203, 81)
        Me.lblPayInAdvance.Name = "lblPayInAdvance"
        Me.lblPayInAdvance.Size = New System.Drawing.Size(13, 17)
        Me.lblPayInAdvance.TabIndex = 133
        Me.lblPayInAdvance.Tag = ""
        Me.lblPayInAdvance.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Blue
        Me.Label4.Location = New System.Drawing.Point(497, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(316, 17)
        Me.Label4.TabIndex = 132
        Me.Label4.Text = "(Allows making payment in advance to Suppliers)"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.Blue
        Me.Label12.Location = New System.Drawing.Point(13, 81)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(106, 17)
        Me.Label12.TabIndex = 131
        Me.Label12.Text = "Pay in Advance"
        '
        'btnDisable5
        '
        Me.btnDisable5.Location = New System.Drawing.Point(396, 75)
        Me.btnDisable5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnDisable5.Name = "btnDisable5"
        Me.btnDisable5.Size = New System.Drawing.Size(96, 28)
        Me.btnDisable5.TabIndex = 130
        Me.btnDisable5.Text = "Disable"
        Me.btnDisable5.UseVisualStyleBackColor = True
        '
        'btnEnable5
        '
        Me.btnEnable5.Location = New System.Drawing.Point(295, 75)
        Me.btnEnable5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEnable5.Name = "btnEnable5"
        Me.btnEnable5.Size = New System.Drawing.Size(96, 28)
        Me.btnEnable5.TabIndex = 129
        Me.btnEnable5.Text = "Enable"
        Me.btnEnable5.UseVisualStyleBackColor = True
        '
        'lblAutoApproveEFTs
        '
        Me.lblAutoApproveEFTs.AutoSize = True
        Me.lblAutoApproveEFTs.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblAutoApproveEFTs.Location = New System.Drawing.Point(203, 48)
        Me.lblAutoApproveEFTs.Name = "lblAutoApproveEFTs"
        Me.lblAutoApproveEFTs.Size = New System.Drawing.Size(13, 17)
        Me.lblAutoApproveEFTs.TabIndex = 104
        Me.lblAutoApproveEFTs.Tag = ""
        Me.lblAutoApproveEFTs.Text = "-"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.Blue
        Me.Label9.Location = New System.Drawing.Point(497, 48)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(277, 17)
        Me.Label9.TabIndex = 103
        Me.Label9.Text = "(automatically approve EFTs on shutdown)"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Blue
        Me.Label10.Location = New System.Drawing.Point(13, 48)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(131, 17)
        Me.Label10.TabIndex = 102
        Me.Label10.Text = "Auto-approve EFTs"
        '
        'btnDisable4
        '
        Me.btnDisable4.Location = New System.Drawing.Point(396, 42)
        Me.btnDisable4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnDisable4.Name = "btnDisable4"
        Me.btnDisable4.Size = New System.Drawing.Size(96, 28)
        Me.btnDisable4.TabIndex = 101
        Me.btnDisable4.Text = "Disable"
        Me.btnDisable4.UseVisualStyleBackColor = True
        '
        'btnEnable4
        '
        Me.btnEnable4.Location = New System.Drawing.Point(295, 42)
        Me.btnEnable4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEnable4.Name = "btnEnable4"
        Me.btnEnable4.Size = New System.Drawing.Size(96, 28)
        Me.btnEnable4.TabIndex = 100
        Me.btnEnable4.Text = "Enable"
        Me.btnEnable4.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Blue
        Me.Label13.Location = New System.Drawing.Point(13, 17)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(176, 17)
        Me.Label13.TabIndex = 97
        Me.Label13.Text = "Auto-approve Credit Notes"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.Blue
        Me.Label11.Location = New System.Drawing.Point(497, 17)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(318, 17)
        Me.Label11.TabIndex = 99
        Me.Label11.Text = "(automatically approve credit notes on shutdown)"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(859, 186)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(129, 28)
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.Visible = False
        '
        'lblAutoApproveCreditNotes
        '
        Me.lblAutoApproveCreditNotes.AutoSize = True
        Me.lblAutoApproveCreditNotes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblAutoApproveCreditNotes.Location = New System.Drawing.Point(203, 17)
        Me.lblAutoApproveCreditNotes.Name = "lblAutoApproveCreditNotes"
        Me.lblAutoApproveCreditNotes.Size = New System.Drawing.Size(13, 17)
        Me.lblAutoApproveCreditNotes.TabIndex = 98
        Me.lblAutoApproveCreditNotes.Tag = ""
        Me.lblAutoApproveCreditNotes.Text = "-"
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button3.Location = New System.Drawing.Point(1013, 171)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(37, 28)
        Me.Button3.TabIndex = 74
        Me.Button3.Text = "SAVE"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'btnEnable3
        '
        Me.btnEnable3.Location = New System.Drawing.Point(295, 11)
        Me.btnEnable3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEnable3.Name = "btnEnable3"
        Me.btnEnable3.Size = New System.Drawing.Size(96, 28)
        Me.btnEnable3.TabIndex = 95
        Me.btnEnable3.Text = "Enable"
        Me.btnEnable3.UseVisualStyleBackColor = True
        '
        'btnDisable3
        '
        Me.btnDisable3.Location = New System.Drawing.Point(396, 11)
        Me.btnDisable3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnDisable3.Name = "btnDisable3"
        Me.btnDisable3.Size = New System.Drawing.Size(96, 28)
        Me.btnDisable3.TabIndex = 96
        Me.btnDisable3.Text = "Disable"
        Me.btnDisable3.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage2.Size = New System.Drawing.Size(819, 377)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Email"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Controls.Add(Me.txtDefaultEmail)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.btnSaveChanges)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 7)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Size = New System.Drawing.Size(1053, 297)
        Me.GroupBox4.TabIndex = 143
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Emailing Settings"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.ckbtoDefault)
        Me.GroupBox5.Controls.Add(Me.ckbToAll)
        Me.GroupBox5.Location = New System.Drawing.Point(172, 49)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Size = New System.Drawing.Size(329, 44)
        Me.GroupBox5.TabIndex = 145
        Me.GroupBox5.TabStop = False
        '
        'ckbtoDefault
        '
        Me.ckbtoDefault.AutoSize = True
        Me.ckbtoDefault.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ckbtoDefault.Location = New System.Drawing.Point(29, 16)
        Me.ckbtoDefault.Margin = New System.Windows.Forms.Padding(4)
        Me.ckbtoDefault.Name = "ckbtoDefault"
        Me.ckbtoDefault.Size = New System.Drawing.Size(128, 21)
        Me.ckbtoDefault.TabIndex = 143
        Me.ckbtoDefault.Text = "Send to Default"
        Me.ckbtoDefault.UseVisualStyleBackColor = True
        '
        'ckbToAll
        '
        Me.ckbToAll.AutoSize = True
        Me.ckbToAll.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ckbToAll.Location = New System.Drawing.Point(181, 16)
        Me.ckbToAll.Margin = New System.Windows.Forms.Padding(4)
        Me.ckbToAll.Name = "ckbToAll"
        Me.ckbToAll.Size = New System.Drawing.Size(98, 21)
        Me.ckbToAll.TabIndex = 144
        Me.ckbToAll.Text = "Send to All"
        Me.ckbToAll.UseVisualStyleBackColor = True
        '
        'txtDefaultEmail
        '
        Me.txtDefaultEmail.Location = New System.Drawing.Point(172, 22)
        Me.txtDefaultEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDefaultEmail.Name = "txtDefaultEmail"
        Me.txtDefaultEmail.Size = New System.Drawing.Size(328, 22)
        Me.txtDefaultEmail.TabIndex = 142
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(19, 26)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(147, 17)
        Me.Label20.TabIndex = 141
        Me.Label20.Text = "Default Email Address"
        '
        'btnSaveChanges
        '
        Me.btnSaveChanges.Location = New System.Drawing.Point(349, 100)
        Me.btnSaveChanges.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnSaveChanges.Name = "btnSaveChanges"
        Me.btnSaveChanges.Size = New System.Drawing.Size(152, 28)
        Me.btnSaveChanges.TabIndex = 130
        Me.btnSaveChanges.Text = "&Save Changes"
        Me.btnSaveChanges.UseVisualStyleBackColor = True
        '
        'TAB_Alchemex
        '
        Me.TAB_Alchemex.Controls.Add(Me.Label2)
        Me.TAB_Alchemex.Controls.Add(Me.Button5)
        Me.TAB_Alchemex.Controls.Add(Me.TextBox2)
        Me.TAB_Alchemex.Controls.Add(Me.Button4)
        Me.TAB_Alchemex.Location = New System.Drawing.Point(4, 25)
        Me.TAB_Alchemex.Name = "TAB_Alchemex"
        Me.TAB_Alchemex.Size = New System.Drawing.Size(819, 377)
        Me.TAB_Alchemex.TabIndex = 2
        Me.TAB_Alchemex.Text = "Repository"
        Me.TAB_Alchemex.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 9)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(205, 17)
        Me.Label2.TabIndex = 133
        Me.Label2.Text = "Reporting Metadata Repository"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btn_TaxUpdate)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.CB_Tx_Default)
        Me.TabPage3.Controls.Add(Me.Label21)
        Me.TabPage3.Controls.Add(Me.TB_TaxPerc)
        Me.TabPage3.Controls.Add(Me.Label18)
        Me.TabPage3.Controls.Add(Me.TB_TaxDesc)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.lbl_TaxID)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.Btn_LoadTax)
        Me.TabPage3.Controls.Add(Me.Btn_removeTax)
        Me.TabPage3.Controls.Add(Me.Btn_AddTax)
        Me.TabPage3.Controls.Add(Me.DGV_Tax)
        Me.TabPage3.Location = New System.Drawing.Point(4, 25)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(819, 377)
        Me.TabPage3.TabIndex = 3
        Me.TabPage3.Text = "TAX Types"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btn_TaxUpdate
        '
        Me.btn_TaxUpdate.Location = New System.Drawing.Point(507, 203)
        Me.btn_TaxUpdate.Name = "btn_TaxUpdate"
        Me.btn_TaxUpdate.Size = New System.Drawing.Size(102, 35)
        Me.btn_TaxUpdate.TabIndex = 13
        Me.btn_TaxUpdate.Tag = "S_UpdateTaxType"
        Me.btn_TaxUpdate.Text = "Update"
        Me.btn_TaxUpdate.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(533, 129)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(92, 17)
        Me.Label22.TabIndex = 12
        Me.Label22.Text = "eg. 14 = 14%"
        '
        'CB_Tx_Default
        '
        Me.CB_Tx_Default.AutoSize = True
        Me.CB_Tx_Default.Location = New System.Drawing.Point(471, 159)
        Me.CB_Tx_Default.Name = "CB_Tx_Default"
        Me.CB_Tx_Default.Size = New System.Drawing.Size(18, 17)
        Me.CB_Tx_Default.TabIndex = 11
        Me.CB_Tx_Default.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(358, 159)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(93, 17)
        Me.Label21.TabIndex = 10
        Me.Label21.Text = "Default Type:"
        '
        'TB_TaxPerc
        '
        Me.TB_TaxPerc.Location = New System.Drawing.Point(471, 126)
        Me.TB_TaxPerc.Name = "TB_TaxPerc"
        Me.TB_TaxPerc.Size = New System.Drawing.Size(56, 22)
        Me.TB_TaxPerc.TabIndex = 9
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(358, 131)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(112, 17)
        Me.Label18.TabIndex = 8
        Me.Label18.Text = "Tax Percentage:"
        '
        'TB_TaxDesc
        '
        Me.TB_TaxDesc.Location = New System.Drawing.Point(471, 98)
        Me.TB_TaxDesc.Name = "TB_TaxDesc"
        Me.TB_TaxDesc.Size = New System.Drawing.Size(138, 22)
        Me.TB_TaxDesc.TabIndex = 7
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(358, 103)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 17)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "Tax Description:"
        '
        'lbl_TaxID
        '
        Me.lbl_TaxID.AutoSize = True
        Me.lbl_TaxID.Location = New System.Drawing.Point(468, 71)
        Me.lbl_TaxID.Name = "lbl_TaxID"
        Me.lbl_TaxID.Size = New System.Drawing.Size(59, 17)
        Me.lbl_TaxID.TabIndex = 5
        Me.lbl_TaxID.Text = "Label16"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(355, 71)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(77, 17)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Tax SeqID:"
        '
        'Btn_LoadTax
        '
        Me.Btn_LoadTax.Location = New System.Drawing.Point(358, 7)
        Me.Btn_LoadTax.Name = "Btn_LoadTax"
        Me.Btn_LoadTax.Size = New System.Drawing.Size(75, 23)
        Me.Btn_LoadTax.TabIndex = 3
        Me.Btn_LoadTax.Text = "Load"
        Me.Btn_LoadTax.UseVisualStyleBackColor = True
        '
        'Btn_removeTax
        '
        Me.Btn_removeTax.Location = New System.Drawing.Point(358, 203)
        Me.Btn_removeTax.Name = "Btn_removeTax"
        Me.Btn_removeTax.Size = New System.Drawing.Size(102, 35)
        Me.Btn_removeTax.TabIndex = 2
        Me.Btn_removeTax.Tag = "S_RemoveTaxType"
        Me.Btn_removeTax.Text = "Remove"
        Me.Btn_removeTax.UseVisualStyleBackColor = True
        '
        'Btn_AddTax
        '
        Me.Btn_AddTax.Location = New System.Drawing.Point(7, 336)
        Me.Btn_AddTax.Name = "Btn_AddTax"
        Me.Btn_AddTax.Size = New System.Drawing.Size(102, 35)
        Me.Btn_AddTax.TabIndex = 1
        Me.Btn_AddTax.Tag = "S_AddTaxType"
        Me.Btn_AddTax.Text = "Add"
        Me.Btn_AddTax.UseVisualStyleBackColor = True
        '
        'DGV_Tax
        '
        Me.DGV_Tax.AllowUserToAddRows = False
        Me.DGV_Tax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Tax.Location = New System.Drawing.Point(7, 7)
        Me.DGV_Tax.Name = "DGV_Tax"
        Me.DGV_Tax.RowTemplate.Height = 24
        Me.DGV_Tax.Size = New System.Drawing.Size(345, 323)
        Me.DGV_Tax.TabIndex = 0
        '
        'Tab_Periods
        '
        Me.Tab_Periods.Controls.Add(Me.Btn_PeriodRemove)
        Me.Tab_Periods.Controls.Add(Me.GroupBox6)
        Me.Tab_Periods.Controls.Add(Me.DGV_Periods)
        Me.Tab_Periods.Location = New System.Drawing.Point(4, 25)
        Me.Tab_Periods.Name = "Tab_Periods"
        Me.Tab_Periods.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_Periods.Size = New System.Drawing.Size(819, 377)
        Me.Tab_Periods.TabIndex = 4
        Me.Tab_Periods.Text = "Periods"
        Me.Tab_Periods.UseVisualStyleBackColor = True
        '
        'Btn_PeriodRemove
        '
        Me.Btn_PeriodRemove.Location = New System.Drawing.Point(6, 342)
        Me.Btn_PeriodRemove.Name = "Btn_PeriodRemove"
        Me.Btn_PeriodRemove.Size = New System.Drawing.Size(151, 32)
        Me.Btn_PeriodRemove.TabIndex = 14
        Me.Btn_PeriodRemove.Tag = "S_DeletePeriod"
        Me.Btn_PeriodRemove.Text = "Remove Selected"
        Me.Btn_PeriodRemove.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.btn_Edit)
        Me.GroupBox6.Controls.Add(Me.btn_AddNewPeriod)
        Me.GroupBox6.Controls.Add(Me.PeriodID)
        Me.GroupBox6.Controls.Add(Me.CB_Closed)
        Me.GroupBox6.Controls.Add(Me.Label3)
        Me.GroupBox6.Controls.Add(Me.Cmb_FinYear)
        Me.GroupBox6.Controls.Add(Me.TB_EndDate)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Controls.Add(Me.TB_StartDate)
        Me.GroupBox6.Controls.Add(Me.Label6)
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.TB_Period)
        Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(813, 78)
        Me.GroupBox6.TabIndex = 3
        Me.GroupBox6.TabStop = False
        '
        'btn_Edit
        '
        Me.btn_Edit.Location = New System.Drawing.Point(693, 49)
        Me.btn_Edit.Name = "btn_Edit"
        Me.btn_Edit.Size = New System.Drawing.Size(75, 23)
        Me.btn_Edit.TabIndex = 13
        Me.btn_Edit.Tag = "S_EditPeriod"
        Me.btn_Edit.Text = "Edit"
        Me.btn_Edit.UseVisualStyleBackColor = True
        '
        'btn_AddNewPeriod
        '
        Me.btn_AddNewPeriod.Location = New System.Drawing.Point(612, 50)
        Me.btn_AddNewPeriod.Name = "btn_AddNewPeriod"
        Me.btn_AddNewPeriod.Size = New System.Drawing.Size(75, 23)
        Me.btn_AddNewPeriod.TabIndex = 12
        Me.btn_AddNewPeriod.Tag = "S_AddPeriod"
        Me.btn_AddNewPeriod.Text = "Add New"
        Me.btn_AddNewPeriod.UseVisualStyleBackColor = True
        '
        'PeriodID
        '
        Me.PeriodID.AutoSize = True
        Me.PeriodID.Location = New System.Drawing.Point(112, 30)
        Me.PeriodID.Name = "PeriodID"
        Me.PeriodID.Size = New System.Drawing.Size(16, 17)
        Me.PeriodID.TabIndex = 11
        Me.PeriodID.Text = "0"
        Me.PeriodID.Visible = False
        '
        'CB_Closed
        '
        Me.CB_Closed.AutoSize = True
        Me.CB_Closed.Location = New System.Drawing.Point(488, 51)
        Me.CB_Closed.Name = "CB_Closed"
        Me.CB_Closed.Size = New System.Drawing.Size(118, 21)
        Me.CB_Closed.TabIndex = 10
        Me.CB_Closed.Text = "Period Closed"
        Me.CB_Closed.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Financial Year"
        '
        'Cmb_FinYear
        '
        Me.Cmb_FinYear.FormattingEnabled = True
        Me.Cmb_FinYear.Location = New System.Drawing.Point(6, 49)
        Me.Cmb_FinYear.Name = "Cmb_FinYear"
        Me.Cmb_FinYear.Size = New System.Drawing.Size(121, 24)
        Me.Cmb_FinYear.TabIndex = 7
        '
        'TB_EndDate
        '
        Me.TB_EndDate.CustomFormat = "dd MMM yyyy"
        Me.TB_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.TB_EndDate.Location = New System.Drawing.Point(357, 51)
        Me.TB_EndDate.Name = "TB_EndDate"
        Me.TB_EndDate.Size = New System.Drawing.Size(125, 22)
        Me.TB_EndDate.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(354, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 17)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "End Date"
        '
        'TB_StartDate
        '
        Me.TB_StartDate.CustomFormat = "dd MMM yyyy"
        Me.TB_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.TB_StartDate.Location = New System.Drawing.Point(211, 51)
        Me.TB_StartDate.Name = "TB_StartDate"
        Me.TB_StartDate.Size = New System.Drawing.Size(125, 22)
        Me.TB_StartDate.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(208, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 17)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Start Date"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(137, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 17)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Period"
        '
        'TB_Period
        '
        Me.TB_Period.Location = New System.Drawing.Point(136, 53)
        Me.TB_Period.Name = "TB_Period"
        Me.TB_Period.Size = New System.Drawing.Size(50, 22)
        Me.TB_Period.TabIndex = 0
        '
        'DGV_Periods
        '
        Me.DGV_Periods.AllowUserToAddRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DGV_Periods.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_Periods.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_Periods.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DGV_Periods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Periods.Location = New System.Drawing.Point(6, 81)
        Me.DGV_Periods.Name = "DGV_Periods"
        Me.DGV_Periods.RowTemplate.Height = 24
        Me.DGV_Periods.Size = New System.Drawing.Size(810, 255)
        Me.DGV_Periods.TabIndex = 2
        '
        'Tab_Update
        '
        Me.Tab_Update.Controls.Add(Me.Btn_UpdateCheck)
        Me.Tab_Update.Location = New System.Drawing.Point(4, 25)
        Me.Tab_Update.Name = "Tab_Update"
        Me.Tab_Update.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_Update.Size = New System.Drawing.Size(819, 377)
        Me.Tab_Update.TabIndex = 5
        Me.Tab_Update.Text = "Update"
        Me.Tab_Update.UseVisualStyleBackColor = True
        '
        'Btn_UpdateCheck
        '
        Me.Btn_UpdateCheck.Location = New System.Drawing.Point(6, 70)
        Me.Btn_UpdateCheck.Name = "Btn_UpdateCheck"
        Me.Btn_UpdateCheck.Size = New System.Drawing.Size(203, 38)
        Me.Btn_UpdateCheck.TabIndex = 1
        Me.Btn_UpdateCheck.Text = "Check for Updates"
        Me.Btn_UpdateCheck.UseVisualStyleBackColor = True
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(833, 444)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Tab_Setting)
        Me.Controls.Add(Me.BtnExit)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblInvalidConnectionString)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSettings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Settings"
        Me.Text = "Settings"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Tab_Setting.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TAB_Alchemex.ResumeLayout(False)
        Me.TAB_Alchemex.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.DGV_Tax, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tab_Periods.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.DGV_Periods, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tab_Update.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSaveDatabaseType As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents lblInvalidConnectionString As System.Windows.Forms.Label
    Friend WithEvents lblLoad As System.Windows.Forms.Label
    Friend WithEvents btnUseConnectionString As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents ListViewCS As System.Windows.Forms.ListView
    Friend WithEvents cmbStartUp_Activation_Mode As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents btnSave_StartUp_Activation_Mode As System.Windows.Forms.Button
    Friend WithEvents cmbCompany As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnExit As System.Windows.Forms.Button
    Friend WithEvents Tab_Setting As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents lblPayInAdvance As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnDisable5 As System.Windows.Forms.Button
    Friend WithEvents btnEnable5 As System.Windows.Forms.Button
    Friend WithEvents lblAutoApproveEFTs As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnDisable4 As System.Windows.Forms.Button
    Friend WithEvents btnEnable4 As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents lblAutoApproveCreditNotes As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnEnable3 As System.Windows.Forms.Button
    Friend WithEvents btnDisable3 As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents ckbtoDefault As System.Windows.Forms.CheckBox
    Friend WithEvents ckbToAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtDefaultEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents btnSaveChanges As System.Windows.Forms.Button
    Friend WithEvents lblIsFuturedate As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnDisable6 As System.Windows.Forms.Button
    Friend WithEvents btnEnable6 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TAB_Alchemex As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Btn_removeTax As System.Windows.Forms.Button
    Friend WithEvents Btn_AddTax As System.Windows.Forms.Button
    Friend WithEvents DGV_Tax As System.Windows.Forms.DataGridView
    Friend WithEvents Btn_LoadTax As System.Windows.Forms.Button
    Friend WithEvents Tab_Periods As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents CB_Closed As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Cmb_FinYear As System.Windows.Forms.ComboBox
    Friend WithEvents TB_EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TB_StartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TB_Period As System.Windows.Forms.TextBox
    Friend WithEvents DGV_Periods As System.Windows.Forms.DataGridView
    Friend WithEvents btn_AddNewPeriod As System.Windows.Forms.Button
    Friend WithEvents PeriodID As System.Windows.Forms.Label
    Friend WithEvents Btn_PeriodRemove As System.Windows.Forms.Button
    Friend WithEvents btn_Edit As System.Windows.Forms.Button
    Friend WithEvents CB_Tx_Default As System.Windows.Forms.CheckBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TB_TaxPerc As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TB_TaxDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lbl_TaxID As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents btn_TaxUpdate As System.Windows.Forms.Button
    Friend WithEvents Tab_Update As System.Windows.Forms.TabPage
    Friend WithEvents Btn_UpdateCheck As System.Windows.Forms.Button
End Class
