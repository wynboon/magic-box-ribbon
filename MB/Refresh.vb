﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class Refresh

    Private Sub Refresh_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Refresh -> Version : " & My.Settings.Setting_Version
        Try
            Me.TopMost = True
            Me.Label_Version.Text = "Version " & My.Settings.Setting_Version
            Call Fill_DGV()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Sub Fill_DGV()
        Try
            Dim sSQL As String

            sSQL = "Select * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                oAccessAdapter_Accounts = New OleDbDataAdapter(sSQL, connection)
                oAccessAdapter_Accounts.Fill(oAccessTable_Accounts)
                Me.dgvAccounting.DataSource = oAccessTable_Accounts
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                oSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
                oSQLAdapter_Accounts.Fill(oSQLTable_Accounts)
                Me.dgvAccounting.DataSource = oSQLTable_Accounts
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 192")
        End Try
    End Sub

    Private Sub oSave()
        Try
            Dim builder As New OleDbCommandBuilder(oAccessAdapter_Accounts)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            oAccessAdapter_Accounts.Update(oAccessTable_Accounts)
            oAccessAdapter_Accounts.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub
End Class