﻿Imports System.Data.SqlClient
Imports System.Data

Module Mod_DataPost
    Function PostTxDataTable() As Boolean
        Try
            Dim BalCheck As Double = 0

            Dim TxTable As DataTable = Globals.Ribbons.Ribbon1.TransactionsTable
            For Each Rw As DataRow In TxTable.Rows
                If Rw("Dr_Cr") = "Dr" Then
                    BalCheck = BalCheck + Rw("Amount")
                Else
                    BalCheck = BalCheck - Rw("Amount")
                End If
            Next
            If Math.Round(BalCheck, 2) <> 0 Then
                MsgBox("Posting Aborted: Transactions do not balance", MsgBoxStyle.Critical, "ABORTING")
                Return False
                Exit Function
            End If

            Using cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                cn.Open()
                Using copy As New SqlBulkCopy(cn)
                    copy.ColumnMappings.Add("BatchID", "BatchID")
                    copy.ColumnMappings.Add("TransactionID", "TransactionID")
                    copy.ColumnMappings.Add("LinkID", "LinkID")
                    copy.ColumnMappings.Add("Capture Date", "Capture Date")
                    copy.ColumnMappings.Add("Transaction Date", "Transaction Date")
                    copy.ColumnMappings.Add("PPeriod", "PPeriod")
                    copy.ColumnMappings.Add("Fin Year", "Fin Year")
                    copy.ColumnMappings.Add("Reference", "Reference")
                    copy.ColumnMappings.Add("Description", "Description")
                    copy.ColumnMappings.Add("Description 2", "Description 2")
                    copy.ColumnMappings.Add("Description 3", "Description 3")
                    copy.ColumnMappings.Add("Description 4", "Description 4")
                    copy.ColumnMappings.Add("Description 5", "Description 5")
                    copy.ColumnMappings.Add("Amount", "Amount")
                    copy.ColumnMappings.Add("TaxType", "TaxType")
                    copy.ColumnMappings.Add("Tax Amount", "Tax Amount")
                    copy.ColumnMappings.Add("UserID", "UserID")
                    copy.ColumnMappings.Add("SupplierID", "SupplierID")
                    copy.ColumnMappings.Add("EmployeeID", "EmployeeID")
                    copy.ColumnMappings.Add("Posted", "Posted")
                    copy.ColumnMappings.Add("Accounting", "Accounting")
                    copy.ColumnMappings.Add("Void", "Void")
                    copy.ColumnMappings.Add("Transaction Type", "Transaction Type")
                    copy.ColumnMappings.Add("DR_CR", "DR_CR")
                    copy.ColumnMappings.Add("Description Code", "Description Code")
                    copy.ColumnMappings.Add("DocType", "DocType")
                    copy.ColumnMappings.Add("SupplierName", "SupplierName")
                    copy.ColumnMappings.Add("Co_ID", "Co_ID")
                    copy.ColumnMappings.Add("Info", "Info")
                    copy.ColumnMappings.Add("Info2", "Info2")
                    copy.ColumnMappings.Add("Info3", "Info3")
                    copy.DestinationTableName = "Transactions"
                    copy.WriteToServer(TxTable)
                End Using
                cn.Close()
            End Using
            Return True
        Catch ex As Exception
            MsgBox("Error Posting transaction: " & ex.Message)
            Return False
            Exit Function
        End Try
    End Function
    Function TxDataTale_AddRow(BatchID As Integer, TransactionID As Integer, LinkID As Integer, TransDate As String, FinYear As Integer, Period As Integer, Ref As String, Description As String, Amount As Double, TaxCode As Integer, TaxAmount As Double, DrCr As String _
                       , AccountCode As String, SupplierID As Integer, SupplierName As String, EmployeeID As Integer, Accounting As Boolean, TransactionType As Integer, DocType As String, Info As String, Info2 As String, Info3 As String)

        '**** there is a global table called TransactionsTable, this method is used to add rows to that datatable, and then at the end bulk Post
        With Globals.Ribbons.Ribbon1.TransactionsTable
            Dim R As DataRow = .NewRow
            R("BatchID") = BatchID
            R("TransactionID") = TransactionID
            R("LinkID") = LinkID
            R("Capture Date") = Now.ToString("dd MMM yyyy hh:mm:ss")
            R("Transaction Date") = TransDate
            R("PPeriod") = Period
            R("Fin Year") = FinYear
            R("Reference") = Ref
            R("Description") = Description
            R("Description 2") = Description
            R("Description 3") = Description
            R("Description 4") = Description
            R("Description 5") = Description
            R("Amount") = Math.Abs(Amount)
            R("TaxType") = TaxCode
            R("Tax Amount") = TaxAmount
            R("UserID") = My.Settings.UserID
            R("SupplierID") = SupplierID
            R("EmployeeID") = EmployeeID
            R("Posted") = True
            R("Accounting") = Accounting
            R("Void") = False
            R("Transaction Type") = TransactionType
            R("DR_CR") = DrCr
            R("Description Code") = AccountCode
            R("DocType") = DocType
            R("SupplierName") = SupplierName
            R("Co_ID") = My.Settings.Setting_CompanyID
            R("Info") = Info
            R("Info2") = Info2
            R("Info3") = Info3
            .Rows.Add(R)
        End With

    End Function
    Function CreateTransactionDataTable() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("BatchID")
        dt.Columns.Add("TransactionID")
        dt.Columns.Add("LinkID")
        dt.Columns.Add("Capture Date")
        dt.Columns.Add("Transaction Date")
        dt.Columns.Add("PPeriod")
        dt.Columns.Add("Fin Year")
        dt.Columns.Add("Reference")
        dt.Columns.Add("Description")
        dt.Columns.Add("Description 2")
        dt.Columns.Add("Description 3")
        dt.Columns.Add("Description 4")
        dt.Columns.Add("Description 5")
        dt.Columns.Add("Amount")
        dt.Columns.Add("TaxType")
        dt.Columns.Add("Tax Amount")
        dt.Columns.Add("UserID")
        dt.Columns.Add("SupplierID")
        dt.Columns.Add("EmployeeID")
        dt.Columns.Add("Posted")
        dt.Columns.Add("Accounting")
        dt.Columns.Add("Void")
        dt.Columns.Add("Transaction Type")
        dt.Columns.Add("DR_CR")
        dt.Columns.Add("Description Code")
        dt.Columns.Add("DocType")
        dt.Columns.Add("SupplierName")
        dt.Columns.Add("Co_ID")
        dt.Columns.Add("Info")
        dt.Columns.Add("Info2")
        dt.Columns.Add("Info3")
        Return dt
    End Function
    Function UpdatePmtRequest(RequestID As Long, RequestApproved As Boolean, ApprovedAmount As Double, Approver As String, BankRequestID As Long, Exported As Boolean, RequestedAmount As Double, PayType As String)
        Dim strSQL As String = ""
        strSQL = "UPDATE [PaymentsRequests] SET [RequestApproved] ='" & RequestApproved & "',[ApprovedAmount] ='" & ApprovedAmount & "'," & _
            "[RequestApprover] = '" & Approver & "',[BankRequestID] = '" & BankRequestID & "',[BankExported] ='" & Exported & "',RequestedAmount ='" & RequestedAmount & "',PaymentType ='" & PayType & "'" & _
            "WHERE RequestID = " & RequestID
        Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(strSQL, conn)
        conn.Open()
        cmd.ExecuteNonQuery()
        conn.Close()
    End Function
    Function InvoiceDueAmount(TransactionID As Integer) As Double
        Try
            Dim CreditorControl As String
            CreditorControl = Get_Segment4_Using_Type_in_Accounting("Creditors Control")

            Dim query As String = "SELECT SUM(Case When Dr_Cr = 'Dr' then Amount else Amount * -1 End) FROM [Transactions] Where (TransactionID = " & TransactionID & " or LinkID = " & TransactionID & ") and [Description Code] <> '" & CreditorControl & "' and Void = 0 and Accounting = 1"


            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(query, conn)
            conn.Open()
            cmd.CommandText = query
            InvoiceDueAmount = cmd.ExecuteScalar()
            conn.Close()
        Catch ex As Exception
            MsgBox("Error creating transaction ID - Err:" & ex.Message)
        End Try
    End Function
End Module
