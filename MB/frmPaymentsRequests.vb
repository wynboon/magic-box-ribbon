﻿Imports System.Windows.Forms
Imports System.Collections
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data

Public Class frmPaymentsRequests
#Region "Variables"
    Dim MBData As New DataExtractsClass
#End Region
#Region "Methods"


    Public Sub Formatting()

        Dim RemoveColumnButtom As New DataGridViewButtonColumn
        With RemoveColumnButtom
            .Width = 150
            .Name = "btnRemoveTrans"
            .Text = "Remove"
            .HeaderText = "Remove Transaction"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            '.FlatStyle = FlatStyle.Flat
            '.DefaultCellStyle.BackColor = Color.White
            .DefaultCellStyle.ForeColor = Color.DarkBlue
        End With
        dgvPaymentsRequests.Columns.Insert(14, RemoveColumnButtom)

        Dim CheckRequest As New DataGridViewCheckBoxColumn
        With CheckRequest
            .Name = "CheckRequest"
            .HeaderText = "Select"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            '.FlatStyle = FlatStyle.Flat
            '.DefaultCellStyle.BackColor = Color.White
            .DefaultCellStyle.ForeColor = Color.DarkBlue
        End With
        dgvPaymentsRequests.Columns.Insert(15, CheckRequest)

    End Sub
    Public Sub InitialLoad()

        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim totalAmount As Decimal = 0
            PaymentsRequestBindingSource.DataSource = MBData.GetPaymentsRequests
            Dim cm As CurrencyManager = DirectCast(BindingContext(dgvPaymentsRequests.DataSource), CurrencyManager)

            For Each row As DataGridViewRow In dgvPaymentsRequests.Rows

                Dim TransactionID As Integer = CInt(row.Cells(3).Value)
                Dim MainTrans As Transaction = MBData.GetMainTransID(TransactionID)
                If Not IsNothing(MainTrans) Then
                    row.Cells(0).Value = CStr(MainTrans.SupplierName)
                    row.Cells(1).Value = CStr(MainTrans.Reference)
                Else
                    cm.SuspendBinding()
                    row.Visible = False
                    cm.ResumeBinding()
                End If

            Next

            For Each row As DataGridViewRow In dgvPaymentsRequests.Rows

                If Not LoggedUser_Roles.Contains(row.Cells(5).Value) And LoggedInUser <> CStr(row.Cells(10).Value) Then
                    cm.SuspendBinding()
                    row.Visible = False
                    cm.ResumeBinding()
                Else
                    totalAmount += CDec(row.Cells(6).Value)
                End If

            Next

            txtRequests.Text = totalAmount.ToString("c")

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Public Sub LoadByDate()

        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim TotalAmount As Decimal = 0
            Dim fromDate As Date = CDate(dtpFrom.Value).ToShortDateString
            Dim ToDate As Date = CDate(dtpTo.Value)

            PaymentsRequestBindingSource.DataSource = MBData.GetPaymentsRequestsByDate(fromDate, ToDate, cmbPaymentType.Text)
            Dim cm As CurrencyManager = DirectCast(BindingContext(dgvPaymentsRequests.DataSource), CurrencyManager)

            For Each row As DataGridViewRow In dgvPaymentsRequests.Rows

                Dim TransactionID As Integer = CInt(row.Cells(3).Value)
                Dim MainTrans As Transaction = MBData.GetMainTransID(TransactionID)

                If Not IsNothing(MainTrans) Then
                    If MainTrans.SupplierName = cmbSupplier.Text Then
                        row.Cells(0).Value = CStr(MainTrans.SupplierName)
                        row.Cells(1).Value = CStr(MainTrans.Reference)
                    Else
                        cm.SuspendBinding()
                        row.Visible = False
                        cm.ResumeBinding()
                    End If
                Else
                    cm.SuspendBinding()
                    row.Visible = False
                    cm.ResumeBinding()
                End If

            Next

            For Each row As DataGridViewRow In dgvPaymentsRequests.Rows

                If Not LoggedUser_Roles.Contains(row.Cells(5).Value) And LoggedInUser <> CStr(row.Cells(10).Value) Then
                    cm.SuspendBinding()
                    row.Visible = False
                    cm.ResumeBinding()
                Else
                    TotalAmount += CDec(row.Cells(6).Value)
                End If

            Next

            txtRequests.Text = TotalAmount.ToString("c")

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Public Sub LoadSupplier()
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub FillPaymentTypesCombo()

        Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Display Name] FROM Accounting Where Type = 'Tender'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            Me.cmbPaymentType.Items.Clear()
            While datareader.Read
                If Not datareader("Display Name").Equals(DBNull.Value) Then
                    If Not cmbPaymentType.Items.Contains(datareader("Display Name")) Then
                        Me.cmbPaymentType.Items.Add(datareader("Display Name"))
                    End If
                End If
            End While
            connection.Close()

            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            If connection.State = ConnectionState.Open Then
                connection.Close()
                connection.Dispose()
            End If
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
#End Region
    Private Sub frmPaymentsRequests_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Formatting()
        LoadSupplier()
        FillPaymentTypesCombo()
        InitialLoad()
    End Sub

    Private Sub dgvPaymentsRequests_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPaymentsRequests.CellClick
        If e.ColumnIndex = 14 Then
            Dim DeleteConfirm As DialogResult = MessageBox.Show("Are you sure you want to delete this request?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If DeleteConfirm = Windows.Forms.DialogResult.Yes Then
                Dim RequestID As Integer = CInt(dgvPaymentsRequests.Rows(e.RowIndex).Cells(2).Value)
                MBData.RemoveRequestWithID(RequestID)
                MessageBox.Show("Payment request succesfully removed", "Request removal", MessageBoxButtons.OK, MessageBoxIcon.Information)
                InitialLoad()
            End If

        End If
    End Sub

    Private Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnFilter.Click
        If Not dgvPaymentsRequests.Columns.Contains("btnRemoveTrans") Then
            Formatting()
        End If
        If cmbSupplier.Text = "" Then
            MessageBox.Show("Please select a supplier in order to continue", "Supplier", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            cmbSupplier.Focus()
            Exit Sub
        End If
        If cmbPaymentType.Text = "" Then
            MessageBox.Show("Please select payment type in order to continue", "Payment Type", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            cmbPaymentType.Focus()
            Exit Sub
        End If
        LoadByDate()
    End Sub
    Private Sub btnResetList_Click(sender As Object, e As EventArgs) Handles btnResetList.Click
        frmPaymentsRequests_Load(sender, e)
    End Sub
    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim cCount As Integer = 0
        For Each row As DataGridViewRow In dgvPaymentsRequests.Rows
            If CBool(row.Cells(15).Value) = True Then
                cCount += 1
            End If
        Next

        If cCount = 0 Then
            MessageBox.Show("There is no request(s) selected, please select request(s) in order to continue", "Request removal", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim DeleteCount As Integer = 0
        Dim DeleteConfirm As DialogResult = MessageBox.Show("Are you sure you want to delete this request(s)?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If DeleteConfirm = Windows.Forms.DialogResult.Yes Then
            For Each row As DataGridViewRow In dgvPaymentsRequests.Rows
                If CBool(row.Cells(15).Value) = True Then
                    DeleteCount += 1
                    Dim RequestID As Integer = CInt(row.Cells(2).Value)
                    MBData.RemoveRequestWithID(RequestID)
                End If
            Next
            InitialLoad()
            MessageBox.Show(DeleteCount & " Payment(s) request(s) succesfully removed", "Request removal", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class