﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_CauldronClear
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_CurrentCompany = New System.Windows.Forms.Label()
        Me.DT_Start = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DT_End = New System.Windows.Forms.DateTimePicker()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.lbl_Status = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbl_CurrentCompany
        '
        Me.lbl_CurrentCompany.AutoSize = True
        Me.lbl_CurrentCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_CurrentCompany.ForeColor = System.Drawing.Color.Red
        Me.lbl_CurrentCompany.Location = New System.Drawing.Point(13, 13)
        Me.lbl_CurrentCompany.Name = "lbl_CurrentCompany"
        Me.lbl_CurrentCompany.Size = New System.Drawing.Size(285, 39)
        Me.lbl_CurrentCompany.TabIndex = 0
        Me.lbl_CurrentCompany.Text = "Current Company"
        '
        'DT_Start
        '
        Me.DT_Start.CustomFormat = "dd MMMM yyyy"
        Me.DT_Start.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_Start.Location = New System.Drawing.Point(29, 95)
        Me.DT_Start.Name = "DT_Start"
        Me.DT_Start.Size = New System.Drawing.Size(200, 22)
        Me.DT_Start.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Start Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "End Date"
        '
        'DT_End
        '
        Me.DT_End.CustomFormat = "dd MMMM yyyy"
        Me.DT_End.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_End.Location = New System.Drawing.Point(29, 175)
        Me.DT_End.Name = "DT_End"
        Me.DT_End.Size = New System.Drawing.Size(200, 22)
        Me.DT_End.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(297, 95)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(255, 110)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Clear Cauldron"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(13, 466)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(910, 23)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'lbl_Status
        '
        Me.lbl_Status.AutoSize = True
        Me.lbl_Status.Location = New System.Drawing.Point(29, 220)
        Me.lbl_Status.Name = "lbl_Status"
        Me.lbl_Status.Size = New System.Drawing.Size(279, 17)
        Me.lbl_Status.TabIndex = 7
        Me.lbl_Status.Text = "This process may take a while, please wait."
        '
        'Frm_CauldronClear
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(935, 501)
        Me.ControlBox = False
        Me.Controls.Add(Me.lbl_Status)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DT_End)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DT_Start)
        Me.Controls.Add(Me.lbl_CurrentCompany)
        Me.Name = "Frm_CauldronClear"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_CurrentCompany As System.Windows.Forms.Label
    Friend WithEvents DT_Start As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DT_End As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents lbl_Status As System.Windows.Forms.Label
End Class
