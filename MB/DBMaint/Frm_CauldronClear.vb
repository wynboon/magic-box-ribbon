﻿Public Class Frm_CauldronClear

    Private Sub Frm_CauldronClear_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_CurrentCompany.Text = My.Settings.CurrentCompany
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ClearCauldron(DT_Start.Text, DT_End.Text)
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class