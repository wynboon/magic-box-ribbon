﻿Imports System.Data.SqlClient

Module Mod_Maint
    Function ClearCauldron(StartDate As String, EndDate As String)
        Try
            Dim sSQL As String
            Dim CoID As Long
            CoID = My.Settings.Setting_CompanyID
            sSQL = "Delete from POS_DayEnd where Co_ID =" & CoID & " and TxDate >='" & StartDate & "' and TxDate <='" & EndDate & "' " & vbNewLine & _
            "Delete from POS_SalesDetail where Co_ID =" & CoID & " and TxDate >='" & StartDate & "' and TxDate <='" & EndDate & "' " & vbNewLine & _
            "Delete from POS_PayoutsDetail where Co_ID =" & CoID & " and TxDate >='" & StartDate & "' and TxDate <='" & EndDate & "' " & vbNewLine & _
            "Delete from POS_TenderDetail where Co_ID =" & CoID & " and TxDate >='" & StartDate & "' and TxDate <='" & EndDate & "' " & vbNewLine & _
            "Delete from POS_GRN_Detail where Co_ID =" & CoID & " and InvDate >='" & StartDate & "' and InvDate <='" & EndDate & "'"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=9999")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error clearing the cauldron:" & vbNewLine & ex.Message)
            Return ""
        End Try
    End Function
End Module
