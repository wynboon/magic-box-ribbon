﻿Imports System.Data
Imports System.Data.SqlClient

Module ReportQueries
    Function SupplierMaster() As DataSet
        Try

            Dim sSQL As String = "SELECT SupplierID,SupplierName,RefID as Code from Suppliers Where Co_ID =" & My.Settings.Setting_CompanyID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()

            connection.Open()
            Dim ds_SupplierMaster As DataSet = New DataSet
            dataadapter.Fill(ds_SupplierMaster, "SupplierMasters")
            connection.Close()
            Return ds_SupplierMaster
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
End Module

