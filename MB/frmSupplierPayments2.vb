﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D
Imports System.ComponentModel
Imports System.Collections

Public Class frmSupplierPaymentsV2

#Region "Variables"

    Public ListIndex_on_Mousehover As Integer
    Private MouseIsDown As Boolean = False
    Dim SelectedSupplier As Long = 0

    Dim oBatchID As Integer
    Dim oTransactionID As Long
    Dim oLinkID As Integer
    Dim dTransactionDate As Date
    Dim dCaptureDate As Date
    Dim sPPeriod As String
    Dim sFinYear As String
    Dim sGDC As String
    Dim sInvoiceNumber As String
    Dim sDescription As String
    Dim sAccountNumber As String
    Dim sContraAccount As String
    Dim sLinkAcc As String
    Dim oAmount As Decimal
    Dim oTaxType As Integer
    Dim oTaxAmount As Decimal
    Dim sUserID As String
    Dim oSupplierID As Integer
    Dim oEmployeeID As Integer
    Dim sDescription2 As String
    Dim sDescription3 As String
    Dim sDescription4 As String
    Dim sDescription5 As String
    Dim blnPosted As Boolean
    Dim blnAccounting As Boolean
    Dim blnVoid As Boolean
    Dim TransactionType As Integer
    Dim oDocType As String
    Dim oSupplierName As String
    Dim sDRCR As String
    Dim sInfo As String
    Dim sInfo2 As String
    Dim sInfo3 As String

    Dim BatchID_Check As Integer

    Dim CellValueBeforeEdit As Decimal

    '==========================================================================================================
    '                                               Make Payments
    '==========================================================================================================
    Public oNumberPayments As Integer
    Public arrID(10000) As Integer '0
    Public arrSupplierName(10000) As String '1
    Public arrReference(10000) As String '2
    Public arrDateOfPayment(10000) As String '3
    Public arrPaymentAmount(10000) As Decimal '4
    Public arrPaymentType(10000) As String ' 5

    '==========================================================================================================

    Dim sTransactionDate As String

    Dim xAccessAdapter_Accounts As OleDbDataAdapter
    Dim xAccessTable_Accounts As New DataTable
    Dim xSQLAdapter_Accounts As SqlDataAdapter
    Dim xSQLTable_Accounts As New DataTable

    Dim xAccessAdapter_Suppliers As OleDbDataAdapter
    Dim xAccessTable_Suppliers As New DataTable
    Dim xSQLAdapter_Suppliers As SqlDataAdapter
    Dim xSQLTable_Suppliers As New DataTable

    Dim xAccessAdapter_Periods As OleDbDataAdapter
    Dim xAccessTable_Periods As New DataTable
    Dim xSQLAdapter_Periods As SqlDataAdapter
    Dim xSQLTable_Periods As New DataTable

    Dim oProgressBar As ProgressBar

    Dim blnAt_Least_One_Payment_Request_Paid As Boolean

    Private Property blnFormLoading As Boolean
    Private blnFirstRun As Boolean
    Private blnForm_Hiding As Boolean

    Dim PayInBatchID As Integer = Nothing
    Dim PayInTransID As Integer = Nothing
    Dim PayInLinkID As Integer = Nothing
    Dim PayInTransDate As String = ""
    Dim PayInCaptureDate As Date
    Dim PayInPeriod As String = ""
    Dim PayInFinYear As String = ""
    Dim PayInSGDC As String = ""
    Dim PayInInvNumber As String = ""
    Dim PayInDescription As String = ""
    Dim PayInAccNumber As String = ""
    Dim PayInLinkAcc As String = ""
    Dim PayInAmount As Decimal = Nothing
    Dim PayInTaxType As Decimal = Nothing
    Dim PayInTaxAmount As Decimal = Nothing
    Dim PayInUserID As String = ""
    Dim PayInSupplierID As Integer = Nothing
    Dim PayInEmpID As Integer = Nothing
    Dim PayInDesc2 As String = ""
    Dim PayInDesc3 As String = ""
    Dim PayInDesc4 As String = ""
    Dim PayInDesc5 As String = ""
    Dim PayInBlnPosted As Boolean = Nothing
    Dim PayInBlnAccounting As Boolean = Nothing
    Dim PayInBlnVoid As Boolean = Nothing
    Dim PayInTransactionType As Integer = Nothing
    Dim PayInDRCR As String = ""
    Dim PayInContraAccount As String = ""
    Dim PayInDescCode As String = ""
    Dim PayInDocType As String = ""
    Dim PayInSuppName As String = ""
    Dim PayInInfo As String = ""
    Dim PayInfo2 As String = ""
    Dim PayInInfo3 As String = ""

    'MB Data
    Dim MBData As New DataExtractsClass

    Dim dsReq As New DataSet()

#End Region
    Dim BusyLoading As Boolean
    Private Sub Supplier_Payments_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        lbl_ExcludedPayTypes.Text = "Excluded Paytypes: " & Globals.Ribbons.Ribbon1.ExcludedPayTypes
    End Sub
    Private Sub frmSupplierPaymentsV2_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        BusyLoading = True
        LockForm(Me)
        LoadPayScreen()
        BusyLoading = False
        Lbl_Status.Visible = False
        dgvSupplierSum.ClearSelection()
        If dgvSupplierSum.SelectedRows.Count > 0 Then
            dgvSupplierSum.Rows(0).Selected = True
        End If
    End Sub
    Function LoadPayScreen()
        Try
            Me.Icon = My.Resources.AppIcon
            Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Supplier Payments -> Version : " & My.Settings.Setting_Version
            Enabled = False
            Cursor = Cursors.AppStarting
            Lbl_Status.Text = "Loading Data..."
            oLoadData()

            Dim DueColumn As DataGridViewColumn = dgvSupplierSum.Columns("Due")
            DueColumn.Width = 100
            DueColumn.DefaultCellStyle.Format = "c"
            DueColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            cmbPaymentType.SelectedItem = "EFT"
            dgvSupplierSum.Columns("SupplierID").Visible = False
            'cmbSupplier.SelectedIndex = -1
            cmbStatus.Text = "UNPAID"

            Call LoadPayRequests()

            Enabled = True
            Cursor = Cursors.Arrow
            rdbSupplier.Checked = True

        Catch ex As Exception
            MessageBox.Show("An Error Occured :" & ex.Message, "Supplier payments Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Function
    Sub oLoadData()
        Try

            blnFormLoading = True
            blnFirstRun = True

            oProgressBar = Me.ProgressBar1
            oProgressBar.Visible = True
            oProgressBar.Minimum = 1
            oProgressBar.Maximum = 100
            oProgressBar.Value = 1
            dgvInvoiceList.DataSource = Nothing
            Lbl_Status.Text = "Loading Accounts..."
            Application.DoEvents()
            Fill_DGV_Accounts()
            oProgressBar.Value = 20
            ''Call Fill_DGV_Suppliers()
            oProgressBar.Value = 40
            Lbl_Status.Text = "Loading Periods..."
            Application.DoEvents()
            Fill_DGV_Periods()

            oProgressBar.Value = 60

            Me.ShowInTaskbar = True 'Mirrors the form closing event

            Me.dtpFrom.Value = System.DateTime.Now.AddMonths(-12)
            Me.ComboBox_SupplierStatus.SelectedIndex = Nothing 'this will force the next line to refresh when the visible state changes as default is 1
            Me.ComboBox_SupplierStatus.SelectedIndex = 1 'This will trigger SelectedIndexChanged procedure below to fill the Supplier DGV

            oProgressBar.Value = 70
            Lbl_Status.Text = "Loading Suppliers..."
            Application.DoEvents()
            FillComboBoxSuppliers()

            'Me.cmbStatus.SelectedIndex = 1
            Lbl_Status.Text = "Loading PayTypes..."
            Application.DoEvents()
            Fill_Payment_Types_Combo()
            oProgressBar.Value = 80


            Lbl_Status.Text = "SupplierSum Refresh..."
            Application.DoEvents()
            dgvSupplierSum.DataSource = Nothing
            dgvSupplierSum.Rows.Clear()
            dgvSupplierSum.Refresh()

            Lbl_Status.Text = "InvoiceList Refresh..."
            Application.DoEvents()
            dgvInvoiceList.DataSource = Nothing
            dgvInvoiceList.Rows.Clear()
            dgvInvoiceList.Refresh()
            dgvInvoiceList.Columns("PtypeEqualiser").Visible = False
            oProgressBar.Value = 80
            Lbl_Status.Text = "Fill Supplier..."
            Application.DoEvents()
            FillDGV_Supplier()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            rdbSupplier.Checked = True
            oProgressBar.Value = 100
            Lbl_Status.Text = "Colour Suppliers ..."
                ' Dim SupplierWithRequests As New Hashtable()
                Dim SupplierWithRequests As New DataTable
                SupplierWithRequests = PaymentRequests()
                ' SupplierWithRequests = MBData.ReturnSuppliersWithRequests()
                Dim distinctSuppliers As DataTable = SupplierWithRequests.DefaultView.ToTable(True, "SupplierName")
                distinctSuppliers.DefaultView.Sort = "SupplierName"

                For Each row As DataGridViewRow In dgvSupplierSum.Rows
                    Dim SupplierName As String = CStr(row.Cells("SupplierName").Value)
                    If Not distinctSuppliers.DefaultView.Find(SupplierName) = -1 Then
                        row.DefaultCellStyle.ForeColor = Color.LimeGreen
                    End If
                Next

            'THis used to add the lines into the payments screen. Has been removed cause all request go directly into the Req screen.
            'If SupplierWithRequests.Rows.Count <> 0 Then
            ' For Each row In SupplierWithRequests.Rows
            'dgvPaymentsList.Rows.Add(row("InvoiceTransactionID"), row("SupplierName"), row("Reference"), row("RequestedAmount"), _
            '                         dtpPaymentDate.Value.ToString("dd MMM yyyy"), row("PaymentType"), dtpPaymentDate.Value.ToString("dd MMM yyyy"))

            'Next
            'For Each row In dgvPaymentsList.Rows
            'row.DefaultCellStyle.ForeColor = Color.LimeGreen
            'Next
            '   End If

            TotalPaidBox()
            blnFormLoading = False
            dtpFrom.Value = Today.AddDays(-30)
            cmbPaymentType.Text = "EFT"
            cmbStatus.Text = "[ALL]"

            oProgressBar.Visible = False
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured " & ex.Message, "Supplier Payments", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try

    End Sub
    Sub Fill_DGV_Accounts()
        Try
            Dim sSQL As String

            sSQL = "Select * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Accounts = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Accounts.Fill(xAccessTable_Accounts)
                Me.dgvAccounts.DataSource = xAccessTable_Accounts
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Accounts.Fill(xSQLTable_Accounts)
                Me.dgvAccounts.DataSource = xSQLTable_Accounts
            End If
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message)
        End Try
    End Sub
    Sub Fill_DGV_Suppliers()
        Try
            Dim sSQL As String

            sSQL = "Select * From Suppliers"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Suppliers = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Suppliers.Fill(xAccessTable_Suppliers)
                Me.dgvSuppliers.DataSource = xAccessTable_Suppliers
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Suppliers = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Suppliers.Fill(xSQLTable_Suppliers)
                Me.dgvSuppliers.DataSource = xSQLTable_Suppliers
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 051")

        End Try
    End Sub
    Sub Fill_DGV_Periods()
        Try
            Dim sSQL As String

            sSQL = "Select * From Periods"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Periods = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Periods.Fill(xAccessTable_Periods)
                Me.dgvPeriods.DataSource = xAccessTable_Periods
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Periods = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Periods.Fill(xSQLTable_Periods)
                Me.dgvPeriods.DataSource = xSQLTable_Periods
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 052")
        End Try
    End Sub

    Sub Fill_Payment_Types_Combo()
        Try
            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Display Name] FROM Accounting Where Type = 'Tender'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                'MsgBox(xSQLTable_Accounts.DefaultView.Count.ToString)

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    xSQLTable_Accounts.DefaultView.RowFilter = "Type = 'Tender' And Co_ID = " & My.Settings.Setting_CompanyID
                Else
                    xSQLTable_Accounts.DefaultView.RowFilter = "Type = 'Tender'"
                End If

                cmbPaymentType.Items.Clear()

                For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                    If Not cmbPaymentType.Items.Contains(xSQLTable_Accounts.DefaultView.Item(i).Item("Display Name")) Then
                        cmbPaymentType.Items.Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Display Name"))
                    End If
                Next

            Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300

                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.cmbPaymentType.Items.Clear()
                While datareader.Read
                    If Not datareader("Display Name").Equals(DBNull.Value) Then
                        If Not cmbPaymentType.Items.Contains(datareader("Display Name")) Then
                            Me.cmbPaymentType.Items.Add(datareader("Display Name"))
                        End If
                    End If
                End While
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 053")
        End Try

    End Sub

    Private Sub ComboBox_SupplierStatus_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox_SupplierStatus.SelectedIndexChanged
        Try
            If blnFormLoading = True Then Exit Sub
            If blnForm_Hiding = True Then Exit Sub
            Call FillDGV_Supplier() 'This changes the supplier index which runs the  GetInvoiceDetailPerSupplier
            ''Call GetInvoiceDetailPerSupplier()
        Catch ex As Exception

        End Try
    End Sub
    Sub FillDGV_Supplier()
        Try
            Dim strSQL As String
            If ckbAllBalances.Checked = True Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=600")
                strSQL = "Select * from Func_SupplierBalancesEverything(" & My.Settings.Setting_CompanyID & ")"

                Dim dataadapter As New SqlDataAdapter(strSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()

                If dgvInvoiceList.Rows.Count <> 0 Then
                    dgvInvoiceList.DataSource = Nothing
                    dgvInvoiceList.Rows.Clear()
                    dgvInvoiceList.Refresh()
                    dgvInvoiceList.Columns("PtypeEqualiser").Visible = False
                End If

                dgvSupplierSum.DataSource = ds
                dgvSupplierSum.DataMember = "Suppliers_table"
                dgvSupplierSum.Columns("SupplierID").Visible = False

                Dim DueColumn As DataGridViewColumn = dgvSupplierSum.Columns("Due")
                DueColumn.Width = 100
                DueColumn.DefaultCellStyle.Format = "c"
                DueColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                cmbStatus.SelectedIndex = -1

            Else
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=600")
                ' strSQL = "Select * from Func_SupplierBalances(" & My.Settings.Setting_CompanyID & ")"

                strSQL = "Select Transactions.SupplierID, Suppliers.SupplierName, " & _
                " SUM(Case When Dr_Cr = 'Cr'then Amount  else amount * -1 end) as Due " & _
                " From Suppliers " & _
                " Inner Join Transactions " & _
                " on Suppliers.SupplierID = Transactions.SupplierID " & _
                "Where Transactions.Co_ID =" & My.Settings.Setting_CompanyID & " and Void = 0 and [Description Code] = 'HAAAAA' " & _
                "Group By Transactions.SupplierID, Suppliers.SupplierName " & _
                "Having SUM(Case When Dr_Cr = 'Cr'then Amount  else amount * -1 end) <> 0 "



                Dim dataadapter As New SqlDataAdapter(strSQL, connection)
                dataadapter.SelectCommand.CommandTimeout = 120
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                '   If dgvInvoiceList.Rows.Count <> 0 Then
                ' dgvInvoiceList.DataSource = Nothing
                ' dgvInvoiceList.Rows.Clear()
                ' dgvInvoiceList.Refresh()
                ' dgvInvoiceList.Columns("PtypeEqualiser").Visible = False
                'End If

                dgvSupplierSum.DataSource = ds
                dgvSupplierSum.DataMember = "Suppliers_table"
                dgvSupplierSum.Columns("SupplierID").Visible = False

                Dim DueColumn As DataGridViewColumn = dgvSupplierSum.Columns("Due")
                DueColumn.Width = 100
                DueColumn.DefaultCellStyle.Format = "c"
                DueColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                cmbStatus.SelectedIndex = -1

            End If

            TotalSuppliersBalBox()
            dgvSupplierSum.Sort(dgvSupplierSum.Columns("SupplierName"), System.ComponentModel.ListSortDirection.Ascending)
            dgvSupplierSum.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dgvSupplierSum.AutoResizeColumns()
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub FillComboBoxSuppliers()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("Error loading the supplier dropdown. Please check that suppliers are loaded! " & ex.Message & " 017")
        End Try
    End Sub
    Sub TotalSuppliersBalBox()

        Dim oTotal As Decimal = 0
        If dgvSupplierSum.Rows.Count <> 0 Then

            For Each row As DataGridViewRow In dgvSupplierSum.Rows
                oTotal += CDec(row.Cells("Due").Value)
            Next

            txtBalance.Text = oTotal.ToString("c")
            txtUnapprovedCNotes.Text = GetCoUnapprovedCNote().ToString("c")
            If My.Settings.User_Control_On = "Yes" Then
                txtPaymentsRequests.Text = (SumPaymentsRequestsByTypes() + SumPaymentsRequestsByOwner()).ToString("c")
            End If
            Dim Difference As Decimal = CDec(CDec(txtBalance.Text) - CDec(txtUnapprovedCNotes.Text))
            txtAsPerSheet.Text = GetCreditorControlBalance.ToString("c")

        End If
    End Sub
    Dim BolBntFilter As Boolean = False
    Sub GetInvoiceDetailPerSupplier(ByVal Osupplier)

        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub
            Dim sSQL_Supplier As String = ""
            dgvInvoiceList.DataSource = Nothing

            Dim oFrom As String = dtpFrom.Value.ToString("dd MMM yyyy")
            Dim oTo As String = dtpTo.Value.ToString("dd MMM yyyy")

            Dim sSQL As String = ""

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                'Herold
                If BolBntFilter Then

                    If cmbStatus.Text = "[ALL]" Then
                        sSQL = CStr("Select * from dbo.Func_SupplierBalanceDetailDate_ALLINV(" & My.Settings.Setting_CompanyID & ",'" & Osupplier & "', '" & oFrom & "','" & oTo & "') order by [Transaction Date] Desc")
                    ElseIf cmbStatus.Text = "UNPAID" Then
                        sSQL = CStr("Select * from dbo.Func_SupplierBalanceDetailDate_UNPAIDINV(" & My.Settings.Setting_CompanyID & ",'" & Osupplier & "', '" & oFrom & "','" & oTo & "') order by [Transaction Date] Desc")
                    ElseIf cmbStatus.Text = "PAID" Then
                        sSQL = CStr("Select * from dbo.Func_SupplierBalanceDetailDate_PAIDINV(" & My.Settings.Setting_CompanyID & ",'" & Osupplier & "', '" & oFrom & "','" & oTo & "') order by [Transaction Date] Desc")
                    End If

                Else
                    sSQL = CStr("Select * from dbo.Func_SupplierBalanceDetailDate_UNPAIDINV(" & My.Settings.Setting_CompanyID & ",'" & Osupplier & "', '" & oFrom & "','" & oTo & "') order by [Transaction Date] Desc")
                End If

                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                dgvInvoiceList.DataSource = ds
                dgvInvoiceList.DataMember = "Suppliers_table"

            End If

            If dgvInvoiceList.ColumnCount > 0 Then
                Me.dgvInvoiceList.Columns(0).Visible = False
                Dim DueColumn As DataGridViewColumn = dgvInvoiceList.Columns("Due")
                DueColumn.DefaultCellStyle.Format = "c"
            End If

            If My.Settings.User_Control_On = "Yes" Then
                LoadSupplierInvoicesRequestDetail(CInt(Osupplier))
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try
        '
    End Sub

    Sub Colour_BackGround_for_Supplier_Debits_And_Credit_Notes()
        Try

            Dim i, j As Integer
            Dim DGV1 As DataGridView = Form_Invoice_Detail_Per_Supplier.DataGridView1
            Dim oInfo2 As String
            Dim oReference As String

            For i = 0 To DGV1.RowCount - 2
                oInfo2 = DGV1.Rows(i).Cells(3).Value.ToString
                If Mid(oInfo2, 1, 14) = "Supplier Debit" Then
                    oReference = DGV1.Rows(i).Cells(0).Value
                    oColourCell(oReference, "Yellow")
                End If
                If oInfo2 = "Credit Note" Then
                    oReference = DGV1.Rows(i).Cells(0).Value.ToString
                    oColourCell(oReference, "Green")
                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message & " 057m")
        End Try
    End Sub
    Sub oColourCell(ByVal oReference As String, ByVal oColour As String)
        Try

            Dim i As Integer
            For i = 0 To Me.dgvInvoiceList.RowCount - 2
                If oReference = Me.dgvInvoiceList.Rows(i).Cells(2).Value Then
                    If oColour = "Green" Then
                        Me.dgvInvoiceList.Rows(i).Cells(1).Style.BackColor = Color.LightGreen
                    ElseIf oColour = "Yellow" Then
                        Me.dgvInvoiceList.Rows(i).Cells(2).Style.BackColor = Color.Yellow
                    End If

                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message & " 057n")
        End Try
    End Sub

    Private Sub DataGridView_Supplier_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgvSupplierSum.SelectionChanged
        If BusyLoading = True Then Exit Sub
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim intRwIndex As Integer
            Dim selectedRowCount As Integer = dgvSupplierSum.Rows.GetRowCount(DataGridViewElementStates.Selected)

            If selectedRowCount > 0 Then
                Dim i As Integer
                For i = 0 To selectedRowCount - 1
                    intRwIndex = dgvSupplierSum.SelectedRows(i).Index
                Next i



                Dim SupplierID = dgvSupplierSum.Item("SupplierID", intRwIndex).Value
                SelectedSupplier = SupplierID
                Dim oSupplier As String = dgvSupplierSum.Item("SupplierName", intRwIndex).Value
                If oSupplier <> String.Empty Then
                    Me.cmbSupplier.Text = oSupplier
                    BolBntFilter = False
                    GetInvoiceDetailPerSupplier(SupplierID)
                    TotalPaidBox()
                End If

                Me.Enabled = False
                Me.Cursor = Cursors.AppStarting

                For Each OriginalRow As DataGridViewRow In dgvInvoiceList.Rows
                    Dim RequestResults = MBData.CheckRequestByTransID(OriginalRow.Cells(1).Value)
                    If Not IsNothing(RequestResults) Then
                        OriginalRow.DefaultCellStyle.ForeColor = Color.LimeGreen
                    End If
                Next
            End If

            'Update request tab:
            Call LoadPayRequests(SelectedSupplier)

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details. : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Button_Add_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveRequest.Click
        'Call AddRow()
        Try
            If Me.DGV_Requests.SelectedRows.Count = 0 Then
                MessageBox.Show("Please select the invoice you want to remove request on in order to continue", "Remove invoice request", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim MainID As Integer = 0

            For Each row In DGV_Requests.SelectedRows
                Dim RequestTransactionID As Integer = CInt(row.Cells("TransactionID").Value)
                Dim Amount As Decimal = CDec(row.Cells("RequestedAmount").Value)
                MBData.RemoveRequest(RequestTransactionID)
            Next

            MessageBox.Show("Payment request succesfully removed.", "Payment Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Supplier_Payments_Load(sender, e)
            Dim selectedRowCount As Long
            Dim intRwIndex As Integer
            selectedRowCount = dgvSupplierSum.SelectedRows.Count
            If selectedRowCount > 0 Then
                Dim i As Integer
                For i = 0 To selectedRowCount - 1
                    intRwIndex = dgvSupplierSum.SelectedRows(i).Index
                Next i
                Dim SupplierID = dgvSupplierSum.Item("SupplierID", intRwIndex).Value
                SelectedSupplier = SupplierID

                Call LoadPayRequests(SelectedSupplier)
            Else
                LoadPayRequests()
            End If


            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured. " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub AddRow()
        Try
            Dim xAmount As Decimal

            Me.Cursor = Cursors.WaitCursor
            Me.Enabled = False

            If Me.dgvInvoiceList.SelectedCells.Count < 1 Then
                MsgBox("Please select an invoice to be paid by clicking a cell in the top box!")
                Exit Sub
            End If

            For Each row As DataGridViewRow In dgvInvoiceList.Rows

                If row.Selected = True Then

                    'Now gather the Amount and Pad cell values in top DGV
                    If IsNumeric(row.Cells(5).Value) = False Then
                        xAmount = 0
                    Else
                        xAmount = row.Cells(5).Value
                    End If

                    Date.Now.ToString("dd MMMM yyyy")
                    row.DefaultCellStyle.ForeColor = Color.OrangeRed

                    Dim oSupplier As String = row.Cells(3).Value
                    Dim oReference As String = row.Cells(2).Value

                    If Check_If_Supplier_And_Reference_in_DGV2(oSupplier, oReference) = False Then

                        Me.dgvPaymentsList.Rows.Add(row.Cells(2).Value, row.Cells(3).Value, row.Cells(4).Value, row.Cells(5).Value, _
                                                  dtpPaymentDate.Value.Date.ToShortDateString, cmbPaymentType.Text, row.Cells(6).Value)  'Date.Now.ToString("MM/dd/yyyy")
                        Dim DueColumn As DataGridViewColumn = dgvPaymentsList.Columns("Amount")
                        DueColumn.DefaultCellStyle.Format = "c"
                    End If

                End If

            Next

            Me.dgvInvoiceList.Refresh()
            Me.dgvInvoiceList.ClearSelection()

            Me.Cursor = Cursors.Arrow
            Me.Enabled = True

        Catch ex As Exception
            MessageBox.Show("An error occurred, please report to System support with details :", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
        End Try
    End Sub
    Private Sub Button_Delete_Click(sender As System.Object, e As System.EventArgs) Handles Button_Delete.Click

        Try

            If Me.dgvPaymentsList.SelectedRows.Count < 1 And Me.dgvPaymentsList.SelectedCells.Count < 1 Then
                MessageBox.Show("Please select the invoice you want to remove in order to continue", "Remove invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Me.Cursor = Cursors.AppStarting
            Me.Enabled = False
            'w
            'First Unrequest any Invoice Payments that are requested
            If My.Settings.User_Control_On = "Yes" And Me.dgvPaymentsList.SelectedCells.Count > 0 Then
                'make sure that rows are selected so select rows based on cells
                Dim oRowIndex As Integer
                For Each cell As DataGridViewCell In dgvPaymentsList.SelectedCells
                    oRowIndex = cell.RowIndex
                    dgvPaymentsList.Rows(oRowIndex).Selected = True
                Next

                'JUST USE THE ID NUMBER FROM DGV2 & CHANGE ROW IN DGV1 WITH THIS BACK FROM GREEN/ORANGE TO BLACK

                Dim oID As String
                Dim oReference As String
                Dim oSupplierName As String
                Dim oRow As Integer = Me.dgvPaymentsList.SelectedRows(0).Index

                oID = Me.dgvPaymentsList.Rows(oRow).Cells(0).Value
                oSupplierName = Me.dgvPaymentsList.Rows(oRow).Cells(1).Value
                oReference = Me.dgvPaymentsList.Rows(oRow).Cells(2).Value

                For Each row As DataGridViewRow In dgvInvoiceList.Rows 'Last line
                    If row.Cells(2).Value = oID Then

                        row.DefaultCellStyle.ForeColor = Color.Black

                        Call UnRequestPayment(oID)

                        If My.Settings.User_Control_On = "Yes" Then
                            If Me.chkTotalRequested.Checked = True Then
                                Me.chkTotalRequested.Checked = False
                                Me.chkTotalRequested.Checked = True
                            End If
                        End If

                    End If
                Next
            End If

            Call DeleteRow()

            Me.Cursor = Cursors.Arrow
            Me.Enabled = True

        Catch ex As Exception
            MessageBox.Show("An error occurred, please report to System support with details :", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
        End Try
    End Sub
    Public Sub UnRequestPayment(ByVal oID As String)

        'For i As Integer = 0 To Me.DataGridView2.Rows.Count - 2
        If My.Settings.DBType = "Access" Then
            Dim sSQL As String
            sSQL = "Update Transactions Set Info = 'Invoice' WHERE INFO LIKE '%Invoice Payment Requested%' And TransactionID = " & oID
            Dim cn As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, cn)
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
        ElseIf My.Settings.DBType = "SQL" Then
            Dim sSQL As String
            'd
            sSQL = "Update Transactions Set Info = 'Invoice' WHERE Info LIKE '%Invoice Payment Requested%' And TransactionID = " & oID
            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
        End If
        ''Next

    End Sub

    Sub DeleteRow()
        On Error Resume Next

        Dim S As String

        'make sure that rows are selected so select rows based on cells
        Dim oRowIndex As Integer
        For Each cell As DataGridViewCell In dgvPaymentsList.SelectedCells
            oRowIndex = cell.RowIndex
            dgvPaymentsList.Rows(oRowIndex).Selected = True
        Next

        For Each row As DataGridViewRow In dgvPaymentsList.SelectedRows
            If S = "" Then
                S = dgvPaymentsList.Rows(row.Index).Cells(0).Value 'gather the IDs
            Else
                S = "," & dgvPaymentsList.Rows(row.Index).Cells(0).Value
            End If
        Next

        For Each row As DataGridViewRow In dgvPaymentsList.SelectedRows
            dgvPaymentsList.Rows.Remove(row)
        Next

        'Now change the color in DGV back to black
        Dim arrSplit As Object = Split(S, ",")
        For j As Integer = 0 To Me.dgvInvoiceList.Rows.Count - 1
            For k As Integer = 0 To UBound(arrSplit)
                If Me.dgvInvoiceList.Rows(j).Cells(0).Value = CInt(arrSplit(k)) Then
                    If Me.dgvInvoiceList.Rows(j).DefaultCellStyle.ForeColor = Color.OrangeRed Then
                        Me.dgvInvoiceList.Rows(j).DefaultCellStyle.ForeColor = Color.Black
                    End If
                End If
            Next k
        Next j
        Me.dgvPaymentsList.Refresh()
    End Sub
    Sub TotalPaidBox()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.dgvInvoiceList.Rows.Count - 1
            oTotal = oTotal + Me.dgvInvoiceList.Rows(i).Cells("Due").Value
        Next
        Me.TextBox_Paid.Text = oTotal.ToString("c")
    End Sub
    Sub ToPay()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For Each row As DataGridViewRow In dgvPaymentsList.Rows
            oTotal = oTotal + row.Cells("Amount").Value
        Next
        Me.TextBox_Total.Text = oTotal.ToString("c")
    End Sub
    Public Function RequestedForPayment() As Decimal
        Dim oTotal As Decimal = 0
        For Each row As DataGridViewRow In dgvPaymentsList.Rows
            oTotal = oTotal + row.Cells("Amount").Value
        Next
        Return oTotal
    End Function
    Private Sub DataGridView2_CellBeginEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvPaymentsList.CellBeginEdit
        Try
            CellValueBeforeEdit = dgvPaymentsList.Rows(e.RowIndex).Cells("Amount").Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView2_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPaymentsList.CellEndEdit
        Try
            If My.Settings.PayInAdvance = "No" Or My.Settings.PayInAdvance = "" Then

                Dim CellValue As Decimal
                If Me.dgvPaymentsList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value <> "" Then
                    CellValue = Me.dgvPaymentsList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                Else
                    CellValue = 0
                End If

                If CellValue > CellValueBeforeEdit Then
                    MessageBox.Show("Pay in advance option is disabled, please activate the option in order to continue", "Over payment", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.dgvPaymentsList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = CellValueBeforeEdit
                End If

                Call TotalTextBox()

            End If
            ToPay()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DataGridView2_Click(sender As Object, e As System.EventArgs) Handles dgvPaymentsList.Click
        On Error Resume Next
        Dim oRowIndex As Integer
        For Each cell As DataGridViewCell In dgvPaymentsList.SelectedCells
            If cell.ColumnIndex < 3 Then 'don't interfere with last three columns as they have to be edited
                oRowIndex = cell.RowIndex
                dgvPaymentsList.Rows(oRowIndex).Selected = True
            End If
        Next
    End Sub
    Private Sub DataGridView2_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvPaymentsList.DragDrop

        Try
            Dim rows As DataGridViewSelectedRowCollection = TryCast(e.Data.GetData(GetType(DataGridViewSelectedRowCollection)), DataGridViewSelectedRowCollection)

            If cmbPaymentType.SelectedIndex = -1 Then
                MessageBox.Show("Please select payment Type", "Supplier Payments", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbPaymentType.Focus()
                Exit Sub
            End If



            For Each row In rows

                If row IsNot Nothing Then

                    Dim newRow As New DataGridViewRow()
                    Dim oAmount As Decimal
                    Dim oDue As Decimal

                    newRow.CreateCells(Me.dgvInvoiceList)


                    If CDec(row.cells("Due").value) <= 0 Then
                        MessageBox.Show("Payment cannot be made for negative or zero value(Amount Due)", "Payments", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If

                    If dgvPaymentsList.Rows.Count > 0 Then
                        For Each Drow As DataGridViewRow In dgvPaymentsList.Rows

                            Dim TransID As Integer = CInt(Drow.Cells("ID").Value)
                            Dim SupplierName As String = CStr(Drow.Cells("SupplierName").Value)
                            Dim Reference As String = CStr(Drow.Cells("Reference").Value)
                            Dim Amount As Decimal = CDec(Drow.Cells("Amount").Value)
                            Dim TransactionDate As Date = CDate(Drow.Cells("TransactionDate").Value)

                            If TransactionDate > dtpPaymentDate.Value Then
                                MessageBox.Show("Payment date cannot be before the transaction date", "Invalid payment date", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                                dtpPaymentDate.Focus()
                                Exit Sub
                            End If

                            If CInt(row.Cells("TransactionID").Value) = TransID And CStr(row.Cells("SupplierName").Value) = SupplierName _
                                And CStr(row.Cells("Reference").Value) = Reference And CDec(row.Cells("Due").Value) = Amount Then
                                MessageBox.Show("The invoice line you selected already exixts in invoice lines for payment, Please select different Invoice line", "Invoice Payments", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                                Exit Sub
                            End If

                        Next
                    End If

                    newRow.Cells(0).Value = row.Cells("TransactionID").Value
                    newRow.Cells(1).Value = row.Cells("SupplierName").Value
                    newRow.Cells(2).Value = row.Cells("Reference").Value
                    oAmount = row.Cells("Due").Value
                    newRow.Cells(3).Value = oAmount
                    InvoiceAmount = oAmount
                    newRow.Cells(4).Value = dtpPaymentDate.Value.Date.ToString("dd MMMM yyyy")
                    newRow.Cells(5).Value = Me.cmbPaymentType.Text

                    'next customize the 
                    Dim oSupplier As String = row.Cells(1).Value
                    Dim oReference As String = row.Cells(2).Value

                    dgvPaymentsList.Rows.Add(newRow)
                    ' dgvPaymentsList.Rows(0).Cells("PaymentType").Value = CStr(ComboBox_PaymentSelected.Text)
                    Dim DueColumn As DataGridViewColumn = dgvPaymentsList.Columns("Amount")
                    DueColumn.DefaultCellStyle.Format = "c"

                End If
                '***Color line orange -on the drop
                Me.dgvInvoiceList.Rows(row.Index).DefaultCellStyle.ForeColor = Color.OrangeRed
                Me.dgvInvoiceList.ClearSelection()

            Next
        Catch
            MsgBox(Err.Description)
        End Try

    End Sub



    Function Check_If_Supplier_And_Reference_in_DGV2(ByVal oSupplier As String, ByVal oReference As String) As Boolean

        Try
            Dim xSupplier As String
            Dim xReference As String

            Check_If_Supplier_And_Reference_in_DGV2 = False

            For i As Integer = 0 To Me.dgvPaymentsList.RowCount - 2
                xSupplier = Me.dgvPaymentsList.Rows(i).Cells(1).Value
                xReference = Me.dgvPaymentsList.Rows(i).Cells(2).Value
                If xSupplier = oSupplier And xReference = oReference Then
                    Check_If_Supplier_And_Reference_in_DGV2 = True
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Private Sub GetListIndexWithoutClicking()
        Dim LocalMousePosition As Point
        LocalMousePosition = Me.dgvPaymentsList.PointToClient(Cursor.Position)

        Dim x As Integer = LocalMousePosition.X
        Dim y As Integer = LocalMousePosition.Y

    End Sub

    Private Sub DataGridView2_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvPaymentsList.DragEnter
        Try
            e.Effect = DragDropEffects.All

        Catch
        End Try
    End Sub

    Private Sub DataGridView2_RowsAdded(sender As Object, e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles dgvPaymentsList.RowsAdded
        'If blnFormLoading = True Then Exit Sub
        Call TotalTextBox()
    End Sub

    Private Sub DataGridView2_RowsRemoved(sender As Object, e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles dgvPaymentsList.RowsRemoved
        'If blnFormLoading = True Then Exit Sub
        Call TotalTextBox()
    End Sub

    Sub TotalTextBox()
        If blnFormLoading = True Then Exit Sub
        If blnForm_Hiding = True Then Exit Sub
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.dgvPaymentsList.Rows.Count - 1
            oTotal = oTotal + Me.dgvPaymentsList.Rows(i).Cells(3).Value
        Next
        Me.TextBox_Total.Text = oTotal.ToString("c")
    End Sub
    Private Sub Button_Pay_Click(sender As System.Object, e As System.EventArgs) Handles Button_Pay.Click

        Try
            Button_Pay.BackColor = Color.DarkCyan
            Button_Pay.Enabled = False
            Dim UserControl As String = ""
            UserControl = My.Settings.User_Control_On

            If UserControl = "Yes" Then
                ''check there are payments waiting
                'If dgvPaymentsList.Rows.Count = 0 Then
                '    MessageBox.Show("There is no invoice to pay, please allocate a payment in order to continue", "Payments Validation", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                '    Exit Sub
                'End If

                'Me.Enabled = False
                'Me.Cursor = Cursors.AppStarting

                'Dim MailIterator As Integer = 0

                ''Create the array of payments
                'For Each Row As DataGridViewRow In dgvPaymentsList.Rows
                '    Dim TransactionID As Integer = CInt(Row.Cells("ID").Value)
                '    Dim Reference As String = CStr(Row.Cells("Reference").Value)
                '    Dim PaymentAmount As Decimal = CDec(Row.Cells("Amount").Value)
                '    Dim DateOfPayment As String = CStr(Row.Cells("TransactionDate").Value)
                '    Dim PaymentType As String = CStr(Row.Cells("PaymentType").Value)
                '    Dim SupplierName As String = CStr(Row.Cells("SupplierName").Value)

                '    oNumberPayments = 0

                '    If TransactionID <> 0 Or TransactionID <> Nothing Then

                '        oNumberPayments = oNumberPayments + 1
                '        arrID(MailIterator) = TransactionID
                '        arrSupplierName(MailIterator) = SupplierName
                '        arrReference(MailIterator) = Reference
                '        arrDateOfPayment(MailIterator) = DateOfPayment
                '        arrPaymentAmount(MailIterator) = PaymentAmount
                '        arrPaymentType(MailIterator) = PaymentType


                '        'Wyndham add this test later - Do a select distinct test
                '        'Dim Test As String = Get_Segm1Desc_From_DisplayName_in_Accounting(arrPaymentType(MailIterator))
                '        'If Test = Nothing Then
                '        ' MsgBox("Transaction aborted! Payment '" & arrPaymentType(MailIterator) & "' at line " & CStr(MailIterator) & " incorrect. No 'Display Name' item has been set up in the Accounting table for this payment type!")
                '        ' Exit Sub
                '        'End If

                '        IfUserControlProcessON(TransactionID, PaymentType, PaymentAmount, Reference, DateOfPayment)
                '        dgvPaymentsList.Rows(Row.Index).Visible = False
                '    End If
                'Next

                'Dim msgString As String = "Payment(s) Results :" & vbCrLf & vbCrLf
                'If PaymentsRequestsN <> 0 Then
                '    msgString &= PaymentsRequestsN.ToString & " payment(s) requests succesfuly made." & vbCrLf
                'End If
                'If PaymentsRequestAdvN <> 0 Then
                '    msgString &= PaymentsRequestAdvN.ToString & " payment(s) requests with advance payment(s) succesfuly made." & vbCrLf
                'End If
                'If PaymentsN <> 0 Then
                '    msgString &= PaymentsN.ToString & " payment(s) succesfuly made." & vbCrLf
                'End If
                'If PayInAdvanceN <> 0 Then
                '    msgString &= PayInAdvanceN.ToString & " payment(s) with advance payment(s) succesfuly made." & vbCrLf
                'End If
                'If msgString = CStr("Payment(s) Results :" & vbCrLf & vbCrLf) Then
                '    MessageBox.Show("No payment(s) were processed", "Payments Results")
                '    PaymentsRequestsN = 0
                '    PaymentsRequestAdvN = 0
                '    PaymentsN = 0
                '    PayInAdvanceN = 0
                'Else
                '    MessageBox.Show(msgString, "Payment(s) Summary", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    PaymentsRequestsN = 0
                '    PaymentsRequestAdvN = 0
                '    PaymentsN = 0
                '    PayInAdvanceN = 0
                'End If

                'dgvPaymentsList.Rows.Clear()
                'oLoadData()

            ElseIf UserControl = "No" Or UserControl = "" Then

                Dim DateInQuesting As Date = dtpPaymentDate.Value.ToString("dd MMM yyyy")

                If cmbPaymentType.Text = "CASH" Then

                    If CheckForCashup(DateInQuesting) > 0 Then
                        MessageBox.Show("There where cashups done on the invoice date you selected, you cannot capture a paid invoice on this date, please contant the MagicBox support team in order to resolve this.", "Invoice capture", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        Me.Enabled = True
                        Me.Cursor = Cursors.Arrow
                        Exit Sub
                    End If

                End If

                If dgvPaymentsList.Rows.Count = 0 Then
                    MessageBox.Show("There is no invoice to pay, please allocate a payment in order to continue", "Payments Validation", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub
                End If

                Me.Enabled = False
                Me.Cursor = Cursors.AppStarting

                Call Pay()

                Enabled = True
                Cursor = Cursors.Arrow
                dgvPaymentsList.Rows.Clear()
                oLoadData()


            End If
            Button_Pay.BackColor = Color.FromKnownColor(KnownColor.Control)
            Button_Pay.Enabled = True
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try
        ' Supplier_Payments_Load(sender, e)
    End Sub
    Public Sub RequestPayment(ByVal oID As String, ByVal oSupplierName As String, ByVal oReference As String, _
                               ByVal oDateOfPayment As String, ByVal oPaymentAmount As String, ByVal oPaymentType As String)
        Try
            'For i As Integer = 0 To Me.DataGridView2.Rows.Count - 2
            Dim AccountingValue As String = ""
            If My.Settings.User_Control_On = "Yes" Then
                If My.Settings.Auto_Approve_EFT = "Yes" Then
                    AccountingValue = "1"
                Else
                    AccountingValue = "0"
                End If
            Else
                If My.Settings.Auto_Approve_CreditNotes = "Yes" And My.Settings.Auto_Approve_EFT = "Yes" Then
                    AccountingValue = "1"
                End If
                If My.Settings.Auto_Approve_CreditNotes <> "Yes" And My.Settings.Auto_Approve_EFT <> "Yes" Then
                    AccountingValue = "0"
                End If
            End If

            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice Payment Requested|" & oPaymentAmount & "|" & oPaymentType & "', Accounting = '" & 0 & "' WHERE Info = 'Invoice'"
                sSQL = sSQL & " And Reference = '" & oReference & "' And SupplierName = '" & SQLConvert(oSupplierName) & "' And Co_ID = " & My.Settings.Setting_CompanyID
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice Payment Requested|" & oPaymentAmount & "|" & oPaymentType & "', Accounting = '" & 0 & "' WHERE Info = 'Invoice'"
                sSQL = sSQL & " AND Reference = '" & oReference & "' And SupplierName = '" & SQLConvert(oSupplierName) & "' And Co_ID = " & My.Settings.Setting_CompanyID
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300

                cn.Open()
                cmd.ExecuteNonQuery()
            End If
            ''Next

        Catch ex As Exception
            MsgBox(ex.Message & " 325d")
        End Try
    End Sub
    Public Sub Nullify_RequestPayment(ByVal oTransID As String)

        Try

            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice' WHERE Mid(Info,1,25) = 'Invoice Payment Requested' And TransactionID = " & oTransID
                sSQL = sSQL & " Co_ID = " & My.Settings.Setting_CompanyID
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice' WHERE SubString(Info,1,25) = 'Invoice Payment Requested' And TransactionID = " & oTransID
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300

                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 326")
        End Try

    End Sub

    Public Sub Pay()

        Try
            Me.Label_PaymentProcessed.Text = ""
            oNumberPayments = 0

            Dim strPayExclusions()
            strPayExclusions = Split(Globals.Ribbons.Ribbon1.ExcludedPayTypes, "|")

            For i As Integer = 0 To Me.dgvPaymentsList.Rows.Count - 1

                oNumberPayments = oNumberPayments + 1
                arrID(i) = Me.dgvPaymentsList.Rows(i).Cells(0).Value '0
                arrSupplierName(i) = Me.dgvPaymentsList.Rows(i).Cells(1).Value '1
                arrReference(i) = Me.dgvPaymentsList.Rows(i).Cells(2).Value '2
                arrDateOfPayment(i) = Me.dgvPaymentsList.Rows(i).Cells(4).Value '3
                arrPaymentAmount(i) = Me.dgvPaymentsList.Rows(i).Cells(3).Value '4
                arrPaymentType(i) = Me.dgvPaymentsList.Rows(i).Cells(5).Value ' 5

                'Load Remittance info
                Globals.Ribbons.Ribbon1.RemittanceTable.Clear()
                RemittanceDataTable_AddRow(arrID(i), arrSupplierName(i), GetSupplierID(arrSupplierName(i)), arrDateOfPayment(i), arrReference(i), _
                arrPaymentAmount(i), arrPaymentType(i))


                For Each y In strPayExclusions
                    If arrPaymentType(i) = y Then
                        MsgBox("Transaction aborted! Payment '" & arrPaymentType(i) & "' is not authorised for your user role!")
                        Exit Sub
                    End If
                Next

                Dim Test As String = Get_Segm1Desc_From_DisplayName_in_Accounting(arrPaymentType(i))
                If Test = Nothing Then
                    MsgBox("Transaction aborted! Payment '" & arrPaymentType(i) & "' at line " & CStr(i) & " incorrect. No 'Display Name' item has been set up in the Accounting table for this payment type!")
                    Exit Sub
                End If

            Next

            sTransactionDate = Me.dtpPaymentDate.Value.Date.ToString("dd MMMM yyyy")

            Pay2()

            If oCheck_Integrity() = False Then
                MessageBox.Show("The numbers are zero or out of balance and cannot be saved!", "Data Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Else
                If My.Settings.DBType = "SQL" Then
                    Call AppendTransactions_SQL()
                    Call SendRemittance()
                End If
            End If

            Me.DGV_Transactions.Rows.Clear()
            dgvPaymentsList.Rows.Clear()

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Public Sub Pay2()

        Try

            Dim Info As String
            Dim Info2 As String
            Dim Info3 As String

            oBatch_or_Transaction_generation_Failed = False

            oBatchID = CLng(oCreateBatchID())
            oProgressBar = Me.ProgressBar1
            oProgressBar.Visible = True
            oProgressBar.Minimum = 1
            oProgressBar.Maximum = 100
            oProgressBar.Value = 1

            blnCRITICAL_ERROR = False
            Dim oPaymentType As String

            Dim sCaptureDate As String = _
            Date.Now.ToString("dd MMM yyyy") & " " & Date.Now.ToLongTimeString
            Dim oTransID As Integer
            Dim sDescriptionCode As String
            Dim oDocType As String
            Dim oNewTransactionID As Integer

            Dim oProgressBarIncrements As Integer
            If Me.dgvPaymentsList.RowCount > 0 Then
                oProgressBarIncrements = Int(80 / Me.dgvPaymentsList.RowCount)
            Else
                oProgressBarIncrements = 0
            End If

            Dim BolHasAdvance As Boolean = False

            For i As Integer = 0 To oNumberPayments - 1

                Info = ""
                Info2 = ""
                Info3 = ""
                'MDB
                If oProgressBar.Value + oProgressBarIncrements < 100 Then
                    oProgressBar.Value = oProgressBar.Value + oProgressBarIncrements
                Else
                    oProgressBar.Value = 100
                End If

                oPaymentType = arrPaymentType(i)

                oLinkID = CInt(arrID(i)) 'Link to original invoice
                Call Load_Invoice_Data_From_ID_to_Variables(oLinkID)
                oNewTransactionID = CLng(oCreateTransactionID())

                If CStr(arrDateOfPayment(i)) <> "" Then
                    dTransactionDate = CDate(arrDateOfPayment(i)) 'if there is a date here then use otherwise use general one on form
                Else
                    dTransactionDate = Now.Date.ToString("dd MMMM yyyy")
                End If

                dCaptureDate = Now.Date.ToString("dd MMM yyyy hh:mm:ss")
                sPPeriod = GetPeriod(dTransactionDate.ToString("dd MMMM yyyy"))
                sGDC = "G" 'General/Debtors/Creditors
                sDescription = oSupplierName
                oAmount = arrPaymentAmount(i)
                oTaxType = 0
                oTaxAmount = 0

                'This used to be based on old approval system now instantly set to Approved
                '   If My.Settings.Auto_Approve_EFT = "Yes" Or My.Settings.Auto_Approve_EFT = "" Or My.Settings.Auto_Approve_EFT = Nothing Then
                '       blnAccounting = True
                '   Else
                '       blnAccounting = False
                '   End If


                blnAccounting = True
                Info = oPaymentType
                TransactionType = 1
                sDRCR = "Dr"

                Dim sCreditorsControl_Segment4 As String
                sCreditorsControl_Segment4 = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
                sDescriptionCode = sCreditorsControl_Segment4
                If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                    MsgBox("No Magic Box Segment 4 code for 'Creditors Control'!")
                    Exit Sub
                End If

                oDocType = "Payment"

                'This is so all line items can be manipulated at once

                sFinYear = GetFinYear(sTransactionDate)

                Dim PayInAdvanceAmount As Decimal = 0
                Dim OriginalAmount As Decimal = CDec(GetOriginalInvAmount(oLinkID))

                If My.Settings.PayInAdvance = "Yes" Then
                    If oAmount > OriginalAmount Then
                        PayInAdvanceAmount = oAmount - OriginalAmount
                        oAmount = OriginalAmount
                    End If
                Else
                    If oAmount > OriginalAmount Then
                        MessageBox.Show("You supplied amount more than the original amount in the invoice" & _
                        ", payment will be commited using the original invoice amount." & _
                        " If you want to pay the supplier in advance please activate 'Pay in advance' on the settings screen.", "Invoice amount", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If

                Call Add_Transaction_DGV(oBatchID, oNewTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                    sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, oDocType, oSupplierName, Info, Info2, Info3)

                If PayInAdvanceAmount > 0 Then

                    BolHasAdvance = True

                    PayInBatchID = oBatchID
                    PayInTransID = CLng(oCreateTransactionID())
                    PayInLinkID = 0
                    PayInTransDate = Date.Now.ToString("dd MMMM yyyy")
                    PayInCaptureDate = Date.Now.ToString("dd MMMM yyyy")
                    PayInPeriod = GetPeriod(PayInTransDate)
                    PayInFinYear = GetFinYear(PayInTransDate)
                    PayInSGDC = sGDC
                    PayInInvNumber = sInvoiceNumber & "_DN"
                    PayInDescription = sDescription
                    PayInAccNumber = sAccountNumber
                    PayInLinkAcc = sLinkAcc
                    PayInAmount = PayInAdvanceAmount
                    PayInTaxType = oTaxType
                    PayInTaxAmount = oTaxAmount
                    PayInUserID = sUserID
                    PayInSupplierID = oSupplierID
                    PayInEmpID = 0
                    PayInDesc2 = sDescription2
                    PayInDesc3 = sDescription3
                    PayInDesc4 = sDescription4
                    PayInDesc5 = sDescription5
                    PayInBlnPosted = blnPosted
                    PayInBlnAccounting = True
                    PayInBlnVoid = blnVoid
                    PayInTransactionType = TransactionType
                    PayInDRCR = sDRCR
                    PayInContraAccount = sContraAccount
                    PayInDescCode = sDescriptionCode
                    PayInDocType = oDocType
                    PayInSuppName = oSupplierName
                    PayInInfo = "Supplier Debit"
                    PayInfo2 = "Supplier Debit"
                    PayInInfo3 = Info3
                End If


                sDescription = oPaymentType
                TransactionType = 1
                oTaxType = 0
                oTaxAmount = 0
                sDRCR = "Cr"

                sDescription = Get_Segm1Desc_From_DisplayName_in_Accounting(oPaymentType)
                sDescription2 = Get_Segm2Desc_From_DisplayName_in_Accounting(oPaymentType)
                sDescription3 = Get_Segm3Desc_From_DisplayName_in_Accounting(oPaymentType)
                sDescriptionCode = Get_Segment4_from_DisplayName_in_Accounting(oPaymentType)
                sAccountNumber = Get_GLACCOUNT_from_DisplayName_in_Accounting(oPaymentType)

                oDocType = ""
                Info = ""
                Info2 = "Payment" 'This is so all line items can be manipulated at once

                Call Add_Transaction_DGV(oBatchID, oNewTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
                oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, oDocType, oSupplierName, Info, Info2, Info3)

                If PayInAdvanceAmount > 0 Then

                    Call Add_Transaction_DGV(PayInBatchID, PayInTransID, PayInLinkID, PayInTransDate, PayInCaptureDate, PayInPeriod, PayInFinYear, PayInSGDC, PayInInvNumber, _
                    PayInDescription, PayInAccNumber, PayInLinkAcc, PayInAdvanceAmount, PayInTaxType, PayInTaxAmount, PayInUserID, _
                    PayInSupplierID, PayInEmpID, PayInDesc2, PayInDesc3, PayInDesc4, PayInDesc5, PayInBlnPosted, _
                    PayInBlnAccounting, PayInBlnVoid, PayInTransactionType, PayInDRCR, PayInContraAccount, PayInDescCode, PayInDocType, PayInSuppName, PayInInfo, PayInfo2, PayInInfo3)

                    Call Add_Transaction_DGV(PayInBatchID, PayInTransID, PayInLinkID, PayInTransDate, PayInCaptureDate, PayInPeriod, PayInFinYear, sGDC, PayInInvNumber, _
                    sDescription, sAccountNumber, sLinkAcc, PayInAdvanceAmount, oTaxType, oTaxAmount, sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    PayInBlnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, oDocType, oSupplierName, "", PayInfo2, Info3)

                End If

            Next

            oProgressBar.Value = 100
            oProgressBar.Visible = False

            If BolHasAdvance Then
                MessageBox.Show("Payment(s) succesfully made with Pay in advance transaction(s)", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                BolHasAdvance = False
            Else
                MessageBox.Show("Payment(s) succesfully made.", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured :" & ex.Message, "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error)
        End Try
    End Sub
    Dim BolHasAdvance As Boolean = False
    Dim IFError As Boolean = False
    Public Sub PayForUserControlOn(ByVal RequestID As Integer, ByVal TransactionID As Integer, ByVal PaymentType As String, _
                                   ByVal Requested As Decimal, ByVal AmountTopay As Decimal, _
                                   ByVal InvTransaction As Transaction, ByVal PaymentDate As String)

        Try

            Dim InInfo As String = ""
            Dim InInfo2 As String = ""
            Dim InInfo3 As String = ""
            Dim InBatchID As Long = Nothing
            Dim InTransID As Long = Nothing
            Dim InDescriptionCode As String = ""
            Dim InDocType As String = ""
            Dim InNewTransactionID As Long = Nothing
            Dim InlinkID As Long = Nothing
            Dim inTransactionDate As String = ""
            Dim InPaymentType As String = ""
            Dim InCaptureDate As String = Date.Now.ToString("dd MMM yyyy") & " " & Date.Now.ToLongTimeString
            Dim InPeriod As String = ""
            Dim InFinYear As String = ""
            Dim InDescription As String = ""
            Dim InInvoiceAmount As Decimal = InvTransaction.Amount
            Dim InAmountTopay As Decimal = AmountTopay
            Dim InTaxType As String = ""
            Dim InTaxAmount As Decimal = Nothing
            Dim InTransactionType As Integer = Nothing
            Dim InDRCR As String = ""
            Dim InAccNumber As String = ""

            InBatchID = CLng(oCreateBatchID())
            InTransID = CLng(oCreateTransactionID())
            InPaymentType = PaymentType

            InlinkID = TransactionID
            'Load_Invoice_Data_From_ID_to_Variables(InlinkID)
            inTransactionDate = CStr((CDate(InvTransaction.Transaction_Date).ToString("dd MMM yyyy")))

            oProgressBar = Me.ProgressBar1
            oProgressBar.Visible = True
            oProgressBar.Minimum = 1
            oProgressBar.Maximum = 100
            oProgressBar.Value = 1

            Dim oProgressBarIncrements As Integer
            If Me.dgvPaymentsList.RowCount > 0 Then
                oProgressBarIncrements = Int(80 / Me.dgvPaymentsList.RowCount)
            Else
                oProgressBarIncrements = 0
            End If

            If oProgressBar.Value + oProgressBarIncrements < 100 Then
                oProgressBar.Value = oProgressBar.Value + oProgressBarIncrements
            Else
                oProgressBar.Value = 100
            End If

            InPeriod = GetPeriod(PaymentDate)
            InFinYear = GetFinYear(PaymentDate)
            Dim inGDC As String = "G"
            InDescription = InvTransaction.SupplierName.ToString

            InTaxType = 0
            InTaxAmount = 0

            If My.Settings.Auto_Approve_EFT = "Yes" Then
                blnAccounting = True
            Else
                blnAccounting = False
            End If

            If My.Settings.Auto_Approve_EFT = "Yes" Then
                blnAccounting = True
            Else
                blnAccounting = False
            End If

            InInfo = InPaymentType
            InTransactionType = 1
            InDRCR = "Dr"

            Dim InCreditorsControlSegment4 As String
            InCreditorsControlSegment4 = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
            InDescriptionCode = InCreditorsControlSegment4
            If InDescriptionCode = "" Or IsDBNull(InDescriptionCode) Then
                MessageBox.Show("No Magic Box Segment 4 code for 'Creditors Control', rectify or call MB support for assistance!", "Creditors Control missing code", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            InAccNumber = InvTransaction.AccNumber.ToString
            InDocType = "Payment"
            InInfo2 = "Payment"

            Dim PayInAdvanceAmount As Decimal = 0

            If My.Settings.PayInAdvance = "Yes" Then
                If InAmountTopay > InInvoiceAmount Then
                    PayInAdvanceAmount = InAmountTopay - InInvoiceAmount
                    InAmountTopay = InInvoiceAmount
                End If
            Else
                If InAmountTopay > InInvoiceAmount Then
                    MessageBox.Show("You supplied amount more than the original amount in the invoice" & _
                    ", payment will be commited using the original invoice amount." & _
                    " If you want to pay the supplier in advance please activate 'Pay in advance' on the settings screen.", "Invoice amount", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If

            Add_Transaction_DGV(InBatchID, InTransID, InlinkID, PaymentDate, InCaptureDate, InPeriod, InFinYear, inGDC, CStr(InvTransaction.Reference), _
                CStr(InvTransaction.Description), InAccNumber, CStr(InvTransaction.LinkAcc), InAmountTopay, InTaxType, InTaxAmount, "", _
                CInt(InvTransaction.SupplierID), 0, CStr(InvTransaction.Description_2), CStr(InvTransaction.Description_3), CStr(InvTransaction.Description_4), CStr(InvTransaction.Description_5), 0, _
                CBool(InvTransaction.Accounting), CBool(InvTransaction.Void), InTransactionType, InDRCR, CStr(InvTransaction.Contra_Account), InDescriptionCode, InDocType, CStr(InvTransaction.SupplierName), InInfo, InInfo2, InInfo3)

            InDescription = InPaymentType
            TransactionType = 1
            InTaxType = 0
            InTaxAmount = 0
            InDRCR = "Cr"

            Dim InDescription2 As String = ""
            Dim InDescription3 As String = ""
            Dim InAccountNumber As String = ""

            InDescription = Get_Segm1Desc_From_DisplayName_in_Accounting(InPaymentType)
            InDescription2 = Get_Segm2Desc_From_DisplayName_in_Accounting(InPaymentType)
            InDescription3 = Get_Segm3Desc_From_DisplayName_in_Accounting(InPaymentType)
            InDescriptionCode = Get_Segment4_from_DisplayName_in_Accounting(InPaymentType)
            InAccountNumber = Get_GLACCOUNT_from_DisplayName_in_Accounting(InPaymentType)

            'set oDocType to nothing
            InDocType = ""
            InInfo = ""
            InInfo2 = "Payment" 'This is so all line items can be manipulated at once


            Call Add_Transaction_DGV(InBatchID, InTransID, InlinkID, PaymentDate, InCaptureDate, InPeriod, InFinYear, inGDC, CStr(InvTransaction.Reference), _
            InDescription, InAccountNumber, CStr(InvTransaction.LinkAcc), InAmountTopay, InTaxType, InTaxAmount, "", _
            CInt(InvTransaction.SupplierID), CInt(InvTransaction.EmployeeID), InDescription2, InDescription3, CStr(InvTransaction.Description_4), CStr(InvTransaction.Description_5), 0, _
            CBool(InvTransaction.Accounting), CBool(InvTransaction.Void), InTransactionType, InDRCR, CStr(InvTransaction.Contra_Account), InDescriptionCode, InDocType, CStr(InvTransaction.SupplierName), InInfo, InInfo2, InInfo3)


            If PayInAdvanceAmount > 0 Then

                BolHasAdvance = True
                InDRCR = "Dr"

                PayInBatchID = InBatchID

                PayInTransID = CLng(oCreateTransactionID())
                PayInLinkID = 0
                PayInTransDate = Now.ToString("dd MMM yyyy")
                PayInCaptureDate = Now.ToString("dd MMM yyyy") & " " & Now.ToShortTimeString
                PayInPeriod = InPeriod
                PayInFinYear = InFinYear
                PayInSGDC = inGDC
                PayInInvNumber = CStr(InvTransaction.Reference) & "_DN"
                PayInDescription = InDescription
                PayInAccNumber = CStr(InvTransaction.AccNumber)
                PayInLinkAcc = CStr(InvTransaction.LinkAcc)
                PayInAmount = PayInAdvanceAmount
                PayInTaxType = InTaxType
                PayInTaxAmount = InTaxAmount
                PayInUserID = ""
                PayInSupplierID = CInt(InvTransaction.SupplierID)
                PayInEmpID = 0
                PayInDesc2 = CStr(InvTransaction.Description_2)
                PayInDesc3 = CStr(InvTransaction.Description_3)
                PayInDesc4 = CStr(InvTransaction.Description_4)
                PayInDesc5 = CStr(InvTransaction.Description_5)
                PayInBlnPosted = False
                PayInBlnAccounting = True
                PayInBlnVoid = False
                PayInTransactionType = 1
                PayInDRCR = InDRCR
                PayInContraAccount = CStr(InvTransaction.Contra_Account)
                PayInDescCode = InCreditorsControlSegment4
                PayInDocType = "Payment"
                PayInSuppName = CStr(InvTransaction.SupplierName)
                PayInInfo = "Supplier Debit"
                PayInfo2 = "Supplier Debit"
                PayInInfo3 = InInfo3

                Call Add_Transaction_DGV(PayInBatchID, PayInTransID, PayInLinkID, PaymentDate, PayInCaptureDate, PayInPeriod, PayInFinYear, PayInSGDC, PayInInvNumber, _
                PayInDescription, PayInAccNumber, PayInLinkAcc, PayInAdvanceAmount, PayInTaxType, PayInTaxAmount, PayInUserID, _
                PayInSupplierID, PayInEmpID, PayInDesc2, PayInDesc3, PayInDesc4, PayInDesc5, PayInBlnPosted, _
                PayInBlnAccounting, PayInBlnVoid, PayInTransactionType, PayInDRCR, PayInContraAccount, PayInDescCode, PayInDocType, PayInSuppName, PayInInfo, PayInfo2, PayInInfo3)

                InDRCR = "Cr"

                PayInDescription = Get_Segm1Desc_From_DisplayName_in_Accounting(InPaymentType)
                PayInDesc2 = Get_Segm2Desc_From_DisplayName_in_Accounting(InPaymentType)
                PayInDesc3 = Get_Segm3Desc_From_DisplayName_in_Accounting(InPaymentType)
                PayInDescCode = Get_Segment4_from_DisplayName_in_Accounting(InPaymentType)
                PayInAccNumber = Get_GLACCOUNT_from_DisplayName_in_Accounting(InPaymentType)

                Call Add_Transaction_DGV(PayInBatchID, PayInTransID, PayInLinkID, PaymentDate, PayInCaptureDate, PayInPeriod, PayInFinYear, PayInSGDC, PayInInvNumber, _
                PayInDescription, PayInAccNumber, PayInLinkAcc, PayInAdvanceAmount, PayInTaxType, PayInTaxAmount, PayInUserID, _
                PayInSupplierID, PayInEmpID, PayInDesc2, PayInDesc3, "", "", PayInBlnPosted, _
                PayInBlnAccounting, PayInBlnVoid, PayInTransactionType, InDRCR, sContraAccount, PayInDescCode, "", PayInSuppName, "", PayInfo2, "")

            End If

            oProgressBar.Value = 100
            oProgressBar.Visible = False

            If BolHasAdvance Then
                If RequestID <> Nothing Then
                    If RequestID <> Nothing Then
                        If AmountTopay < Requested Then
                            MBData.UpdateRequest(RequestID, False, InAmountTopay, InTransID, Nothing)
                        Else
                            MBData.UpdateRequest(RequestID, True, InAmountTopay, InTransID, PayInTransID)
                        End If
                    End If
                End If
            Else
                BolHasAdvance = False
                If RequestID <> Nothing Then
                    If AmountTopay < Requested Then
                        MBData.UpdateRequest(RequestID, False, InAmountTopay, InTransID, Nothing)
                    Else
                        MBData.UpdateRequest(RequestID, True, InAmountTopay, InTransID, PayInTransID)
                    End If
                End If
            End If

            If My.Settings.EmailAll = "Yes" Or My.Settings.DefaultEmailAddress = "Yes" Then
                Call oEmail_Payment_Info(CInt(InvTransaction.SupplierID))
            End If

        Catch ex As Exception
            IFError = True
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub oEmail_Payment_Info(ByVal SupplierID As Integer)
        Try
            Dim oNextSupplier As String
            Dim oSupplierName As String
            Dim oTo As String
            Dim oCc As String
            Dim oSubject As String
            Dim oBody As String
            Dim oAttachment As String
            oBody = "This is a notification email to confirm that the following payments have been made to your organisation:" & " " & vbCrLf

            For i As Integer = 0 To oNumberPayments - 1

                If Not IsNothing(arrReference(i)) And Not IsNothing(arrPaymentAmount(i)) Then
                    oBody = oBody & vbCrLf & "Invoice Number: " & arrReference(i) & ", Amount: " & arrPaymentAmount(i) & ", Payment Type: " & arrPaymentType(i) & "    " & vbCrLf
                End If

                If i >= 0 And i < oNumberPayments - 1 Then
                    oNextSupplier = arrSupplierName(i)
                    If oSupplierName = oNextSupplier Then
                        'Do nothing - keep building oBody
                    Else
                        oBody = oBody & vbCrLf & "Thank you," & vbCrLf & CStr(GetCompanyName())
                        oSubject = "Payment(s) were made by : " & CStr(GetCompanyName()) & " to Supplier : " & CStr(GetSupplierName(SupplierID)) & " on the : " & CStr(Date.Now.ToString("dd MMM yyyy"))

                        If My.Settings.EmailAll = "Yes" Then
                            oTo = CStr(My.Settings.DefaultEmailAddress)
                            oCc = Get_Supplier_Email(SupplierID)
                        End If

                        If My.Settings.EmailDefault = "Yes" Then
                            oTo = CStr(My.Settings.DefaultEmailAddress)
                        End If

                        oSendEmail(oTo, oCc, oSubject, oBody, "")
                        oBody = "This is a notification email to confirm that the following payments have been made to your organisation:" & "    " & vbCrLf
                    End If
                ElseIf i = oNumberPayments - 1 Then 'Last one in list
                    oBody = oBody & vbCrLf & "Thank you," & vbCrLf & CStr(GetCompanyName())
                    oSubject = "Payment made by : " & CStr(GetCompanyName()) & " to Supplier : " & CStr(GetSupplierName(SupplierID)) & " on the : " & CStr(Date.Now.ToString("dd MMM yyyy"))

                    If My.Settings.EmailAll = "Yes" Then
                        oTo = CStr(My.Settings.DefaultEmailAddress)
                        oCc = Get_Supplier_Email(SupplierID)
                    End If

                    If My.Settings.EmailDefault = "Yes" Then
                        oTo = CStr(My.Settings.DefaultEmailAddress)
                    End If

                    oSendEmail(oTo, oCc, oSubject, oBody, "")
                End If
                i += 1
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Add_Transaction_DGV(ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                         ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                         ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                         ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                         ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                         ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                         ByVal oDocType As String, ByVal oSupplierName As String, ByVal oInfo As String, ByVal oInfo2 As String, ByVal oInfo3 As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)
            'newRow.Cells(3).Value = CStr(Format(dTransactionDate, "yyy-MMM-dd"))
            'newRow.Cells(4).Value = CStr(Format(dCaptureDate, "yyy-MMM-dd"))
            'newRow.Cells(3).Value = CStr(dTransactionDate.Year) & "-" & CStr(dTransactionDate.Month) & "-" & CStr(dTransactionDate.Day)
            'newRow.Cells(4).Value = CStr(dCaptureDate.Year) & "-" & CStr(dCaptureDate.Month) & "-" & CStr(dCaptureDate.Day)
            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            'newRow.Cells(22).Value = CStr(blnPosted)
            'newRow.Cells(23).Value = CStr(blnAccounting)
            'newRow.Cells(24).Value = CStr(blnVoid)
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName
            newRow.Cells(31).Value = oInfo
            newRow.Cells(32).Value = oInfo2
            newRow.Cells(33).Value = oInfo3

            Me.DGV_Transactions.Rows.Add(newRow) '

        Catch ex As Exception
            MsgBox(ex.Message & " 058")
        End Try


    End Sub
    Sub AppendTransactions()

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String


        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To Me.DGV_Transactions.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = Me.DGV_Transactions.Rows(i).Cells(0).Value
                oTransactionID = Me.DGV_Transactions.Rows(i).Cells(1).Value
                oLinkID = Me.DGV_Transactions.Rows(i).Cells(2).Value

                strTransactionDate = Me.DGV_Transactions.Rows(i).Cells(3).Value
                strCaptureDate = Me.DGV_Transactions.Rows(i).Cells(4).Value

                sPPeriod = Me.DGV_Transactions.Rows(i).Cells(5).Value
                sFinYear = Me.DGV_Transactions.Rows(i).Cells(6).Value
                sGDC = Me.DGV_Transactions.Rows(i).Cells(7).Value
                sReference = Me.DGV_Transactions.Rows(i).Cells(8).Value
                sDescription = Me.DGV_Transactions.Rows(i).Cells(9).Value
                sAccountNumber = Me.DGV_Transactions.Rows(i).Cells(10).Value
                sLinkAcc = Me.DGV_Transactions.Rows(i).Cells(11).Value
                oAmount = Me.DGV_Transactions.Rows(i).Cells(12).Value
                oTaxType = Me.DGV_Transactions.Rows(i).Cells(13).Value
                oTaxAmount = Me.DGV_Transactions.Rows(i).Cells(14).Value
                sUserID = Me.DGV_Transactions.Rows(i).Cells(15).Value
                oSupplierID = Me.DGV_Transactions.Rows(i).Cells(16).Value
                oEmployeeID = Me.DGV_Transactions.Rows(i).Cells(17).Value
                sDescription2 = Me.DGV_Transactions.Rows(i).Cells(18).Value
                sDescription3 = Me.DGV_Transactions.Rows(i).Cells(19).Value
                sDescription4 = Me.DGV_Transactions.Rows(i).Cells(20).Value
                sDescription5 = Me.DGV_Transactions.Rows(i).Cells(21).Value
                blnPosted = Me.DGV_Transactions.Rows(i).Cells(22).Value
                blnAccounting = Me.DGV_Transactions.Rows(i).Cells(23).Value
                blnVoid = Me.DGV_Transactions.Rows(i).Cells(24).Value
                sTransactionType = Me.DGV_Transactions.Rows(i).Cells(25).Value
                oDR_CR = Me.DGV_Transactions.Rows(i).Cells(26).Value
                oContraAccount = Me.DGV_Transactions.Rows(i).Cells(27).Value
                oDescriptionCode = Me.DGV_Transactions.Rows(i).Cells(28).Value
                oDocType = Me.DGV_Transactions.Rows(i).Cells(29).Value
                oSupplierName = Me.DGV_Transactions.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO Transactions ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"
                End If


                '// define the sql statement to execute
                'Dim cmd As New OleDbCommand(sSQL, cn)
                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next



            trans.Commit()  '+++++++ Transaction and rollback ++++++++

            cmd = Nothing



            'System.Threading.Thread.Sleep(1200) 'A little append to make sure it hits the database before any further code is written

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++

            MsgBox(ex.Message & " 059")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True

        Finally

            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL()

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String 'Must be string - If Boolean then will turns a -1 into True
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String



        Dim i As Integer
        Dim cmd As SqlCommand


        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To Me.DGV_Transactions.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = Me.DGV_Transactions.Rows(i).Cells(0).Value
                oTransactionID = Me.DGV_Transactions.Rows(i).Cells(1).Value
                oLinkID = Me.DGV_Transactions.Rows(i).Cells(2).Value


                strTransactionDate = Me.DGV_Transactions.Rows(i).Cells(3).Value
                strCaptureDate = Date.Now.ToString("dd MMM yyyy") & " " & Date.Now.ToLongTimeString

                sPPeriod = Me.DGV_Transactions.Rows(i).Cells(5).Value
                sFinYear = Me.DGV_Transactions.Rows(i).Cells(6).Value
                sGDC = Me.DGV_Transactions.Rows(i).Cells(7).Value
                sReference = Me.DGV_Transactions.Rows(i).Cells(8).Value
                sDescription = Me.DGV_Transactions.Rows(i).Cells(9).Value
                sAccountNumber = Me.DGV_Transactions.Rows(i).Cells(10).Value
                sLinkAcc = Me.DGV_Transactions.Rows(i).Cells(11).Value
                oAmount = Me.DGV_Transactions.Rows(i).Cells(12).Value
                oTaxType = Me.DGV_Transactions.Rows(i).Cells(13).Value
                oTaxAmount = Me.DGV_Transactions.Rows(i).Cells(14).Value
                sUserID = Me.DGV_Transactions.Rows(i).Cells(15).Value
                oSupplierID = Me.DGV_Transactions.Rows(i).Cells(16).Value
                oEmployeeID = Me.DGV_Transactions.Rows(i).Cells(17).Value
                sDescription2 = Me.DGV_Transactions.Rows(i).Cells(18).Value
                sDescription3 = Me.DGV_Transactions.Rows(i).Cells(19).Value
                sDescription4 = Me.DGV_Transactions.Rows(i).Cells(20).Value
                sDescription5 = Me.DGV_Transactions.Rows(i).Cells(21).Value
                blnPosted = Me.DGV_Transactions.Rows(i).Cells(22).Value
                blnAccounting = Me.DGV_Transactions.Rows(i).Cells(23).Value
                blnVoid = Me.DGV_Transactions.Rows(i).Cells(24).Value
                sTransactionType = Me.DGV_Transactions.Rows(i).Cells(25).Value
                oDR_CR = Me.DGV_Transactions.Rows(i).Cells(26).Value
                oContraAccount = Me.DGV_Transactions.Rows(i).Cells(27).Value
                oDescriptionCode = Me.DGV_Transactions.Rows(i).Cells(28).Value
                oDocType = Me.DGV_Transactions.Rows(i).Cells(29).Value
                oSupplierName = Me.DGV_Transactions.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO Transactions ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                'sSQL = sSQL & "#" & CYear & "-" & CMonth & "-" & CDay & "# As MyDate1, "
                'sSQL = sSQL & "#" & TYear & "-" & TMonth & "-" & TDay & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"
                End If

                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show("An error occured with details :" & _
                            ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If
            If Not IsNothing(cn) Then
                cn.Dispose()
            End If
        End Try
    End Sub

    Function oCheck_Integrity() As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try


            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For Each row As DataGridViewRow In DGV_Transactions.Rows
                If row.Cells("DR_CR").Value = "Dr" Then
                    oDebits = oDebits + CDec(row.Cells(12).Value)
                ElseIf row.Cells("DR_CR").Value = "Cr" Then
                    oCredits = oCredits + CDec(row.Cells(12).Value)
                End If
            Next

            If oDebits = oCredits Then
                If oDebits = 0 Or oCredits = 0 Then
                    oCheck_Integrity = False
                Else
                    oCheck_Integrity = True
                End If
            Else
                oCheck_Integrity = False
            End If

        Catch ex As Exception
            MsgBox("There was an error checking integrity " & ex.Message & " 061")
        End Try
    End Function

    Sub Load_Invoice_Data_From_ID_to_Variables(ByVal oTransactionID As Integer)

        Dim sSQL As String
        sSQL = "SELECT * FROM Transactions WHERE TransactionID = " & oTransactionID & " And (DocType = 'Invoice' or DocType ='Supplier Journal') and [Description Code] = '" & Get_CreditorControlAcc() & "'"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim xConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim cmd As New SqlCommand(sSQL, xConnection)
        cmd.CommandTimeout = 300
        xConnection.Open()
        Dim datareader As SqlDataReader = cmd.ExecuteReader

        Try

            While datareader.Read

                If Not datareader("TransactionID").Equals(DBNull.Value) Then
                    oTransactionID = datareader("TransactionID")
                Else
                    oTransactionID = 0
                End If

                If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                    dTransactionDate = datareader("Transaction Date")
                Else
                    dTransactionDate = Now.Date
                End If

                If Not datareader("Capture Date").Equals(DBNull.Value) Then
                    dCaptureDate = datareader("Capture Date")
                Else
                    dCaptureDate = Now.Date
                End If

                If Not datareader("PPeriod").Equals(DBNull.Value) Then
                    sPPeriod = datareader("PPeriod")
                Else
                    sPPeriod = 0
                End If

                If Not datareader("Fin Year").Equals(DBNull.Value) Then
                    sFinYear = datareader("Fin Year")
                Else
                    sFinYear = 0
                End If

                If Not datareader("GDC").Equals(DBNull.Value) Then
                    sGDC = datareader("GDC")
                Else
                    sGDC = ""
                End If

                If Not datareader("Reference").Equals(DBNull.Value) Then
                    sInvoiceNumber = datareader("Reference")
                Else
                    sInvoiceNumber = ""
                End If

                If Not datareader("Description").Equals(DBNull.Value) Then
                    sDescription = datareader("Description")
                Else
                    sDescription = ""
                End If

                If Not datareader("AccNumber").Equals(DBNull.Value) Then
                    sAccountNumber = datareader("AccNumber")
                Else
                    sAccountNumber = ""
                End If
                If Not datareader("Contra Account").Equals(DBNull.Value) Then
                    sContraAccount = datareader("Contra Account")
                Else
                    sContraAccount = ""
                End If
                If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                    sLinkAcc = datareader("LinkAcc")
                Else
                    sLinkAcc = ""
                End If
                If Not datareader("Amount").Equals(DBNull.Value) Then
                    oAmount = datareader("Amount")
                Else
                    oAmount = 0
                End If
                If Not datareader("TaxType").Equals(DBNull.Value) Then
                    oTaxType = datareader("TaxType")
                Else
                    oTaxType = 0
                End If
                If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                    oTaxAmount = datareader("Tax Amount")
                Else
                    oTaxAmount = 0
                End If
                If Not datareader("UserID").Equals(DBNull.Value) Then
                    sUserID = datareader("UserID")
                Else
                    sUserID = 0
                End If
                If Not datareader("SupplierID").Equals(DBNull.Value) Then
                    oSupplierID = datareader("SupplierID")
                Else
                    oSupplierID = 0
                End If
                If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                    oEmployeeID = datareader("EmployeeID")
                Else
                    oEmployeeID = 0
                End If
                If Not datareader("Description 2").Equals(DBNull.Value) Then
                    sDescription2 = datareader("Description 2")
                Else
                    sDescription2 = ""
                End If
                If Not datareader("Description 3").Equals(DBNull.Value) Then
                    sDescription3 = datareader("Description 3")
                Else
                    sDescription3 = ""
                End If
                If Not datareader("Description 4").Equals(DBNull.Value) Then
                    sDescription4 = datareader("Description 4")
                Else
                    sDescription4 = ""
                End If
                If Not datareader("Description 5").Equals(DBNull.Value) Then
                    sDescription5 = datareader("Description 5")
                Else
                    sDescription5 = ""
                End If
                If Not datareader("Posted").Equals(DBNull.Value) Then
                    blnPosted = datareader("Posted")
                Else
                    blnPosted = False
                End If
                If Not datareader("Accounting").Equals(DBNull.Value) Then
                    blnAccounting = datareader("Accounting")
                Else
                    blnAccounting = False
                End If
                If Not datareader("Void").Equals(DBNull.Value) Then
                    blnVoid = datareader("Void")
                Else
                    blnVoid = False
                End If
                If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                    TransactionType = datareader("Transaction Type")
                Else
                    TransactionType = 0
                End If
                If Not datareader("DR_CR").Equals(DBNull.Value) Then
                    sDRCR = datareader("DR_CR")
                Else
                    sDRCR = ""
                End If

                If Not datareader("SupplierName").Equals(DBNull.Value) Then
                    oSupplierName = datareader("SupplierName")
                Else
                    oSupplierName = ""
                End If

                If Not datareader("DocType").Equals(DBNull.Value) Then
                    oDocType = datareader("DocType")
                Else
                    oDocType = ""
                End If

                If Not datareader("Info").Equals(DBNull.Value) Then
                    sInfo = datareader("Info")
                Else
                    sInfo = ""
                End If
                If Not datareader("Info2").Equals(DBNull.Value) Then
                    sInfo2 = datareader("Info2")
                Else
                    sInfo2 = ""
                End If
                If Not datareader("Info3").Equals(DBNull.Value) Then
                    sInfo3 = datareader("Info3")
                Else
                    sInfo3 = ""
                End If
            End While
            xConnection.Close()
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub ComboBox_Supplier_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        cmbSupplier.DroppedDown = True
    End Sub

    Private Sub Paid_Text_TextChanged(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub DateTimePicker_From_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If blnFormLoading = True Then Exit Sub
        GetInvoiceDetailPerSupplier(cmbSupplier.Text)
    End Sub


    Private Sub DateTimePicker_To_ValueChanged(sender As System.Object, e As System.EventArgs)
        If blnFormLoading = True Then Exit Sub
        GetInvoiceDetailPerSupplier(cmbSupplier.Text)
    End Sub



    Function GetPeriod(ByVal sTransactionDate As String) As String
        Try

            Dim sSQL As String

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= Format('" & sTransactionDate & "',""Short Date"")"
                sSQL = sSQL & " AND [End Date] >= Format('" & sTransactionDate & "',""Short Date"")"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID.ToString
                End If
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetPeriod = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                ''DISABLED ###########################
                ''If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                ''Dim oResult As Integer
                ''Dim oCriteria As String
                ''If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                ''oCriteria = "[Start Date]<='" & sTransactionDate & "' And " & "[End Date]>='" & sTransactionDate & "'" & " And Co_ID = " & My.Settings.Setting_CompanyID.ToString
                ''Else
                ''oCriteria = "[Start Date]<='" & sTransactionDate & "' And " & "[End Date]>='" & sTransactionDate & "'"
                ''End If
                ''xSQLTable_Periods.DefaultView.RowFilter = oCriteria
                'xSQLTable_Suppliers.DefaultView.RowFilter = "[Start Date]<='" & sTransactionDate & " And " & "[End Date]>='" & sTransactionDate
                ''If xSQLTable_Periods.DefaultView.Count = 1 Then
                ''oResult = xSQLTable_Periods.DefaultView.Item(0).Item("Period")
                ''End If
                ''GetPeriod = CStr(oResult)

                ''Else

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= CAST('" & sTransactionDate & "' as date)"
                sSQL = sSQL & " AND [End Date] >= CAST('" & sTransactionDate & "' as date)"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetPeriod = cmd.ExecuteScalar().ToString
                connection.Close()
                ''End If
            End If
        Catch ex As Exception
            MsgBox("There was an error getting the accounting period!" & ex.Message & " 064")
            GetPeriod = "NA"
        End Try
    End Function


    Function Get_Segment4_from_GLACCOUNT_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String


            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [GL ACCOUNT] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID.ToString
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment4_from_GLACCOUNT_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        xSQLTable_Accounts.DefaultView.RowFilter = "[GL ACCOUNT]='" & SQLConvert(sString) & "' AND Co_ID = " & My.Settings.Setting_CompanyID '##### NEW #####
                    Else
                        xSQLTable_Accounts.DefaultView.RowFilter = "[GL ACCOUNT]='" & SQLConvert(sString) & "'"
                    End If
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4")
                    'End If
                    Get_Segment4_from_GLACCOUNT_in_Accounting = oResult

                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segment4_from_GLACCOUNT_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account in Accounting!")
        End Try
    End Function


    Function Get_Segm1Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm1Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'" & " And Co_ID = " & My.Settings.Setting_CompanyID '##### NEW #####
                    Else
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    End If
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 1 Desc")
                    'End If
                    Get_Segm1Desc_From_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segm1Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()

                End If

            End If


        Catch ex As Exception
            MsgBox("There was an error looking up Segment 1 Desc in Accounting! " & ex.Message & " 061b")
        End Try
    End Function

    Function Get_Segm2Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm2Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'" & " And Co_ID = " & My.Settings.Setting_CompanyID  '##### NEW #####
                    Else
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    End If
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 2 Desc")
                    'End If
                    Get_Segm2Desc_From_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segm2Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up Segment 2 Desc in Accounting!")
        End Try
    End Function

    Function Get_Segm3Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm3Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'" & " And Co_ID = " & My.Settings.Setting_CompanyID '##### NEW #####
                    Else
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    End If
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 3 Desc")
                    'End If
                    Get_Segm3Desc_From_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segm3Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up Segment 3 Desc in Accounting!")
        End Try
    End Function

    Function Get_GLACCOUNT_from_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_GLACCOUNT_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'" & " AND Co_ID = " & My.Settings.Setting_CompanyID.ToString '##### NEW #####
                    Else
                        xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    End If
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("GL ACCOUNT")
                    'End If
                    Get_GLACCOUNT_from_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_GLACCOUNT_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the GL Account in Accounting!")
        End Try
    End Function

    Function Database_Lookup_Int_return_String(ByVal oTable As String, oLookupField As String, ByVal WhereField As String, ByVal WhereValue As String, ByVal DataType As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT " & oLookupField & " FROM " & oTable & " WHERE " & WhereField & " = " & SQLConvert(WhereValue)
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Database_Lookup_Int_return_String = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        xSQLTable_Suppliers.DefaultView.RowFilter = WhereField & "='" & SQLConvert(WhereValue) & "'" & " And Co_ID = " & My.Settings.Setting_CompanyID '##### NEW #####
                    Else
                        xSQLTable_Suppliers.DefaultView.RowFilter = WhereField & "='" & SQLConvert(WhereValue) & "'"
                    End If
                    'If xSQLTable_Suppliers.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item(oLookupField).ToString
                    'End If
                    Database_Lookup_Int_return_String = oResult

                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Database_Lookup_Int_return_String = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            'MsgBox("There was an error looking up a database value " & oLookupField & " in table " & oTable & Err.Description)
            Database_Lookup_Int_return_String = "No Value Found"
        End Try
    End Function
    Private Sub GroupBox1_Enter(sender As System.Object, e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub TrackBar1_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar1.Scroll
        Try

            If Me.TrackBar1.Value = 0 Then
                Me.dtpFrom.Value = System.DateTime.Now
            ElseIf Me.TrackBar1.Value = 1 Then
                Me.dtpFrom.Value = System.DateTime.Now.AddDays(-14)
            ElseIf Me.TrackBar1.Value = 2 Then
                Me.dtpFrom.Value = System.DateTime.Now.AddDays(-21)
            ElseIf Me.TrackBar1.Value = 3 Then
                Me.dtpFrom.Value = System.DateTime.Now.AddDays(-30)
            ElseIf Me.TrackBar1.Value = 4 Then
                Me.dtpFrom.Value = System.DateTime.Now.AddMonths(-1)
            ElseIf Me.TrackBar1.Value = 5 Then
                Me.dtpFrom.Value = System.DateTime.Now.AddDays(-45)
            ElseIf Me.TrackBar1.Value = 6 Then
                Me.dtpFrom.Value = System.DateTime.Now.AddMonths(-2)
            End If
            Call GetInvoiceDetailPerSupplier(cmbSupplier.Text)
        Catch ex As Exception
            MsgBox("Problem loading date range " & ex.Message)
        End Try
    End Sub

    Function Get_Segment4_from_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment4_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                    'If
                    Get_Segment4_from_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segment4_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the Magic Box Code fro the Display name in Accounting!")
        End Try
    End Function

    Function Get_Payment_Types_for_user(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT [Approve_Payments_Types] FROM Users WHERE [UserName] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Payment_Types_for_user = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Payment_Types_for_user = cmd.ExecuteScalar().ToString
                connection.Close()

            End If


        Catch ex As Exception
            MsgBox("There was an error looking up a users Payment type authorizations! The current user most probably doesn't exist for the present Company ID" & ex.Message & " 323")
        End Try
    End Function

    '    Sub Loop_Invoice_Payment_Requests_Color_Lime_Green()

    '        Dim x As Date
    '        Dim y As Date

    '        x = DateTime.Now
    '        'Exit Sub
    '        '------------------- Timer ----------------------------------------

    '        Dim TransID As Integer
    '        Dim Supplier As String
    '        Dim Reference As String
    '        Dim oInfo As String
    '        Dim xAmount As Decimal
    '        Dim xDue As Decimal
    '        Dim arrSplit As Object
    '        Dim oTotalAmount As Decimal = 0 'Used because script to total box wasn't working on load

    '        'Me.DataGridView2.Rows.Clear()

    '        Dim oTransactionID_In_Requests As String
    '        Dim oSupplier_In_Requests As String
    '        Dim oReference_In_Requests As String
    '        Dim oInfo_In_Requests As String

    '        Try
    '            For k As Integer = 0 To Me.DGV_Payment_Requests.RowCount - 2

    '                oTransactionID_In_Requests = Me.DGV_Payment_Requests.Rows(k).Cells(0).Value
    '                oSupplier_In_Requests = Me.DGV_Payment_Requests.Rows(k).Cells(1).Value
    '                oReference_In_Requests = Me.DGV_Payment_Requests.Rows(k).Cells(2).Value
    '                oInfo_In_Requests = Me.DGV_Payment_Requests.Rows(k).Cells(3).Value

    '                'Now loop through right invoices grid
    '                For Each row As DataGridViewRow In dgvInvoiceList.Rows

    '                    TransID = row.Cells(2).Value
    '                    Supplier = row.Cells(3).Value
    '                    Reference = row.Cells(4).Value

    '                    'oInfo = Get_Info_Field(TransID)

    '                    'If Mid(oInfo, 1, 25) = "Invoice Payment Requested" Then
    '                    If oTransactionID_In_Requests = TransID And oSupplier_In_Requests = Supplier And oReference_In_Requests = Reference Then

    '                        'First color the row
    '                        row.DefaultCellStyle.ForeColor = Color.LimeGreen

    '                        'Get the amount and payment type by splitting the string in the Info field
    '                        arrSplit = Split(oInfo_In_Requests, "|")
    '                        If IsNumeric(arrSplit(1)) = False Then
    '                            xAmount = 0
    '                        Else
    '                            xAmount = arrSplit(1)
    '                        End If

    '                        'If IsNumeric(row.Cells(4).Value) = False Then
    '                        'xDue = 0
    '                        'Else
    '                        '    xDue = row.Cells(4).Value
    '                        'End If

    '                        'If xDue = 0 Then
    '                        '    'MsgBox("Row " & CStr(i + 1) & " has been paid!")
    '                        '    GoTo Jump
    '                        'End If

    '                        Date.Now.ToString("dd MMMM yyyy")

    '                        oTotalAmount = oTotalAmount + CDec(arrSplit(1))

    '                        '-------------------------------------------------------------------------------------------------------------------------------------------------------
    '                        'Only add the line (like Drag n Drop) if user autorized to make payment
    '                        If User_Authorized_to_Make_Payment_Type(arrSplit(2)) = True Then
    '                            'fix
    '                            If row.DefaultCellStyle.ForeColor <> Color.OrangeRed Then
    '                                'Prevent multiple loading with following line

    '                                If Check_if_ID_in_DGV2(TransID) = False Then

    '                                    'Set colour to OrangeRed as if dragged - for some reason this doesn't work
    '                                    row.DefaultCellStyle.ForeColor = Color.OrangeRed

    '                                    Me.dgvPaymentsList.Rows.Add(TransID, Supplier, Reference _
    '                                                            , arrSplit(1), CDate(Me.dtpPaymentDate.Value).Date, arrSplit(2))  'Date.Now.ToString("MM/dd/yyyy")
    '                                    Me.dgvPaymentsList.Rows(Me.dgvPaymentsList.RowCount - 2).DefaultCellStyle.ForeColor = Color.LimeGreen

    '                                    Dim DueColumn As DataGridViewColumn = dgvPaymentsList.Columns("Amount")
    '                                    DueColumn.DefaultCellStyle.Format = "c"
    '                                End If

    '                            End If

    '                        End If
    '                        '--------------------------------------------------------------------------------------------------------------------------------------------------------

    '                    End If
    'Jump:

    '                Next

    '            Next

    '            TextBox_Total.Text = oTotalAmount.ToString("c")

    '            '------------------- Timer ----------------------------------------
    '            y = DateTime.Now

    '            Dim SecondsDifference As Integer

    '            SecondsDifference = DateDiff(DateInterval.Second, x, y)

    '            Globals.Ribbons.Ribbon1.lblTimer.Label = SecondsDifference.ToString

    '            'MsgBox("Difference in seconds is : " & SecondsDifference.ToString())

    '            ''lblTimeTest.Text = SecondsDifference.ToString()

    '        Catch ex As Exception
    '            MsgBox(ex.Message & " 326b")
    '        End Try

    '    End Sub

    Function Check_if_ID_in_DGV2(ByVal oID As String) As Boolean
        Try
            Check_if_ID_in_DGV2 = False
            For i As Integer = 0 To Me.dgvPaymentsList.RowCount - 1
                If Me.dgvPaymentsList.Rows(i).Cells(0).Value = oID Then
                    Check_if_ID_in_DGV2 = True
                End If
            Next
        Catch ex As Exception
            'no error because not sure what last row has in it...
        End Try
    End Function


    Function Get_Info_Field(ByVal sString As String) As String
        Try
            Dim sSQL As String
            sSQL = "SELECT Info FROM Transactions WHERE TransactionID = " & sString & ""
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Info_Field = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Info_Field = cmd.ExecuteScalar().ToString
                connection.Close()

            End If

        Catch ex As Exception
            Get_Info_Field = ""
            'MsgBox("There was an error looking up information on a payment request! " & ex.Message & " 324")
        End Try
    End Function
    'Sub Fill_DGV_Payment_Requests_Suppliers_Info()
    '    Try

    '        Dim xAccessAdapter_Info As OleDbDataAdapter
    '        Dim xAccessTable_Info As New DataTable
    '        Dim xSQLAdapter_Info As SqlDataAdapter
    '        Dim xSQLTable_Info As New DataTable

    '        Dim sSQL As String

    '        Dim paymentTypeList As String = "OR ("
    '        For Each item In LoggedUser_Roles
    '            paymentTypeList &= "Info like '%|" & item & "%' or "
    '        Next
    '        paymentTypeList = paymentTypeList.Substring(0, paymentTypeList.Length - 4)
    '        paymentTypeList &= ")"

    '        If My.Settings.DBType = "Access" Then
    '            sSQL = "SELECT TransactionID, SupplierName, Reference, Info FROM Transactions Where Info like '%Invoice Payment Requested%' And Void = 0 " & paymentTypeList
    '            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
    '                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '            End If

    '            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
    '            xAccessAdapter_Info = New OleDbDataAdapter(sSQL, connection)
    '            xAccessAdapter_Info.Fill(xAccessTable_Info)
    '            Me.DGV_Payment_Requests.DataSource = xAccessTable_Info
    '        ElseIf My.Settings.DBType = "SQL" Then
    '            sSQL = "SELECT * into #Test FROM Transactions Where Info like '%Invoice Payment Requested%' " & paymentTypeList
    '            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
    '                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
    '            End If
    '            sSQL = sSQL & " Select TransactionID, SupplierName ,reference, Amount as Due, [Transaction Date] from #Test where Void=0 and Accounting = 1 order by [Transaction Date] Desc"
    '            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
    '            xSQLAdapter_Info = New SqlDataAdapter(sSQL, connection)
    '            xSQLAdapter_Info.Fill(xSQLTable_Info)
    '            Me.DGV_Payment_Requests.DataSource = xSQLTable_Info
    '        End If
    '        'Me.DGV_Payment_Requests.Columns(3).Width = 300
    '    Catch ex As Exception
    '        MsgBox(ex.Message & " 324b")
    '    End Try
    'End Sub
    'Sub oLoop_Thru_Checking_for_zero_Due_Payment_Requests()
    '    Try

    '        Dim oDue As Decimal
    '        Dim blnRecordUpdated As Boolean = False
    '        Dim oTransactionID As Integer
    '        For i As Integer = 0 To Me.DGV_Payment_Requests.RowCount - 2
    '            oTransactionID = Me.DGV_Payment_Requests.Rows(i).Cells(0).Value
    '            If Check_if_Due_is_zero(oTransactionID) = True Then
    '                'MsgBox(oTransactionID) 'TITS
    '                UnRequestPayment(oTransactionID)
    '                blnRecordUpdated = True
    '            End If

    '        Next
    '        If blnRecordUpdated = True Then
    '            Fill_DGV_Payment_Requests_Suppliers_Info()
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub
    Function Check_if_Due_is_zero(ByVal oTransactionID As String) As Boolean
        Try
            Dim oGetVal As Object
            Dim sSQL As String
            sSQL = "SELECT Due From Step4b Where Trans = " & oTransactionID
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oGetVal = cmd.ExecuteScalar().ToString
                If CDec(oGetVal) = 0 Then
                    Check_if_Due_is_zero = True
                Else
                    Check_if_Due_is_zero = False
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oGetVal = cmd.ExecuteScalar().ToString
                If CDec(oGetVal) = 0 Then
                    Check_if_Due_is_zero = True
                Else
                    Check_if_Due_is_zero = False
                End If
                connection.Close()

            End If

        Catch ex As Exception
            Check_if_Due_is_zero = False
        End Try
    End Function
    'Sub oColor_Suppliers_If_Any_Requests()

    '    Dim oSupplier_In_Requests As String
    '    Dim strSupplier As String
    '    'Dim strCount As String
    '    'Dim intCount As String
    '    Try

    '        Me.DataGridView1.DefaultCellStyle.ForeColor = Color.Black

    '        'First loop through the DGV holding the invoice requests
    '        For k As Integer = 0 To Me.DGV_Payment_Requests.RowCount - 2
    '            oSupplier_In_Requests = Me.DGV_Payment_Requests.Rows(k).Cells(1).Value

    '            'Now loop through Left Supplier grid and colour lime green if matches
    '            For i As Integer = 0 To Me.DataGridView_Supplier.Rows.Count - 2
    '                strSupplier = Me.DataGridView_Supplier.Rows(i).Cells(1).Value
    '                If oSupplier_In_Requests = strSupplier Then
    '                    Me.DataGridView_Supplier.Rows(i).DefaultCellStyle.ForeColor = Color.LimeGreen
    '                End If
    '            Next

    '        Next
    '    Catch ex As Exception
    '        MsgBox(" 325")
    '    End Try
    'End Sub
    Function Count_Instance_Of_Requests(ByVal oSupplier As String) As String

        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Transactions Where Mid(Info,1,25) = 'Invoice Payment Requested' And SupplierName = '" & SQLConvert(oSupplier) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()

                Count_Instance_Of_Requests = cmd.ExecuteScalar.ToString

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Transactions Where SubString(Info,1,25) = 'Invoice Payment Requested' And SupplierName = '" & SQLConvert(oSupplier) & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()

                Count_Instance_Of_Requests = cmd.ExecuteScalar.ToString

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            Count_Instance_Of_Requests = "Error"
            MsgBox(Err.Description & " 324a")
        End Try
    End Function
    Private Sub btnViewDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDetail.Click
        Try
            Dim InvD As New frmInvoiceDetail
            If dgvInvoiceList.SelectedRows.Count = 0 Then
                InvD.isGridSelected = False
                MessageBox.Show("Please select the invoice you want to view in order to proceed", "Invoice Details", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                InvD.isGridSelected = True
                Dim SupplierID As Integer = CInt(MBData.GetSupplierID(cmbSupplier.Text.Trim))
                InvD.SupplierID = SupplierID
                InvD.Reference = CStr(dgvInvoiceList.SelectedRows(0).Cells("Reference").Value)
                InvD.Show()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label13.Click

    End Sub

    Private Sub TrackBar2_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar2.Scroll
        Try

            If Me.TrackBar2.Value = 0 Then
                Me.dtpTo.Value = System.DateTime.Now
            ElseIf Me.TrackBar2.Value = 1 Then
                Me.dtpTo.Value = System.DateTime.Now.AddDays(-14)
            ElseIf Me.TrackBar2.Value = 2 Then
                Me.dtpTo.Value = System.DateTime.Now.AddDays(-21)
            ElseIf Me.TrackBar2.Value = 3 Then
                Me.dtpTo.Value = System.DateTime.Now.AddDays(-30)
            ElseIf Me.TrackBar2.Value = 4 Then
                Me.dtpTo.Value = System.DateTime.Now.AddMonths(-1)
            ElseIf Me.TrackBar2.Value = 5 Then
                Me.dtpTo.Value = System.DateTime.Now.AddDays(-45)
            ElseIf Me.TrackBar2.Value = 6 Then
                Me.dtpTo.Value = System.DateTime.Now.AddMonths(-2)
            End If
            Call GetInvoiceDetailPerSupplier(cmbSupplier.Text)
        Catch ex As Exception
            MsgBox("Problem loading date range " & ex.Message)
        End Try
    End Sub

    Function User_Authorized_to_Make_Payment_Type(ByVal oPaymentType As String) As Boolean
        Try

            Dim oPaymentAuthorizationTypes As String
            Dim arrSplit As Object
            Dim blnAutorized As Boolean = False

            oPaymentAuthorizationTypes = Get_Payment_Types_for_user(Globals.Ribbons.Ribbon1.lblUsername.Label)
            arrSplit = Split(oPaymentAuthorizationTypes, ",")

            ''Check if user is authorized to pay this
            User_Authorized_to_Make_Payment_Type = False

            For k As Integer = 0 To UBound(arrSplit)
                If oPaymentType = arrSplit(k) Then

                    User_Authorized_to_Make_Payment_Type = True

                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


    Sub oTimeDifference()
        Dim x As Date
        Dim y As Date

        x = DateTime.Now

        System.Threading.Thread.Sleep(5000)

        y = DateTime.Now


        Dim SecondsDifference As Integer

        SecondsDifference = DateDiff(DateInterval.Second, x, y)

        MsgBox("Difference in seconds is : " & SecondsDifference.ToString())

    End Sub

    Sub oGet_Total_Payment_Requests()

        Dim sSQL As String

        Dim sAmount As String
        Dim oAmount As Decimal
        Dim oTotal As Decimal = 0

        'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
        Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
        Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")

        Dim sDateCriteria As String

        If My.Settings.DBType = "Access" Then
            sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
        ElseIf My.Settings.DBType = "SQL" Then
            sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"

        End If

        Dim arrSplit As Object


        If My.Settings.DBType = "Access" Then

            sSQL = "SELECT * FROM Transactions WHERE Mid(Info,1,25) = 'Invoice Payment Requested'"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            sSQL = sSQL & " And Void = False"

            sSQL = sSQL & sDateCriteria

            Dim xConnection As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, xConnection)
            xConnection.Open()
            Dim datareader As OleDbDataReader = cmd.ExecuteReader

            Try

                While datareader.Read

                    If Not datareader("TransactionID").Equals(DBNull.Value) Then
                        oTransactionID = datareader("TransactionID")
                    Else
                        oTransactionID = 0
                    End If
                    If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                        dTransactionDate = datareader("Transaction Date")
                    Else
                        dTransactionDate = Now.Date
                    End If
                    If Not datareader("Capture Date").Equals(DBNull.Value) Then
                        dCaptureDate = datareader("Capture Date")
                    Else
                        dCaptureDate = Now.Date
                    End If
                    If Not datareader("PPeriod").Equals(DBNull.Value) Then
                        sPPeriod = datareader("PPeriod")
                    Else
                        sPPeriod = 0
                    End If
                    If Not datareader("Fin Year").Equals(DBNull.Value) Then
                        sFinYear = datareader("Fin Year")
                    Else
                        sFinYear = 0
                    End If
                    If Not datareader("GDC").Equals(DBNull.Value) Then
                        sGDC = datareader("GDC")
                    Else
                        sGDC = ""
                    End If
                    If Not datareader("Reference").Equals(DBNull.Value) Then
                        sInvoiceNumber = datareader("Reference")
                    Else
                        sInvoiceNumber = ""
                    End If
                    If Not datareader("Description").Equals(DBNull.Value) Then
                        sDescription = datareader("Description")
                    Else
                        sDescription = ""
                    End If

                    If Not datareader("AccNumber").Equals(DBNull.Value) Then
                        sAccountNumber = datareader("AccNumber")
                    Else
                        sAccountNumber = ""
                    End If
                    If Not datareader("Contra Account").Equals(DBNull.Value) Then
                        sContraAccount = datareader("Contra Account")
                    Else
                        sContraAccount = ""
                    End If
                    If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                        sLinkAcc = datareader("LinkAcc")
                    Else
                        sLinkAcc = ""
                    End If
                    If Not datareader("Amount").Equals(DBNull.Value) Then
                        oAmount = datareader("Amount")
                    Else
                        oAmount = 0
                    End If
                    If Not datareader("TaxType").Equals(DBNull.Value) Then
                        oTaxType = datareader("TaxType")
                    Else
                        oTaxType = 0
                    End If
                    If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                        oTaxAmount = datareader("Tax Amount")
                    Else
                        oTaxAmount = 0
                    End If
                    If Not datareader("UserID").Equals(DBNull.Value) Then
                        sUserID = datareader("UserID")
                    Else
                        sUserID = 0
                    End If
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        oSupplierID = datareader("SupplierID")
                    Else
                        oSupplierID = 0
                    End If
                    If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                        oEmployeeID = datareader("EmployeeID")
                    Else
                        oEmployeeID = 0
                    End If
                    If Not datareader("Description 2").Equals(DBNull.Value) Then
                        sDescription2 = datareader("Description 2")
                    Else
                        sDescription2 = ""
                    End If
                    If Not datareader("Description 3").Equals(DBNull.Value) Then
                        sDescription3 = datareader("Description 3")
                    Else
                        sDescription3 = ""
                    End If
                    If Not datareader("Description 4").Equals(DBNull.Value) Then
                        sDescription4 = datareader("Description 4")
                    Else
                        sDescription4 = ""
                    End If
                    If Not datareader("Description 5").Equals(DBNull.Value) Then
                        sDescription5 = datareader("Description 5")
                    Else
                        sDescription5 = ""
                    End If
                    If Not datareader("Posted").Equals(DBNull.Value) Then
                        blnPosted = datareader("Posted")
                    Else
                        blnPosted = False
                    End If
                    If Not datareader("Accounting").Equals(DBNull.Value) Then
                        blnAccounting = datareader("Accounting")
                    Else
                        blnAccounting = False
                    End If
                    If Not datareader("Void").Equals(DBNull.Value) Then
                        blnVoid = datareader("Void")
                    Else
                        blnVoid = False
                    End If
                    If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                        TransactionType = datareader("Transaction Type")
                    Else
                        TransactionType = 0
                    End If
                    If Not datareader("DR_CR").Equals(DBNull.Value) Then
                        sDRCR = datareader("DR_CR")
                    Else
                        sDRCR = ""
                    End If
                    If Not datareader("Info").Equals(DBNull.Value) Then
                        sInfo = datareader("Info")
                    Else
                        sInfo = ""
                    End If
                    If Not datareader("Info2").Equals(DBNull.Value) Then
                        sInfo2 = datareader("Info2")
                    Else
                        sInfo2 = ""
                    End If
                    If Not datareader("Info3").Equals(DBNull.Value) Then
                        sInfo3 = datareader("Info3")
                    Else
                        sInfo3 = ""
                    End If


                    arrSplit = Split(sInfo, "|")
                    sAmount = arrSplit(1)
                    oAmount = CDec(sAmount)
                    oTotal = oTotal + oAmount


                End While
                xConnection.Close()

                Me.txtRequested.Text = CStr(oTotal)

            Catch ex As Exception
                MsgBox(ex.Message & " 062")
            End Try

        ElseIf My.Settings.DBType = "SQL" Then

            Try

                sSQL = "SELECT * FROM Transactions WHERE Info like '%Invoice Payment Requested%'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                sSQL = sSQL & " And Void = 0"

                Dim xConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, xConnection)
                cmd.CommandTimeout = 300
                xConnection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader


                While datareader.Read

                    If Not datareader("TransactionID").Equals(DBNull.Value) Then
                        oTransactionID = datareader("TransactionID")
                    Else
                        oTransactionID = 0
                    End If
                    If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                        dTransactionDate = datareader("Transaction Date")
                    Else
                        dTransactionDate = Now.Date
                    End If
                    If Not datareader("Capture Date").Equals(DBNull.Value) Then
                        dCaptureDate = datareader("Capture Date")
                    Else
                        dCaptureDate = Now.Date
                    End If
                    If Not datareader("PPeriod").Equals(DBNull.Value) Then
                        sPPeriod = datareader("PPeriod")
                    Else
                        sPPeriod = 0
                    End If
                    If Not datareader("Fin Year").Equals(DBNull.Value) Then
                        sFinYear = datareader("Fin Year")
                    Else
                        sFinYear = 0
                    End If
                    If Not datareader("GDC").Equals(DBNull.Value) Then
                        sGDC = datareader("GDC")
                    Else
                        sGDC = ""
                    End If
                    If Not datareader("Reference").Equals(DBNull.Value) Then
                        sInvoiceNumber = datareader("Reference")
                    Else
                        sInvoiceNumber = ""
                    End If
                    If Not datareader("Description").Equals(DBNull.Value) Then
                        sDescription = datareader("Description")
                    Else
                        sDescription = ""
                    End If

                    If Not datareader("AccNumber").Equals(DBNull.Value) Then
                        sAccountNumber = datareader("AccNumber")
                    Else
                        sAccountNumber = ""
                    End If
                    If Not datareader("Contra Account").Equals(DBNull.Value) Then
                        sContraAccount = datareader("Contra Account")
                    Else
                        sContraAccount = ""
                    End If
                    If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                        sLinkAcc = datareader("LinkAcc")
                    Else
                        sLinkAcc = ""
                    End If
                    If Not datareader("Amount").Equals(DBNull.Value) Then
                        oAmount = datareader("Amount")
                    Else
                        oAmount = 0
                    End If
                    If Not datareader("TaxType").Equals(DBNull.Value) Then
                        oTaxType = datareader("TaxType")
                    Else
                        oTaxType = 0
                    End If
                    If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                        oTaxAmount = datareader("Tax Amount")
                    Else
                        oTaxAmount = 0
                    End If
                    If Not datareader("UserID").Equals(DBNull.Value) Then
                        sUserID = datareader("UserID")
                    Else
                        sUserID = 0
                    End If
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        oSupplierID = datareader("SupplierID")
                    Else
                        oSupplierID = 0
                    End If
                    If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                        oEmployeeID = datareader("EmployeeID")
                    Else
                        oEmployeeID = 0
                    End If
                    If Not datareader("Description 2").Equals(DBNull.Value) Then
                        sDescription2 = datareader("Description 2")
                    Else
                        sDescription2 = ""
                    End If
                    If Not datareader("Description 3").Equals(DBNull.Value) Then
                        sDescription3 = datareader("Description 3")
                    Else
                        sDescription3 = ""
                    End If
                    If Not datareader("Description 4").Equals(DBNull.Value) Then
                        sDescription4 = datareader("Description 4")
                    Else
                        sDescription4 = ""
                    End If
                    If Not datareader("Description 5").Equals(DBNull.Value) Then
                        sDescription5 = datareader("Description 5")
                    Else
                        sDescription5 = ""
                    End If
                    If Not datareader("Posted").Equals(DBNull.Value) Then
                        blnPosted = datareader("Posted")
                    Else
                        blnPosted = False
                    End If
                    If Not datareader("Accounting").Equals(DBNull.Value) Then
                        blnAccounting = datareader("Accounting")
                    Else
                        blnAccounting = False
                    End If
                    If Not datareader("Void").Equals(DBNull.Value) Then
                        blnVoid = datareader("Void")
                    Else
                        blnVoid = False
                    End If
                    If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                        TransactionType = datareader("Transaction Type")
                    Else
                        TransactionType = 0
                    End If
                    If Not datareader("DR_CR").Equals(DBNull.Value) Then
                        sDRCR = datareader("DR_CR")
                    Else
                        sDRCR = ""
                    End If
                    If Not datareader("Info").Equals(DBNull.Value) Then
                        sInfo = datareader("Info")
                    Else
                        sInfo = ""
                    End If
                    If Not datareader("Info2").Equals(DBNull.Value) Then
                        sInfo2 = datareader("Info2")
                    Else
                        sInfo2 = ""
                    End If
                    If Not datareader("Info3").Equals(DBNull.Value) Then
                        sInfo3 = datareader("Info3")
                    Else
                        sInfo3 = ""
                    End If

                    arrSplit = Split(sInfo, "|")
                    sAmount = arrSplit(1)
                    oAmount = CDec(sAmount)
                    oTotal = oTotal + oAmount

                End While

                xConnection.Close()

                Me.txtRequested.Text = oTotal.ToString("c")

            Catch ex As Exception
                MsgBox(ex.Message & " 063c")
            End Try
        End If
    End Sub
    Dim TotalRequested As Decimal = 0
    Private Sub chkTotalRequested_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTotalRequested.CheckedChanged
        If Me.chkTotalRequested.Checked = True Then
            Me.txtRequested.Visible = True
            txtRequested.Text = TotalRequested.ToString("c")
        Else
            Me.txtRequested.Visible = False
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox(Me.dgvInvoiceList.Rows(Me.dgvInvoiceList.Rows.Count - 2).Cells(2).Value)
    End Sub
    Private Sub btnSuppDetail_Click(sender As System.Object, e As System.EventArgs) Handles btnSuppDetail.Click
        Try
            Dim InvDetPerSupplier As New frmInvoiceDetail_Per_Supplier
            InvDetPerSupplier.SupplierName = CStr(cmbSupplier.Text)
            InvDetPerSupplier.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    
    Private Sub DataGridView1_MouseDown_1(sender As Object, e As MouseEventArgs)
        Try
            '-----------Multiple Item Drag---------------------------------------------
            Dim info As DataGridView.HitTestInfo = Me.dgvInvoiceList.HitTest(e.X, e.Y)
            Dim rows As DataGridViewSelectedRowCollection = Me.dgvInvoiceList.SelectedRows ''PROBLEM
            Me.dgvInvoiceList.DoDragDrop(rows, DragDropEffects.Copy)
            '------------------------------------------------------------------------
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub DataGridView1_SelectionChanged_1(sender As Object, e As EventArgs)
        If blnFormLoading = True Then Exit Sub
        If blnForm_Hiding = True Then Exit Sub
        Dim oRowIndex As Integer
        For Each cell As DataGridViewCell In dgvInvoiceList.SelectedCells
            oRowIndex = cell.RowIndex
            dgvInvoiceList.Rows(oRowIndex).Selected = True
        Next
    End Sub
    Private Sub DataGridView1_DoubleClick_1(sender As Object, e As EventArgs)
        AddRow()
    End Sub
    Private Sub DataGridView1_DataBindingComplete_1(sender As Object, e As DataGridViewBindingCompleteEventArgs)
        If blnFormLoading = True Then Exit Sub
        If blnForm_Hiding = True Then Exit Sub
        Call TotalPaidBox()
    End Sub
    Private Sub rdbSupplier_CheckedChanged(sender As Object, e As EventArgs) Handles rdbSupplier.CheckedChanged
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            dgvSupplierSum.Sort(dgvSupplierSum.Columns("SupplierName"), System.ComponentModel.ListSortDirection.Ascending)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured : " & ex.Message, "Supplier payments error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub rdbBalance_CheckedChanged(sender As Object, e As EventArgs) Handles rdbBalance.CheckedChanged
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            dgvSupplierSum.Sort(dgvSupplierSum.Columns("Due"), System.ComponentModel.ListSortDirection.Ascending)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured : " & ex.Message, "Supplier payments error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnFilter.Click
        Try

            BolBntFilter = True
            If cmbSupplier.SelectedIndex = -1 Then
                MessageBox.Show("Please select a supplier in order to filter invoices", "Supplier Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbSupplier.Focus()
                Exit Sub
            End If
            If cmbStatus.SelectedIndex = -1 Then
                MessageBox.Show("Please select invoice status", "Supplier Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbStatus.Focus()
                Exit Sub
            End If

            If CDate(dtpFrom.Value) > CDate(dtpTo.Value) Then
                MessageBox.Show("Please select the filter dates in a correct date range.", "Date Range", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                dtpFrom.Focus()
                Exit Sub
            End If

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim SupplierID As Integer = CInt(cmbSupplier.SelectedValue)
            GetInvoiceDetailPerSupplier(SupplierID)

            For Each OriginalRow As DataGridViewRow In dgvInvoiceList.Rows
                Dim RequestResults = MBData.CheckRequestByTransID(OriginalRow.Cells(1).Value)
                If Not IsNothing(RequestResults) Then
                    OriginalRow.DefaultCellStyle.ForeColor = Color.LimeGreen
                End If
            Next

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("Ar error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ckbAllBalances_CheckedChanged(sender As Object, e As EventArgs) Handles ckbAllBalances.CheckedChanged
        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            oLoadData()
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            MessageBox.Show("An Error Occured :" & ex.Message, "Supplier payments Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub
    Function GetOriginalInvAmount(ByVal REF As String) As Decimal

        Dim InvoiceAmount As Decimal = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "SELECT Amount FROM Transactions WHERE TransactionID = " & REF & " AND (Doctype = 'Invoice' or Doctype = 'Supplier Journal') AND [Description Code]='" & Get_CreditorControlAcc() & "' AND Accounting = '1' and Void = '0' "
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            InvoiceAmount = CDec(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return InvoiceAmount
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " _
                            & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try
    End Function
    Function GetCoUnapprovedCNote() As Decimal

        Dim UnapprovedAmount As Decimal = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "select sum(Amount) from Transactions where Accounting = 0 AND Info2='Credit Note' and [Description Code] = 'HAAAAA' and Void = 0"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300

            cn.Open()
            If IsDBNull(cmd.ExecuteScalar()) Or IsNothing(cmd.ExecuteScalar()) Then
                UnapprovedAmount = 0
            Else
                UnapprovedAmount = CDec(cmd.ExecuteScalar())
            End If
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return UnapprovedAmount
        Catch ex As Exception
        End Try
    End Function
    Function GetCreditorControlBalance() As Decimal

        Dim CreditorControlBalance As Decimal = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "select sum(Case When Dr_Cr = 'Dr' then Amount * -1 Else Amount End) from Transactions where Accounting = 0 and [Description Code] = 'HAAAAA' and Void = 0"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300

            cn.Open()
            If IsDBNull(cmd.ExecuteScalar()) Or IsNothing(cmd.ExecuteScalar()) Then
                CreditorControlBalance = 0
            Else
                CreditorControlBalance = CDec(cmd.ExecuteScalar())
            End If
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return CreditorControlBalance
        Catch ex As Exception
        End Try
    End Function
    Function SumPaymentsRequestsByTypes() As Decimal

        Dim PaymentsRequests As Decimal = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try
            Dim UserRoles As String = "'"
            If Not IsNothing(LoggedUser_Roles) Then
                For Each role In LoggedUser_Roles
                    UserRoles &= role & "', '"
                Next
            End If

            UserRoles = UserRoles.Substring(0, UserRoles.Length - 3)

            Dim sSQL As String
            sSQL = "select sum(RequestedAmount) from PaymentsRequests where RequestApproved = 0 AND PaymentType in (" & UserRoles & ")"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300

            cn.Open()
            If IsDBNull(cmd.ExecuteScalar()) Or IsNothing(cmd.ExecuteScalar()) Then
                PaymentsRequests = 0
            Else
                PaymentsRequests = CDec(cmd.ExecuteScalar())
            End If
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return PaymentsRequests
        Catch ex As Exception
        End Try
    End Function
    Function SumPaymentsRequestsByOwner() As Decimal

        Dim PaymentsRequests As Decimal = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try


            Dim sSQL As String
            sSQL = "select sum(RequestedAmount) from PaymentsRequests where RequestOwner = '" & LoggedInUser & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300

            cn.Open()
            If IsDBNull(cmd.ExecuteScalar()) Or IsNothing(cmd.ExecuteScalar()) Then
                PaymentsRequests = 0
            Else
                PaymentsRequests = CDec(cmd.ExecuteScalar())
            End If
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return PaymentsRequests
        Catch ex As Exception
        End Try
    End Function
    Function CheckCreditNoteInInvoice(ByVal Reference As String) As Boolean

        Dim Count As Integer = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select count(*) from Transactions where Info2 = 'Credit Note' and [Description Code] = 'HAAAAA' AND Void = 0 AND Accounting= 0 AND Reference = '" & Reference
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & "' AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300

            cn.Open()
            Count = CInt(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            If Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("There was an error checking credit notes on the invoice! " & ex.Message & ".")
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try
    End Function
    Private Sub btnViewCreditNotes_Click(sender As Object, e As EventArgs) Handles btnViewCreditNotes.Click

        Dim CrNApproval As New frmCreditNoteApprovals
        CrNApproval.ckbAllSuppliers.Checked = True
        CrNApproval.FromSupplier = True
        CrNApproval.ShowDialog()
        Supplier_Payments_Load(sender, e)

    End Sub

#Region "Payments Variables"
    Dim PaymentsRequestsN As Integer = 0
    Dim PaymentsRequestAdvN
    Dim PaymentsN As Integer = 0
    Dim PayInAdvanceN As Integer = 0
#End Region

    Private Sub IfUserControlProcessON(ByVal TransactionID As Integer, ByVal PaymentType As String, ByVal PaymentAmount As String, _
                                       ByVal Reference As String, ByVal PaymentDate As String)
        Dim CheckResults = MBData.CheckRequestByTransID(TransactionID)
        Dim OriginalAmount As Decimal = CDec(MBData.GetInvoiceOriginalAmount(TransactionID))
        Dim PayInAdvance As String = CStr(My.Settings.PayInAdvance)
        Dim Invoice As Transaction = MBData.GetInvoice(TransactionID)

        Dim DateInQuesting As Date = CDate(PaymentDate)

        If cmbPaymentType.Text = "CASH" Then
            If CheckForCashup(DateInQuesting) > 0 Then
                MessageBox.Show("There where cashups done on the invoice date you selected, you cannot capture a paid invoice on this date, please contant the MagicBox support team in order to resolve this. Invoice Ref '" _
                                & Reference & "'.", "Invoice capture", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If
        End If

        If Not IsNothing(CheckResults) Then

            Dim RequestedAmount As Decimal = CDec(CheckResults.RequestedAmount)
            Dim Payment_Type As String = CStr(CheckResults.PaymentType)

            'Payment
            If LoggedUser_Roles.Contains(PaymentType) Then
                PayForUserControlOn(CInt(CheckResults.RequestID), CInt(CheckResults.InvoiceTransactionID), _
                                    PaymentType, RequestedAmount, PaymentAmount, Invoice, PaymentDate)
                AppendTransactions_SQL()
                Me.DGV_Transactions.Rows.Clear()

                If Not IFError Then
                    If BolHasAdvance Then
                        PayInAdvanceN += 1
                    Else
                        PaymentsN += 1
                    End If
                End If

            Else
                MessageBox.Show("The payment request against invoice '" & Invoice.Reference & "' already exist and will be approved as soon as a user with approval rights picks it.", "Request Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Else
            'Check if authorised to Pay
            If LoggedUser_Roles.Contains(PaymentType) Then
                'Check if is pay in advance
                If PaymentAmount > OriginalAmount Then

                    If PayInAdvance = "Yes" Then

                        'Payment
                        PayForUserControlOn(Nothing, TransactionID, _
                                            PaymentType, RequestedForPayment, PaymentAmount, Invoice, PaymentDate)
                        AppendTransactions_SQL()
                        Me.DGV_Transactions.Rows.Clear()

                        If Not IFError Then
                            If BolHasAdvance Then
                                'MessageBox.Show("Payment(s) succesfully made with Pay in advance transaction(s)", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                PayInAdvanceN += 1
                            Else
                                'MessageBox.Show("Payment(s) succesfully made.", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                PaymentsN += 1
                            End If
                        End If
                    Else

                        Dim Results As DialogResult = MessageBox.Show("The amount you want to pay is more than the invoice amount, in order to 'Pay in Advance'" & _
                        " enable the option on the settings screen, or do you rather want to enable 'Pay in Advance' now?", "Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        If Results = Windows.Forms.DialogResult.Yes Then

                            My.Settings.PayInAdvance = "Yes"
                            My.Settings.Save()
                            MessageBox.Show("'Pay in Advance option succesfully enabled' please wait while the system continue with processing your payment(s).", "Payments Approval", MessageBoxButtons.OK, MessageBoxIcon.Information)

                            'Payment
                            PayForUserControlOn(Nothing, TransactionID, _
                                    PaymentType, RequestedForPayment, PaymentAmount, Invoice, PaymentDate)
                            AppendTransactions_SQL()
                            Me.DGV_Transactions.Rows.Clear()

                            If BolHasAdvance Then
                                'MessageBox.Show("Payment(s) succesfully made with Pay in advance transaction(s)", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                PayInAdvanceN += 1
                            Else
                                'MessageBox.Show("Payment(s) succesfully made.", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                PaymentsN += 1
                            End If
                        Else

                            MessageBox.Show("Please supply the amount less than amount due on this invoice. Ref '" & Reference & "'.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.Enabled = True
                            Me.Cursor = Cursors.Arrow
                            Exit Sub

                        End If

                    End If

                Else
                    'Payment
                    PayForUserControlOn(Nothing, TransactionID, _
                    PaymentType, RequestedForPayment, PaymentAmount, Invoice, PaymentDate)
                    AppendTransactions_SQL()
                    Me.DGV_Transactions.Rows.Clear()

                    If BolHasAdvance Then
                        'MessageBox.Show("Payment(s) succesfully made with Pay in advance transaction(s)", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        PayInAdvanceN += 1
                    Else
                        'MessageBox.Show("Payment(s) succesfully made.", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        PaymentsN += 1
                    End If
                End If

            Else
                'Make a payment Request
                If PaymentAmount > OriginalAmount Then

                    If PayInAdvance = "Yes" Then

                        If IsNothing(MBData.CheckIfRequestExist(TransactionID, PaymentAmount)) Then
                            MBData.CreateNewRequest(TransactionID, PaymentType, PaymentAmount)
                            'MessageBox.Show("Your payment request, with an Advance payment has been succesfully processed, the payment will be approved as soon as the approver receives it.", "Payment Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            PaymentsRequestAdvN += 1
                        Else
                            MessageBox.Show("The  payment request against invoice '" & Invoice.Reference & "' already exist", "Request Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    Else

                        Dim Results As DialogResult = MessageBox.Show("The amount you requested is more than the invoice amount, in order to 'Pay in Advance'" & _
                        " enable the option on the settings screen, or do you rather want to enable 'Pay in Advance' now?", "Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        If Results = Windows.Forms.DialogResult.Yes Then

                            My.Settings.PayInAdvance = "Yes"
                            My.Settings.Save()
                            MessageBox.Show("'Pay in Advance option succesfully enabled' please wait while the system continue with processing your payment(s).", "Payments Approval", MessageBoxButtons.OK, MessageBoxIcon.Information)

                            'Make a request.
                            If IsNothing(MBData.CheckIfRequestExist(TransactionID, PaymentAmount)) Then
                                MBData.CreateNewRequest(TransactionID, PaymentType, PaymentAmount)
                                'MessageBox.Show("Your payment request, with an Advance payment has been succesfully processed, the payment will be approved as soon as the approver receives it.", "Payment Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                PaymentsRequestAdvN += 1
                            Else
                                MessageBox.Show("The  payment request against invoice '" & Invoice.Reference & "' already exist", "Request Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        Else

                            MessageBox.Show("Please supply the amount less than amount due on this invoice. Ref '" & Reference & "'.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.Enabled = True
                            Me.Cursor = Cursors.Arrow
                            Exit Sub

                        End If

                    End If

                Else
                    'Make a request.
                    If IsNothing(MBData.CheckIfRequestExist(TransactionID, PaymentAmount)) Then
                        MBData.CreateNewRequest(TransactionID, PaymentType, PaymentAmount)
                        'MessageBox.Show("Your payment request has been succesfully processed, the payment will be approved as soon as the approver receives it.", "Payment Request", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        PaymentsRequestsN += 1
                    Else
                        MessageBox.Show("The  payment request against invoice '" & Invoice.Reference & "' already exist", "Request Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            End If
        End If
    End Sub
    Public Sub CommitPayment(ByVal InvoiceLine As Transaction)

    End Sub
    Public Sub LoadInvoicesWithRequests(ByVal TransactinIDs As String)

        Try
            Dim sSQL As String = ""
            sSQL = CStr("Select * from dbo.Func_InvoicesWithRequest(" & My.Settings.Setting_CompanyID & ", " & TransactinIDs) & ") order by SupplierName"
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "InvoicesWithRequest")
            connection.Close()
            dgvInvoiceList.DataSource = ds
            dgvInvoiceList.DataMember = "InvoicesWithRequest"
            Dim DueColumn As DataGridViewColumn = dgvInvoiceList.Columns("Due")
            DueColumn.DefaultCellStyle.Format = "c"

            For Each row As DataGridViewRow In dgvInvoiceList.Rows
                Dim Reference As String = row.Cells("Reference").Value.ToString
                Dim HasCNote As Boolean = CBool(CheckCreditNoteInInvoice(Reference))
                If HasCNote Then
                    row.DefaultCellStyle.ForeColor = Color.DarkBlue
                    row.DefaultCellStyle.Font = New Font("Arial", 10, FontStyle.Bold)
                End If
            Next

            If dgvInvoiceList.ColumnCount > 0 Then
                Me.dgvInvoiceList.Columns(0).Visible = False
                Me.dgvInvoiceList.Columns(3).DefaultCellStyle.Format = "n2"
            End If

            LoadInvoicesRequestDetail()

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.AppStarting
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Public Sub LoadInvoicesRequestDetail()

        Try

            Dim UserRoles As String = "'"
            If Not IsNothing(LoggedUser_Roles) Then

                For Each item In LoggedUser_Roles
                    UserRoles &= item & ","
                Next
            End If
            UserRoles &= "'"

            Dim sSQL As String = ""
            sSQL = CStr("Select * from dbo.Func_GetPaymentRequestDetail(" & My.Settings.Setting_CompanyID) & "," & UserRoles & ") order by SupplierName"
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "PaymentRequestDetail")
            connection.Close()

            DGVResults.DataSource = ds
            DGVResults.DataMember = "PaymentRequestDetail"
            PeformPrepareRequestsData()

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.AppStarting
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Public Sub LoadSupplierInvoicesRequestDetail(ByVal SupplierID As Integer)

        Try

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            If LoggedUser_Roles.Count <> 0 Then

                Dim UserRoles As String = "'"
                For Each item In LoggedUser_Roles
                    UserRoles &= item & ","
                Next

                UserRoles &= "'"

                Dim sSQL As String = ""
                sSQL = CStr("Select * from dbo.Func_GetSupplierPaymentRequestDetail(" & My.Settings.Setting_CompanyID) & ", " _
                    & SupplierID & "," & UserRoles & "  ) order by SupplierName"

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "PaymentRequestDetail")
                connection.Close()
                'dgvPaymentsList.Rows.Clear()
                DGVResults.DataSource = ds
                DGVResults.DataMember = "PaymentRequestDetail"
                If dgvSupplierSum.Rows.Count <> 0 Then

                    If dgvSupplierSum.CurrentRow.DefaultCellStyle.ForeColor = Color.LimeGreen Then
                        dgvPaymentsList.Rows.Clear()
                        PeformPrepareRequestsData()
                    End If

                End If
            End If
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            'MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Public Sub PeformPrepareRequestsData()

        dgvPaymentsList.DataSource = Nothing
        dgvPaymentsList.Rows.Clear()
        dgvPaymentsList.Refresh()

        For Each row As DataGridViewRow In DGVResults.Rows

            If row IsNot Nothing Then

                If LoggedUser_Roles.Contains(row.Cells("PaymentType").Value) Then

                    Dim newRow As New DataGridViewRow()
                    Dim oAmount As Decimal
                    Dim oDue As Decimal

                    newRow.CreateCells(Me.dgvInvoiceList)

                    newRow.Cells(0).Value = row.Cells("ID").Value
                    newRow.Cells(1).Value = row.Cells("SupplierName").Value
                    newRow.Cells(2).Value = row.Cells("Reference").Value
                    'newRow.Cells(2)
                    newRow.Cells(4).Value = row.Cells("TransactionDate").Value.ToString("dd MMM yyyy")

                    'Now gather the Amount and Pad cell values in top DGV
                    If Not IsNumeric(row.Cells("Amount").Value) Then
                        oAmount = 0
                    Else
                        oAmount = row.Cells("Amount").Value
                        InvoiceAmount = oAmount
                    End If

                    newRow.Cells(3).Value = oAmount
                    newRow.Cells(5).Value = row.Cells("PaymentType").Value
                    newRow.DefaultCellStyle.ForeColor = Color.LimeGreen
                    dgvPaymentsList.Rows.Add(newRow)
                End If
            End If
        Next
        ToPay()
        TotalRequested = RequestedForPayment().ToString("c")
    End Sub
    Private Function CountPayments(ByVal TransactionID As Integer) As Integer

        Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try

            Dim sSQL As String
            Dim Count As Integer = 0

            sSQL = "SELECT Count(ID) FROM Transactions WHERE LinkID = " & TransactionID & ""
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300

            With connection
                connection.Open()
                Count = cmd.ExecuteScalar()
                connection.Close()
            End With

            Return Count
        Catch ex As Exception
            If connection.State = ConnectionState.Open Then
                connection.Close()
                connection.Dispose()
            End If
            MessageBox.Show("An Error occured with details : " & ex.Message)
        End Try
    End Function
    Private Sub txtUnapprovedCNotes_TextChanged(sender As Object, e As EventArgs) Handles txtUnapprovedCNotes.TextChanged
        If CDec(txtUnapprovedCNotes.Text) > 0 Then
            btnViewCreditNotes.Enabled = True
        Else
            btnViewCreditNotes.Enabled = False
        End If
    End Sub
    Private Sub dgvInvoiceList_SelectionChanged(sender As Object, e As EventArgs) Handles dgvInvoiceList.SelectionChanged
        Dim oRowIndex As Integer
        For Each cell As DataGridViewCell In dgvInvoiceList.SelectedCells
            oRowIndex = dgvInvoiceList.CurrentRow.Index
            dgvInvoiceList.Rows(oRowIndex).Selected = True
        Next
    End Sub
    Private Sub dgvInvoiceList_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles dgvInvoiceList.DataBindingComplete
        Call TotalPaidBox()
    End Sub
    Private Sub dgvInvoiceList_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvInvoiceList.MouseDown
        Try
            Dim info As DataGridView.HitTestInfo = Me.dgvInvoiceList.HitTest(e.X, e.Y)
            Dim rows As DataGridViewSelectedRowCollection = Me.dgvInvoiceList.SelectedRows ''PROBLEM
            Me.dgvInvoiceList.DoDragDrop(rows, DragDropEffects.Copy)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub txtPaymentsRequests_TextChanged(sender As Object, e As EventArgs) Handles txtPaymentsRequests.TextChanged
        If CDec(txtPaymentsRequests.Text) > 0 Then
            btnViewPaymentsRequests.Enabled = True
        Else
            btnViewPaymentsRequests.Enabled = False
        End If
    End Sub
    Private Sub btnViewPaymentsRequests_Click(sender As Object, e As EventArgs) Handles btnViewPaymentsRequests.Click
        If My.Settings.User_Control_On = "Yes" Then
            Dim frmPayreq As New frmPaymentsRequests
            frmPayreq.ShowDialog()
            Supplier_Payments_Load(sender, e)
        Else
            MessageBox.Show("This feature of the MagicBox functions when user control mode is on, the current mode is off!", "Payments Requests", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Private Sub btnUpdatePayments_Click(sender As Object, e As EventArgs) Handles btnUpdatePayments.Click
        Try
            Dim DGV_Control As DataGridView
            If TabControl1.SelectedTab.Name = "TB_Requests" Then
                DGV_Control = DGV_Requests
            Else
                DGV_Control = dgvPaymentsList
            End If
            If dtpPaymentDate.Value > Date.Now Then
                MessageBox.Show("Payment date cannot be after today", "Payment date", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbPaymentType.Focus()
                Exit Sub
            End If
            If DGV_Control.Rows.Count = 0 Then
                MessageBox.Show("There are no invoice(s) to update, allocate invoice(s) for payment in order to continue", "Payment date updating", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbPaymentType.Focus()
                Exit Sub
            End If
            If DGV_Control.SelectedRows.Count = 0 Then
                MessageBox.Show("Please select invoice(s) that you want to update the payment date.", "Payment date updating", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbPaymentType.Focus()
                Exit Sub
            End If
            If DGV_Control.Rows.Count <> 0 Then
                Dim PaymentDate As String = dtpPaymentDate.Value.ToString("dd MMMM yyyy")
                Dim PaymentType As String = cmbPaymentType.Text
                For Each row As DataGridViewRow In DGV_Control.SelectedRows
                    '        If row.Cells(1).Value <> Nothing Then
                    row.Cells("TransactionDate").Value = PaymentDate
                    row.Cells("PaymentType").Value = PaymentType
                    ' End If
                    If TabControl1.SelectedTab.Name = "TB_Requests" Then
                        UpdatePmtRequest(row.Cells("RequestID").Value, False, 0, My.Settings.FirstName, row.Cells("RequestID").Value, False, row.Cells("RequestedAmount").Value, row.Cells("PaymentType").Value)
                    End If
                Next
            End If
            SumRequests()
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub

    Private Sub DGV_Requests_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Requests.CellEndEdit
        Try
            Dim RwIndesx As Long = e.RowIndex
            If e.RowIndex < 0 Then Exit Sub
            If DGV_Requests.Rows(RwIndesx).Cells("RequestedAmount").Value > InvoiceDueAmount(DGV_Requests.Rows(RwIndesx).Cells("TransactionID").Value) Then
                MsgBox("You have change to amount to greater then the invoice!!!!")
                Call LoadPayRequests()
            End If
            UpdatePmtRequest(DGV_Requests.Rows(RwIndesx).Cells("RequestID").Value, False, 0, My.Settings.FirstName, DGV_Requests.Rows(RwIndesx).Cells("RequestID").Value, False, DGV_Requests.Rows(RwIndesx).Cells("RequestedAmount").Value, DGV_Requests.Rows(RwIndesx).Cells("PaymentType").Value)
            SumRequests()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub DGV_Requests_DragDrop(sender As Object, e As DragEventArgs) Handles DGV_Requests.DragDrop

        Try
            ' For Each col As DataGridViewColumn In DGV_Requests.Columns
            ' MsgBox(col.Name)
            ' Next


            '***** Create datatable for requests *******
            Dim dt As New DataTable

            dt.Columns.Add("InvoiceTransactionID")
            dt.Columns.Add("CapturedDate")
            dt.Columns.Add("PaymentType")
            dt.Columns.Add("RequestedAmount")
            dt.Columns.Add("RequestApproved")
            dt.Columns.Add("ApprovedAmount")
            dt.Columns.Add("PaymentTransactionID")
            dt.Columns.Add("RequestOwner")
            dt.Columns.Add("RequestApprover")
            dt.Columns.Add("PayInAdvanceTransactionID")
            dt.Columns.Add("Co_ID")
            dt.Columns.Add("BankRequestID")
            dt.Columns.Add("BankExported")

            Dim rows As DataGridViewSelectedRowCollection = TryCast(e.Data.GetData(GetType(DataGridViewSelectedRowCollection)), DataGridViewSelectedRowCollection)

            lbl_RequestStatus.Text = "Prepairing requests..."

            If cmbPaymentType.SelectedIndex = -1 Then
                MessageBox.Show("Please select payment Type", "Supplier Payments", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbPaymentType.Focus()
                Exit Sub
            End If

            For Each row In rows
                lbl_RequestStatus.Text = rows.Count & " to Process"
                Dim R As DataRow = dt.NewRow
                Dim newRow As New DataGridViewRow()
                Dim oAmount As Decimal
                Dim oDue As Decimal
                lbl_RequestStatus.Text = CStr(row.Cells("Reference").Value)
                If CDec(row.cells("Due").value) <= 0 Then
                    MessageBox.Show("Payment cannot be made for negative or zero value(Amount Due)", "Payments", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Continue For
                End If

                If IsNothing(MBData.CheckIfRequestExist2(CInt(row.Cells("TransactionID").Value))) Then
                Else
                    MessageBox.Show("The  payment request against invoice '" & CStr(row.Cells("Reference").Value) & "' already exist", "Request Exist", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Continue For
                End If

                Dim TransID As Integer = CInt(row.Cells("TransactionID").Value)
                Dim SupplierName As String = CStr(row.Cells("SupplierName").Value)
                Dim Reference As String = CStr(row.Cells("Reference").Value)
                Dim Amount As Decimal = CDec(row.Cells("Amount").Value)

                R("InvoiceTransactionID") = TransID
                R("CapturedDate") = Now.ToString("dd MMM yyyy")
                R("PaymentType") = Me.cmbPaymentType.Text
                R("RequestedAmount") = CDec(row.Cells("Due").Value)
                R("RequestApproved") = False
                R("ApprovedAmount") = 0
                R("PaymentTransactionID") = 0
                R("RequestOwner") = My.Settings.Username
                R("RequestApprover") = My.Settings.FirstName
                R("PayInAdvanceTransactionID") = 0
                R("Co_ID") = My.Settings.Setting_CompanyID
                R("BankRequestID") = TransID
                R("BankExported") = "False"

                'If CInt(row.Cells("TransactionID").Value) = TransID And CStr(row.Cells("SupplierName").Value) = SupplierName _
                'And CStr(row.Cells("Reference").Value) = Reference And CDec(row.Cells("Due").Value) = Amount Then
                'MessageBox.Show("The invoice line you selected already exixts in invoice lines for payment, Please select different Invoice line", "Invoice Payments", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                'Exit Sub
                'Else
                dt.Rows.Add(R)
                'End If

                '***Color line orange -on the drop
                Me.dgvInvoiceList.Rows(row.Index).DefaultCellStyle.ForeColor = Color.OrangeRed
                '*********
                'Manually add row without refresh


                Dim myrow As DataRow = dsReq.Tables(0).NewRow
                myrow("RequestID") = 0
                myrow("TransactionID") = TransID
                myrow("SupplierID") = GetSupplierID(SupplierName)
                myrow("SupplierName") = SupplierName
                myrow("Reference") = Reference
                myrow("RequestedAmount") = Amount
                myrow("TransactionDate") = Now.ToString("dd MMM yyyy")
                myrow("PaymentType") = Me.cmbPaymentType.Text

                dsReq.Tables(0).Rows.Add(myrow)
                '*** Force Refresh before Pay
                Button2.Text = "Refresh"

                '**************** End Manual Add ************
            Next
            Using cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                cn.Open()
                Using copy As New SqlBulkCopy(cn)
                    copy.ColumnMappings.Add("InvoiceTransactionID", "InvoiceTransactionID")
                    copy.ColumnMappings.Add("CapturedDate", "CapturedDate")
                    copy.ColumnMappings.Add("PaymentType", "PaymentType")
                    copy.ColumnMappings.Add("RequestedAmount", "RequestedAmount")
                    copy.ColumnMappings.Add("RequestApproved", "RequestApproved")
                    copy.ColumnMappings.Add("ApprovedAmount", "ApprovedAmount")
                    copy.ColumnMappings.Add("PaymentTransactionID", "PaymentTransactionID")
                    copy.ColumnMappings.Add("RequestOwner", "RequestOwner")
                    copy.ColumnMappings.Add("RequestApprover", "RequestApprover")
                    copy.ColumnMappings.Add("PayInAdvanceTransactionID", "PayInAdvanceTransactionID")
                    copy.ColumnMappings.Add("Co_ID", "Co_ID")
                    copy.ColumnMappings.Add("BankRequestID", "BankRequestID")
                    copy.ColumnMappings.Add("BankExported", "BankExported")
                    copy.DestinationTableName = "PaymentsRequests"
                    copy.WriteToServer(dt)
                End Using
                cn.Close()
            End Using
            '***** Remove need to refresh entire grid****
            'Call LoadPayRequests()

            Me.dgvInvoiceList.ClearSelection()
            SumRequests()
        Catch
            MsgBox(Err.Description)
        End Try
    End Sub

    Private Sub DGV_Requests_DragEnter(sender As Object, e As DragEventArgs) Handles DGV_Requests.DragEnter
        Try
            e.Effect = DragDropEffects.All
        Catch
        End Try
    End Sub
    Function LoadPayRequests(Optional ByVal FilterSupplierID As Long = 0)
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            DGV_Requests.DataSource = Nothing
            dsReq.Clear()
            Dim sSQL As String = ""
            sSQL = CStr("SELECT distinct RequestID,TransactionID,Suppliers.SupplierID,Suppliers.SupplierName,Transactions.Reference, " & _
            "RequestedAmount,'" & Me.dtpPaymentDate.Value.ToString("dd MMM yyyy") & "' as [TransactionDate], PaymentsRequests.PaymentType as [PaymentType] " & _
            "FROM [PaymentsRequests] " & _
            "left Join Transactions " & _
            "On transactions.TransactionID = PaymentsRequests.InvoiceTransactionID " & _
            "Left Join Suppliers " & _
            "on Suppliers.SupplierID = Transactions.SupplierID " & _
            "where RequestApproved = 0 and [Transaction Type] = '12' And [PaymentsRequests].Co_ID = " & My.Settings.Setting_CompanyID)
            If FilterSupplierID <> 0 Then
                sSQL = sSQL & CStr(" and Suppliers.SupplierID = " & FilterSupplierID)
            End If
            sSQL = sSQL & CStr(" Order by RequestID,TransactionID,Suppliers.SupplierName,Transactions.Reference, " & _
            "RequestedAmount,PaymentsRequests.PaymentType")

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)

            connection.Open()
            dataadapter.Fill(dsReq, "CashPay")
            connection.Close()
            DGV_Requests.DataSource = dsReq
            DGV_Requests.DataMember = "CashPay"
            DGV_Requests.Columns("RequestID").Visible = False
            DGV_Requests.Columns("SupplierID").Visible = False
            DGV_Requests.Columns("Ignor").Width = 45
            DGV_Requests.Columns("TransactionID").Width = 100
            Me.TabControl1.TabPages("TB_Requests").Text = "Requests (" & dsReq.Tables(0).Rows.Count & ")"
            Call SumRequests()
            Button2.Text = "Pay"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Function
    Function SumRequests()
        Dim TRequests As Double = 0
        Dim row As DataGridViewRow
        For Each row In DGV_Requests.Rows
            Dim isSelected As Boolean = Convert.ToBoolean(row.Cells("Ignor").Value)
            If isSelected = False Then
                TRequests = TRequests + row.Cells("RequestedAmount").Value
            End If
        Next
        TB_TotalRequests.Text = TRequests.ToString("c")
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            If Button2.Text = "Refresh" Then
                If dgvSupplierSum.SelectedRows.Count > 0 Then
                    Dim i As Integer
                    Dim intRwIndex As Integer
                    intRwIndex = dgvSupplierSum.SelectedRows(i).Index
                    Dim SupplierID = dgvSupplierSum.Item("SupplierID", intRwIndex).Value
                    SelectedSupplier = SupplierID
                    LoadPayRequests(SelectedSupplier)
                Else
                    LoadPayRequests()
                End If
                Exit Sub
            End If

            Dim TxTable As DataTable = Globals.Ribbons.Ribbon1.TransactionsTable
            Dim RemitTable As DataTable = Globals.Ribbons.Ribbon1.RemittanceTable
            Dim counter As Long = 0
            Dim Bankcounter As Long = 0
            Dim pBatchID As Integer
            Dim PFinYear As Integer
            Dim pDate As String
            Dim pPeriod As Integer
            Dim pTransactionID As Integer
            Dim pDescription As String
            Dim pPaymentType As String
            Dim pTypeAccount As String
            Dim PAmount As Double
            Dim pCreditorControlAccount As String
            Dim pRef As String
            Dim PSupplierName As String
            Dim pSupplierID As Integer
            Dim SentToBank As String = ""
            Application.DoEvents()
            Dim BankRequestsIDs
            Dim BankRequestsBatchID As Long
            Dim RequestIDs
            TxTable.Rows.Clear()
            RemitTable.Rows.Clear()
            pBatchID = oCreateBatchID()

            For Each row As DataGridViewRow In DGV_Requests.Rows
                If row.Cells("Ignor").Value = False Then
                    If row.Cells("RequestID").Value = 0 Then
                        MsgBox("Please note that there are requests that have been loaded with out the grid refreshing. Please refresh the grid before trying to make payments.", MsgBoxStyle.Exclamation, "Refresh Required")
                        Exit Sub
                    End If
                End If
                Dim strPayExclusions()
                strPayExclusions = Split(Globals.Ribbons.Ribbon1.ExcludedPayTypes, "|")
                For Each y In strPayExclusions
                    If row.Cells("PaymentType").Value = y Then
                        MsgBox("Transaction aborted! Payment '" & row.Cells("PaymentType").Value & "' is not authorised for your user role!", MsgBoxStyle.Exclamation, "Limited by Access")
                        Exit Sub
                    End If
                Next
            Next

            For Each row As DataGridViewRow In DGV_Requests.Rows
                If row.Cells("Ignor").Value = False Then
                    counter = counter + 1
                    RequestIDs = 0
                    'Get ID's for the Request Update
                    If counter = 1 Then
                        RequestIDs = row.Cells("RequestID").Value
                    Else
                        RequestIDs = RequestIDs & "," & row.Cells("RequestID").Value
                    End If
                    lbl_RequestStatus.Text = "Prepping " & row.Cells("Reference").Value & "..."
                    Application.DoEvents()
                    pTransactionID = oCreateTransactionID()
                    PFinYear = GetFinYear(row.Cells("TransactionDate").Value)
                    pPeriod = GetPeriod(row.Cells("TransactionDate").Value)
                    pPaymentType = row.Cells("PaymentType").Value
                    pRef = row.Cells("Reference").Value
                    pDescription = pPaymentType & "-" & pRef
                    PAmount = row.Cells("RequestedAmount").Value
                    pSupplierID = row.Cells("SupplierID").Value
                    PSupplierName = row.Cells("SupplierName").Value
                    pDate = row.Cells("TransactionDate").Value
                    pTypeAccount = Get_Segment4_from_DisplayName_in_Accounting(pPaymentType)
                    pCreditorControlAccount = Get_Segment4_Using_Type_in_Accounting("Creditors Control")

                    Dim InvoiceTxID As Integer = row.Cells("TransactionID").Value

                    '**Add to remittance table
                    RemittanceDataTable_AddRow(InvoiceTxID, PSupplierName, pSupplierID, pDate, pRef, PAmount, pPaymentType)
                    '**

                    TxDataTale_AddRow(pBatchID, pTransactionID, InvoiceTxID, pDate, PFinYear, pPeriod, pRef, pDescription, PAmount, _
                                      0, 0, "Cr", pTypeAccount, pSupplierID, PSupplierName, 0, True, 1, "Payment", "Payment", "Payment", pPaymentType)
                    TxDataTale_AddRow(pBatchID, pTransactionID, InvoiceTxID, pDate, PFinYear, pPeriod, pRef, pDescription, PAmount, _
                                      0, 0, "Dr", pCreditorControlAccount, pSupplierID, PSupplierName, 0, True, 1, "Payment", "Payment", "Payment", pPaymentType)
                    lbl_RequestStatus.Text = "Adding " & row.Cells("Reference").Value & " to batch"
                    Application.DoEvents()
                End If
            Next
            lbl_RequestStatus.Text = "Posting Batch..."
            Application.DoEvents()
            If PostTxDataTable() = True Then
                SendRemittance()
                MsgBox("Invoice(s) successfuly posted", MsgBoxStyle.Information, "Success")
                For Each row As DataGridViewRow In DGV_Requests.Rows
                    If row.Cells("Ignor").Value = False Then
                        lbl_RequestStatus.Text = "Updating " & row.Cells("Reference").Value & " request status"
                        Dim PrepBankID As Long
                        PrepBankID = 0
                        Call UpdatePmtRequest(row.Cells("RequestID").Value, True, row.Cells("RequestedAmount").Value, My.Settings.FirstName, PrepBankID, False, row.Cells("RequestedAmount").Value, row.Cells("PaymentType").Value)
                    End If
                Next
                LoadPayScreen()
            Else
            End If
            lbl_RequestStatus.Text = "Ready"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Btn_all_Click(sender As Object, e As EventArgs) Handles Btn_all.Click
            For Each row As DataGridViewRow In DGV_Requests.Rows
                row.Cells("Ignor").Value = True
            Next
    End Sub
    Private Sub btn_DeselectAll_Click(sender As Object, e As EventArgs) Handles btn_DeselectAll.Click
        For Each row As DataGridViewRow In DGV_Requests.Rows
            row.Cells("Ignor").Value = False
        Next
    End Sub
    Private Sub Btn_Bank_Click(sender As Object, e As EventArgs) Handles Btn_Bank.Click
        Dim Frm_ACB As New frmPay_ACB_Files
        Frm_ACB.ShowDialog()
    End Sub

    Private Sub dgvPaymentsList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPaymentsList.CellContentClick
        Dim colName As String = dgvPaymentsList.Columns(e.ColumnIndex).Name
        If colName = "Action" Then
            dgvPaymentsList.Rows.RemoveAt(e.RowIndex)
        End If
    End Sub


    Private Sub DGV_Requests_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles DGV_Requests.CurrentCellDirtyStateChanged
        SumRequests()
    End Sub

    Private Sub DGV_Requests_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Requests.CellContentClick
        If DGV_Requests.Columns(e.ColumnIndex).HeaderText = "Ignor" Then
            DGV_Requests.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub dgvSupplierSum_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSupplierSum.CellContentClick

    End Sub


End Class