﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS


Module modCreditNotes


    Public cInvoiceAmount As Decimal
    Public oProportion As Decimal 'holds the proportion of credit note over invoice

    Public cBatchID As Integer
    Public cTransactionID As Long
    Public cLinkID As Integer
    Public cTransactionDate As Date
    Public cCaptureDate As Date
    Public cPPeriod As String
    Public cFinYear As String
    Public cGDC As String
    Public cInvoiceNumber As String
    Public cDescription As String
    Public cAccountNumber As String
    Public cContraAccount As String
    Public cLinkAcc As String
    Public cAmount As Decimal
    Public cTaxType As Integer
    Public cTaxAmount As Decimal
    Public cUserID As String
    Public cSupplierID As Integer
    Public cEmployeeID As Integer
    Public cDescription2 As String
    Public cDescription3 As String
    Public cDescription4 As String
    Public cDescription5 As String
    Public cPosted As Boolean
    Public cAccounting As Boolean
    Public cVoid As Boolean
    Public cTransactionType As Integer
    Public cDRCR As String
    Public cDescriptionCode As String
    Public cDocType As String
    Public cSupplierName As String
    Public cInfo As String
    Public cInfo2 As String
    Public cInfo3 As String

    Public cProportionalAmount As Decimal
    Public cNew_TransactionID As String
    Public cNew_LinkID As String
    Public cNew_CaptureDate As String



    Public Sub IssueCreditNote(ByVal oTransID As Integer, ByVal Credit_Amount As String, ByVal oSupplier_Invoice_Reference As String, ByVal oSupplierName As String, SupplierInvoiceAmount As String)
        Try

            Dim oBatchID As Integer


            Dim oLinkID As Integer = 0

            Dim sCaptureDate As String = Now.Date.ToString("dd MMMM yyyy")

            Dim sPPeriod As String 'in loop below
            ''Dim sFinYear As String = My.Settings.Setting_Year 'Set this once
            Dim sFinYear As String = GetFinYear(sCaptureDate)

            '=======++++++++++++ First Generate TransactionID and BatchID =========================== 
            oBatch_or_Transaction_generation_Failed = False

            oBatchID = CLng(oCreateBatchID())

            cNew_TransactionID = CLng(oCreateTransactionID()) 'adds a new record to the TransactionID table thus creating a new ID number that is used as the Transaction ID


        Catch ex As Exception
            MsgBox(ex.Message)


        End Try
    End Sub


    Sub Load_Invoice_Data_From_ID_to_Variables(ByVal oID As Integer)

        Try

            Dim sSQL As String

            sSQL = "SELECT * FROM Transactions WHERE ID = " & oID

            If My.Settings.DBType = "SQL" Then

                Dim xConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, xConnection)
                cmd.CommandTimeout = 300
                xConnection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                Try

                    While datareader.Read

                        cBatchID = datareader("BatchID")
                        cTransactionID = datareader("TransactionID")
                        cLinkID = datareader("LinkID")
                        cTransactionDate = datareader("Transaction Date")
                        cCaptureDate = datareader("Capture Date")
                        cPPeriod = datareader("PPeriod")
                        cFinYear = datareader("Fin Year")
                        cGDC = datareader("GDC")
                        cInvoiceNumber = datareader("Reference")
                        cDescription = datareader("Description")
                        cAccountNumber = datareader("AccNumber")
                        cContraAccount = datareader("Contra Account")
                        If Not IsDBNull(datareader("LinkAcc")) Then
                            cLinkAcc = datareader("LinkAcc")
                        Else
                            cLinkAcc = ""
                        End If
                        cAmount = datareader("Amount")
                        cTaxType = datareader("TaxType")
                        cTaxAmount = datareader("Tax Amount")
                        cUserID = datareader("UserID")
                        cSupplierID = datareader("SupplierID")
                        cEmployeeID = datareader("EmployeeID")
                        cDescription2 = datareader("Description 2")
                        cDescription3 = datareader("Description 3")
                        cDescription4 = datareader("Description 4")
                        cDescription5 = datareader("Description 5")
                        cPosted = datareader("Posted")
                        cAccounting = datareader("Accounting")
                        cVoid = datareader("Void")
                        cTransactionType = datareader("Transaction Type")
                        cDRCR = datareader("DR_CR")
                        cDescriptionCode = datareader("Description Code")
                        cDocType = datareader("DocType")
                        cSupplierName = datareader("SupplierName")

                    End While

                    xConnection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message)
                    xConnection.Close()
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub



End Module
