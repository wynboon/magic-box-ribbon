﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployee_New
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployee_New))
        Me.EmployeeTab = New System.Windows.Forms.TabControl()
        Me.tbBasicDetails = New System.Windows.Forms.TabPage()
        Me.ckbIsScheduler = New System.Windows.Forms.CheckBox()
        Me.txtIDNumber = New System.Windows.Forms.TextBox()
        Me.btnClearBasic = New System.Windows.Forms.Button()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dtpContractEnds = New System.Windows.Forms.DateTimePicker()
        Me.dtpDateSarted = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtContact2 = New System.Windows.Forms.TextBox()
        Me.txtContactNumber1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbStatus = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbEmployeeType = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSurname = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.tbPAYEDetails = New System.Windows.Forms.TabPage()
        Me.txtPayrollNumber = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnClearPaye = New System.Windows.Forms.Button()
        Me.txtWaiterNumber = New System.Windows.Forms.TextBox()
        Me.txtLoanAmount = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtWage = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCommission = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbPaymentType = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbBankingDetails = New System.Windows.Forms.TabPage()
        Me.btnClearBanking = New System.Windows.Forms.Button()
        Me.txtBranchNumber = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtAccNumber = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtBankName = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtAccountHolder = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.EmployeeTab.SuspendLayout()
        Me.tbBasicDetails.SuspendLayout()
        Me.tbPAYEDetails.SuspendLayout()
        Me.tbBankingDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'EmployeeTab
        '
        Me.EmployeeTab.Controls.Add(Me.tbBasicDetails)
        Me.EmployeeTab.Controls.Add(Me.tbPAYEDetails)
        Me.EmployeeTab.Controls.Add(Me.tbBankingDetails)
        Me.EmployeeTab.Location = New System.Drawing.Point(0, 0)
        Me.EmployeeTab.Margin = New System.Windows.Forms.Padding(4)
        Me.EmployeeTab.Name = "EmployeeTab"
        Me.EmployeeTab.SelectedIndex = 0
        Me.EmployeeTab.Size = New System.Drawing.Size(592, 574)
        Me.EmployeeTab.TabIndex = 1
        '
        'tbBasicDetails
        '
        Me.tbBasicDetails.Controls.Add(Me.btnExit)
        Me.tbBasicDetails.Controls.Add(Me.ckbIsScheduler)
        Me.tbBasicDetails.Controls.Add(Me.txtIDNumber)
        Me.tbBasicDetails.Controls.Add(Me.btnAdd)
        Me.tbBasicDetails.Controls.Add(Me.btnClearBasic)
        Me.tbBasicDetails.Controls.Add(Me.txtAddress)
        Me.tbBasicDetails.Controls.Add(Me.Label15)
        Me.tbBasicDetails.Controls.Add(Me.dtpContractEnds)
        Me.tbBasicDetails.Controls.Add(Me.dtpDateSarted)
        Me.tbBasicDetails.Controls.Add(Me.Label13)
        Me.tbBasicDetails.Controls.Add(Me.Label12)
        Me.tbBasicDetails.Controls.Add(Me.Label11)
        Me.tbBasicDetails.Controls.Add(Me.txtContact2)
        Me.tbBasicDetails.Controls.Add(Me.txtContactNumber1)
        Me.tbBasicDetails.Controls.Add(Me.Label10)
        Me.tbBasicDetails.Controls.Add(Me.Label6)
        Me.tbBasicDetails.Controls.Add(Me.cmbStatus)
        Me.tbBasicDetails.Controls.Add(Me.Label4)
        Me.tbBasicDetails.Controls.Add(Me.cmbEmployeeType)
        Me.tbBasicDetails.Controls.Add(Me.Label3)
        Me.tbBasicDetails.Controls.Add(Me.txtSurname)
        Me.tbBasicDetails.Controls.Add(Me.Label2)
        Me.tbBasicDetails.Controls.Add(Me.Label1)
        Me.tbBasicDetails.Controls.Add(Me.txtName)
        Me.tbBasicDetails.Location = New System.Drawing.Point(4, 25)
        Me.tbBasicDetails.Margin = New System.Windows.Forms.Padding(4)
        Me.tbBasicDetails.Name = "tbBasicDetails"
        Me.tbBasicDetails.Padding = New System.Windows.Forms.Padding(4)
        Me.tbBasicDetails.Size = New System.Drawing.Size(584, 545)
        Me.tbBasicDetails.TabIndex = 0
        Me.tbBasicDetails.Text = "Basic Details"
        Me.tbBasicDetails.UseVisualStyleBackColor = True
        '
        'ckbIsScheduler
        '
        Me.ckbIsScheduler.AutoSize = True
        Me.ckbIsScheduler.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbIsScheduler.ForeColor = System.Drawing.Color.Navy
        Me.ckbIsScheduler.Location = New System.Drawing.Point(185, 358)
        Me.ckbIsScheduler.Margin = New System.Windows.Forms.Padding(4)
        Me.ckbIsScheduler.Name = "ckbIsScheduler"
        Me.ckbIsScheduler.Size = New System.Drawing.Size(149, 23)
        Me.ckbIsScheduler.TabIndex = 93
        Me.ckbIsScheduler.Text = "Use on Scheduler"
        Me.ckbIsScheduler.UseVisualStyleBackColor = True
        '
        'txtIDNumber
        '
        Me.txtIDNumber.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDNumber.Location = New System.Drawing.Point(188, 97)
        Me.txtIDNumber.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtIDNumber.MaxLength = 20
        Me.txtIDNumber.Name = "txtIDNumber"
        Me.txtIDNumber.Size = New System.Drawing.Size(372, 23)
        Me.txtIDNumber.TabIndex = 3
        '
        'btnClearBasic
        '
        Me.btnClearBasic.Location = New System.Drawing.Point(400, 352)
        Me.btnClearBasic.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnClearBasic.Name = "btnClearBasic"
        Me.btnClearBasic.Size = New System.Drawing.Size(161, 30)
        Me.btnClearBasic.TabIndex = 10
        Me.btnClearBasic.Text = "&Clear Window"
        Me.btnClearBasic.UseVisualStyleBackColor = True
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(188, 282)
        Me.txtAddress.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtAddress.MaxLength = 200
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(373, 61)
        Me.txtAddress.TabIndex = 9
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Navy
        Me.Label15.Location = New System.Drawing.Point(9, 282)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(61, 19)
        Me.Label15.TabIndex = 92
        Me.Label15.Text = "Address"
        '
        'dtpContractEnds
        '
        Me.dtpContractEnds.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpContractEnds.Location = New System.Drawing.Point(188, 250)
        Me.dtpContractEnds.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtpContractEnds.Name = "dtpContractEnds"
        Me.dtpContractEnds.Size = New System.Drawing.Size(373, 23)
        Me.dtpContractEnds.TabIndex = 8
        '
        'dtpDateSarted
        '
        Me.dtpDateSarted.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateSarted.Location = New System.Drawing.Point(188, 219)
        Me.dtpDateSarted.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtpDateSarted.Name = "dtpDateSarted"
        Me.dtpDateSarted.Size = New System.Drawing.Size(373, 23)
        Me.dtpDateSarted.TabIndex = 7
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Navy
        Me.Label13.Location = New System.Drawing.Point(11, 255)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(103, 19)
        Me.Label13.TabIndex = 90
        Me.Label13.Text = "Contact Ends"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Navy
        Me.Label12.Location = New System.Drawing.Point(11, 224)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(91, 19)
        Me.Label12.TabIndex = 89
        Me.Label12.Text = "Day Started"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Navy
        Me.Label11.Location = New System.Drawing.Point(9, 194)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(138, 19)
        Me.Label11.TabIndex = 88
        Me.Label11.Text = "Contact Number 2"
        '
        'txtContact2
        '
        Me.txtContact2.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContact2.Location = New System.Drawing.Point(188, 190)
        Me.txtContact2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtContact2.MaxLength = 20
        Me.txtContact2.Name = "txtContact2"
        Me.txtContact2.Size = New System.Drawing.Size(373, 23)
        Me.txtContact2.TabIndex = 6
        '
        'txtContactNumber1
        '
        Me.txtContactNumber1.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactNumber1.Location = New System.Drawing.Point(188, 160)
        Me.txtContactNumber1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtContactNumber1.MaxLength = 20
        Me.txtContactNumber1.Name = "txtContactNumber1"
        Me.txtContactNumber1.Size = New System.Drawing.Size(373, 23)
        Me.txtContactNumber1.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Navy
        Me.Label10.Location = New System.Drawing.Point(9, 164)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(138, 19)
        Me.Label10.TabIndex = 87
        Me.Label10.Text = "Contact Number 1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Navy
        Me.Label6.Location = New System.Drawing.Point(11, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 19)
        Me.Label6.TabIndex = 83
        Me.Label6.Text = "Status"
        '
        'cmbStatus
        '
        Me.cmbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbStatus.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbStatus.FormattingEnabled = True
        Me.cmbStatus.Items.AddRange(New Object() {"Active", "Inactive"})
        Me.cmbStatus.Location = New System.Drawing.Point(188, 127)
        Me.cmbStatus.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbStatus.Name = "cmbStatus"
        Me.cmbStatus.Size = New System.Drawing.Size(373, 25)
        Me.cmbStatus.TabIndex = 4
        Me.cmbStatus.Text = "Active"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(9, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 19)
        Me.Label4.TabIndex = 81
        Me.Label4.Text = "Employee Type"
        '
        'cmbEmployeeType
        '
        Me.cmbEmployeeType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbEmployeeType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbEmployeeType.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEmployeeType.FormattingEnabled = True
        Me.cmbEmployeeType.Items.AddRange(New Object() {"BAKERY", "BAR", "GENERAL", "GRILL", "HOSTESS", "MANAGER", "PIZZA", "PREP", "RETAIL", "RUNNER", "SALAD", "SCULLERY", "TRAINEE", "WAITER"})
        Me.cmbEmployeeType.Location = New System.Drawing.Point(189, 5)
        Me.cmbEmployeeType.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbEmployeeType.Name = "cmbEmployeeType"
        Me.cmbEmployeeType.Size = New System.Drawing.Size(372, 25)
        Me.cmbEmployeeType.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Navy
        Me.Label3.Location = New System.Drawing.Point(11, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 19)
        Me.Label3.TabIndex = 80
        Me.Label3.Text = "Surname"
        '
        'txtSurname
        '
        Me.txtSurname.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Location = New System.Drawing.Point(188, 68)
        Me.txtSurname.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSurname.MaxLength = 50
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.Size = New System.Drawing.Size(373, 23)
        Me.txtSurname.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(9, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 19)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(11, 103)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 19)
        Me.Label1.TabIndex = 78
        Me.Label1.Text = "ID Number"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(189, 38)
        Me.txtName.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtName.MaxLength = 50
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(372, 23)
        Me.txtName.TabIndex = 1
        '
        'tbPAYEDetails
        '
        Me.tbPAYEDetails.Controls.Add(Me.txtPayrollNumber)
        Me.tbPAYEDetails.Controls.Add(Me.Label20)
        Me.tbPAYEDetails.Controls.Add(Me.btnClearPaye)
        Me.tbPAYEDetails.Controls.Add(Me.txtWaiterNumber)
        Me.tbPAYEDetails.Controls.Add(Me.txtLoanAmount)
        Me.tbPAYEDetails.Controls.Add(Me.Label14)
        Me.tbPAYEDetails.Controls.Add(Me.txtWage)
        Me.tbPAYEDetails.Controls.Add(Me.Label9)
        Me.tbPAYEDetails.Controls.Add(Me.Label8)
        Me.tbPAYEDetails.Controls.Add(Me.txtCommission)
        Me.tbPAYEDetails.Controls.Add(Me.Label7)
        Me.tbPAYEDetails.Controls.Add(Me.cmbPaymentType)
        Me.tbPAYEDetails.Controls.Add(Me.Label5)
        Me.tbPAYEDetails.Location = New System.Drawing.Point(4, 25)
        Me.tbPAYEDetails.Margin = New System.Windows.Forms.Padding(4)
        Me.tbPAYEDetails.Name = "tbPAYEDetails"
        Me.tbPAYEDetails.Padding = New System.Windows.Forms.Padding(4)
        Me.tbPAYEDetails.Size = New System.Drawing.Size(584, 545)
        Me.tbPAYEDetails.TabIndex = 1
        Me.tbPAYEDetails.Text = "PAYE Details"
        Me.tbPAYEDetails.UseVisualStyleBackColor = True
        '
        'txtPayrollNumber
        '
        Me.txtPayrollNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtPayrollNumber.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayrollNumber.Location = New System.Drawing.Point(189, 32)
        Me.txtPayrollNumber.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtPayrollNumber.MaxLength = 20
        Me.txtPayrollNumber.Name = "txtPayrollNumber"
        Me.txtPayrollNumber.Size = New System.Drawing.Size(373, 23)
        Me.txtPayrollNumber.TabIndex = 102
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Navy
        Me.Label20.Location = New System.Drawing.Point(11, 32)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(113, 19)
        Me.Label20.TabIndex = 103
        Me.Label20.Text = "Payroll Number"
        '
        'btnClearPaye
        '
        Me.btnClearPaye.Location = New System.Drawing.Point(197, 213)
        Me.btnClearPaye.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnClearPaye.Name = "btnClearPaye"
        Me.btnClearPaye.Size = New System.Drawing.Size(161, 30)
        Me.btnClearPaye.TabIndex = 5
        Me.btnClearPaye.Text = "&Clear Window"
        Me.btnClearPaye.UseVisualStyleBackColor = True
        '
        'txtWaiterNumber
        '
        Me.txtWaiterNumber.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWaiterNumber.Location = New System.Drawing.Point(189, 62)
        Me.txtWaiterNumber.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtWaiterNumber.MaxLength = 20
        Me.txtWaiterNumber.Name = "txtWaiterNumber"
        Me.txtWaiterNumber.Size = New System.Drawing.Size(373, 23)
        Me.txtWaiterNumber.TabIndex = 0
        '
        'txtLoanAmount
        '
        Me.txtLoanAmount.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmount.Location = New System.Drawing.Point(189, 183)
        Me.txtLoanAmount.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtLoanAmount.MaxLength = 20
        Me.txtLoanAmount.Name = "txtLoanAmount"
        Me.txtLoanAmount.Size = New System.Drawing.Size(373, 23)
        Me.txtLoanAmount.TabIndex = 4
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Navy
        Me.Label14.Location = New System.Drawing.Point(11, 187)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(101, 19)
        Me.Label14.TabIndex = 101
        Me.Label14.Text = "Loan Amount"
        '
        'txtWage
        '
        Me.txtWage.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWage.Location = New System.Drawing.Point(189, 154)
        Me.txtWage.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtWage.MaxLength = 20
        Me.txtWage.Name = "txtWage"
        Me.txtWage.Size = New System.Drawing.Size(373, 23)
        Me.txtWage.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(11, 158)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 19)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Wage"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(9, 124)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(89, 19)
        Me.Label8.TabIndex = 99
        Me.Label8.Text = "Commission"
        '
        'txtCommission
        '
        Me.txtCommission.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCommission.Location = New System.Drawing.Point(189, 124)
        Me.txtCommission.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtCommission.MaxLength = 20
        Me.txtCommission.Name = "txtCommission"
        Me.txtCommission.Size = New System.Drawing.Size(373, 23)
        Me.txtCommission.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Navy
        Me.Label7.Location = New System.Drawing.Point(9, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 19)
        Me.Label7.TabIndex = 98
        Me.Label7.Text = "Payment Type"
        '
        'cmbPaymentType
        '
        Me.cmbPaymentType.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPaymentType.FormattingEnabled = True
        Me.cmbPaymentType.Items.AddRange(New Object() {"Commission", "Wage", "Salary"})
        Me.cmbPaymentType.Location = New System.Drawing.Point(189, 91)
        Me.cmbPaymentType.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbPaymentType.Name = "cmbPaymentType"
        Me.cmbPaymentType.Size = New System.Drawing.Size(373, 25)
        Me.cmbPaymentType.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Navy
        Me.Label5.Location = New System.Drawing.Point(11, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 19)
        Me.Label5.TabIndex = 97
        Me.Label5.Text = "Waiter Number"
        '
        'tbBankingDetails
        '
        Me.tbBankingDetails.Controls.Add(Me.btnClearBanking)
        Me.tbBankingDetails.Controls.Add(Me.txtBranchNumber)
        Me.tbBankingDetails.Controls.Add(Me.Label16)
        Me.tbBankingDetails.Controls.Add(Me.txtAccNumber)
        Me.tbBankingDetails.Controls.Add(Me.Label17)
        Me.tbBankingDetails.Controls.Add(Me.txtBankName)
        Me.tbBankingDetails.Controls.Add(Me.Label18)
        Me.tbBankingDetails.Controls.Add(Me.txtAccountHolder)
        Me.tbBankingDetails.Controls.Add(Me.Label19)
        Me.tbBankingDetails.Location = New System.Drawing.Point(4, 25)
        Me.tbBankingDetails.Margin = New System.Windows.Forms.Padding(4)
        Me.tbBankingDetails.Name = "tbBankingDetails"
        Me.tbBankingDetails.Padding = New System.Windows.Forms.Padding(4)
        Me.tbBankingDetails.Size = New System.Drawing.Size(584, 545)
        Me.tbBankingDetails.TabIndex = 2
        Me.tbBankingDetails.Text = "Banking Details"
        Me.tbBankingDetails.UseVisualStyleBackColor = True
        '
        'btnClearBanking
        '
        Me.btnClearBanking.Location = New System.Drawing.Point(175, 145)
        Me.btnClearBanking.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnClearBanking.Name = "btnClearBanking"
        Me.btnClearBanking.Size = New System.Drawing.Size(161, 30)
        Me.btnClearBanking.TabIndex = 4
        Me.btnClearBanking.Text = "&Clear Window"
        Me.btnClearBanking.UseVisualStyleBackColor = True
        '
        'txtBranchNumber
        '
        Me.txtBranchNumber.Location = New System.Drawing.Point(175, 114)
        Me.txtBranchNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBranchNumber.MaxLength = 50
        Me.txtBranchNumber.Name = "txtBranchNumber"
        Me.txtBranchNumber.Size = New System.Drawing.Size(380, 22)
        Me.txtBranchNumber.TabIndex = 3
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Navy
        Me.Label16.Location = New System.Drawing.Point(29, 118)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(125, 19)
        Me.Label16.TabIndex = 21
        Me.Label16.Text = "Branch Number :"
        '
        'txtAccNumber
        '
        Me.txtAccNumber.Location = New System.Drawing.Point(175, 82)
        Me.txtAccNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAccNumber.MaxLength = 50
        Me.txtAccNumber.Name = "txtAccNumber"
        Me.txtAccNumber.Size = New System.Drawing.Size(380, 22)
        Me.txtAccNumber.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Navy
        Me.Label17.Location = New System.Drawing.Point(29, 86)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(135, 19)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "Account Number :"
        '
        'txtBankName
        '
        Me.txtBankName.Location = New System.Drawing.Point(175, 50)
        Me.txtBankName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBankName.MaxLength = 50
        Me.txtBankName.Name = "txtBankName"
        Me.txtBankName.Size = New System.Drawing.Size(380, 22)
        Me.txtBankName.TabIndex = 1
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Navy
        Me.Label18.Location = New System.Drawing.Point(29, 54)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(97, 19)
        Me.Label18.TabIndex = 16
        Me.Label18.Text = "Bank Name :"
        '
        'txtAccountHolder
        '
        Me.txtAccountHolder.Location = New System.Drawing.Point(175, 18)
        Me.txtAccountHolder.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAccountHolder.MaxLength = 50
        Me.txtAccountHolder.Name = "txtAccountHolder"
        Me.txtAccountHolder.Size = New System.Drawing.Size(380, 22)
        Me.txtAccountHolder.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Navy
        Me.Label19.Location = New System.Drawing.Point(29, 22)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(125, 19)
        Me.Label19.TabIndex = 13
        Me.Label19.Text = "Account Holder :"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(303, 387)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(109, 30)
        Me.btnExit.TabIndex = 77
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(185, 387)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(112, 30)
        Me.btnAdd.TabIndex = 75
        Me.btnAdd.Text = "&Save"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'frmEmployee_New
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(589, 598)
        Me.Controls.Add(Me.EmployeeTab)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximumSize = New System.Drawing.Size(607, 645)
        Me.MinimumSize = New System.Drawing.Size(607, 645)
        Me.Name = "frmEmployee_New"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "S_New&Employee"
        Me.Text = "New Employee"
        Me.EmployeeTab.ResumeLayout(False)
        Me.tbBasicDetails.ResumeLayout(False)
        Me.tbBasicDetails.PerformLayout()
        Me.tbPAYEDetails.ResumeLayout(False)
        Me.tbPAYEDetails.PerformLayout()
        Me.tbBankingDetails.ResumeLayout(False)
        Me.tbBankingDetails.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EmployeeTab As System.Windows.Forms.TabControl
    Friend WithEvents tbBasicDetails As System.Windows.Forms.TabPage
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnClearBasic As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dtpContractEnds As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDateSarted As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtContact2 As System.Windows.Forms.TextBox
    Friend WithEvents txtContactNumber1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbEmployeeType As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSurname As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents tbPAYEDetails As System.Windows.Forms.TabPage
    Friend WithEvents tbBankingDetails As System.Windows.Forms.TabPage
    Friend WithEvents txtBranchNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtAccNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtBankName As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtAccountHolder As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtWaiterNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtWage As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCommission As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbPaymentType As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnClearPaye As System.Windows.Forms.Button
    Friend WithEvents btnClearBanking As System.Windows.Forms.Button
    Friend WithEvents txtIDNumber As System.Windows.Forms.TextBox
    Friend WithEvents ckbIsScheduler As System.Windows.Forms.CheckBox
    Friend WithEvents txtPayrollNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
End Class
