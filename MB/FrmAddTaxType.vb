﻿Imports System.Data.SqlClient
Imports System.Data

Public Class FrmAddTaxType

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim cn1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            Dim sSQL As String

            Dim oCompanyID As Integer = Nothing
            If My.Settings.Setting_CompanyID <> "" Or My.Settings.Setting_CompanyID <> Nothing Then
                oCompanyID = CInt(My.Settings.Setting_CompanyID)
            End If

            sSQL = "Insert Into [TaxTypes] ([TaxDesc], [TaxPerc]"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & ", [Co_ID]"
            End If
            sSQL = sSQL & " ) "
            sSQL = sSQL & " Values ("
            '##### NEW #####
            sSQL = sSQL & "'" & SQLConvert(TB_TaxDesc.Text) & "', "
            sSQL = sSQL & "'" & SQLConvert(TB_TaxPerc.Text) & "'"
            If oCompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & ", " & oCompanyID
            End If
            sSQL = sSQL & ") "


            Dim cmd As New SqlCommand(sSQL, cn1)
            cmd.CommandTimeout = 300
            Using cn1
                cn1.Open()
                cmd.ExecuteNonQuery()
                cn1.Close()
            End Using

            MsgBox("TaxType successfully created!")


        Catch ex As Exception
            If cn1.State = ConnectionState.Open Then
                cn1.Dispose()
            End If
            MsgBox("There was an error adding Tax Type! " & Err.Description)
            'Me.Edit_ID_Label.Text = "None"
        End Try

    End Sub
End Class