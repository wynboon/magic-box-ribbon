﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMigrateAccountTransactions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMigrateAccountTransactions))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbCurrentAccount = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdbDesc = New System.Windows.Forms.RadioButton()
        Me.rdbAccount = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnLoadTransactions = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmbDestinationAccount = New System.Windows.Forms.ComboBox()
        Me.btnMoveTransactions = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.ckbSelectAll = New System.Windows.Forms.CheckBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvTransactions = New System.Windows.Forms.DataGridView()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BatchIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CaptureDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriodDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinYearDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReferenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAccDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxAmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PostedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccountingDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VoidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DRCRDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContraAccountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescriptionCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InfoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvTransactions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpTo)
        Me.GroupBox1.Controls.Add(Me.dtpFrom)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmbCurrentAccount)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(8, 23)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(491, 100)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Current Account"
        '
        'dtpTo
        '
        Me.dtpTo.CustomFormat = "dd MMM yyyy"
        Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTo.Location = New System.Drawing.Point(289, 57)
        Me.dtpTo.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(161, 22)
        Me.dtpTo.TabIndex = 3
        '
        'dtpFrom
        '
        Me.dtpFrom.CustomFormat = "dd MMM yyyy"
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFrom.Location = New System.Drawing.Point(107, 58)
        Me.dtpFrom.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(173, 22)
        Me.dtpFrom.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label3.Location = New System.Drawing.Point(22, 65)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Date filter :"
        '
        'cmbCurrentAccount
        '
        Me.cmbCurrentAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbCurrentAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCurrentAccount.FormattingEnabled = True
        Me.cmbCurrentAccount.Location = New System.Drawing.Point(107, 25)
        Me.cmbCurrentAccount.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbCurrentAccount.Name = "cmbCurrentAccount"
        Me.cmbCurrentAccount.Size = New System.Drawing.Size(344, 24)
        Me.cmbCurrentAccount.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label1.Location = New System.Drawing.Point(11, 28)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Description :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdbDesc)
        Me.GroupBox2.Controls.Add(Me.rdbAccount)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(507, 23)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(327, 63)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Accounts sort order"
        '
        'rdbDesc
        '
        Me.rdbDesc.AutoSize = True
        Me.rdbDesc.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbDesc.Location = New System.Drawing.Point(163, 31)
        Me.rdbDesc.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbDesc.Name = "rdbDesc"
        Me.rdbDesc.Size = New System.Drawing.Size(100, 21)
        Me.rdbDesc.TabIndex = 1
        Me.rdbDesc.Text = "Description"
        Me.rdbDesc.UseVisualStyleBackColor = True
        '
        'rdbAccount
        '
        Me.rdbAccount.AutoSize = True
        Me.rdbAccount.Checked = True
        Me.rdbAccount.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbAccount.Location = New System.Drawing.Point(24, 31)
        Me.rdbAccount.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbAccount.Name = "rdbAccount"
        Me.rdbAccount.Size = New System.Drawing.Size(80, 21)
        Me.rdbAccount.TabIndex = 0
        Me.rdbAccount.TabStop = True
        Me.rdbAccount.Text = "Account"
        Me.rdbAccount.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.btnLoadTransactions)
        Me.GroupBox3.Controls.Add(Me.GroupBox4)
        Me.GroupBox3.Controls.Add(Me.GroupBox2)
        Me.GroupBox3.Controls.Add(Me.GroupBox1)
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Location = New System.Drawing.Point(5, -1)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(1368, 130)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Accounts"
        '
        'btnLoadTransactions
        '
        Me.btnLoadTransactions.Location = New System.Drawing.Point(507, 89)
        Me.btnLoadTransactions.Margin = New System.Windows.Forms.Padding(4)
        Me.btnLoadTransactions.Name = "btnLoadTransactions"
        Me.btnLoadTransactions.Size = New System.Drawing.Size(327, 30)
        Me.btnLoadTransactions.TabIndex = 4
        Me.btnLoadTransactions.Text = "&Load Account transactions"
        Me.btnLoadTransactions.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmbDestinationAccount)
        Me.GroupBox4.Controls.Add(Me.btnMoveTransactions)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox4.Location = New System.Drawing.Point(841, 23)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Size = New System.Drawing.Size(513, 95)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Destination Account"
        '
        'cmbDestinationAccount
        '
        Me.cmbDestinationAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbDestinationAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbDestinationAccount.FormattingEnabled = True
        Me.cmbDestinationAccount.Location = New System.Drawing.Point(107, 25)
        Me.cmbDestinationAccount.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbDestinationAccount.Name = "cmbDestinationAccount"
        Me.cmbDestinationAccount.Size = New System.Drawing.Size(315, 24)
        Me.cmbDestinationAccount.TabIndex = 7
        '
        'btnMoveTransactions
        '
        Me.btnMoveTransactions.Location = New System.Drawing.Point(107, 60)
        Me.btnMoveTransactions.Margin = New System.Windows.Forms.Padding(4)
        Me.btnMoveTransactions.Name = "btnMoveTransactions"
        Me.btnMoveTransactions.Size = New System.Drawing.Size(316, 30)
        Me.btnMoveTransactions.TabIndex = 6
        Me.btnMoveTransactions.Tag = "S_Move_Transaction"
        Me.btnMoveTransactions.Text = "&Move transactions to Account"
        Me.btnMoveTransactions.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label2.Location = New System.Drawing.Point(11, 30)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Description :"
        '
        'GroupBox5
        '
        Me.GroupBox5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox5.Controls.Add(Me.ckbSelectAll)
        Me.GroupBox5.Controls.Add(Me.Panel1)
        Me.GroupBox5.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox5.Location = New System.Drawing.Point(5, 137)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Size = New System.Drawing.Size(1368, 539)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Transactions"
        '
        'ckbSelectAll
        '
        Me.ckbSelectAll.AutoSize = True
        Me.ckbSelectAll.Location = New System.Drawing.Point(11, 23)
        Me.ckbSelectAll.Margin = New System.Windows.Forms.Padding(4)
        Me.ckbSelectAll.Name = "ckbSelectAll"
        Me.ckbSelectAll.Size = New System.Drawing.Size(88, 21)
        Me.ckbSelectAll.TabIndex = 1
        Me.ckbSelectAll.Text = "Select All"
        Me.ckbSelectAll.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.dgvTransactions)
        Me.Panel1.Location = New System.Drawing.Point(8, 52)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1352, 480)
        Me.Panel1.TabIndex = 0
        '
        'dgvTransactions
        '
        Me.dgvTransactions.AllowUserToAddRows = False
        Me.dgvTransactions.AllowUserToDeleteRows = False
        Me.dgvTransactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTransactions.AutoGenerateColumns = False
        Me.dgvTransactions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTransactions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.BatchIDDataGridViewTextBoxColumn, Me.TransactionIDDataGridViewTextBoxColumn, Me.LinkIDDataGridViewTextBoxColumn, Me.CaptureDateDataGridViewTextBoxColumn, Me.TransactionDateDataGridViewTextBoxColumn, Me.AmountDataGridViewTextBoxColumn, Me.SupplierNameDataGridViewTextBoxColumn, Me.PPeriodDataGridViewTextBoxColumn, Me.FinYearDataGridViewTextBoxColumn, Me.GDCDataGridViewTextBoxColumn, Me.ReferenceDataGridViewTextBoxColumn, Me.DescriptionDataGridViewTextBoxColumn, Me.AccNumberDataGridViewTextBoxColumn, Me.LinkAccDataGridViewTextBoxColumn, Me.TaxTypeDataGridViewTextBoxColumn, Me.TaxAmountDataGridViewTextBoxColumn, Me.UserIDDataGridViewTextBoxColumn, Me.SupplierIDDataGridViewTextBoxColumn, Me.EmployeeIDDataGridViewTextBoxColumn, Me.Description2DataGridViewTextBoxColumn, Me.Description3DataGridViewTextBoxColumn, Me.Description4DataGridViewTextBoxColumn, Me.Description5DataGridViewTextBoxColumn, Me.PostedDataGridViewTextBoxColumn, Me.TransactionTypeDataGridViewTextBoxColumn, Me.AccountingDataGridViewTextBoxColumn, Me.VoidDataGridViewTextBoxColumn, Me.DRCRDataGridViewTextBoxColumn, Me.ContraAccountDataGridViewTextBoxColumn, Me.DescriptionCodeDataGridViewTextBoxColumn, Me.DocTypeDataGridViewTextBoxColumn, Me.CoIDDataGridViewTextBoxColumn, Me.InfoDataGridViewTextBoxColumn, Me.Info2DataGridViewTextBoxColumn, Me.Info3DataGridViewTextBoxColumn})
        Me.dgvTransactions.DataSource = Me.TransactionBindingSource
        Me.dgvTransactions.Location = New System.Drawing.Point(0, 0)
        Me.dgvTransactions.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvTransactions.Name = "dgvTransactions"
        Me.dgvTransactions.Size = New System.Drawing.Size(1352, 480)
        Me.dgvTransactions.TabIndex = 1
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(1233, 677)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(132, 28)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.Visible = False
        '
        'BatchIDDataGridViewTextBoxColumn
        '
        Me.BatchIDDataGridViewTextBoxColumn.DataPropertyName = "BatchID"
        Me.BatchIDDataGridViewTextBoxColumn.HeaderText = "BatchID"
        Me.BatchIDDataGridViewTextBoxColumn.Name = "BatchIDDataGridViewTextBoxColumn"
        Me.BatchIDDataGridViewTextBoxColumn.Visible = False
        '
        'TransactionIDDataGridViewTextBoxColumn
        '
        Me.TransactionIDDataGridViewTextBoxColumn.DataPropertyName = "TransactionID"
        Me.TransactionIDDataGridViewTextBoxColumn.HeaderText = "Transaction ID"
        Me.TransactionIDDataGridViewTextBoxColumn.Name = "TransactionIDDataGridViewTextBoxColumn"
        Me.TransactionIDDataGridViewTextBoxColumn.Visible = False
        '
        'LinkIDDataGridViewTextBoxColumn
        '
        Me.LinkIDDataGridViewTextBoxColumn.DataPropertyName = "LinkID"
        Me.LinkIDDataGridViewTextBoxColumn.HeaderText = "LinkID"
        Me.LinkIDDataGridViewTextBoxColumn.Name = "LinkIDDataGridViewTextBoxColumn"
        Me.LinkIDDataGridViewTextBoxColumn.Visible = False
        '
        'CaptureDateDataGridViewTextBoxColumn
        '
        Me.CaptureDateDataGridViewTextBoxColumn.DataPropertyName = "Capture_Date"
        Me.CaptureDateDataGridViewTextBoxColumn.HeaderText = "Date captured"
        Me.CaptureDateDataGridViewTextBoxColumn.Name = "CaptureDateDataGridViewTextBoxColumn"
        '
        'TransactionDateDataGridViewTextBoxColumn
        '
        Me.TransactionDateDataGridViewTextBoxColumn.DataPropertyName = "Transaction_Date"
        Me.TransactionDateDataGridViewTextBoxColumn.HeaderText = "Trans date"
        Me.TransactionDateDataGridViewTextBoxColumn.Name = "TransactionDateDataGridViewTextBoxColumn"
        '
        'AmountDataGridViewTextBoxColumn
        '
        Me.AmountDataGridViewTextBoxColumn.DataPropertyName = "Amount"
        Me.AmountDataGridViewTextBoxColumn.HeaderText = "Amount"
        Me.AmountDataGridViewTextBoxColumn.Name = "AmountDataGridViewTextBoxColumn"
        '
        'SupplierNameDataGridViewTextBoxColumn
        '
        Me.SupplierNameDataGridViewTextBoxColumn.DataPropertyName = "SupplierName"
        Me.SupplierNameDataGridViewTextBoxColumn.HeaderText = "Supplier"
        Me.SupplierNameDataGridViewTextBoxColumn.Name = "SupplierNameDataGridViewTextBoxColumn"
        '
        'PPeriodDataGridViewTextBoxColumn
        '
        Me.PPeriodDataGridViewTextBoxColumn.DataPropertyName = "PPeriod"
        Me.PPeriodDataGridViewTextBoxColumn.HeaderText = "Period"
        Me.PPeriodDataGridViewTextBoxColumn.Name = "PPeriodDataGridViewTextBoxColumn"
        Me.PPeriodDataGridViewTextBoxColumn.Visible = False
        '
        'FinYearDataGridViewTextBoxColumn
        '
        Me.FinYearDataGridViewTextBoxColumn.DataPropertyName = "Fin_Year"
        Me.FinYearDataGridViewTextBoxColumn.HeaderText = "Fin. Year"
        Me.FinYearDataGridViewTextBoxColumn.Name = "FinYearDataGridViewTextBoxColumn"
        Me.FinYearDataGridViewTextBoxColumn.Visible = False
        '
        'GDCDataGridViewTextBoxColumn
        '
        Me.GDCDataGridViewTextBoxColumn.DataPropertyName = "GDC"
        Me.GDCDataGridViewTextBoxColumn.HeaderText = "GDC"
        Me.GDCDataGridViewTextBoxColumn.Name = "GDCDataGridViewTextBoxColumn"
        Me.GDCDataGridViewTextBoxColumn.Visible = False
        '
        'ReferenceDataGridViewTextBoxColumn
        '
        Me.ReferenceDataGridViewTextBoxColumn.DataPropertyName = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.HeaderText = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.Name = "ReferenceDataGridViewTextBoxColumn"
        '
        'DescriptionDataGridViewTextBoxColumn
        '
        Me.DescriptionDataGridViewTextBoxColumn.DataPropertyName = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.HeaderText = "Description"
        Me.DescriptionDataGridViewTextBoxColumn.Name = "DescriptionDataGridViewTextBoxColumn"
        '
        'AccNumberDataGridViewTextBoxColumn
        '
        Me.AccNumberDataGridViewTextBoxColumn.DataPropertyName = "AccNumber"
        Me.AccNumberDataGridViewTextBoxColumn.HeaderText = "AccNumber"
        Me.AccNumberDataGridViewTextBoxColumn.Name = "AccNumberDataGridViewTextBoxColumn"
        Me.AccNumberDataGridViewTextBoxColumn.Visible = False
        '
        'LinkAccDataGridViewTextBoxColumn
        '
        Me.LinkAccDataGridViewTextBoxColumn.DataPropertyName = "LinkAcc"
        Me.LinkAccDataGridViewTextBoxColumn.HeaderText = "LinkAcc"
        Me.LinkAccDataGridViewTextBoxColumn.Name = "LinkAccDataGridViewTextBoxColumn"
        Me.LinkAccDataGridViewTextBoxColumn.Visible = False
        '
        'TaxTypeDataGridViewTextBoxColumn
        '
        Me.TaxTypeDataGridViewTextBoxColumn.DataPropertyName = "TaxType"
        Me.TaxTypeDataGridViewTextBoxColumn.HeaderText = "TaxType"
        Me.TaxTypeDataGridViewTextBoxColumn.Name = "TaxTypeDataGridViewTextBoxColumn"
        Me.TaxTypeDataGridViewTextBoxColumn.Visible = False
        '
        'TaxAmountDataGridViewTextBoxColumn
        '
        Me.TaxAmountDataGridViewTextBoxColumn.DataPropertyName = "Tax_Amount"
        Me.TaxAmountDataGridViewTextBoxColumn.HeaderText = "Tax_Amount"
        Me.TaxAmountDataGridViewTextBoxColumn.Name = "TaxAmountDataGridViewTextBoxColumn"
        Me.TaxAmountDataGridViewTextBoxColumn.Visible = False
        '
        'UserIDDataGridViewTextBoxColumn
        '
        Me.UserIDDataGridViewTextBoxColumn.DataPropertyName = "UserID"
        Me.UserIDDataGridViewTextBoxColumn.HeaderText = "UserID"
        Me.UserIDDataGridViewTextBoxColumn.Name = "UserIDDataGridViewTextBoxColumn"
        Me.UserIDDataGridViewTextBoxColumn.Visible = False
        '
        'SupplierIDDataGridViewTextBoxColumn
        '
        Me.SupplierIDDataGridViewTextBoxColumn.DataPropertyName = "SupplierID"
        Me.SupplierIDDataGridViewTextBoxColumn.HeaderText = "SupplierID"
        Me.SupplierIDDataGridViewTextBoxColumn.Name = "SupplierIDDataGridViewTextBoxColumn"
        Me.SupplierIDDataGridViewTextBoxColumn.Visible = False
        '
        'EmployeeIDDataGridViewTextBoxColumn
        '
        Me.EmployeeIDDataGridViewTextBoxColumn.DataPropertyName = "EmployeeID"
        Me.EmployeeIDDataGridViewTextBoxColumn.HeaderText = "EmployeeID"
        Me.EmployeeIDDataGridViewTextBoxColumn.Name = "EmployeeIDDataGridViewTextBoxColumn"
        Me.EmployeeIDDataGridViewTextBoxColumn.Visible = False
        '
        'Description2DataGridViewTextBoxColumn
        '
        Me.Description2DataGridViewTextBoxColumn.DataPropertyName = "Description_2"
        Me.Description2DataGridViewTextBoxColumn.HeaderText = "Description_2"
        Me.Description2DataGridViewTextBoxColumn.Name = "Description2DataGridViewTextBoxColumn"
        Me.Description2DataGridViewTextBoxColumn.Visible = False
        '
        'Description3DataGridViewTextBoxColumn
        '
        Me.Description3DataGridViewTextBoxColumn.DataPropertyName = "Description_3"
        Me.Description3DataGridViewTextBoxColumn.HeaderText = "Description_3"
        Me.Description3DataGridViewTextBoxColumn.Name = "Description3DataGridViewTextBoxColumn"
        Me.Description3DataGridViewTextBoxColumn.Visible = False
        '
        'Description4DataGridViewTextBoxColumn
        '
        Me.Description4DataGridViewTextBoxColumn.DataPropertyName = "Description_4"
        Me.Description4DataGridViewTextBoxColumn.HeaderText = "Alt Desc"
        Me.Description4DataGridViewTextBoxColumn.Name = "Description4DataGridViewTextBoxColumn"
        '
        'Description5DataGridViewTextBoxColumn
        '
        Me.Description5DataGridViewTextBoxColumn.DataPropertyName = "Description_5"
        Me.Description5DataGridViewTextBoxColumn.HeaderText = "Description_5"
        Me.Description5DataGridViewTextBoxColumn.Name = "Description5DataGridViewTextBoxColumn"
        Me.Description5DataGridViewTextBoxColumn.Visible = False
        '
        'PostedDataGridViewTextBoxColumn
        '
        Me.PostedDataGridViewTextBoxColumn.DataPropertyName = "Posted"
        Me.PostedDataGridViewTextBoxColumn.HeaderText = "Posted"
        Me.PostedDataGridViewTextBoxColumn.Name = "PostedDataGridViewTextBoxColumn"
        Me.PostedDataGridViewTextBoxColumn.Visible = False
        '
        'TransactionTypeDataGridViewTextBoxColumn
        '
        Me.TransactionTypeDataGridViewTextBoxColumn.DataPropertyName = "Transaction_Type"
        Me.TransactionTypeDataGridViewTextBoxColumn.HeaderText = "Transaction Type"
        Me.TransactionTypeDataGridViewTextBoxColumn.Name = "TransactionTypeDataGridViewTextBoxColumn"
        '
        'AccountingDataGridViewTextBoxColumn
        '
        Me.AccountingDataGridViewTextBoxColumn.DataPropertyName = "Accounting"
        Me.AccountingDataGridViewTextBoxColumn.HeaderText = "Accounting"
        Me.AccountingDataGridViewTextBoxColumn.Name = "AccountingDataGridViewTextBoxColumn"
        Me.AccountingDataGridViewTextBoxColumn.Visible = False
        '
        'VoidDataGridViewTextBoxColumn
        '
        Me.VoidDataGridViewTextBoxColumn.DataPropertyName = "Void"
        Me.VoidDataGridViewTextBoxColumn.HeaderText = "Void"
        Me.VoidDataGridViewTextBoxColumn.Name = "VoidDataGridViewTextBoxColumn"
        Me.VoidDataGridViewTextBoxColumn.Visible = False
        '
        'DRCRDataGridViewTextBoxColumn
        '
        Me.DRCRDataGridViewTextBoxColumn.DataPropertyName = "DR_CR"
        Me.DRCRDataGridViewTextBoxColumn.HeaderText = "Tran type"
        Me.DRCRDataGridViewTextBoxColumn.Name = "DRCRDataGridViewTextBoxColumn"
        '
        'ContraAccountDataGridViewTextBoxColumn
        '
        Me.ContraAccountDataGridViewTextBoxColumn.DataPropertyName = "Contra_Account"
        Me.ContraAccountDataGridViewTextBoxColumn.HeaderText = "Contra_Account"
        Me.ContraAccountDataGridViewTextBoxColumn.Name = "ContraAccountDataGridViewTextBoxColumn"
        Me.ContraAccountDataGridViewTextBoxColumn.Visible = False
        '
        'DescriptionCodeDataGridViewTextBoxColumn
        '
        Me.DescriptionCodeDataGridViewTextBoxColumn.DataPropertyName = "Description_Code"
        Me.DescriptionCodeDataGridViewTextBoxColumn.HeaderText = "Account"
        Me.DescriptionCodeDataGridViewTextBoxColumn.Name = "DescriptionCodeDataGridViewTextBoxColumn"
        '
        'DocTypeDataGridViewTextBoxColumn
        '
        Me.DocTypeDataGridViewTextBoxColumn.DataPropertyName = "DocType"
        Me.DocTypeDataGridViewTextBoxColumn.HeaderText = "Type"
        Me.DocTypeDataGridViewTextBoxColumn.Name = "DocTypeDataGridViewTextBoxColumn"
        '
        'CoIDDataGridViewTextBoxColumn
        '
        Me.CoIDDataGridViewTextBoxColumn.DataPropertyName = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.HeaderText = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.Name = "CoIDDataGridViewTextBoxColumn"
        Me.CoIDDataGridViewTextBoxColumn.Visible = False
        '
        'InfoDataGridViewTextBoxColumn
        '
        Me.InfoDataGridViewTextBoxColumn.DataPropertyName = "Info"
        Me.InfoDataGridViewTextBoxColumn.HeaderText = "Info"
        Me.InfoDataGridViewTextBoxColumn.Name = "InfoDataGridViewTextBoxColumn"
        Me.InfoDataGridViewTextBoxColumn.Visible = False
        '
        'Info2DataGridViewTextBoxColumn
        '
        Me.Info2DataGridViewTextBoxColumn.DataPropertyName = "Info2"
        Me.Info2DataGridViewTextBoxColumn.HeaderText = "Info2"
        Me.Info2DataGridViewTextBoxColumn.Name = "Info2DataGridViewTextBoxColumn"
        Me.Info2DataGridViewTextBoxColumn.Visible = False
        '
        'Info3DataGridViewTextBoxColumn
        '
        Me.Info3DataGridViewTextBoxColumn.DataPropertyName = "Info3"
        Me.Info3DataGridViewTextBoxColumn.HeaderText = "Info3"
        Me.Info3DataGridViewTextBoxColumn.Name = "Info3DataGridViewTextBoxColumn"
        Me.Info3DataGridViewTextBoxColumn.Visible = False
        '
        'TransactionBindingSource
        '
        Me.TransactionBindingSource.DataSource = GetType(MB.Transaction)
        '
        'frmMigrateAccountTransactions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1386, 708)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmMigrateAccountTransactions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "S_Migrate_Transactions"
        Me.Text = "Migrate Account Transactions"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvTransactions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbCurrentAccount As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDesc As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAccount As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMoveTransactions As System.Windows.Forms.Button
    Friend WithEvents btnLoadTransactions As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents TransactionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbDestinationAccount As System.Windows.Forms.ComboBox
    Friend WithEvents ckbSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvTransactions As System.Windows.Forms.DataGridView
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BatchIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CaptureDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriodDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FinYearDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReferenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAccDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxAmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PostedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccountingDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VoidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DRCRDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContraAccountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescriptionCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InfoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
