﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmCashSpotCheck

    Private Sub frmCashSpotCheck_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Cash Spot Check -> Version : " & My.Settings.Setting_Version
    End Sub

    Private Sub btnRangeTotal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRangeTotal.Click
        Try

            Me.Hide()

            Dim R As Microsoft.Office.Interop.Excel.Range
            Dim A As String
            R = Globals.ThisAddIn.Application.InputBox(Prompt:="Please select range", Type:=8)
            A = R.Address
            ' Me.txtYourCashCount.Text = A

            Me.Show() 'note that because this is shown last, it will be on top.

            Dim oColumn As Integer = Globals.ThisAddIn.Application.Range(A).Column
            Dim oRow As Long = Globals.ThisAddIn.Application.Range(A).Column
            Dim oRowCount As Long = Globals.ThisAddIn.Application.Range(A).Rows.Count
            Dim oNextRow As Long = oRow + oRowCount
            Globals.ThisAddIn.Application.Cells(oNextRow, oColumn).formula = "=SUM(" & A & ")"
            Me.txtYourCashCount.Text = Globals.ThisAddIn.Application.Cells(oNextRow, oColumn).value


        Catch
            MsgBox("An error has ocurred! " & Err.Description & ".")
            Me.Show()
        End Try
    End Sub



    Sub oResult()
        Try
            Dim oVariance As Decimal
            If IsNumeric(Me.txtYourCashCount.Text) = True And IsNumeric(Me.txtCashOnSystem.Text) = True Then

                If CDec(Me.txtYourCashCount.Text) = CDec(Me.txtCashOnSystem.Text) Then
                    oVariance = 0
                    Me.Panel2.BackgroundImage = My.Resources.CashSpotCheckTick
                    Me.Panel_Result.Visible = True
                    Me.txtVariance.Text = CStr(oVariance)
                    Me.Panel_Action.Visible = False
                Else
                    Me.Panel2.BackgroundImage = My.Resources.CashSpotCheckCross
                    oVariance = CDec(Me.txtYourCashCount.Text) - CDec(Me.txtCashOnSystem.Text)
                    Me.Panel_Result.Visible = True
                    Me.txtVariance.Text = CStr(oVariance)
                    Me.Panel_Action.Visible = True
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Function Get_Petty_Cash_Opening_Balance_Dr() As String
        Try

            Dim sSQL As String

            Dim oResult As String
            If My.Settings.DBType = "Access" Then

                sSQL = "Select SUM(Case When DR_CR ='Cr'then  Amount*-1 else amount end)) as Total From Transactions " & _
                    "Left Join Accounting On Accounting.[Segment 4] = Transactions.[Description Code] and Accounting.[Co_ID] = Transactions.Co_ID" & _
                    "Where [Display Name] = 'CASH' And Accounting = True and Void = FALSE"

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Petty_Cash_Opening_Balance_Dr = "0"
                Else
                    Get_Petty_Cash_Opening_Balance_Dr = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Select SUM(Case When DR_CR ='Cr'then  Amount*-1 else amount end)) as Total From Transactions " & _
                "Left Join Accounting On Accounting.[Segment 4] = Transactions.[Description Code] and Accounting.[Co_ID] = Transactions.Co_ID" & _
                "Where [Display Name] = 'CASH' And Accounting = True and Void = -1"

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Petty_Cash_Opening_Balance_Dr = "0"
                Else
                    Get_Petty_Cash_Opening_Balance_Dr = oResult
                End If
                connection.Close()

            End If

        Catch ex As Exception
            Get_Petty_Cash_Opening_Balance_Dr = "0"
        End Try
    End Function

    Function Get_Petty_Cash_Opening_Balance_Cr() As String
        Try

            Dim sSQL As String

            Dim oResult As String
            If My.Settings.DBType = "Access" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where [Description Code] = 'EAACAA' And DR_CR = 'Cr' And Accounting = True"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()

                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Petty_Cash_Opening_Balance_Cr = "0"
                Else
                    Get_Petty_Cash_Opening_Balance_Cr = oResult
                End If

                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where [Description Code] = 'EAACAA' And DR_CR = 'Cr' And Accounting = 1"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()

                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Petty_Cash_Opening_Balance_Cr = "0"
                Else
                    Get_Petty_Cash_Opening_Balance_Cr = oResult
                End If

                connection.Close()

            End If

        Catch ex As Exception
            Get_Petty_Cash_Opening_Balance_Cr = "0"
        End Try
    End Function


    Function Get_Total_Payments() As String
        Try

            Dim oFrom As String = Me.DateTimePicker_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTimePicker_To.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If

            Dim oResult As String
            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "Select SUM(Amount) as Total From Transactions"
                sSQL = sSQL & " Where [DocType] = 'Payment'"
                sSQL = sSQL & " And Info = 'CASH'"
                sSQL = sSQL & " And DR_CR = 'Dr'"
                sSQL = sSQL & " And Void = False"
                sSQL = sSQL & " And Accounting = True"
                sSQL = sSQL & sDateCriteria
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Payments = "0"
                Else
                    Get_Total_Payments = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "Select SUM(Amount) as Total From Transactions"
                sSQL = sSQL & " Where [DocType] = 'Payment'"
                sSQL = sSQL & " And Info = 'CASH'"
                sSQL = sSQL & " And DR_CR = 'Dr'"
                sSQL = sSQL & " And Void = 0"
                sSQL = sSQL & " And Accounting = 1"
                sSQL = sSQL & sDateCriteria
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()

                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Payments = "0"
                Else
                    Get_Total_Payments = oResult
                End If
                connection.Close()

            End If

        Catch ex As Exception
            Get_Total_Payments = "0"
        End Try
    End Function


    Function Get_Total_Crs_POS_CASH_CONTROL() As String
        Try
            Dim oResult As String
            Dim sSQL As String


            If My.Settings.DBType = "Access" Then


                sSQL = "Select SUM(Amount) as Total From Transactions Where [Transaction Type] = 145 And [Description 3] = 'POS CASH CONTROL' And DR_CR = 'Cr' And Accounting = True"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Crs_POS_CASH_CONTROL = "0"
                Else
                    Get_Total_Crs_POS_CASH_CONTROL = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then


                sSQL = "Select SUM(Amount) as Total From Transactions Where [Transaction Type] = 145 And [Description 3] = 'POS CASH CONTROL' And DR_CR = 'Cr' And Accounting = 1"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Crs_POS_CASH_CONTROL = "0"
                Else
                    Get_Total_Crs_POS_CASH_CONTROL = oResult
                End If
                connection.Close()

            End If

        Catch ex As Exception
            Get_Total_Crs_POS_CASH_CONTROL = "0"
        End Try
    End Function

    Function Get_Total_Drs_POS_CASH_CONTROL() As String
        Try
            Dim oResult As String
            Dim sSQL As String


            If My.Settings.DBType = "Access" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where [Transaction Type] = 145 And [Description 3] = 'POS CASH CONTROL' And DR_CR = 'Dr' And Accounting = True"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Drs_POS_CASH_CONTROL = "0"
                Else
                    Get_Total_Drs_POS_CASH_CONTROL = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Select SUM(Amount) as Total From Transactions Where [Transaction Type] = 145 And [Description 3] = 'POS CASH CONTROL' And DR_CR = 'Dr' And Accounting = 1"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar().ToString
                If oResult = "" Then
                    Get_Total_Drs_POS_CASH_CONTROL = "0"
                Else
                    Get_Total_Drs_POS_CASH_CONTROL = oResult
                End If
                connection.Close()

            End If
        Catch ex As Exception
            Get_Total_Drs_POS_CASH_CONTROL = "0"
        End Try
    End Function

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try


            Dim oOpeningFloat As Decimal = CDec(Get_Petty_Cash_Opening_Balance_Dr()) - CDec(Get_Petty_Cash_Opening_Balance_Cr())
            Dim InvoicePayments As Decimal = CDec(Get_Total_Payments()) 'invoice payments CASH
            'Dim oTotal_Crs_POS_CASH_CONTROL As Decimal = CDec(Get_Total_Crs_POS_CASH_CONTROL())
            'Dim oTotal_Drs_POS_CASH_CONTROL As Decimal = CDec(Get_Total_Drs_POS_CASH_CONTROL())
            Me.txtCashOnSystem.Text = CStr(oOpeningFloat - InvoicePayments)
            'Me.txtCashOnSystem.Text = CStr(oOpeningFloat - InvoicePayments + oTotal_Drs_POS_CASH_CONTROL - oTotal_Crs_POS_CASH_CONTROL)

            Call oResult()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Public Sub Add_Transaction_to_DGV(ByVal oWhich_DGV As String, ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                  ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                  ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                  ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                  ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                  ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                  ByVal oDocType As String, ByVal oSupplierName As String, ByVal oInfo As String, ByVal oInfo2 As String, ByVal oInfo3 As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)
            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName
            newRow.Cells(31).Value = oInfo
            newRow.Cells(32).Value = oInfo2
            newRow.Cells(33).Value = oInfo3

            If oWhich_DGV = "DGV_Transactions" Then
                Me.DGV_Transactions.Rows.Add(newRow)
            Else
                MsgBox("ERROR ON DGV CLEANUP")
            End If

        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox(ex.Message & " 048")
        End Try


    End Sub



    Sub AppendTransactions(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String


        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value

                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value

                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 049")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String

        Dim i As Integer
        Dim cmd As SqlCommand
        cmd.CommandTimeout = 300

        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value
                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value
                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 050")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Call oCreate_Cash_Spot_Check_Transactions()
        Me.Close()
    End Sub

    Sub oCreate_Cash_Spot_Check_Transactions()

        Try
            If My.Settings.Cash_Spot_Check_Segment4 = "" Or My.Settings.Cash_Spot_Check_Segment4 = Nothing Then
                MsgBox("Please set up the Cash Spot Check account and contra account in Settings")
                Exit Sub
            End If
            If My.Settings.Cash_Spot_Check_Segment4_Contra = "" Or My.Settings.Cash_Spot_Check_Segment4_Contra = Nothing Then
                MsgBox("Please set up the Cash Spot Check account and contra account in Settings")
                Exit Sub
            End If


            Dim oTo As String
            Dim oCc As String
            Dim oSubject As String
            Dim oBody As String

            Dim oVariance As Decimal = CDec(Me.txtVariance.Text)
            Dim oAmount As Decimal
            Dim sDRCR As String
            Dim sContra_DRCR As String
            Dim oSegment4_Accounting As String = My.Settings.Cash_Spot_Check_Segment4
            Dim oSegment4_Accounting_Contra As String = My.Settings.Cash_Spot_Check_Segment4_Contra
            Dim sReference As String
            Dim blnAccounting As Boolean

            If oVariance < 0 Then
                oAmount = Math.Abs(CDec(Me.txtVariance.Text))
                sDRCR = "Dr" 'Suspense Account
                sContra_DRCR = "Cr" 'Cash Control/Bank
            Else
                oAmount = CDec(Me.txtVariance.Text)
                sDRCR = "Cr"
                sContra_DRCR = "Dr"
            End If


            If Me.rdbIgnore.Checked = True Then
                blnAccounting = False
                Add_Transaction_to_DGV(oAmount, sDRCR, oSegment4_Accounting, sReference, blnAccounting)
                Add_Transaction_to_DGV(oAmount, sContra_DRCR, oSegment4_Accounting_Contra, sReference, blnAccounting)

            ElseIf Me.rdbAdjust.Checked = True Then
                blnAccounting = True 'Only one where Accounting is True
                Add_Transaction_to_DGV(oAmount, sDRCR, oSegment4_Accounting, sReference, blnAccounting)
                Add_Transaction_to_DGV(oAmount, sContra_DRCR, oSegment4_Accounting_Contra, sReference, blnAccounting)
            ElseIf Me.rdbAlert.Checked = True Then
                'Transact
                blnAccounting = False
                Add_Transaction_to_DGV(oAmount, sDRCR, oSegment4_Accounting, sReference, blnAccounting)
                Add_Transaction_to_DGV(oAmount, sContra_DRCR, oSegment4_Accounting_Contra, sReference, blnAccounting)
                'Email standard alert
                oTo = My.Settings.Cash_Spot_Check_Email_Alerts_To
                oCc = ""
                oSubject = "Cash Spot Check Alert: Date " & Now.Date.ToString("dd MMMM yyyy")
                oBody = "Alert of a variance in the Cash Spot account of: " & Me.txtVariance.Text & "."
                oSendEmail(oTo, oCc, oSubject, oBody, "")
            ElseIf Me.rdbExplain.Checked = True Then
                If Me.txtExplanation.Text = "" Then
                    MsgBox("Please fill provide an explanation")
                    Exit Sub
                End If
                'Transact
                blnAccounting = False
                Add_Transaction_to_DGV(oAmount, sDRCR, oSegment4_Accounting, sReference, blnAccounting)
                Add_Transaction_to_DGV(oAmount, sContra_DRCR, oSegment4_Accounting_Contra, sReference, blnAccounting)
                'Email explanation given
                oTo = My.Settings.Cash_Spot_Check_Email_Explanation_To
                oCc = ""
                oBody = "Explanation of a variance in the Cash Spot account of: " & Me.txtVariance.Text & "."
                oBody = oBody & vbCrLf & Me.txtExplanation.Text
                oSubject = "Cash Spot Check Explanation: Date " & Now.Date.ToString("dd MMMM yyyy")

                oSendEmail(oTo, oCc, oSubject, oBody, "")

            Else
                MsgBox("Please select one of the options!")
                Exit Sub
            End If


            If Me.DGV_Transactions.RowCount > 1 Then 'always more than 1 debit/credit
                If Check_Integrity(Me.DGV_Transactions) = False Then
                    MsgBox("The numbers are out of balance and cannot be saved!")
                Else
                    If My.Settings.DBType = "Access" Then
                        Call AppendTransactions(Me.DGV_Transactions, "Transactions")
                    ElseIf My.Settings.DBType = "SQL" Then
                        Call AppendTransactions_SQL(Me.DGV_Transactions, "Transactions")
                    End If

                End If
            End If
            Me.DGV_Transactions.Rows.Clear()

            Me.Label_Success.Visible = True
            Me.Label_Success.Text = "Sucessful Entry"

        Catch ex As Exception
            Me.Label_Success.Visible = True
            Me.Label_Success.Text = "Failure"
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub Add_Transaction_to_DGV(ByVal oAmount As Decimal, ByVal sDRCR As String, ByVal oSegment4_Accounting As String, ByVal sReference As String, ByVal blnAccounting As Boolean)
        Try
            'HERE SEGMENT 4 IS USED TO LOOK UP ALL OTHER FIELDS IN ACCOUNTING TABLE

            Dim oBatchID As Integer
            Dim oTransactionID As Long
            Dim oLinkID As Integer
            Dim sTransactionDate As String
            Dim sCaptureDate As String
            Dim sPPeriod As String
            Dim sFinYear As String
            Dim sGDC As String

            Dim sDescription As String
            Dim sAccountNumber As String
            Dim sContraAccount As String
            Dim sLinkAcc As String

            Dim oTaxType As Integer
            Dim oTaxAmount As Decimal
            Dim sUserID As String
            Dim sSupplierName As String
            Dim oSupplierID As Integer
            Dim oEmployeeID As Integer
            Dim sDescription2 As String
            Dim sDescription3 As String
            Dim sDescription4 As String
            Dim sDescription5 As String
            Dim blnPosted As Boolean

            Dim blnVoid As Boolean
            Dim TransactionType As String

            Dim sDescriptionCode As String
            Dim sDocType As String
            Dim Info As String = ""
            Dim Info2 As String = ""
            Dim Info3 As String = ""

            Dim Credit_Note_DRCR As String
            Dim oDGV As String



            '=======++++++++++++=  First Generate TransactionID and BatchID =========================== 
            oBatch_or_Transaction_generation_Failed = False

            oBatchID = CLng(oCreateBatchID())

            oTransactionID = CLng(oCreateTransactionID()) 'adds a new record to the TransactionID table thus creating a new ID number that is used as the Transaction ID

            'ABORT IF ANY OF THE FOLLOWING TRUE

            If oBatch_or_Transaction_generation_Failed = True Or oBatchID = 0 Or oTransactionID = 0 Then
                blnCRITICAL_ERROR = True
                Me.Label_Success.Text = "ENTRY FAILED!"
                Exit Sub
            End If

            '=========================================================================================

            oLinkID = 0
            sTransactionDate = Now.Date.ToString("dd MMMM yyyy") 'ALWAYS GET DATE OTHERWISE INCLUDES TIME OF DAY
            sPPeriod = GetPeriod(sTransactionDate)
            If IsNumeric(sPPeriod) = False Or CInt(sPPeriod) < 1 Then
                MsgBox("The date doesn't fall into any period in 'Periods' table!")
                Me.DGV_Transactions.Rows.Clear()
                Exit Sub
            End If

            sCaptureDate = Now.Date.ToString("dd MMMM yyyy")

            'sFinYear = My.Settings.Setting_Year 'Set this once
            ' If sFinYear = "" Or sFinYear = Nothing Then
            'MsgBox("No financial year has been set. Please set this up in 'periods'!")
            'Me.DGV_Transactions.Rows.Clear()
            'Exit Sub
            'End If
            sFinYear = GetFinYear(sTransactionDate)
            If IsNumeric(sFinYear) = False Or CInt(sFinYear) < 1 Then
                MsgBox("The date doesn't fall into any period in 'Periods' table!")
                Me.DGV_Transactions.Rows.Clear()
                Exit Sub
            End If



            sGDC = "G" 'General/Debtors/Creditors

            sLinkAcc = ""

            sUserID = "" 'for later - they should log on to use this
            sSupplierName = ""
            If sSupplierName <> "" Then
                oSupplierID = Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") 'Supplier Name
                If IsDBNull(oSupplierID) Then
                    MsgBox("No supplier ID found! Please check the supplier table.")
                    Exit Sub
                End If
            Else
                oSupplierID = Nothing
            End If

            sDescription4 = ""
            sDescription5 = ""
            oEmployeeID = 0 'not required



            oTaxAmount = 0

            'Note oSegment4_Accounting is parsed into this procedure and we use this to fetch other Accounting detail

            sDescription = Get_Segment1Desc_from_Segment4(oSegment4_Accounting)
            If sDescription = "Not found" Then
                MsgBox("Error. Please make sure that the relevant data is set up in the accounts table!")
                Me.DGV_Transactions.Rows.Clear()
                Exit Sub
            End If
            sDescription2 = Get_Segment2Desc_from_Segment4(oSegment4_Accounting)
            If sDescription2 = "Not found" Then
                MsgBox("Error. Please make sure that the relevant data is set up in the accounts table!")
                Me.DGV_Transactions.Rows.Clear()
                Exit Sub
            End If
            sDescription3 = "CASH CONTROL SPOT CHECK"
            'sDescription3 = Get_Segment3Desc_from_Segment4(oSegment4_Accounting)
            'If sDescription3 = "Not found" Then
            'MsgBox("Error. Please make sure that the relevant data is set up in the accounts table!")
            'Me.DGV_Transactions.Rows.Clear()
            'Exit Sub
            'End If

            TransactionType = 20 'Same as General Journal 

            oTaxType = 0

            sAccountNumber = Get_GLACCOUNT_Using_Segment4(oSegment4_Accounting)
            If sAccountNumber = "Not found" Then
                MsgBox("Error. Please make sure that the relevant data is set up in the accounts table!")
                Me.DGV_Transactions.Rows.Clear()
                Exit Sub
            End If
            sDescriptionCode = oSegment4_Accounting

            sDocType = ""
            Info = ""
            Info2 = ""


            Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sReference, _
    sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
   oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
   blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function Check_Integrity(ByVal oDataGridView As DataGridView) As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To oDataGridView.RowCount - 2 'extra row
                If oDataGridView.Rows(i).Cells(26).Value = "Dr" Then
                    oDebits = oDebits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                ElseIf oDataGridView.Rows(i).Cells(26).Value = "Cr" Then
                    oCredits = oCredits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                End If
            Next

            If oDebits = oCredits Then
                Check_Integrity = True
            Else
                Check_Integrity = False
            End If


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox("There was an error checking integrity " & ex.Message & " 024")
        End Try
    End Function


    Function Get_GLACCOUNT_Using_Segment4(ByVal sString As String) As String
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_GLACCOUNT_Using_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_GLACCOUNT_Using_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()

                'End If
            End If
        Catch ex As Exception
            Get_GLACCOUNT_Using_Segment4 = "Not found"
        End Try
    End Function

    Function Get_Segment1Desc_from_Segment4(ByVal sString As String) As String
        Try
            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment1Desc_from_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()

                'End If
            End If
        Catch ex As Exception
            Get_Segment1Desc_from_Segment4 = "Not found"
        End Try
    End Function

    Function Get_Segment2Desc_from_Segment4(ByVal sString As String) As String
        Try
            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment2Desc_from_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment2Desc_from_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            Get_Segment2Desc_from_Segment4 = "Not found"
        End Try
    End Function

    Function Get_Segment3Desc_from_Segment4(ByVal sString As String) As String
        Try
            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment3Desc_from_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            Get_Segment3Desc_from_Segment4 = "Not found"
        End Try
    End Function

    Sub oShowHideBoxes()
        Try
            If Me.rdbExplain.Checked = True Then
                Me.txtExplanation.Text = ""
                Me.txtExplanation.Visible = True
                Me.lblExplanation.Visible = True
            Else
                Me.txtExplanation.Text = ""
                Me.txtExplanation.Visible = False
                Me.lblExplanation.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub rdbIgnore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbIgnore.CheckedChanged
        oShowHideBoxes()
    End Sub

  
    Private Sub rdbAdjust_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAdjust.CheckedChanged
        oShowHideBoxes()
    End Sub

    Private Sub rdbAlert_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAlert.CheckedChanged
        oShowHideBoxes()
    End Sub

    Private Sub rdbExplain_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbExplain.CheckedChanged
        oShowHideBoxes()
    End Sub

    Private Sub DateTimePicker_To_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker_To.ValueChanged

    End Sub
End Class