﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPay_ACB_Files
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Database1DataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Btn_Pay = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.chk = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LBL_LayoutType = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CB_BankAccount = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DT_EffectDate = New System.Windows.Forms.DateTimePicker()
        Me.lblCompanyPayer = New System.Windows.Forms.Label()
        Me.Lbl_BankRequestID = New System.Windows.Forms.Label()
        Me.Btn_SupplierMaster = New System.Windows.Forms.Button()
        Me.Btn_Accounts = New System.Windows.Forms.Button()
        CType(Me.Database1DataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(30, 61)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(88, 21)
        Me.CheckBox1.TabIndex = 5
        Me.CheckBox1.Text = "Select All"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Btn_Pay
        '
        Me.Btn_Pay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Pay.Location = New System.Drawing.Point(1157, 13)
        Me.Btn_Pay.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Pay.Name = "Btn_Pay"
        Me.Btn_Pay.Size = New System.Drawing.Size(134, 74)
        Me.Btn_Pay.TabIndex = 4
        Me.Btn_Pay.Tag = "S_Allow_Export"
        Me.Btn_Pay.Text = "Export Payment Batch"
        Me.Btn_Pay.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.chk})
        Me.DataGridView1.Location = New System.Drawing.Point(30, 90)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1121, 565)
        Me.DataGridView1.TabIndex = 3
        '
        'chk
        '
        Me.chk.HeaderText = "Choose"
        Me.chk.Name = "chk"
        Me.chk.Width = 62
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.LBL_LayoutType)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.CB_BankAccount)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DT_EffectDate)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 0)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1135, 53)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Company Paying "
        '
        'LBL_LayoutType
        '
        Me.LBL_LayoutType.AutoSize = True
        Me.LBL_LayoutType.Location = New System.Drawing.Point(665, 19)
        Me.LBL_LayoutType.Name = "LBL_LayoutType"
        Me.LBL_LayoutType.Size = New System.Drawing.Size(13, 17)
        Me.LBL_LayoutType.TabIndex = 6
        Me.LBL_LayoutType.Text = "-"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(311, 23)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "From Account:"
        '
        'CB_BankAccount
        '
        Me.CB_BankAccount.FormattingEnabled = True
        Me.CB_BankAccount.Location = New System.Drawing.Point(423, 13)
        Me.CB_BankAccount.Margin = New System.Windows.Forms.Padding(4)
        Me.CB_BankAccount.Name = "CB_BankAccount"
        Me.CB_BankAccount.Size = New System.Drawing.Size(220, 24)
        Me.CB_BankAccount.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 22)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Effective Date:"
        '
        'DT_EffectDate
        '
        Me.DT_EffectDate.CustomFormat = "dd MMM yyyy"
        Me.DT_EffectDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_EffectDate.Location = New System.Drawing.Point(123, 13)
        Me.DT_EffectDate.Margin = New System.Windows.Forms.Padding(4)
        Me.DT_EffectDate.Name = "DT_EffectDate"
        Me.DT_EffectDate.Size = New System.Drawing.Size(169, 22)
        Me.DT_EffectDate.TabIndex = 1
        '
        'lblCompanyPayer
        '
        Me.lblCompanyPayer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCompanyPayer.AutoSize = True
        Me.lblCompanyPayer.Location = New System.Drawing.Point(1159, 303)
        Me.lblCompanyPayer.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCompanyPayer.Name = "lblCompanyPayer"
        Me.lblCompanyPayer.Size = New System.Drawing.Size(138, 17)
        Me.lblCompanyPayer.TabIndex = 0
        Me.lblCompanyPayer.Text = "Loading Company...."
        Me.lblCompanyPayer.Visible = False
        '
        'Lbl_BankRequestID
        '
        Me.Lbl_BankRequestID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Lbl_BankRequestID.AutoSize = True
        Me.Lbl_BankRequestID.Location = New System.Drawing.Point(1159, 276)
        Me.Lbl_BankRequestID.Name = "Lbl_BankRequestID"
        Me.Lbl_BankRequestID.Size = New System.Drawing.Size(133, 17)
        Me.Lbl_BankRequestID.TabIndex = 7
        Me.Lbl_BankRequestID.Text = "Lbl_BankRequestID"
        Me.Lbl_BankRequestID.Visible = False
        '
        'Btn_SupplierMaster
        '
        Me.Btn_SupplierMaster.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_SupplierMaster.Location = New System.Drawing.Point(1157, 195)
        Me.Btn_SupplierMaster.Name = "Btn_SupplierMaster"
        Me.Btn_SupplierMaster.Size = New System.Drawing.Size(135, 36)
        Me.Btn_SupplierMaster.TabIndex = 8
        Me.Btn_SupplierMaster.Tag = "S_Allow_Supplier_Edit"
        Me.Btn_SupplierMaster.Text = "Supplier Master"
        Me.Btn_SupplierMaster.UseVisualStyleBackColor = True
        '
        'Btn_Accounts
        '
        Me.Btn_Accounts.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Accounts.Location = New System.Drawing.Point(1157, 237)
        Me.Btn_Accounts.Name = "Btn_Accounts"
        Me.Btn_Accounts.Size = New System.Drawing.Size(135, 36)
        Me.Btn_Accounts.TabIndex = 9
        Me.Btn_Accounts.Tag = "S_Allow_Supplier_Edit"
        Me.Btn_Accounts.Text = "Accounts Master"
        Me.Btn_Accounts.UseVisualStyleBackColor = True
        '
        'frmPay_ACB_Files
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1306, 682)
        Me.Controls.Add(Me.Btn_Accounts)
        Me.Controls.Add(Me.Btn_SupplierMaster)
        Me.Controls.Add(Me.Lbl_BankRequestID)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblCompanyPayer)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Btn_Pay)
        Me.Controls.Add(Me.DataGridView1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmPay_ACB_Files"
        Me.Tag = "S_Bank_Export"
        Me.Text = "Please wait while screen loads ....."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Database1DataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Database1DataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Btn_Pay As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblCompanyPayer As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CB_BankAccount As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DT_EffectDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Lbl_BankRequestID As System.Windows.Forms.Label
    Friend WithEvents Btn_SupplierMaster As System.Windows.Forms.Button
    Friend WithEvents Btn_Accounts As System.Windows.Forms.Button
    Friend WithEvents chk As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents LBL_LayoutType As System.Windows.Forms.Label
End Class
