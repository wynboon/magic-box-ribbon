﻿Imports System.Windows.Forms

Public Class frmUpdateBankAcc
#Region "Variables"
    Public ID As Integer = Nothing
    Public isExit As Boolean = False
    Public IsNewAccount As Boolean = False
    Dim MBData As New DataExtractsClass
#End Region
    Private Sub btnUpdateAccNumber_Click(sender As Object, e As EventArgs) Handles btnUpdateAccNumber.Click
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            If btnUpdateAccNumber.Text <> "&Ok" Then
                If Not IsNumeric(txtBankAccNo.Text.Trim) Or txtBankAccNo.TextLength = 0 Then
                    MessageBox.Show("Please supply the Bank Acc no. in a correct format", _
                                    "Bank Acc no. update", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Enabled = True
                    Cursor = Cursors.Arrow
                    txtBankAccNo.Focus()
                Else
                    MBData.UpdateBankAcc(ID, CStr(txtBankAccNo.Text.Trim))
                    MessageBox.Show("The account has been succesfully updated with the Bank Acc no.", _
                                    "Bank Acc no.", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Close()
                End If
            Else

                If Not IsNumeric(txtBankAccNo.Text.Trim) Or txtBankAccNo.TextLength = 0 Then
                    MessageBox.Show("Please supply the Bank Acc no. in a correct format", _
                                    "Bank Acc no. update", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Enabled = True
                    Cursor = Cursors.Arrow
                    txtBankAccNo.Focus()
                    Exit Sub
                End If

                Me.Close()

                End If

                Enabled = Enabled
                Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details :", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = Enabled
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        isExit = True
        Close()
    End Sub

    Private Sub frmUpdateBankAcc_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If IsNewAccount Then
            btnUpdateAccNumber.Text = "&Ok"
        End If
    End Sub
End Class