﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data

Public Class FrmSupplierBank

    Private Sub Frm_SupplierBank_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "Suppliers Bank Details"
        LockForm(Me)
        LoadSupplierGrid()
    End Sub
    Public Sub LoadSupplierGrid()

        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select ID, RTrim([AccountNumber]) as AccountNumber,RTrim([AccountName]) as AccountName,RTrim([Branch]) as Branch,RTrim([DefaultRef]) as DefaultRef from [SupplierBank] where [ID] = " & BankID.Text)

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Using connection
                Dim command As SqlCommand = New SqlCommand(sSQL, connection)
                connection.Open()

                Dim reader As SqlDataReader = command.ExecuteReader()

                If reader.HasRows Then
                    Do While reader.Read()
                        TB_AccountName.Text = reader.Item("AccountName")
                        TB_AccountNumber.Text = reader.Item("AccountNumber")
                        TB_Branch.Text = reader.Item("Branch")
                        TB_DefaultRef.Text = reader.Item("DefaultRef")
                    Loop
                Else
                    TB_AccountName.Text = ""
                    TB_AccountNumber.Text = ""
                    TB_Branch.Text = ""
                    TB_DefaultRef.Text = ""
                End If

                reader.Close()
            End Using
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub Btn_Add_Click(sender As Object, e As EventArgs) Handles Btn_Add.Click

        If TB_AccountName.Text = "" Then
            MsgBox("Account name may not be blank", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If TB_AccountNumber.Text = "" Then
            MsgBox("Account number may not be blank", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If TB_Branch.Text = "" Then
            MsgBox("Account branch may not be blank", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If TB_DefaultRef.Text = "" Then
            MsgBox("Default reference may not be blank", MsgBoxStyle.Critical)
            Exit Sub
        End If


        Dim sSQL As String

        If BankID.Text = "0" Then
            sSQL = "INSERT INTO [SupplierBank] ([SupplierID],[AccountNumber],[AccountName],[Branch],[DefaultRef]) Values ('" & _
                    LBL_SupplierID.Text & "','" & TB_AccountNumber.Text & "','" & TB_AccountName.Text & "','" & TB_Branch.Text & "','" & TB_DefaultRef.Text & "')"
        Else
            sSQL = "Update [SupplierBank] set " & _
               " [SupplierID] = '" & LBL_SupplierID.Text & "',[AccountNumber] = '" & TB_AccountNumber.Text & "'," & _
               " [AccountName] = '" & TB_AccountName.Text & "',[Branch] = '" & TB_Branch.Text & "'," & _
               "[DefaultRef] = '" & TB_DefaultRef.Text & "' Where ID = " & BankID.Text
        End If


        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        '// define the sql statement to execute
        Dim cmd As New SqlCommand(sSQL, cn)
        cmd.CommandTimeout = 300
        '    '// open the connection
        cn.Open()
        cmd.ExecuteNonQuery()
        cn.Close()
        MessageBox.Show("New supplier bank details added!", "Bank Details", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Cursor = Cursors.Arrow
        LoadSupplierGrid()
        Me.Close()
    End Sub

    
End Class