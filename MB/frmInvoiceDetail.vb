﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D


Public Class frmInvoiceDetail
#Region "Variables"
    Public isGridSelected As Boolean = Nothing
    Public SupplierID As Integer = Nothing
    Public Reference As String = ""
#End Region
    Private Sub frmInvoiceDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If CStr(My.Settings.CS_Setting) = String.Empty And CStr(My.Settings.ListViewValues) = String.Empty Then
                Me.Text = "MagicBox -> Invoice Detail -> Version : " & My.Settings.Setting_Version
                Exit Sub
            Else
                Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Invoice Detail -> Version : " & My.Settings.Setting_Version
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            GetInvoiceDetailPerSupplier()
            DueSums()

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = False
            Cursor = Cursors.AppStarting
        End Try

    End Sub
    Sub GetInvoiceDetailPerSupplier()
        Try
            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            If Not isGridSelected Then
                MsgBox("Please invoice an invoice to view details..")
                Exit Sub
            End If

            Dim sSQL_Supplier As String = ""
            DataGridView1.DataSource = Nothing

            Dim sSQL As String

            sSQL = "Select [Transaction Date], Reference, (Case When Dr_Cr = 'Cr'then Amount  else amount * -1 end) as Amount, DocType, Info, Info2 From Transactions "
            sSQL = sSQL & " Where DocType <> '' AND [Description Code]='HAAAAA' And SupplierID = " & SupplierID _
            & " AND Reference = '" & Reference & "'"


            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            sSQL = sSQL & " And Void = 0"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Suppliers_table")
            connection.Close()
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "Suppliers_table"

            If DataGridView1.ColumnCount > 0 Then

                Me.DataGridView1.Columns(2).DefaultCellStyle.Format = "n2"
                Me.DataGridView1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub DueSums()
        Try
            Dim strDate As String
            Dim oDate As Date
            Dim Due As Decimal
            For Each row As DataGridViewRow In DataGridView1.Rows
                strDate = row.Cells("Transaction Date").Value
                oDate = CDate(strDate)
                If oDate > Now.Date Then
                    row.DefaultCellStyle.BackColor = Color.Red
                    lblRed.Visible = True
                End If
                Due += CDec(row.Cells("Amount").Value)
            Next

            txtAmount.TextAlign = HorizontalAlignment.Right
            txtAmount.Text = Due.ToString("c")

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class