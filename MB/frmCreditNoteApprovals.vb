﻿

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D

Public Class frmCreditNoteApprovals
#Region "Variables"
    Dim MBData As New DataExtractsClass
    Public FromSupplier As Boolean = False
    Dim blnSuccessOrFailure
#End Region

    Private Sub frmCreditNoteApprovals_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Credit Note Approvals -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            FillComboBoxSuppliers()
            If FromSupplier Then
                btnSearch_Click(sender, e)
            End If
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured has occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub FillComboBoxSuppliers()
        Try

            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Sub BuildSQL()

        Try

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Dim sSQL As String = ""

            If ckbAllSuppliers.Checked = True Then
                sSQL = "Select ID, TransactionID, LinkID, SupplierName, Amount,Reference, [Transaction Date],DocType,Info from Transactions where Info2 like '%Credit Note%' AND DocType = 'Invoice' And Accounting=0"
            Else
                If cmbSupplier.SelectedIndex = -1 Then
                    MessageBox.Show("Please select a supplier in order to continue", "Supplier", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    cmbSupplier.Focus()
                    Exit Sub
                End If
                sSQL = "Select ID, TransactionID, LinkID, SupplierName, Amount,Reference, [Transaction Date],DocType,Info from Transactions where Info2 like '%Credit Note%' AND DocType = 'Invoice' And Accounting=0 AND SupplierID = '" & CInt(cmbSupplier.SelectedValue) & "'"
            End If
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Call Load_DGV(sSQL)

            Dim ckbApprove As New DataGridViewCheckBoxColumn
            With ckbApprove
                .Name = "ckbApprove"
                .HeaderText = "Approve"
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            End With
            grdCrNApprovals.Columns.Insert(0, ckbApprove)
            grdCrNApprovals.Columns("ID").Visible = False
            grdCrNApprovals.Columns("LinkID").Visible = False

            grdCrNApprovals.Columns("TransactionID").ReadOnly = True
            grdCrNApprovals.Columns("SupplierName").ReadOnly = True
            grdCrNApprovals.Columns("Amount").ReadOnly = True
            grdCrNApprovals.Columns("Reference").ReadOnly = True
            grdCrNApprovals.Columns("Transaction Date").ReadOnly = True
            grdCrNApprovals.Columns("DocType").ReadOnly = True
            grdCrNApprovals.Columns("Info").ReadOnly = True

            Call Calc_Totals()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured. Error message : " & ex.Message, "Credit Notes approvals", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try

    End Sub

    Sub Load_DGV(ByVal sSQL As String)

        Dim connection1 As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Try
            'First clear DGV
            grdCrNApprovals.DataSource = Nothing
            grdCrNApprovals.Columns.Clear() 'in case of extra columns

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Data_table")
                connection.Close()
                grdCrNApprovals.DataSource = ds
                grdCrNApprovals.DataMember = "Data_table"

                grdCrNApprovals.Columns(1).DefaultCellStyle.Format = "n2" 'note extra credit box hasn't been added yet

            ElseIf My.Settings.DBType = "SQL" Then


                Dim dataadapter As New SqlDataAdapter(sSQL, connection1)
                Dim ds As New DataSet()
                connection1.Open()
                dataadapter.Fill(ds, "Data_table")
                connection1.Close()
                grdCrNApprovals.DataSource = ds
                grdCrNApprovals.DataMember = "Data_table"

            End If

            grdCrNApprovals.Columns("Amount").DefaultCellStyle.Format = "c"
            Me.grdCrNApprovals.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        Catch ex As Exception
            If connection1.State = ConnectionState.Open Then
                connection1.Dispose()
            End If
            MsgBox(ex.Message & " 090")
        End Try

    End Sub

    Private Sub DateTimePicker_From_ValueChanged(sender As System.Object, e As System.EventArgs)
        Call BuildSQL()
    End Sub

    Private Sub DateTimePicker_To_ValueChanged(sender As System.Object, e As System.EventArgs)
        Call BuildSQL()
    End Sub

    Private Sub btnApprove_Click(sender As System.Object, e As System.EventArgs) Handles btnApprove.Click
        Call oApprove()
    End Sub
    Sub oApprove()
        Try

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            For Each row In grdCrNApprovals.Rows
                If row.Cells(0).Value = True Then

                    Dim ID As Integer = CInt(row.Cells("ID").Value)
                    Dim TransactionID As Integer = CInt(row.Cells("TransactionID").Value)
                    Dim LinkID As Integer = CInt(row.Cells("LinkID").Value)

                    Nullify_RequestPayment(TransactionID, ID)
                    Set_Accounting_to_True(TransactionID, LinkID, ID)

                End If
            Next
            MsgBox("Approval succeeded!")
            Call BuildSQL()
            Call Calc_Totals()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Nullify_RequestPayment(ByVal oTransID As String, ByVal ID As Integer)

        Try

            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice' WHERE Info2 like  '%Credit Note%' And TransactionID = " & oTransID & " AND ID =" & ID
                sSQL = sSQL & " Co_ID = " & My.Settings.Setting_CompanyID
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice' WHERE Info2 like  '%Credit Note%' And TransactionID = " & oTransID
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 326")
        End Try

    End Sub
    Function oCheck_Integrity() As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal

            oDebits = 0
            oCredits = 0

            For Each row As DataGridViewRow In DGV_Transactions.Rows
                If row.Cells(26).Value = "Dr" Then
                    oDebits = oDebits + CDec(row.Cells(12).Value)
                ElseIf row.Cells(26).Value = "Cr" Then
                    oCredits = oCredits + CDec(row.Cells(12).Value)
                End If
            Next

            If oDebits = oCredits Then
                If oDebits = 0 Or oCredits = 0 Then
                    oCheck_Integrity = False
                Else
                    oCheck_Integrity = True
                End If
            Else
                oCheck_Integrity = False
            End If


        Catch ex As Exception
            MsgBox("There was an error checking integrity " & ex.Message & " 061")
        End Try
    End Function
    Sub Set_Accounting_to_True(ByVal oTransactionID As String, ByVal oLinkID As String, ByVal ID As Integer)

        Try
            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = -1 WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0 AND ID = " & ID
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = 1 WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 097")
        End Try

    End Sub
    Private Sub btnApproveAll_Click(sender As System.Object, e As System.EventArgs) Handles btnApproveAll.Click
        ckbSelectAll.Checked = True
        oApprove()
    End Sub

    Sub Calc_Totals()
        Try

            Dim oTotal As Decimal = 0
            For Each row As DataGridViewRow In grdCrNApprovals.Rows
                oTotal = oTotal + CDec(row.Cells("Amount").Value)
            Next
            txtBalance.Text = oTotal.ToString("c")

        Catch ex As Exception
            MsgBox(ex.Message & " 092")
        End Try
    End Sub
    Private Sub ckbSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles ckbSelectAll.CheckedChanged
        If ckbSelectAll.Checked = True Then
            For Each r As DataGridViewRow In grdCrNApprovals.Rows
                r.Cells("ckbApprove").Value = CBool(True)
            Next
        Else
            For Each r As DataGridViewRow In grdCrNApprovals.Rows
                r.Cells("ckbApprove").Value = CBool(False)
            Next
        End If
    End Sub

    Private Sub ckbAllSuppliers_CheckedChanged(sender As Object, e As EventArgs) Handles ckbAllSuppliers.CheckedChanged
        If ckbAllSuppliers.Checked = True Then
            cmbSupplier.Enabled = False
        Else
            cmbSupplier.Enabled = True
        End If
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BuildSQL()
    End Sub
End Class