﻿Imports System.Windows.Forms
Imports System.Data

Public Class frmItemEdit

    Private Sub Btn_Search_Click(sender As Object, e As EventArgs) Handles Btn_Search.Click
        LoadItems()
    End Sub
    Sub LoadItems()
        DGV_Items.Enabled = False
        TSS_Progress.Style = Windows.Forms.ProgressBarStyle.Marquee
        TSS_Progress.Visible = True
        TSS_Progress.MarqueeAnimationSpeed = 30
        Application.DoEvents()

        '   DGV_Items.Rows.Clear()
        Dim ds_FullData As DataTable
        ds_FullData = ItemList(TB_Search.Text)

        DGV_Items.DataSource = ds_FullData
        DGV_Items.Columns("ID").Visible = False
        DGV_Items.Columns("Co_ID").Visible = False
        TSS_Progress.MarqueeAnimationSpeed = 0
        TSS_Progress.Visible = False
        DGV_Items.Enabled = True
    End Sub

    Private Sub DGV_Items_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Items.CellClick
        If e.RowIndex = -1 Then Exit Sub
        LBL_ID.Text = DGV_Items.Item("ID", e.RowIndex).Value.ToString
        TB_ItemCode.Text = DGV_Items.Item("ItemCode", e.RowIndex).Value.ToString
        TB_ItemName.Text = DGV_Items.Item("ItemName", e.RowIndex).Value.ToString
        CB_VAT.Checked = DGV_Items.Item("HasVAT", e.RowIndex).Value.ToString
        CB_Cat1.Text = Get_AccountInfo_from_Seg4(DGV_Items.Item("Account", e.RowIndex).Value.ToString, "Segment 1 Desc")
        CB_Cat2.Text = Get_AccountInfo_from_Seg4(DGV_Items.Item("Account", e.RowIndex).Value.ToString, "Segment 2 Desc")
        CB_Cat3.Text = Get_AccountInfo_from_Seg4(DGV_Items.Item("Account", e.RowIndex).Value.ToString, "Segment 3 Desc")
    End Sub

    Sub LoadCats1()
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        ds = ds.DefaultView.ToTable(True, "Segment 1 Desc")
        CB_Cat1.DataSource = ds
        CB_Cat1.DisplayMember = "Segment 1 Desc"
        ds = Nothing
    End Sub

    Private Sub CB_Cat1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Cat1.SelectedIndexChanged
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        If CB_Cat1.Text.ToString = "" Then Exit Sub
        ds.DefaultView.RowFilter = "[Segment 1 Desc] = '" & CB_Cat1.Text & "'"
        ds = ds.DefaultView.ToTable(True, "Segment 2 Desc")
        CB_Cat2.DataSource = ds
        CB_Cat2.DisplayMember = "Segment 2 Desc"
        ds = Nothing
    End Sub

    Private Sub CB_Cat2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Cat2.SelectedIndexChanged
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        If CB_Cat2.Text = "" Then Exit Sub
        ds.DefaultView.RowFilter = "[Segment 2 Desc] = '" & CB_Cat2.Text & "'"
        ds = ds.DefaultView.ToTable(True, "Segment 3 Desc")
        CB_Cat3.DataSource = ds
        CB_Cat3.DisplayMember = "Segment 3 Desc"
        ds = Nothing
    End Sub

    Private Sub frmItemEdit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.AppIcon
        Me.Text = "MagicBox -> " & CStr(My.Settings.CurrentCompany) & " -> Item Master Edit -> Version : " & My.Settings.Setting_Version
        Call LoadCats1()
    End Sub

    Private Sub DGV_Items_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Items.CellContentClick

    End Sub

    Private Sub BTN_Edit_Click(sender As Object, e As EventArgs) Handles BTN_Edit.Click
        Dim AccountNo As String
        AccountNo = Get_Segment4(CB_Cat1.Text, CB_Cat2.Text, CB_Cat3.Text)
        EditItemMaster(LBL_ID.Text, TB_ItemCode.Text, TB_ItemName.Text, AccountNo, CB_VAT.Checked)
        LoadItems()
    End Sub
End Class