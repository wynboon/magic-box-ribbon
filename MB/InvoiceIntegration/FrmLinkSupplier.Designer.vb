﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLinkSupplier
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LBL_SupplierName = New System.Windows.Forms.Label()
        Me.btn_Link = New System.Windows.Forms.Button()
        Me.btn_CreateNew = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Supplier Name:"
        '
        'LBL_SupplierName
        '
        Me.LBL_SupplierName.AutoSize = True
        Me.LBL_SupplierName.Location = New System.Drawing.Point(124, 13)
        Me.LBL_SupplierName.Name = "LBL_SupplierName"
        Me.LBL_SupplierName.Size = New System.Drawing.Size(13, 17)
        Me.LBL_SupplierName.TabIndex = 1
        Me.LBL_SupplierName.Text = "-"
        '
        'btn_Link
        '
        Me.btn_Link.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Link.Location = New System.Drawing.Point(367, 13)
        Me.btn_Link.Name = "btn_Link"
        Me.btn_Link.Size = New System.Drawing.Size(156, 37)
        Me.btn_Link.TabIndex = 2
        Me.btn_Link.Text = "Link Supplier"
        Me.btn_Link.UseVisualStyleBackColor = True
        '
        'btn_CreateNew
        '
        Me.btn_CreateNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_CreateNew.Location = New System.Drawing.Point(529, 13)
        Me.btn_CreateNew.Name = "btn_CreateNew"
        Me.btn_CreateNew.Size = New System.Drawing.Size(148, 37)
        Me.btn_CreateNew.TabIndex = 3
        Me.btn_CreateNew.Text = "Create New Supplier"
        Me.btn_CreateNew.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(13, 70)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(664, 400)
        Me.DataGridView1.TabIndex = 4
        '
        'FrmLinkSupplier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(689, 482)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.btn_CreateNew)
        Me.Controls.Add(Me.btn_Link)
        Me.Controls.Add(Me.LBL_SupplierName)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmLinkSupplier"
        Me.Text = "FrmLinkSupplier"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LBL_SupplierName As System.Windows.Forms.Label
    Friend WithEvents btn_Link As System.Windows.Forms.Button
    Friend WithEvents btn_CreateNew As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
End Class
