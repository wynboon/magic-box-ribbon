﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_Items = New System.Windows.Forms.DataGridView()
        Me.TB_Search = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Btn_Search = New System.Windows.Forms.Button()
        Me.TSS_Progress = New System.Windows.Forms.ProgressBar()
        Me.TB_ItemCode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_ItemName = New System.Windows.Forms.TextBox()
        Me.CB_VAT = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CB_Cat1 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CB_Cat2 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CB_Cat3 = New System.Windows.Forms.ComboBox()
        Me.LBL_ID = New System.Windows.Forms.Label()
        Me.BTN_Edit = New System.Windows.Forms.Button()
        CType(Me.DGV_Items, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Items
        '
        Me.DGV_Items.AllowUserToAddRows = False
        Me.DGV_Items.AllowUserToDeleteRows = False
        Me.DGV_Items.AllowUserToOrderColumns = True
        Me.DGV_Items.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGV_Items.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Items.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGV_Items.Location = New System.Drawing.Point(13, 103)
        Me.DGV_Items.MultiSelect = False
        Me.DGV_Items.Name = "DGV_Items"
        Me.DGV_Items.RowTemplate.Height = 24
        Me.DGV_Items.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_Items.Size = New System.Drawing.Size(782, 388)
        Me.DGV_Items.TabIndex = 0
        '
        'TB_Search
        '
        Me.TB_Search.Location = New System.Drawing.Point(75, 13)
        Me.TB_Search.Name = "TB_Search"
        Me.TB_Search.Size = New System.Drawing.Size(247, 22)
        Me.TB_Search.TabIndex = 1
        Me.TB_Search.Text = "%"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Search:"
        '
        'Btn_Search
        '
        Me.Btn_Search.Location = New System.Drawing.Point(338, 5)
        Me.Btn_Search.Name = "Btn_Search"
        Me.Btn_Search.Size = New System.Drawing.Size(75, 30)
        Me.Btn_Search.TabIndex = 3
        Me.Btn_Search.Text = "Search"
        Me.Btn_Search.UseVisualStyleBackColor = True
        '
        'TSS_Progress
        '
        Me.TSS_Progress.Location = New System.Drawing.Point(419, 12)
        Me.TSS_Progress.Name = "TSS_Progress"
        Me.TSS_Progress.Size = New System.Drawing.Size(207, 23)
        Me.TSS_Progress.TabIndex = 4
        '
        'TB_ItemCode
        '
        Me.TB_ItemCode.Location = New System.Drawing.Point(15, 75)
        Me.TB_ItemCode.Name = "TB_ItemCode"
        Me.TB_ItemCode.Size = New System.Drawing.Size(114, 22)
        Me.TB_ItemCode.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Item Code"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(132, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Item Name"
        '
        'TB_ItemName
        '
        Me.TB_ItemName.Location = New System.Drawing.Point(135, 75)
        Me.TB_ItemName.Name = "TB_ItemName"
        Me.TB_ItemName.Size = New System.Drawing.Size(114, 22)
        Me.TB_ItemName.TabIndex = 7
        '
        'CB_VAT
        '
        Me.CB_VAT.AutoSize = True
        Me.CB_VAT.Location = New System.Drawing.Point(260, 78)
        Me.CB_VAT.Name = "CB_VAT"
        Me.CB_VAT.Size = New System.Drawing.Size(18, 17)
        Me.CB_VAT.TabIndex = 9
        Me.CB_VAT.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(253, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 17)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "VAT"
        '
        'CB_Cat1
        '
        Me.CB_Cat1.FormattingEnabled = True
        Me.CB_Cat1.Location = New System.Drawing.Point(294, 72)
        Me.CB_Cat1.Name = "CB_Cat1"
        Me.CB_Cat1.Size = New System.Drawing.Size(163, 24)
        Me.CB_Cat1.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(291, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Level 1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(460, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 17)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Level 2"
        '
        'CB_Cat2
        '
        Me.CB_Cat2.FormattingEnabled = True
        Me.CB_Cat2.Location = New System.Drawing.Point(463, 71)
        Me.CB_Cat2.Name = "CB_Cat2"
        Me.CB_Cat2.Size = New System.Drawing.Size(163, 24)
        Me.CB_Cat2.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(629, 51)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 17)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Level 3"
        '
        'CB_Cat3
        '
        Me.CB_Cat3.FormattingEnabled = True
        Me.CB_Cat3.Location = New System.Drawing.Point(632, 71)
        Me.CB_Cat3.Name = "CB_Cat3"
        Me.CB_Cat3.Size = New System.Drawing.Size(163, 24)
        Me.CB_Cat3.TabIndex = 15
        '
        'LBL_ID
        '
        Me.LBL_ID.AutoSize = True
        Me.LBL_ID.Location = New System.Drawing.Point(632, 18)
        Me.LBL_ID.Name = "LBL_ID"
        Me.LBL_ID.Size = New System.Drawing.Size(16, 17)
        Me.LBL_ID.TabIndex = 17
        Me.LBL_ID.Text = "0"
        '
        'BTN_Edit
        '
        Me.BTN_Edit.Location = New System.Drawing.Point(654, 5)
        Me.BTN_Edit.Name = "BTN_Edit"
        Me.BTN_Edit.Size = New System.Drawing.Size(141, 36)
        Me.BTN_Edit.TabIndex = 18
        Me.BTN_Edit.Text = "EDIT ITEM"
        Me.BTN_Edit.UseVisualStyleBackColor = True
        '
        'frmItemEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 503)
        Me.Controls.Add(Me.BTN_Edit)
        Me.Controls.Add(Me.LBL_ID)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.CB_Cat3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.CB_Cat2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.CB_Cat1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CB_VAT)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TB_ItemName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TB_ItemCode)
        Me.Controls.Add(Me.TSS_Progress)
        Me.Controls.Add(Me.Btn_Search)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TB_Search)
        Me.Controls.Add(Me.DGV_Items)
        Me.Name = "frmItemEdit"
        Me.Text = "frmItemEdit"
        CType(Me.DGV_Items, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_Items As System.Windows.Forms.DataGridView
    Friend WithEvents TB_Search As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Btn_Search As System.Windows.Forms.Button
    Friend WithEvents TSS_Progress As System.Windows.Forms.ProgressBar
    Friend WithEvents TB_ItemCode As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_ItemName As System.Windows.Forms.TextBox
    Friend WithEvents CB_VAT As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CB_Cat1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CB_Cat2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CB_Cat3 As System.Windows.Forms.ComboBox
    Friend WithEvents LBL_ID As System.Windows.Forms.Label
    Friend WithEvents BTN_Edit As System.Windows.Forms.Button
End Class
