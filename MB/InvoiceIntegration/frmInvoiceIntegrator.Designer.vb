﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceIntegrator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DT_EndDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DT_StartDate = New System.Windows.Forms.DateTimePicker()
        Me.Btn_Refresh = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TSS_Note = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSS_Progress = New System.Windows.Forms.ToolStripProgressBar()
        Me.SS_Note2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lbl_InvRef = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbl_SupplierName = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lbl_InvDate = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_LineDesc = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CB_Vat = New System.Windows.Forms.ComboBox()
        Me.CB_Cat2 = New System.Windows.Forms.ComboBox()
        Me.CB_Cat1 = New System.Windows.Forms.ComboBox()
        Me.CB_Cat3 = New System.Windows.Forms.ComboBox()
        Me.DGV_InvoiceList = New System.Windows.Forms.DataGridView()
        Me.DGV_InvoiceDetail = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Lbl_Total_Incl = New System.Windows.Forms.Label()
        Me.Lbl_Total_Vat = New System.Windows.Forms.Label()
        Me.Lbl_Total_Excl = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TB_PaidAmount = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.DTP_PaidDate = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.CB_PayType = New System.Windows.Forms.ComboBox()
        Me.CB_IsPaid = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TB_TaxAmount = New System.Windows.Forms.TextBox()
        Me.BTN_AddLine = New System.Windows.Forms.Button()
        Me.Btn_LineEdit = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TB_AmountIncl = New System.Windows.Forms.TextBox()
        Me.TB_ExclAmount = New System.Windows.Forms.TextBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ImportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MyMicrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GAAPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Lbl_PaidAmount = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lbl_Outstanding = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DGV_InvoiceList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_InvoiceDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.DT_EndDate)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DT_StartDate)
        Me.GroupBox1.Controls.Add(Me.Btn_Refresh)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 42)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(337, 150)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Date Selection"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "End Date:"
        '
        'DT_EndDate
        '
        Me.DT_EndDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DT_EndDate.CustomFormat = "dd MMMM yyyy"
        Me.DT_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_EndDate.Location = New System.Drawing.Point(97, 59)
        Me.DT_EndDate.Name = "DT_EndDate"
        Me.DT_EndDate.Size = New System.Drawing.Size(189, 22)
        Me.DT_EndDate.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Start Date:"
        '
        'DT_StartDate
        '
        Me.DT_StartDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DT_StartDate.CustomFormat = "dd MMMM yyyy"
        Me.DT_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_StartDate.Location = New System.Drawing.Point(97, 21)
        Me.DT_StartDate.Name = "DT_StartDate"
        Me.DT_StartDate.Size = New System.Drawing.Size(189, 22)
        Me.DT_StartDate.TabIndex = 1
        '
        'Btn_Refresh
        '
        Me.Btn_Refresh.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Refresh.Location = New System.Drawing.Point(6, 97)
        Me.Btn_Refresh.Name = "Btn_Refresh"
        Me.Btn_Refresh.Size = New System.Drawing.Size(325, 47)
        Me.Btn_Refresh.TabIndex = 0
        Me.Btn_Refresh.Text = "Refresh"
        Me.Btn_Refresh.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSS_Note, Me.TSS_Progress, Me.SS_Note2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 669)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1577, 25)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TSS_Note
        '
        Me.TSS_Note.Name = "TSS_Note"
        Me.TSS_Note.Size = New System.Drawing.Size(15, 20)
        Me.TSS_Note.Text = "-"
        '
        'TSS_Progress
        '
        Me.TSS_Progress.Name = "TSS_Progress"
        Me.TSS_Progress.Size = New System.Drawing.Size(100, 19)
        Me.TSS_Progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'SS_Note2
        '
        Me.SS_Note2.Name = "SS_Note2"
        Me.SS_Note2.Size = New System.Drawing.Size(15, 20)
        Me.SS_Note2.Text = "-"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.lbl_InvRef)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.lbl_SupplierName)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.lbl_InvDate)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(356, 42)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1209, 54)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Items and Actions"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(962, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(241, 25)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Post Invoice"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lbl_InvRef
        '
        Me.lbl_InvRef.AutoSize = True
        Me.lbl_InvRef.Location = New System.Drawing.Point(581, 18)
        Me.lbl_InvRef.Name = "lbl_InvRef"
        Me.lbl_InvRef.Size = New System.Drawing.Size(13, 17)
        Me.lbl_InvRef.TabIndex = 5
        Me.lbl_InvRef.Text = "-"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(484, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 17)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Invoice Ref:"
        '
        'lbl_SupplierName
        '
        Me.lbl_SupplierName.AutoSize = True
        Me.lbl_SupplierName.Location = New System.Drawing.Point(103, 18)
        Me.lbl_SupplierName.Name = "lbl_SupplierName"
        Me.lbl_SupplierName.Size = New System.Drawing.Size(13, 17)
        Me.lbl_SupplierName.TabIndex = 3
        Me.lbl_SupplierName.Text = "-"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(105, 17)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Supplier Name:"
        '
        'lbl_InvDate
        '
        Me.lbl_InvDate.AutoSize = True
        Me.lbl_InvDate.Location = New System.Drawing.Point(407, 18)
        Me.lbl_InvDate.Name = "lbl_InvDate"
        Me.lbl_InvDate.Size = New System.Drawing.Size(13, 17)
        Me.lbl_InvDate.TabIndex = 1
        Me.lbl_InvDate.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(310, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 17)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Invoice Date:"
        '
        'TB_LineDesc
        '
        Me.TB_LineDesc.Location = New System.Drawing.Point(173, 15)
        Me.TB_LineDesc.Name = "TB_LineDesc"
        Me.TB_LineDesc.Size = New System.Drawing.Size(408, 22)
        Me.TB_LineDesc.TabIndex = 15
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(696, 36)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 17)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "VAT Type"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(411, 35)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(132, 17)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Account Category 3"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(217, 35)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(132, 17)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Account Category 2"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(19, 36)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(132, 17)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "Account Category 1"
        '
        'CB_Vat
        '
        Me.CB_Vat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Vat.FormattingEnabled = True
        Me.CB_Vat.Location = New System.Drawing.Point(699, 56)
        Me.CB_Vat.Name = "CB_Vat"
        Me.CB_Vat.Size = New System.Drawing.Size(90, 24)
        Me.CB_Vat.TabIndex = 10
        '
        'CB_Cat2
        '
        Me.CB_Cat2.FormattingEnabled = True
        Me.CB_Cat2.Location = New System.Drawing.Point(220, 55)
        Me.CB_Cat2.Name = "CB_Cat2"
        Me.CB_Cat2.Size = New System.Drawing.Size(188, 24)
        Me.CB_Cat2.TabIndex = 9
        '
        'CB_Cat1
        '
        Me.CB_Cat1.FormattingEnabled = True
        Me.CB_Cat1.Location = New System.Drawing.Point(19, 56)
        Me.CB_Cat1.Name = "CB_Cat1"
        Me.CB_Cat1.Size = New System.Drawing.Size(195, 24)
        Me.CB_Cat1.TabIndex = 8
        '
        'CB_Cat3
        '
        Me.CB_Cat3.FormattingEnabled = True
        Me.CB_Cat3.Location = New System.Drawing.Point(414, 55)
        Me.CB_Cat3.Name = "CB_Cat3"
        Me.CB_Cat3.Size = New System.Drawing.Size(167, 24)
        Me.CB_Cat3.TabIndex = 7
        '
        'DGV_InvoiceList
        '
        Me.DGV_InvoiceList.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.DGV_InvoiceList.AllowUserToAddRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DGV_InvoiceList.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_InvoiceList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.DGV_InvoiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGV_InvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_InvoiceList.Location = New System.Drawing.Point(13, 198)
        Me.DGV_InvoiceList.MultiSelect = false
        Me.DGV_InvoiceList.Name = "DGV_InvoiceList"
        Me.DGV_InvoiceList.RowTemplate.Height = 24
        Me.DGV_InvoiceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_InvoiceList.Size = New System.Drawing.Size(337, 468)
        Me.DGV_InvoiceList.TabIndex = 3
        '
        'DGV_InvoiceDetail
        '
        Me.DGV_InvoiceDetail.AllowUserToAddRows = false
        Me.DGV_InvoiceDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.DGV_InvoiceDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGV_InvoiceDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_InvoiceDetail.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGV_InvoiceDetail.Location = New System.Drawing.Point(356, 198)
        Me.DGV_InvoiceDetail.MultiSelect = false
        Me.DGV_InvoiceDetail.Name = "DGV_InvoiceDetail"
        Me.DGV_InvoiceDetail.RowTemplate.Height = 24
        Me.DGV_InvoiceDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_InvoiceDetail.Size = New System.Drawing.Size(1209, 369)
        Me.DGV_InvoiceDetail.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(1367, 570)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Total Excl:"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = true
        Me.Label7.Location = New System.Drawing.Point(1367, 588)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(75, 17)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Total VAT:"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = true
        Me.Label8.Location = New System.Drawing.Point(1367, 605)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 17)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Total Incl:"
        '
        'Lbl_Total_Incl
        '
        Me.Lbl_Total_Incl.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Lbl_Total_Incl.Location = New System.Drawing.Point(1448, 605)
        Me.Lbl_Total_Incl.Name = "Lbl_Total_Incl"
        Me.Lbl_Total_Incl.Size = New System.Drawing.Size(116, 22)
        Me.Lbl_Total_Incl.TabIndex = 10
        Me.Lbl_Total_Incl.Text = "0"
        Me.Lbl_Total_Incl.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Lbl_Total_Vat
        '
        Me.Lbl_Total_Vat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Lbl_Total_Vat.Location = New System.Drawing.Point(1448, 588)
        Me.Lbl_Total_Vat.Name = "Lbl_Total_Vat"
        Me.Lbl_Total_Vat.Size = New System.Drawing.Size(116, 17)
        Me.Lbl_Total_Vat.TabIndex = 9
        Me.Lbl_Total_Vat.Text = "0"
        Me.Lbl_Total_Vat.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Lbl_Total_Excl
        '
        Me.Lbl_Total_Excl.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Lbl_Total_Excl.Location = New System.Drawing.Point(1451, 570)
        Me.Lbl_Total_Excl.Name = "Lbl_Total_Excl"
        Me.Lbl_Total_Excl.Size = New System.Drawing.Size(114, 17)
        Me.Lbl_Total_Excl.TabIndex = 8
        Me.Lbl_Total_Excl.Text = "0"
        Me.Lbl_Total_Excl.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.TB_PaidAmount)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.DTP_PaidDate)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.CB_PayType)
        Me.GroupBox3.Controls.Add(Me.CB_IsPaid)
        Me.GroupBox3.Location = New System.Drawing.Point(357, 605)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1005, 58)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = false
        Me.GroupBox3.Text = "Summay and Payment"
        '
        'TB_PaidAmount
        '
        Me.TB_PaidAmount.Location = New System.Drawing.Point(627, 27)
        Me.TB_PaidAmount.Name = "TB_PaidAmount"
        Me.TB_PaidAmount.Size = New System.Drawing.Size(100, 22)
        Me.TB_PaidAmount.TabIndex = 5
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(499, 31)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(122, 17)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = ", amount paid was"
        '
        'DTP_PaidDate
        '
        Me.DTP_PaidDate.CustomFormat = "dd MMM yyyy"
        Me.DTP_PaidDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTP_PaidDate.Location = New System.Drawing.Point(367, 27)
        Me.DTP_PaidDate.Name = "DTP_PaidDate"
        Me.DTP_PaidDate.Size = New System.Drawing.Size(127, 22)
        Me.DTP_PaidDate.TabIndex = 3
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(312, 32)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 17)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "on the "
        '
        'CB_PayType
        '
        Me.CB_PayType.FormattingEnabled = true
        Me.CB_PayType.Location = New System.Drawing.Point(185, 26)
        Me.CB_PayType.Name = "CB_PayType"
        Me.CB_PayType.Size = New System.Drawing.Size(121, 24)
        Me.CB_PayType.TabIndex = 1
        '
        'CB_IsPaid
        '
        Me.CB_IsPaid.AutoSize = true
        Me.CB_IsPaid.Location = New System.Drawing.Point(8, 28)
        Me.CB_IsPaid.Name = "CB_IsPaid"
        Me.CB_IsPaid.Size = New System.Drawing.Size(173, 21)
        Me.CB_IsPaid.TabIndex = 0
        Me.CB_IsPaid.Text = "This has been paid via"
        Me.CB_IsPaid.UseVisualStyleBackColor = true
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Location = New System.Drawing.Point(19, 18)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(114, 17)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Line Description:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.TB_TaxAmount)
        Me.GroupBox4.Controls.Add(Me.BTN_AddLine)
        Me.GroupBox4.Controls.Add(Me.Btn_LineEdit)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.TB_AmountIncl)
        Me.GroupBox4.Controls.Add(Me.TB_ExclAmount)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.TB_LineDesc)
        Me.GroupBox4.Controls.Add(Me.CB_Cat3)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.CB_Cat1)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.CB_Cat2)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.CB_Vat)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Location = New System.Drawing.Point(357, 96)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1209, 96)
        Me.GroupBox4.TabIndex = 12
        Me.GroupBox4.TabStop = false
        Me.GroupBox4.Text = "Line Edit Details"
        '
        'Label18
        '
        Me.Label18.AutoSize = true
        Me.Label18.Location = New System.Drawing.Point(790, 35)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(87, 17)
        Me.Label18.TabIndex = 25
        Me.Label18.Text = "Amount Tax:"
        '
        'TB_TaxAmount
        '
        Me.TB_TaxAmount.Location = New System.Drawing.Point(793, 57)
        Me.TB_TaxAmount.Name = "TB_TaxAmount"
        Me.TB_TaxAmount.ReadOnly = true
        Me.TB_TaxAmount.Size = New System.Drawing.Size(103, 22)
        Me.TB_TaxAmount.TabIndex = 24
        '
        'BTN_AddLine
        '
        Me.BTN_AddLine.Location = New System.Drawing.Point(1111, 43)
        Me.BTN_AddLine.Name = "BTN_AddLine"
        Me.BTN_AddLine.Size = New System.Drawing.Size(92, 41)
        Me.BTN_AddLine.TabIndex = 23
        Me.BTN_AddLine.Text = "Add Line"
        Me.BTN_AddLine.UseVisualStyleBackColor = true
        '
        'Btn_LineEdit
        '
        Me.Btn_LineEdit.Location = New System.Drawing.Point(1013, 43)
        Me.Btn_LineEdit.Name = "Btn_LineEdit"
        Me.Btn_LineEdit.Size = New System.Drawing.Size(92, 41)
        Me.Btn_LineEdit.TabIndex = 22
        Me.Btn_LineEdit.Text = "Edit Line"
        Me.Btn_LineEdit.UseVisualStyleBackColor = true
        '
        'Label17
        '
        Me.Label17.AutoSize = true
        Me.Label17.Location = New System.Drawing.Point(587, 36)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(89, 17)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "Amount Excl:"
        '
        'Label16
        '
        Me.Label16.AutoSize = true
        Me.Label16.Location = New System.Drawing.Point(899, 36)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(85, 17)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "Amount Incl:"
        '
        'TB_AmountIncl
        '
        Me.TB_AmountIncl.Location = New System.Drawing.Point(902, 58)
        Me.TB_AmountIncl.Name = "TB_AmountIncl"
        Me.TB_AmountIncl.Size = New System.Drawing.Size(103, 22)
        Me.TB_AmountIncl.TabIndex = 18
        '
        'TB_ExclAmount
        '
        Me.TB_ExclAmount.Location = New System.Drawing.Point(590, 56)
        Me.TB_ExclAmount.Name = "TB_ExclAmount"
        Me.TB_ExclAmount.Size = New System.Drawing.Size(103, 22)
        Me.TB_ExclAmount.TabIndex = 17
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImportToolStripMenuItem, Me.EditItemsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1577, 28)
        Me.MenuStrip1.TabIndex = 13
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ImportToolStripMenuItem
        '
        Me.ImportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MyMicrosToolStripMenuItem, Me.GAAPToolStripMenuItem})
        Me.ImportToolStripMenuItem.Name = "ImportToolStripMenuItem"
        Me.ImportToolStripMenuItem.Size = New System.Drawing.Size(66, 24)
        Me.ImportToolStripMenuItem.Text = "Import"
        '
        'MyMicrosToolStripMenuItem
        '
        Me.MyMicrosToolStripMenuItem.Name = "MyMicrosToolStripMenuItem"
        Me.MyMicrosToolStripMenuItem.Size = New System.Drawing.Size(142, 24)
        Me.MyMicrosToolStripMenuItem.Text = "MyMicros"
        '
        'GAAPToolStripMenuItem
        '
        Me.GAAPToolStripMenuItem.Name = "GAAPToolStripMenuItem"
        Me.GAAPToolStripMenuItem.Size = New System.Drawing.Size(142, 24)
        Me.GAAPToolStripMenuItem.Text = "GAAP"
        '
        'EditItemsToolStripMenuItem
        '
        Me.EditItemsToolStripMenuItem.Name = "EditItemsToolStripMenuItem"
        Me.EditItemsToolStripMenuItem.Size = New System.Drawing.Size(87, 24)
        Me.EditItemsToolStripMenuItem.Text = "Edit Items"
        '
        'Lbl_PaidAmount
        '
        Me.Lbl_PaidAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Lbl_PaidAmount.Location = New System.Drawing.Point(1448, 622)
        Me.Lbl_PaidAmount.Name = "Lbl_PaidAmount"
        Me.Lbl_PaidAmount.Size = New System.Drawing.Size(116, 22)
        Me.Lbl_PaidAmount.TabIndex = 15
        Me.Lbl_PaidAmount.Text = "0"
        Me.Lbl_PaidAmount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = true
        Me.Label20.Location = New System.Drawing.Point(1367, 622)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 17)
        Me.Label20.TabIndex = 14
        Me.Label20.Text = "Paid:"
        '
        'lbl_Outstanding
        '
        Me.lbl_Outstanding.AccessibleDescription = ""
        Me.lbl_Outstanding.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lbl_Outstanding.Location = New System.Drawing.Point(1449, 637)
        Me.lbl_Outstanding.Name = "lbl_Outstanding"
        Me.lbl_Outstanding.Size = New System.Drawing.Size(116, 22)
        Me.lbl_Outstanding.TabIndex = 17
        Me.lbl_Outstanding.Text = "0"
        Me.lbl_Outstanding.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = true
        Me.Label21.Location = New System.Drawing.Point(1368, 637)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(89, 17)
        Me.Label21.TabIndex = 16
        Me.Label21.Text = "Outstanding:"
        '
        'frmInvoiceIntegrator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(1577, 694)
        Me.Controls.Add(Me.lbl_Outstanding)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Lbl_PaidAmount)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Lbl_Total_Incl)
        Me.Controls.Add(Me.Lbl_Total_Vat)
        Me.Controls.Add(Me.Lbl_Total_Excl)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DGV_InvoiceDetail)
        Me.Controls.Add(Me.DGV_InvoiceList)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmInvoiceIntegrator"
        Me.Text = "Invoice Integrator"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.StatusStrip1.ResumeLayout(false)
        Me.StatusStrip1.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        CType(Me.DGV_InvoiceList,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DGV_InvoiceDetail,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox3.ResumeLayout(false)
        Me.GroupBox3.PerformLayout
        Me.GroupBox4.ResumeLayout(false)
        Me.GroupBox4.PerformLayout
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents TSS_Note As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSS_Progress As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DT_EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DT_StartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Btn_Refresh As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DGV_InvoiceList As System.Windows.Forms.DataGridView
    Friend WithEvents DGV_InvoiceDetail As System.Windows.Forms.DataGridView
    Friend WithEvents SS_Note2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lbl_SupplierName As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lbl_InvDate As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbl_InvRef As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TB_LineDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents CB_Vat As System.Windows.Forms.ComboBox
    Friend WithEvents CB_Cat2 As System.Windows.Forms.ComboBox
    Friend WithEvents CB_Cat1 As System.Windows.Forms.ComboBox
    Friend WithEvents CB_Cat3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Lbl_Total_Incl As System.Windows.Forms.Label
    Friend WithEvents Lbl_Total_Vat As System.Windows.Forms.Label
    Friend WithEvents Lbl_Total_Excl As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TB_PaidAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents DTP_PaidDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents CB_PayType As System.Windows.Forms.ComboBox
    Friend WithEvents CB_IsPaid As System.Windows.Forms.CheckBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TB_AmountIncl As System.Windows.Forms.TextBox
    Friend WithEvents TB_ExclAmount As System.Windows.Forms.TextBox
    Friend WithEvents Btn_LineEdit As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents BTN_AddLine As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ImportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MyMicrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GAAPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TB_TaxAmount As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_PaidAmount As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lbl_Outstanding As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
End Class
