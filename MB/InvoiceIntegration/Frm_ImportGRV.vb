﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Windows.Forms
Imports System.IO

Public Class Frm_ImportGRV
    Dim ImportType As String
    Dim MicrosFileName As String
    Dim GAAPcsvPath As String
    Dim GAAPcsvFileName As String
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Btn_Close.Click
        Me.Close()
    End Sub

    Private Sub Btn_Browse_Click(sender As Object, e As EventArgs) Handles Btn_Browse.Click
        If LBL_ImportType.Text = "Micros" Then
            Micros.Filter = "xlsx (*.xlsx)|*.xlsx"
            If Micros.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                PB_Import.Visible = True
                PB_Import.MarqueeAnimationSpeed = 30
                ImportType = "Micros"
                MicrosFileName = Micros.FileName
                BGW_Import.RunWorkerAsync()
                Btn_Browse.Enabled = False
            Else
                MsgBox("No file selected")
            End If
        ElseIf LBL_ImportType.Text = "GAAP" Then
            Micros.Filter = "csv (*.csv)|*.csv"
            If Micros.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                PB_Import.Visible = True
                PB_Import.MarqueeAnimationSpeed = 30
                GAAPcsvPath = Path.GetDirectoryName(Micros.FileName)
                GAAPcsvFileName = Path.GetFileName(Micros.FileName)
                BGW_Import.RunWorkerAsync()
                Btn_Browse.Enabled = False
            Else
                MsgBox("No file selected")
            End If
            End If
    End Sub

    Sub ImportMicros()
        'Import the excel file into a datatable
        Dim dtSheet1 As New DataTable
        Using cn As New System.Data.OleDb.OleDbConnection
            Dim Builder As New OleDbConnectionStringBuilder With _
                { _
                    .DataSource = MicrosFileName, _
                    .Provider = "Microsoft.ACE.OLEDB.12.0" _
                }
            Builder.Add("Extended Properties", "Excel 12.0; IMEX=1;HDR=Yes;")
            cn.ConnectionString = Builder.ConnectionString

            cn.Open()

            Using cmd As OleDbCommand = New OleDbCommand With {.Connection = cn}
                cmd.CommandText = "SELECT * FROM [Report$]"
                Dim dr As System.Data.IDataReader = cmd.ExecuteReader
                dtSheet1.Load(dr)
            End Using
        End Using

        'Transform dtSheet1 into correct format
        Dim dsTransform As New DataTable
        dsTransform.Columns.Add("SupplierName")
        dsTransform.Columns.Add("InvoiceNo")
        dsTransform.Columns.Add("InvDate", System.Type.GetType("System.DateTime"))
        dsTransform.Columns.Add("CostGroup")
        dsTransform.Columns.Add("ItemName")
        dsTransform.Columns.Add("Qty")
        dsTransform.Columns.Add("Price")
        dsTransform.Columns.Add("Total")
        dsTransform.Columns.Add("Co_ID")
        dsTransform.Columns.Add("Paid")
        dsTransform.Columns.Add("PaidAmount")

        Dim provider As New CultureInfo("en-US")
        For Each rw As DataRow In dtSheet1.Rows
            Dim dsTransRw As DataRow = dsTransform.NewRow
            dsTransRw("SupplierName") = rw.Item("SupplierName").ToString
            dsTransRw("InvoiceNo") = rw.Item("InvoiceNo")
            dsTransRw("InvDate") = DateTime.ParseExact(rw.Item("Invoice Date"), "dd-MM-yyyy", provider)
            If rw.Item("VATType") = 1 Then
                dsTransRw("CostGroup") = "Y-" & rw.Item("ItemGroup")
                dsTransRw("ItemName") = "Y-" & rw.Item("ItemGroup")
            Else
                dsTransRw("CostGroup") = "N-" & rw.Item("ItemGroup")
                dsTransRw("ItemName") = "N-" & rw.Item("ItemGroup")
            End If
            dsTransRw("Qty") = "1"
            dsTransRw("Price") = rw.Item("Excl Amount")
            dsTransRw("Total") = rw.Item("Excl Amount")
            dsTransRw("Co_ID") = My.Settings.Setting_CompanyID
            dsTransRw("Paid") = rw.Item("Paid")
            dsTransRw("PaidAmount") = rw.Item("PayAmount")
            dsTransform.Rows.Add(dsTransRw)
        Next

        Dim constr As String = My.Settings.CS_Setting & ";Connect Timeout=180"
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("Update_GRVDetails")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@tblGRVs", dsTransform)
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub
    Sub ImportGAAP()
        'Run Micros import

        'Import the excel file into a datatable
        Dim dtSheet1 As New DataTable
        Using cn As New System.Data.OleDb.OleDbConnection
            Dim Builder As New OleDbConnectionStringBuilder With _
                { _
                    .DataSource = GAAPcsvPath, _
                    .Provider = "Microsoft.ACE.OLEDB.12.0" _
                }
            Builder.Add("Extended Properties", "text;HDR=Yes;FMT=Delimited")
            cn.ConnectionString = Builder.ConnectionString

            cn.Open()

            Using cmd As OleDbCommand = New OleDbCommand With {.Connection = cn}
                cmd.CommandText = "SELECT * FROM [" & GAAPcsvFileName & "]"
                Dim dr As System.Data.IDataReader = cmd.ExecuteReader
                dtSheet1.Load(dr)
            End Using
        End Using

        'Transform dtSheet1 into correct format
        Dim dsTransform As New DataTable
        dsTransform.Columns.Add("SupplierName")
        dsTransform.Columns.Add("InvoiceNo")
        dsTransform.Columns.Add("InvDate", System.Type.GetType("System.DateTime"))
        dsTransform.Columns.Add("CostGroup")
        dsTransform.Columns.Add("ItemName")
        dsTransform.Columns.Add("Qty")
        dsTransform.Columns.Add("Price")
        dsTransform.Columns.Add("Total")
        dsTransform.Columns.Add("Co_ID")
        dsTransform.Columns.Add("Paid")
        dsTransform.Columns.Add("PaidAmount")

        Dim provider As New CultureInfo("en-US")
        For Each rw As DataRow In dtSheet1.Rows
            Dim dsTransRw As DataRow = dsTransform.NewRow
            dsTransRw("SupplierName") = rw.Item("SupplierName").ToString
            dsTransRw("InvoiceNo") = rw.Item("SupplierRef")
            dsTransRw("InvDate") = rw.Item("InvoiceDate")
            If IsDBNull(rw.Item("DeptName")) Then
                dsTransRw("CostGroup") = rw.Item("Tax") & " - Dept Not Set"
            Else
                dsTransRw("CostGroup") = rw.Item("Tax") & " - " & rw.Item("DeptName")
            End If
            dsTransRw("ItemName") = rw.Item("ItemName")
            dsTransRw("Qty") = rw.Item("Qty")
            dsTransRw("Price") = rw.Item("ValueEx")
            dsTransRw("Total") = rw.Item("ValueEx")
            dsTransRw("Co_ID") = My.Settings.Setting_CompanyID
            dsTransRw("Paid") = "False"
            dsTransRw("PaidAmount") = "0"
            dsTransform.Rows.Add(dsTransRw)
        Next

        Dim constr As String = My.Settings.CS_Setting & ";Connect Timeout=180"
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("Update_GRVDetails")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@tblGRVs", dsTransform)
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

    End Sub

    Private Sub BGW_Import_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BGW_Import.DoWork
        If ImportType = "Micros" Then
            ImportMicros()
        Else
            ImportGAAP()
        End If
    End Sub

    Private Sub Frm_ImportGRV_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Icon = My.Resources.AppIcon
        Me.Text = "MagicBox -> " & CStr(My.Settings.CurrentCompany) & " -> Invoice Integrator -> Version : " & My.Settings.Setting_Version
        PB_Import.MarqueeAnimationSpeed = 0
    End Sub
End Class