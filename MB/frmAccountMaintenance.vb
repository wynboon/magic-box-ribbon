﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class frmAccountMaintenance

#Region "Variables"
    Dim MBData As New DataExtractsClass
    Dim BankAccNumber As String = ""
    Dim oErrorOpeningAccount As Boolean
#End Region

#Region "Methods"

    Private Sub ClearControls()

        txtCode1.Text = ""
        txtDesc1.Text = ""
        txtCode2.Text = ""
        txtDesc2.Text = ""
        txtCode3.Text = ""
        txtDesc3.Text = ""

        txtAccount.Text = ""
        txtAccountDescription.Text = ""

        rdbIncomeStatement.Checked = False
        rdbBalanceSheet.Checked = False
        cmbFinancialCategory.SelectedIndex = -1

        txtReportingCategory.Text = ""
        txtDisplayName.Text = ""
        txtType.Text = ""
        txtBankAccNo.Text = ""
        txtBankBranchCode.Text = ""
        CB_BankLayout.Text = ""

        lblSegment4CountInTransactions.Text = ""

    End Sub

#End Region

    Private Sub AccountMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Account Maintanance -> Version : " & My.Settings.Setting_Version
        'Test security
        LockForm(Me)

        Call Fill_ListBox1()
        Me.rdbIncomeStatement.Checked = True

    End Sub

    Sub Fill_ListBox1()
        Try
            Dim sSQL As String
            Dim oSegment1 As String
            Dim oSegment1Desc As String
            Me.ListBox1.Items.Clear()

            sSQL = "SELECT DISTINCT [Segment 1],[Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            Call Fill_DataGridView1(sSQL)

            For i As Integer = 0 To DataGridView1.RowCount - 2
                Me.ListBox1.Items.Add(Me.DataGridView1.Rows(i).Cells(0).Value.ToString & "-" & Me.DataGridView1.Rows(i).Cells(1).Value.ToString)
            Next

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = False
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Sub Fill_DataGridView1(ByVal sSQL As String)
        Try

            Dim AA As OleDbDataAdapter
            Dim AT As New DataTable
            Dim SA As SqlDataAdapter
            Dim ST As New DataTable

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            SA = New SqlDataAdapter(sSQL, connection)
            SA.Fill(ST)
            Me.DataGridView1.DataSource = ST

        Catch ex As Exception
            MsgBox(ex.Message & " 071")
        End Try
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListBox1.SelectedIndexChanged

        Try
            If ListBox1.SelectedItems.Count < 1 Then Exit Sub 'To stop error on nothing selected "Object Reference not set to an instance of an object!"

            Me.ListBox3.Items.Clear()
            Dim arrSplit As Object = Split(Me.ListBox1.SelectedItem.ToString, "-")
            Call Fill_ListBox2(arrSplit(0))
        Catch ex As Exception
            MsgBox(ex.Message & " 072")
        End Try

    End Sub


    Sub Fill_ListBox2(ByVal oSegment1 As String)
        Try
            Dim sSQL As String
            Dim oSegment2 As String
            Dim oSegment2Desc As String

            sSQL = "SELECT DISTINCT [Segment 2],[Segment 2 Desc] FROM Accounting Where [Segment 1] = '" & oSegment1 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            Me.ListBox2.Items.Clear()

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader


                While datareader.Read

                    If Not datareader("Segment 2").Equals(DBNull.Value) Then
                        oSegment2 = datareader("Segment 2")
                        oSegment2Desc = datareader("Segment 2 Desc")
                        Me.ListBox2.Items.Add(oSegment2 & "-" & oSegment2Desc)
                    End If

                End While

                connection.Close()

            Else
                Call Fill_DataGridView1(sSQL)

                For i As Integer = 0 To DataGridView1.RowCount - 2
                    Me.ListBox2.Items.Add(Me.DataGridView1.Rows(i).Cells(0).Value.ToString & "-" & Me.DataGridView1.Rows(i).Cells(1).Value.ToString)
                Next

                'Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                'Dim cmd As New SqlCommand(sSQL, connection)
                'connection.Open()
                'Dim datareader As SqlDataReader = cmd.ExecuteReader
                ''While datareader.Read
                'If Not IsDBNull(datareader("Segment 2")) Then
                'oSegment2 = datareader("Segment 2")
                'oSegment2Desc = datareader("Segment 1 Desc")
                'Me.ListBox2.Items.Add(oSegment2 & "-" & oSegment2Desc)
                'End If
                'End While
                'connection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 072")
        End Try

    End Sub
    Private Sub ListBox2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        Try
            If ListBox2.SelectedItems.Count < 1 And ListBox1.SelectedItems.Count < 1 Then Exit Sub 'To stop error on nothing selected "Object Reference not set to an instance of an object!"

            Dim arrSplit As Object = Split(Me.ListBox1.SelectedItem.ToString, "-")
            Dim arrSplit2 As Object = Split(Me.ListBox2.SelectedItem.ToString, "-")
            Call Fill_ListBox3(arrSplit(0), arrSplit2(0))
        Catch ex As Exception
            MsgBox(ex.Message & " 073")
        End Try
    End Sub
    Sub Fill_ListBox3(ByVal oSegment1 As String, ByVal oSegment2 As String)
        Try
            Dim sSQL As String
            Dim oSegment3 As String
            Dim oSegment3Desc As String

            sSQL = "SELECT DISTINCT [Segment 3],[Segment 3 Desc] FROM Accounting Where [Segment 1] = '" & oSegment1 & "' And [Segment 2] = '" & oSegment2 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            Me.ListBox3.Items.Clear()
            Call Fill_DataGridView1(sSQL)

            For i As Integer = 0 To DataGridView1.RowCount - 2
                Me.ListBox3.Items.Add(Me.DataGridView1.Rows(i).Cells(0).Value.ToString & "-" & Me.DataGridView1.Rows(i).Cells(1).Value.ToString)
            Next


            'NOTE - MUST USE THIS FORMAT FOR SEGMENT 3
            'Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            'Dim cmd As New SqlCommand(sSQL, connection)
            'connection.Open()
            ' Dim datareader As SqlDataReader = cmd.ExecuteReader
            'While datareader.Read
            'If Not IsDBNull(datareader("Segment 3")) Then
            'oSegment3 = datareader("Segment 3")
            'oSegment3Desc = datareader("Segment 3 Desc")
            'Me.ListBox3.Items.Add(oSegment3 & "-" & oSegment3Desc)
            'End If
            'End While
            'connection.Close()

        Catch ex As Exception
            MsgBox(ex.Message & " 074")
        End Try

    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        Try
            Me.txtCode1.Text = ""
            Me.txtDesc1.Text = ""
            Me.txtCode2.Text = ""
            Me.txtDesc2.Text = ""
            Me.txtCode3.Text = ""
            Me.txtDesc3.Text = ""

            Dim Code1 As String
            Dim Desc1 As String
            Dim Code2 As String
            Dim Desc2 As String
            Dim arrSplit As Object
            Dim arrSplit2 As Object

            If Me.ListBox1.SelectedItems.Count > 0 Then
                arrSplit = Split(Me.ListBox1.SelectedItem.ToString, "-")
                Code1 = arrSplit(0)
                Desc1 = arrSplit(1)
                Me.txtCode1.Text = Code1
                Me.txtDesc1.Text = Desc1
            End If

            If Me.ListBox2.SelectedItems.Count > 0 Then
                arrSplit2 = Split(Me.ListBox2.SelectedItem.ToString, "-")
                Code2 = arrSplit2(0)
                Desc2 = arrSplit2(1)
                Me.txtCode2.Text = Code2
                Me.txtDesc2.Text = Desc2
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 075")
        End Try
    End Sub
    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Try


            If cmbFinancialCategory.Text.Trim = "Bank" Then
                If txtBankAccNo.Text.Trim = "" And Not IsNumeric(txtBankAccNo.Text.Trim) Then
                    MessageBox.Show("Please supply the Bank account number in a correct format", "Legder Account", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtBankAccNo.Focus()
                    Exit Sub
                End If
            End If

            If Me.txtCode1.Text = "" Or Me.txtCode2.Text = "" Or Me.txtCode3.Text = "" _
                Or Me.txtDesc1.Text = "" Or Me.txtDesc2.Text = "" Or Me.txtDesc3.Text = "" Or Me.cmbFinancialCategory.Text = "" Then
                MsgBox("Please at least complete all six boxes on the right as well as Finacial Category!")
                Exit Sub
            Else
                Call oInsertAccount()
                ckbIsNew.Checked = False
                AccountMaintenance_Load(sender, e)
                ListBox2.Items.Clear()
                ListBox3.Items.Clear()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 076")
        End Try
    End Sub
    Sub oInsertAccount()

        Try

            Dim ra As Integer

            Dim oCode1 As String = Me.txtCode1.Text
            Dim oDesc1 As String = Me.txtDesc1.Text
            Dim oCode2 As String = Me.txtCode2.Text
            Dim oDesc2 As String = Me.txtDesc2.Text
            Dim oCode3 As String = Me.txtCode3.Text
            Dim oDesc3 As String = Me.txtDesc3.Text
            Dim oSegment4 As String = oCode1 & oCode2 & oCode3

            If Account_There(oSegment4) = True Then
                MsgBox("Account cannot be created. That account/code combination already exists!")
                Exit Sub
            End If

            Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####

            Dim sSQL As String
            sSQL = "Insert Into Accounting ([Segment 1],[Segment 1 Desc],[Segment 2],[Segment 2 Desc],[Segment 3],[Segment 3 Desc],[Segment 4]"
            sSQL = sSQL & " ,[Display Name], [Report Category],[FinCatCode], BankAccountNo"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & ", Co_ID"
            End If
            If BankAccNumber <> "" Then
                sSQL = sSQL & ", BankAccountNo"
            End If
            sSQL = sSQL & " ) "
            sSQL = sSQL & " VALUES ('" & oCode1 & "', '" & oDesc1 & "', '" & oCode2 & "', '" & oDesc2 & "', '" & oCode3 & "', '" & oDesc3 & "', '" & oSegment4 & "', "
            sSQL = sSQL & "'" & Me.txtDisplayName.Text & "', " & "'" & Me.txtReportingCategory.Text & "', "
            sSQL = sSQL & "'" & Get_FinCode_from_FinDesc(Me.cmbFinancialCategory.Text) & "', '" & txtBankAccNo.Text & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & ", " & oCompanyID
            End If
            If BankAccNumber <> "" Then '##### NEW #####
                sSQL = sSQL & ", '" & BankAccNumber & "'"
            End If
            sSQL = sSQL & ") "

            If My.Settings.DBType = "SQL" Then

                Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                myConnection.Open()
                Dim myCommand As SqlCommand
                myCommand = New SqlCommand(sSQL, myConnection)
                myCommand.CommandTimeout = 300
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                MessageBox.Show("New account succesfully created", "Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information)
                myConnection.Close()

            End If

            Me.ListBox1.SelectedItem = oCode1
            Call Fill_ListBox2(oCode1)
            Me.ListBox2.SelectedItem = oCode2
            Call Fill_ListBox3(oCode1, oCode2)
            Me.ListBox3.SelectedItem = oCode3
        Catch ex As Exception
            MsgBox(ex.Message & " 077b")
        End Try
    End Sub

    Function Account_There(ByVal oSegment4 As String) As Boolean

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Accounting WHERE [Segment 4]='" & oSegment4 & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Account_There = True
                Else
                    Account_There = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Accounting WHERE [Segment 4]='" & oSegment4 & "'"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Account_There = True
                Else
                    Account_There = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error checking an invoice number against a Supplier! " & ex.Message & ".")
        End Try
    End Function
    Function Other_Account_There_With_This_Segment4(ByVal oSegment4 As String, ByVal oID As String) As Boolean

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Accounting WHERE [Segment 4]='" & oSegment4 & "' And ID <> " & oID
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Other_Account_There_With_This_Segment4 = True
                Else
                    Other_Account_There_With_This_Segment4 = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Accounting WHERE [Segment 4]='" & oSegment4 & "' And ID <> " & oID
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Other_Account_There_With_This_Segment4 = True
                Else
                    Other_Account_There_With_This_Segment4 = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error checking an invoice number against a Supplier! " & ex.Message & ".")
        End Try
    End Function
    Private Sub ListBox3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListBox3.SelectedIndexChanged

        Try

            If Me.ListBox1.SelectedItems.Count < 1 Then Exit Sub
            If Me.ListBox2.SelectedItems.Count < 1 Then Exit Sub
            If Me.ListBox3.SelectedItems.Count < 1 Then Exit Sub

            Dim arrSplit As Object = Split(Me.ListBox1.SelectedItem.ToString, "-")
            Dim arrSplit2 As Object = Split(Me.ListBox2.SelectedItem.ToString, "-")
            Dim arrSplit3 As Object = Split(Me.ListBox3.SelectedItem.ToString, "-")

            Dim oSegment4 As String = arrSplit(0) & arrSplit2(0) & arrSplit3(0)

            Me.lblSegment4CountInTransactions.Text = oCount_Of_Segment4_in_Transactions(oSegment4)

            If IsNumeric(lblSegment4CountInTransactions.Text) Then
                If CInt(lblSegment4CountInTransactions.Text) <> 0 Then

                    If ckbIsNew.Checked = False Then
                        txtCode1.ReadOnly = True
                        txtCode2.ReadOnly = True
                        txtCode3.ReadOnly = True
                    End If

                End If
            End If

            Me.txtAccountDescription.Text = arrSplit3(1)
            Me.txtAccount.Text = oSegment4
            'Get the Accounting ID number to be used for Updates and Deletion

            oErrorOpeningAccount = False
            Me.lblID.Text = Get_ID_from_Segment4(oSegment4)

            If oErrorOpeningAccount = True Then Exit Sub

            'Report Category - just look up in Accounting using Segment4 and load to text box 
            Me.txtReportingCategory.Text = Get_ReportCategory(oSegment4)
            'same for Display Name and Type
            Me.txtDisplayName.Text = Get_DisplayName(oSegment4)
            Me.txtType.Text = Get_Type(oSegment4)
            Me.txtBankAccNo.Text = BankAccNo(oSegment4)
            Me.txtBankBranchCode.Text = BankBranch(oSegment4)
            CB_BankLayout.Text = BankLayout(oSegment4)

            Me.txtDesc1.Text = Get_Segment1Desc(oSegment4)
            Me.txtDesc2.Text = Get_Segment2Desc(oSegment4)
            Me.txtDesc3.Text = Get_Segment3Desc(oSegment4)

            Me.txtCode1.Text = Mid(oSegment4, 1, 2)
            Me.txtCode2.Text = Mid(oSegment4, 3, 2)
            Me.txtCode3.Text = Mid(oSegment4, 5, 2)
            TB_GLAcc.Text = Get_GLAcc(oSegment4)
            Dim oFinCatCode As String = Get_FinCatCode(oSegment4)

            'Now select the Income Statement or Balance Sheet radio button based on FinCatCode
            'Income statement is >= 85 (FinCatCode)
            'Balance Sheet is < 85 and <> 15
            Me.rdbBalanceSheet.Checked = False 'do this so refreshes
            Me.rdbIncomeStatement.Checked = False 'd this so refreshes

            If oFinCatCode = "" Then
                MsgBox("No financial category has been set up for this account!")
                Exit Sub
            End If


            If oFinCatCode < 85 And oFinCatCode <> 15 Then
                Me.rdbBalanceSheet.Checked = True
            Else
                Me.rdbIncomeStatement.Checked = True
            End If

            If oFinCatCode = "" Then
                MsgBox("No financial category has been setup for this account. Please ensure this is done.")
            Else
                Me.cmbFinancialCategory.SelectedItem = Get_FinDesc_from_Financial_Categories(oFinCatCode)
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Function Get_FinDesc_from_Financial_Categories(ByVal oFinCode As String) As String
        Try
            Dim sSQL As String

            sSQL = "Select FinDesc from FinancialCategories Where FinCode = " & oFinCode

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Get_FinDesc_from_Financial_Categories = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("Error looking up FinCode " & oFinCode & "in FinancialCategories table " & ex.Message & " Error code 078")
        End Try

    End Function

    Function Get_FinCatCode(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select FinCatCode from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_FinCatCode = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_FinCatCode = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the FinCatCode in Accounting!")
        End Try
    End Function

    Function Get_ReportCategory(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [Report Category] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_ReportCategory = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_ReportCategory = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function Get_GLAcc(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [GL ACCOUNT] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_GLAcc = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_GLAcc = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function Get_DisplayName(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [Display Name] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_DisplayName = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_DisplayName = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function Get_Type(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [Type] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Type = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Type = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    
    Function Get_Segment1Desc(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [Segment 1 Desc] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment1Desc = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment1Desc = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Segment 1 Desc' in Accounting! " & ex.Message)
        End Try
    End Function

    Function Get_Segment2Desc(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [Segment 2 Desc] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment2Desc = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment2Desc = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Segment 1 Desc' in Accounting! " & ex.Message)
        End Try
    End Function
    Function Get_Segment3Desc(ByVal oSegment4 As String) As String

        Try

            Dim sSQL As String

            sSQL = "Select [Segment 3 Desc] from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment3Desc = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment3Desc = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Segment 1 Desc' in Accounting! " & ex.Message)
        End Try
    End Function
    Function Get_ID_from_Segment4(ByVal oSegment4 As String) As Integer

        Try

            Dim AccountID As Integer = Nothing
            Dim sSQL As String

            sSQL = "Select ID from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            AccountID = cmd.ExecuteScalar().ToString
            connection.Close()

            Return AccountID

        Catch ex As Exception
            MessageBox.Show("An error occured with details  :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            oErrorOpeningAccount = True
        End Try
    End Function



    Sub Fill_FinancialCategories()
        Try
            Dim sSQL As String
            Dim oSegment1 As String
            Dim oSegment1Desc As String

            'Income statement is >= 85 (FinCatCode)
            ' Balance Sheet is < 85 and <> 15
            If Me.rdbIncomeStatement.Checked = True Then
                sSQL = "SELECT DISTINCT FinDesc FROM FinancialCategories Where Fincode >= 85"
            Else
                sSQL = "SELECT DISTINCT FinDesc FROM FinancialCategories Where Fincode < 85 And FinCode <> 15"
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.cmbFinancialCategory.Text = ""
                Me.cmbFinancialCategory.Items.Clear()
                While datareader.Read

                    If Not datareader("FinDesc").Equals(DBNull.Value) Then
                        Me.cmbFinancialCategory.Items.Add(datareader("FinDesc"))
                    End If

                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                Me.cmbFinancialCategory.Text = ""
                Me.cmbFinancialCategory.Items.Clear()
                While datareader.Read

                    If Not datareader("FinDesc").Equals(DBNull.Value) Then
                        Me.cmbFinancialCategory.Items.Add(datareader("FinDesc"))
                    End If

                End While

                connection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 079")
        End Try

    End Sub

    Private Sub rdbIncomeStatement_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbIncomeStatement.CheckedChanged
        Fill_FinancialCategories()
    End Sub

    Private Sub rdbBalanceSheet_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbBalanceSheet.CheckedChanged
        Fill_FinancialCategories()
    End Sub

    Private Sub btnUpdateChanges_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateChanges.Click
        Try

            If cmbFinancialCategory.Text.Trim = "Bank" Then
                If txtBankAccNo.Text.Trim = "" And Not IsNumeric(txtBankAccNo.Text.Trim) Then
                    MessageBox.Show("Please supply the Bank account number in a correct format", "Legder Account", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtBankAccNo.Focus()
                    Exit Sub
                End If
            End If

            Dim oCountOfTransactionsWithSegment4In
            If IsNumeric(Me.lblSegment4CountInTransactions.Text) = True Then
                oCountOfTransactionsWithSegment4In = CInt(Me.lblSegment4CountInTransactions.Text)
                If oCountOfTransactionsWithSegment4In > 0 Then
                    Dim UserOption As DialogResult = _
                    MessageBox.Show("There are transactions against this code combination in the database, are you sure you want to change the descriptions?", "Descriptions Update confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    If UserOption = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If
            End If

            Dim ra As Integer

            Dim oCode1 As String = Me.txtCode1.Text
            Dim oDesc1 As String = Me.txtDesc1.Text
            Dim oCode2 As String = Me.txtCode2.Text
            Dim oDesc2 As String = Me.txtDesc2.Text
            Dim oCode3 As String = Me.txtCode3.Text
            Dim oDesc3 As String = Me.txtDesc3.Text
            Dim oSegment4 As String = oCode1 & oCode2 & oCode3
            Dim BankLayout As String = CB_BankLayout.Text

            If Other_Account_There_With_This_Segment4(oSegment4, Me.lblID.Text) Then
                MsgBox("These changes cannot be made because there is already an account with another ID that has that Category Code Combination(Segment 4)!")
                Exit Sub
            End If

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String
            sSQL = "Update Accounting Set " & _
            "[Segment 1] = '" & oCode1 & "', " & _
            "[Segment 1 Desc] = '" & oDesc1 & "', " & _
            "[Segment 2] = '" & oCode2 & "', " & _
            "[Segment 2 Desc] = '" & oDesc2 & "', " & _
            "[Segment 3] = '" & oCode3 & "', " & _
            "[Segment 3 Desc] = '" & oDesc3 & "', " & _
            "[Display Name] = '" & Me.txtDisplayName.Text & "', " & _
            "[GL ACCOUNT] = '" & Me.TB_GLAcc.Text & "', " & _
            "[Type] = '" & Me.txtType.Text & "', " & _
            "[Report Category] = '" & Me.txtReportingCategory.Text & "', " & _
            "[FinCatCode] = '" & Get_FinCode_from_FinDesc(Me.cmbFinancialCategory.Text) & "', " & _
            "BankAccountNo= '" & txtBankAccNo.Text & "', " & _
            "BranchCode= '" & txtBankBranchCode.Text & "', " & _
            "BankLayout= '" & CB_BankLayout.Text & "' " & _
            "Where ID = " & Me.lblID.Text
            If My.Settings.Setting_CompanyID <> "" Then
                sSQL &= " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            myConnection.Open()
            Dim myCommand As SqlCommand
            myCommand = New SqlCommand(sSQL, myConnection)
            myCommand.CommandTimeout = 300
            ra = myCommand.ExecuteNonQuery()
            'Since no value is returned we use ExecuteNonQuery
            MsgBox("Records Updated " & ra)
            myConnection.Close()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

            AccountMaintenance_Load(sender, e)
            ListBox2.Items.Clear()
            ListBox3.Items.Clear()
            ClearControls()

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message & ". Please report to system support", "Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try

    End Sub

    Function Get_FinCode_from_FinDesc(ByVal oFinDesc As String) As String

        Try

            Dim sSQL As String
            sSQL = "Select FinCode From FinancialCategories Where FinDesc = '" & oFinDesc & "'"

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_FinCode_from_FinDesc = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the FinCatCode in Accounting!")
        End Try
    End Function

    Private Sub btnDeleteAccount_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteAccount.Click
        Try

            Dim CodeCombination As String = CStr(txtCode1.Text & txtCode2.Text & txtCode3.Text)
            If oCountCashBookTransactions(CodeCombination) <> 0 Then
                MessageBox.Show("This account cannot be deleted, there are cashbook transactions againts it. Report to the System Admin", "Delete Account", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Dim oCountOfTransactionsWithSegment4In
            If IsNumeric(Me.lblSegment4CountInTransactions.Text) = True Then
                oCountOfTransactionsWithSegment4In = CInt(Me.lblSegment4CountInTransactions.Text)
                If oCountOfTransactionsWithSegment4In > 0 Then
                    Dim UserOption As DialogResult = _
                        MessageBox.Show("The account cannot be deleted as there are transactions related to the Account in the database.", "Account editing", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub
                End If
            End If

            Dim ra As Integer

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim sSQL As String = "Delete From [Accounting] where [ID] = " & Me.lblID.Text


            Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            myConnection.Open()
            Dim myCommand As SqlCommand
            myCommand = New SqlCommand(sSQL, myConnection)
            myCommand.CommandTimeout = 300
            ra = myCommand.ExecuteNonQuery()



            MessageBox.Show("Selected account(s) successfully deleted", "Delete Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information)
            myConnection.Close()

            AccountMaintenance_Load(sender, e)
            ListBox2.Items.Clear()
            ListBox3.Items.Clear()
            ClearControls()

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnMakeAPayout_Click(sender As System.Object, e As System.EventArgs) Handles btnMakeAPayout.Click
        Try
            Dim ra As Integer

            Dim sSQL As String
            sSQL = "Update Accounting Set [Type] = 'Payout' Where ID = " & Me.lblID.Text

            If My.Settings.DBType = "Access" Then

                Dim myConnection As New OleDbConnection(My.Settings.CS_Setting)
                myConnection.Open()
                Dim myCommand As OleDbCommand
                myCommand = New OleDbCommand(sSQL, myConnection)
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                MsgBox("Records Updated " & ra)
                myConnection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                myConnection.Open()
                Dim myCommand As SqlCommand
                myCommand = New SqlCommand(sSQL, myConnection)
                myCommand.CommandTimeout = 300
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                MsgBox("Records Updated " & ra)
                myConnection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 082")
        End Try
    End Sub

    Function oCount_Of_Segment4_in_Transactions(ByVal oSegment4 As String) As String

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) FROM Transactions WHERE [Description Code] = '" & oSegment4 & "'"
                If My.Settings.Setting_CompanyID <> "" Then
                    sSQL &= " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                oCount_Of_Segment4_in_Transactions = CStr(oCount)

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error checking an invoice number against a Supplier! " & ex.Message & ".")
        End Try
    End Function
    Function oCountCashBookTransactions(ByVal oSegment4 As String) As String

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "SELECT Count(*) FROM Transactions WHERE [Description Code] = '" & oSegment4 & "' AND DocType = 'Cashbook'"
                If My.Settings.Setting_CompanyID <> "" Then
                    sSQL &= " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar)
                oCountCashBookTransactions = CStr(oCount)

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If

            End If
        Catch ex As Exception
            MsgBox("There was an error checking an invoice number against a Supplier! " & ex.Message & ".")
        End Try
    End Function
    Private Sub btnMigrateTransactions_Click(sender As Object, e As EventArgs) Handles btnMigrateTransactions.Click
        Dim frmMigrateTransactions As New frmMigrateAccountTransactions
        frmMigrateTransactions.ShowDialog()
    End Sub

    Private Sub ckbIsNew_CheckedChanged_1(sender As Object, e As EventArgs) Handles ckbIsNew.CheckedChanged
        If ckbIsNew.Checked = True Then
            btnAdd.Enabled = True
            btnUpdateChanges.Enabled = False
            btnMakeAPayout.Enabled = False
            Btn_MakeTender.Enabled = False
            btnDeleteAccount.Enabled = False
            btnMigrateTransactions.Enabled = False
            txtCode1.ReadOnly = False
            txtCode2.ReadOnly = False
            txtCode3.ReadOnly = False
            txtCode1.Enabled = True
            txtCode2.Enabled = True
            txtCode3.Enabled = True

        Else
            btnUpdateChanges.Enabled = True
            btnAdd.Enabled = False
            btnUpdateChanges.Enabled = True
            btnMakeAPayout.Enabled = True
            Btn_MakeTender.Enabled = True
            btnDeleteAccount.Enabled = True
            btnMigrateTransactions.Enabled = True
            txtCode1.ReadOnly = True
            txtCode2.ReadOnly = True
            txtCode3.ReadOnly = True
            txtCode1.Enabled = False
            txtCode2.Enabled = False
            txtCode3.Enabled = False
        End If
    End Sub
    Private Sub cmbFinancialCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFinancialCategory.SelectedIndexChanged
        'Try

        '    Enabled = False
        '    Cursor = Cursors.AppStarting

        '    If ckbIsNew.Checked = False Then
        '        If cmbFinancialCategory.Text.Trim = "Bank" Then
        '            If Desc3.Text <> "" And Code3.Text <> "" Then
        '                Dim Segment3Code As String = CStr(Code3.Text.Trim)
        '                Dim Segment3Desc As String = CStr(Desc3.Text.Trim)
        '                Dim Results = MBData.GetBankAcc(Segment3Code, Segment3Desc)
        '                If Not IsNothing(Results) Then

        '                    Dim UserResponse As DialogResult = _
        '                      MessageBox.Show("The MagicBox has detected that the current selected Acc(Bank)" & _
        '                    " doesn't have a bank account number assigned to it,It is highly recommended that you update this Account with the Bank Acc no. " & _
        '                    "Do you want to quickly go through the process of updating this Acc?, if yes please quickly have the correct Bank Acc no. with you", _
        '                    "Account recommended update", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        '                    If UserResponse = Windows.Forms.DialogResult.Yes Then
        '                        Dim frmUpdateAcc As New frmUpdateBankAcc
        '                        frmUpdateAcc.ID = CInt(Results.ID)
        '                        frmUpdateAcc.ShowDialog()
        '                        If frmUpdateAcc.isExit Then
        '                            MessageBox.Show("It is recommended that you upddate this Account as the Bank Acc no. is used for processing in the CashBook", "Reminder", _
        '                            MessageBoxButtons.OK, MessageBoxIcon.Information)
        '                        End If
        '                    Else
        '                        MessageBox.Show("It is recommended that you upddate this Account as the Bank Acc number is used for processing.", "Reminder", _
        '                        MessageBoxButtons.OK, MessageBoxIcon.Information)
        '                        Enabled = True
        '                        Cursor = Cursors.Arrow
        '                        Exit Sub
        '                    End If
        '                End If
        '            End If
        '        End If
        '    Else
        '        Dim frmUpdateAcc As New frmUpdateBankAcc
        '        frmUpdateAcc.IsNewAccount = True
        '        frmUpdateAcc.ShowDialog()
        '        BankAccNumber = CStr(frmUpdateAcc.txtBankAccNo.Text)
        '    End If

        '    Enabled = True
        '    Cursor = Cursors.Arrow

        'Catch ex As Exception
        '    Enabled = True
        '    Cursor = Cursors.Arrow
        '    MessageBox.Show("An error occured with details :" & ex.Message, "Error", _
        '                    MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub Btn_MakeTender_Click(sender As Object, e As EventArgs) Handles Btn_MakeTender.Click
        Try
            If txtDisplayName.Text = "" Then
                MsgBox("Please ensure you give the Account a display name!", MsgBoxStyle.Critical, "Display Name")
                Exit Sub
            End If
            Dim ra As Integer

            Dim sSQL As String

            sSQL = "Update Accounting Set [Type] = 'Tender', [Display Name] = '" & txtDisplayName.Text & "' Where ID = " & Me.lblID.Text

            If My.Settings.DBType = "Access" Then

                Dim myConnection As New OleDbConnection(My.Settings.CS_Setting)
                myConnection.Open()
                Dim myCommand As OleDbCommand
                myCommand = New OleDbCommand(sSQL, myConnection)
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                MsgBox("Records Updated " & ra)
                myConnection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                myConnection.Open()
                Dim myCommand As SqlCommand
                myCommand = New SqlCommand(sSQL, myConnection)
                myCommand.CommandTimeout = 300
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                MsgBox("Records Updated " & ra)
                myConnection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 082")
        End Try
    End Sub
End Class