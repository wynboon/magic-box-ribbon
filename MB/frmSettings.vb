﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Collections
Imports System.IO
Imports System.Configuration
Imports System.Diagnostics

Public Class frmSettings
    Dim CompanyKeyValue As Hashtable = New Hashtable()
    Dim CompanyValueKey As Hashtable = New Hashtable()
    Dim dbadp As OleDbDataAdapter
    Dim dTable As New DataTable
    Dim dbadp2 As SqlDataAdapter
    Dim dTable2 As New DataTable
    Private Sub S_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            LockForm(Me)
            '**********  Periods screen
            PeriodID.Text = 0
            Cmb_FinYear.Items.Add(Year(DateAdd(DateInterval.Year, -3, Today)))
            Cmb_FinYear.Items.Add(Year(DateAdd(DateInterval.Year, -2, Today)))
            Cmb_FinYear.Items.Add(Year(DateAdd(DateInterval.Year, -1, Today)))
            Cmb_FinYear.Items.Add(Year(DateAdd(DateInterval.Year, 0, Today)))
            Cmb_FinYear.Items.Add(Year(DateAdd(DateInterval.Year, 1, Today)))
            Cmb_FinYear.Items.Add(Year(DateAdd(DateInterval.Year, 2, Today)))
            '*********** End Periods



            If CStr(My.Settings.CS_Setting) = String.Empty And CStr(My.Settings.ListViewValues) = String.Empty Then
                Me.Text = "MagicBox -> Settings -> Version : " & My.Settings.Setting_Version
                Exit Sub
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            If My.Settings.Setting_CompanyID <> String.Empty And My.Settings.CS_Setting <> String.Empty Then
                Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Settings -> Version : " & My.Settings.Setting_Version
            End If

            If My.Settings.Setting_CompanyID = String.Empty And My.Settings.CS_Setting <> String.Empty Then
                MessageBox.Show("Please be aware that you have not set the Default Company, due to this the processing might return wrong or no information at all.", "Default Company not set", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            Me.ComboBox1.Text = My.Settings.DBType
            Me.lblAutoApproveCreditNotes.Text = My.Settings.Auto_Approve_CreditNotes
            Me.lblAutoApproveEFTs.Text = My.Settings.Auto_Approve_EFT
            lblPayInAdvance.Text = My.Settings.PayInAdvance
            lblIsFuturedate.Text = My.Settings.FutureInvoiceEnabled

            ReloadEmailDefaults()

            'If My.Settings.DBType = "Access" Then
            '    Me.Button1.Visible = True
            '    Me.Button2.Text = "Save Database Folder"
            'ElseIf My.Settings.DBType = "SQL" Then
            '    Me.TextBox1.Text = My.Settings.CS_Setting
            '    Me.Button1.Visible = False
            '    Me.Button2.Text = "Save Connection String"
            'End If

            Me.TextBox2.Text = My.Settings.Meta_Path

            If My.Settings.Setting_CompanyID <> String.Empty And My.Settings.CS_Setting <> String.Empty Then
                Call Fill_DGV1()
            End If

            If ListViewCS.Columns.Count <> 2 Then
                Me.ListViewCS.Columns.Add("", 172, HorizontalAlignment.Left)
                Me.ListViewCS.Columns.Add("", 10, HorizontalAlignment.Left)
            End If


            If My.Settings.ListViewValues <> "" Then
                LoadConnectionString()
            End If


            Me.cmbStartUp_Activation_Mode.Text = My.Settings.Start_Up_Activation_Mode

            'INI
            Dim mbDllDirectory As String = AppDomain.CurrentDomain.BaseDirectory().ToString
            'mbDllDirectory &= mbDllDirectory & "\DBAccess.ini"
            'MsgBox(mbDllDirectory.ToString)
            'Test

            mbDllDirectory &= "\DBAccess.ini"
            CompanyKeyValue.Clear()
            CompanyValueKey.Clear()
            Using sr As New System.IO.StreamReader(mbDllDirectory)
                Do While sr.Peek <> -1
                    Dim ConnDetails As String = sr.ReadLine()
                    Dim Details As String() = ConnDetails.Split(New Char() {"="c})

                    Dim CompanyName As String = Details(0).ToString
                    Dim ID As Integer = CInt(Details(1))
                    CompanyKeyValue.Add(ID, CompanyName)
                    CompanyValueKey.Add(CompanyName, ID)
                    cmbCompany.Items.Add(CompanyName)
                Loop

            End Using

            If CompanyKeyValue(CInt(My.Settings.Setting_CompanyID)) <> Nothing Then
                Dim ActiveCompany As String = CompanyKeyValue(CInt(My.Settings.Setting_CompanyID)).ToString
                cmbCompany.SelectedItem = ActiveCompany
            End If

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured. : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub LoadConnectionString()

        Dim strConnectionStrings As String = My.Settings.ListViewValues

        Me.ListViewCS.Items.Clear()
        Dim arrSplit As Object = Split(strConnectionStrings, "|")
        Dim strListViewLine As String
        Dim arrSplit2 As Object 'used to split each line

        Dim str(2) As String
        Dim itm As ListViewItem

        ListViewCS.Items.Clear()

        For i As Integer = 0 To UBound(arrSplit)
            strListViewLine = arrSplit(i)
            arrSplit2 = Split(strListViewLine, "~")
            str(0) = arrSplit2(2) 'other wat around to Database connections form
            str(1) = arrSplit2(1)
            str(2) = arrSplit2(0)

            If str(2).ToString = CStr(My.Settings.CS_Setting) Then
                itm = New ListViewItem(str)
                itm.ForeColor = Color.LimeGreen
                itm.Font = New Font(itm.Font, FontStyle.Bold)
                Me.ListViewCS.Items.Add(itm)
            Else
                itm = New ListViewItem(str)
                Me.ListViewCS.Items.Add(itm)
            End If

        Next
        '=============================================================
    End Sub

    Sub Fill_DGV1()
        Try

            Dim sSQL As String

            sSQL = "SELECT * FROM Settings"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                dbadp = New OleDbDataAdapter(sSQL, connection)
                dbadp.Fill(dTable)
                DataGridView1.DataSource = dTable
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                dbadp2 = New SqlDataAdapter(sSQL, connection)
                dbadp2.Fill(dTable2)
                Me.DataGridView1.DataSource = dTable2
            End If



            'Me.DataGridView1.Columns(0).Width = 0

        Catch ex As Exception
            MsgBox(ex.Message & " 197")
        End Try

    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'If Me.TextBox1.Text = "G" Then
        '    If Me.ComboBox1.Text = "Access" Then
        '        MsgBox("Not saved! Please enter a database folder before saving!")
        '    ElseIf Me.ComboBox1.Text = "SQL" Then
        '        MsgBox("Not saved! Please enter a connection string before saving!")
        '    End If
        '    Exit Sub
        'End If

        My.Settings.DBType = Me.ComboBox1.Text

        'Dim oFullPath As String = Me.TextBox1.Text & "\MagicBox.accdb"
        'If Me.ComboBox1.Text = "Access" Then
        '    My.Settings.CS_Setting = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"
        '    My.Settings.Setting_Path = oFullPath
        '    My.Settings.DBType = "Access"
        '    My.Settings.Save()
        'ElseIf Me.ComboBox1.Text = "SQL" Then
        '    My.Settings.CS_Setting = Me.TextBox1.Text
        '    My.Settings.DBType = "SQL"
        '    My.Settings.Save()
        'End If

        Me.Close()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim builder As New OleDbCommandBuilder(dbadp)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            dbadp.Update(dTable)
            dbadp.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Dim folderDlg As New FolderBrowserDialog

        folderDlg.ShowNewFolderButton = True

        If (folderDlg.ShowDialog() = DialogResult.OK) Then

            TextBox2.Text = folderDlg.SelectedPath

            Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        My.Settings.Meta_Path = Me.TextBox2.Text
        My.Settings.Save()
        INIAccess.INIWrite(App_Path() & "Alchemex.ini", "Publisher", "Conn", Me.TextBox2.Text)
        Me.Close()
    End Sub




    Private Sub btnEnable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        My.Settings.Load_Checks = "Yes"
        My.Settings.Save()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        My.Settings.Load_Checks = "No"
        My.Settings.Save()

    End Sub

    Private Sub btnEnable2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        My.Settings.ID_Checks = "Yes"
        My.Settings.Save()
    End Sub

    Private Sub btnDisable2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        My.Settings.ID_Checks = "No"
        My.Settings.Save()
    End Sub
    Private Sub btnEnable3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnable3.Click
        My.Settings.Auto_Approve_CreditNotes = "Yes"
        My.Settings.Save()
        Me.lblAutoApproveCreditNotes.Text = My.Settings.Auto_Approve_CreditNotes
    End Sub

    Private Sub btnEnable4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnable4.Click
        My.Settings.Auto_Approve_EFT = "Yes"
        My.Settings.Save()
        Me.lblAutoApproveEFTs.Text = My.Settings.Auto_Approve_EFT
    End Sub

    Private Sub btnDisable3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisable3.Click
        My.Settings.Auto_Approve_CreditNotes = "No"
        My.Settings.Save()
        Me.lblAutoApproveCreditNotes.Text = My.Settings.Auto_Approve_CreditNotes
    End Sub

    Private Sub btnDisable4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisable4.Click
        My.Settings.Auto_Approve_EFT = "No"
        My.Settings.Save()
        Me.lblAutoApproveEFTs.Text = My.Settings.Auto_Approve_EFT
    End Sub
    Function oTest_Connection_String(ByVal strConnectionString As String, ByVal oDatabaseType As String) As Boolean
        Try

            Dim sSQL As String = "Select * From Periods"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(strConnectionString)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                connection.Close()
                oTest_Connection_String = True

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                connection.Close()
                oTest_Connection_String = True
            End If

        Catch ex As Exception
            oTest_Connection_String = False
            MsgBox("Invalid Connection string or database connection down " & ex.Message)
        End Try

    End Function
    'Private Sub TextBox1_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.MouseLeave
    '    If Me.TextBox1.Text <> "" Then
    '        If Me.ComboBox1.Text = "Access" Then Exit Sub
    '        If oTest_Connection_String(Me.TextBox1.Text, Me.ComboBox1.Text) = False Then
    '            Me.lblInvalidConnectionString.Text = "Invalid Connection String ! " & Me.TextBox1.Text
    '            Me.TextBox1.Text = ""
    '        Else
    '            Me.lblInvalidConnectionString.Text = ""
    '        End If
    '    End If
    'End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub btnUseConnectionString_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUseConnectionString.Click

        Try

            If Me.ListViewCS.SelectedItems.Count < 1 Then
                MsgBox("Please select a database connection from the list.")
                Exit Sub
            End If

            Dim oConnectionName As String = Me.ListViewCS.SelectedItems(0).SubItems(0).Text
            Dim oDatabaseType As String = Me.ListViewCS.SelectedItems(0).SubItems(1).Text
            Dim oConnectionString As String = Me.ListViewCS.SelectedItems(0).SubItems(2).Text

            If oDatabaseType = "Access" Then

                My.Settings.CS_Setting = oConnectionString
                My.Settings.DBType = oDatabaseType
                My.Settings.Save()

                Me.ComboBox1.Text = oDatabaseType
                'Me.TextBox1.Text = oConnectionString

            ElseIf oDatabaseType = "SQL" Then

                My.Settings.CS_Setting = oConnectionString
                My.Settings.DBType = oDatabaseType
                My.Settings.Save()
                Me.ComboBox1.Text = oDatabaseType
                'Me.TextBox1.Text = oConnectionString

                If Globals.Ribbons.Ribbon1.btnLogin.Label = "Log out" Then
                    Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                    Globals.Ribbons.Ribbon1.lblGroup.Label = "-"
                    Globals.Ribbons.Ribbon1.btnLogin.Label = "Log in"
                    Globals.Ribbons.Ribbon1.txtUserName.Text = ""
                    Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                    Globals.Ribbons.Ribbon1.txtPassword.Text = ""
                Else
                    Globals.Ribbons.Ribbon1.txtPassword.Text = ""
                    Globals.Ribbons.Ribbon1.txtUserName.Text = ""
                End If
            End If
            S_Load(sender, e)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            Dim FormCS As New frmDataBaseConnections
            FormCS.ShowDialog()
            Cursor = Cursors.AppStarting
            S_Load(sender, e)
            Cursor = Cursors.Arrow
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveMultiple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnEnableUserControl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        My.Settings.User_Control_On = "Yes"
        My.Settings.Save()
        Globals.Ribbons.Ribbon1.GrpLogin.Visible = True
    End Sub

    Private Sub btnDisableUserControl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        My.Settings.User_Control_On = "No"
        My.Settings.Save()
        Globals.Ribbons.Ribbon1.GrpLogin.Visible = False
    End Sub

    Private Sub BtnExit_Click(sender As Object, e As EventArgs) Handles BtnExit.Click
        If My.Settings.Setting_CompanyID = String.Empty Then
            MessageBox.Show("Please be aware that you have not set the Default Company, due to this the processing might return wrong or no information at all.", "Default Company not set", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        Me.Close()
    End Sub
    Private Sub btnEnable5_Click(sender As Object, e As EventArgs) Handles btnEnable5.Click
        My.Settings.PayInAdvance = "Yes"
        My.Settings.Save()
        Me.lblPayInAdvance.Text = My.Settings.PayInAdvance
    End Sub
    Private Sub btnDisable5_Click(sender As Object, e As EventArgs) Handles btnDisable5.Click
        My.Settings.PayInAdvance = "No"
        My.Settings.Save()
        Me.lblPayInAdvance.Text = My.Settings.PayInAdvance
    End Sub
    Private Sub ckbtoDefault_CheckedChanged(sender As Object, e As EventArgs) Handles ckbtoDefault.CheckedChanged
        If ckbtoDefault.Checked = True Then
            If txtDefaultEmail.Text = "" Then
                MessageBox.Show("Please supply the default email address in order to continue.", _
                                "Default Email address", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                ckbtoDefault.Checked = False
                txtDefaultEmail.Focus()
                Exit Sub
            End If
            If ckbToAll.Checked = True Then
                ckbToAll.Checked = False
            End If
        End If
    End Sub
    Private Sub ckbToAll_CheckedChanged(sender As Object, e As EventArgs) Handles ckbToAll.CheckedChanged
        If ckbToAll.Checked = True Then
            If txtDefaultEmail.Text = "" Then
                MessageBox.Show("Please supply the default email address in order to continue.", _
                                "Default Email address", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                ckbToAll.Checked = False
                txtDefaultEmail.Focus()
                Exit Sub
            End If
            If ckbtoDefault.Checked = True Then
                ckbtoDefault.Checked = False
            End If
        End If
    End Sub
    Private Sub btnSaveChanges_Click(sender As Object, e As EventArgs) Handles btnSaveChanges.Click
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            If ckbToAll.Checked = True Or ckbtoDefault.Checked = True Then
                If txtDefaultEmail.Text = "" Then
                    MessageBox.Show("Please supply default email address", "Default Email", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtDefaultEmail.Focus()
                    Exit Sub
                End If
                My.Settings.DefaultEmailAddress = CStr(txtDefaultEmail.Text.Trim)
                If ckbToAll.Checked = True Then
                    My.Settings.EmailAll = "Yes"
                    My.Settings.EmailDefault = ""
                End If
                If ckbtoDefault.Checked = True Then
                    My.Settings.EmailDefault = "Yes"
                    My.Settings.EmailAll = ""
                End If
                My.Settings.Save()
                MessageBox.Show("Email settings succesfully saved", "Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ReloadEmailDefaults()
            End If

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ReloadEmailDefaults()
        txtDefaultEmail.Text = My.Settings.DefaultEmailAddress
        If My.Settings.EmailAll = "Yes" Then
            ckbToAll.Checked = True
        ElseIf My.Settings.EmailDefault = "Yes" Then
            ckbtoDefault.Checked = True
        End If
    End Sub

    Private Sub TextBox1_Leave(sender As Object, e As EventArgs)
        'If Me.TextBox1.Text <> "" Then
        '    If Me.ComboBox1.Text = "Access" Then Exit Sub
        '    If oTest_Connection_String(Me.TextBox1.Text, Me.ComboBox1.Text) = False Then
        '        Me.lblInvalidConnectionString.Text = "Invalid Connection String ! " & Me.TextBox1.Text
        '        Me.TextBox1.Text = ""
        '    Else
        '        Me.lblInvalidConnectionString.Text = ""
        '    End If
        'End If
    End Sub

    Private Sub btnEnable6_Click(sender As Object, e As EventArgs) Handles btnEnable6.Click
        My.Settings.FutureInvoiceEnabled = "Yes"
        My.Settings.Save()
        lblIsFuturedate.Text = My.Settings.FutureInvoiceEnabled
    End Sub

    Private Sub btnDisable6_Click(sender As Object, e As EventArgs) Handles btnDisable6.Click
        My.Settings.FutureInvoiceEnabled = "No"
        My.Settings.Save()
        lblIsFuturedate.Text = My.Settings.FutureInvoiceEnabled
    End Sub
    Private Sub btnLoad_Click_1(sender As Object, e As EventArgs) Handles btnLoad.Click
        Try
            Dim frmDBConns As New frmDataBaseConnections
            frmDBConns.ShowDialog()
            If frmDBConns.NewItems = True Then
                S_Load(sender, e)
            End If
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Try

            Enabled = False
            Cursor = Cursors.AppStarting
            Dim Co_ID As Integer = CInt(CompanyValueKey(CStr(cmbCompany.SelectedItem.ToString)))
            Dim CurrentCompany As String = CStr(cmbCompany.Text)
            My.Settings.Setting_CompanyID = Co_ID
            My.Settings.CurrentCompany = CurrentCompany
            My.Settings.Save()

            If My.Settings.Setting_CompanyID <> String.Empty And My.Settings.CS_Setting <> String.Empty Then
                Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Settings -> Version : " & My.Settings.Setting_Version
            End If

            Enabled = True
            Cursor = Cursors.Arrow
            MsgBox("Current workspace set to :" & cmbCompany.SelectedItem)

            If Globals.Ribbons.Ribbon1.btnLogin.Label = "Log out" Then
                Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                Globals.Ribbons.Ribbon1.lblGroup.Label = "-"
                Globals.Ribbons.Ribbon1.btnLogin.Label = "Log in"
                Globals.Ribbons.Ribbon1.txtUserName.Text = ""
                Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                Globals.Ribbons.Ribbon1.txtPassword.Text = ""
            Else
                Globals.Ribbons.Ribbon1.txtPassword.Text = ""
                Globals.Ribbons.Ribbon1.txtUserName.Text = ""
            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSaveDatabaseType_Click(sender As Object, e As EventArgs) Handles btnSaveDatabaseType.Click
        Try

            My.Settings.DBType = Me.ComboBox1.Text
            My.Settings.Save()

        Catch ex As Exception
            MessageBox.Show("An error occured with details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSave_StartUp_Activation_Mode_Click(sender As Object, e As EventArgs) Handles btnSave_StartUp_Activation_Mode.Click
        Try
            My.Settings.Start_Up_Activation_Mode = Me.cmbStartUp_Activation_Mode.Text
            My.Settings.Save()
        Catch ex As Exception
            MessageBox.Show("An error occured with details.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim FilePath As String
        Dim config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal)
        FilePath = config.FilePath
        If File.Exists(FilePath) = True Then Exit Sub
        Dim directory As String = Path.GetDirectoryName(FilePath)
        Dim parentinfo As New DirectoryInfo(IO.Directory.GetParent(directory).ToString)
        Dim OldFilePath As String
        Dim counter1
        Dim dirsize
        Dim ArrDir(100) As String

        counter1 = My.Computer.FileSystem.GetFiles(parentinfo.ToString)
        dirsize = counter1.Count

        Dim i As Integer = 0

        For Each dir As FileSystemInfo In parentinfo.GetFileSystemInfos()
            If dir.Attributes() And IO.FileAttributes.Directory Then
                ArrDir(i) = dir.Name
            End If

            i += 1
        Next dir
        System.Array.Sort(Of String)(ArrDir)
        OldFilePath = parentinfo.ToString & "\" & (ArrDir(99) & "\user.config")
        File.Copy(OldFilePath, FilePath, True)
        MsgBox("MagicBox has restore the system settings, please restart Excel for the updated settings to take place.", MsgBoxStyle.Critical)
    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub Btn_LoadTax_Click(sender As Object, e As EventArgs) Handles Btn_LoadTax.Click
        LoadTax()
    End Sub
    Function LoadTax()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select * from TaxTypes Where Co_ID = " & My.Settings.Setting_CompanyID)

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "CashPay")
            connection.Close()
            DGV_Tax.DataSource = ds
            DGV_Tax.DataMember = "CashPay"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            DGV_Tax.Columns("Co_ID").Visible = False
            DGV_Tax.Columns("ID").Visible = False

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Function
    Private Sub Btn_AddTax_Click(sender As Object, e As EventArgs) Handles Btn_AddTax.Click
        Dim FrmTax As New FrmAddTaxType
        FrmTax.ShowDialog()
        LoadTax()
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Periods.CellContentClick
        Try
            Dim colName As String = DGV_Periods.Columns(e.ColumnIndex).Name
            TB_Period.Text = DGV_Periods.Rows(e.RowIndex).Cells("Period").Value()
            TB_StartDate.Text = DGV_Periods.Rows(e.RowIndex).Cells("Start Date").Value()
            TB_StartDate.Text = DGV_Periods.Rows(e.RowIndex).Cells("End Date").Value()
            If IsDBNull(DGV_Periods.Rows(e.RowIndex).Cells("BLOCKED").Value) Then
                CB_Closed.Checked = False
            Else
                If DGV_Periods.Rows(e.RowIndex).Cells("BLOCKED").Value = "Y" Then
                    CB_Closed.Checked = True
                Else
                    CB_Closed.Checked = False
                End If
            End If
            PeriodID.Text = DGV_Periods.Rows(e.RowIndex).Cells("Period").Value()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Btn_Refresh_Click(sender As Object, e As EventArgs)
        Call LoadPeriods()
    End Sub
    Function LoadPeriods()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select [ID],[Period],[Start Date],[End Date],[BLOCKED],[FinYear] from Periods Where Co_ID = " & My.Settings.Setting_CompanyID & _
                " and [FinYear] = " & Cmb_FinYear.Text & " Order By Period")

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "CashPay")
            connection.Close()
            DGV_Periods.DataSource = ds
            DGV_Periods.DataMember = "CashPay"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            DGV_Periods.Columns("ID").Visible = False
            DGV_Periods.Columns("FinYear").Visible = False
            DGV_Periods.Columns("Start Date").DefaultCellStyle.Format = "dd MMM yyyy"
            DGV_Periods.Columns("End Date").DefaultCellStyle.Format = "dd MMM yyyy"

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Function

    Private Sub Btn_PeriodRemove_Click(sender As Object, e As EventArgs) Handles Btn_PeriodRemove.Click
        Dim Period_ID As String
        Period_ID = DGV_Periods.Rows(DGV_Periods.CurrentCell.RowIndex).Cells("ID").Value()
        If MsgBox("Are you sure you want to delete the selected period?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Period_Delete(Period_ID)
            PeriodID.Text = 0
            LoadPeriods()
        End If
    End Sub

    Private Sub btn_AddNewPeriod_Click(sender As Object, e As EventArgs) Handles btn_AddNewPeriod.Click
        If MsgBox("Are you sure you want to add the entered period?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Period_Add(Cmb_FinYear.Text, TB_Period.Text, TB_StartDate.Text, TB_EndDate.Text, CB_Closed.CheckState)
            PeriodID.Text = 0
            LoadPeriods()
        End If
    End Sub

    Private Sub Cmb_FinYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Cmb_FinYear.SelectedIndexChanged
        PeriodID.Text = 0
        Call LoadPeriods()
    End Sub
    Private Sub btn_Edit_Click(sender As Object, e As EventArgs) Handles btn_Edit.Click
        If MsgBox("Are you sure you want to edit the entered period?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Period_Edit(PeriodID.Text, Cmb_FinYear.Text, TB_Period.Text, TB_StartDate.Text, TB_EndDate.Text, CB_Closed.CheckState)
            PeriodID.Text = 0
            LoadPeriods()
        End If
    End Sub
    Private Sub PeriodID_TextChanged(sender As Object, e As EventArgs) Handles PeriodID.TextChanged
        If PeriodID.Text = 0 Then
            btn_Edit.Visible = False

        Else
            btn_Edit.Visible = True
        End If
    End Sub
    Private Sub DGV_Tax_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Tax.CellContentClick
        Try
            lbl_TaxID.Text = DGV_Tax.Rows(e.RowIndex).Cells("ID").Value
            TB_TaxDesc.Text = DGV_Tax.Rows(e.RowIndex).Cells("TaxDesc").Value
            TB_TaxPerc.Text = DGV_Tax.Rows(e.RowIndex).Cells("TaxPerc").Value
            If IsDBNull(DGV_Tax.Rows(e.RowIndex).Cells("Default").Value) Then
                CB_Tx_Default.Checked = False
            Else
                CB_Tx_Default.Checked = DGV_Tax.Rows(e.RowIndex).Cells("Default").Value
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Btn_removeTax_Click(sender As Object, e As EventArgs) Handles Btn_removeTax.Click
        If MsgBox("Are you sure you want to remove the tax record?", MsgBoxStyle.YesNo) = vbYes Then
            TaxRecord_Delete(lbl_TaxID.Text)
        End If


    End Sub

    Private Sub btn_TaxUpdate_Click(sender As Object, e As EventArgs) Handles btn_TaxUpdate.Click
        If MsgBox("Are you sure you want to update the tax record?", MsgBoxStyle.YesNo) = vbYes Then
            If Not IsNumeric(TB_TaxPerc.Text) Then
                MsgBox("Tax percentage must be a numeric value!" & vbNewLine & "Exitting")
            End If
            Call Tax_Edit(lbl_TaxID.Text, TB_TaxDesc.Text, TB_TaxPerc.Text, CB_Tx_Default.Checked)
            LoadTax()
        End If
    End Sub

    Private Sub Btn_UpdateCheck_Click(sender As Object, e As EventArgs) Handles Btn_UpdateCheck.Click
        Try
            Dim mbDllDirectory As String = AppDomain.CurrentDomain.BaseDirectory().ToString
            Dim startInfo As New ProcessStartInfo(mbDllDirectory & "\Bank Statement Importer.exe")
            Dim userid As Integer = 1
            startInfo.Arguments = My.Settings.CS_Setting & "|" & My.Settings.Setting_CompanyID & "|" & "Cashbook"
            Process.Start(startInfo)
        Catch ex As Exception
            MessageBox.Show("An Error occured. Please report to System support with details :" & ex.Message, "MagicBox Cashbook Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class