﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPeriods
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPeriods))
        Me.DirectoryEntry1 = New System.DirectoryServices.DirectoryEntry()
        Me.dgvPeriods = New System.Windows.Forms.DataGridView()
        Me.Button_EditSelectedDate = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvPeriods
        '
        Me.dgvPeriods.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPeriods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPeriods.Location = New System.Drawing.Point(4, 25)
        Me.dgvPeriods.Name = "dgvPeriods"
        Me.dgvPeriods.Size = New System.Drawing.Size(441, 374)
        Me.dgvPeriods.TabIndex = 0
        '
        'Button_EditSelectedDate
        '
        Me.Button_EditSelectedDate.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Button_EditSelectedDate.Location = New System.Drawing.Point(266, 404)
        Me.Button_EditSelectedDate.Name = "Button_EditSelectedDate"
        Me.Button_EditSelectedDate.Size = New System.Drawing.Size(92, 23)
        Me.Button_EditSelectedDate.TabIndex = 2
        Me.Button_EditSelectedDate.Text = "&Save Changes"
        Me.Button_EditSelectedDate.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnExit.Location = New System.Drawing.Point(362, 404)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(83, 23)
        Me.btnExit.TabIndex = 74
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'frmPeriods
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 430)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Button_EditSelectedDate)
        Me.Controls.Add(Me.dgvPeriods)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(466, 469)
        Me.MinimumSize = New System.Drawing.Size(466, 469)
        Me.Name = "frmPeriods"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Periods"
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DirectoryEntry1 As System.DirectoryServices.DirectoryEntry
    Friend WithEvents dgvPeriods As System.Windows.Forms.DataGridView
    Friend WithEvents Button_EditSelectedDate As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
End Class
