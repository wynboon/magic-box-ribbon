﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashPayRemoveV2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCashPayRemoveV2))
        Me.DGV_Payments = New System.Windows.Forms.DataGridView()
        Me.Void = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btn_Void = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DTP_FromDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DTP_ToDate = New System.Windows.Forms.DateTimePicker()
        Me.Btn_Search = New System.Windows.Forms.Button()
        CType(Me.DGV_Payments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Payments
        '
        Me.DGV_Payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Payments.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Void})
        Me.DGV_Payments.Location = New System.Drawing.Point(12, 58)
        Me.DGV_Payments.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_Payments.Name = "DGV_Payments"
        Me.DGV_Payments.Size = New System.Drawing.Size(994, 291)
        Me.DGV_Payments.TabIndex = 0
        '
        'Void
        '
        Me.Void.HeaderText = "Remove"
        Me.Void.Name = "Void"
        Me.Void.Width = 50
        '
        'btn_Void
        '
        Me.btn_Void.Location = New System.Drawing.Point(20, 433)
        Me.btn_Void.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Void.Name = "btn_Void"
        Me.btn_Void.Size = New System.Drawing.Size(427, 32)
        Me.btn_Void.TabIndex = 1
        Me.btn_Void.Tag = "S_VoidPayment"
        Me.btn_Void.Text = "Void Selected Payments"
        Me.btn_Void.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(579, 431)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(427, 32)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(9, 5)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(507, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Payments May Only Be Removed If There Has Been No Cash Up On That Date"
        '
        'DTP_FromDate
        '
        Me.DTP_FromDate.CustomFormat = "dd MMM yyyy"
        Me.DTP_FromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTP_FromDate.Location = New System.Drawing.Point(92, 32)
        Me.DTP_FromDate.Name = "DTP_FromDate"
        Me.DTP_FromDate.Size = New System.Drawing.Size(123, 22)
        Me.DTP_FromDate.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "From Date:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(244, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "To Date:"
        '
        'DTP_ToDate
        '
        Me.DTP_ToDate.CustomFormat = "dd MMM yyyy"
        Me.DTP_ToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTP_ToDate.Location = New System.Drawing.Point(324, 33)
        Me.DTP_ToDate.Name = "DTP_ToDate"
        Me.DTP_ToDate.Size = New System.Drawing.Size(123, 22)
        Me.DTP_ToDate.TabIndex = 7
        '
        'Btn_Search
        '
        Me.Btn_Search.Location = New System.Drawing.Point(467, 32)
        Me.Btn_Search.Name = "Btn_Search"
        Me.Btn_Search.Size = New System.Drawing.Size(119, 23)
        Me.Btn_Search.TabIndex = 9
        Me.Btn_Search.Text = "Search"
        Me.Btn_Search.UseVisualStyleBackColor = True
        '
        'frmCashPayRemove
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1054, 531)
        Me.Controls.Add(Me.Btn_Search)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DTP_ToDate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DTP_FromDate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn_Void)
        Me.Controls.Add(Me.DGV_Payments)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmCashPayRemove"
        Me.Tag = "S_PaymentMaintenance"
        Me.Text = "frmCashPayRemove"
        CType(Me.DGV_Payments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_Payments As System.Windows.Forms.DataGridView
    Friend WithEvents Void As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btn_Void As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DTP_FromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DTP_ToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Btn_Search As System.Windows.Forms.Button
End Class
