﻿Public Class OtherModulesData
#Region "Variable"
    Dim OtherModulesContext As New OtherModulesDataContext
#End Region
#Region "Methods"
    Public Function CheckStoreModules(ByVal DBName As String, ByVal Co_ID As Integer)
        If IsDBNull(OtherModulesContext.MBModules_CountStoreModules(DBName, Co_ID).FirstOrDefault().Column1) _
            Or IsNothing(OtherModulesContext.MBModules_CountStoreModules(DBName, Co_ID).FirstOrDefault().Column1) Then
            Return 0
        Else
            Return OtherModulesContext.MBModules_CountStoreModules(DBName, Co_ID).FirstOrDefault().Column1
        End If
    End Function
    Public Function StoreModules(ByVal DBName As String, ByVal Co_ID As Integer) As IQueryable(Of MB_ModulesInventory)
        Return OtherModulesContext.Func_GetStoreModules(DBName, Co_ID)
    End Function
    Public Function ModuleParametres(ByVal ModuleID As Integer) As IQueryable(Of MB_ModuleParametre)
        Return OtherModulesContext.MB_ModuleParametres.Where(Function(fn) fn.ModuleID = ModuleID)
    End Function
#End Region
End Class
