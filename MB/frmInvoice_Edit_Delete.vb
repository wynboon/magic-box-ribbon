﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Collections


Public Class frmInvoice_Edit_Delete

#Region "Variables"
    Dim frmInvcCapture As frmInvoice_Capture
    Dim frmInvcEdit As frmInvoiceEdit
    Dim MBData As New DataExtractsClass
#End Region
#Region "Methods"
#End Region

    Private Sub frmInvoice_Edit_Delete_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            LockForm(Me)
            Me.Cursor = Cursors.AppStarting
            Me.Enabled = False

            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Invoice Editing -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            FillSupplierCombobox()
            Me.dtpFrom.Value = System.DateTime.Now.AddYears(-1)
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub FindButton_Click(sender As System.Object, e As System.EventArgs) Handles FindButton.Click
        Try

            Me.Cursor = Cursors.AppStarting
            Me.Enabled = False
            LoadInvoices(CInt(cmbSupplier.SelectedValue), dtpFrom.Value, dtpTo.Value)
            dgvInvoices.Columns("Amount").DefaultCellStyle.Format = "c"
            dgvInvoices.Columns("Due").DefaultCellStyle.Format = "c"

            dgvInvoices.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("Due").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("Reference").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("Transaction Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvInvoices.Columns("SupplierName").Visible = False

            Me.Cursor = Cursors.Arrow
            Me.Enabled = True

        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub oFind()
        Call GetInvoiceDetailPerSupplier2()
        Call Load_Invoices_for_Supplier_to_ComboBox()
    End Sub

    Sub GetInvoiceDetailPerSupplier2()

        Try

            Dim oSupplier As String
            oSupplier = Me.cmbSupplier.Text
            Dim sSQL_Supplier As String = ""

            If oSupplier = "[ALL]" Or oSupplier = "" Then
                'do nothing
            Else
                oSupplier = Me.cmbSupplier.Text
                sSQL_Supplier = " And SupplierName = '" & SQLConvert(oSupplier) & "'"
            End If


            Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            Else
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If


            Dim sSQL As String
            'If Me.ComboBox_Status_Delete_Invoice.Text = "[ALL]" Then
            '    sSQL = "Select * From [Step4b] Where (Due <=0 Or Due >0)"
            'ElseIf Me.ComboBox_Status_Delete_Invoice.Text = "PAID" Then
            '    sSQL = "Select * From [Step4b] Where Due <=0"
            'ElseIf Me.ComboBox_Status_Delete_Invoice.Text = "UNPAID" Then
            '    sSQL = "Select * From [Step4b] Where Due >0"
            'End If

            sSQL = sSQL & sSQL_Supplier
            sSQL = sSQL & sDateCriteria


            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            'First clear DGV
            dgvInvoices.DataSource = Nothing

            System.Threading.Thread.Sleep(1000)

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                dgvInvoices.DataSource = ds
                dgvInvoices.DataMember = "Suppliers_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                dgvInvoices.DataSource = ds
                dgvInvoices.DataMember = "Suppliers_table"
            End If

            dgvInvoices.Columns(0).Visible = False
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 149")
        End Try

    End Sub

    Sub Load_Invoices_for_Supplier_to_ComboBox()
        Try
            If My.Settings.DBType = "Access then" Then
                Dim sSQL As String
                sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = False"


                Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
                Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")
                Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= #" & oFrom & "# And Transactions.[Transaction Date] <= #" & oTo & "#)"
                sSQL = sSQL & sDateCriteria
                Dim oSupplier As String
                oSupplier = Me.cmbSupplier.Text
                If oSupplier <> "[ALL]" Then
                    oSupplier = Me.cmbSupplier.Text
                    sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                sSQL = sSQL & " ORDER BY Transactions.Reference"
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)

                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()

                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Dim oReference As String
                Try

                    Me.Invoice_Numbers_Combo.Items.Clear()
                    Me.Invoice_Numbers_Combo.Items.Add("[ALL]")
                    While datareader.Read
                        If Not IsDBNull(datareader("Invoice Number")) = True Then
                            oReference = datareader("Invoice Number").ToString()
                            Me.Invoice_Numbers_Combo.Items.Add(oReference)
                        End If
                    End While
                    Me.Invoice_Numbers_Combo.SelectedIndex = 0

                    connection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message & " 150")
                    connection.Close()
                End Try
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = 0"
                Dim oFrom As String = Me.dtpFrom.Value.Date.ToString("dd MMMM yyyy")
                Dim oTo As String = Me.dtpTo.Value.Date.ToString("dd MMMM yyyy")
                Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= '" & oFrom & "' And Transactions.[Transaction Date] <= '" & oTo & "')"
                sSQL = sSQL & sDateCriteria
                Dim oSupplier As String
                oSupplier = Me.cmbSupplier.Text
                If oSupplier <> "[ALL]" Then
                    oSupplier = Me.cmbSupplier.Text
                    sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                sSQL = sSQL & " ORDER BY Transactions.Reference"
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()

                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Dim oReference As String
                Try
                    Me.Invoice_Numbers_Combo.Items.Clear()
                    Me.Invoice_Numbers_Combo.Items.Add("[ALL]")
                    While datareader.Read
                        If Not IsDBNull(datareader("Invoice Number")) = True Then
                            oReference = datareader("Invoice Number").ToString()
                            Me.Invoice_Numbers_Combo.Items.Add(oReference)
                        End If
                    End While
                    Me.Invoice_Numbers_Combo.SelectedIndex = 0
                    connection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message & " 151")
                    connection.Close()
                End Try
            End If
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 152")
        End Try
    End Sub


    Private Sub Button_Activate_Click(sender As System.Object, e As System.EventArgs)
        Try
            Me.Label_Level.Text = CheckUserLevel(Me.txtUserName.Text, Me.txtPassword.Text)
        Catch ex As Exception
            MsgBox(ex.Message & " 153")
        End Try
    End Sub

    Private Sub Button_Activate_Click_1(sender As System.Object, e As System.EventArgs) Handles Button_Activate.Click
        Try
            Me.Label_Level.Text = CheckUserLevel(Me.txtUserName.Text, Me.txtPassword.Text)
        Catch ex As Exception
            MsgBox(ex.Message & " 154")
        End Try
    End Sub

    Sub Fill_Edit_Supplier_Combobox()
        Try

            Dim sSQL As String
            sSQL = "SELECT Distinct SupplierName FROM Suppliers"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            sSQL = sSQL & " Order By SupplierName"


            Me.cmbSupplier.Items.Clear()

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Try
                    Me.cmbSupplier.Items.Clear()
                    Me.cmbSupplier.Items.Add("[ALL]")
                    While datareader.Read
                        Me.cmbSupplier.Items.Add(datareader("SupplierName"))
                    End While
                    Me.cmbSupplier.SelectedIndex = 0
                    connection.Close()
                Catch ex As Exception
                    MsgBox(ex.Message & " 018")
                    connection.Close()
                End Try
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Try
                    Me.cmbSupplier.Items.Clear()
                    Me.cmbSupplier.Items.Add("[ALL]")
                    While datareader.Read
                        Me.cmbSupplier.Items.Add(datareader("SupplierName"))
                    End While
                    Me.cmbSupplier.SelectedIndex = 0

                    connection.Close()
                Catch ex As Exception
                    MsgBox(ex.Message & " 019")
                    connection.Close()
                End Try
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 020")
        End Try

    End Sub
    Private Sub Delete_Invoice_Button_Click(sender As System.Object, e As System.EventArgs) Handles Delete_Invoice_Button.Click

        Me.Enabled = False
        Me.Cursor = Cursors.AppStarting
        Try


            '  If Me.Label_Level.Text <> "Super" Then
            MessageBox.Show("You need to activate the required user level to delete an invoice that was not captured today!", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ' Me.Enabled = True
            '  Me.Cursor = Cursors.Arrow
            '  Exit Sub
            '  End If

            Dim Results As DialogResult = MessageBox.Show("Are you sure you want to delete this invoice?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Results = Windows.Forms.DialogResult.No Then
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.dgvInvoices.SelectedCells.Count < 1 Then
                MsgBox("Please select a row containing an invoice!")
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            Dim TransactionID As String = Me.dgvInvoices.SelectedRows(0).Cells("TransactionID").Value
            If CheckForRequests(TransactionID) > 0 Then
                MessageBox.Show("There are payment(s) request against this invoice, please pay/remove the requests before you attempt to delete this invoice", _
                                "Invoice Movements", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.dgvInvoices.SelectedRows.Count < 1 Then
                Me.dgvInvoices.Rows(Me.dgvInvoices.SelectedCells(0).RowIndex).Selected = True
            End If

            Dim oRowIndex As Integer
            oRowIndex = Me.dgvInvoices.SelectedRows(0).Index

            Dim oAmount As Decimal
            oAmount = Me.dgvInvoices.SelectedRows(0).Cells(3).Value
            Dim oDue As Decimal
            oDue = Me.dgvInvoices.SelectedRows(0).Cells(4).Value
            If oDue <> oAmount Then
                '  If Me.Label_Level.Text <> "Super" Then
                ' MsgBox("Payments or other reversals have been made against this invoice. Please activate the required level to delete this with your username and password!")
                ' Me.Enabled = True
                ' Me.Cursor = Cursors.Arrow
                ' Exit Sub
                '  End If
            End If

            Dim oTransactionDate As Date = CDate(Me.dgvInvoices.SelectedRows(0).Cells(5).Value)
            If oTransactionDate < Now.Date Then
                '  If Me.Label_Level.Text <> "Super" Then
                ' MsgBox("You need to activate the required user level to delete an invoice that wasn't entered today!")
                ' Me.Enabled = True
                ' Me.Cursor = Cursors.Arrow
                ' Exit Sub
                ' End If
            End If


            Dim sSupplierName As String = Me.dgvInvoices.SelectedRows(0).Cells(1).Value
            Dim oSupplierID As Integer
            oSupplierID = Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") 'Supplier Name

            Dim oReference As String = Me.dgvInvoices.SelectedRows(0).Cells(2).Value

            Dim oResponse As MsgBoxResult
            Dim oCount_Credit_Notes_against_Invoice As Integer = oCount_Credit_Notes(sSupplierName, oReference)
            If oCount_Credit_Notes_against_Invoice > 0 Then
                oResponse = MsgBox("There are credit notes against this invoice that will be deleted. Would you like to continue?", MsgBoxStyle.YesNo)
                If oResponse = MsgBoxResult.Yes Or MsgBoxResult.Ok Then
                    'do nothing
                Else
                    Me.Enabled = True
                    Me.Cursor = Cursors.Arrow
                    Exit Sub
                End If
            End If

            Dim oCount_Supplier_Debits_Notes_against_Invoice As Integer = oCount_Supplier_Debits(sSupplierName, oReference)
            If oCount_Supplier_Debits_Notes_against_Invoice > 0 Then
                oResponse = MsgBox("There are supplier debit notes against this invoice that will be deleted. Would you like to continue?", MsgBoxStyle.YesNo)
                If oResponse = MsgBoxResult.Yes Or MsgBoxResult.Ok Then
                    'do nothing
                Else
                    Me.Enabled = True
                    Me.Cursor = Cursors.Arrow
                    Exit Sub
                End If
            End If

            Dim oTransactionID As Integer = Me.dgvInvoices.SelectedRows(0).Cells(0).Value

            Call DeleteInvoice(oTransactionID)
            Call DeleteInvoicePaymentsCreditsDebits(oTransactionID)
            Me.dgvInvoices.Refresh()
            Me.dgvInvoices.DataSource = Nothing
            MessageBox.Show("Invoice succesfully deleted", "Invoice Removal", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LoadInvoices(cmbSupplier.SelectedValue, dtpFrom.Value, dtpTo.Value)

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

    Sub DeleteInvoice(ByVal TransactionID As String)
        If TransactionID = "0" Then
            MsgBox("Voiding of this payment has been cancelled due to the TransactionID not being valid. Please take note of what you have been doing and contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
            Exit Sub
        End If
        If TransactionID = "" Then
            MsgBox("Voiding of this payment has been cancelled due to the TransactionID not being valid. Please take note of what you have been doing and contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
            Exit Sub
        End If

        Dim sSQL As String
        Me.Enabled = False
        Me.Cursor = Cursors.AppStarting

        Try
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True WHERE TransactionID = " & TransactionID
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET Void = 1 WHERE TransactionID = " & TransactionID
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            End If

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("There was a problem deleting an invoice " & Err.Description & "029a")
        End Try


    End Sub


    Sub DeleteInvoicePaymentsCreditsDebits(ByVal LinkID As String)
        If LinkID = "0" Then
            MsgBox("Voiding of this payment has been cancelled due to the LinkID not being valid. Please take note of what you have been doing and contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
            Exit Sub
        End If
        If LinkID = "" Then
            MsgBox("Voiding of this payment has been cancelled due to the LinkID not being valid. Please take note of what you have been doing and contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
            Exit Sub
        End If
        Me.Enabled = False
        Me.Cursor = Cursors.AppStarting

        Dim sSQL As String

        Try
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True WHERE LinkID = " & LinkID
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET Void = 1 WHERE LinkID = " & LinkID
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If


            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            End If

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox("There was a problem deleting payments relating to an invoice " & ex.Message & " 029")
        End Try

    End Sub
    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click

        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            If Me.dgvInvoices.SelectedCells.Count < 1 Then
                MessageBox.Show("Please select an invoice in order to continue with Editing!", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            '  If Me.Label_Level.Text <> "Super" Then
            ' MessageBox.Show("You need to activate the required user level to edit an invoice that was not captured today!", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ' Me.Enabled = True
            ' Me.Cursor = Cursors.Arrow
            ' Exit Sub
            ' End If

            If Me.dgvInvoices.SelectedRows.Count < 1 Then
                Me.dgvInvoices.Rows(Me.dgvInvoices.SelectedCells(0).RowIndex).Selected = True
            End If

            Dim oRowIndex As Integer
            oRowIndex = Me.dgvInvoices.SelectedRows(0).Index

            Dim oAmount As Decimal
            oAmount = Me.dgvInvoices.SelectedRows(0).Cells(3).Value
            Dim oDue As Decimal
            oDue = Me.dgvInvoices.SelectedRows(0).Cells(4).Value
            If oDue <> oAmount Then
                'If Me.Label_Level.Text <> "Super" Then
                MessageBox.Show("Payments have been made on this invoice. In order to proceed please login to activate", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
                'End If
            End If

            Dim oTransactionDate As Date = CDate(Me.dgvInvoices.SelectedRows(0).Cells(5).Value)
            'Add period check here

            'If oTransactionDate < Now.Date Then
            ' If Me.Label_Level.Text <> "Super" Then
            ' MessageBox.Show("You need to activate the required user level to delete an invoice that was not captured today!", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ' Me.Enabled = True
            ' Me.Cursor = Cursors.Arrow
            'Exit Sub
            'End If
            'End If

            Dim TransactionID As String = Me.dgvInvoices.SelectedRows(0).Cells("TransactionID").Value

            If CheckForRequests(TransactionID) > 0 Then
                MessageBox.Show("There are payment(s) request against this invoice, please pay/remove the requests before you attempt to edit this invoice", _
                                "Invoice Movements", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            Dim sSupplierName As String = CStr(cmbSupplier.Text)
            Dim oReference As String = Me.dgvInvoices.SelectedRows(0).Cells("Reference").Value

            'Don't allow an invoice with credits or supplier debits passed against it to be edited
            Dim oCount_Credit_Notes_against_Invoice As Integer = oCount_Credit_Notes(sSupplierName, oReference)
            If oCount_Credit_Notes_against_Invoice > 0 Then
                MessageBox.Show("There are credit notes against this invoice. It cannot be edited.", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            Dim oCount_Supplier_Debits_Notes_against_Invoice As Integer = oCount_Supplier_Debits(sSupplierName, oReference)
            If oCount_Supplier_Debits_Notes_against_Invoice > 0 Then
                MessageBox.Show("There is a supplier debit against this invoice. It cannot be edited.", "Invoice Editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Enabled = True
                Me.Cursor = Cursors.Arrow
                Exit Sub
            End If

            Dim oSupplierID As Integer
            oSupplierID = CInt(cmbSupplier.SelectedValue)
            'Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") 'Supplier Name

            Dim oTransactionID As Integer = Me.dgvInvoices.SelectedRows(0).Cells("TransactionID").Value   'GetTransactionID_FromID(oID)

            'Now delete invoice lines which will have said TransactionID as well as related payments which will have this in their LinkID column
            Call Fill_DGV_Transaction(oTransactionID)

            frmInvcEdit = New frmInvoiceEdit
            frmInvcEdit.Lbl_OrigTxID.Text = TransactionID
            frmInvcEdit.oFormLoad()
            frmInvcEdit.CB_SupplierName.Text = sSupplierName
            frmInvcEdit.TB_InvNo.Text = oReference
            frmInvcEdit.DTP_InvoiceDate.Text = oTransactionDate
            frmInvcEdit.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Invoice Editing -> Version : " & My.Settings.Setting_Version

            ' frmInvcEdit.BringToFront()

            'Populate_Invoice_Capture_Form()

            'frmInvcCapture.Save1.Visible = True
            'frmInvcCapture.lblEditInvoice.Visible = True
            'frmInvcCapture.lblEditInvoice_TransactionID.Text = CStr(oTransactionID)
            'frmInvcCapture.BringToFront()

            Enabled = True
            Cursor = Cursors.Arrow
            frmInvcEdit.ShowDialog()
            Close()

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured in invoice editing screen :" & _
                            ex.Message, "Invoice editing", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub Fill_DGV_Transaction(ByVal oTransactionID As String)
        Try
            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            'First clear DGV
            DataGridView2.DataSource = Nothing

            Dim sSQL As String

            sSQL = "Select * From Transactions"
            sSQL = sSQL & " Where TransactionID = " & oTransactionID
            sSQL = sSQL & " And [Tax Amount] <> " & 0
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            sSQL = sSQL & " Order By [ID]"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Transaction_table")
                connection.Close()
                DataGridView2.DataSource = ds
                DataGridView2.DataMember = "Transaction_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Transaction_table")
                connection.Close()
                DataGridView2.DataSource = ds
                DataGridView2.DataMember = "Transaction_table"
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 0mm57")
        End Try
    End Sub

    Sub Populate_Invoice_Capture_Form()
        Try
            Dim oInvoiceDate As String
            Dim oSupplierID As Integer
            Dim oInvoiceNo As String
            Dim oInvoiceTotal As String

            Dim oVATAmt As String
            Dim oVATType As String
            Dim exVAT As String

            Dim oCat1 As String
            Dim oCat2 As String
            Dim oCat3 As String

            Dim tid As Integer = CInt(Me.DataGridView2.Rows(0).Cells("TransactionID").Value)
            Dim oGrossAmount As String 'Only multiples
            Dim PaymentStatus As Boolean = CBool(MBData.InvoiceHasPayment(tid))
            If PaymentStatus = True Then
                frmInvcCapture.ComboBox_Status.Text = "PAID"
            Else
                frmInvcCapture.ComboBox_Status.Text = "UNPAID"
            End If

            oInvoiceDate = Me.DataGridView2.Rows(0).Cells("Transaction Date").Value
            frmInvcCapture.InvoiceDate_DateTimePicker.Value = CDate(oInvoiceDate)
            oSupplierID = CStr(cmbSupplier.SelectedValue)
            frmInvcCapture.cmbSupplier.SelectedValue = CInt(oSupplierID)
            oInvoiceNo = Me.DataGridView2.Rows(0).Cells("Reference").Value
            frmInvcCapture.txtInvoiceNumber.Text = oInvoiceNo

            Dim AmountValue As Decimal = 0
            Dim TaxValue As Decimal = 0
            For Each row As DataGridViewRow In DataGridView2.Rows
                AmountValue += CDec(row.Cells("Amount").Value)
                TaxValue += CDec(row.Cells("Tax Amount").Value)
            Next
            oInvoiceTotal = CDec(AmountValue + TaxValue)
            frmInvcCapture.InvoiceTotal_TextBox.Text = oInvoiceTotal

            If Me.DataGridView2.RowCount = 4 Then 'DGV adds on extra line

                frmInvcCapture.MultipleLines_CheckBox.Checked = False

                oVATAmt = Me.DataGridView2.Rows(1).Cells("Tax Amount").Value
                frmInvcCapture.txtVATAmount.Text = oVATAmt
                oVATType = Me.DataGridView2.Rows(1).Cells("TaxType").Value
                frmInvcCapture.cmbVATType.Text = oVATType
                exVAT = Me.DataGridView2.Rows(1).Cells(13).Value
                frmInvcCapture.txtNonVATAmount.Text = exVAT
                oCat1 = Me.DataGridView2.Rows(1).Cells(10).Value
                frmInvcCapture.Cat1.Text = oCat1
                oCat2 = Me.DataGridView2.Rows(1).Cells(19).Value
                frmInvcCapture.Cat2.Text = oCat2
                oCat3 = Me.DataGridView2.Rows(1).Cells(20).Value
                frmInvcCapture.Cat3.Text = oCat3
            Else

                frmInvcCapture.MultipleLines_CheckBox.Checked = True

                For i As Integer = 1 To Me.DataGridView2.RowCount - 2 Step 2
                    oVATAmt = Me.DataGridView2.Rows(i).Cells(15).Value
                    oVATType = Me.DataGridView2.Rows(i).Cells(14).Value
                    exVAT = Me.DataGridView2.Rows(i).Cells(13).Value
                    oGrossAmount = CStr(CDec(exVAT) + CDec(oVATAmt))
                    oCat1 = Me.DataGridView2.Rows(i).Cells(10).Value
                    oCat2 = Me.DataGridView2.Rows(i).Cells(19).Value
                    oCat3 = Me.DataGridView2.Rows(i).Cells(20).Value


                    Dim oCol1 As String = oGrossAmount
                    Dim oCol2 As String = "" ''Me.txtCredit.Text 'Credit
                    Dim oCol3 As String = oVATType
                    Dim oCol4 As String = oVATAmt
                    Dim oCol5 As String = "" 'Me.txtCrVAT.Text 'Credit VAT
                    Dim oCol6 As String = exVAT 'Me.NonVAT_Amount_TextBox.Text
                    Dim oCol7 As String = oCat1
                    Dim oCol8 As String = oCat2
                    Dim oCol9 As String = oCat3
                    Dim oCol10 As String = "" 'Me.lblMagicBox_Account_Segment4.Text


                    Dim str(9) As String
                    Dim itm As ListViewItem
                    str(0) = oCol1
                    str(1) = oCol2
                    str(2) = oCol3
                    str(3) = oCol4
                    str(4) = oCol5
                    str(5) = oCol6
                    str(6) = oCol7
                    str(7) = oCol8
                    str(8) = oCol9
                    str(9) = oCol10

                    itm = New ListViewItem(str)
                    frmInvcCapture.ListView1.Items.Add(itm)
                Next
            End If
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Function oCount_Credit_Notes(ByVal oSupplierName As String, ByVal oReference As String) As Integer

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "Access" Then

                Dim sSQL As String

                sSQL = "SELECT Count(*) As MyCount FROM Transactions WHERE [SupplierName]='" & oSupplierName & "'"
                sSQL = sSQL & " And Reference = '" & oReference & "' And Info2 = 'Credit Note'"
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())
                oCount_Credit_Notes = oCount
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Transactions WHERE [SupplierName]='" & oSupplierName & "'"
                sSQL = sSQL & " And Reference = '" & oReference & "' And Info2 = 'Credit Note'"
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())
                oCount_Credit_Notes = oCount

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error checking for credit notes against an invoice! " & ex.Message & ". tatw")
        End Try
    End Function

    Function oCount_Supplier_Debits(ByVal oSupplierName As String, ByVal oReference As String) As Integer

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "Access" Then

                Dim sSQL As String

                sSQL = "SELECT Count(*) As MyCount FROM Transactions WHERE [SupplierName]='" & oSupplierName & "'"
                sSQL = sSQL & " And Reference = '" & oReference & "' And Info2 = 'Supplier Debit'"
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())
                oCount_Supplier_Debits = oCount
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If

            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Transactions WHERE [SupplierName]='" & oSupplierName & "'"
                sSQL = sSQL & " And Reference = '" & oReference & "' And Info2 = 'Supplier Debit'"
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())
                oCount_Supplier_Debits = oCount

                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error checking for credit notes against an invoice! " & ex.Message & ". tatw")
        End Try
    End Function
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Public Sub LoadInvoices(ByVal SupplierID As Integer, ByVal fromDate As Date, ByVal toDate As Date)

        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim d1 As String = fromDate.ToString("dd MMMM yyyy")
            Dim d2 As String = toDate.ToString("dd MMMM yyyy")

            Dim sSQL As String = ""
            sSQL = CStr("Select * from dbo.Func_GeneralInvoicesQuery(" & My.Settings.Setting_CompanyID) & ", " _
                & SupplierID & ",'" & d1 & "','" & d2 & "')"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "SupplierInvoices")
            connection.Close()
            dgvInvoices.DataSource = ds
            dgvInvoices.DataMember = "SupplierInvoices"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Sub FillSupplierCombobox()
        Try

            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class