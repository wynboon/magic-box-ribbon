﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS


Public Class frmGround_to_Air

    Private Sub frmGround_to_Air_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Ground to Air -> Version : " & My.Settings.Setting_Version

    End Sub

    Sub Read_Transaction_Data(ByVal oAboveID As Integer)

        Dim oBatchID As Integer
        Dim oTransactionID As Integer
        Dim oLinkID As Integer
        Dim dTransactionDate As Date
        Dim dCaptureDate As Date
        Dim sPPeriod As String
        Dim oFinYear As Integer
        Dim sGDC As String
        Dim sInvoiceNumber As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As Decimal
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As String
        Dim oEmployeeID As Integer
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim sSupplierName As Integer
        Dim oCo_ID As Long
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String


        Try

            Dim sSQL As String
            sSQL = "SELECT * FROM Transactions WHERE ID > " & oAboveID

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                While datareader.Read
                    If Not datareader("ID").Equals(DBNull.Value) Then
                        oAboveID = datareader("ID")
                    Else
                        MsgBox("The invoice at the line you clicked does not exist")
                        connection.Close()
                        Exit Sub
                    End If
                    If Not datareader("BatchID").Equals(DBNull.Value) Then
                        oBatchID = datareader("BatchID")
                    Else

                    End If
                    If Not datareader("TransactionID").Equals(DBNull.Value) Then
                        oTransactionID = datareader("TransactionID")
                    Else

                    End If
                    If Not datareader("LinkID").Equals(DBNull.Value) Then
                        oLinkID = datareader("LinkID")
                    Else

                    End If

                    If Not datareader("Capture Date").Equals(DBNull.Value) Then
                        dCaptureDate = datareader("Capture Date")

                    End If

                    If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                        dTransactionDate = datareader("Transaction Date")
                    Else
                    End If

                    If Not datareader("PPeriod").Equals(DBNull.Value) Then
                        sPPeriod = datareader("PPeriod")
                    End If

                    If Not datareader("Fin Year").Equals(DBNull.Value) Then
                        oFinYear = datareader("Fin Year")
                    End If

                    If Not datareader("GDC").Equals(DBNull.Value) Then
                        sGDC = datareader("GDC")
                    Else

                    End If
                    If Not datareader("Reference").Equals(DBNull.Value) Then
                        sInvoiceNumber = datareader("Reference")
                    Else

                    End If
                    If Not datareader("Description").Equals(DBNull.Value) Then
                        sDescription = datareader("Description")
                    Else

                    End If
                    If Not datareader("AccNumber").Equals(DBNull.Value) Then
                        sAccountNumber = datareader("AccNumber")
                    Else

                    End If
                    If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                        sLinkAcc = datareader("LinkAcc")
                    Else

                    End If
                    If Not datareader("Amount").Equals(DBNull.Value) Then
                        oAmount = datareader("Amount")
                    Else


                    End If
                    If Not datareader("TaxType").Equals(DBNull.Value) Then
                        oTaxType = datareader("TaxType")
                    Else


                    End If
                    If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                        oTaxAmount = datareader("Tax Amount")
                    Else

                    End If
                    If Not datareader("UserID").Equals(DBNull.Value) Then
                        sUserID = datareader("UserID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        oSupplierID = datareader("SupplierID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                        oEmployeeID = datareader("EmployeeID")
                    Else

                    End If
                    If Not datareader("Description 2").Equals(DBNull.Value) Then
                        sDescription2 = datareader("Description 2")
                    Else

                    End If
                    If Not datareader("Description 3").Equals(DBNull.Value) Then
                        sDescription3 = datareader("Description 3")
                    Else

                    End If
                    If Not datareader("Description 4").Equals(DBNull.Value) Then
                        sDescription4 = datareader("Description 4")
                    Else

                    End If
                    If Not datareader("Description 5").Equals(DBNull.Value) Then
                        sDescription5 = datareader("Description 5")
                    Else

                    End If
                    If Not datareader("Posted").Equals(DBNull.Value) Then
                        blnPosted = datareader("Posted")
                    Else

                    End If
                    If Not datareader("Accounting").Equals(DBNull.Value) Then
                        blnAccounting = datareader("Accounting")
                    Else

                    End If
                    If Not datareader("Void").Equals(DBNull.Value) Then
                        blnVoid = datareader("Void")
                    Else

                    End If
                    If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                        sTransactionType = datareader("Transaction Type").ToString
                    Else

                    End If
                    If Not datareader("DR_CR").Equals(DBNull.Value) Then
                        oDR_CR = datareader("DR_CR").ToString
                    End If

                    If Not datareader("Contra Account").Equals(DBNull.Value) Then
                        oContraAccount = datareader("Contra Account").ToString
                    End If

                    If Not datareader("Description Code").Equals(DBNull.Value) Then
                        oDescriptionCode = datareader("Description Code").ToString
                    End If

                    If Not datareader("DocType").Equals(DBNull.Value) Then
                        oDocType = datareader("DocType").ToString
                    End If

                    If Not datareader("SupplierName").Equals(DBNull.Value) Then
                        sSupplierName = datareader("SupplierName").ToString
                    End If

                    If Not datareader("Co_ID").Equals(DBNull.Value) Then
                        oCo_ID = datareader("Co_ID").ToString
                    End If

                    If Not datareader("Info").Equals(DBNull.Value) Then
                        oInfo = datareader("Info").ToString
                    End If

                    If Not datareader("Info2").Equals(DBNull.Value) Then
                        oInfo2 = datareader("Info2").ToString
                    End If

                    If Not datareader("Info3").Equals(DBNull.Value) Then
                        oInfo3 = datareader("Info3").ToString
                    End If

                    Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, dTransactionDate, dCaptureDate, sPPeriod, oFinYear, sGDC, sInvoiceNumber, _
sDescription, sAccountNumber, sLinkAcc, oAmount, oTaxType, oTaxAmount, sUserID, _
oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
blnAccounting, blnVoid, sTransactionType, oDR_CR, oContraAccount, oDescriptionCode, oDocType, sSupplierName, oInfo, oInfo2, oInfo3)



                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader


                While datareader.Read
                    If Not datareader("ID").Equals(DBNull.Value) Then
                        oAboveID = datareader("ID")

                    End If
                    If Not datareader("BatchID").Equals(DBNull.Value) Then
                        oBatchID = datareader("BatchID")
                    Else

                    End If
                    If Not datareader("TransactionID").Equals(DBNull.Value) Then
                        oTransactionID = datareader("TransactionID")
                    Else

                    End If
                    If Not datareader("LinkID").Equals(DBNull.Value) Then
                        oLinkID = datareader("LinkID")
                    Else

                    End If

                    If Not datareader("Capture Date").Equals(DBNull.Value) Then
                        dCaptureDate = datareader("Capture Date")
                    Else
                    End If

                    If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                        dTransactionDate = datareader("Transaction Date")
                    Else
                    End If

                    If Not datareader("PPeriod").Equals(DBNull.Value) Then
                        sPPeriod = datareader("PPeriod")
                    End If

                    If Not datareader("Fin Year").Equals(DBNull.Value) Then
                        oFinYear = datareader("Fin Year")
                    End If

                    If Not datareader("GDC").Equals(DBNull.Value) Then
                        sGDC = datareader("GDC")
                    Else

                    End If
                    If Not datareader("Reference").Equals(DBNull.Value) Then
                        sInvoiceNumber = datareader("Reference")
                    Else

                    End If
                    If Not datareader("Description").Equals(DBNull.Value) Then
                        sDescription = datareader("Description")
                    Else

                    End If
                    If Not datareader("AccNumber").Equals(DBNull.Value) Then
                        sAccountNumber = datareader("AccNumber")
                    Else

                    End If
                    If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                        sLinkAcc = datareader("LinkAcc")
                    Else

                    End If
                    If Not datareader("Amount").Equals(DBNull.Value) Then
                        oAmount = datareader("Amount")
                    Else


                    End If
                    If Not datareader("TaxType").Equals(DBNull.Value) Then
                        oTaxType = datareader("TaxType")
                    Else


                    End If
                    If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                        oTaxAmount = datareader("Tax Amount")
                    Else

                    End If
                    If Not datareader("UserID").Equals(DBNull.Value) Then
                        sUserID = datareader("UserID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        oSupplierID = datareader("SupplierID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                        oEmployeeID = datareader("EmployeeID")
                    Else

                    End If
                    If Not datareader("Description 2").Equals(DBNull.Value) Then
                        sDescription2 = datareader("Description 2")
                    Else

                    End If
                    If Not datareader("Description 3").Equals(DBNull.Value) Then
                        sDescription3 = datareader("Description 3")
                    Else

                    End If
                    If Not datareader("Description 4").Equals(DBNull.Value) Then
                        sDescription4 = datareader("Description 4")
                    Else

                    End If
                    If Not datareader("Description 5").Equals(DBNull.Value) Then
                        sDescription5 = datareader("Description 5")
                    Else

                    End If
                    If Not datareader("Posted").Equals(DBNull.Value) Then
                        blnPosted = datareader("Posted")
                    Else

                    End If
                    If Not datareader("Accounting").Equals(DBNull.Value) Then
                        blnAccounting = datareader("Accounting")
                    Else

                    End If
                    If Not datareader("Void").Equals(DBNull.Value) Then
                        blnVoid = datareader("Void")
                    Else

                    End If
                    If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                        sTransactionType = datareader("Transaction Type").ToString
                    Else

                    End If
                    If Not datareader("DR_CR").Equals(DBNull.Value) Then
                        oDR_CR = datareader("DR_CR").ToString
                    End If

                    If Not datareader("Contra Account").Equals(DBNull.Value) Then
                        oContraAccount = datareader("Contra Account").ToString
                    End If

                    If Not datareader("Description Code").Equals(DBNull.Value) Then
                        oDescriptionCode = datareader("Description Code").ToString
                    End If

                    If Not datareader("DocType").Equals(DBNull.Value) Then
                        oDocType = datareader("DocType").ToString
                    End If

                    If Not datareader("SupplierName").Equals(DBNull.Value) Then
                        sSupplierName = datareader("SupplierName").ToString
                    End If

                    If Not datareader("Co_ID").Equals(DBNull.Value) Then
                        oCo_ID = datareader("Co_ID").ToString
                    End If

                    If Not datareader("Info").Equals(DBNull.Value) Then
                        oInfo = datareader("Info").ToString
                    End If

                    If Not datareader("Info2").Equals(DBNull.Value) Then
                        oInfo2 = datareader("Info2").ToString
                    End If

                    If Not datareader("Info3").Equals(DBNull.Value) Then
                        oInfo3 = datareader("Info3").ToString
                    End If


                End While
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " a3")
        End Try
    End Sub


    Public Sub Add_Transaction_to_DGV(ByVal oWhich_DGV As String, ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                  ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                  ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                  ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                  ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                  ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                  ByVal oDocType As String, ByVal oSupplierName As String, ByVal oInfo As String, ByVal oInfo2 As String, ByVal oInfo3 As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)
            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName
            newRow.Cells(31).Value = oInfo
            newRow.Cells(32).Value = oInfo2
            newRow.Cells(33).Value = oInfo3

            If oWhich_DGV = "DGV_Transactions" Then
                Me.DGV_Transactions.Rows.Add(newRow)
            Else
                MsgBox("ERROR ON DGV CLEANUP")
            End If

        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox(ex.Message & " 048")
        End Try


    End Sub
    Sub AppendTransactions_Access(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String


        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value

                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value

                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 049")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String

        Dim i As Integer
        Dim cmd As SqlCommand


        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value
                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value
                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 050")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub
End Class