﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSupplierNew
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSupplierNew))
        Me.DetailDefault_ComboBox = New System.Windows.Forms.ComboBox()
        Me.CategoryDefault_ComboBox = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.DetailDefault2_ComboBox = New System.Windows.Forms.ComboBox()
        Me.Fax_TextBox = New System.Windows.Forms.TextBox()
        Me.EmailAddress_TextBox = New System.Windows.Forms.TextBox()
        Me.ContactPerson_TextBox = New System.Windows.Forms.TextBox()
        Me.ContactNo_TextBox = New System.Windows.Forms.TextBox()
        Me.RefID_TextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ContactNo_label = New System.Windows.Forms.Label()
        Me.RefID_label = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SupplierName_TextBox = New System.Windows.Forms.TextBox()
        Me.Button_AddSupplier = New System.Windows.Forms.Button()
        Me.cmbVAT = New System.Windows.Forms.ComboBox()
        Me.Address_TextBox = New System.Windows.Forms.RichTextBox()
        Me.Label_Saved = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.NudTerms = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Lbl_IntegrationCode = New System.Windows.Forms.Label()
        CType(Me.NudTerms, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DetailDefault_ComboBox
        '
        Me.DetailDefault_ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.DetailDefault_ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.DetailDefault_ComboBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DetailDefault_ComboBox.FormattingEnabled = True
        Me.DetailDefault_ComboBox.Location = New System.Drawing.Point(189, 172)
        Me.DetailDefault_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.DetailDefault_ComboBox.Name = "DetailDefault_ComboBox"
        Me.DetailDefault_ComboBox.Size = New System.Drawing.Size(421, 24)
        Me.DetailDefault_ComboBox.TabIndex = 4
        '
        'CategoryDefault_ComboBox
        '
        Me.CategoryDefault_ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.CategoryDefault_ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CategoryDefault_ComboBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CategoryDefault_ComboBox.FormattingEnabled = True
        Me.CategoryDefault_ComboBox.Location = New System.Drawing.Point(189, 135)
        Me.CategoryDefault_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.CategoryDefault_ComboBox.Name = "CategoryDefault_ComboBox"
        Me.CategoryDefault_ComboBox.Size = New System.Drawing.Size(421, 24)
        Me.CategoryDefault_ComboBox.TabIndex = 3
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(17, 213)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(132, 18)
        Me.Label17.TabIndex = 88
        Me.Label17.Text = "Category Level 3:"
        '
        'DetailDefault2_ComboBox
        '
        Me.DetailDefault2_ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.DetailDefault2_ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.DetailDefault2_ComboBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DetailDefault2_ComboBox.FormattingEnabled = True
        Me.DetailDefault2_ComboBox.Location = New System.Drawing.Point(189, 209)
        Me.DetailDefault2_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.DetailDefault2_ComboBox.Name = "DetailDefault2_ComboBox"
        Me.DetailDefault2_ComboBox.Size = New System.Drawing.Size(421, 24)
        Me.DetailDefault2_ComboBox.TabIndex = 5
        '
        'Fax_TextBox
        '
        Me.Fax_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fax_TextBox.Location = New System.Drawing.Point(189, 455)
        Me.Fax_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.Fax_TextBox.Name = "Fax_TextBox"
        Me.Fax_TextBox.Size = New System.Drawing.Size(421, 23)
        Me.Fax_TextBox.TabIndex = 11
        '
        'EmailAddress_TextBox
        '
        Me.EmailAddress_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailAddress_TextBox.Location = New System.Drawing.Point(189, 418)
        Me.EmailAddress_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.EmailAddress_TextBox.Name = "EmailAddress_TextBox"
        Me.EmailAddress_TextBox.Size = New System.Drawing.Size(421, 23)
        Me.EmailAddress_TextBox.TabIndex = 10
        '
        'ContactPerson_TextBox
        '
        Me.ContactPerson_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactPerson_TextBox.Location = New System.Drawing.Point(189, 246)
        Me.ContactPerson_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.ContactPerson_TextBox.Name = "ContactPerson_TextBox"
        Me.ContactPerson_TextBox.Size = New System.Drawing.Size(421, 23)
        Me.ContactPerson_TextBox.TabIndex = 6
        '
        'ContactNo_TextBox
        '
        Me.ContactNo_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactNo_TextBox.Location = New System.Drawing.Point(189, 101)
        Me.ContactNo_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.ContactNo_TextBox.Name = "ContactNo_TextBox"
        Me.ContactNo_TextBox.Size = New System.Drawing.Size(421, 23)
        Me.ContactNo_TextBox.TabIndex = 2
        '
        'RefID_TextBox
        '
        Me.RefID_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefID_TextBox.Location = New System.Drawing.Point(189, 65)
        Me.RefID_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.RefID_TextBox.Name = "RefID_TextBox"
        Me.RefID_TextBox.Size = New System.Drawing.Size(421, 23)
        Me.RefID_TextBox.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(17, 422)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(118, 18)
        Me.Label12.TabIndex = 87
        Me.Label12.Text = "Email Address :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(17, 459)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(44, 18)
        Me.Label13.TabIndex = 86
        Me.Label13.Text = "Fax :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 139)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(132, 18)
        Me.Label9.TabIndex = 85
        Me.Label9.Text = "Category Level 1:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(17, 176)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(132, 18)
        Me.Label10.TabIndex = 84
        Me.Label10.Text = "Category Level 2:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(17, 352)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(77, 18)
        Me.Label11.TabIndex = 82
        Me.Label11.Text = "Address :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(17, 289)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 18)
        Me.Label6.TabIndex = 77
        Me.Label6.Text = "VAT Default :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(17, 250)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(127, 18)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Contact Person :"
        '
        'ContactNo_label
        '
        Me.ContactNo_label.AutoSize = True
        Me.ContactNo_label.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactNo_label.Location = New System.Drawing.Point(17, 105)
        Me.ContactNo_label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ContactNo_label.Name = "ContactNo_label"
        Me.ContactNo_label.Size = New System.Drawing.Size(98, 18)
        Me.ContactNo_label.TabIndex = 74
        Me.ContactNo_label.Text = "Contact No :"
        '
        'RefID_label
        '
        Me.RefID_label.AutoSize = True
        Me.RefID_label.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefID_label.Location = New System.Drawing.Point(17, 69)
        Me.RefID_label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RefID_label.Name = "RefID_label"
        Me.RefID_label.Size = New System.Drawing.Size(63, 18)
        Me.RefID_label.TabIndex = 68
        Me.RefID_label.Text = "Ref ID :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(17, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 18)
        Me.Label1.TabIndex = 67
        Me.Label1.Text = "Supplier Name :"
        '
        'SupplierName_TextBox
        '
        Me.SupplierName_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupplierName_TextBox.Location = New System.Drawing.Point(189, 27)
        Me.SupplierName_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.SupplierName_TextBox.Name = "SupplierName_TextBox"
        Me.SupplierName_TextBox.Size = New System.Drawing.Size(421, 23)
        Me.SupplierName_TextBox.TabIndex = 0
        '
        'Button_AddSupplier
        '
        Me.Button_AddSupplier.Location = New System.Drawing.Point(343, 486)
        Me.Button_AddSupplier.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button_AddSupplier.Name = "Button_AddSupplier"
        Me.Button_AddSupplier.Size = New System.Drawing.Size(133, 32)
        Me.Button_AddSupplier.TabIndex = 12
        Me.Button_AddSupplier.Text = "&Add Supplier"
        Me.Button_AddSupplier.UseVisualStyleBackColor = True
        '
        'cmbVAT
        '
        Me.cmbVAT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbVAT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbVAT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVAT.FormattingEnabled = True
        Me.cmbVAT.Items.AddRange(New Object() {"0", "1"})
        Me.cmbVAT.Location = New System.Drawing.Point(189, 284)
        Me.cmbVAT.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbVAT.Name = "cmbVAT"
        Me.cmbVAT.Size = New System.Drawing.Size(239, 24)
        Me.cmbVAT.TabIndex = 7
        '
        'Address_TextBox
        '
        Me.Address_TextBox.Location = New System.Drawing.Point(189, 347)
        Me.Address_TextBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Address_TextBox.Name = "Address_TextBox"
        Me.Address_TextBox.Size = New System.Drawing.Size(239, 58)
        Me.Address_TextBox.TabIndex = 9
        Me.Address_TextBox.Text = ""
        '
        'Label_Saved
        '
        Me.Label_Saved.AutoSize = True
        Me.Label_Saved.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Saved.Location = New System.Drawing.Point(23, 683)
        Me.Label_Saved.Name = "Label_Saved"
        Me.Label_Saved.Size = New System.Drawing.Size(18, 17)
        Me.Label_Saved.TabIndex = 91
        Me.Label_Saved.Text = "--"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(481, 486)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(129, 32)
        Me.btnClose.TabIndex = 13
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'NudTerms
        '
        Me.NudTerms.Location = New System.Drawing.Point(189, 316)
        Me.NudTerms.Margin = New System.Windows.Forms.Padding(4)
        Me.NudTerms.Name = "NudTerms"
        Me.NudTerms.Size = New System.Drawing.Size(240, 22)
        Me.NudTerms.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(17, 320)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 18)
        Me.Label2.TabIndex = 94
        Me.Label2.Text = "Terms :"
        '
        'Lbl_IntegrationCode
        '
        Me.Lbl_IntegrationCode.AutoSize = True
        Me.Lbl_IntegrationCode.Location = New System.Drawing.Point(13, 500)
        Me.Lbl_IntegrationCode.Name = "Lbl_IntegrationCode"
        Me.Lbl_IntegrationCode.Size = New System.Drawing.Size(16, 17)
        Me.Lbl_IntegrationCode.TabIndex = 95
        Me.Lbl_IntegrationCode.Text = "_"
        '
        'frmSupplierNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(625, 523)
        Me.Controls.Add(Me.Lbl_IntegrationCode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.NudTerms)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Label_Saved)
        Me.Controls.Add(Me.Address_TextBox)
        Me.Controls.Add(Me.Button_AddSupplier)
        Me.Controls.Add(Me.cmbVAT)
        Me.Controls.Add(Me.DetailDefault_ComboBox)
        Me.Controls.Add(Me.CategoryDefault_ComboBox)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.DetailDefault2_ComboBox)
        Me.Controls.Add(Me.Fax_TextBox)
        Me.Controls.Add(Me.EmailAddress_TextBox)
        Me.Controls.Add(Me.ContactPerson_TextBox)
        Me.Controls.Add(Me.ContactNo_TextBox)
        Me.Controls.Add(Me.RefID_TextBox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ContactNo_label)
        Me.Controls.Add(Me.RefID_label)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SupplierName_TextBox)
        Me.ForeColor = System.Drawing.Color.DarkBlue
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSupplierNew"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Supplier"
        CType(Me.NudTerms, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DetailDefault_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CategoryDefault_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents DetailDefault2_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Fax_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailAddress_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContactPerson_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContactNo_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents RefID_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ContactNo_label As System.Windows.Forms.Label
    Friend WithEvents RefID_label As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SupplierName_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button_AddSupplier As System.Windows.Forms.Button
    Friend WithEvents cmbVAT As System.Windows.Forms.ComboBox
    Friend WithEvents Address_TextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents Label_Saved As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents NudTerms As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Lbl_IntegrationCode As System.Windows.Forms.Label
End Class
