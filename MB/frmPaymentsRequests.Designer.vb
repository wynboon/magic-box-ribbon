﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaymentsRequests
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaymentsRequests))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvPaymentsRequests = New System.Windows.Forms.DataGridView()
        Me.PaymentsRequestBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnResetList = New System.Windows.Forms.Button()
        Me.btnFilter = New System.Windows.Forms.Button()
        Me.cmbPaymentType = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbSupplier = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtRequests = New System.Windows.Forms.TextBox()
        Me.SupplierName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvoiceNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvoiceTransactionIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CapturedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestedAmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestApprovedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApprovedAmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentTransactionIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestOwnerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestApproverDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvPaymentsRequests, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PaymentsRequestBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.dgvPaymentsRequests)
        Me.Panel1.Location = New System.Drawing.Point(5, 78)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(909, 433)
        Me.Panel1.TabIndex = 0
        '
        'dgvPaymentsRequests
        '
        Me.dgvPaymentsRequests.AllowUserToAddRows = False
        Me.dgvPaymentsRequests.AllowUserToDeleteRows = False
        Me.dgvPaymentsRequests.AllowUserToOrderColumns = True
        Me.dgvPaymentsRequests.AutoGenerateColumns = False
        Me.dgvPaymentsRequests.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPaymentsRequests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPaymentsRequests.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SupplierName, Me.InvoiceNumber, Me.RequestIDDataGridViewTextBoxColumn, Me.InvoiceTransactionIDDataGridViewTextBoxColumn, Me.CapturedDateDataGridViewTextBoxColumn, Me.PaymentTypeDataGridViewTextBoxColumn, Me.RequestedAmountDataGridViewTextBoxColumn, Me.RequestApprovedDataGridViewTextBoxColumn, Me.ApprovedAmountDataGridViewTextBoxColumn, Me.PaymentTransactionIDDataGridViewTextBoxColumn, Me.RequestOwnerDataGridViewTextBoxColumn, Me.RequestApproverDataGridViewTextBoxColumn, Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn, Me.CoIDDataGridViewTextBoxColumn})
        Me.dgvPaymentsRequests.DataSource = Me.PaymentsRequestBindingSource
        Me.dgvPaymentsRequests.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPaymentsRequests.Location = New System.Drawing.Point(0, 0)
        Me.dgvPaymentsRequests.Name = "dgvPaymentsRequests"
        Me.dgvPaymentsRequests.Size = New System.Drawing.Size(909, 433)
        Me.dgvPaymentsRequests.TabIndex = 0
        '
        'PaymentsRequestBindingSource
        '
        Me.PaymentsRequestBindingSource.DataSource = GetType(MB.PaymentsRequest)
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(839, 517)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.cmbPaymentType)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.dtpTo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.dtpFrom)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmbSupplier)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(909, 60)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filter Options"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnResetList)
        Me.GroupBox2.Controls.Add(Me.btnFilter)
        Me.GroupBox2.Location = New System.Drawing.Point(725, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(178, 48)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options"
        '
        'btnResetList
        '
        Me.btnResetList.Location = New System.Drawing.Point(91, 18)
        Me.btnResetList.Name = "btnResetList"
        Me.btnResetList.Size = New System.Drawing.Size(79, 23)
        Me.btnResetList.TabIndex = 139
        Me.btnResetList.Text = "&Reset List"
        Me.btnResetList.UseVisualStyleBackColor = True
        '
        'btnFilter
        '
        Me.btnFilter.Location = New System.Drawing.Point(6, 18)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(79, 23)
        Me.btnFilter.TabIndex = 136
        Me.btnFilter.Text = "&Filter List"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'cmbPaymentType
        '
        Me.cmbPaymentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbPaymentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPaymentType.Location = New System.Drawing.Point(295, 26)
        Me.cmbPaymentType.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbPaymentType.Name = "cmbPaymentType"
        Me.cmbPaymentType.Size = New System.Drawing.Size(102, 21)
        Me.cmbPaymentType.TabIndex = 138
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(217, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 137
        Me.Label4.Text = "Payment Type :"
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(599, 27)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(119, 20)
        Me.dtpTo.TabIndex = 135
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(570, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 13)
        Me.Label3.TabIndex = 134
        Me.Label3.Text = "To :"
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(443, 27)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(121, 20)
        Me.dtpFrom.TabIndex = 133
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(404, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 13)
        Me.Label2.TabIndex = 132
        Me.Label2.Text = "From :"
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSupplier.Location = New System.Drawing.Point(84, 25)
        Me.cmbSupplier.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(126, 21)
        Me.cmbSupplier.TabIndex = 131
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Supplier name :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(487, 522)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Total Amount :"
        '
        'txtRequests
        '
        Me.txtRequests.Location = New System.Drawing.Point(572, 519)
        Me.txtRequests.Name = "txtRequests"
        Me.txtRequests.ReadOnly = True
        Me.txtRequests.Size = New System.Drawing.Size(137, 20)
        Me.txtRequests.TabIndex = 4
        '
        'SupplierName
        '
        Me.SupplierName.HeaderText = "Supplier Name"
        Me.SupplierName.Name = "SupplierName"
        Me.SupplierName.ReadOnly = True
        '
        'InvoiceNumber
        '
        Me.InvoiceNumber.HeaderText = "Invoice Number"
        Me.InvoiceNumber.Name = "InvoiceNumber"
        Me.InvoiceNumber.ReadOnly = True
        '
        'RequestIDDataGridViewTextBoxColumn
        '
        Me.RequestIDDataGridViewTextBoxColumn.DataPropertyName = "RequestID"
        Me.RequestIDDataGridViewTextBoxColumn.HeaderText = "RequestID"
        Me.RequestIDDataGridViewTextBoxColumn.Name = "RequestIDDataGridViewTextBoxColumn"
        Me.RequestIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.RequestIDDataGridViewTextBoxColumn.Visible = False
        '
        'InvoiceTransactionIDDataGridViewTextBoxColumn
        '
        Me.InvoiceTransactionIDDataGridViewTextBoxColumn.DataPropertyName = "InvoiceTransactionID"
        Me.InvoiceTransactionIDDataGridViewTextBoxColumn.HeaderText = "InvoiceTransactionID"
        Me.InvoiceTransactionIDDataGridViewTextBoxColumn.Name = "InvoiceTransactionIDDataGridViewTextBoxColumn"
        Me.InvoiceTransactionIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.InvoiceTransactionIDDataGridViewTextBoxColumn.Visible = False
        '
        'CapturedDateDataGridViewTextBoxColumn
        '
        Me.CapturedDateDataGridViewTextBoxColumn.DataPropertyName = "CapturedDate"
        Me.CapturedDateDataGridViewTextBoxColumn.HeaderText = "Captured Date"
        Me.CapturedDateDataGridViewTextBoxColumn.Name = "CapturedDateDataGridViewTextBoxColumn"
        Me.CapturedDateDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PaymentTypeDataGridViewTextBoxColumn
        '
        Me.PaymentTypeDataGridViewTextBoxColumn.DataPropertyName = "PaymentType"
        Me.PaymentTypeDataGridViewTextBoxColumn.HeaderText = "Payment Type"
        Me.PaymentTypeDataGridViewTextBoxColumn.Name = "PaymentTypeDataGridViewTextBoxColumn"
        Me.PaymentTypeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RequestedAmountDataGridViewTextBoxColumn
        '
        Me.RequestedAmountDataGridViewTextBoxColumn.DataPropertyName = "RequestedAmount"
        Me.RequestedAmountDataGridViewTextBoxColumn.HeaderText = "Requested Amount"
        Me.RequestedAmountDataGridViewTextBoxColumn.Name = "RequestedAmountDataGridViewTextBoxColumn"
        Me.RequestedAmountDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RequestApprovedDataGridViewTextBoxColumn
        '
        Me.RequestApprovedDataGridViewTextBoxColumn.DataPropertyName = "RequestApproved"
        Me.RequestApprovedDataGridViewTextBoxColumn.HeaderText = "RequestApproved"
        Me.RequestApprovedDataGridViewTextBoxColumn.Name = "RequestApprovedDataGridViewTextBoxColumn"
        Me.RequestApprovedDataGridViewTextBoxColumn.ReadOnly = True
        Me.RequestApprovedDataGridViewTextBoxColumn.Visible = False
        '
        'ApprovedAmountDataGridViewTextBoxColumn
        '
        Me.ApprovedAmountDataGridViewTextBoxColumn.DataPropertyName = "ApprovedAmount"
        Me.ApprovedAmountDataGridViewTextBoxColumn.HeaderText = "ApprovedAmount"
        Me.ApprovedAmountDataGridViewTextBoxColumn.Name = "ApprovedAmountDataGridViewTextBoxColumn"
        Me.ApprovedAmountDataGridViewTextBoxColumn.ReadOnly = True
        Me.ApprovedAmountDataGridViewTextBoxColumn.Visible = False
        '
        'PaymentTransactionIDDataGridViewTextBoxColumn
        '
        Me.PaymentTransactionIDDataGridViewTextBoxColumn.DataPropertyName = "PaymentTransactionID"
        Me.PaymentTransactionIDDataGridViewTextBoxColumn.HeaderText = "PaymentTransactionID"
        Me.PaymentTransactionIDDataGridViewTextBoxColumn.Name = "PaymentTransactionIDDataGridViewTextBoxColumn"
        Me.PaymentTransactionIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.PaymentTransactionIDDataGridViewTextBoxColumn.Visible = False
        '
        'RequestOwnerDataGridViewTextBoxColumn
        '
        Me.RequestOwnerDataGridViewTextBoxColumn.DataPropertyName = "RequestOwner"
        Me.RequestOwnerDataGridViewTextBoxColumn.HeaderText = "Request Owner"
        Me.RequestOwnerDataGridViewTextBoxColumn.Name = "RequestOwnerDataGridViewTextBoxColumn"
        Me.RequestOwnerDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RequestApproverDataGridViewTextBoxColumn
        '
        Me.RequestApproverDataGridViewTextBoxColumn.DataPropertyName = "RequestApprover"
        Me.RequestApproverDataGridViewTextBoxColumn.HeaderText = "RequestApprover"
        Me.RequestApproverDataGridViewTextBoxColumn.Name = "RequestApproverDataGridViewTextBoxColumn"
        Me.RequestApproverDataGridViewTextBoxColumn.ReadOnly = True
        Me.RequestApproverDataGridViewTextBoxColumn.Visible = False
        '
        'PayInAdvanceTransactionIDDataGridViewTextBoxColumn
        '
        Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn.DataPropertyName = "PayInAdvanceTransactionID"
        Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn.HeaderText = "PayInAdvanceTransactionID"
        Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn.Name = "PayInAdvanceTransactionIDDataGridViewTextBoxColumn"
        Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.PayInAdvanceTransactionIDDataGridViewTextBoxColumn.Visible = False
        '
        'CoIDDataGridViewTextBoxColumn
        '
        Me.CoIDDataGridViewTextBoxColumn.DataPropertyName = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.HeaderText = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.Name = "CoIDDataGridViewTextBoxColumn"
        Me.CoIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.CoIDDataGridViewTextBoxColumn.Visible = False
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Location = New System.Drawing.Point(715, 517)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(118, 23)
        Me.btnRemove.TabIndex = 5
        Me.btnRemove.Text = "&Remove Selected"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'frmPaymentsRequests
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 544)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.txtRequests)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPaymentsRequests"
        Me.Text = "Payments Requests"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvPaymentsRequests, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PaymentsRequestBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvPaymentsRequests As System.Windows.Forms.DataGridView
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents PaymentsRequestBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPaymentType As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnResetList As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtRequests As System.Windows.Forms.TextBox
    Friend WithEvents SupplierName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InvoiceNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InvoiceTransactionIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CapturedDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestedAmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestApprovedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ApprovedAmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentTransactionIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestOwnerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestApproverDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PayInAdvanceTransactionIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnRemove As System.Windows.Forms.Button
End Class
