﻿Imports System.Windows.Forms
Imports System.Diagnostics
Imports System.Collections

Public Class frmOtherModules
#Region "Variables"
    Dim ModulesData As New OtherModulesData
#End Region
#Region "Methods"
    Public Function RipConnectionString() As String

        Dim DatabaseName As String = ""

        If My.Settings.CS_Setting <> Nothing Then

            Dim ConnString As String = CStr(My.Settings.CS_Setting)
            Dim ConnSplit() As String = ConnString.Split(";")

            Dim DatabaseCombination() As String = ConnSplit(1).Split("=")
            DatabaseName = CStr(DatabaseCombination(1))

        End If

        Return DatabaseName

    End Function
    Public Sub InitialLoad()

        Dim DatabaseName As String = CStr(RipConnectionString())
        Dim Co_ID As Integer = CInt(My.Settings.Setting_CompanyID)

        If DatabaseName = "" And Co_ID = Nothing Then
            MessageBox.Show("Missing information: Please double check that the connection string and the default company has been set properly", "Connection details" _
                            , MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Me.Close()
        End If

        Dim AllocationCount As Integer = CInt(ModulesData.CheckStoreModules(DatabaseName, Co_ID))
        If AllocationCount = 0 Then
            MessageBox.Show("The are no extra MagicBox modules allocated to Store :" & My.Settings.CurrentCompany, "MB Modules", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Else
            ModulesData = New OtherModulesData
            MBModulesInventoryBindingSource.DataSource = ModulesData.StoreModules(DatabaseName, Co_ID)
            PrepareLaunchButton()
        End If

    End Sub
    Public Sub PrepareLaunchButton()
        Dim LauchApplicationButtom As New DataGridViewButtonColumn
        With LauchApplicationButtom
            .Width = 200
            .Name = "btnLauchApplication"
            .Text = "Launch"
            .HeaderText = "Launch Application"
            .UseColumnTextForButtonValue = True
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            '.FlatStyle = FlatStyle.Flat
            '.DefaultCellStyle.BackColor = Color.White
            '.DefaultCellStyle.ForeColor = Color.DarkBlue
        End With
        dgvActiveModules.Columns.Insert(10, LauchApplicationButtom)
    End Sub
#End Region
    Private Sub frmOtherModules_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Extra tools -> Version : " & My.Settings.Setting_Version
            Enabled = False
            Cursor = Cursors.AppStarting
            InitialLoad()
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "MB Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvActiveModules_SelectionChanged(sender As Object, e As EventArgs) Handles dgvActiveModules.SelectionChanged
        Try

            If Not IsNothing(dgvActiveModules.CurrentRow) Then

                Enabled = False
                Cursor = Cursors.AppStarting

                Dim ModuleName As String = ""
                If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ModuleName) Then
                    ModuleName = CStr(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ModuleName)
                End If

                Dim DateCreated As Date = Nothing
                If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).DateCreated) Then
                    DateCreated = CDate(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).DateCreated)
                End If

                'Dim AuthorName As String = ""
                'If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ModuleAuthor) Then
                '    AuthorName = CStr(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ModuleAuthor)
                'End If

                Dim DetaiDesc As String = ""
                If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).DetailDescription) Then
                    DetaiDesc = CStr(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).DetailDescription)
                End If

                txtModuleName.Text = ModuleName

                If DateCreated <> Nothing Then
                    dtpDateReleased.Value = DateCreated
                End If

                'txtAuthor.Text = AuthorName
                txtDetailDesc.Text = DetaiDesc

                Enabled = True
                Cursor = Cursors.Arrow

            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Modules manager error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub dgvActiveModules_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvActiveModules.CellContentClick

        Try

            If e.ColumnIndex = 10 And (dgvActiveModules.Rows(e.RowIndex).Cells("btnLauchApplication").Value = "Launch") Then

                Enabled = False
                Cursor = Cursors.AppStarting

                Dim Launchpath As String = ""
                If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).LaunchPath) Then
                    Launchpath = CStr(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).LaunchPath)
                End If

                Dim Filename As String = ""
                If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ActualFileName) Then
                    Filename = CStr(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ActualFileName)
                End If

                Dim ModuleID As Integer = 0
                If Not IsNothing(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ModuleID) Then
                    ModuleID = CInt(CType(MBModulesInventoryBindingSource.Current, MB_ModulesInventory).ModuleID)
                End If

                Dim FileString As String = CStr(Launchpath & Filename)
                If Not IO.File.Exists(FileString) Then
                    Enabled = True
                    Cursor = Cursors.Arrow
                    MessageBox.Show("The application requested was not find in the default location, please report this to system support", "File missing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                Else

                    Enabled = True
                    Cursor = Cursors.Arrow

                    Dim ParametresHash As New Hashtable()
                    Dim ParametresString As String = ""
                    Dim ConnStringAndCoIDParametre As String = CStr(My.Settings.CS_Setting) & ":~:" & CStr(My.Settings.Setting_CompanyID) & ":~:" & CStr(My.Settings.CurrentCompany)

                    Dim Parametres = ModulesData.ModuleParametres(ModuleID)
                    For Each Param In Parametres
                        ParametresHash.Add(Param.ParametreKey, Param.ParametreValue)
                        ParametresString &= Param.ParametreKey & "=" & Param.ParametreValue & "*"
                    Next

                    Dim startInfo As New ProcessStartInfo(FileString)
                    Dim userid As Integer = 1
                    startInfo.Arguments = ConnStringAndCoIDParametre & "-|-" & ParametresString

                    Process.Start(startInfo)
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Modules manager error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class