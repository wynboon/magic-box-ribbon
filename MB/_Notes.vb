﻿Module _Notes


    '*****  NOTE - VERY IMPORTANT - Invoices have their own TransactionID and any Payment, Credit Note or Supplier Debit uses 
    'this as their LinkID. They each have their own TransationID

    ''The idea is to include all things relating to an Invoice under one line item in Supplier Payments
    ''So the greater category called DocType stays at "Invoice". The Info Field then holds the sub-categories
    ''of this so we know what is incorporated in an Invoice amount eg Invoice, Credit Note, Supplier Debit
    ''Finally the Info2 column is used to put a marker against every line item in a category or sub-category 
    ''so that all the line items can be manipulated at once: eg changing an unapproved Credit Note, EFT or Payment
    '' to paid - this is in essence a status field that can be used in many ways (eg 3 stage approvals on a payment
    ''could be handled here.

    ''Information Fields
    'DocType - The greater document eg Invoice (Used to achieve a single balancing figure per Reference/Invoice number
    ''Info - Sub Category of DocType eg Credit Note for "Invoice" or EFT for "Payment"
    ''Info2 - Used as a descriptive status for all line items in an Invoice, CreditNote, EFT etc

End Module
