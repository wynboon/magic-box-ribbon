﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_IssueEditTask
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_EditTask = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CB_Status = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CB_Assigned = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TV_Category = New System.Windows.Forms.TreeView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DTP_DueDate = New System.Windows.Forms.DateTimePicker()
        Me.TB_Description = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_Title = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LBL_TaskID = New System.Windows.Forms.Label()
        Me.SuspendLayout
        '
        'Btn_EditTask
        '
        Me.Btn_EditTask.Location = New System.Drawing.Point(8, 525)
        Me.Btn_EditTask.Name = "Btn_EditTask"
        Me.Btn_EditTask.Size = New System.Drawing.Size(660, 42)
        Me.Btn_EditTask.TabIndex = 26
        Me.Btn_EditTask.Text = "Edit Task"
        Me.Btn_EditTask.UseVisualStyleBackColor = true
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Location = New System.Drawing.Point(7, 236)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 17)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Category:"
        '
        'CB_Status
        '
        Me.CB_Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Status.FormattingEnabled = True
        Me.CB_Status.Items.AddRange(New Object() {"1;Open", "2;Pening", "3;Closed"})
        Me.CB_Status.Location = New System.Drawing.Point(141, 480)
        Me.CB_Status.Name = "CB_Status"
        Me.CB_Status.Size = New System.Drawing.Size(200, 24)
        Me.CB_Status.TabIndex = 24
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 487)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 17)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Status:"
        '
        'CB_Assigned
        '
        Me.CB_Assigned.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Assigned.FormattingEnabled = True
        Me.CB_Assigned.Location = New System.Drawing.Point(141, 206)
        Me.CB_Assigned.Name = "CB_Assigned"
        Me.CB_Assigned.Size = New System.Drawing.Size(200, 24)
        Me.CB_Assigned.TabIndex = 22
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(8, 206)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 17)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Assigned:"
        '
        'TV_Category
        '
        Me.TV_Category.Location = New System.Drawing.Point(141, 236)
        Me.TV_Category.Name = "TV_Category"
        Me.TV_Category.Size = New System.Drawing.Size(527, 238)
        Me.TV_Category.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(8, 178)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 17)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Due Date:"
        '
        'DTP_DueDate
        '
        Me.DTP_DueDate.CustomFormat = "dd MMMM yyyy"
        Me.DTP_DueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTP_DueDate.Location = New System.Drawing.Point(141, 173)
        Me.DTP_DueDate.Name = "DTP_DueDate"
        Me.DTP_DueDate.Size = New System.Drawing.Size(200, 22)
        Me.DTP_DueDate.TabIndex = 18
        '
        'TB_Description
        '
        Me.TB_Description.Location = New System.Drawing.Point(141, 36)
        Me.TB_Description.Multiline = true
        Me.TB_Description.Name = "TB_Description"
        Me.TB_Description.Size = New System.Drawing.Size(527, 130)
        Me.TB_Description.TabIndex = 17
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(8, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 17)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Description:"
        '
        'TB_Title
        '
        Me.TB_Title.Location = New System.Drawing.Point(141, 8)
        Me.TB_Title.Name = "TB_Title"
        Me.TB_Title.Size = New System.Drawing.Size(527, 22)
        Me.TB_Title.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(8, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 17)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Title:"
        '
        'LBL_TaskID
        '
        Me.LBL_TaskID.AutoSize = true
        Me.LBL_TaskID.Location = New System.Drawing.Point(617, 487)
        Me.LBL_TaskID.Name = "LBL_TaskID"
        Me.LBL_TaskID.Size = New System.Drawing.Size(51, 17)
        Me.LBL_TaskID.TabIndex = 27
        Me.LBL_TaskID.Text = "Label7"
        '
        'frm_IssueEditTask
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 576)
        Me.Controls.Add(Me.LBL_TaskID)
        Me.Controls.Add(Me.Btn_EditTask)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.CB_Status)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.CB_Assigned)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TV_Category)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DTP_DueDate)
        Me.Controls.Add(Me.TB_Description)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TB_Title)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_IssueEditTask"
        Me.Text = "frm_IssueEditTask"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents Btn_EditTask As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CB_Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CB_Assigned As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TV_Category As System.Windows.Forms.TreeView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DTP_DueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents TB_Description As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB_Title As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LBL_TaskID As System.Windows.Forms.Label
End Class
