﻿
Imports System.IO


Module TextFiles

    Public arrSettings(100) As String


    Sub WriteText()


     
        'Dim oFullPath As String = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData & "\TD.txt"
        Dim oDirectory As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Dim oFullPath As String = oDirectory & "\TD.txt"

        If Not Directory.Exists(oDirectory) Then
            Directory.CreateDirectory(oDirectory)
        End If

        File.WriteAllLines(oFullPath, arrSettings)

    End Sub

    Sub ReadText()
        

        Dim oDirectory As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Dim oFullPath As String = oDirectory & "\TD.txt"
        If Not File.Exists(oFullPath) Then
            Exit Sub
        End If


        arrSettings = File.ReadAllLines(oFullPath)

        Dim Reader As System.IO.StreamReader
        Reader = New System.IO.StreamReader(oFullPath) '<-- where to read from



        '======= NOTE - Alternatively Loop Through
        'Dim oCount As Integer = -1
        'Dim tempstring As String
        'Do
        'tempstring = Reader.ReadLine()
        'oCount = oCount + 1
        'arrSettings(oCount) = tempstring
        'Loop Until tempstring = ""


    End Sub
    Public Function WriteToLog(ByVal TextToLog As String)

        Dim sw As IO.StreamWriter = Nothing

        'Define log file path and name. 
        Dim CurrentLogFilePath As String = _
            AppDomain.CurrentDomain.BaseDirectory & "\Log.txt"

        sw = New IO.StreamWriter(CurrentLogFilePath, True)

        'Write data to log file. 
        sw.WriteLine(TextToLog)
        sw.Flush()
        sw.Close()

        If sw IsNot Nothing Then sw.Dispose()

    End Function

End Module
