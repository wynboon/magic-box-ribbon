﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSystemBalance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSystemBalance))
        Me.txtTotalDebits = New System.Windows.Forms.TextBox()
        Me.txtTotalCredits = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.btnGetBalance = New System.Windows.Forms.Button()
        Me.rdbCurrentCompany = New System.Windows.Forms.RadioButton()
        Me.rdbSpecificCoID = New System.Windows.Forms.RadioButton()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.rdbAllCoIDs = New System.Windows.Forms.RadioButton()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTotalDebits
        '
        Me.txtTotalDebits.Location = New System.Drawing.Point(112, 30)
        Me.txtTotalDebits.Name = "txtTotalDebits"
        Me.txtTotalDebits.Size = New System.Drawing.Size(131, 20)
        Me.txtTotalDebits.TabIndex = 0
        '
        'txtTotalCredits
        '
        Me.txtTotalCredits.Location = New System.Drawing.Point(112, 65)
        Me.txtTotalCredits.Name = "txtTotalCredits"
        Me.txtTotalCredits.Size = New System.Drawing.Size(131, 20)
        Me.txtTotalCredits.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label1.Location = New System.Drawing.Point(25, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Total Debits"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label2.Location = New System.Drawing.Point(25, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Total Credits"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label3.Location = New System.Drawing.Point(25, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Balance"
        '
        'lblBalance
        '
        Me.lblBalance.AutoSize = True
        Me.lblBalance.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblBalance.Location = New System.Drawing.Point(109, 104)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(10, 13)
        Me.lblBalance.TabIndex = 5
        Me.lblBalance.Text = "-"
        '
        'btnGetBalance
        '
        Me.btnGetBalance.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnGetBalance.Location = New System.Drawing.Point(160, 205)
        Me.btnGetBalance.Name = "btnGetBalance"
        Me.btnGetBalance.Size = New System.Drawing.Size(83, 23)
        Me.btnGetBalance.TabIndex = 6
        Me.btnGetBalance.Text = "Get Balance"
        Me.btnGetBalance.UseVisualStyleBackColor = True
        '
        'rdbCurrentCompany
        '
        Me.rdbCurrentCompany.AutoSize = True
        Me.rdbCurrentCompany.Checked = True
        Me.rdbCurrentCompany.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbCurrentCompany.Location = New System.Drawing.Point(28, 132)
        Me.rdbCurrentCompany.Name = "rdbCurrentCompany"
        Me.rdbCurrentCompany.Size = New System.Drawing.Size(92, 17)
        Me.rdbCurrentCompany.TabIndex = 7
        Me.rdbCurrentCompany.TabStop = True
        Me.rdbCurrentCompany.Text = "Current Co_ID"
        Me.rdbCurrentCompany.UseVisualStyleBackColor = True
        '
        'rdbSpecificCoID
        '
        Me.rdbSpecificCoID.AutoSize = True
        Me.rdbSpecificCoID.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbSpecificCoID.Location = New System.Drawing.Point(28, 171)
        Me.rdbSpecificCoID.Name = "rdbSpecificCoID"
        Me.rdbSpecificCoID.Size = New System.Drawing.Size(96, 17)
        Me.rdbSpecificCoID.TabIndex = 8
        Me.rdbSpecificCoID.Text = "Specific Co_ID"
        Me.rdbSpecificCoID.UseVisualStyleBackColor = True
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(147, 171)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(96, 20)
        Me.NumericUpDown1.TabIndex = 9
        Me.NumericUpDown1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rdbAllCoIDs
        '
        Me.rdbAllCoIDs.AutoSize = True
        Me.rdbAllCoIDs.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbAllCoIDs.Location = New System.Drawing.Point(174, 132)
        Me.rdbAllCoIDs.Name = "rdbAllCoIDs"
        Me.rdbAllCoIDs.Size = New System.Drawing.Size(69, 17)
        Me.rdbAllCoIDs.TabIndex = 10
        Me.rdbAllCoIDs.Text = "All Co_ID"
        Me.rdbAllCoIDs.UseVisualStyleBackColor = True
        '
        'frmSystemBalance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(268, 240)
        Me.Controls.Add(Me.rdbAllCoIDs)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.rdbSpecificCoID)
        Me.Controls.Add(Me.rdbCurrentCompany)
        Me.Controls.Add(Me.btnGetBalance)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTotalCredits)
        Me.Controls.Add(Me.txtTotalDebits)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSystemBalance"
        Me.Text = "System Balance"
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTotalDebits As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalCredits As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents btnGetBalance As System.Windows.Forms.Button
    Friend WithEvents rdbCurrentCompany As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSpecificCoID As System.Windows.Forms.RadioButton
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents rdbAllCoIDs As System.Windows.Forms.RadioButton
End Class
