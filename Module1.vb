﻿
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Collections



Module Module1
    'Used when editing an invoice
    Public ChangedTxID As String
    'When Accounting etc loaded send a flag to Flags table
    'Each run check flags table and re-download that table

    'Herold's code
    Public TenantEnabledStores As New Hashtable()
    Public SortByDesc As Boolean = Nothing
    Public InvoiceAmount As Decimal = Nothing
    Public LoggedUser_Roles As String()
    Public LoggedInUser As String = ""

    Public Const TransT_Invoice As Integer = 12
    Public Const TransT_Payment As Integer = 1
    Public Const DocT_Quote As Integer = 101
    Public Const DocT_ProForma As Integer = 102
    Public Const DocT_Invoice As Integer = 103

    Public oFullPath As String = My.Settings.Setting_Path & "\MagicBox.accdb"

    '==================== Change these two for Access - SQL switch ==========================================
    'Public DBType As String = "Access"
    'Public DBType As String = My.Settings.DBType '"SQL"

    'Public ConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"
    'Public ConnectionStringSQL As String = "server=196.220.43.247,1444;uid=murbro73;pwd=chestnut3;database=test"
    'Server=41.72.128.100;Database=MagicBox_Doppio;Uid=MB_Doppio; Pwd=D0pp!0
    '=========================================================================================================

    Public frmInvcCapture As New frmInvoice_Capture
    Public Form_Invoice_Edit_Delete As New frmInvoice_Edit_Delete
    Public Form_Supplier_Payments As New frmSupplierPayments
    Public Form_Invoice_Detail_Per_Supplier As New frmInvoiceDetail_Per_Supplier
    Public Form_Accounts As New Accounts
    Public Form_Settings As New frmSettings

    Public Form_Suppliers As New frmSuppliers
    Public Form_Periods As New frmPeriods

    Public oAccessAdapter_Accounts As OleDbDataAdapter
    Public oAccessTable_Accounts As New DataTable
    Public oSQLAdapter_Accounts As SqlDataAdapter
    Public oSQLTable_Accounts As New DataTable

    Public oAccessAdapter_Suppliers As OleDbDataAdapter
    Public oAccessTable_Suppliers As New DataTable
    Public oSQLAdapter_Suppliers As SqlDataAdapter
    Public oSQLTable_Suppliers As New DataTable

    Public arrUpdate_SQL(1000) As String 'For Transact roll back oMULTI_UPDATE below
    Public oNumberQueries As Integer


    Function oGET(ByVal sSQL As String, ByVal sErrorComesFrom As String) As String
        Try

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oGET = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oGET = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

        Catch ex As Exception
            MsgBox("There was an error extracting data! " & sErrorComesFrom & " " & ex.Message)
        End Try
    End Function

    Sub oUPDATE(ByVal sSQL As String, ByVal sErrorComesFrom As String)

        Try

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

        Catch ex As Exception
            MsgBox("There was an error updating data! " & sErrorComesFrom & " " & ex.Message)
        End Try
    End Sub
    Function CheckForCashup(ByVal DateInQuestion As Date) As Integer

        Dim oCount As Integer
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String

            sSQL = "Select COUNT(ID) from Transactions where [Transaction Type] = 144 and [Transaction Date] = '" & DateInQuestion.ToString("dd MMM yyyy") & "'"
            If My.Settings.Setting_CompanyID <> Nothing Then
                sSQL &= " AND Co_ID=" & CInt(My.Settings.Setting_CompanyID)
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            oCount = CInt(cmd.ExecuteScalar())
            Return oCount

        Catch ex As Exception
            MsgBox("An error occured with details : " & ex.Message)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try

    End Function
    Function UPDATE_APPEND_MULTI() As Boolean

        If My.Settings.DBType = "Access" Then

            Dim sSQL As String
            Dim i As Integer
            Dim cmd As OleDbCommand

            Dim cn As New OleDbConnection(My.Settings.CS_Setting)
            Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

            Try

                cn.Open()

                trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

                For i = 0 To oNumberQueries - 1

                    sSQL = arrUpdate_SQL(i)
                    cmd = New OleDbCommand(sSQL, cn)
                    cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                    cmd.ExecuteNonQuery()

                Next

                trans.Commit()  '+++++++ Transaction and rollback ++++++++
                cmd = Nothing
                UPDATE_APPEND_MULTI = True

            Catch ex As Exception
                trans.Rollback() '+++++++ Transaction and rollback ++++++++
                MsgBox(ex.Message & " Update01")
                UPDATE_APPEND_MULTI = False
            Finally
                If Not IsNothing(cmd) Then
                    cmd.Dispose()
                End If

                If Not IsNothing(cn) Then
                    cn.Dispose()
                End If

            End Try

        ElseIf My.Settings.DBType = "SQL" Then

            Dim sSQL As String
            Dim i As Integer
            Dim cmd As SqlCommand


            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

            Try
                '    '// open the connection
                cn.Open()

                trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

                For i = 0 To oNumberQueries - 1

                    sSQL = arrUpdate_SQL(i)
                    cmd = New SqlCommand(sSQL, cn)
                    cmd.CommandTimeout = 300
                    cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                    cmd.ExecuteNonQuery()

                Next

                trans.Commit()  '+++++++ Transaction and rollback ++++++++
                cmd = Nothing

                UPDATE_APPEND_MULTI = True

            Catch ex As Exception
                trans.Rollback() '+++++++ Transaction and rollback ++++++++
                MsgBox(ex.Message & " Update01")
                UPDATE_APPEND_MULTI = False
            Finally
                If Not IsNothing(cmd) Then
                    cmd.Dispose()
                End If

                If Not IsNothing(cn) Then
                    cn.Dispose()
                End If

            End Try
        End If
    End Function
    Public Function CheckForRequests(ByVal TransID As Integer) As Integer

        Dim CountRequests As Integer = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "SELECT Count(*) FROM PaymentsRequests WHERE InvoiceTransactionID = " & TransID
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()

            CountRequests = CInt(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return CountRequests

        Catch ex As Exception
            MsgBox("An error occured with details: " & ex.Message & ".")
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
                cn.Dispose()
            End If
        End Try
    End Function
    Public Function GetCompanyName() As String

        Dim CompanyName As String = ""
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select CompanyName from [Company Details]"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            CompanyName = CStr(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return CompanyName

        Catch ex As Exception
            MsgBox("An error occured with details : " & ex.Message)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try

    End Function
    Public Function GetSupplierName(ByVal SupplierID As Integer) As String

        Dim SupplierName As String = ""
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select SupplierName from Suppliers WHERE SupplierID=" & SupplierID

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            SupplierName = CStr(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return SupplierName

        Catch ex As Exception
            MsgBox("An error occured with details : " & ex.Message)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try

    End Function
    Public Function GetSupplierID(ByVal SupplierName As String) As Long

        Dim SupplierID As Long = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select SupplierID from Suppliers WHERE SupplierName= '" & SupplierName & "' and Co_ID = " & My.Settings.Setting_CompanyID

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            SupplierID = CStr(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return SupplierID

        Catch ex As Exception
            MsgBox("An error occured with details : " & ex.Message)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try

    End Function

    Public Function GetSupplierID_IntegrationCode(ByVal SupplierName As String) As Long

        Dim SupplierID As Long = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select SupplierID from Suppliers WHERE IntegrationCode= '" & SupplierName & "' and Co_ID = " & My.Settings.Setting_CompanyID

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            SupplierID = CStr(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return SupplierID

        Catch ex As Exception
            MsgBox("An error occured with details : " & ex.Message)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try

    End Function
End Module
Public Class ValueDescriptionPair

    Public Value As Object
    Public Description As String

    Public Sub New(ByVal NewValue As Object, ByVal NewDescription As String)
        Value = NewValue
        Description = NewDescription
    End Sub

    Public Overrides Function ToString() As String
        Return Description
    End Function

End Class