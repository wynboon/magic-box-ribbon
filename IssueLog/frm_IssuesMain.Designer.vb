﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_IssuesMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_TaskDetail = New System.Windows.Forms.DataGridView()
        Me.Top_Panel = New System.Windows.Forms.Panel()
        Me.Ckb_ViewComplete = New System.Windows.Forms.CheckBox()
        Me.Btn_EditTask = New System.Windows.Forms.Button()
        Me.Btn_Setup = New System.Windows.Forms.Button()
        Me.ckb_ViewAll = New System.Windows.Forms.CheckBox()
        Me.btn_Task = New System.Windows.Forms.Button()
        Me.DGV_TaskHistory = New System.Windows.Forms.DataGridView()
        Me.Btn_Comment = New System.Windows.Forms.Button()
        Me.Btn_EditComment = New System.Windows.Forms.Button()
        CType(Me.DGV_TaskDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Top_Panel.SuspendLayout()
        CType(Me.DGV_TaskHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_TaskDetail
        '
        Me.DGV_TaskDetail.AllowUserToAddRows = False
        Me.DGV_TaskDetail.AllowUserToDeleteRows = False
        Me.DGV_TaskDetail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_TaskDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGV_TaskDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_TaskDetail.Location = New System.Drawing.Point(12, 109)
        Me.DGV_TaskDetail.Name = "DGV_TaskDetail"
        Me.DGV_TaskDetail.RowTemplate.Height = 24
        Me.DGV_TaskDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_TaskDetail.Size = New System.Drawing.Size(1318, 526)
        Me.DGV_TaskDetail.TabIndex = 0
        '
        'Top_Panel
        '
        Me.Top_Panel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Top_Panel.Controls.Add(Me.Ckb_ViewComplete)
        Me.Top_Panel.Controls.Add(Me.Btn_EditTask)
        Me.Top_Panel.Controls.Add(Me.Btn_Setup)
        Me.Top_Panel.Controls.Add(Me.ckb_ViewAll)
        Me.Top_Panel.Controls.Add(Me.btn_Task)
        Me.Top_Panel.Location = New System.Drawing.Point(12, 12)
        Me.Top_Panel.Name = "Top_Panel"
        Me.Top_Panel.Size = New System.Drawing.Size(1318, 91)
        Me.Top_Panel.TabIndex = 1
        '
        'Ckb_ViewComplete
        '
        Me.Ckb_ViewComplete.AutoSize = True
        Me.Ckb_ViewComplete.Location = New System.Drawing.Point(129, 54)
        Me.Ckb_ViewComplete.Name = "Ckb_ViewComplete"
        Me.Ckb_ViewComplete.Size = New System.Drawing.Size(172, 21)
        Me.Ckb_ViewComplete.TabIndex = 5
        Me.Ckb_ViewComplete.Text = "View Completed Tasks"
        Me.Ckb_ViewComplete.UseVisualStyleBackColor = True
        '
        'Btn_EditTask
        '
        Me.Btn_EditTask.Location = New System.Drawing.Point(151, 3)
        Me.Btn_EditTask.Name = "Btn_EditTask"
        Me.Btn_EditTask.Size = New System.Drawing.Size(142, 45)
        Me.Btn_EditTask.TabIndex = 4
        Me.Btn_EditTask.Text = "Edit Task"
        Me.Btn_EditTask.UseVisualStyleBackColor = True
        '
        'Btn_Setup
        '
        Me.Btn_Setup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Setup.BackgroundImage = Global.MB.My.Resources.Resources.Settings
        Me.Btn_Setup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Btn_Setup.Location = New System.Drawing.Point(1258, 3)
        Me.Btn_Setup.Name = "Btn_Setup"
        Me.Btn_Setup.Size = New System.Drawing.Size(60, 59)
        Me.Btn_Setup.TabIndex = 3
        Me.Btn_Setup.UseVisualStyleBackColor = True
        '
        'ckb_ViewAll
        '
        Me.ckb_ViewAll.AutoSize = True
        Me.ckb_ViewAll.Location = New System.Drawing.Point(3, 54)
        Me.ckb_ViewAll.Name = "ckb_ViewAll"
        Me.ckb_ViewAll.Size = New System.Drawing.Size(120, 21)
        Me.ckb_ViewAll.TabIndex = 1
        Me.ckb_ViewAll.Text = "View All Tasks"
        Me.ckb_ViewAll.UseVisualStyleBackColor = True
        '
        'btn_Task
        '
        Me.btn_Task.Location = New System.Drawing.Point(3, 3)
        Me.btn_Task.Name = "btn_Task"
        Me.btn_Task.Size = New System.Drawing.Size(142, 45)
        Me.btn_Task.TabIndex = 0
        Me.btn_Task.Text = "Create New Task"
        Me.btn_Task.UseVisualStyleBackColor = True
        '
        'DGV_TaskHistory
        '
        Me.DGV_TaskHistory.AllowUserToAddRows = False
        Me.DGV_TaskHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_TaskHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGV_TaskHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_TaskHistory.Location = New System.Drawing.Point(12, 691)
        Me.DGV_TaskHistory.Name = "DGV_TaskHistory"
        Me.DGV_TaskHistory.RowTemplate.Height = 24
        Me.DGV_TaskHistory.Size = New System.Drawing.Size(1318, 170)
        Me.DGV_TaskHistory.TabIndex = 2
        '
        'Btn_Comment
        '
        Me.Btn_Comment.Location = New System.Drawing.Point(12, 641)
        Me.Btn_Comment.Name = "Btn_Comment"
        Me.Btn_Comment.Size = New System.Drawing.Size(193, 44)
        Me.Btn_Comment.TabIndex = 3
        Me.Btn_Comment.Text = "Create New Comment"
        Me.Btn_Comment.UseVisualStyleBackColor = True
        '
        'Btn_EditComment
        '
        Me.Btn_EditComment.Location = New System.Drawing.Point(211, 641)
        Me.Btn_EditComment.Name = "Btn_EditComment"
        Me.Btn_EditComment.Size = New System.Drawing.Size(193, 44)
        Me.Btn_EditComment.TabIndex = 4
        Me.Btn_EditComment.Text = "Edit Comment"
        Me.Btn_EditComment.UseVisualStyleBackColor = True
        Me.Btn_EditComment.Visible = False
        '
        'frm_IssuesMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(1342, 873)
        Me.Controls.Add(Me.Btn_EditComment)
        Me.Controls.Add(Me.Btn_Comment)
        Me.Controls.Add(Me.DGV_TaskHistory)
        Me.Controls.Add(Me.Top_Panel)
        Me.Controls.Add(Me.DGV_TaskDetail)
        Me.Name = "frm_IssuesMain"
        Me.Text = "frm_IssuesMain"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DGV_TaskDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Top_Panel.ResumeLayout(False)
        Me.Top_Panel.PerformLayout()
        CType(Me.DGV_TaskHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DGV_TaskDetail As System.Windows.Forms.DataGridView
    Friend WithEvents Top_Panel As System.Windows.Forms.Panel
    Friend WithEvents DGV_TaskHistory As System.Windows.Forms.DataGridView
    Friend WithEvents btn_Task As System.Windows.Forms.Button
    Friend WithEvents ckb_ViewAll As System.Windows.Forms.CheckBox
    Friend WithEvents Btn_Setup As System.Windows.Forms.Button
    Friend WithEvents Btn_EditTask As System.Windows.Forms.Button
    Friend WithEvents Btn_Comment As System.Windows.Forms.Button
    Friend WithEvents Btn_EditComment As System.Windows.Forms.Button
    Friend WithEvents Ckb_ViewComplete As System.Windows.Forms.CheckBox
End Class
