﻿Imports System.Data
Imports System.Windows.Forms
Imports System.Drawing

Public Class Frm_IssueSetup

    Private Sub Frm_IssueSetup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Stask Setup -> Version : " & My.Settings.Setting_Version
        Me.Icon = My.Resources.AppIcon
        Call BuildTree()
        Call BuildActionTypes()
    End Sub
    Function BuildTree(Optional FindString As String = "0")
        Try
            TV_Catalog.Nodes.Clear()
            Dim dt As New DataTable
            'Fill Treeview with summary and then detailed info
            dt = IssuesCategoriesTree()
            If dt.Rows.Count <= 0 Then
                Exit Function
            End If

            For Each dr As DataRow In dt.Rows
                AddNode(dr("ParentID").ToString(), dr("ParentDesc").ToString(), _
                          dr("ChildID").ToString(), dr("ChildDesc").ToString(), dr("ChildID").ToString(), True)
            Next

        Catch ex As Exception
            MsgBox("Error loading tree view: " & ex.Message, MsgBoxStyle.Critical, "Loading Error")
        End Try
    End Function
    Private Sub AddNode(ParentKey As String, parentText As String, ChildKey As String, ChildText As String, ChildTag As String, ExpNode As Boolean)
        Dim node As New List(Of TreeNode)
        Dim NewParentNode As New TreeNode
        Dim InsertNode As New TreeNode
        node.AddRange(TV_Catalog.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            NewParentNode.Name = ParentKey
            NewParentNode.Tag = ParentKey
            NewParentNode.Text = parentText
            TV_Catalog.Nodes.Add(NewParentNode)
            node.AddRange(TV_Catalog.Nodes.Find(ParentKey, True))
        End If
        InsertNode.Name = ChildKey
        InsertNode.Text = ChildText
        InsertNode.Tag = ChildTag
        node(0).Nodes.Add(InsertNode)

        If ExpNode = True Then
            node(0).Expand()
        End If

    End Sub
    Sub BuildActionTypes()
        Dim dtTaskType As DataTable
        dtTaskType = IssuesTaskTypes()
        Dim TT As New List(Of TaskTypeItem)()
        If dtTaskType Is Nothing Then
        Else
            For Each rw As DataRow In dtTaskType.Rows
                TT.Add(New TaskTypeItem(rw.Item("TaskTypeName"), rw.Item("TaskTypeID")))
            Next
        End If

        Me.LSTB_ActionTypes.DataSource = TT
        Me.LSTB_ActionTypes.DisplayMember = "Name"
        Me.LSTB_ActionTypes.ValueMember = "Id"
    End Sub
    Private Sub TV_Catalog_ItemDrag(sender As Object, e As ItemDragEventArgs) Handles TV_Catalog.ItemDrag
        DoDragDrop(e.Item, DragDropEffects.Move)
    End Sub
    Private Sub TV_Catalog_DragEnter(sender As Object, e As DragEventArgs) Handles TV_Catalog.DragEnter
        e.Effect = DragDropEffects.Move
    End Sub

    Private Sub TV_Catalog_DragDrop(sender As Object, e As DragEventArgs) Handles TV_Catalog.DragDrop
        Dim NewNode As TreeNode
        If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
            Dim pt As Point
            Dim DestinationNode As TreeNode
            pt = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
            DestinationNode = CType(sender, TreeView).GetNodeAt(pt)
            NewNode = CType(e.Data.GetData("System.Windows.Forms.TreeNode"),  _
                                            TreeNode)

            DestinationNode.Nodes.Add(NewNode.Clone)
            DestinationNode.Expand()
            Issue_UpdateCatalogItem(NewNode.Tag.ToString, DestinationNode.Tag.ToString)
            'Remove original node
            NewNode.Remove()
        End If
    End Sub

    Private Sub TV_Catalog_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TV_Catalog.AfterSelect
        LBL_EditID.Text = TV_Catalog.SelectedNode.Tag
        TB_EditName.Text = TV_Catalog.SelectedNode.Text
        LBL_NewParent.Text = TV_Catalog.SelectedNode.Tag
        lbl_ParentName.Text = TV_Catalog.SelectedNode.Text
    End Sub

    Private Sub BTN_Edit_Click(sender As Object, e As EventArgs) Handles BTN_Edit.Click
        If Edit_CategorieItem(LBL_EditID.Text, TB_EditName.Text, True) = Nothing Then
            BuildTree()
        End If
    End Sub

    Private Sub Btn_Delete_Click(sender As Object, e As EventArgs) Handles Btn_Delete.Click
        If MsgBox("Are you sure you want to delete the item:" & TB_EditName.Text & "?", MsgBoxStyle.YesNo, "Confirm") = MsgBoxResult.Yes Then
            If Edit_CategorieItem(LBL_EditID.Text, TB_EditName.Text, False) = Nothing Then
                BuildTree()
            End If
        End If
    End Sub

    Private Sub Btn_NewItem_Click(sender As Object, e As EventArgs) Handles Btn_NewItem.Click
        Dim ParentID As Long
        Dim NewParentID As Long

        If Ckb_MakeRoot.Checked = True Then
            ParentID = 0

            MsgBox("When creating a root item, a place holder is created on your behalf.")
            NewParentID = Insert_CategorieItem(ParentID, TB_NewItem.Text, True)
            Insert_CategorieItem(NewParentID, TB_NewItem.Text, True)
            BuildTree()
        Else
            ParentID = LBL_NewParent.Text
            If Insert_CategorieItem(ParentID, TB_NewItem.Text, True) > 0 Then
                BuildTree()
            End If
        End If

        
    End Sub

    Private Sub Btn_Refresh_Click(sender As Object, e As EventArgs) Handles Btn_Refresh.Click
        BuildTree()
    End Sub

    Private Sub LB_ActionTypes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LSTB_ActionTypes.SelectedIndexChanged
        LBL_EditActionID.Text = LSTB_ActionTypes.SelectedValue.ToString
        TB_EditActionName.Text = LSTB_ActionTypes.Text
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If TB_NewActionType.Text = "" Then
            MsgBox("Action type cannot be blank.", MsgBoxStyle.Information, "Caution")
            Exit Sub
        Else
            Insert_ActionType(TB_NewActionType.Text)
            BuildActionTypes()
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If TB_EditActionName.Text = "" Then
            MsgBox("Action type cannot be blank.", MsgBoxStyle.Information, "Caution")
            Exit Sub
        ElseIf LBL_EditActionID.Text = 0 Then
            MsgBox("Please select an action type.", MsgBoxStyle.Information, "Caution")
            Exit Sub
        Else
            Edit_ActionType(LBL_EditActionID.Text, TB_EditActionName.Text, True)
            BuildActionTypes()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If TB_EditActionName.Text = "" Then
            MsgBox("Action type cannot be blank.", MsgBoxStyle.Information, "Caution")
            Exit Sub
        ElseIf LBL_EditActionID.Text = 0 Then
            MsgBox("Please select an action type.", MsgBoxStyle.Information, "Caution")
            Exit Sub
        Else
            Edit_ActionType(LBL_EditActionID.Text, TB_EditActionName.Text, False)
            BuildActionTypes()
        End If
    End Sub
End Class
