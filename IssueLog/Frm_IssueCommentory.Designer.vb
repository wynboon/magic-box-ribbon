﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_IssueCommentory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TB_ActionComment = New System.Windows.Forms.TextBox()
        Me.CB_ActionTypes = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Btn_Cancel = New System.Windows.Forms.Button()
        Me.Btn_Save = New System.Windows.Forms.Button()
        Me.lbl_TaskID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TB_ActionComment
        '
        Me.TB_ActionComment.Location = New System.Drawing.Point(13, 43)
        Me.TB_ActionComment.Multiline = True
        Me.TB_ActionComment.Name = "TB_ActionComment"
        Me.TB_ActionComment.Size = New System.Drawing.Size(605, 209)
        Me.TB_ActionComment.TabIndex = 0
        '
        'CB_ActionTypes
        '
        Me.CB_ActionTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_ActionTypes.FormattingEnabled = True
        Me.CB_ActionTypes.Location = New System.Drawing.Point(102, 9)
        Me.CB_ActionTypes.Name = "CB_ActionTypes"
        Me.CB_ActionTypes.Size = New System.Drawing.Size(197, 24)
        Me.CB_ActionTypes.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Action Type"
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.Location = New System.Drawing.Point(16, 259)
        Me.Btn_Cancel.Name = "Btn_Cancel"
        Me.Btn_Cancel.Size = New System.Drawing.Size(149, 37)
        Me.Btn_Cancel.TabIndex = 3
        Me.Btn_Cancel.Text = "Cancel"
        Me.Btn_Cancel.UseVisualStyleBackColor = True
        '
        'Btn_Save
        '
        Me.Btn_Save.Location = New System.Drawing.Point(469, 259)
        Me.Btn_Save.Name = "Btn_Save"
        Me.Btn_Save.Size = New System.Drawing.Size(149, 37)
        Me.Btn_Save.TabIndex = 4
        Me.Btn_Save.Text = "Save"
        Me.Btn_Save.UseVisualStyleBackColor = True
        '
        'lbl_TaskID
        '
        Me.lbl_TaskID.AutoSize = True
        Me.lbl_TaskID.Location = New System.Drawing.Point(563, 13)
        Me.lbl_TaskID.Name = "lbl_TaskID"
        Me.lbl_TaskID.Size = New System.Drawing.Size(51, 17)
        Me.lbl_TaskID.TabIndex = 5
        Me.lbl_TaskID.Text = "Label2"
        '
        'Frm_IssueCommentory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(626, 301)
        Me.Controls.Add(Me.lbl_TaskID)
        Me.Controls.Add(Me.Btn_Save)
        Me.Controls.Add(Me.Btn_Cancel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CB_ActionTypes)
        Me.Controls.Add(Me.TB_ActionComment)
        Me.Name = "Frm_IssueCommentory"
        Me.Text = "Frm_Commentory"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TB_ActionComment As System.Windows.Forms.TextBox
    Friend WithEvents CB_ActionTypes As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Btn_Cancel As System.Windows.Forms.Button
    Friend WithEvents Btn_Save As System.Windows.Forms.Button
    Friend WithEvents lbl_TaskID As System.Windows.Forms.Label
End Class
