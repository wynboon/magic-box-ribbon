﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_IssueSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FormTabs = New System.Windows.Forms.TabControl()
        Me.TB_Catalog = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lbl_ParentName = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Ckb_MakeRoot = New System.Windows.Forms.CheckBox()
        Me.LBL_NewParent = New System.Windows.Forms.Label()
        Me.Btn_NewItem = New System.Windows.Forms.Button()
        Me.TB_NewItem = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Delete = New System.Windows.Forms.Button()
        Me.LBL_EditID = New System.Windows.Forms.Label()
        Me.BTN_Edit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_EditName = New System.Windows.Forms.TextBox()
        Me.Btn_Refresh = New System.Windows.Forms.Button()
        Me.TV_Catalog = New System.Windows.Forms.TreeView()
        Me.TB_ActionTypes = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TB_NewActionType = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.LBL_EditActionID = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TB_EditActionName = New System.Windows.Forms.TextBox()
        Me.LSTB_ActionTypes = New System.Windows.Forms.ListBox()
        Me.FormTabs.SuspendLayout
        Me.TB_Catalog.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.Panel1.SuspendLayout
        Me.TB_ActionTypes.SuspendLayout
        Me.Panel3.SuspendLayout
        Me.Panel4.SuspendLayout
        Me.SuspendLayout
        '
        'FormTabs
        '
        Me.FormTabs.Controls.Add(Me.TB_Catalog)
        Me.FormTabs.Controls.Add(Me.TB_ActionTypes)
        Me.FormTabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormTabs.Location = New System.Drawing.Point(0, 0)
        Me.FormTabs.Name = "FormTabs"
        Me.FormTabs.SelectedIndex = 0
        Me.FormTabs.Size = New System.Drawing.Size(986, 542)
        Me.FormTabs.TabIndex = 0
        '
        'TB_Catalog
        '
        Me.TB_Catalog.Controls.Add(Me.Panel2)
        Me.TB_Catalog.Controls.Add(Me.Panel1)
        Me.TB_Catalog.Controls.Add(Me.Btn_Refresh)
        Me.TB_Catalog.Controls.Add(Me.TV_Catalog)
        Me.TB_Catalog.Location = New System.Drawing.Point(4, 25)
        Me.TB_Catalog.Name = "TB_Catalog"
        Me.TB_Catalog.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_Catalog.Size = New System.Drawing.Size(978, 513)
        Me.TB_Catalog.TabIndex = 0
        Me.TB_Catalog.Text = "Catalog"
        Me.TB_Catalog.UseVisualStyleBackColor = true
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.lbl_ParentName)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Ckb_MakeRoot)
        Me.Panel2.Controls.Add(Me.LBL_NewParent)
        Me.Panel2.Controls.Add(Me.Btn_NewItem)
        Me.Panel2.Controls.Add(Me.TB_NewItem)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(381, 102)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(589, 109)
        Me.Panel2.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 17)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "New Item"
        '
        'lbl_ParentName
        '
        Me.lbl_ParentName.AutoSize = true
        Me.lbl_ParentName.Location = New System.Drawing.Point(111, 33)
        Me.lbl_ParentName.Name = "lbl_ParentName"
        Me.lbl_ParentName.Size = New System.Drawing.Size(13, 17)
        Me.lbl_ParentName.TabIndex = 6
        Me.lbl_ParentName.Text = "-"
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(5, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 17)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Parent ID"
        '
        'Ckb_MakeRoot
        '
        Me.Ckb_MakeRoot.AutoSize = true
        Me.Ckb_MakeRoot.Location = New System.Drawing.Point(8, 53)
        Me.Ckb_MakeRoot.Name = "Ckb_MakeRoot"
        Me.Ckb_MakeRoot.Size = New System.Drawing.Size(150, 21)
        Me.Ckb_MakeRoot.TabIndex = 5
        Me.Ckb_MakeRoot.Text = "Create as root item"
        Me.Ckb_MakeRoot.UseVisualStyleBackColor = true
        '
        'LBL_NewParent
        '
        Me.LBL_NewParent.AutoSize = true
        Me.LBL_NewParent.Location = New System.Drawing.Point(78, 33)
        Me.LBL_NewParent.Name = "LBL_NewParent"
        Me.LBL_NewParent.Size = New System.Drawing.Size(16, 17)
        Me.LBL_NewParent.TabIndex = 1
        Me.LBL_NewParent.Text = "0"
        '
        'Btn_NewItem
        '
        Me.Btn_NewItem.Location = New System.Drawing.Point(352, 66)
        Me.Btn_NewItem.Name = "Btn_NewItem"
        Me.Btn_NewItem.Size = New System.Drawing.Size(87, 31)
        Me.Btn_NewItem.TabIndex = 4
        Me.Btn_NewItem.Text = "New"
        Me.Btn_NewItem.UseVisualStyleBackColor = true
        '
        'TB_NewItem
        '
        Me.TB_NewItem.Location = New System.Drawing.Point(60, 75)
        Me.TB_NewItem.Name = "TB_NewItem"
        Me.TB_NewItem.Size = New System.Drawing.Size(286, 22)
        Me.TB_NewItem.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(5, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Name:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Btn_Delete)
        Me.Panel1.Controls.Add(Me.LBL_EditID)
        Me.Panel1.Controls.Add(Me.BTN_Edit)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.TB_EditName)
        Me.Panel1.Location = New System.Drawing.Point(381, 7)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(591, 89)
        Me.Panel1.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Edit Item"
        '
        'Btn_Delete
        '
        Me.Btn_Delete.Location = New System.Drawing.Point(444, 43)
        Me.Btn_Delete.Name = "Btn_Delete"
        Me.Btn_Delete.Size = New System.Drawing.Size(87, 31)
        Me.Btn_Delete.TabIndex = 5
        Me.Btn_Delete.Text = "Delete"
        Me.Btn_Delete.UseVisualStyleBackColor = true
        '
        'LBL_EditID
        '
        Me.LBL_EditID.AutoSize = true
        Me.LBL_EditID.Location = New System.Drawing.Point(31, 30)
        Me.LBL_EditID.Name = "LBL_EditID"
        Me.LBL_EditID.Size = New System.Drawing.Size(16, 17)
        Me.LBL_EditID.TabIndex = 1
        Me.LBL_EditID.Text = "0"
        '
        'BTN_Edit
        '
        Me.BTN_Edit.Location = New System.Drawing.Point(351, 43)
        Me.BTN_Edit.Name = "BTN_Edit"
        Me.BTN_Edit.Size = New System.Drawing.Size(87, 31)
        Me.BTN_Edit.TabIndex = 4
        Me.BTN_Edit.Text = "Edit"
        Me.BTN_Edit.UseVisualStyleBackColor = true
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(4, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(4, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Name:"
        '
        'TB_EditName
        '
        Me.TB_EditName.Location = New System.Drawing.Point(59, 52)
        Me.TB_EditName.Name = "TB_EditName"
        Me.TB_EditName.Size = New System.Drawing.Size(286, 22)
        Me.TB_EditName.TabIndex = 2
        '
        'Btn_Refresh
        '
        Me.Btn_Refresh.Location = New System.Drawing.Point(8, 466)
        Me.Btn_Refresh.Name = "Btn_Refresh"
        Me.Btn_Refresh.Size = New System.Drawing.Size(365, 39)
        Me.Btn_Refresh.TabIndex = 5
        Me.Btn_Refresh.Text = "Refresh"
        Me.Btn_Refresh.UseVisualStyleBackColor = true
        '
        'TV_Catalog
        '
        Me.TV_Catalog.AllowDrop = true
        Me.TV_Catalog.Location = New System.Drawing.Point(7, 7)
        Me.TV_Catalog.Name = "TV_Catalog"
        Me.TV_Catalog.Size = New System.Drawing.Size(366, 453)
        Me.TV_Catalog.TabIndex = 0
        '
        'TB_ActionTypes
        '
        Me.TB_ActionTypes.Controls.Add(Me.Panel3)
        Me.TB_ActionTypes.Controls.Add(Me.Panel4)
        Me.TB_ActionTypes.Controls.Add(Me.LSTB_ActionTypes)
        Me.TB_ActionTypes.Location = New System.Drawing.Point(4, 25)
        Me.TB_ActionTypes.Name = "TB_ActionTypes"
        Me.TB_ActionTypes.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_ActionTypes.Size = New System.Drawing.Size(978, 513)
        Me.TB_ActionTypes.TabIndex = 1
        Me.TB_ActionTypes.Text = "Action Types"
        Me.TB_ActionTypes.UseVisualStyleBackColor = true
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Controls.Add(Me.TB_NewActionType)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Location = New System.Drawing.Point(217, 113)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(589, 65)
        Me.Panel3.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 17)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "New Item"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(351, 18)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 31)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "New"
        Me.Button2.UseVisualStyleBackColor = true
        '
        'TB_NewActionType
        '
        Me.TB_NewActionType.Location = New System.Drawing.Point(59, 27)
        Me.TB_NewActionType.Name = "TB_NewActionType"
        Me.TB_NewActionType.Size = New System.Drawing.Size(286, 22)
        Me.TB_NewActionType.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = true
        Me.Label11.Location = New System.Drawing.Point(4, 32)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 17)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Name:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.Button3)
        Me.Panel4.Controls.Add(Me.LBL_EditActionID)
        Me.Panel4.Controls.Add(Me.Button4)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.TB_EditActionName)
        Me.Panel4.Location = New System.Drawing.Point(217, 18)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(591, 89)
        Me.Panel4.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = true
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label12.Location = New System.Drawing.Point(5, 5)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 17)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Edit Item"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(444, 43)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 31)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Delete"
        Me.Button3.UseVisualStyleBackColor = true
        '
        'LBL_EditActionID
        '
        Me.LBL_EditActionID.AutoSize = true
        Me.LBL_EditActionID.Location = New System.Drawing.Point(31, 30)
        Me.LBL_EditActionID.Name = "LBL_EditActionID"
        Me.LBL_EditActionID.Size = New System.Drawing.Size(16, 17)
        Me.LBL_EditActionID.TabIndex = 1
        Me.LBL_EditActionID.Text = "0"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(351, 43)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 31)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "Edit"
        Me.Button4.UseVisualStyleBackColor = true
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(4, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(21, 17)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "ID"
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Location = New System.Drawing.Point(4, 57)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(49, 17)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Name:"
        '
        'TB_EditActionName
        '
        Me.TB_EditActionName.Location = New System.Drawing.Point(59, 52)
        Me.TB_EditActionName.Name = "TB_EditActionName"
        Me.TB_EditActionName.Size = New System.Drawing.Size(286, 22)
        Me.TB_EditActionName.TabIndex = 2
        '
        'LSTB_ActionTypes
        '
        Me.LSTB_ActionTypes.FormattingEnabled = true
        Me.LSTB_ActionTypes.ItemHeight = 16
        Me.LSTB_ActionTypes.Location = New System.Drawing.Point(8, 6)
        Me.LSTB_ActionTypes.Name = "LSTB_ActionTypes"
        Me.LSTB_ActionTypes.Size = New System.Drawing.Size(203, 436)
        Me.LSTB_ActionTypes.TabIndex = 0
        '
        'Frm_IssueSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(986, 542)
        Me.Controls.Add(Me.FormTabs)
        Me.Name = "Frm_IssueSetup"
        Me.Text = "Frm_IssueSetup"
        Me.FormTabs.ResumeLayout(false)
        Me.TB_Catalog.ResumeLayout(false)
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.TB_ActionTypes.ResumeLayout(false)
        Me.Panel3.ResumeLayout(false)
        Me.Panel3.PerformLayout
        Me.Panel4.ResumeLayout(false)
        Me.Panel4.PerformLayout
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents FormTabs As System.Windows.Forms.TabControl
    Friend WithEvents TB_Catalog As System.Windows.Forms.TabPage
    Friend WithEvents TB_ActionTypes As System.Windows.Forms.TabPage
    Friend WithEvents TV_Catalog As System.Windows.Forms.TreeView
    Friend WithEvents Btn_NewItem As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_NewItem As System.Windows.Forms.TextBox
    Friend WithEvents LBL_NewParent As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BTN_Edit As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB_EditName As System.Windows.Forms.TextBox
    Friend WithEvents LBL_EditID As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Ckb_MakeRoot As System.Windows.Forms.CheckBox
    Friend WithEvents Btn_Delete As System.Windows.Forms.Button
    Friend WithEvents lbl_ParentName As System.Windows.Forms.Label
    Friend WithEvents Btn_Refresh As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TB_NewActionType As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents LBL_EditActionID As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TB_EditActionName As System.Windows.Forms.TextBox
    Friend WithEvents LSTB_ActionTypes As System.Windows.Forms.ListBox
End Class
