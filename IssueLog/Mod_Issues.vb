﻿Imports System.Data
Imports System.Data.SqlClient

Module Mod_Issues
    Public Function IssuesCategoriesTree() As DataTable
        Try
            Dim sSQL As String = "SELECT a.CatID as ParentID,a.description as ParentDesc,b.CatID as ChildID,b.description as ChildDesc " & _
                                "FROM [TaskCategories] b left Join [TaskCategories] a on a.CatID = b.ParentID " & _
                                "Where a.ParentID is not null and a.Active = 'True' and b.Active = 'True' Order By a.CatID asc"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=360")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function InsertIssue(Title As String, Description As String, DueDate As String, _
                         StatusID As Long, CategoryID As Long, AssignedTo As Integer) As String

        Try
            If Title.Length > 200 Then
                Return "Title can only be 200 chars"
                Exit Function
            End If
            If DueDate < Date.Today Then
                Return "Due date cannot be less then current date"
                Exit Function
            End If

            Dim sSQL As String

            sSQL = "INSERT INTO [TaskDetails] ([Title],[Description],[CreateDate],[DueDate],[StatusID]" & _
           ",[CategoryID],[Creator],[AssignedTo]) VALUES ('" & _
           Title & " ','" & Description & "','" & Date.Today.ToString("dd MMM yyyy") & "','" & DueDate & "','" & _
           StatusID & " ','" & CategoryID & "','" & Globals.Ribbons.Ribbon1.CurrentTenantID & "','" & AssignedTo & "')"


            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error inserting new task:" & vbNewLine & ex.Message)
        End Try
    End Function
    Function EditIssue(Title As String, Description As String, DueDate As String, _
                         StatusID As Long, CategoryID As Long, AssignedTo As Integer, TaskID As Integer) As String

        Try
            If Title.Length > 200 Then
                Return "Title can only be 200 chars"
                Exit Function
            End If
            If DueDate < Date.Today Then
                Return "Due date cannot be less then current date"
                Exit Function
            End If

            Dim sSQL As String

            sSQL = "Update [TaskDetails] set [Title] = '" & Title & " ',[Description] = '" & Description & "'," & _
                    "[LastModifiedDate] = '" & Date.Today.ToString("dd MMM yyyy") & "'," & _
                    "[DueDate] = '" & DueDate & "'," & _
                    "[StatusID] = '" & StatusID & "'," & _
                    "[CategoryID] = '" & CategoryID & "'," & _
                    "[LastModifier] = '" & Globals.Ribbons.Ribbon1.CurrentTenantID & "'," & _
                    "[AssignedTo] = '" & AssignedTo & "' " & _
                    "Where TaskID = " & TaskID

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            Return "Complete"
        Catch ex As Exception
            MsgBox("There was an error editing new task:" & vbNewLine & ex.Message)
        End Try
    End Function
    Public Function Issue_UpdateCatalogItem(CatID As Long, NewParentID As Long)
        Try
            Dim sSQL As String
            sSQL = "Update [TaskCategories] Set ParentID = " & NewParentID & " Where CatID = " & CatID


            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            Return ""
        Catch ex As Exception
            MsgBox("There was an error moving catalog item:" & vbNewLine & ex.Message)
            Return ""
        End Try
    End Function
    Public Function IssuesItem(TaskID As Long) As DataTable
        Try
            Dim sSQL As String = "SELECT Top 1 * From TaskDetails Where TaskID = " & TaskID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=360")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function Edit_CategorieItem(CatID As Long, CatName As String, Active As Boolean)
        Try
            Dim sSQL As String
            sSQL = "Update [TaskCategories] set [description] = '" & CatName & "',[Active] = '" & Active & "' " & _
                    "Where CatID = " & CatID

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            Return Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
            Return "Error"
        End Try
    End Function
    Function Insert_CategorieItem(ParentID As Long, CatName As String, Active As Boolean) As Long
        Try
            Dim sSQL As String
            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            sSQL = "Insert Into [TaskCategories]([ParentID],[description],[Active]) Values ('" & ParentID & "','" & CatName & "','" & Active & "')"

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
            Return Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function
    Function Insert_ActionType(ActionName As String)
        Try
            Dim sSQL As String
            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            sSQL = "Insert Into [TaskTypes]([TaskTypeName],TaskTypeActive) Values ('" & ActionName & "','True')"

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
            Return Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function
    Function Edit_ActionType(TaskTypeID As Long, TypeName As String, TypeActive As Boolean)
        Try
            Dim sSQL As String

            sSQL = "Update [TaskTypes] Set [TaskTypeName] ='" & TypeName & "', [TaskTypeActive] = '" & TypeActive & "' Where TaskTypeID = " & TaskTypeID

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            Return Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function
    Public Function IssuesTaskTypes() As DataTable
        Try
            Dim sSQL As String = "SELECT * From TaskTypes Where TaskTypeActive = 'True'"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=360")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function Insert_TaskCommentory(TaskID As Integer, ActionType As Integer, Comment As String, _
                         Status As Integer) As String

        Try

            Dim sSQL As String

            sSQL = "INSERT INTO [TaskCommentory] ([TaskID],[ActionType],[Comment],[Status],[CreateDate]" & _
           ",[LastUpdated],[Creator],[LastEditedBy]) VALUES ('" & _
           TaskID & " ','" & ActionType & "','" & Comment & "','" & Status & "','" & _
           Date.Today.ToString("dd MMM yyyy") & " ','" & Date.Today.ToString("dd MMM yyyy") & "','" & Globals.Ribbons.Ribbon1.CurrentTenantID & "','" & Globals.Ribbons.Ribbon1.CurrentTenantID & "')"


            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error inserting new task:" & vbNewLine & ex.Message)
        End Try
    End Function
    Function List_Commentory(TaskID As Integer)
        Try
            Dim sSQL As String = "SELECT TaskTypeName, Comment,(SELECT FirstName + ' ' + Surname AS Expr1 " & _
            "FROM MagicBox_Central.dbo.Tenants WHERE   (TenantID = [TaskCommentory].Creator)) AS CreatorName, CreateDate " & _
            "FROM [TaskCommentory] Left Join TaskTypes on [TaskCommentory].ActionType = TaskTypes.TaskTypeID Order By CreateDate Desc"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=360")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
End Module
Public Class TaskTypeItem
    Public Sub New(ByVal name As String, ByVal id As Integer)
        mID = id
        mName = name
    End Sub

    Private mID As Integer
    Public Property ID() As Integer
        Get
            Return mID
        End Get
        Set(ByVal value As Integer)
            mID = value
        End Set
    End Property

    Private mName As String
    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

End Class