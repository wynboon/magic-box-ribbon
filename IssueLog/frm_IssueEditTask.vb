﻿Imports System.Windows.Forms
Imports System.Data

Public Class frm_IssueEditTask

    Private Sub frm_IssueEditTask_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        TV_Category.Focus()
    End Sub

    Private Sub frm_IssueEditTask_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Add Task -> Version : " & My.Settings.Setting_Version
            Me.Icon = My.Resources.AppIcon

            'Get users
            Call GetUsers()
            Call BuildTree()

            Dim Status As New List(Of StatusItem)()
            Status.Add(New StatusItem("Waiting", 1))
            Status.Add(New StatusItem("In Progress", 2))
            Status.Add(New StatusItem("Complete", 3))
            Status.Add(New StatusItem("On Hold", 4))
            Me.CB_Status.DataSource = Status
            Me.CB_Status.DisplayMember = "name"
            Me.CB_Status.ValueMember = "id"

            Call LoadItem()


        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub GetUsers()
        Dim ds As New DataSet
        ds = GetGroupUsers()
        CB_Assigned.DataSource = ds.Tables(0)
        CB_Assigned.ValueMember = "Value"
        CB_Assigned.DisplayMember = "Display"
        CB_Assigned.SelectedIndex = 0
    End Sub
    Function BuildTree(Optional FindString As String = "0")
        Try
            TV_Category.Nodes.Clear()
            Dim dt As New DataTable
            'Fill Treeview with summary and then detailed info
            dt = IssuesCategoriesTree()
            If dt.Rows.Count <= 0 Then
                Exit Function
            End If

            For Each dr As DataRow In dt.Rows
                AddNode(dr("ParentID").ToString(), dr("ParentDesc").ToString(), _
                          dr("ChildID").ToString(), dr("ChildDesc").ToString(), dr("ChildID").ToString(), True)
            Next

        Catch ex As Exception
            MsgBox("Error loading tree view: " & ex.Message, MsgBoxStyle.Critical, "Loading Error")
        End Try
    End Function
    Private Sub AddNode(ParentKey As String, parentText As String, ChildKey As String, ChildText As String, ChildTag As String, ExpNode As Boolean)
        Dim node As New List(Of TreeNode)
        Dim InsertNode As New TreeNode
        node.AddRange(TV_Category.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            node.Add(TV_Category.Nodes.Add(ParentKey, parentText))
        End If
        InsertNode.Name = ChildKey
        InsertNode.Text = ChildText
        InsertNode.Tag = ChildTag
        node(0).Nodes.Add(InsertNode)

        If ExpNode = True Then
            node(0).Expand()
        End If

    End Sub

    Private Sub Btn_EditTask_Click(sender As Object, e As EventArgs) Handles Btn_EditTask.Click
        If CB_Status.Text = "" Then
            MsgBox("A Status Must Be Selected!", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If TV_Category.SelectedNode.Tag = Nothing Then
            MsgBox("A Category Must Be Selected!", MsgBoxStyle.Critical)
            Exit Sub
        End If
        MsgBox(Mod_Issues.EditIssue(TB_Title.Text, TB_Description.Text, DTP_DueDate.Text, CB_Status.SelectedValue.ToString, _
                                        TV_Category.SelectedNode.Tag.ToString, CB_Assigned.SelectedValue.ToString, LBL_TaskID.Text), MsgBoxStyle.Information, "Notification")
        Me.Close()
    End Sub
    Sub LoadItem()
        Try
            Dim TaskItem As New DataTable
            Dim intIndex As Integer
            TaskItem = IssuesItem(LBL_TaskID.Text)
            TB_Title.Text = TaskItem.Rows(0).Item("Title").ToString
            TB_Description.Text = TaskItem.Rows(0).Item("Description").ToString
            DTP_DueDate.Value = TaskItem.Rows(0).Item("DueDate").ToString
            intIndex = TaskItem.Rows(0).Item("StatusID").ToString
            CB_Status.SelectedIndex = intIndex - 1
            CB_Assigned.SelectedValue = TaskItem.Rows(0).Item("AssignedTo").ToString

            Dim n As TreeNode
            For Each n In TV_Category.Nodes
                PrintRecursive(n, TaskItem.Rows(0).Item("CategoryID").ToString)
            Next

        Catch ex As Exception
            MsgBox("Error loading task: " & ex.Message)
        End Try
    End Sub
    'Do Loopin
    Private Function PrintRecursive(ByVal n As TreeNode, NodeTag As Long)
        Dim aNode As TreeNode
        For Each aNode In n.Nodes
            If aNode.Tag = NodeTag Then
                TV_Category.SelectedNode = aNode
                aNode.EnsureVisible()

                Exit Function
            End If
            PrintRecursive(aNode, NodeTag)

        Next
    End Function
End Class
Public Class StatusItem
    Public Sub New(ByVal name As String, ByVal id As Integer)
        mID = id
        mName = name
    End Sub

    Private mID As Integer
    Public Property ID() As Integer
        Get
            Return mID
        End Get
        Set(ByVal value As Integer)
            mID = value
        End Set
    End Property

    Private mName As String
    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

End Class