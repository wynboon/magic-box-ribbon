﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing

Public Class frm_IssuesMain

    Private Sub frm_IssuesMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Payment Remover -> Version : " & My.Settings.Setting_Version
            Me.Icon = My.Resources.AppIcon
            LockForm(Me)
            Me.Cursor = Cursors.AppStarting

            'load form
            Call LoadTasks()

            Me.Enabled = False
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True

        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Public Sub LoadTasks()

        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select * from [vw_TaskItems]")

            If ckb_ViewAll.Checked = False Or Ckb_ViewComplete.Checked = False Then
                sSQL = sSQL & " Where "
            End If

            If ckb_ViewAll.Checked = False Then
                sSQL = sSQL & " (Creator = " & Globals.Ribbons.Ribbon1.CurrentTenantID & " ) or " & _
                "(AssignedTo = " & Globals.Ribbons.Ribbon1.CurrentTenantID & " ) "
            End If

            If Ckb_ViewComplete.Checked = False Then
                If ckb_ViewAll.Checked = False Then
                    sSQL = sSQL & "and "
                End If
                sSQL = sSQL & " StatusName <> 'Complete'"
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Tasks")
            connection.Close()
            DGV_TaskDetail.DataSource = ds
            DGV_TaskDetail.DataMember = "Tasks"
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

            DGV_TaskDetail.Columns("TaskID").Visible = False
            DGV_TaskDetail.Columns("CategoryID").Visible = False
            DGV_TaskDetail.Columns("Creator").Visible = False
            DGV_TaskDetail.Columns("AssignedTo").Visible = False

            DGV_TaskDetail.Columns("Title").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            DGV_TaskDetail.Columns("Description").Width = 600

            'if overdue, color in red
            For Each row As DataGridViewRow In DGV_TaskDetail.Rows
                If Date.Parse(DGV_TaskDetail.Rows(row.Index).Cells("DueDate").Value, Nothing, Globalization.DateTimeStyles.None) < Date.Today Then
                    DGV_TaskDetail.Rows(row.Index).DefaultCellStyle.ForeColor = Color.Red
                    DGV_TaskDetail.Rows(row.Index).ReadOnly = True
                Else
                    DGV_TaskDetail.Rows(row.Index).DefaultCellStyle.ForeColor = Color.Green
                    DGV_TaskDetail.Rows(row.Index).ReadOnly = False
                End If
            Next

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub btn_Task_Click(sender As Object, e As EventArgs) Handles btn_Task.Click
        Dim frmNew As New frm_IssueAdd
        frmNew.ShowDialog()
        LoadTasks()
    End Sub

    Private Sub ckb_ViewAll_CheckedChanged(sender As Object, e As EventArgs) Handles ckb_ViewAll.CheckedChanged
        LoadTasks()
    End Sub
    Private Sub Ckb_ViewComplete_CheckedChanged(sender As Object, e As EventArgs) Handles Ckb_ViewComplete.CheckedChanged
        LoadTasks()
    End Sub

    Private Sub Btn_Setup_Click(sender As Object, e As EventArgs) Handles Btn_Setup.Click
        Dim Frm_IssueSetup As New Frm_IssueSetup
        Frm_IssueSetup.ShowDialog()
    End Sub

    Private Sub Btn_EditTask_Click(sender As Object, e As EventArgs) Handles Btn_EditTask.Click
        Dim TaskID As Integer
        TaskID = DGV_TaskDetail.SelectedRows(0).Cells("TaskID").Value.ToString
        Dim Frm_IssueEdit As New frm_IssueEditTask
        Frm_IssueEdit.LBL_TaskID.Text = TaskID
        Frm_IssueEdit.ShowDialog()
        Call LoadTasks()
    End Sub

    Private Sub DGV_TaskDetail_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_TaskDetail.CellContentClick
        GetCommentory()
    End Sub

    Private Sub DGV_TaskDetail_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DGV_TaskDetail.CellMouseClick
        ' MsgBox(DGV_TaskDetail.SelectedRows.Item("StatusName").ToString)
    End Sub

    Private Sub Btn_Comment_Click(sender As Object, e As EventArgs) Handles Btn_Comment.Click
        Dim frm_commentory As New Frm_IssueCommentory
        frm_commentory.lbl_TaskID.Text = DGV_TaskDetail.SelectedRows(0).Cells("TaskID").Value
        frm_commentory.ShowDialog()
        GetCommentory()
    End Sub
    Function GetCommentory()
        'get Commentory
        Dim dt_Commentory As DataTable
        dt_Commentory = List_Commentory(DGV_TaskDetail.SelectedRows(0).Cells("TaskID").Value)
        If Not IsNothing(dt_Commentory) Then
            DGV_TaskHistory.DataSource = dt_Commentory
            DGV_TaskHistory.Refresh()
        End If
    End Function
End Class