﻿Imports System.Windows.Forms
Imports System.Data

Public Class frm_IssueAdd

    Private Sub frm_IssueAdd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Text = "MagicBox -> " & My.Settings.CurrentCompany & " -> Add Task -> Version : " & My.Settings.Setting_Version
            Me.Icon = My.Resources.AppIcon

            'Get users
            Call GetUsers()
            Call BuildTree()

            Dim Status As New List(Of Status)()
            Status.Add(New Status("Waiting", 1))
            Status.Add(New Status("In Progress", 2))
            Status.Add(New Status("Complete", 3))
            Status.Add(New Status("On Hold", 4))
            Me.CB_Status.DataSource = Status
            Me.CB_Status.DisplayMember = "Name"
            Me.CB_Status.ValueMember = "Id"

        Catch ex As Exception
            Me.Cursor = Cursors.Arrow
            Me.Enabled = True
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub GetUsers()
        Dim ds As New DataSet
        ds = GetGroupUsers()
        CB_Assigned.DataSource = ds.Tables(0)
        CB_Assigned.ValueMember = "Value"
        CB_Assigned.DisplayMember = "Display"
        CB_Assigned.SelectedIndex = 0
    End Sub
    Function BuildTree(Optional FindString As String = "0")
        Try
            TV_Category.Nodes.Clear()
            Dim dt As New DataTable
            'Fill Treeview with summary and then detailed info
            dt = IssuesCategoriesTree()
            If dt.Rows.Count <= 0 Then
                Exit Function
            End If

            For Each dr As DataRow In dt.Rows
                AddNode(dr("ParentID").ToString(), dr("ParentDesc").ToString(), _
                          dr("ChildID").ToString(), dr("ChildDesc").ToString(), dr("ChildID").ToString(), True)
            Next

        Catch ex As Exception
            MsgBox("Error loading tree view: " & ex.Message, MsgBoxStyle.Critical, "Loading Error")
        End Try
    End Function
    Private Sub AddNode(ParentKey As String, parentText As String, ChildKey As String, ChildText As String, ChildTag As String, ExpNode As Boolean)
        Dim node As New List(Of TreeNode)
        Dim InsertNode As New TreeNode
        node.AddRange(TV_Category.Nodes.Find(ParentKey, True))
        If Not node.Any Then
            node.Add(TV_Category.Nodes.Add(ParentKey, parentText))
        End If
        InsertNode.Name = ChildKey
        InsertNode.Text = ChildText
        InsertNode.Tag = ChildTag
        node(0).Nodes.Add(InsertNode)

        If ExpNode = True Then
            node(0).Expand()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MsgBox(Mod_Issues.InsertIssue(TB_Title.Text, TB_Description.Text, DTP_DueDate.Text, CB_Status.SelectedValue.ToString, _
                                      TV_Category.SelectedNode.Tag.ToString, CB_Assigned.SelectedValue.ToString), MsgBoxStyle.Information, "Add Task")
    End Sub
End Class
Public Class Status
    Public Sub New(name As String, id As Integer)
        Me.Name = name
        Me.Id = id
    End Sub
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String
    Public Property Id() As Integer
        Get
            Return m_Id
        End Get
        Set(value As Integer)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer
End Class