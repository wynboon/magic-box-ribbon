﻿Imports System.Data

Public Class Frm_IssueCommentory

    Private Sub Frm_Commentory_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim dt_ActionTypes As New DataTable
        dt_ActionTypes = Mod_Issues.IssuesTaskTypes
        CB_ActionTypes.DataSource = dt_ActionTypes
        CB_ActionTypes.ValueMember = "TaskTypeID"
        CB_ActionTypes.DisplayMember = "TaskTypeName"
        CB_ActionTypes.Refresh()

    End Sub

    Private Sub Btn_Save_Click(sender As Object, e As EventArgs) Handles Btn_Save.Click
        Call Insert_TaskCommentory(lbl_TaskID.Text, CB_ActionTypes.SelectedValue.ToString, TB_ActionComment.Text, 1)
        Me.Close()

    End Sub

    Private Sub Btn_Cancel_Click(sender As Object, e As EventArgs) Handles Btn_Cancel.Click
        Me.Close()
    End Sub
End Class