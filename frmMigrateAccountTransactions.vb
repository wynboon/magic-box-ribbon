﻿Imports System.Windows.Forms
Imports System.Data
Imports System.Globalization

Public Class frmMigrateAccountTransactions

#Region "Variables"
    Dim ExtractedData As New DataExtractsClass
#End Region
#Region "Methods"
    Private Sub InitialLoad()
        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            cmbCurrentAccount.DataSource = ExtractedData.GetAccounts
            cmbDestinationAccount.DataSource = ExtractedData.GetAccounts
            Cursor = Cursors.Arrow
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Accounts error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub AddGridFormatting()
        Dim IgnoreTransactionCheckBox As New DataGridViewCheckBoxColumn
        With IgnoreTransactionCheckBox
            .Name = "ckbMoveTrans"
            .HeaderText = "Move"
            .ReadOnly = False
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        End With
        dgvTransactions.Columns.Insert(0, IgnoreTransactionCheckBox)
        For c As Integer = 0 To dgvTransactions.ColumnCount - 1
            If c = 0 Or c = 13 Then
                dgvTransactions.Columns(c).ReadOnly = False
            Else
                dgvTransactions.Columns(c).ReadOnly = True
            End If
        Next

    End Sub
#End Region
    Private Sub frmMigrateAccountTransactions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Transactions Migration -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
        InitialLoad()
        Enabled = True
        Cursor = Cursors.Arrow
    End Sub
    Private Sub rdbAccount_CheckedChanged(sender As Object, e As EventArgs) Handles rdbAccount.CheckedChanged
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            If rdbAccount.Checked = True Then
                SortByDesc = False
                cmbCurrentAccount.DataSource = Nothing
                cmbCurrentAccount.DataSource = ExtractedData.GetAccounts
                cmbDestinationAccount.DataSource = Nothing
                cmbDestinationAccount.DataSource = ExtractedData.GetAccounts
                cmbCurrentAccount.Focus()
            End If

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Accounts error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try

    End Sub
    Private Sub rdbDesc_CheckedChanged(sender As Object, e As EventArgs) Handles rdbDesc.CheckedChanged
        Try
            Enabled = False
            Cursor = Cursors.AppStarting

            If rdbDesc.Checked = True Then
                SortByDesc = True
                cmbCurrentAccount.DataSource = Nothing
                cmbCurrentAccount.DataSource = ExtractedData.GetAccountsSortByDesc
                cmbDestinationAccount.DataSource = Nothing
                cmbDestinationAccount.DataSource = ExtractedData.GetAccountsSortByDesc
                cmbCurrentAccount.Focus()
            End If

            Enabled = True
            Cursor = Windows.Forms.Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Accounts error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try

    End Sub
    Private Sub btnLoadTransactions_Click(sender As Object, e As EventArgs) Handles btnLoadTransactions.Click

        Try
            Dim StartDate As Date
            Dim EndDate As Date

            If cmbCurrentAccount.SelectedIndex <> -1 Then

                Dim AccountInfo As String = CStr(cmbCurrentAccount.Text)
                Dim Details As String() = AccountInfo.Split(New Char() {"-"c})
                Dim SegmentFour As String = ""
                Dim SegmentThreeDesc As String = ""

                If rdbAccount.Checked = True Then
                    SegmentFour = Trim(Details(0).ToString)
                    SegmentThreeDesc = Trim(Details(1).ToString)
                Else
                    SegmentFour = Trim(Details(1).ToString)
                    SegmentThreeDesc = Trim(Details(0).ToString)
                End If


                If SegmentFour = "" Or SegmentFour = String.Empty Then
                    Exit Sub
                Else
                    Enabled = False
                    Cursor = Cursors.AppStarting
                    StartDate = DateTime.ParseExact(dtpFrom.Text, "dd MMM yyyy", CultureInfo.InvariantCulture)
                    EndDate = DateTime.ParseExact(dtpTo.Text, "dd MMM yyyy", CultureInfo.InvariantCulture)
                    TransactionBindingSource.DataSource = ExtractedData.GetAccountTransactions(SegmentFour, StartDate, EndDate)
                    If dgvTransactions.Columns.Contains("ckbMoveTrans") = False Then
                        AddGridFormatting()
                    End If
                    Enabled = True
                    Cursor = Cursors.Arrow
                End If
            Else
                MessageBox.Show("Please select current account in order to continue!", "Transactions migration", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbCurrentAccount.Focus()
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Accounts error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try

    End Sub
    Private Sub btnMoveTransactions_Click(sender As Object, e As EventArgs) Handles btnMoveTransactions.Click

        Try

            If cmbCurrentAccount.SelectedIndex <> -1 Then

                Dim AccountInfo As String = CStr(cmbDestinationAccount.Text)
                Dim Details As String() = AccountInfo.Split(New Char() {"-"c})
                Dim SegmentFour As String = Details(0).ToString
                Dim SegmentThreeDesc As String = Details(1).ToString
                Dim DetailsCount As Integer
                DetailsCount = Details.Length - 1
                If rdbAccount.Checked = True Then
                    SegmentFour = Trim(Details(0).ToString)
                    SegmentThreeDesc = Trim(Details(1).ToString)
                Else
                    SegmentFour = Trim(Details(DetailsCount).ToString)
                    SegmentThreeDesc = Trim(Details(0).ToString)
                End If



                If SegmentFour = "" Or SegmentFour = String.Empty Then
                    Exit Sub
                Else

                    If cmbCurrentAccount.Text = cmbDestinationAccount.Text Then
                        MessageBox.Show("You cannot move transactions to the same account!", "Accounts selection", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If

                    Enabled = False
                    Cursor = Cursors.AppStarting

                    Dim GLAccount As String = ExtractedData.GetAccNumber(SegmentFour)

                    Dim MoveCount As Integer = 0
                    For Each row As DataGridViewRow In dgvTransactions.Rows

                        If CBool(row.Cells("ckbMoveTrans").Value) = True Then
                            'row.Cells(12).Value = GLAccount
                            row.Cells("DescriptionCodeDataGridViewTextBoxColumn").Value = SegmentFour
                            MoveCount += 1
                        End If

                    Next

                    Dim ConfirmMove As DialogResult = MessageBox.Show("Are you sure you want to move '" & MoveCount & _
                    "' transaction(s) from this account '" & cmbCurrentAccount.Text & "' to this account '" & cmbDestinationAccount.Text & "' ?, please confirm.", _
                    "Transaction(s) migration confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                    If ConfirmMove = Windows.Forms.DialogResult.Yes Then
                        TransactionBindingSource.EndEdit()
                        ExtractedData.SaveChanges()
                        MessageBox.Show("'" & MoveCount & "' transaction(s) succesfully moved from '" & cmbCurrentAccount.Text & "' to '" & cmbDestinationAccount.Text & "'.", "Migration results", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        TransactionBindingSource.DataSource = Nothing
                    End If

                    TransactionBindingSource.DataSource = Nothing
                    Enabled = True
                    Cursor = Cursors.Arrow

                End If

            Else
                MessageBox.Show("Please select a destination account in order to continue!", "Transactions migration", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbDestinationAccount.Focus()
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Accounts error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub ckbSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles ckbSelectAll.CheckedChanged
        Try
            If ckbSelectAll.Checked = True Then
                If dgvTransactions.Rows.Count = 0 Then
                    ckbSelectAll.Checked = False
                    MessageBox.Show("Please load transactions in order to use this feauture", "Migrate Transactions", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cmbCurrentAccount.Focus()
                    Exit Sub
                End If
            End If

            With ckbSelectAll

                If .Checked = True Then

                    Enabled = False
                    Cursor = Cursors.AppStarting
                    For Each row As DataGridViewRow In dgvTransactions.Rows
                        row.Cells("ckbMoveTrans").Value = CBool(True)
                    Next
                    Enabled = True
                    Cursor = Cursors.Arrow

                ElseIf .Checked = False Then

                    Enabled = False
                    Cursor = Cursors.AppStarting
                    For Each row As DataGridViewRow In dgvTransactions.Rows
                        row.Cells("ckbMoveTrans").Value = CBool(False)
                    Next
                    Enabled = True
                    Cursor = Cursors.Arrow

                End If
            End With

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Accounts error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub
End Class