﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Queries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Queries))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DateTimePicker_To = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DateTimePicker_From = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button_All_Supplier_Invoices = New System.Windows.Forms.Button()
        Me.TextBox_TotInvoiced = New System.Windows.Forms.TextBox()
        Me.TextBox_TotBalanced = New System.Windows.Forms.TextBox()
        Me.TextBox_TotPaid = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(38, 202)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(410, 228)
        Me.DataGridView1.TabIndex = 0
        '
        'DateTimePicker_To
        '
        Me.DateTimePicker_To.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_To.Location = New System.Drawing.Point(168, 52)
        Me.DateTimePicker_To.Name = "DateTimePicker_To"
        Me.DateTimePicker_To.Size = New System.Drawing.Size(137, 20)
        Me.DateTimePicker_To.TabIndex = 31
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Blue
        Me.Label5.Location = New System.Drawing.Point(165, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "To Date"
        '
        'DateTimePicker_From
        '
        Me.DateTimePicker_From.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_From.Location = New System.Drawing.Point(38, 53)
        Me.DateTimePicker_From.Name = "DateTimePicker_From"
        Me.DateTimePicker_From.Size = New System.Drawing.Size(124, 20)
        Me.DateTimePicker_From.TabIndex = 30
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Blue
        Me.Label3.Location = New System.Drawing.Point(35, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "From Date"
        '
        'Button_All_Supplier_Invoices
        '
        Me.Button_All_Supplier_Invoices.Location = New System.Drawing.Point(38, 94)
        Me.Button_All_Supplier_Invoices.Name = "Button_All_Supplier_Invoices"
        Me.Button_All_Supplier_Invoices.Size = New System.Drawing.Size(147, 23)
        Me.Button_All_Supplier_Invoices.TabIndex = 34
        Me.Button_All_Supplier_Invoices.Text = "All Supplier Invoices"
        Me.Button_All_Supplier_Invoices.UseVisualStyleBackColor = True
        '
        'TextBox_TotInvoiced
        '
        Me.TextBox_TotInvoiced.Location = New System.Drawing.Point(255, 445)
        Me.TextBox_TotInvoiced.Name = "TextBox_TotInvoiced"
        Me.TextBox_TotInvoiced.Size = New System.Drawing.Size(93, 20)
        Me.TextBox_TotInvoiced.TabIndex = 44
        '
        'TextBox_TotBalanced
        '
        Me.TextBox_TotBalanced.Location = New System.Drawing.Point(156, 446)
        Me.TextBox_TotBalanced.Name = "TextBox_TotBalanced"
        Me.TextBox_TotBalanced.Size = New System.Drawing.Size(93, 20)
        Me.TextBox_TotBalanced.TabIndex = 43
        '
        'TextBox_TotPaid
        '
        Me.TextBox_TotPaid.Location = New System.Drawing.Point(354, 446)
        Me.TextBox_TotPaid.Name = "TextBox_TotPaid"
        Me.TextBox_TotPaid.Size = New System.Drawing.Size(93, 20)
        Me.TextBox_TotPaid.TabIndex = 42
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(96, 449)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "TOTAL"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(38, 123)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(147, 23)
        Me.Button1.TabIndex = 46
        Me.Button1.Text = "All Supplier Payments"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label8.Location = New System.Drawing.Point(388, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 13)
        Me.Label8.TabIndex = 49
        Me.Label8.Text = "version 5.6"
        '
        'Queries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(473, 484)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox_TotInvoiced)
        Me.Controls.Add(Me.TextBox_TotBalanced)
        Me.Controls.Add(Me.TextBox_TotPaid)
        Me.Controls.Add(Me.Button_All_Supplier_Invoices)
        Me.Controls.Add(Me.DateTimePicker_To)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.DateTimePicker_From)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Queries"
        Me.Text = "Queries"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DateTimePicker_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button_All_Supplier_Invoices As System.Windows.Forms.Button
    Friend WithEvents TextBox_TotInvoiced As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_TotBalanced As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_TotPaid As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
