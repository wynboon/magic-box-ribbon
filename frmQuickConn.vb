﻿Imports System.Windows.Forms

Public Class frmQuickConn

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try

            If txtConnName.Text = "" Then
                MessageBox.Show("Please supply the connection name", "Connection name", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtConnName.Focus()
                Exit Sub
            End If

            If txtConnString.Text = "" Then
                MessageBox.Show("Please supply the connection string", "Connection string", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtConnString.Focus()
                Exit Sub
            End If

            If txtCo_ID.Text = "" Or Not IsNumeric(txtCo_ID.Text) Then
                MessageBox.Show("Please supply the company ID in a correct format, Co_ID value should be numeric", "Company ID", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtCo_ID.Focus()
                Exit Sub
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            My.Settings.CS_Setting = txtConnName.Text.Trim
            My.Settings.ListViewValues = txtConnString.Text.Trim & "~" & "SQL" & "~" & txtConnName.Text.Trim
            My.Settings.Setting_CompanyID = txtCo_ID.Text.Trim
            My.Settings.Save()

            Enabled = True
            Cursor = Cursors.Arrow

            MessageBox.Show("Intial connection succesfully set, please continue to add more connections on the settings screen", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured. : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Temp_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class