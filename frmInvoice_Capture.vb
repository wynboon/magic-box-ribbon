﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Collections


Public Class frmInvoice_Capture
#Region "Variables"
    Public IDforLink As String = ""
    Dim theLink As Integer = 0
    Public RefreshEdit As Boolean = False
    Dim MBData As New DataExtractsClass
#End Region
    Dim oProgressBar As ProgressBar
    Dim oSuppId
    Dim blnLoadingForm As Boolean
    Dim blnCOMPLETE As Boolean
    Dim BatchID_Check As Integer
    Dim blnMakeAdditionalPayments As Boolean
    Dim blnFormFinishedLoading As Boolean
    Dim blnRunning As Boolean
    Dim blnFormLoadEventRunning As Boolean

    Dim xAccessAdapter_Accounts As OleDbDataAdapter
    Dim xAccessTable_Accounts As New DataTable
    Dim xSQLAdapter_Accounts As SqlDataAdapter
    Dim xSQLTable_Accounts As New DataTable

    Dim xAccessAdapter_Suppliers As OleDbDataAdapter
    Dim xAccessTable_Suppliers As New DataTable
    Dim xSQLAdapter_Suppliers As SqlDataAdapter
    Dim xSQLTable_Suppliers As New DataTable

    Dim xAccessAdapter_Periods As OleDbDataAdapter
    Dim xAccessTable_Periods As New DataTable
    Dim xSQLAdapter_Periods As SqlDataAdapter
    Dim xSQLTable_Periods As New DataTable

    Dim xAccessAdapter_Search As OleDbDataAdapter
    Dim xAccessTable_Search As New DataTable
    Dim xSQLAdapter_Search As SqlDataAdapter
    Dim xSQLTable_Search As New DataTable

    Private blnDefaultsLoaded As Boolean
    Dim TaxPerc As Double
    Dim TaxCalc As Double


    Public Sub Add_Transaction_to_DGV(ByVal oWhich_DGV As String, ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                     ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                     ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                     ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                     ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                     ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                     ByVal oDocType As String, ByVal oSupplierName As String, ByVal oInfo As String, ByVal oInfo2 As String, ByVal oInfo3 As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)
            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName
            newRow.Cells(31).Value = oInfo
            newRow.Cells(32).Value = oInfo2
            newRow.Cells(33).Value = oInfo3

            If oWhich_DGV = "DGV_Transactions" Then
                Me.DGV_Transactions.Rows.Add(newRow)
            Else
                MsgBox("ERROR ON DGV CLEANUP")
            End If

        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox(ex.Message & " 048")
        End Try


    End Sub


    Sub AppendTransactions(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String


        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value
                oLinkID = oDGV.Rows(i).Cells(2).Value

                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value

                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value
                oSupplierID = oDGV.Rows(i).Cells(16).Value
                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                End If

                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 049")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL(ByVal oDGV As DataGridView, ByVal oTable As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer = 0
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String

        Dim i As Integer
        Dim cmd As SqlCommand

        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To oDGV.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = oDGV.Rows(i).Cells(0).Value
                oTransactionID = oDGV.Rows(i).Cells(1).Value

                'If Text = "Supplier Debits" Then
                '    If theLink = 0 Then
                '        If GetLinkIDByRef(oDGV.Rows(i).Cells(8).Value.ToString) <> 0 Then
                '            Dim LinkID As Integer = CInt(GetLinkIDByRef(oDGV.Rows(i).Cells(8).Value.ToString))
                '            oLinkID = LinkID
                '            theLink = oLinkID
                '        Else
                '            MessageBox.Show("Supplied invoice number does not exist, please re-type the invoice number to verify", "Supplier Debit", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                '            InvoiceNumber_TextBox.Text = ""
                '            InvoiceNumber_TextBox.Focus()
                '            Label_Success.Text = "ENTRY FAILED!"

                '            Exit Sub
                '        End If
                '    End If
                'Else
                '    oLinkID = oDGV.Rows(i).Cells(2).Value
                'End If
                oLinkID = oDGV.Rows(i).Cells(2).Value
                strTransactionDate = oDGV.Rows(i).Cells(3).Value
                strCaptureDate = oDGV.Rows(i).Cells(4).Value
                strCaptureDate = Date.Now.ToString("dd MMM yyyy") & " " & Date.Now.ToLongTimeString
                sPPeriod = oDGV.Rows(i).Cells(5).Value
                sFinYear = oDGV.Rows(i).Cells(6).Value
                sGDC = oDGV.Rows(i).Cells(7).Value
                sReference = oDGV.Rows(i).Cells(8).Value
                sDescription = oDGV.Rows(i).Cells(9).Value
                sAccountNumber = oDGV.Rows(i).Cells(10).Value
                sLinkAcc = oDGV.Rows(i).Cells(11).Value
                oAmount = oDGV.Rows(i).Cells(12).Value
                oTaxType = oDGV.Rows(i).Cells(13).Value
                oTaxAmount = oDGV.Rows(i).Cells(14).Value
                sUserID = oDGV.Rows(i).Cells(15).Value

                oSupplierID = CInt(cmbSupplier.SelectedValue)

                oEmployeeID = oDGV.Rows(i).Cells(17).Value
                sDescription2 = oDGV.Rows(i).Cells(18).Value
                sDescription3 = oDGV.Rows(i).Cells(19).Value
                sDescription4 = oDGV.Rows(i).Cells(20).Value
                sDescription5 = oDGV.Rows(i).Cells(21).Value
                blnPosted = oDGV.Rows(i).Cells(22).Value
                blnAccounting = oDGV.Rows(i).Cells(23).Value
                blnVoid = oDGV.Rows(i).Cells(24).Value
                sTransactionType = oDGV.Rows(i).Cells(25).Value
                oDR_CR = oDGV.Rows(i).Cells(26).Value
                oContraAccount = oDGV.Rows(i).Cells(27).Value
                oDescriptionCode = oDGV.Rows(i).Cells(28).Value
                oDocType = oDGV.Rows(i).Cells(29).Value
                oSupplierName = oDGV.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value
                    sSQL = "INSERT INTO " & oTable & " ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                    sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                    sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                        sSQL = sSQL & ", Co_ID"
                    End If
                    sSQL = sSQL & " ) "
                    sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                    sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                    sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                    sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                    sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                    sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                    sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                    sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                    sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                    sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                    sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                    sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                    sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                    sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                    sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                    If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                        sSQL = sSQL & ", " & oCompanyID & " as Expr32"  '##### NEW #####
                    End If

                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++
            MsgBox(ex.Message & " 050")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True
        Finally
            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub
    Function GetLinkIDByRef(ByVal REF As String) As Integer

        Dim LinkID As Integer = 0
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "SELECT TOP 1 TransactionID FROM Transactions WHERE Reference = '" & REF & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            LinkID = CInt(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return LinkID
        Catch ex As Exception
            MsgBox("There was an error checking an invoice number against a Supplier! " & ex.Message & ".")
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try
    End Function
    Private Sub Invoice_Capture_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LockForm(Me)
        oFormLoad()
    End Sub
    Public Sub oFormLoad()
        Try
            Dim SizePoint As Point
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            blnFormFinishedLoading = False

            Me.ShowInTaskbar = True 'used to mirror Form_Closing
            oProgressBar = Me.ProgressBar1
            oProgressBar.Visible = True
            oProgressBar.Minimum = 1
            oProgressBar.Maximum = 100
            oProgressBar.Value = 1
            Me.Label_Version.Text = "Version " & My.Settings.Setting_Version
            blnLoadingForm = True
            Me.Date_From.Value = System.DateTime.Now.AddMonths(-1)
            ' Me.Size = New Size(SizePoint)
            ResizeForm("PositionSingle")

            Me.ListView1.Columns.Add("", 100, HorizontalAlignment.Left) 'Gross Amount
            Me.ListView1.Columns.Add("", 0, HorizontalAlignment.Left) 'Credit
            Me.ListView1.Columns.Add("", 40, HorizontalAlignment.Left) 'VAT type
            Me.ListView1.Columns.Add("", 100, HorizontalAlignment.Left) 'VAT Amount
            Me.ListView1.Columns.Add("", 0, HorizontalAlignment.Left) 'Credit VAT
            Me.ListView1.Columns.Add("", 80, HorizontalAlignment.Left) 'ex VAT
            Me.ListView1.Columns.Add("", 195, HorizontalAlignment.Left) 'Cat1
            Me.ListView1.Columns.Add("", 195, HorizontalAlignment.Left) 'Cat2
            Me.ListView1.Columns.Add("", 195, HorizontalAlignment.Left) 'Cat3
            Me.ListView1.Columns.Add("", 1, HorizontalAlignment.Left) 'ACCOUNT

            Dim oResponse As MsgBoxResult
            If Fill_DGV_Accounts() = False Then
                oResponse = MessageBox.Show("Problem connecting to database, Would you like to continue?", "Database Connectivity", MessageBoxButtons.YesNo)
                If oResponse = MsgBoxResult.No Then
                    Me.Close()
                End If
            End If

            oProgressBar.Value = 10
            Fill_DGV_Suppliers()

            oProgressBar.Value = 30
            Fill_DGV_Periods()
            oProgressBar.Value = 50

            ' Me.AddRow_Button.Visible = False
            If LoadTax() = False Then
                MsgBox("Error loading TaxTypes, please resolve before trying to enter invoices.", MsgBoxStyle.Critical, "Tax Types")
                Me.Close()
                Exit Sub
            End If

            Fill_Supplier_Combobox()
            oProgressBar.Value = 60

            Fill_Edit_Supplier_Combobox()
            oProgressBar.Value = 70

            Me.DateTimePicker_From.Value = System.DateTime.Now.AddYears(-1) 'NB Don't do because default should be one day

            Fill_Payment_Types_Combo()
            oProgressBar.Value = 80
            'Fill the category ComboBoxes

            Fill_Category1_Combobox()
            Fill_DataGridView2()
            oProgressBar.Value = 90
            Me.EditInvoices_CheckBox.Checked = False
            Me.ComboBox_Status_Delete_Invoice.SelectedIndex = 0
            Me.InvoiceDate_DateTimePicker.Focus()
            blnLoadingForm = False
            Call ClearBoxes()
            Me.InvoiceDate_DateTimePicker.Value = Date.Now
            oProgressBar.Value = 100
            oProgressBar.Visible = False
            blnFormFinishedLoading = True
            cmbSupplier.SelectedIndex = 0

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured in supplier invoice main screen : " & _
                            ex.Message, "Supplier Invoice", MessageBoxButtons.OK, MessageBoxIcon.Error)
            oProgressBar.Visible = False
        End Try
    End Sub
    Function Fill_DGV_Accounts() As Boolean
        Try
            Dim sSQL As String

            sSQL = "Select * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Accounts.Fill(xSQLTable_Accounts)
                Me.dgvAccounts.DataSource = xSQLTable_Accounts
            End If
            Fill_DGV_Accounts = True
        Catch ex As Exception
            MsgBox(ex.Message & " 011")
            Fill_DGV_Accounts = False
        End Try
    End Function
    Sub Fill_DGV_Suppliers()
        Try

            Dim sSQL As String

            sSQL = "Select * From Suppliers"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Suppliers = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Suppliers.Fill(xSQLTable_Suppliers)
                If xSQLTable_Suppliers.Rows.Count > 0 Then
                    Me.dgvSuppliers.DataSource = xSQLTable_Suppliers
                Else
                    MsgBox("There are no suppliers loaded in the company.", MsgBoxStyle.Critical)
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 012")
        End Try
    End Sub
    Sub Fill_DGV_Periods()
        Try
            Dim sSQL As String

            sSQL = "Select * From Periods"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Periods = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Periods.Fill(xAccessTable_Periods)
                Me.dgvPeriods.DataSource = xAccessTable_Periods
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Periods = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Periods.Fill(xSQLTable_Periods)
                Me.dgvPeriods.DataSource = xSQLTable_Periods
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 013")
        End Try
    End Sub

    Function Last_ID_in_Transactions() As Integer
        Try

            Dim sSQL As String

            Dim connection As New OleDbConnection(My.Settings.CS_Setting)

            sSQL = "SELECT Max(ID) As Expr1 From Transactions"

            Dim cmd As New OleDbCommand(sSQL, connection)

            'you may want to check the state of the connection before opening it.If its already open you'll get an exception.
            connection.Open()

            Last_ID_in_Transactions = cmd.ExecuteScalar()

            connection.Close()

        Catch ex As Exception

            Last_ID_in_Transactions = 1
        End Try
    End Function


    Sub Fill_Payment_Types_Combo()
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Display Name] FROM Accounting Where Type = 'Tender'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            Me.cmbPaymentType.Items.Clear()

            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.cmbPaymentType.Items.Clear()
                While datareader.Read
                    If Not datareader("Display Name").Equals(DBNull.Value) Then
                        Me.cmbPaymentType.Items.Add(datareader("Display Name"))
                    End If
                End While
                connection.Close()
                cmbPaymentType.SelectedIndex = 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 014")
        End Try

    End Sub

    Function SQLConvert(ByVal sString As String) As String
        SQLConvert = Replace(sString, "'", "''")
    End Function


    Sub Fill_Supplier_Combobox()
        Try

            Dim CompanyID As Integer = CInt(My.Settings.Setting_CompanyID)
            ComboBox_Supplier.DataSource = MBData.SupplierList(CompanyID)
            ComboBox_Supplier.DisplayMember = "SupplierName"
            ComboBox_Supplier.ValueMember = "SupplierID"

            cmbSupplier.DataSource = MBData.SupplierList(CompanyID)
            ' cmbSupplier.DataSource = New BindingSource(SupplierIDName, Nothing)
            cmbSupplier.DisplayMember = "SupplierName"
            cmbSupplier.ValueMember = "SupplierID"

            'Dim sSQL As String
            'sSQL = "SELECT Distinct SupplierName FROM Suppliers"

            'If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            '    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            'End If

            'sSQL = sSQL & " Order By SupplierName"

            'If My.Settings.DBType = "Access" Then
            '    Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            '    Dim cmd As New OleDbCommand(sSQL, connection)
            '    connection.Open()
            '    Dim datareader As OleDbDataReader = cmd.ExecuteReader

            '    Try

            '        Me.Supplier_ComboBox.Items.Clear()
            '        Me.ComboBox_Supplier.Items.Clear() 'new
            '        Me.Supplier_ComboBox.Items.Add("[ALL]")
            '        While datareader.Read
            '            Me.Supplier_ComboBox.Items.Add(datareader("SupplierName"))
            '            Me.ComboBox_Supplier.Items.Add(datareader("SupplierName")) 'new
            '        End While
            '        If Me.Supplier_ComboBox.Items.Count > 0 Then
            '            Me.Supplier_ComboBox.SelectedIndex = 0
            '        End If
            '        If Me.ComboBox_Supplier.Items.Count > 0 Then
            '            Me.ComboBox_Supplier.SelectedIndex = 0
            '        End If

            '        connection.Close()

            '    Catch ex As Exception
            '        MsgBox(ex.Message & " 015")
            '        connection.Close()
            '    End Try
            'ElseIf My.Settings.DBType = "SQL" Then

            '    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            '    Dim cmd As New SqlCommand(sSQL, connection)
            '    connection.Open()
            '    Dim datareader As SqlDataReader = cmd.ExecuteReader

            '    Try
            '        Me.Supplier_ComboBox.Items.Clear()
            '        Me.ComboBox_Supplier.Items.Clear() 'new
            '        Me.Supplier_ComboBox.Items.Add("[ALL]")
            '        While datareader.Read
            '            Me.Supplier_ComboBox.Items.Add(datareader("SupplierName"))
            '            Me.ComboBox_Supplier.Items.Add(datareader("SupplierName")) 'new
            '        End While
            '        Me.Supplier_ComboBox.SelectedIndex = 0
            '        Me.ComboBox_Supplier.SelectedIndex = 0
            '        connection.Close()

            'Catch ex As Exception
            '    MsgBox(ex.Message & " 016")
            '    connection.Close()
            'End Try
            '    End If
        Catch ex As Exception
            MsgBox("Error loading the supplier dropdown. Please check that suppliers are loaded! " & ex.Message & " 017")
        End Try

    End Sub

    Sub Fill_Edit_Supplier_Combobox()
        Try


            Dim SupplierIDName As New Hashtable()
            Dim CompanyID As Integer = CInt(My.Settings.Setting_CompanyID)
            'SupplierIDName = MBData.SupplierNameIDPair(CompanyID)
            'Edit_Supplier_Combo.DataSource = New BindingSource(SupplierIDName, Nothing)
            'Edit_Supplier_Combo.DisplayMember = "Value"
            'Edit_Supplier_Combo.ValueMember = "Key"

            Edit_Supplier_Combo.DataSource = MBData.SupplierList(CompanyID)
            'ComboBox_Supplier.DataSource = New BindingSource(SupplierIDName, Nothing)
            Edit_Supplier_Combo.DisplayMember = "SupplierName"
            Edit_Supplier_Combo.ValueMember = "SupplierID"

            'cmbSupplier.DataSource = New BindingSource(SupplierIDName, Nothing)
            'cmbSupplier.DisplayMember = "Value"
            'cmbSupplier.ValueMember = "Key"

            cmbSupplier.DataSource = MBData.SupplierList(CompanyID)
            'ComboBox_Supplier.DataSource = New BindingSource(SupplierIDName, Nothing)
            cmbSupplier.DisplayMember = "SupplierName"
            cmbSupplier.ValueMember = "SupplierID"

            'Dim sSQL As String
            'sSQL = "SELECT Distinct SupplierName FROM Suppliers"
            'If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            '    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            'End If
            'sSQL = sSQL & " Order By SupplierName"


            'Me.Edit_Supplier_Combo.Items.Clear()

            'If My.Settings.DBType = "Access" Then
            '    Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            '    Dim cmd As New OleDbCommand(sSQL, connection)
            '    connection.Open()
            '    Dim datareader As OleDbDataReader = cmd.ExecuteReader
            '    Try
            '        Me.Edit_Supplier_Combo.Items.Clear()
            '        Me.Edit_Supplier_Combo.Items.Add("[ALL]")
            '        While datareader.Read
            '            Me.Edit_Supplier_Combo.Items.Add(datareader("SupplierName"))
            '        End While
            '        Me.Edit_Supplier_Combo.SelectedIndex = 0
            '        connection.Close()
            '    Catch ex As Exception
            '        MsgBox(ex.Message & " 018")
            '        connection.Close()
            '    End Try
            'ElseIf My.Settings.DBType = "SQL" Then
            '    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            '    Dim cmd As New SqlCommand(sSQL, connection)
            '    connection.Open()
            '    Dim datareader As SqlDataReader = cmd.ExecuteReader
            '    Try
            '        Me.Edit_Supplier_Combo.Items.Clear()
            '        Me.Edit_Supplier_Combo.Items.Add("[ALL]")
            '        While datareader.Read
            '            Me.Edit_Supplier_Combo.Items.Add(datareader("SupplierName"))
            '        End While
            '        Me.Edit_Supplier_Combo.SelectedIndex = 0
            '        connection.Close()
            '    Catch ex As Exception
            '        MsgBox(ex.Message & " 019")
            '        connection.Close()
            '    End Try
            'End If
        Catch ex As Exception
            MsgBox(ex.Message & " 020")
        End Try

    End Sub
    Sub GetInvoiceDetailPerSupplier2()

        Try

            Dim oSupplier As String
            oSupplier = Me.Edit_Supplier_Combo.Text
            Dim sSQL_Supplier As String = ""

            If oSupplier = "[ALL]" Or oSupplier = "" Then
                'do nothing
            Else
                oSupplier = Me.Edit_Supplier_Combo.Text
                sSQL_Supplier = " And SupplierName = '" & SQLConvert(oSupplier) & "'"
            End If


            Dim oFrom As String = Me.DateTimePicker_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTimePicker_To.Value.Date.ToString("dd MMMM yyyy")


            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            Else
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If


            Dim sSQL As String
            If Me.ComboBox_Status_Delete_Invoice.Text = "[ALL]" Then
                sSQL = "Select * From [Step4b] Where (Due <=0 Or Due >0)"
            ElseIf Me.ComboBox_Status_Delete_Invoice.Text = "PAID" Then
                sSQL = "Select * From [Step4b] Where Due <=0"
            ElseIf Me.ComboBox_Status_Delete_Invoice.Text = "UNPAID" Then
                sSQL = "Select * From [Step4b] Where Due >0"
            End If

            sSQL = sSQL & sSQL_Supplier
            sSQL = sSQL & sDateCriteria

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            'First clear DGV
            DataGridView1.DataSource = Nothing

            System.Threading.Thread.Sleep(1000)

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            End If

        Catch ex As Exception
            Me.Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
    Private Sub Save1_Click(sender As System.Object, e As System.EventArgs) Handles Save1.Click
        Try
            Dim supplier As String = cmbSupplier.Text
            Dim InvcNumber As String = ""
            If txtInvoiceNumber.Text <> "" Then
                InvcNumber = CStr(txtInvoiceNumber.Text)
            End If

            If txtInvoiceNumber.Text.Trim = "" Then
                MessageBox.Show("Please enter invoice number!", "Invoice Number", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtInvoiceNumber.Focus()
                Exit Sub
            End If


            If lblEditInvoice.Visible = False Then
                If Text.Contains("Supplier Debit") Or Text.Contains("Invoice Capture") Then

                    If cmbSupplier.Text = "" Or cmbSupplier.SelectedIndex = -1 Then
                        MessageBox.Show("Please select a supplier to capture the invoice against before you continue", "Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        txtInvoiceNumber.Text = ""
                        cmbSupplier.Focus()
                        Exit Sub
                    End If

                    If Reference_There(txtInvoiceNumber.Text, CInt(cmbSupplier.SelectedValue)) = True Then
                        MessageBox.Show("The invoice/reference  number already exists for the supplier selected. Please use a different invoice/reference number in order to proceed.", "Duplicate Reference number", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        txtInvoiceNumber.Text = ""
                        txtInvoiceNumber.Focus()
                        Exit Sub
                    End If

                End If
            End If

            If Me.InvoiceTotal_TextBox.Text = "" And Me.MultipleLines_CheckBox.Checked = False Then
                MessageBox.Show("Please enter an invoice amount!", "Invoice Amount", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Enabled = True
                Cursor = Cursors.Arrow
                InvoiceTotal_TextBox.Focus()
                Me.Save1.Enabled = True
                Exit Sub
            End If

            If Me.cmbVATType.Text = "" Then
                MessageBox.Show("Please enter  VAT type!", "VAT type", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Enabled = True
                Cursor = Cursors.Arrow
                cmbVATType.Focus()
                Me.Save1.Enabled = True
                Exit Sub
            End If

            If Me.txtVATAmount.Text = "" Then
                MessageBox.Show("Please enter VAT amount!", "VAT Amount", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Enabled = True
                Cursor = Cursors.Arrow
                txtVATAmount.Focus()
                Me.Save1.Enabled = True
                Exit Sub
            End If


            If Text.Contains("Invoice Capture") Then
                If ComboBox_Status.SelectedIndex = -1 Then
                    MessageBox.Show("Please select the 'invoice status' before you attempt to capture the invoice", "Invoice Capture", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    ComboBox_Status.Focus()
                    Exit Sub
                End If

                If cmbPaymentType.SelectedIndex = -1 And cmbPaymentType.Text <> "" Then
                    MessageBox.Show("Please select the 'payment type' before you attempt to capture the invoice", "Invoice Capture", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    cmbPaymentType.Focus()
                    Exit Sub
                End If

                If InvoiceDate_DateTimePicker.Value < Date.Now And ComboBox_Status.Text = "PAID" Then
                    Dim DateInQuestion As Date = CDate(InvoiceDate_DateTimePicker.Value.ToShortDateString)
                    If cmbPaymentType.Text = "CASH" Then
                        If CheckForCashup(DateInQuestion) > 0 Then
                            MessageBox.Show("There where cashups done on the invoice date you selected, you cannot capture a paid invoice on this date, please contant the MagicBox support team in order to resolve this.", "Invoice capture", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            ClearBoxes()
                            Exit Sub
                        End If
                    End If
                End If
            End If

            blnRunning = True
            Me.DGV_Transactions.Rows.Clear() 'Extra safeguard for intermittent internet to stop
            'data caught in the grid just before a power drop being used
            '   If My.Settings.User_Control_On = "Yes" Then
            ' 'User control On - so test if User is logged in
            ' If Globals.Ribbons.Ribbon1.lblUsername.Label = "-" Then
            ' MsgBox("User control is On. Please log in on the ribbon!")
            ' Exit Sub
            ' End If
            ' End If

            Enabled = False
            Cursor = Cursors.AppStarting
            Dim SuppID As Integer = CInt(cmbSupplier.SelectedValue)
            Call oSave1()
            Me.chkCredit.Checked = False
            Me.MultipleLines_CheckBox.Checked = False
            cmbSupplier.Focus()
            blnRunning = False

            Label_Success.Text = ""
            Invoice_Capture_Load(sender, e)
            cmbSupplier.SelectedValue = SuppID
            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception

        End Try
    End Sub
    Sub oSave1()
        Try
            Me.Save1.Enabled = False

            Me.Label_Success.ForeColor = Color.DarkBlue

            blnCRITICAL_ERROR = False

            Dim blnBalance As Boolean = True

            If blnBalance = False Then
                Exit Sub
            Else
                Call oSave()

                If Text.Contains("Invoice Capture") Then
                    If blnCRITICAL_ERROR = False Then
                        MessageBox.Show("Invoice succesfully entered.", "Invoice confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("Invoice capture aborted.", "Invoice capture", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                ElseIf Text.Contains("Supplier Debit") Then
                    If blnCRITICAL_ERROR = False Then
                        MessageBox.Show("Supplier debit succesfully entered.", "Invoice confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("Supplier debit capture aborted.", "Supplier debit", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                ElseIf Text.Contains("Invoice Editing") Then
                    If blnCRITICAL_ERROR = False Then
                        MessageBox.Show("Invoice succesfully edited.", "Invoice editing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("Invoice edit aborted.", "Invoice edit", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If

            End If

            Me.Save1.Enabled = True

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 022")
            Me.Save1.Enabled = True
            blnCRITICAL_ERROR = True
            Me.Label_Success.Text = "ENTRY FAILED!"
        End Try
    End Sub
    Private Sub Save2_Click(sender As System.Object, e As System.EventArgs) Handles Save2.Click
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim supplier As String = cmbSupplier.Text
            Dim InvcNumber As String = ""
            If txtInvoiceNumber.Text <> "" Then
                InvcNumber = CStr(txtInvoiceNumber.Text)
            End If

            blnRunning = True
            Me.DGV_Transactions.Rows.Clear()

            Dim SuppID As Integer = CInt(cmbSupplier.SelectedValue)
            Call oSave2()

            Me.chkCredit.Checked = False
            Me.MultipleLines_CheckBox.Checked = False
            cmbSupplier.Focus()
            blnRunning = False

            Invoice_Capture_Load(sender, e)
            cmbSupplier.SelectedValue = SuppID
            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try

    End Sub
    Sub oSave2()

        Try

            Me.Label_Success.ForeColor = Color.DarkBlue
            Me.Label_Success.Text = ""
            Me.Save2.Enabled = False
            blnCRITICAL_ERROR = False

            If Me.ListView1.Items.Count < 2 Then
                MsgBox("There are less than 2 line items in the multiples box. This cannot be processed!")
                Me.Save2.Enabled = True
                Exit Sub
            End If

            'First check multiples to see if everything balances
            Dim blnBalance As Boolean = True
            Dim GrossAmount As Decimal
            Dim VATAmount As Decimal
            Dim exVAT As Decimal

            For i As Integer = 0 To Me.ListView1.Items.Count - 1
                'oTotal = oTotal + CDec(Me.ListView1.Items(i).SubItems(0).Text)
                GrossAmount = CDec(Me.ListView1.Items(i).SubItems(0).Text)
                VATAmount = CDec(Me.ListView1.Items(i).SubItems(3).Text)
                exVAT = CDec(Me.ListView1.Items(i).SubItems(5).Text)

                If GrossAmount <> VATAmount + exVAT Then
                    MsgBox("Balance error at line " & CStr(i + 1) & ": 'Gross Amount' of " & CStr(GrossAmount) & " does not equal 'VAT Amount' of " & CStr(VATAmount) & " plus " & " 'ex VAT' of " & CStr(exVAT))
                    blnBalance = False
                End If

            Next

            If blnBalance = False Then
                Exit Sub
            Else
                Call oSave()
            End If

            Me.Save2.Enabled = True

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 023")
            blnCRITICAL_ERROR = True
            Me.Label_Success.Text = "ENTRY FAILED!"
            Me.Save2.Enabled = True
        End Try
    End Sub


    Function Check_Integrity(ByVal oDataGridView As DataGridView) As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To oDataGridView.RowCount - 2 'extra row
                If oDataGridView.Rows(i).Cells(26).Value = "Dr" Then
                    oDebits = oDebits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                ElseIf oDataGridView.Rows(i).Cells(26).Value = "Cr" Then
                    oCredits = oCredits + CDec(oDataGridView.Rows(i).Cells(12).Value)
                End If
            Next

            If oDebits = oCredits Then
                Check_Integrity = True
            Else
                Check_Integrity = False
            End If


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox("There was an error checking integrity " & ex.Message & " 024")
        End Try
    End Function

    Function Supplier_Debits_Switch_Drs_Crs(ByVal oDataGridView As DataGridView) As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim i As Integer

            For i = 0 To oDataGridView.RowCount - 2 'extra row
                If oDataGridView.Rows(i).Cells(26).Value = "Dr" Then
                    oDataGridView.Rows(i).Cells(26).Value = "Cr"
                ElseIf oDataGridView.Rows(i).Cells(26).Value = "Cr" Then
                    oDataGridView.Rows(i).Cells(26).Value = "Dr"
                End If
                'So where DocType is "Invoice" make the Info (sub category of DocType) equal "Supplier Debit"
                If oDataGridView.Rows(i).Cells(29).Value = "Invoice" Then '29 = DocType
                    oDataGridView.Rows(i).Cells(31).Value = "Supplier Debit"
                End If
                'Also make all of the descriptive fields "Supplier Debit"
                oDataGridView.Rows(i).Cells(32).Value = "Supplier Debit"
            Next
            Supplier_Debits_Switch_Drs_Crs = True
        Catch ex As Exception
            blnCRITICAL_ERROR = True
            Supplier_Debits_Switch_Drs_Crs = False
            MsgBox("There was an error switching Drs and Crs in Supplier Debits " & ex.Message & " 024b")
        End Try
    End Function

    Sub oSave()
        Try

            oProgressBar = Me.ProgressBar1
            oProgressBar.Visible = True
            oProgressBar.Minimum = 1
            oProgressBar.Maximum = 100
            oProgressBar.Value = 1

            If Me.MultipleLines_CheckBox.Checked = False Then
                If Me.Cat1.Text = "" Or Me.Cat2.Text = "" Or Me.Cat3.Text = "" Then
                    MsgBox("Please select all three categories for the invoice!")
                    oProgressBar.Visible = False
                    Exit Sub
                End If
            End If

            Dim Test As String

            If Me.ComboBox_Status.Text = "PAID" Then

                If IsNumeric(Me.AmountPaid_TextBox.Text) = False Then
                    MsgBox("Please enter a valid amount in the 'Amount Paid' box")
                    oProgressBar.Visible = False
                    Exit Sub
                Else
                    If CDec(Me.InvoiceTotal_TextBox.Text) - CDec(Me.txtCreditAmount.Text) - CDec(Me.AmountPaid_TextBox.Text) < 0 Then
                        MsgBox("You cannot pay more than the invoice amount (less any credit amount)!")
                        oProgressBar.Visible = False
                        Exit Sub
                    End If
                End If
                ' now test if the payment type is in accounting and if not stop entire transaction
                Test = Get_Segm1Desc_From_DisplayName_in_Accounting(Me.cmbPaymentType.Text)
                If Test = Nothing Then
                    MsgBox("Transaction aborted! Payment not possible. No 'Display Name' item has been set up in the Accounting table for this payment type!")
                    oProgressBar.Visible = False
                    Exit Sub
                End If
            End If

            If cmbSupplier.Text = "" Then
                MessageBox.Show("Please select a supplier in order to continue", "Supplier validation", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Beep()
                oProgressBar.Visible = False
                Exit Sub
            End If

            If Me.lblMagicBox_Account_Segment4.Text = "## NO ACCOUNT ##" Or Me.lblMagicBox_Account_Segment4.Text = "" Or Me.lblMagicBox_Account_Segment4.Text = "." Or Me.lblMagicBox_Account_Segment4.Text = "REF!" Then
                MsgBox("There is no GL account connected to these categories!")
                oProgressBar.Visible = False
                Exit Sub
            End If

            If Me.ComboBox_Status.Text = "PAID" And Me.AmountPaid_TextBox.Text = "" Then
                MsgBox("Please enter an the amount paid!")
                oProgressBar.Visible = False
                Exit Sub
            End If

            'Check multiples add up to invoice total
            Dim oTotal_of_Multiples As Decimal = 0
            If Me.MultipleLines_CheckBox.Checked = True Then
                For k As Integer = 0 To Me.ListView1.Items.Count - 1
                    oTotal_of_Multiples = oTotal_of_Multiples + Me.ListView1.Items(k).SubItems(0).Text()
                Next
                If CDec(Me.InvoiceTotal_TextBox.Text) <> oTotal_of_Multiples Then
                    MsgBox("The total of the gross amounts in the multiples box, must equal the invoice total!")
                    oProgressBar.Visible = False
                    Exit Sub
                End If
            End If

            Call oTotalVAT_To_Box()

            Dim Original_TransactionID = Me.lblEditInvoice_TransactionID.Text
            Call Add_Transactions_to_Invoice_Capture_DGVs(Original_TransactionID)

            If blnCRITICAL_ERROR = True Then
                MsgBox("The current invoice capture has caused critical errors! No data has been saved!")
                GoTo Jump
            End If

            If Me.Text = "Supplier Debit" Then
                'Following is a function so can throw an exception here if it doesn't work
                If Supplier_Debits_Switch_Drs_Crs(Me.DGV_Transactions) = False Then
                    GoTo Jump
                End If
            End If

            If Me.DGV_Transactions.RowCount > 1 Then 'always more than 1 debit/credit
                If Check_Integrity(Me.DGV_Transactions) = False Then
                    MsgBox("The numbers are out of balance and cannot be saved!")
                Else
                    If My.Settings.DBType = "Access" Then
                        Call AppendTransactions(Me.DGV_Transactions, "Transactions")
                    ElseIf My.Settings.DBType = "SQL" Then
                        Call AppendTransactions_SQL(Me.DGV_Transactions, "Transactions")
                    End If

                End If
            End If

Jump:
            Me.DGV_Transactions.Rows.Clear()


            Me.chkCredit.Checked = False
            Me.txtCreditAmount.Text = "0"
            Me.txtCreditExVAT.Text = "0"
            Me.txtCreditVAT.Text = "0"
            If Me.ComboBox_Status.Text <> "PAID LOCK" Then
                Me.ComboBox_Status.Text = "UNPAID"
            End If
            Me.AmountPaid_TextBox.Text = "0"

            oProgressBar.Value = 100
            System.Threading.Thread.Sleep(1000)

            If Me.lblEditInvoice.Visible = True Then

                Call Form_Invoice_Edit_Delete.DeleteInvoice(Original_TransactionID)
                Me.lblEditInvoice.Visible = False
            End If

            Call ClearBoxes()

            oProgressBar.Visible = False


        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MsgBox(ex.Message & " 025")
            blnCRITICAL_ERROR = True
            Me.Label_Success.Text = "ENTRY FAILED!"
        End Try
    End Sub
    Sub oTotalVAT_To_Box()
        'Calculate VAT total based on single or multiple category lines
        Try

            Dim oTotal_VAT As Decimal = 0
            Dim oTotal_Amount As Decimal = 0
            Dim oTotal_exVAT As Decimal = 0
            If Me.MultipleLines_CheckBox.Checked = True Then
                For i As Integer = 0 To Me.ListView1.Items.Count - 1
                    oTotal_VAT = oTotal_VAT + Me.ListView1.Items(i).SubItems(2).Text()
                    oTotal_Amount = oTotal_Amount + Me.ListView1.Items(i).SubItems(0).Text()
                Next
                Me.VATTotal_TextBox.Text = oTotal_VAT
                Me.TotalAmount.Text = oTotal_Amount
            Else
                Me.VATTotal_TextBox.Text = Me.txtVATAmount.Text
                Me.TotalAmount.Text = Me.InvoiceTotal_TextBox.Text
            End If

        Catch ex As Exception
            MsgBox("Box totalling: " & ex.Message & " 026")
        End Try
    End Sub

    Sub oUndo(ByVal sMessage As String)
        Me.DGV_Transactions.Rows.Clear()
        MsgBox(sMessage & vbCrLf & "Please correct this! Invoice abborted!")
        blnCRITICAL_ERROR = True
        oProgressBar.Visible = False
        Exit Sub
    End Sub
    Public Sub Add_Transactions_to_Invoice_Capture_DGVs(ByVal OldTID As String)

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        'Credit note must have different Transaction ID
        Dim oCreditTransactionID As Long

        Dim oLinkID As Integer
        If OldTID <> String.Empty Then
            oLinkID = CInt(OldTID)
        End If
        Dim sTransactionDate As String
        Dim sCaptureDate As String = Date.Now.ToString("dd MMM yyyy") & " " & Date.Now.ToLongTimeString
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sInvoiceNumber As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sContraAccount As String
        Dim sLinkID As String
        Dim oAmount As Decimal
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim sSupplierName As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As Integer
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim TransactionType As String
        Dim sDRCR As String
        Dim sDescriptionCode As String
        Dim sDocType As String
        Dim Info As String = ""
        Dim Info2 As String = ""
        Dim Info3 As String = "This invoice was recreated from Original Invoice : " & IDforLink & " of TransactionID: " & OldTID

        Dim Credit_Note_DRCR As String
        Dim oDGV As String

        Dim Full_Credit_Amount As Decimal
        Dim Credit_Amount As Decimal
        Dim oCredit_List_Box_RowIndex As Integer = -1

        Dim Credit_TaxAmount As Decimal
        Dim Credit_ex_VAT As Decimal

        'NOTE: sContraAccount - VALUE APPLIED ONCE HERE
        Dim sContraAccount_Segment As String
        If Me.lblMagicBox_Account_Segment4.Text <> "## NO ACCOUNT ##" Then
            sContraAccount_Segment = Get_Segment4_Using_Type_in_Accounting("Contra Account")
            sContraAccount = Get_Account_Number_From_Segment4(sContraAccount_Segment)
        Else
            Call oUndo("No valid GL Account.")
            Exit Sub
        End If
        '=======++++++++++++=  First Generate TransactionID and BatchID =========================== 
        oBatch_or_Transaction_generation_Failed = False
        oBatchID = CLng(oCreateBatchID())

        oTransactionID = CLng(oCreateTransactionID()) 'adds a new record to the TransactionID table thus creating a new ID number that is used as the Transaction ID

        Dim oPaid_TransacionID As Long
        If Me.ComboBox_Status.Text = "PAID" Then
            oPaid_TransacionID = CLng(oCreateTransactionID())
        End If
        'ABORT IF ANY OF THE FOLLOWING TRUE
        If oBatch_or_Transaction_generation_Failed = True Or oBatchID = 0 Or oTransactionID = 0 Then
            blnCRITICAL_ERROR = True
            Me.Label_Success.Text = "ENTRY FAILED!"
            Exit Sub
        End If

        '=========================================================================================

        oLinkID = 0
        sTransactionDate = Me.InvoiceDate_DateTimePicker.Value.Date.ToString("dd MMMM yyyy") 'ALWAYS GET DATE OTHERWISE INCLUDES TIME OF DAY
        sPPeriod = GetPeriod(sTransactionDate)
        If IsNumeric(sPPeriod) = False Or CInt(sPPeriod) < 1 Then
            Call oUndo("The invoice date selected doesn't fall into any period in 'Periods' table!")
            Exit Sub
        End If

        sCaptureDate = Date.Now

        'sFinYear = My.Settings.Setting_Year 'Set this once

        sFinYear = GetFinYear(sTransactionDate)
        If IsNumeric(sFinYear) = False Or CInt(sFinYear) < 1 Then
            Call oUndo("The invoice date selected doesn't fall into any Financial Year in 'Periods' table!")
            Exit Sub
        End If

        If sFinYear = "" Or sFinYear = Nothing Then
            Call oUndo("No financial year has been set. Please set this up in 'periods'!")
            Exit Sub
        End If

        sGDC = "G" 'General/Debtors/Creditorsoice 
        sInvoiceNumber = Me.txtInvoiceNumber.Text
        sSupplierName = CStr(cmbSupplier.Text)
        sLinkID = CStr(oBatchID) '$-NEW

        sUserID = Globals.Ribbons.Ribbon1.CurrentTenantID 'for later - they should log on to use this
        'HEROLD-Wrong Suppliers link 2014-01-20
        oSupplierID = CInt(cmbSupplier.SelectedValue) 'NOTE - a DLookup is used here in the SQL Statement
        'oSupplierID = Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") 'Supplier Name
        If IsDBNull(oSupplierID) Then
            Call oUndo("No supplier ID found! Please check the supplier table.")
            Exit Sub
        End If

        sDescription4 = Me.Notes_RichTextBox.Text
        sDescription5 = ""
        oEmployeeID = 0 'not required


        oAmount = CDec(Me.TotalAmount.Text) 'Amount Field

        oTaxAmount = 0 'CDec(Me.VATTotal_TextBox.Text) 'out of the three invoice rows this is the only one that has the tax amount in this column, the others are zero
        sDescription = "CREDITORS CONTROL" '[Description] field
        sDescription2 = "CREDITORS CONTROL" '[Description2] field
        sDescription3 = "CREDITORS CONTROL" '[Description3] field

        TransactionType = 12
        oTaxType = 0 'ALWAYS ZERO
        If Text.Contains("Supplier Debit") Then
            sDRCR = "Dr"
            TransactionType = 1
        Else
            sDRCR = "Cr"
        End If


        'seg4
        Dim sCreditorsControl_Segment4 As String
        'Sorted
        sCreditorsControl_Segment4 = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
        sDescriptionCode = sCreditorsControl_Segment4
        If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
            Call oUndo("No Magic Box Segment 4 code!")
            Exit Sub
        End If
        sAccountNumber = Get_Account_Number_From_Segment4(sDescriptionCode)

        sDocType = "Invoice"
        Info = "Invoice"
        Info2 = "Invoice"

        If Text.Contains("Supplier Debit") Then
            sDocType = "Payment"
            Info = "Supplier Debit"
            Info2 = "Supplier Debit"
        End If

        blnAccounting = True

        If IDforLink <> Nothing Or OldTID <> Nothing Then
            Info3 = "This invoice was recreated from Original Invoice : " & IDforLink & " of TransactionID: " & OldTID
        Else
            Info3 = ""
        End If
        If oAmount <> 0 Then
            Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
                oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)
        End If
        '-----------------    Credit Note   -----------------------------------------

        If Me.chkCredit.Checked = True Then

            oCreditTransactionID = CLng(oCreateTransactionID()) 'adds a new record to the TransactionID

            Credit_Note_DRCR = "Dr"

            blnAccounting = False 'Not in trial balance yet
            Info = "Credit Note" 'oNEW
            Info2 = "Credit Note" 'oNEW

            If Me.MultipleLines_CheckBox.Checked = True Then

                'Calculate full credit amount by totalling the third column (index = 2)
                Dim oTotalCredits As Decimal = 0
                For t As Integer = 0 To Me.ListView1.Items.Count - 1
                    If IsNumeric(Me.ListView1.Items(t).SubItems(1).Text) = True Then
                        oTotalCredits = oTotalCredits + CDec(Me.ListView1.Items(t).SubItems(1).Text)
                    End If
                Next
                Full_Credit_Amount = oTotalCredits
                'Credit_Multiple_Proportion = oTotalCredits / CDec(Me.GrossAmount_TextBox.Text)

            Else
                Full_Credit_Amount = CDec(Me.txtCreditAmount.Text)
                Credit_TaxAmount = CDec(Me.txtCreditVAT.Text)
                Credit_ex_VAT = CDec(Me.txtCreditExVAT.Text)
            End If

            oLinkID = oTransactionID

            Info3 = ""

            If My.Settings.User_Control_On = "Yes" Then
                If My.Settings.Auto_Approve_CreditNotes = "Yes" Then
                    blnAccounting = True
                Else
                    blnAccounting = False
                End If
                If My.Settings.Auto_Approve_EFT = "Yes" Then
                    blnAccounting = True
                Else
                    blnAccounting = False
                End If
            ElseIf My.Settings.User_Control_On = "No" Or My.Settings.User_Control_On = "" Then
                If My.Settings.Auto_Approve_CreditNotes = "Yes" Then
                    blnAccounting = True
                Else
                    blnAccounting = False
                End If
                If My.Settings.Auto_Approve_EFT = "Yes" Then
                    blnAccounting = True
                Else
                    blnAccounting = False
                End If
            End If
            If oAmount <> 0 Then
                Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oCreditTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                    sDescription, sAccountNumber, sLinkID, Full_Credit_Amount, oTaxType, "0", sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, Credit_Note_DRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)
            End If
        End If
        '----------------------------------------------------------------------------
        'Reset
        oLinkID = 0
        sDocType = ""
        Info = ""
        Info2 = ""
        Info3 = ""
        If Text.Contains("Supplier Debit") Then
            Info2 = "Supplier Debit"
            TransactionType = 1
        End If


        oProgressBar.Value = 20
        'STEP 2 
        'Single lince Incoice
        If Me.MultipleLines_CheckBox.Checked = False Then

            oAmount = CDec(Me.txtNonVATAmount.Text) 'Amount before VAT $-NEW
            oTaxType = CInt(Me.cmbVATType.SelectedValue.ToString)
            oTaxAmount = CDec(Me.txtVATAmount.Text)

            sDescription = Me.Cat1.Text '[Description] field
            sDescription2 = Me.Cat2.Text '[Description2] field
            sDescription3 = Me.Cat3.Text '[Description3] field

            blnPosted = False 'becomes true when posted to Pastel

            blnVoid = False


            If sDRCR = "Dr" Then
                sDRCR = "Cr"
            ElseIf sDRCR = "Cr" Then
                sDRCR = "Dr"
            End If

            sDescriptionCode = Me.lblMagicBox_Account_Segment4.Text
            sAccountNumber = Get_Account_Number_From_Segment4(sDescriptionCode)

            If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                Call oUndo("No Magic Box Segment 4 code!")
                Exit Sub
            End If
            'Set DocType to blank so not picked up in queries
            'Reset
            sDocType = ""
            Info = ""
            If Not Text.Contains("Supplier Debit") Then
                Info2 = "Invoice"
            End If
            Info3 = ""

            blnAccounting = True


            If Me.lblVATSplit.Visible = False Then
                'VAT is at correct 14%

                Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
          sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
          oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
          blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3) 'Supplier name blank here but 


            Else
                'So if the VAT Amount entered isn't 14% with a VAT Type of 1
                Dim oNew_VAT_Amount As Decimal
                Dim VAT_able_Portion As Decimal 'eg if invoice is 114 and VAT is changed from 14 to 10 then 10 * 114/14 = 81.43
                Dim Non_VAT_part_of_VAT_able_portion As Decimal
                Dim Non_VAT_able_Portion As Decimal

                'Add a second entry for non-VAT portion
                'InvoiceVATSPLIT
                TaxPerc = oTaxType
                TaxCalc = 1 + (TaxPerc / 100)
                oNew_VAT_Amount = CDec(Me.txtVATAmount.Text)
                VAT_able_Portion = Math.Round(CDec(Me.txtVATAmount.Text * TaxCalc), 2)
                Non_VAT_part_of_VAT_able_portion = VAT_able_Portion - oNew_VAT_Amount
                Non_VAT_able_Portion = CDec(Me.InvoiceTotal_TextBox.Text) - VAT_able_Portion

                If IDforLink <> Nothing Or OldTID <> Nothing Then
                    Info3 = "This invoice was recreated from Original Invoice : " & IDforLink & " of TransactionID: " & OldTID
                Else
                    Info3 = ""
                End If

                If Non_VAT_part_of_VAT_able_portion <> 0 Then
                    Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                    sDescription, sAccountNumber, sLinkID, Non_VAT_part_of_VAT_able_portion, oTaxType, oNew_VAT_Amount, sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)
                End If

                If Non_VAT_able_Portion <> 0 Then
                    Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                    sDescription, sAccountNumber, sLinkID, Non_VAT_able_Portion, "0", "0", sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)
                End If
                'Supplier name blank here but 

            End If
            'Reset after appending to DGV
            oLinkID = 0
            sDocType = ""
            Info = ""
            Info2 = ""
            Info3 = ""

            '-----------------    Credit Note   -----------------------------------------
            If Me.chkCredit.Checked = True Then
                Credit_Note_DRCR = "Cr"

                'Credit_Amount = Credit_ex_VAT
                'Credit_TaxAmount - calculated above


                If Credit_TaxAmount = 0 Then
                    Credit_Amount = CDec(Me.txtCreditAmount.Text)
                    Credit_TaxAmount = CDec(Me.txtCreditVAT.Text)
                Else
                    Credit_TaxAmount = CDec(Me.txtCreditVAT.Text)
                    Credit_Amount = CDec(Me.txtCreditExVAT.Text)
                End If

                blnAccounting = False 'Not in trial balance yet
                Info2 = "Credit Note" 'oNEW
                oLinkID = oTransactionID 'Link to Invoice Transaction ID

                Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oCreditTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                    sDescription, sAccountNumber, sLinkID, Credit_Amount, oTaxType, Credit_TaxAmount, sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, Credit_Note_DRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

            End If
            '----------------------------------------------------------------------------
            oProgressBar.Value = 50

            oLinkID = 0
            sDocType = ""
            Info = ""
            Info2 = ""
            Info3 = ""

            'STEP 3 Add VAT row

            If cmbVATType.SelectedValue.ToString <> 0 Then

                oAmount = CDec(Me.txtVATAmount.Text) 'Amount Field
                oTaxAmount = 0
                sDescription = sSupplierName
                sDescription2 = "VAT CONTROL"
                sDescription3 = "VAT CONTROL"

                TransactionType = 12
                oTaxType = 0

                'seg4
                'Second Trap
                sDescriptionCode = Get_Segment4_Using_Type_in_Accounting("VAT Control")
                If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                    Call oUndo("No Magic Box Segment 4 code!")
                    Exit Sub
                End If
                Dim sVAT_Account As String
                sVAT_Account = Get_Account_Number_From_Segment4(sDescriptionCode)
                sAccountNumber = sVAT_Account
                blnAccounting = True
                If Not Text.Contains("Supplier Debit") Then
                    Info2 = "Invoice"
                Else
                    Info2 = "Supplier Debit"
                    TransactionType = 1
                End If

                Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                    sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)
            End If


            sDocType = ""
            Info = ""
            Info2 = ""
            Info3 = ""

            '-----------------    Credit Note   -----------------------------------------
            If Me.chkCredit.Checked = True Then


                If Credit_TaxAmount <> 0 Then

                    Credit_Note_DRCR = "Cr"
                    ' Credit_TaxAmount - calculated above
                    Credit_Amount = Credit_TaxAmount

                    blnAccounting = False 'Not in trial balance yet
                    Info2 = "Credit Note"
                    oLinkID = oTransactionID

                    Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oCreditTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                        sDescription, sAccountNumber, sLinkID, Credit_Amount, oTaxType, "0", sUserID, _
                        oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                        blnAccounting, blnVoid, TransactionType, Credit_Note_DRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)
                End If

            End If
            '----------------------------------------------------------------------------

            oLinkID = 0
            sDocType = ""
            Info = ""
            Info2 = ""
            Info3 = ""
            oProgressBar.Value = 80

        Else

            '///////// -------  MULTIPLE LINES --------- ///////////////
            'Progess Bar is at 20 and must get to 80 by end of multiples

            Dim oProgressBarIncrements As Integer
            If Me.ListView1.Items.Count > 0 Then
                oProgressBarIncrements = Int(60 / Me.ListView1.Items.Count)
            Else
                oProgressBarIncrements = 0
            End If


            For k As Integer = 0 To Me.ListView1.Items.Count - 1

                oProgressBar.Value = oProgressBar.Value + oProgressBarIncrements

                'Gross Amount=0, Credit Amount=1, VAT Type=2, VATAmount=3, CredVAT=4 
                'exVAT=5, Cat1=6, Cat2=7, Cat3=8, GLAccount=9

                oAmount = Me.ListView1.Items(k).SubItems(5).Text '///// EX VAT AMOUNT *****
                oTaxType = Me.ListView1.Items(k).SubItems(2).Text
                oTaxAmount = Me.ListView1.Items(k).SubItems(3).Text
                sDescription = Me.ListView1.Items(k).SubItems(6).Text
                sDescription2 = Me.ListView1.Items(k).SubItems(7).Text
                sDescription3 = Me.ListView1.Items(k).SubItems(8).Text

                blnPosted = False 'becomes true when posted to Pastel

                blnVoid = False
                TransactionType = 12
                sDRCR = "Dr"

                'sorted
                sDescriptionCode = Me.ListView1.Items(k).SubItems(9).Text
                If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                    Call oUndo("No Magic Box Segment 4 code!")
                    Exit Sub
                End If
                sAccountNumber = Get_Account_Number_From_Segment4(sDescriptionCode)

                'Reset
                sDocType = ""
                Info = ""
                Info2 = "Invoice"
                Info3 = ""

                blnAccounting = True

                Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
                oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                 blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

                '-----------------    Credit Note   -----------------------------------------
                If Me.chkCredit.Checked = True Then
                    'Gross Amount=0, CreditAmount=1, VAT Type=2, VATAmount=3, CredVAT=4 
                    'exVAT=5, Cat1=6, Cat2=7, Cat3=8, GLAccount=9

                    If IsNumeric(Me.ListView1.Items(k).SubItems(4).Text) = True Then '// BLANKS IGNORED

                        Dim Credit_exVAT_Amount As Decimal = CDec(Me.ListView1.Items(k).SubItems(1).Text) - CDec(Me.ListView1.Items(k).SubItems(4).Text)

                        oTaxAmount = CDec(Me.ListView1.Items(k).SubItems(4).Text)
                        Credit_Note_DRCR = "Cr"

                        blnAccounting = False 'Not in trial balance yet
                        Info2 = "Credit Note"
                        oLinkID = oTransactionID

                        Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oCreditTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                            sDescription, sAccountNumber, sLinkID, Credit_exVAT_Amount, oTaxType, oTaxAmount, sUserID, _
                            oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                            blnAccounting, blnVoid, TransactionType, Credit_Note_DRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

                        oLinkID = 0
                        sDocType = ""
                        Info = ""
                        Info2 = ""
                        Info3 = ""

                    End If

                End If
                '----------------------------------------------------------------------------

                'STEP 3 Add VAT row
                'Gross Amount=0, CreditAmount=1, VAT Type=2, VATAmount=3, CredVAT=4 
                'exVAT=5, Cat1=6, Cat2=7, Cat3=8, GLAccount=9

                oAmount = CDec(Me.ListView1.Items(k).SubItems(3).Text) '//// VAT AMOUNT USED
                If oAmount <> 0 Then
                    oTaxType = 0
                    oTaxAmount = 0
                    sDescription = sSupplierName
                    sDescription2 = "VAT CONTROL"
                    sDescription3 = "VAT CONTROL"

                    TransactionType = 12
                    oTaxType = 0
                    sDRCR = "Dr"

                    'seg4
                    'Sorted
                    sDescriptionCode = Get_Segment4_Using_Type_in_Accounting("VAT Control")
                    If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                        Call oUndo("No Magic Box Segment 4 code!")
                    End If
                    'No need to flag if no account number - as not mandatory
                    Dim sVAT_Account As String
                    sVAT_Account = Get_Account_Number_From_Segment4(sDescriptionCode)
                    sAccountNumber = sVAT_Account


                    'Reset
                    sDocType = ""
                    Info = ""
                    Info2 = "Invoice"
                    Info3 = ""

                    blnAccounting = True

                    Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                     sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
                    oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                    blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

                    '-----------------    Credit Note   -----------------------------------------
                    If Me.chkCredit.Checked = True Then
                        If IsNumeric(Me.ListView1.Items(k).SubItems(4).Text) = True Then '// Blanks Ignored
                            'Gross Amount=0, CreditAmount=1, VAT Type=2, VATAmount=3, CredVAT=4 
                            'exVAT=5, Cat1=6, Cat2=7, Cat3=8, GLAccount=9
                            Credit_Note_DRCR = "Cr"
                            Dim Credit_VAT_Amount As Decimal = CDec(Me.ListView1.Items(k).SubItems(4).Text) 'Cr VAT

                            blnAccounting = False 'Not in trial balance yet
                            Info2 = "Credit Note"
                            oLinkID = oTransactionID

                            Call Add_Transaction_to_DGV("DGV_Transactions", oBatchID, oCreditTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
                                sDescription, sAccountNumber, sLinkID, Credit_VAT_Amount, oTaxType, "0", sUserID, _
                                oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
                                blnAccounting, blnVoid, TransactionType, Credit_Note_DRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

                            oLinkID = 0
                            sDocType = ""
                            Info = ""
                            Info2 = ""
                            Info3 = ""

                        End If
                    End If
                    '----------------------------------------------------------------------------
                End If
            Next k
        End If


        '========================================================================================================================================================
        'PAYMENT MADE -------------------------------------------------------------------------------------------------------------------------------------------
        '========================================================================================================================================================

        'NOW IF THE PAYMENT IS MADE IMMEDIATELY DO TWO MORE ENTRIES
        'Continue code if "PAID" or "PAID LOCK"

        If Me.ComboBox_Status.Text = "UNPAID" Then
            GoTo Jump
        End If


        '-----------------------------------------------------------------------------------------------
        Dim blnPaymentRequested As Boolean = False
        If My.Settings.User_Control_On = "zzz" Then
            'If User Control is on then check if this user can make the payment. If not then just request
            'Add eg "Invoice Payment Requested|500|EFT" to Info (cell 31) in first line of DGV_Transactions
            'before Append to Database

            If User_Authorized_to_Make_Payment_Type(Me.cmbPaymentType.Text) = False Then

                'If Payment not autorized then request the payment by changing the Info Cell value in DGV_Transactions 
                Dim rPaymentAmount As String = Me.AmountPaid_TextBox.Text
                Dim rPaymentType As String = Me.cmbPaymentType.Text
                Dim oRequest As String = "Invoice Payment Requested|" & rPaymentAmount & "|" & rPaymentType
                Me.DGV_Transactions.Rows(0).Cells(31).Value = oRequest
                blnPaymentRequested = True
                'and abort rest of code
                GoTo Jump

            End If
            '-----------------------------------------------------------------------------------------------
        End If

        'NOW RUN USUAL PAYMENT CODE
        If Not Text.Contains("Supplier Debit") Then

            oLinkID = oTransactionID 'Need the linkID to equal the TransactionID of the priginal invoice
            oTransactionID = oPaid_TransacionID
            sTransactionDate = Me.DatePaid_DateTimePicker.Value.Date.ToString("dd MMMM yyyy")

            'Only set Accounting to true if cash is paid
            If Me.cmbPaymentType.Text = "EFT" Then
                blnAccounting = False 'Not in trial balance yet
                oDGV = "DGV_Transactions"
                'Info = "EFT"

            ElseIf Me.cmbPaymentType.Text = "CHEQUE" Then
                oDGV = "DGV_Transactions"
                blnAccounting = False 'Not in trial balance yet
                ''Info = "CHEQUE"
            Else
                oDGV = "DGV_Transactions"
                blnAccounting = True
            End If

            Info = Me.cmbPaymentType.Text

            sDescription = sSupplierName
            TransactionType = 1
            oTaxType = 0 '[Tax Type] field
            oTaxAmount = 0

            If Text.Contains("Invoice Capture") Then
                oAmount = CDec(Me.AmountPaid_TextBox.Text)
            End If

            If Text.Contains("Supplier Debit") Then
                oAmount = CDec(Me.InvoiceTotal_TextBox.Text)
            End If

            sDRCR = "Dr"

            sDescription2 = "CREDITORS CONTROL" '[Description2] field
            sDescription3 = "CREDITORS CONTROL" '[Description3] field

            'seg4
            'Third Trap
            sCreditorsControl_Segment4 = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
            sDescriptionCode = sCreditorsControl_Segment4
            If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                Call oUndo("No Magic Box Segment 4 code!")
                Exit Sub
            End If
            sAccountNumber = Get_Account_Number_From_Segment4(sDescriptionCode)

            sDocType = "Payment"
            Info2 = "Payment"

            Call Add_Transaction_to_DGV(oDGV, oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
            sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
            oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
            blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

            If blnAppend_Failed = True Then
                Me.Label_Success.Text = "ENTRY FAILED!"
            End If

            sDocType = ""
            Info = ""
            Info2 = ""
            Info3 = ""

            'STEP 5 - Add record with [Description] = Payment Type combo, [Transaction Type] = "Payment", [Reference] = "Invoice M", [Tax Type] = 0

            sDescription = Get_Segm1Desc_From_DisplayName_in_Accounting(Me.cmbPaymentType.Text)
            sDescription2 = Get_Segm2Desc_From_DisplayName_in_Accounting(Me.cmbPaymentType.Text)
            sDescription3 = Get_Segm3Desc_From_DisplayName_in_Accounting(Me.cmbPaymentType.Text)

            'seg4
            sDescriptionCode = Get_Segment4_from_DisplayName_in_Accounting(Me.cmbPaymentType.Text)
            sAccountNumber = Get_GLACCOUNT_from_DisplayName_in_Accounting(Me.cmbPaymentType.Text)

            TransactionType = 1
            oTaxType = 0
            oTaxAmount = 0

            If Text.Contains("Invoice Capture") Then
                oAmount = CDec(Me.AmountPaid_TextBox.Text)
            End If
            If Text.Contains("Supplier Debit") Then
                oAmount = CDec(Me.InvoiceTotal_TextBox.Text)
            End If
            sDRCR = "Cr"
            'Set the DocType to blank again to avoid being picked up in the query
            sDocType = ""
            Info = ""
            Info2 = "Payment"

            Call Add_Transaction_to_DGV(oDGV, oBatchID, oTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, sInvoiceNumber, _
            sDescription, sAccountNumber, sLinkID, oAmount, oTaxType, oTaxAmount, sUserID, _
            oSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
            blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, sDocType, sSupplierName, Info, Info2, Info3)

            sDocType = ""
            Info = ""
            Info2 = ""
            Info3 = ""

        End If

Jump:

        If blnCRITICAL_ERROR = False Then 'make sure that there where no errors in the Catch of each Sub

            If blnCRITICAL_ERROR = False Then
                Me.Label_Success.Text = "Successful Entry!"
                If My.Settings.User_Control_On = "zz" And blnPaymentRequested = True Then
                    Me.Label_Success.Text = "Success! pay request."
                End If
                Me.Label_Success.ForeColor = Color.DarkGreen
                Call ClearBoxes()
            Else
                Me.Label_Success.Text = "ENTRY FAILED!"
                Me.Label_Success.ForeColor = Color.Red
            End If

        End If

    End Sub


    Private Function GetLastDayInMonth(ByVal dDate As Date) As Date
        dDate = DateAdd(DateInterval.Month, 1, dDate)
        dDate = Convert.ToDateTime(Month(dDate).ToString() & "/" & "1/" & Year(dDate).ToString())
        dDate = DateAdd(DateInterval.Day, -1, dDate)
        Return dDate
    End Function

    Sub ColorCode()
        Dim o30Days As Date = System.DateTime.Now.AddYears(-1)
        Dim oInvoiceDate As String
        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            oInvoiceDate = Me.DataGridView1.Rows(i).Cells(3).Value
            'o30Days = oInvoiceDate.
        Next
    End Sub

    Private Sub cmbVATType_LostFocus(sender As Object, e As System.EventArgs) Handles cmbVATType.LostFocus
        Call oCalc_Numbers()
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbVATType.SelectedIndexChanged
        Call oCalc_Numbers()
    End Sub

    Sub oCalc_Numbers()

        On Error Resume Next

        Dim oGrossAmount As Decimal
        Dim oVatType As Integer
        Dim oVatAmount As Decimal

        TaxPerc = cmbVATType.SelectedValue.ToString
        If Me.InvoiceTotal_TextBox.Text = "" Then
            'do nothing
        Else
            oGrossAmount = CDec(Me.InvoiceTotal_TextBox.Text)
            oVatType = CInt(TaxPerc)
            If Me.cmbVATType.Text = "" Then
                'do nothing
            ElseIf oVatType = 0 Then
                Me.txtVATAmount.Text = Format(0, "###,##0.00")
                Me.txtNonVATAmount.Text = Format(oGrossAmount, "###,##0.00")
            Else
                TaxCalc = 1 + (TaxPerc / 100)
                oVatAmount = Format(oGrossAmount - (oGrossAmount / TaxCalc), "###,##0.00")
                Me.txtVATAmount.Text = oVatAmount
                Me.txtNonVATAmount.Text = Format(oGrossAmount - oVatAmount, "###,##0.00")
            End If
        End If
    End Sub


    Private Sub VATType_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles VATType_ComboBox.SelectedIndexChanged
        Call Calc_Numbers()
        If Me.chkCredit.Checked = True And Me.MultipleLines_CheckBox.Checked = True Then
            Calc_Credit_Numbers()
        End If

    End Sub

    Sub Calc_Numbers()

        Try

            Dim oGrossAmount As Decimal
            Dim oVatType As Integer
            Dim oVatAmount As Decimal


            If Me.txtGrossAmount.Text = "" Then
                'do nothing
            Else
                TaxPerc = VATType_ComboBox.SelectedValue.ToString
                oGrossAmount = CDec(Me.txtGrossAmount.Text)
                oVatType = CInt(TaxPerc)
                If oVatType = 0 Then
                    Me.VATAmount_TextBox.Text = Format(0, "###,##0.00")
                    Me.NonVAT_Amount_TextBox.Text = Format(oGrossAmount, "###,##0.00")
                Else
                    'oVatAmount = Format(Round(oGrossAmount * 14 / 114, 2), "###,##0.00")
                    TaxCalc = 1 + (TaxPerc / 100)
                    oVatAmount = Format(oGrossAmount - (oGrossAmount / TaxCalc), "###,##0.00")
                    VATAmount_TextBox.Text = oVatAmount
                    NonVAT_Amount_TextBox.Text = Format(oGrossAmount - oVatAmount, "###,##0.00")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            txtGrossAmount.Text = 0

        End Try

    End Sub

    Sub Calc_Credit_Numbers()

        On Error Resume Next

        Dim oCreditAmount As Decimal
        Dim oVatType As Integer
        Dim oCreditVatAmount As Decimal

        TaxPerc = cmbVATType.Tag.ToString
        If Me.txtCredit.Text = "" Then
            'do nothing
        Else
            oCreditAmount = CDec(Me.txtCredit.Text)
            oVatType = CInt(TaxPerc)
            If Me.VATType_ComboBox.Text = "" Then
                'do nothing
            ElseIf oVatType = 0 Then
                Me.txtCrVAT.Text = Format(0, "###,##0.00")
                'Me.NonVAT_Amount_TextBox.Text = Format(oGrossAmount, "###,##0.00")
            Else
                TaxCalc = 1 + (TaxPerc / 100)
                oCreditVatAmount = Format((oCreditAmount / TaxCalc), "###,##0.00")
                Me.txtCrVAT.Text = oCreditVatAmount
                'Me.NonVAT_Amount_TextBox.Text = Format(oGrossAmount - oVatAmount, "###,##0.00")
            End If
        End If
    End Sub

    Private Sub DataGridView1_Click(sender As Object, e As System.EventArgs) Handles DataGridView1.Click

        Dim oRowIndex As Integer
        For Each cell As DataGridViewCell In DataGridView1.SelectedCells
            If cell.ColumnIndex < 3 Then 'don't interfere with last three columns as they have to be edited
                oRowIndex = cell.RowIndex
                DataGridView1.Rows(oRowIndex).Selected = True
            End If
        Next

    End Sub



    Sub Get_Invoice_Data_using_ID(ByVal oID As Integer)

        Dim oBatchID As Integer
        Dim oTransactionID As Integer
        Dim oLinkID As Integer
        Dim dTransactionDate As Date
        Dim dCaptureDate As Date
        Dim sPPeriod As String
        Dim sGDC As String
        Dim sInvoiceNumber As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkID As String
        Dim oAmount As Decimal
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim sSupplierName As Integer
        Dim oEmployeeID As Integer
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String

        Try

            Dim sSQL As String
            sSQL = "SELECT * FROM Transactions WHERE ID = " & oID

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader


                While datareader.Read
                    If Not datareader("ID").Equals(DBNull.Value) Then
                        oID = datareader("ID")
                    Else
                        MsgBox("The invoice at the line you clicked does not exist")
                        connection.Close()
                        Exit Sub
                    End If
                    If Not datareader("BatchID").Equals(DBNull.Value) Then
                        oBatchID = datareader("BatchID")
                    Else

                    End If
                    If Not datareader("TransactionID").Equals(DBNull.Value) Then
                        oTransactionID = datareader("TransactionID")
                    Else

                    End If
                    If Not datareader("LinkID").Equals(DBNull.Value) Then
                        oLinkID = datareader("LinkID")
                    Else

                    End If
                    If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                        dTransactionDate = datareader("Transaction Date")
                    Else

                    End If
                    If Not datareader("Capture Date").Equals(DBNull.Value) Then
                        dCaptureDate = datareader("Capture Date")
                    Else

                    End If
                    If Not datareader("PPeriod").Equals(DBNull.Value) Then
                        sPPeriod = datareader("PPeriod")
                    Else

                    End If
                    If Not datareader("GDC").Equals(DBNull.Value) Then
                        sGDC = datareader("GDC")
                    Else

                    End If
                    If Not datareader("Reference").Equals(DBNull.Value) Then
                        sInvoiceNumber = datareader("Reference")
                    Else

                    End If
                    If Not datareader("Description").Equals(DBNull.Value) Then
                        sDescription = datareader("Description")
                    Else

                    End If
                    If Not datareader("AccNumber").Equals(DBNull.Value) Then
                        sAccountNumber = datareader("AccNumber")
                    Else

                    End If
                    If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                        sLinkID = datareader("LinkAcc")
                    Else

                    End If
                    If Not datareader("Amount").Equals(DBNull.Value) Then
                        oAmount = datareader("Amount")
                    Else


                    End If
                    If Not datareader("TaxType").Equals(DBNull.Value) Then
                        oTaxType = datareader("TaxType")
                    Else


                    End If
                    If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                        oTaxAmount = datareader("Tax Amount")
                    Else

                    End If
                    If Not datareader("UserID").Equals(DBNull.Value) Then
                        sUserID = datareader("UserID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        sSupplierName = datareader("SupplierID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                        oEmployeeID = datareader("EmployeeID")
                    Else

                    End If
                    If Not datareader("Description 2").Equals(DBNull.Value) Then
                        sDescription2 = datareader("Description 2")
                    Else

                    End If
                    If Not datareader("Description 3").Equals(DBNull.Value) Then
                        sDescription3 = datareader("Description 3")
                    Else

                    End If
                    If Not datareader("Description 4").Equals(DBNull.Value) Then
                        sDescription4 = datareader("Description 4")
                    Else

                    End If
                    If Not datareader("Description 5").Equals(DBNull.Value) Then
                        sDescription5 = datareader("Description 5")
                    Else

                    End If
                    If Not datareader("Posted").Equals(DBNull.Value) Then
                        blnPosted = datareader("Posted")
                    Else

                    End If
                    If Not datareader("Accounting").Equals(DBNull.Value) Then
                        blnAccounting = datareader("Accounting")
                    Else

                    End If
                    If Not datareader("Void").Equals(DBNull.Value) Then
                        blnVoid = datareader("Void")
                    Else

                    End If
                    If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                        sTransactionType = datareader("Transaction Type").ToString
                    Else

                    End If
                    If Not datareader("DR_CR").Equals(DBNull.Value) Then
                        oDR_CR = datareader("DR_CR").ToString
                    Else

                    End If

                End While
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader


                While datareader.Read
                    If Not datareader("ID").Equals(DBNull.Value) Then
                        oID = datareader("ID")
                    Else
                        MsgBox("The invoice at the line you clicked does not exist")
                        connection.Close()
                        Exit Sub
                    End If
                    If Not datareader("BatchID").Equals(DBNull.Value) Then
                        oBatchID = datareader("BatchID")
                    Else

                    End If
                    If Not datareader("TransactionID").Equals(DBNull.Value) Then
                        oTransactionID = datareader("TransactionID")
                    Else

                    End If
                    If Not datareader("LinkID").Equals(DBNull.Value) Then
                        oLinkID = datareader("LinkID")
                    Else

                    End If
                    If Not datareader("Transaction Date").Equals(DBNull.Value) Then
                        dTransactionDate = datareader("Transaction Date")
                    Else

                    End If
                    If Not datareader("Capture Date").Equals(DBNull.Value) Then
                        dCaptureDate = datareader("Capture Date")
                    Else

                    End If
                    If Not datareader("PPeriod").Equals(DBNull.Value) Then
                        sPPeriod = datareader("PPeriod")
                    Else

                    End If
                    If Not datareader("GDC").Equals(DBNull.Value) Then
                        sGDC = datareader("GDC")
                    Else

                    End If
                    If Not datareader("Reference").Equals(DBNull.Value) Then
                        sInvoiceNumber = datareader("Reference")
                    Else

                    End If
                    If Not datareader("Description").Equals(DBNull.Value) Then
                        sDescription = datareader("Description")
                    Else

                    End If
                    If Not datareader("AccNumber").Equals(DBNull.Value) Then
                        sAccountNumber = datareader("AccNumber")
                    Else

                    End If
                    If Not datareader("LinkAcc").Equals(DBNull.Value) Then
                        sLinkID = datareader("LinkAcc")
                    Else

                    End If
                    If Not datareader("Amount").Equals(DBNull.Value) Then
                        oAmount = datareader("Amount")
                    Else


                    End If
                    If Not datareader("TaxType").Equals(DBNull.Value) Then
                        oTaxType = datareader("TaxType")
                    Else


                    End If
                    If Not datareader("Tax Amount").Equals(DBNull.Value) Then
                        oTaxAmount = datareader("Tax Amount")
                    Else

                    End If
                    If Not datareader("UserID").Equals(DBNull.Value) Then
                        sUserID = datareader("UserID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("SupplierID").Equals(DBNull.Value) Then
                        sSupplierName = datareader("SupplierID")
                    Else

                    End If
                    '------------------------------------------
                    If Not datareader("EmployeeID").Equals(DBNull.Value) Then
                        oEmployeeID = datareader("EmployeeID")
                    Else

                    End If
                    If Not datareader("Description 2").Equals(DBNull.Value) Then
                        sDescription2 = datareader("Description 2")
                    Else

                    End If
                    If Not datareader("Description 3").Equals(DBNull.Value) Then
                        sDescription3 = datareader("Description 3")
                    Else

                    End If
                    If Not datareader("Description 4").Equals(DBNull.Value) Then
                        sDescription4 = datareader("Description 4")
                    Else

                    End If
                    If Not datareader("Description 5").Equals(DBNull.Value) Then
                        sDescription5 = datareader("Description 5")
                    Else

                    End If
                    If Not datareader("Posted").Equals(DBNull.Value) Then
                        blnPosted = datareader("Posted")
                    Else

                    End If
                    If Not datareader("Accounting").Equals(DBNull.Value) Then
                        blnAccounting = datareader("Accounting")
                    Else

                    End If
                    If Not datareader("Void").Equals(DBNull.Value) Then
                        blnVoid = datareader("Void")
                    Else

                    End If
                    If Not datareader("Transaction Type").Equals(DBNull.Value) Then
                        sTransactionType = datareader("Transaction Type").ToString
                    Else

                    End If
                    If Not datareader("DR_CR").Equals(DBNull.Value) Then
                        oDR_CR = datareader("DR_CR").ToString
                    Else

                    End If

                End While
                connection.Close()
            End If


            'oBatchID
            'oTransactionID = 9999 'just unique
            'oLinkID = 9999
            Me.InvoiceDate_DateTimePicker.Value = dTransactionDate
            Me.CaptureDate_DateTimePicker.Value = dCaptureDate
            'sPPeriod 
            'sGDC = "G" 'General/Debtots/Creditors
            Me.txtInvoiceNumber.Text = sInvoiceNumber
            Me.CategoryType1_ComboBox.Text = sDescription
            'sAccountNumber = "in pastel setup"
            'sLinkAcc = "in pastel setup"
            Me.txtGrossAmount.Text = oAmount
            Me.VATType_ComboBox.Text = oTaxType
            Me.VATAmount_TextBox.Text = oTaxAmount
            'sUserID = "" 'for later - they should log on to use this
            cmbSupplier.Text = sSupplierName  '***********************^&&^^&^%$%^&&
            'oEmployeeID = "" 'not required
            sDescription2 = Me.CategoryType1_ComboBox.Text '[Description2] field
            sDescription3 = Me.CategoryType2_ComboBox.Text '[Description3] field
            'sDescription4 = ""
            'sDescription5 = ""
            'blnPosted = False 'becomes true when posted to Pastel
            'blnAccounting = True
            'blnVoid = False
            'oTransactionType = "Invoice"
            'oDR_CR = 1


            'Exit Sub

        Catch ex As Exception
            MsgBox(ex.Message & " 027")
        End Try
    End Sub



    Sub Void_Invoice(ByVal oBatchID As Integer)
        Try
            If oBatchID = 0 Then
                MsgBox("There was an issue with the LinkID not being valid, please contact support", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            Dim sSQL As String
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True WHERE BatchID = " & oBatchID & ""
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET Void = 1 WHERE BatchID = " & oBatchID & ""
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 028")
        End Try
    End Sub




    Private Sub Delete_Button_Click(sender As System.Object, e As System.EventArgs) Handles Delete_Invoice_Button.Click


        If Me.DataGridView1.SelectedCells.Count < 1 Then
            MsgBox("Please select a row containing an invoice!")
            Exit Sub
        End If


        If Me.DataGridView1.SelectedRows.Count < 1 Then
            Me.DataGridView1.Rows(Me.DataGridView1.SelectedCells(0).RowIndex).Selected = True
        End If

        Dim oRowIndex As Integer
        oRowIndex = Me.DataGridView1.SelectedRows(0).Index

        Dim oAmount As Decimal
        oAmount = Me.DataGridView1.SelectedRows(0).Cells(3).Value
        Dim oDue As Decimal
        oDue = Me.DataGridView1.SelectedRows(0).Cells(4).Value
        If oDue <> oAmount Then
            If Me.Label_Level.Text <> "Super" Then
                MsgBox("Payments have been made on this invoice. Please activate the required level to delete this with your username and password!")
                Exit Sub
            End If
        End If

        Dim oTransactionDate As Date = CDate(Me.DataGridView1.SelectedRows(0).Cells(5).Value)
        If oTransactionDate < Now.Date Then
            If Me.Label_Level.Text <> "Super" Then
                MsgBox("You need to activate the required user level to delete an invoice that wasn't entered into the system today!")
                Exit Sub
            End If
        End If


        'Now gather supplier ID and invoice number and see if any payments made on that invoice
        Dim sSupplierName As String = Me.DataGridView1.SelectedRows(0).Cells(1).Value
        Dim oSupplierID As Integer
        oSupplierID = Database_Lookup_String_return_Integer("Suppliers", "SupplierID", "SupplierName", sSupplierName, "Number") 'Supplier Name

        Dim oTransactionID As Integer = Me.DataGridView1.SelectedRows(0).Cells(0).Value   'GetTransactionID_FromID(oID)

        'Now delete invoice lines which will have said TransactionID as well as related payments which will have this in their LinkID column
        Call DeleteInvoice(oTransactionID)
        Call DeleteInvoicePayments(oTransactionID)
        'Reload the DataGridView with table data
        Call GetInvoiceDetailPerSupplier2()
        Call Load_Invoices_for_Supplier_to_ComboBox()
        Me.DataGridView1.Refresh()
        Me.DataGridView1.DataSource = Nothing
        System.Threading.Thread.Sleep(2000)
        Call oFind()
    End Sub

    Sub DeleteInvoice(ByVal TransactionID As String)


        Dim sSQL As String

        Try
            If TransactionID = "" Then
                MsgBox("There was an issue with the transaction ID, please contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            If TransactionID = "0" Then
                MsgBox("There was an issue with the transaction ID, please contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True WHERE TransactionID = " & TransactionID
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET Void = 1 WHERE TransactionID = " & TransactionID
            End If
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            End If


        Catch ex As Exception
            MsgBox("There was a problem deleting an invoice " & Err.Description)
        End Try


    End Sub
    Sub DeleteInvoicePayments(ByVal LinkID As String)
        If LinkID = "0" Then
            MsgBox("Voiding of this payment has been cancelled due to the LinkID not being valid. Please take note of what you have been doing and contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
            Exit Sub
        End If
        If LinkID = "" Then
            MsgBox("Voiding of this payment has been cancelled due to the LinkID not being valid. Please take note of what you have been doing and contact support.", MsgBoxStyle.Critical, "CALL SUPPORT")
            Exit Sub
        End If
        Dim sSQL As String

        Try

            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True WHERE LinkID = " & LinkID
            Else
                sSQL = "UPDATE Transactions SET Void = 1 WHERE LinkID = " & LinkID
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If


            If My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            End If

        Catch ex As Exception
            MsgBox("There was a problem deleting payments relating to an invoice " & ex.Message & " 029")
        End Try


    End Sub

    Sub ClearBoxes()
        'Me.cmbSupplier.SelectedIndex = -1
        Me.txtInvoiceNumber.Text = ""
        Me.txtGrossAmount.Text = ""
        Me.VATType_ComboBox.Text = ""
        'Me.NonVAT_Amount_TextBox.Text = ""
        Me.CategoryType1_ComboBox.Text = ""
        Me.CategoryType2_ComboBox.Text = ""
        Me.CategoryType3_ComboBox.Text = ""
        Me.Cat1.Text = ""
        Me.Cat2.Text = ""
        Me.Cat3.Text = ""
        Me.AmountPaid_TextBox.Text = ""
        Me.Notes_RichTextBox.Text = ""
        Me.VATAmount_TextBox.Text = ""
        Me.InvoiceTotal_TextBox.Text = ""
        Me.ListView1.Items.Clear()
        Me.NonVAT_Amount_TextBox.Text = ""
        Me.lblCredit.Visible = False
        Me.txtCredit.Visible = False
        Me.lblCrVAT.Visible = False
        Me.txtCrVAT.Visible = False
        Me.ListView1.Columns(1).Width = 0
        Me.ListView1.Columns(4).Width = 0
        Me.txtCredit.Text = ""
        Me.txtCrVAT.Text = ""
        Me.txtVATAmount.Text = ""
        Me.cmbVATType.Text = ""
        Me.txtNonVATAmount.Text = ""
        ''Me.InvoiceDate_DateTimePicker.Value = Date.Now
    End Sub
    Sub Fill_Category1_Combobox()
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Me.CategoryType1_ComboBox.SelectedIndex = 0
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            Me.CategoryType1_ComboBox.Items.Clear()
            Me.Cat1.Items.Clear()
            While datareader.Read
                If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                    Me.CategoryType1_ComboBox.Items.Add(datareader("Segment 1 Desc"))
                    Me.Cat1.Items.Add(datareader("Segment 1 Desc"))
                End If
            End While
            connection.Close()
        Catch ex As Exception
            MsgBox(ex.Message & " 030")
        End Try

    End Sub


    Private Sub CategoryType1_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CategoryType1_ComboBox.SelectedIndexChanged

        If Me.CategoryType1_ComboBox.Text = "" Then Exit Sub
        'First clear other two boxes
        Me.CategoryType2_ComboBox.Items.Clear()
        Me.CategoryType2_ComboBox.Text = ""
        Me.CategoryType3_ComboBox.Items.Clear()
        Me.CategoryType3_ComboBox.Text = ""
        'Then fill next box
        Call Fill_Category2_Combobox(Me.CategoryType1_ComboBox.Text)

    End Sub

    Private Sub Cat1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Cat1.SelectedIndexChanged
        If Me.Cat1.Text = "" Then Exit Sub
        'First clear other two boxes
        Me.Cat2.Items.Clear()
        Me.Cat2.Text = ""
        Me.Cat3.Items.Clear()
        Me.Cat3.Text = ""
        'Then fill next box
        Call Fill_Cat2(Me.Cat1.Text)
    End Sub

    Sub Fill_Category2_Combobox(Category1 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Me.CategoryType2_ComboBox.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.CategoryType2_ComboBox.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'dTable.DefaultView.RowFilter = "SupplierID=12"  'or you can concatenate a variable instead of hardcoding ID

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 1 Desc]='" & Category1 & "'"   'THIS IS ALREADY COMPANY SPECIFIC

                    'xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 1 Desc]='PURCHASES'"
                    With Me.CategoryType2_ComboBox.Items
                        .Clear()
                        For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                            If AlreadInCombo(Me.CategoryType2_ComboBox, xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 2 Desc")) = False Then
                                .Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 2 Desc"))
                            End If
                        Next
                    End With

                Else

                    'the following technique is supposed to speed up the data reader compared to the other boxes
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    With Me.CategoryType2_ComboBox.Items
                        .Clear()
                        While datareader.Read
                            .Add(datareader("Segment 2 Desc"))
                        End While
                    End With
                    connection.Close()
                End If
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 031")
        End Try

    End Sub

    Sub Fill_Cat2(Cat1 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Cat1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Me.Cat2.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.Cat2.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'dTable.DefaultView.RowFilter = "SupplierID=12"  'or you can concatenate a variable instead of hardcoding ID

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 1 Desc]='" & Cat1 & "'"


                    'xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 1 Desc]='PURCHASES'"
                    With Me.Cat2.Items
                        .Clear()
                        For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                            If AlreadInCombo(Cat2, xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 2 Desc")) = False Then
                                .Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 2 Desc"))
                            End If
                        Next
                    End With

                Else

                    'the following technique is supposed to speed up the data reader compared to the other boxes
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    With Me.Cat2.Items
                        .Clear()
                        While datareader.Read
                            .Add(datareader("Segment 2 Desc"))
                        End While
                    End With
                    connection.Close()
                End If
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 032")
        End Try

    End Sub

    Private Sub CategoryType2_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CategoryType2_ComboBox.SelectedIndexChanged

        If Me.CategoryType2_ComboBox.Text = "" Then Exit Sub
        Me.CategoryType3_ComboBox.Text = ""
        Call Fill_Category3_Combobox(Me.CategoryType2_ComboBox.Text)

    End Sub
    Private Sub Cat2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Cat2.SelectedIndexChanged
        If Me.Cat2.Text = "" Then Exit Sub
        Me.Cat3.Text = ""
        Call Fill_Cat3(Me.Cat2.Text)
    End Sub

    Sub Fill_Category3_Combobox(Category2 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryType1_ComboBox.Text) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.CategoryType3_ComboBox.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.CategoryType3_ComboBox.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 2 Desc]='" & Category2 & "'"


                    With Me.CategoryType3_ComboBox.Items
                        .Clear()
                        For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                            If AlreadInCombo(Me.CategoryType3_ComboBox, xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 3 Desc")) = False Then
                                .Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 3 Desc"))
                            End If
                        Next
                    End With

                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    Me.CategoryType3_ComboBox.Items.Clear()
                    While datareader.Read
                        If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                            Me.CategoryType3_ComboBox.Items.Add(datareader("Segment 3 Desc"))
                        End If
                    End While
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 033")
        End Try

    End Sub

    Sub Fill_Cat3(Cat2 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Cat2) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.Cat1.Text) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.Cat3.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.Cat3.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 2 Desc]='" & Cat2 & "'"

                    With Me.Cat3.Items
                        .Clear()
                        For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                            If AlreadInCombo(Cat3, xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 3 Desc")) = False Then
                                .Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 3 Desc"))
                            End If
                        Next
                    End With

                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    Me.Cat3.Items.Clear()
                    While datareader.Read
                        If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                            Me.Cat3.Items.Add(datareader("Segment 3 Desc"))
                        End If
                    End While
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 034")
        End Try

    End Sub

    Private Sub CategoryType3_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CategoryType3_ComboBox.SelectedIndexChanged
        If Me.CategoryType3_ComboBox.Text = "" Then Exit Sub
        Me.lblMagicBox_Account_Segment4.Text = Get_Segment4_MB_Account_Multiples(Me.CategoryType3_ComboBox.Text)
        If Me.lblMagicBox_Account_Segment4.Text = "## NO ACCOUNT ##" Then
            MsgBox("There is no account for this category!")
        End If
    End Sub


    Private Sub Cat3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Cat3.SelectedIndexChanged
        If Me.Cat3.Text = "" Then Exit Sub
        Me.lblMagicBox_Account_Segment4.Text = Get_Segment4_MB_Account(Me.Cat3.Text)
        If Me.lblMagicBox_Account_Segment4.Text = "## NO ACCOUNT ##" Then
            MsgBox("There is no account for this category!")
        End If
    End Sub

    Function Get_GL_Account(Category3 As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(Me.CategoryType2_ComboBox.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryType1_ComboBox.Text) & "'"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("GL ACCOUNT").Equals(DBNull.Value) Then
                        Get_GL_Account = datareader("GL ACCOUNT")
                    Else
                        Get_GL_Account = "## NO ACCOUNT ##"
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    Dim oResult As String
                    Dim oCriteria As String

                    oCriteria = "[Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                                    Me.CategoryType2_ComboBox.Text & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryType1_ComboBox.Text) & "'"

                    xSQLTable_Accounts.DefaultView.RowFilter = oCriteria
                    If xSQLTable_Accounts.DefaultView.Count = 0 Then
                        Get_GL_Account = "## NO ACCOUNT ##"
                    Else
                        oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("GL ACCOUNT").ToString
                        If oResult = "" Then
                            Get_GL_Account = "## NO ACCOUNT ##"
                        Else
                            Get_GL_Account = oResult
                        End If
                    End If
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    While datareader.Read
                        If Not datareader("GL ACCOUNT").Equals(DBNull.Value) Then
                            Get_GL_Account = datareader("GL ACCOUNT")
                        Else
                            Get_GL_Account = "## NO ACCOUNT ##"
                        End If
                    End While
                    connection.Close()
                End If
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 035")
        End Try


    End Function


    Function Get_Segment4_MB_Account(Category3 As String) As String 'Used for top new ComboBoxes
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(Me.Cat2.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.Cat1.Text) & "' and Co_ID = " & CInt(My.Settings.Setting_CompanyID)

            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4_MB_Account = datareader("Segment 4")
                    Else
                        Get_Segment4_MB_Account = "## NO ACCOUNT ##"
                    End If
                End While
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 035a")
        End Try


    End Function

    Function Get_Segment4_MB_Account_Multiples(Category3 As String) As String 'Used for top new ComboBoxes
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(Me.CategoryType2_ComboBox.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryType1_ComboBox.Text) & "'"


            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4_MB_Account_Multiples = datareader("Segment 4")
                    Else
                        Get_Segment4_MB_Account_Multiples = "## NO ACCOUNT ##"
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    Dim oResult As String
                    Dim oCriteria As String
                    oCriteria = "[Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                        Me.CategoryType2_ComboBox.Text & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CategoryType1_ComboBox.Text) & "'"
                    xSQLTable_Accounts.DefaultView.RowFilter = oCriteria
                    If xSQLTable_Accounts.DefaultView.Count = 0 Then
                        Get_Segment4_MB_Account_Multiples = "## NO ACCOUNT ##"
                    Else
                        oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                        If oResult = "" Then
                            Get_Segment4_MB_Account_Multiples = "## NO ACCOUNT ##"
                        Else
                            Get_Segment4_MB_Account_Multiples = oResult
                        End If
                    End If
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    While datareader.Read
                        If Not datareader("Segment 4").Equals(DBNull.Value) Then
                            Get_Segment4_MB_Account_Multiples = datareader("Segment 4")
                        Else
                            Get_Segment4_MB_Account_Multiples = "## NO ACCOUNT ##"
                        End If
                    End While
                    connection.Close()
                End If
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 035a")
        End Try


    End Function

    Function Get_Segment4(ByVal Category1 As String, ByVal Category2 As String, ByVal Category3 As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4 = datareader("Segment 4")
                    Else
                        Get_Segment4 = ""
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    Dim oCriteria As String
                    oCriteria = "[Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                        SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
                    xSQLTable_Accounts.DefaultView.RowFilter = oCriteria
                    If xSQLTable_Accounts.DefaultView.Count > 0 Then
                        oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                        Get_Segment4 = oResult
                    Else
                        Get_Segment4 = ""
                    End If

                Else

                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    While datareader.Read
                        If Not datareader("Segment 4").Equals(DBNull.Value) Then
                            Get_Segment4 = datareader("Segment 4")
                        Else
                            Get_Segment4 = ""
                        End If
                    End While
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("Error: " & ex.Message & " 036")
        End Try


    End Function

    Private Sub Supplier_ComboBox_DropDown(ByVal sender As Object, ByVal e As System.EventArgs)
        ''MsgBox("Hello")
    End Sub

    Private Sub Supplier_ComboBox_Enter(ByVal sender As Object, ByVal e As System.EventArgs)
        blnDefaultsLoaded = False
    End Sub
    Private Sub Supplier_ComboBox_Leave(ByVal sender As Object, ByVal e As System.EventArgs)
        If blnDefaultsLoaded = False Then
            'System.Threading.Thread.Sleep(1500)
            'Call oLoad_Defaults_Supplier()
        End If
    End Sub
    Private Sub Supplier_ComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)
        Call oLoad_Defaults_Supplier()
    End Sub
    Sub oLoad_Defaults_Supplier()
        Try
            If blnRunning = True Then Exit Sub
            If blnFormFinishedLoading = False Then Exit Sub
            Me.Save1.Enabled = True

            GetDefaultsforSupplier(cmbSupplier.SelectedValue)
            Call Fill_Category2_Combobox(Me.CategoryType1_ComboBox.Text)
            Call Fill_Cat2(Me.Cat1.Text)
            Call Fill_Category3_Combobox(Me.CategoryType2_ComboBox.Text)
            Call Fill_Cat3(Me.Cat2.Text)
            If Me.MultipleLines_CheckBox.Checked = True Then
                Me.lblMagicBox_Account_Segment4.Text = Get_Segment4_MB_Account(Me.CategoryType3_ComboBox.Text)
            Else
                Me.lblMagicBox_Account_Segment4.Text = Get_Segment4_MB_Account(Me.Cat3.Text)
            End If

            If lblMagicBox_Account_Segment4.Text = "" Then
                MessageBox.Show("The supplier's default account is missing, please manually select the account you want to capture the invoice against", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Cat1.Text = ""
                Cat2.Text = ""
                Cat3.Text = ""
                Cat1.Focus()
                ComboBox_Supplier.Text = cmbSupplier.Text
                Label_Success.Text = ""

            Else

                ComboBox_Supplier.Text = cmbSupplier.Text
                Me.Label_Success.Text = ""
                blnDefaultsLoaded = True

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub GetDefaultsforSupplier(ByVal SupplierID As Integer)

        '////// NOTE - THE DEFAULT VIEWS ARE DRAWM FROM TABLES THAT ARE ALREADY COMPANY-SPECIFIC SO WE DON"T NEED TO FILTER BY Co_ID '##### NEW #####

        If My.Settings.DBType = "Access" Then
            'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
            Dim oResult As String
            xAccessTable_Suppliers.DefaultView.RowFilter = "SupplierID='" & SQLConvert(SupplierID) & "'"
            '(1) Default for Category 1 ComboBox
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Category_Default").ToString
                'Me.CategoryType1_ComboBox.Text = ""
                Me.CategoryType1_ComboBox.SelectedText = oResult
                Me.Cat1.Text = ""
                Me.Cat1.SelectedText = oResult
            Else
                Me.CategoryType1_ComboBox.Text = ""
                Me.Cat1.Text = ""
            End If
            '(2) Default for Category 2 ComboBox
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Detail_Default").ToString
                Me.CategoryType2_ComboBox.Text = ""
                Me.CategoryType2_ComboBox.SelectedText = oResult
                Me.Cat2.Text = ""
                Me.Cat2.SelectedText = oResult
            Else
                Me.CategoryType2_ComboBox.Text = ""
                Me.Cat2.Text = ""
            End If
            '(3) Default for Category 3 ComboBox
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Detail_Default2").ToString
                Me.CategoryType3_ComboBox.Text = ""
                Me.CategoryType3_ComboBox.SelectedText = oResult
                Me.Cat3.Text = ""
                Me.Cat3.SelectedText = oResult
            Else
                Me.CategoryType3_ComboBox.Text = ""
                Me.Cat3.Text = ""
            End If
            '(4) VAT Default 
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("VAT_Default").ToString
                Me.cmbVATType.Text = ""
                Me.cmbVATType.Text = oResult
                Me.VATType_ComboBox.Text = ""
                Me.VATType_ComboBox.SelectedText = oResult
            Else
                'Me.VATType_ComboBox.Text = "1"
            End If
            '(5) Payment Default 
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Payment_Default").ToString
                Me.cmbPaymentType.Text = ""
                Me.cmbPaymentType.SelectedText = oResult
            Else
                Me.cmbPaymentType.SelectedText = "CASH"
            End If
        ElseIf My.Settings.DBType = "SQL" Then

            'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
            Dim oResult As String
            xSQLTable_Suppliers.DefaultView.RowFilter = "SupplierID='" & SQLConvert(SupplierID) & "'"
            '(1) Default for Category 1 ComboBox
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Category_Default").ToString
                Me.CategoryType1_ComboBox.Text = ""
                Me.CategoryType1_ComboBox.SelectedText = oResult

                Me.CategoryType1_ComboBox.Text = oResult
                'Me.CategoryType1_ComboBox.DropDownStyle = ComboBoxStyle.DropDown
                Me.CategoryType1_ComboBox.Text = oResult
                'Me.CategoryType1_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList
                'Me.CategoryType1_ComboBox.SelectedIndex = Me.CategoryType1_ComboBox.
                Me.CategoryType1_ComboBox.SelectedItem = oResult
                Me.Cat1.Text = ""
                Me.Cat1.SelectedText = oResult
            Else
                Me.CategoryType1_ComboBox.Text = ""
                Me.Cat1.Text = ""
            End If
            '(2) Default for Category 2 ComboBox
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Detail_Default").ToString
                Me.CategoryType2_ComboBox.Text = ""
                Me.CategoryType2_ComboBox.SelectedText = oResult

                Me.Cat2.Text = ""
                Me.Cat2.SelectedText = oResult
            Else
                Me.CategoryType2_ComboBox.Text = ""
                Me.Cat2.Text = ""
            End If
            '(3) Default for Category 3 ComboBox
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Detail_Default2").ToString
                Me.CategoryType3_ComboBox.Text = ""
                Me.CategoryType3_ComboBox.SelectedText = oResult
                Me.Cat3.Text = ""
                Me.Cat3.SelectedText = oResult
            Else
                Me.CategoryType3_ComboBox.Text = ""
                Me.Cat3.Text = ""
            End If
            '(4) VAT Default 
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("VAT_Default").ToString
                Me.cmbVATType.Text = ""
                Me.cmbVATType.Text = oResult
                Me.VATType_ComboBox.Text = ""
                Me.VATType_ComboBox.SelectedText = oResult
            Else
                'Me.VATType_ComboBox.Text = "1"
            End If
            '(5) Payment Default 
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Payment_Default").ToString
                Me.cmbPaymentType.Text = ""
                Me.cmbPaymentType.SelectedText = oResult
            Else
                Me.cmbPaymentType.SelectedText = "CASH"
            End If
        End If
    End Sub

    Private Sub AmountPaid_TextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles AmountPaid_TextBox.TextChanged
        If Me.ComboBox_Status.Text = "" Then
            MsgBox("Please set the payment status to 'PAID'!")
        End If
    End Sub

    Function Reference_There(ByVal oReference As String, ByVal SupplierID As Integer) As Boolean

        Dim oCount As Integer

        Try
            If My.Settings.DBType = "Access" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Transactions WHERE [Reference]='" & oReference & "' And SupplierID = " & SupplierID & " And Void = False"

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Reference_There = True
                Else
                    Reference_There = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Count(*) As MyCount FROM Transactions WHERE [Reference]='" & oReference & "' And SupplierID = " & SupplierID & " And Void = 0"

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                oCount = CInt(cmd.ExecuteScalar())

                If oCount > 0 Then
                    Reference_There = True
                Else
                    Reference_There = False
                End If
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error checking an invoice number against a Supplier! " & ex.Message & "." & " 038")
        End Try
    End Function
    Private Sub Button1_Click_2(sender As System.Object, e As System.EventArgs)

        If lblEditInvoice.Visible = False Then
            If lblEditInvoice.Visible <> True And Text.Contains("Supplier Debit") Then
                If Reference_There(Me.txtInvoiceNumber.Text, CInt(cmbSupplier.SelectedValue)) = True Then
                    MsgBox("The invoice number already exists for that supplier. It will not be possible to save this!")
                    txtInvoiceNumber.Text = ""
                    txtInvoiceNumber.Focus()
                End If
            End If
        End If

    End Sub
    Private Sub InvoiceNumber_TextBox_Leave(sender As Object, e As System.EventArgs) Handles txtInvoiceNumber.Leave

        If lblEditInvoice.Visible = False Then
            If Text.Contains("Supplier Debit") Or Text.Contains("Invoice Capture") Then

                If cmbSupplier.Text = "" Or cmbSupplier.SelectedIndex = -1 Then
                    MessageBox.Show("Please select a supplier to capture the invoice against before you continue", "Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtInvoiceNumber.Text = ""
                    cmbSupplier.Focus()
                    Exit Sub
                End If

                If Reference_There(txtInvoiceNumber.Text, CInt(cmbSupplier.SelectedValue)) = True Then
                    MessageBox.Show("The invoice/reference  number already exists for the supplier selected. Please use a different invoice/reference number in order to proceed.", "Duplicate Reference number", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtInvoiceNumber.Text = ""
                    txtInvoiceNumber.Focus()
                    Exit Sub
                End If

            End If
        End If

    End Sub
    Private Sub ComboBox_Status_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox_Status.SelectedIndexChanged

        Try
            If Me.ComboBox_Status.Text = "PAID" Or Me.ComboBox_Status.Text = "PAID LOCK" Then
                Me.AmountPaid_TextBox.Enabled = True
                Me.AmountPaid_TextBox.Text = Me.InvoiceTotal_TextBox.Text
            Else
                Me.AmountPaid_TextBox.Enabled = False
                Me.AmountPaid_TextBox.Text = ""
            End If
        Catch ex As Exception
            MsgBox("Error enabling Paid Amount box")
        End Try
    End Sub
    Function ResizeForm(PosType As String)

        If PosType = "PositionSingle" Then
            Dim lt, tp As Integer
            lt = PositionSingle.Right + 15
            tp = PositionSingle.Bottom + 15
            Me.Size = New Size(lt, tp)
        Else
            Dim lt, tp As Integer
            lt = PositionMulti.Right + 15
            tp = PositionMulti.Bottom + 15
            Me.Size = New Size(lt, tp)
        End If



    End Function
    Private Sub MultipleLines_CheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles MultipleLines_CheckBox.CheckedChanged
        Try

            If Me.MultipleLines_CheckBox.Checked = True Then
                VATType_ComboBox.SelectedItem = 0
                Me.InvoiceTotal_TextBox.Enabled = False
                Me.cmbVATType.Visible = False
                Me.LabelVATType.Visible = False
                'Me.txtVATAmount.Visible = False
                'Me.LabelVATAmt.Visible = False
                Me.txtNonVATAmount.Visible = False
                Me.LabelExVAT.Visible = False
                Me.lblVT.Visible = True


                If Me.chkCredit.Checked = True Then

                    Me.GroupBox_Credit.Visible = False
                    'Multiple Credits
                    Me.lblCredit.Visible = True
                    Me.txtCredit.Visible = True
                    Me.lblCrVAT.Visible = True
                    Me.txtCrVAT.Visible = True
                    Me.ListView1.Columns(1).Width = 60
                    Me.ListView1.Columns(4).Width = 60

                Else

                    Me.GroupBox_Credit.Visible = False
                    'Multiple Credits
                    Me.lblCredit.Visible = False
                    Me.txtCredit.Visible = False
                    Me.lblCrVAT.Visible = False
                    Me.txtCrVAT.Visible = False
                    Me.ListView1.Columns(1).Width = 0
                    Me.ListView1.Columns(4).Width = 0

                End If

                Me.ListView1.Visible = True
                Me.Size = New Size(1033, 530)

                Me.Cat1.Visible = False
                Me.Cat2.Visible = False
                Me.Cat3.Visible = False
                Me.lblCat1.Visible = False
                Me.lblCat2.Visible = False
                Me.lblCat3.Visible = False

                'Me.txtVATAmount.Text = ""
                'Me.cmbVATType.Text = ""
                'Me.txtNonVATAmount.Text = ""
                Me.txtVATAmount.Enabled = False
                Me.cmbVATType.Enabled = False
                Me.txtNonVATAmount.Enabled = False

                Me.AddRow_Button.Visible = True
                Me.DeleteButton.Visible = True
                'Me.EditButton.Visible = True
                Me.ListView1.Visible = True
                Me.Save1.Visible = False
                Me.Save2.Visible = True

                Me.FindInvoice_GroupBox.Visible = False

                'resize form
                ResizeForm("PositionMulti")

            Else
                'MULTIPLES NOT CHECKED
                Me.cmbVATType.Visible = True
                Me.LabelVATType.Visible = True
                'Me.txtVATAmount.Visible = True
                'Me.LabelVATAmt.Visible = True
                Me.txtNonVATAmount.Visible = True
                Me.LabelExVAT.Visible = True
                Me.lblVT.Visible = False

                Me.InvoiceTotal_TextBox.Enabled = True

                If Me.chkCredit.Checked = True Then
                    Me.txtCreditAmount.Visible = True
                    Me.GroupBox_Credit.Visible = True
                End If


                Me.EditInvoices_CheckBox.Visible = True 'show again
                Me.ListView1.Visible = False

                Me.Cat1.Visible = True
                Me.Cat2.Visible = True
                Me.Cat3.Visible = True
                Me.lblCat1.Visible = True
                Me.lblCat2.Visible = True
                Me.lblCat3.Visible = True

                Me.txtVATAmount.Enabled = True
                Me.cmbVATType.Enabled = True
                'Me.Size = New Size(900, 420)
                Me.Size = New Size(850, 375)
                Me.AddRow_Button.Visible = False
                Me.DeleteButton.Visible = False
                'Me.EditButton.Visible = False
                Me.ListView1.Visible = False
                Me.Save1.Visible = True
                Me.Save2.Visible = False
                Me.FindInvoice_GroupBox.Visible = True
                ResizeForm("PositionSingle")
            End If

            If Me.EditInvoices_CheckBox.Checked = True Then
                Me.DataGridView1.Visible = True
            Else
                Me.DataGridView1.Visible = False
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub

    Private Sub EditInvoices_CheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles EditInvoices_CheckBox.CheckedChanged
        Try

            Me.Hide()
            Dim frmInvcEdDel As New frmInvoice_Edit_Delete
            frmInvcEdDel.ShowDialog()

            If Me.EditInvoices_CheckBox.Checked = True Then

                Me.Size = New Size(1252, 890)

            Else
                If Me.MultipleLines_CheckBox.Checked = True Then
                    Me.Size = New Size(1252, 650)
                Else
                    Me.Size = New Size(1100, 410)
                End If

            End If


        Catch ex As Exception
            MsgBox("There was an error resizing the form")
        End Try
    End Sub
    Dim blnClear As Boolean = False
    Private Sub AddRow_Button_Click(sender As System.Object, e As System.EventArgs) Handles AddRow_Button.Click
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            If cmbSupplier.Text = "" Then
                MessageBox.Show("Please choose a supplier in order to continue!", "Supplier validation", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbSupplier.Focus()
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            If txtInvoiceNumber.Text.Trim.Length = 0 Then
                MessageBox.Show("Please supply invoice/Reference number in order to continue", "Invoice Number", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtInvoiceNumber.Focus()
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If


            If Not IsNumeric(txtGrossAmount.Text) Or txtGrossAmount.TextLength = 0 Then
                MessageBox.Show("Please supply the Gross amount in a correct format", "Gross Amount", MessageBoxButtons.OK, MessageBoxIcon.Question)
                Me.txtGrossAmount.Text = ""
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.VATType_ComboBox.Text = "" Then
                MessageBox.Show("Please Select the vat type.", "Vat Type", MessageBoxButtons.OK, MessageBoxIcon.Question)
                VATType_ComboBox.Focus()
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If


            If Me.VATAmount_TextBox.Text = "" Or Not IsNumeric(VATAmount_TextBox.Text) Then
                MessageBox.Show("Please enter VAT amount in a correct format", "VAT Amount", MessageBoxButtons.OK, MessageBoxIcon.Question)
                VATAmount_TextBox.Focus()
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Get_Segment4_MB_Account_Multiples(Me.CategoryType3_ComboBox.Text) = "## NO ACCOUNT ##" Then
                MessageBox.Show("There is no Segment 4 account for this category, please select segment 4", "Accounts", MessageBoxButtons.OK, MessageBoxIcon.Question)
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            If Me.CategoryType3_ComboBox.Text = "" Then
                MessageBox.Show("Please select a third category", "Accounts Category", MessageBoxButtons.OK, MessageBoxIcon.Question)
                Enabled = True
                Cursor = Cursors.Arrow
                Exit Sub
            End If

            Dim oCol1 As String = Me.txtGrossAmount.Text
            Dim oCol2 As String = Me.txtCredit.Text 'Credit
            Dim oCol3 As String = Me.VATType_ComboBox.SelectedValue.ToString
            Dim oCol4 As String = Me.VATAmount_TextBox.Text
            Dim oCol5 As String = Me.txtCrVAT.Text 'Credit VAT
            Dim oCol6 As String = Me.NonVAT_Amount_TextBox.Text
            Dim oCol7 As String = Me.CategoryType1_ComboBox.Text
            Dim oCol8 As String = Me.CategoryType2_ComboBox.Text
            Dim oCol9 As String = Me.CategoryType3_ComboBox.Text
            Dim oCol10 As String = Me.lblMagicBox_Account_Segment4.Text


            Dim str(9) As String
            Dim itm As ListViewItem
            str(0) = oCol1
            str(1) = oCol2
            str(2) = oCol3
            str(3) = oCol4
            str(4) = oCol5
            str(5) = oCol6
            str(6) = oCol7
            str(7) = oCol8
            str(8) = oCol9
            str(9) = oCol10

            itm = New ListViewItem(str)
            Me.ListView1.Items.Add(itm)

            Call TotalAmountBox()
            Call TotalVATBox()
            'If Me.chkCredit.Checked = True Then
            'Call oLoad_Credit_ListBox()
            'End If

            blnClear = True
            Me.txtGrossAmount.Text = ""
            Me.txtCrVAT.Text = ""
            Me.txtCredit.Text = ""
            Me.NonVAT_Amount_TextBox.Text = ""
            Me.VATAmount_TextBox.Text = ""
            Me.txtGrossAmount.Focus()

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try
    End Sub

    Private Sub DeleteButton_Click(sender As System.Object, e As System.EventArgs) Handles DeleteButton.Click
        Try

            If Me.ListView1.SelectedItems.Count > 1 Then
                MsgBox("Please only delete one line at a time!")
                Exit Sub
            End If

            Me.Refresh()

            Dim items(Me.ListView1.SelectedItems.Count - 1) As Object 'this code needed as multiselect is true
            Me.ListView1.SelectedItems.CopyTo(items, 0)
            For Each item As ListViewItem In items
                Me.ListView1.Items.Remove(item)
            Next

            Call TotalAmountBox()
            Call TotalVATBox()

        Catch ex As Exception
            MsgBox("There was an error deleting rows " & ex.Message & " 032")
        End Try


    End Sub
    Private Sub EditButton_Click(sender As System.Object, e As System.EventArgs)
        Try

            If Me.ListView1.SelectedIndices.Count = 0 Then
                MsgBox("Please select a line in the list box. This will be edited with the values in the entry boxes.")
            Else

                Dim oCol1 As String = Me.txtGrossAmount.Text
                Dim oCol2 As String = Me.txtCredit.Text
                Dim oCol3 As String = Me.VATType_ComboBox.Text
                Dim oCol4 As String = Me.VATAmount_TextBox.Text
                Dim oCol5 As String = Me.txtCrVAT.Text
                Dim oCol6 As String = Me.NonVAT_Amount_TextBox.Text
                Dim oCol7 As String = Me.CategoryType1_ComboBox.Text
                Dim oCol8 As String = Me.CategoryType2_ComboBox.Text
                Dim oCol9 As String = Me.CategoryType3_ComboBox.Text

                With Me.ListView1.SelectedItems(0)
                    .SubItems(0).Text = oCol1
                    .SubItems(1).Text = oCol2
                    .SubItems(2).Text = oCol3
                    .SubItems(3).Text = oCol4
                    .SubItems(4).Text = oCol5
                    .SubItems(5).Text = oCol6
                    .SubItems(6).Text = oCol7
                    .SubItems(7).Text = oCol8
                    .SubItems(8).Text = oCol9
                End With
            End If

            Call TotalAmountBox()
            Call TotalVATBox()

        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Sub TotalAmountBox()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.ListView1.Items.Count - 1
            oTotal = oTotal + CDec(Me.ListView1.Items(i).SubItems(0).Text)
        Next
        Me.InvoiceTotal_TextBox.Text = CStr(oTotal)
    End Sub
    Sub TotalVATBox()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.ListView1.Items.Count - 1
            oTotal = oTotal + CDec(Me.ListView1.Items(i).SubItems(3).Text)
        Next
        Me.txtVATAmount.Text = CStr(oTotal)
    End Sub
    Private Sub ListView1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        Try

            Dim oCol1 As String
            Dim oCol2 As String
            Dim oCol3 As String
            Dim oCol4 As String
            Dim oCol5 As String
            Dim oCol6 As String
            Dim oCol7 As String
            Dim oCol8 As String
            Dim oCol9 As String



            oCol1 = ListView1.FocusedItem.SubItems(0).Text
            oCol2 = ListView1.FocusedItem.SubItems(1).Text
            oCol3 = ListView1.FocusedItem.SubItems(2).Text
            oCol4 = ListView1.FocusedItem.SubItems(3).Text
            oCol5 = ListView1.FocusedItem.SubItems(4).Text
            oCol6 = ListView1.FocusedItem.SubItems(5).Text
            oCol7 = ListView1.FocusedItem.SubItems(6).Text
            oCol8 = ListView1.FocusedItem.SubItems(7).Text
            oCol9 = ListView1.FocusedItem.SubItems(8).Text

            Me.txtGrossAmount.Text = oCol1
            Me.txtCredit.Text = oCol2 'Credit
            Me.VATType_ComboBox.Text = oCol3
            Me.VATAmount_TextBox.Text = oCol4
            Me.txtCrVAT.Text = oCol5 'Credit VAT
            Me.NonVAT_Amount_TextBox.Text = oCol6
            Me.CategoryType1_ComboBox.Text = oCol7
            Me.CategoryType2_ComboBox.Text = oCol8
            Me.CategoryType3_ComboBox.Text = oCol9

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message & ". Please report this to system support", "Multiple invoice edit Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Enabled = True
            Cursor = Cursors.Arrow
        End Try

    End Sub


    Sub Load_Invoices_for_Supplier_to_ComboBox()
        Try
            If My.Settings.DBType = "Access then" Then
                Dim sSQL As String
                sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = False"

                Dim oFrom As String = Me.DateTimePicker_From.Value.ToString
                Dim oTo As String = Me.DateTimePicker_To.Value.ToString
                Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= #" & oFrom & "# And Transactions.[Transaction Date] <= #" & oTo & "#)"
                sSQL = sSQL & sDateCriteria
                Dim oSupplier As String
                oSupplier = Me.Edit_Supplier_Combo.Text
                If oSupplier <> "[ALL]" Then
                    oSupplier = Me.Edit_Supplier_Combo.Text
                    sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                sSQL = sSQL & " ORDER BY Transactions.Reference"
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)

                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()

                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Dim oReference As String
                Try

                    Me.Invoice_Numbers_Combo.Items.Clear()
                    Me.Invoice_Numbers_Combo.Items.Add("[ALL]")
                    While datareader.Read
                        If Not IsDBNull(datareader("Invoice Number")) = True Then
                            oReference = datareader("Invoice Number").ToString()
                            Me.Invoice_Numbers_Combo.Items.Add(oReference)
                        End If
                    End While
                    Me.Invoice_Numbers_Combo.SelectedIndex = 0

                    connection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message & " 033")
                    connection.Close()
                End Try
            ElseIf My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "SELECT Distinct Transactions.Reference as [Invoice Number] From Transactions Where Void = 0"
                Dim oFrom As String = Me.DateTimePicker_From.Value.ToString
                Dim oTo As String = Me.DateTimePicker_To.Value.ToString
                Dim sDateCriteria As String = " And (Transactions.[Transaction Date] >= '" & oFrom & "' And Transactions.[Transaction Date] <= '" & oTo & "')"
                sSQL = sSQL & sDateCriteria
                Dim oSupplier As String
                oSupplier = Me.Edit_Supplier_Combo.Text
                If oSupplier <> "[ALL]" Then
                    oSupplier = Me.Edit_Supplier_Combo.Text
                    sSQL = sSQL & " And Transactions.SupplierName = '" & SQLConvert(oSupplier) & "'"
                End If

                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                sSQL = sSQL & " ORDER BY Transactions.Reference"
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()

                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Dim oReference As String
                Try
                    Me.Invoice_Numbers_Combo.Items.Clear()
                    Me.Invoice_Numbers_Combo.Items.Add("[ALL]")
                    While datareader.Read
                        If Not IsDBNull(datareader("Invoice Number")) = True Then
                            oReference = datareader("Invoice Number").ToString()
                            Me.Invoice_Numbers_Combo.Items.Add(oReference)
                        End If
                    End While
                    Me.Invoice_Numbers_Combo.SelectedIndex = 0
                    connection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message & " 034")
                    connection.Close()
                End Try
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 035")
        End Try
    End Sub
    Private Sub InvoiceTotal_TextBox_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles InvoiceTotal_TextBox.Enter
        'this runs if the Supplier was tabbed through and the index changed code didn't run
        If blnDefaultsLoaded = False Then
            Call oLoad_Defaults_Supplier()
        End If
    End Sub
    Private Sub InvoiceTotal_TextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles InvoiceTotal_TextBox.TextChanged
        Try

            Me.Save1.Enabled = True
            If Me.InvoiceTotal_TextBox.Text = "" Then Exit Sub 'prevents code from firing again after set to blank

            If cmbSupplier.Text = "" Then
                MessageBox.Show("Please choose a supplier in order to continue!", "Supplier validation", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                InvoiceTotal_TextBox.Text = ""
                Exit Sub
            End If

            If IsNumeric(Me.InvoiceTotal_TextBox.Text) = True Then
                If CDec(Me.InvoiceTotal_TextBox.Text) < 0 Then
                    MsgBox("Negative numbers cannot be entered here!")
                    Me.InvoiceTotal_TextBox.Text = ""
                    Exit Sub
                End If
            End If

            If IsNumeric(InvoiceTotal_TextBox.Text) = False Then
                MsgBox("Please enter a number!")
                InvoiceTotal_TextBox.Text = ""
                Exit Sub
            Else
                Call oCalc_Numbers()
            End If


            Me.txtGrossAmount.Text = Me.InvoiceTotal_TextBox.Text

            If Me.ComboBox_Status.Text = "PAID" Or Me.ComboBox_Status.Text = "PAID LOCK" Then
                Me.AmountPaid_TextBox.Text = Me.InvoiceTotal_TextBox.Text
            End If
        Catch ex As Exception
            MsgBox("Error: " & ex.Message & " 036")
        End Try



    End Sub

    Private Sub FindButton_Click(sender As System.Object, e As System.EventArgs) Handles FindButton.Click
        Call oFind()
    End Sub

    Sub oFind()
        Call GetInvoiceDetailPerSupplier2()
        Call Load_Invoices_for_Supplier_to_ComboBox()
    End Sub
    Private Sub txtVATAmount_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtVATAmount.TextChanged
        On Error Resume Next
        If blnRunning = False Then

            If txtVATAmount.Text = "" Then Exit Sub
            TaxPerc = cmbVATType.SelectedValue.ToString
            TaxCalc = 1 + (TaxPerc / 100)
            If CDec(Me.txtVATAmount.Text) > CDec(Me.InvoiceTotal_TextBox.Text) Then
                MsgBox("The VAT amount cannot be more than the invoice amount!")
                Exit Sub
            End If
            Me.txtNonVATAmount.Text = CStr(CDec(Me.InvoiceTotal_TextBox.Text) - CDec(Me.txtVATAmount.Text))
            Dim TestVAT As Decimal
            TestVAT = Math.Round(Me.InvoiceTotal_TextBox.Text - (Me.InvoiceTotal_TextBox.Text / TaxCalc), 2)
            If Math.Round(CDec(Me.txtVATAmount.Text), 2) <> TestVAT And CInt(Me.VATType_ComboBox.SelectedValue.ToString) <> 0 Then
                Me.lblVATSplit.Visible = True
            Else
                Me.lblVATSplit.Visible = False
            End If
        End If
    End Sub

    Private Sub VATAmount_TextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles VATAmount_TextBox.TextChanged
        On Error Resume Next
        If blnRunning = False Then
            Dim TxCheckAmount As Double
            Dim CurrentTaxAmount As Double

            If VATAmount_TextBox.Text = "" Then Exit Sub

            If CDec(Me.VATAmount_TextBox.Text) > CDec(Me.txtGrossAmount.Text) Then
                MsgBox("The VAT amount cannot be more than the invoice amount!")
                Exit Sub
            End If

            TaxPerc = VATType_ComboBox.SelectedValue.ToString
            TaxCalc = 1 + (TaxPerc / 100)
            Me.NonVAT_Amount_TextBox.Text = CStr(CDec(Me.txtGrossAmount.Text) - CDec(Me.VATAmount_TextBox.Text))
            TxCheckAmount = Format(CDec(Me.txtGrossAmount.Text) - (CDec(Me.txtGrossAmount.Text) / TaxCalc), "0.00")
            CurrentTaxAmount = Format(CDec(Me.VATAmount_TextBox.Text), "0.00")
            If CurrentTaxAmount <> TxCheckAmount And TaxPerc <> 0 Then
                Me.lblVATSplit.Visible = True
            Else
                Me.lblVATSplit.Visible = False
            End If
        End If
    End Sub


    Function DBCount() As Long
        'Dim My.Settings.CS_Setting As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"
        Dim sSQL As String


        '------------------- Build SQL Statement ------------------------------------------------------------------------------
        Dim oSupplier As String
        oSupplier = Me.Edit_Supplier_Combo.Text
        Dim sSQL_Supplier As String = ""

        If oSupplier = "[ALL]" Or oSupplier = "" Then
            'do nothing
        Else
            oSupplier = Me.Edit_Supplier_Combo.Text
            sSQL_Supplier = " And Suppliers.SupplierName = '" & SQLConvert(oSupplier) & "'"
        End If

        '---------------------------------------------------------------------------------------------------------------------
        If My.Settings.DBType = "Access" Then

            sSQL = "SELECT Amount FROM Transactions WHERE [Transaction Type] = " & TransT_Payment & " And [DR_CR] = ''Dr''"
            sSQL = sSQL & " And Void = False"
            sSQL = sSQL & sSQL_Supplier
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            sSQL = sSQL & ")"

            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, connection)
            connection.Open()
            DBCount = cmd.ExecuteScalar
        ElseIf My.Settings.DBType = "SQL" Then

            sSQL = "SELECT Amount FROM Transactions WHERE [Transaction Type] = " & TransT_Payment & " And [DR_CR] = ''Dr''"
            sSQL = sSQL & " And Void = 0"
            sSQL = sSQL & sSQL_Supplier
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            sSQL = sSQL & ")"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            DBCount = cmd.ExecuteScalar
        End If

    End Function


    Private Sub Button_Activate_Click(sender As System.Object, e As System.EventArgs) Handles Button_Activate.Click
        Try
            Me.Label_Level.Text = CheckUserLevel(Me.txtUserName.Text, Me.txtPassword.Text)
        Catch ex As Exception
            MsgBox(ex.Message & " 037")
        End Try
    End Sub

    Private Sub VATType_ComboBox_TextChanged(sender As Object, e As System.EventArgs) Handles VATType_ComboBox.TextChanged

        '   If Me.VATType_ComboBox.Text = "2" Then
        ' Me.VATType_ComboBox.Text = "1"
        ' End If
        Call Calc_Numbers()
    End Sub

    Sub OldFillDGV_Unpaid_Invoices()
        Try

            Dim sSQL As String
            Dim oSupplier As String = Me.ComboBox_Supplier.Text
            Dim sSQL_Supplier As String = " And SupplierName = '" & SQLConvert(oSupplier) & "'"
            'First clear DGV
            DataGridView_Unpaid_Invoices.DataSource = Nothing
            DataGridView_Unpaid_Invoices.Columns.Clear()


            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.Date_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.Date_To.Value.Date.ToString("dd MMMM yyyy")
            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If


            sSQL = "Select [Reference], [Due], [Invoiced], [Transaction Date], [SupplierName], [Trans] As [ID] From [Step4b] Where Due >0"
            'sSQL = "Select * From [Step4] Where Due >0"

            sSQL = sSQL & sSQL_Supplier

            sSQL = sSQL & sDateCriteria

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            'DataGridView_Unpaid_Invoices.AutoGenerateColumns = False

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Outstanding_table")
                connection.Close()
                DataGridView_Unpaid_Invoices.DataSource = ds
                DataGridView_Unpaid_Invoices.DataMember = "Outstanding_table"
                Me.DataGridView_Unpaid_Invoices.Columns(1).DefaultCellStyle.Format = "n2"
                Me.DataGridView_Unpaid_Invoices.Columns(2).DefaultCellStyle.Format = "n2"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Outstanding_table")
                connection.Close()
                DataGridView_Unpaid_Invoices.DataSource = ds
                DataGridView_Unpaid_Invoices.DataMember = "Outstanding_table"
                Me.DataGridView_Unpaid_Invoices.Columns(1).DefaultCellStyle.Format = "n2"
                Me.DataGridView_Unpaid_Invoices.Columns(2).DefaultCellStyle.Format = "n2"
            End If

            Me.DataGridView_Unpaid_Invoices.Columns.Insert(0, New DataGridViewTextBoxColumn)
            Me.DataGridView_Unpaid_Invoices.Columns.Insert(0, New DataGridViewTextBoxColumn)
            Me.DataGridView_Unpaid_Invoices.Columns.Insert(0, New DataGridViewTextBoxColumn)

            Me.DataGridView_Unpaid_Invoices.Columns(0).DefaultCellStyle.Format = "n2"
            Me.DataGridView_Unpaid_Invoices.Columns(0).HeaderText = "Amount"
            Me.DataGridView_Unpaid_Invoices.Columns(0).DefaultCellStyle.ForeColor = Color.ForestGreen
            Me.DataGridView_Unpaid_Invoices.Columns(0).Width = 80
            Me.DataGridView_Unpaid_Invoices.Columns(1).HeaderText = "Date of Payment"
            Me.DataGridView_Unpaid_Invoices.Columns(1).DefaultCellStyle.ForeColor = Color.ForestGreen
            Me.DataGridView_Unpaid_Invoices.Columns(2).HeaderText = "Payment Type"
            Me.DataGridView_Unpaid_Invoices.Columns(2).DefaultCellStyle.ForeColor = Color.ForestGreen
            Me.DataGridView_Unpaid_Invoices.Columns(2).Width = 80
            Me.DataGridView_Unpaid_Invoices.Columns(3).Width = 80
            Me.DataGridView_Unpaid_Invoices.Columns(4).Width = 80

            For j As Integer = 0 To Me.DataGridView_Unpaid_Invoices.RowCount - 2
                Me.DataGridView_Unpaid_Invoices.Rows(j).Cells(1).Value = Me.CaptureDate_DateTimePicker.Value.Date.ToString("dd MMMM yyyy")
                Me.DataGridView_Unpaid_Invoices.Rows(j).Cells(2).Value = Me.cmbPaymentType.Text
            Next

        Catch ex As Exception
            MsgBox(ex.Message & " 038")
        End Try

    End Sub


    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        blnRunning = True
        Dim oResponse As MsgBoxResult
        Dim oSupplier As String = Me.ComboBox_Supplier.Text
        If Me.CheckBox1.Checked = True Then
            Me.GroupBox2.Visible = False
            Me.DataGridView_Unpaid_Invoices.Visible = True
            Me.DataGridView_Unpaid_Invoices.Location = New Point(40, 70)
            Me.DataGridView_Unpaid_Invoices.Size = New Size(525, 140)
            blnMakeAdditionalPayments = True 'set to true for run
            'Me.DataGridView_Unpaid_Invoices.Columns.Insert(0, New DataGridViewCheckBoxColumn)
            Me.GroupBox1.Visible = True
            Me.GroupBox3.Visible = False
            Me.Save1.Visible = False
            Me.Save2.Visible = False
            Me.btnPay.Visible = True
            If MultipleLines_CheckBox.Checked = False Then
                Me.btnPay.Location = Me.Save1.Location
            Else : MultipleLines_CheckBox.Checked = True
                Me.btnPay.Location = Me.Save2.Location
            End If

            If Me.txtInvoiceNumber.Text <> "" Then
                oResponse = MsgBox("Would you like to save the current invoice first?", MsgBoxStyle.YesNo)
                If oResponse = MsgBoxResult.Yes Then
                    If MultipleLines_CheckBox.Checked = True Then
                        Call oSave2()
                    Else
                        Call oSave1()
                    End If
                End If
            End If
            Me.ComboBox_Supplier.Text = oSupplier
            ' System.Threading.Thread.Sleep(2000)
            ' FillDGV_Unpaid_Invoices()
        Else
            Me.GroupBox2.Visible = True
            Me.DataGridView_Unpaid_Invoices.Visible = False
            Me.GroupBox1.Visible = False
            Me.GroupBox3.Visible = True
            Me.btnPay.Visible = False
            Me.Save1.Visible = True
            Me.Save2.Visible = True
            Me.Save1.Enabled = True
            Me.Save2.Enabled = True
        End If




        blnRunning = False
    End Sub

    Private Sub ComboBox_Supplier_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox_Supplier.SelectedIndexChanged
        ' Call FillDGV_Unpaid_Invoices()
    End Sub


    Private Sub Date_From_ValueChanged(sender As System.Object, e As System.EventArgs) Handles Date_From.ValueChanged
        '  Call FillDGV_Unpaid_Invoices()
    End Sub

    Private Sub Date_To_ValueChanged(sender As System.Object, e As System.EventArgs) Handles Date_To.ValueChanged
        '  Call FillDGV_Unpaid_Invoices()
    End Sub
    Private Sub btnPay_Click(sender As System.Object, e As System.EventArgs) Handles btnPay.Click
        Call oPay_Additional_Invoices()
        Me.CheckBox1.Checked = False
    End Sub

    Sub oPay_Additional_Invoices()

        Try
            '-------------------------------
            ' 6 boxes in supplier payments
            Dim oID As String
            Dim oSupplierName As String
            Dim oReference As String
            Dim oDateOfPayment As String
            Dim oPaymentAmount As String
            Dim oPaymentType As String
            '-------------------------------

            'Run quick test  to see if payment data valid
            Dim blnSomethingToBePaid As Boolean = False
            For j As Integer = 0 To Me.DataGridView_Unpaid_Invoices.RowCount - 1
                oPaymentAmount = Me.DataGridView_Unpaid_Invoices.Rows(j).Cells(0).Value
                If oPaymentAmount <> "" And IsNumeric(oPaymentAmount) = True Then
                    oPaymentType = Me.DataGridView_Unpaid_Invoices.Rows(j).Cells(2).Value
                    Dim Test As String = Get_Segm1Desc_From_DisplayName_in_Accounting(oPaymentType)
                    If Test = Nothing Then
                        MsgBox("Additional payments aborted! Payment '" & oPaymentAmount & "' at line " & CStr(5) & " incorrect. No 'Display Name' item has been set up in the Accounting table for this payment type!")
                        Exit Sub
                    End If
                    If oPaymentAmount > Me.DataGridView_Unpaid_Invoices.Rows(j).Cells(4).Value Then
                        MsgBox("Additional payments aborted! Payment amount is more than that due at line " & CStr(j + 1))
                        Exit Sub
                    End If
                    blnSomethingToBePaid = True
                End If
            Next

            If blnSomethingToBePaid = False Then
                MsgBox("There are no additional invoices to be paid!")
                Exit Sub
            End If



            Dim SP_Form As New frmSupplierPayments

            SP_Form.Show() 'NECESSARY FOR CODE TO BE CALLED ON IT
            'SP_Form.Visible = False

            For i As Integer = 0 To Me.DataGridView_Unpaid_Invoices.RowCount - 1
                oPaymentAmount = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(0).Value
                If oPaymentAmount <> "" And IsNumeric(oPaymentAmount) = True Then

                    oID = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(8).Value
                    oSupplierName = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(7).Value
                    oReference = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(3).Value
                    oDateOfPayment = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(1).Value
                    oPaymentType = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(2).Value
                    SP_Form.dgvPaymentsList.Rows.Add({oID, oSupplierName, oReference, oDateOfPayment, oPaymentAmount, oPaymentType})

                End If
            Next


            Call SP_Form.Pay()



            SP_Form.Close()

            '  Call FillDGV_Unpaid_Invoices() 'this will clear and reload the form

            Me.CheckBox1.Checked = False

        Catch ex As Exception
            MsgBox("Error running other payments!" & ex.Message & " 038")

        Finally

        End Try

    End Sub



    Sub CreateCreditNotes()


        Dim oTransactionID As String
        Dim oSupplierName As String
        Dim oReference As String
        Dim oCreditAmount As String
        Dim oInvoiceAmount As String

        Try


            For i As Integer = 0 To Me.DataGridView_Unpaid_Invoices.RowCount - 1

                oCreditAmount = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(0).Value
                If oCreditAmount <> "" And IsNumeric(oCreditAmount) = True Then

                    oTransactionID = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(8).Value
                    oSupplierName = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(7).Value
                    oReference = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(3).Value
                    oInvoiceAmount = Me.DataGridView_Unpaid_Invoices.Rows(i).Cells(5).Value

                    oProportion = CDec(oCreditAmount) / CDec(oInvoiceAmount)

                    Call IssueCreditNote(oTransactionID, oCreditAmount, oReference, oSupplierName, oInvoiceAmount)

                    Call Loop_Through_Transactions_With_Same_TransactionID(oTransactionID)
                    cNew_LinkID = cTransactionID
                    cNew_CaptureDate = Now.Date.ToString("dd MMMM yyyy")

                End If
            Next

            Me.DGV_Transactions.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error crediting invoices. " & ex.Message & " 039")
        End Try
    End Sub

    Sub Loop_Through_Transactions_With_Same_TransactionID(ByVal oTransactionID As Integer)
        Try

            Dim xID As Integer
            Dim sSQL As String

            Dim blnFirstRow As Boolean = True 'set to false after first read

            sSQL = "Select * From Transactions Where TransactionID = " & oTransactionID
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If
            sSQL = sSQL & " Order By ID"
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Dim oNewAmount As Decimal
                While datareader.Read
                    If Not IsDBNull(datareader("TransactionID")) Then
                        xID = datareader("TransactionID") 'ID is unique ID or that line
                        Load_Invoice_Data_From_ID_to_Variables(xID)
                        oNewAmount = cAmount * oProportion
                        If cDRCR = "Dr" Then
                            cDRCR = "Cr"
                        ElseIf cDRCR = "Cr" Then
                            cDRCR = "Dr"
                        End If
                        cGDC = "D"
                        Call Add_Transaction_to_DGV("DGV_Transactions2", cBatchID, cNew_TransactionID, cLinkID, cTransactionDate, cCaptureDate, cPPeriod, cFinYear, cGDC, cInvoiceNumber, _
    cDescription, cAccountNumber, cLinkAcc, oNewAmount, cTaxType, cTaxAmount, cUserID, _
    cSupplierID, cEmployeeID, cDescription2, cDescription3, cDescription4, cDescription5, cPosted, _
    cAccounting, cVoid, cTransactionType, cDRCR, cContraAccount, cDescriptionCode, cDocType, cSupplierName, cInfo, cInfo2, cInfo3)

                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Dim oNewAmount As Decimal
                While datareader.Read
                    If Not IsDBNull(datareader("TransactionID")) Then
                        xID = datareader("TransactionID") 'ID is unique ID or that line
                        Load_Invoice_Data_From_ID_to_Variables(xID)
                        oNewAmount = cAmount * oProportion
                        If cDRCR = "Dr" Then
                            cDRCR = "Cr"
                        ElseIf cDRCR = "Cr" Then
                            cDRCR = "Dr"
                        End If
                        cGDC = "D"
                        Call Add_Transaction_to_DGV("DGV_Transactions2", cBatchID, cNew_TransactionID, cLinkID, cTransactionDate, cCaptureDate, cPPeriod, cFinYear, cGDC, cInvoiceNumber, _
    cDescription, cAccountNumber, cLinkAcc, oNewAmount, cTaxType, cTaxAmount, cUserID, _
    cSupplierID, cEmployeeID, cDescription2, cDescription3, cDescription4, cDescription5, cPosted, _
    cAccounting, cVoid, cTransactionType, cDRCR, cContraAccount, cDescriptionCode, cDocType, cSupplierName, cInfo, cInfo2, cInfo3)

                    End If
                End While
                connection.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 040")
        Finally

        End Try

    End Sub

    '----------------New Credit note code -----------------------


    Private Sub chkCredit_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkCredit.CheckedChanged
        Try
            If Val(InvoiceTotal_TextBox.Text) = 0 Then
                MsgBox("Invoice amount must be greater then zero!")
                chkCredit.Checked = False
                Exit Sub
            End If
            Me.txtCreditAmount.Text = "0"
            Me.txtCreditExVAT.Text = "0"
            Me.txtCreditVAT.Text = "0"


            If Me.chkCredit.Checked = True Then
                If MultipleLines_CheckBox.Checked = True Then
                    Me.GroupBox_Credit.Visible = False
                    Me.ListView1.Visible = False
                    Me.ListView1.Visible = True
                    Me.GroupBox2.Visible = True

                    'Multiple Credits
                    Me.lblCredit.Visible = True
                    Me.txtCredit.Visible = True
                    Me.lblCrVAT.Visible = True
                    Me.txtCrVAT.Visible = True
                    Me.ListView1.Columns(1).Width = 60
                    Me.ListView1.Columns(4).Width = 60
                Else
                    Me.GroupBox_Credit.Visible = True
                    Me.GroupBox2.Visible = True
                    'Multiple Credits
                    Me.lblCredit.Visible = False
                    Me.txtCredit.Visible = False
                    Me.lblCrVAT.Visible = False
                    Me.txtCrVAT.Visible = False
                    Me.ListView1.Columns(1).Width = 0
                    Me.ListView1.Columns(4).Width = 0
                End If

            ElseIf Me.chkCredit.Checked = False Then
                Me.GroupBox_Credit.Visible = False

                Me.GroupBox2.Visible = True
                Me.lblCredit.Visible = False
                Me.txtCredit.Visible = False
                Me.lblCrVAT.Visible = False
                Me.txtCrVAT.Visible = False
                Me.ListView1.Columns(1).Width = 0
                Me.ListView1.Columns(4).Width = 0
            End If
        Catch ex As Exception
            MsgBox("Error with credit visuala. " & ex.Message & " 041")
        End Try
    End Sub

    Private Sub txtCreditAmount_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtCreditAmount.TextChanged
        If blnFormFinishedLoading = True Then
            If Me.txtCreditAmount.Text = "" Then
                Me.txtCreditAmount.Text = "0"
            End If
            oRunCreditNumbersCheck()
        End If
    End Sub


    Sub oRunCreditNumbersCheck()
        Dim oVATAmount As Decimal
        Dim oGrossAmount As Decimal
        Dim oProportion As Decimal
        Dim oCreditVAT As Decimal
        Dim oCreditAmount As Decimal

        Try

            If IsNumeric(Me.txtCreditAmount.Text) = True Then
                oCreditAmount = CDec(Me.txtCreditAmount.Text)
            Else
                'MsgBox("Please enter a numeric value for the credit amount!")
                Exit Sub
            End If

            If IsNumeric(Me.txtGrossAmount.Text) = True Then
                oGrossAmount = CDec(Me.txtGrossAmount.Text)
            Else
                'MsgBox("The gross amount is not numeric!")
                Exit Sub
            End If

            If IsNumeric(Me.VATAmount_TextBox.Text) = True Then
                oVATAmount = CDec(Me.VATAmount_TextBox.Text)
            Else
                'MsgBox("The VAT amount is not numeric!")
                Exit Sub
            End If

            If Me.cmbVATType.Text = "1" Then
                oProportion = oVATAmount / oGrossAmount
                oCreditVAT = oProportion * oCreditAmount
                oCreditVAT = Math.Round(oCreditVAT, 2)
                Me.txtCreditVAT.Text = CStr(oCreditVAT)
                Me.txtCreditExVAT.Text = CStr(oCreditAmount - oCreditVAT)
            Else
                Me.txtCreditVAT.Text = "0"
                Me.txtCreditExVAT.Text = oCreditAmount
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 042")
        End Try
    End Sub

    Private Sub btnPayMultiple_Click(sender As System.Object, e As System.EventArgs) Handles btnPayMultiple.Click

        Try

            blnRunning = True

            Me.ComboBox_Status.Text = "UNPAID"

            If Me.txtInvoiceNumber.Text = "" Then
                Form_Supplier_Payments.Show()
                Form_Supplier_Payments.TopMost = True
                Exit Sub
            End If

            Dim oSupplier As String = cmbSupplier.Text
            Dim oInvoiceNumber As String = Me.txtInvoiceNumber.Text
            Dim oInvoiceTotal As String = Me.InvoiceTotal_TextBox.Text

            Dim oResponse As MsgBoxResult
            If Me.txtInvoiceNumber.Text <> "" Then
                oResponse = MsgBox("Would you like to save the current invoice first?", MsgBoxStyle.YesNo)
                If oResponse = MsgBoxResult.Yes Then
                    If MultipleLines_CheckBox.Checked = True Then
                        Call oSave2()
                    Else
                        Call oSave1()
                    End If
                End If
            End If
            Form_Supplier_Payments.Show()
            Form_Supplier_Payments.TopMost = True
            'Form_Supplier_Payments.
            Form_Supplier_Payments.cmbSupplier.Text = oSupplier

            'loop through and find invoice then select and click add button
            Dim i As Integer
            Dim oTrans As String
            For i = 0 To Form_Supplier_Payments.dgvInvoiceList.RowCount - 1
                oTrans = Form_Supplier_Payments.dgvInvoiceList.Rows(i).Cells(2).Value
                If oTrans = oInvoiceNumber Then
                    Form_Supplier_Payments.dgvInvoiceList.Rows(i).Selected = True
                    Call Form_Supplier_Payments.AddRow()
                End If
            Next

            blnRunning = False

        Catch ex As Exception
            MsgBox(ex.Message & " 043")
        End Try

    End Sub
    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        Call ClearBoxes()
        Me.InvoiceDate_DateTimePicker.Value = Date.Now
        Me.lblEditInvoice.Visible = False
        Invoice_Capture_Load(sender, e)
    End Sub
    Function Get_Segment4_Using_Type_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Type] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment4_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    Dim oResult As String
                    xSQLTable_Accounts.DefaultView.RowFilter = "Type='" & SQLConvert(sString) & "'"
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                    Get_Segment4_Using_Type_in_Accounting = oResult

                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segment4_Using_Type_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()

                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the Segment 4 Account based on Type in Accounting!")
        End Try
    End Function


    Function Get_Account_Number_From_Segment4(ByVal sString As String) As String
        Try

            Dim sSQL As String

            sSQL = "SELECT DISTINCT [GL ACCOUNT] FROM Accounting WHERE [Segment 4] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Account_Number_From_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Account_Number_From_Segment4 = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            'MsgBox("There was an error looking up the GL Account based a Segment 4 code in Accounting! " & ex.Message)
        End Try
    End Function


    Function Database_Lookup_String_return_Integer(ByVal oTable As String, oLookupField As String, ByVal WhereField As String, ByVal WhereValue As String, ByVal DataType As String) As Integer
        Try
            Dim oResult As Object
            Dim sSQL As String

            sSQL = "SELECT DISTINCT " & oLookupField & " FROM " & oTable & " WHERE " & WhereField & " = '" & SQLConvert(WhereValue) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Database_Lookup_String_return_Integer = CInt(cmd.ExecuteScalar().ToString)
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Suppliers.DefaultView.RowFilter = WhereField & "='" & SQLConvert(WhereValue) & "'"
                    'If xSQLTable_Suppliers.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item(oLookupField).ToString
                    'End If
                    Database_Lookup_String_return_Integer = oResult

                Else

                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    oResult = cmd.ExecuteScalar()
                    If IsDBNull(oResult) = True Then
                        Database_Lookup_String_return_Integer = 0
                    Else
                        Database_Lookup_String_return_Integer = oResult
                    End If
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up a database value " & oLookupField & " in table " & oTable & Err.Description)

        End Try
    End Function

    Function GetPeriod(ByVal sTransactionDate As String) As String
        Try

            Dim sSQL As String

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= Format('" & sTransactionDate & "',""Short Date"")"
                sSQL = sSQL & " AND [End Date] >= Format('" & sTransactionDate & "',""Short Date"")"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetPeriod = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                ''DISABLED ###########################
                ''If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                ''Dim oResult As Integer
                ''Dim oCriteria As String = "[Start Date]<='" & sTransactionDate & "' And " & "[End Date]>='" & sTransactionDate & "'"
                ''xSQLTable_Periods.DefaultView.RowFilter = oCriteria
                'xSQLTable_Suppliers.DefaultView.RowFilter = "[Start Date]<='" & sTransactionDate & " And " & "[End Date]>='" & sTransactionDate
                ''If xSQLTable_Periods.DefaultView.Count = 1 Then
                ''oResult = xSQLTable_Periods.DefaultView.Item(0).Item("Period")
                ''End If
                ''GetPeriod = CStr(oResult)

                ''Else

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                sSQL = "SELECT DISTINCT Period FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= CAST('" & sTransactionDate & "' as date)"
                sSQL = sSQL & " AND [End Date] >= CAST('" & sTransactionDate & "' as date)"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetPeriod = cmd.ExecuteScalar()
                connection.Close()
                ''End If
            End If
        Catch ex As Exception
            MsgBox("There was an error getting the accounting period!" & ex.Message & " 045. Please make sure that Co_ID and other data is correct in the periods table! ")
            GetPeriod = "NA"
        End Try
    End Function

    Function GetFinYear(ByVal sTransactionDate As String) As String
        Try

            Dim sSQL As String

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                sSQL = "SELECT DISTINCT FinYear FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= Format('" & sTransactionDate & "',""Short Date"")"
                sSQL = sSQL & " AND [End Date] >= Format('" & sTransactionDate & "',""Short Date"")"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetFinYear = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

                sSQL = "SELECT DISTINCT FinYear FROM Periods"
                sSQL = sSQL & " WHERE [Start Date] <= CAST('" & sTransactionDate & "' as date)"
                sSQL = sSQL & " AND [End Date] >= CAST('" & sTransactionDate & "' as date)"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetFinYear = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error getting the 'financial year!" & ex.Message & " 045bb. Please make sure that Co_ID and other data is correct in the periods table! ")
            GetFinYear = "NA"
        End Try
    End Function

    Function Get_Segm1Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm1Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 1 Desc").ToString
                    'End If
                    Get_Segm1Desc_From_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segm1Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()

                End If

            End If


        Catch ex As Exception
            MsgBox("There was an error looking up Segment 1 Desc in Accounting! " & ex.Message & " 046")
        End Try
    End Function

    Function Get_Segm2Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm2Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 2 Desc").ToString
                    'If
                    Get_Segm2Desc_From_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segm2Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up Segment 2 Desc in Accounting!")
        End Try
    End Function

    Function Get_Segm3Desc_From_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segm3Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 3 Desc").ToString
                    'End If
                    Get_Segm3Desc_From_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segm3Desc_From_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox("There was an error looking up Segment 3 Desc in Accounting!")
        End Try
    End Function

    Function Get_Segment4_from_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Get_Segment4_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
                    Dim oResult As String
                    xSQLTable_Accounts.DefaultView.RowFilter = "[Display Name]='" & SQLConvert(sString) & "'"
                    'If xSQLTable_Accounts.DefaultView.Count = 1 Then
                    oResult = xSQLTable_Accounts.DefaultView.Item(0).Item("Segment 4").ToString
                    'If
                    Get_Segment4_from_DisplayName_in_Accounting = oResult
                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Get_Segment4_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                    connection.Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the Magic Box Code fro the Display name in Accounting!")
        End Try
    End Function

    Private Sub InvoiceDate_DateTimePicker_ValueChanged(sender As System.Object, e As System.EventArgs) Handles InvoiceDate_DateTimePicker.ValueChanged
        Try
            If CheckClosedPeriod(InvoiceDate_DateTimePicker.Value) = False Then
                MsgBox("The period for the selected date is closed. You may not transact on this date", MsgBoxStyle.Exclamation, "Closed period")
                Save1.Enabled = False
                Save2.Enabled = False
                Exit Sub
            Else
                Save1.Enabled = True
                Save2.Enabled = True
            End If


            If My.Settings.FutureInvoiceEnabled = "No" Or My.Settings.FutureInvoiceEnabled = Nothing Then
                If Me.InvoiceDate_DateTimePicker.Value.Date > Now.Date Then
                    MessageBox.Show("Invoice date cannot be in the future, if you want to capture future dated invoice please enable the option 'Allow future Invoices' on the settings screen", "Invoice date", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Me.InvoiceDate_DateTimePicker.Value = Now.Date
                    Exit Sub
                Else
                    DatePaid_DateTimePicker.Value = InvoiceDate_DateTimePicker.Value
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub txtCredit_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtCredit.TextChanged
        If Me.chkCredit.Checked = True And Me.MultipleLines_CheckBox.Checked = True Then
            Calc_Credit_Numbers()
        End If
    End Sub

    Private Sub txtCreditVAT_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtCreditVAT.TextChanged
        Me.txtCreditExVAT.Text = CStr(CDec(Me.txtCreditAmount.Text) - CDec(Me.txtCreditVAT.Text))
    End Sub


    Private Sub chkNotes_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkNotes.CheckedChanged
        If Me.chkNotes.Checked = True Then
            Me.Notes_RichTextBox.Visible = True
        Else
            Me.Notes_RichTextBox.Visible = False
        End If
    End Sub

    Function AlreadInCombo(ByVal oComboBox As ComboBox, ByVal oText As String) As Boolean
        AlreadInCombo = False
        For i As Integer = 0 To oComboBox.Items.Count - 1
            If oComboBox.Items(i).ToString = oText Then
                AlreadInCombo = True
            End If
        Next
    End Function

    Private Sub Edit_Supplier_Combo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Edit_Supplier_Combo.SelectedIndexChanged

    End Sub
    Private Sub Supplier_ComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ''Supplier_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList
        ''ComboBox1.DroppedDown = True 
    End Sub


    Function Get_Payment_Types_for_user(ByVal sString As String) As String
        'Try

        Dim sSQL As String
        sSQL = "SELECT [Approve_Payments_Types] FROM Users WHERE [UserName] = '" & SQLConvert(sString) & "'"
        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
        End If

        If My.Settings.DBType = "Access" Then
            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, connection)
            connection.Open()
            Get_Payment_Types_for_user = cmd.ExecuteScalar().ToString
            connection.Close()

        ElseIf My.Settings.DBType = "SQL" Then
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Get_Payment_Types_for_user = cmd.ExecuteScalar().ToString
            connection.Close()

        End If

        'Catch ex As Exception
        'MsgBox("There was an error looking up a users Payment type authorizations! " & ex.Message & " 323")
        'End Try
    End Function

    Function User_Authorized_to_Make_Payment_Type(ByVal oPaymentType As String) As Boolean
        Try

            Dim oPaymentAuthorizationTypes As String
            Dim arrSplit As Object
            Dim blnAutorized As Boolean = False

            oPaymentAuthorizationTypes = Get_Payment_Types_for_user(Globals.Ribbons.Ribbon1.lblUsername.Label)
            arrSplit = Split(oPaymentAuthorizationTypes, ",")

            ''Check if user is authorized to pay this
            User_Authorized_to_Make_Payment_Type = False

            For k As Integer = 0 To UBound(arrSplit)
                If oPaymentType = arrSplit(k) Then

                    User_Authorized_to_Make_Payment_Type = True

                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Sub oFilter()

        Try
            Dim oColName As String = "Segment 3 Desc"

            Dim strFilter As String

            If My.Settings.DBType = "Access" Then
                strFilter = "([" & oColName & "] LIKE '*" & Me.txtSearch.Text & "*' OR [" & oColName & "] LIKE '*" & Me.txtSearch.Text & "*')"
                xAccessTable_Search.DefaultView.RowFilter = strFilter
            Else
                strFilter = "([" & oColName & "] LIKE '*" & Me.txtSearch.Text & "*' OR [" & oColName & "] LIKE '*" & Me.txtSearch.Text & "*')"
                xSQLTable_Search.DefaultView.RowFilter = strFilter
            End If
            'ds.Tables("MyTable").DefaultView.RowFilter = "[Account Type] LIKE '*SA*' OR [Account Type] LIKE '*SA*'"

            'ds.Tables("MyTable").DefaultView.RowFilter = strFilter

            ''Dim xAccessTable_Search As New DataTable
            ''Dim xSQLTable_Search As New DataTable

        Catch ex As Exception
            MsgBox(ex.Message & " 12c")
        End Try
    End Sub

    Private Sub txtSearch_Enter(sender As Object, e As System.EventArgs) Handles txtSearch.Enter
        Try
            Me.DataGridView2.Visible = True
        Catch ex As Exception

        End Try
    End Sub





    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If Me.txtSearch.Text <> "" Then
                Me.DataGridView2.Visible = True
                Me.DataGridView2.BringToFront()
                oFilter()
            Else
                Me.DataGridView2.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Fill_DataGridView2()

        Try

            ''Dim xAccessAdapter_Search As OleDbDataAdapter
            ''Dim xAccessTable_Search As New DataTable
            ''Dim xSQLAdapter_Search As SqlDataAdapter
            ''Dim xSQLTable_Search As New DataTable

            Dim sSQL As String

            sSQL = "Select [Segment 3 Desc], [Segment 1 Desc], [Segment 2 Desc] From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Search = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Search.Fill(xAccessTable_Search)
                Me.DataGridView2.DataSource = xAccessTable_Search
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Search = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Search.Fill(xSQLTable_Search)
                Me.DataGridView2.DataSource = xSQLTable_Search
            End If
            Me.DataGridView2.ColumnHeadersVisible = False
            Me.DataGridView2.RowHeadersVisible = False
            'Me.DataGridView2.DefaultCellStyle.Font.Size = 8
        Catch ex As Exception
            MsgBox(ex.Message & " 051h")
        End Try
    End Sub
    Private Sub DataGridView2_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        Try

            Dim oIndex As Integer = e.RowIndex

            Dim Cat3 As String = Me.DataGridView2.Rows(oIndex).Cells(0).Value
            Dim Cat1 As String = Me.DataGridView2.Rows(oIndex).Cells(1).Value
            Dim Cat2 As String = Me.DataGridView2.Rows(oIndex).Cells(2).Value


            If MultipleLines_CheckBox.Checked = True Then
                Me.CategoryType1_ComboBox.Text = Cat1
                Me.CategoryType2_ComboBox.Text = Cat2
                Me.CategoryType3_ComboBox.Text = Cat3
            Else
                Me.Cat1.Text = Cat1
                Me.Cat2.Text = Cat2
                Me.Cat3.Text = Cat3
            End If


            Me.DataGridView2.Visible = False

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub DataGridView2_Leave(sender As Object, e As System.EventArgs) Handles DataGridView2.Leave
        Try
            Me.DataGridView2.Visible = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub DataGridView2_MouseLeave(sender As Object, e As System.EventArgs) Handles DataGridView2.MouseLeave
        Me.DataGridView2.Visible = False
    End Sub
    Private Sub DatePaid_DateTimePicker_ValueChanged(sender As Object, e As EventArgs) Handles DatePaid_DateTimePicker.ValueChanged
        Try
            Dim SelectedDate As Date = CDate(DatePaid_DateTimePicker.Value.Date.ToShortDateString)
            Dim InvoiceDate As Date = CDate(InvoiceDate_DateTimePicker.Value.Date.ToShortDateString)

            If SelectedDate < InvoiceDate Then
                DatePaid_DateTimePicker.Value = InvoiceDate_DateTimePicker.Value
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cmbSupplier_KeyDown(sender As Object, e As KeyEventArgs) Handles cmbSupplier.KeyDown
        If e.KeyCode = 13 And IsDroppedDown = True Then
            cmbSupplier_SelectedIndexChanged(sender, e)
            IsDroppedDown = False
        End If
    End Sub
    Private Sub cmbSupplier_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSupplier.SelectedIndexChanged
        Try
            If cmbSupplier.Text = "" Then Exit Sub
            Enabled = False
            Cursor = Cursors.AppStarting
            oLoad_Defaults_Supplier()
            oSuppId = cmbSupplier.SelectedValue
            If IsNumeric(oSuppId) Then
                Dim SupplierVat As String = CStr(GetSupplierVAT(oSuppId))
                cmbVATType.SelectedValue = SupplierVat
            End If
            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub txtGrossAmount_TextChanged(sender As Object, e As EventArgs) Handles txtGrossAmount.TextChanged
        Try

            If blnClear = True Then
                blnClear = False
            Else

                If MultipleLines_CheckBox.Checked = True Then

                    If Not IsNumeric(txtGrossAmount.Text) Or txtGrossAmount.TextLength = 0 Then
                        MessageBox.Show("Please supply the Gross amount in a correct format", "Gross Amount", MessageBoxButtons.OK, MessageBoxIcon.Question)
                        Me.txtGrossAmount.Text = ""
                        Exit Sub
                    End If

                    If Me.VATType_ComboBox.Text = "" Then
                        MessageBox.Show("Please Select the vat type.", "Vat Type", MessageBoxButtons.OK, MessageBoxIcon.Question)
                        VATType_ComboBox.Focus()
                        Exit Sub
                    End If

                    Calc_Numbers()

                End If

            End If
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "An Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub txtSearch_Leave(sender As Object, e As EventArgs) Handles txtSearch.Leave
        Me.DataGridView2.Visible = False
    End Sub
    Function GetSupplierVAT(ByVal SupplierID As Integer) As String

        Dim SupplierVat As String = ""
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select Vat_Default from Suppliers where SupplierID =" & SupplierID

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            If IsDBNull(cmd.ExecuteScalar()) Or IsNothing(cmd.ExecuteScalar()) Then
                SupplierVat = "0"
            Else
                SupplierVat = CStr(cmd.ExecuteScalar())
            End If
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
            Return SupplierVat
        Catch ex As Exception
        End Try
    End Function
    Dim IsDroppedDown As Boolean = Nothing
    Private Sub cmbSupplier_DropDown(sender As Object, e As EventArgs) Handles cmbSupplier.DropDown
        IsDroppedDown = True
    End Sub
    Function LoadTax() As Boolean

        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        '  cmbVATType.Items.Clear()
        '  VATType_ComboBox.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            cmbVATType.DataSource = ds.Tables(0)
            cmbVATType.ValueMember = "Value"
            cmbVATType.DisplayMember = "Display"
            cmbVATType.SelectedIndex = 0
            VATType_ComboBox.DataSource = ds.Tables(0)
            VATType_ComboBox.ValueMember = "Value"
            VATType_ComboBox.DisplayMember = "Display"
            VATType_ComboBox.SelectedIndex = 0
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

End Class