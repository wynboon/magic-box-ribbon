﻿Imports System.Collections

Public Class DataExtractsClass
#Region "Variable"
    Dim CompanyID As Integer = If(My.Settings.Setting_CompanyID = "", 0, CInt(My.Settings.Setting_CompanyID))
    Dim MagicBoxContext As New MagicBoxDataDataContext(My.Settings.CS_Setting)
#End Region
#Region "Methods"
    Public Function GetAccounts() As IEnumerable(Of Accounting)
        Return From Acc In MagicBoxContext.Accountings
           Where Acc.Co_ID = CompanyID
           Select Acc Order By Acc.Segment_4   
    End Function
    Public Sub DeleteUser(ByVal UserID As Integer)

        Dim UserToDelete As User = (From Usr In MagicBoxContext.Users
            Where Usr.UserId = UserID
        Select Usr).FirstOrDefault
        MagicBoxContext.Users.DeleteOnSubmit(UserToDelete)
        SaveChanges()

    End Sub
    Public Function GetUser(ByVal Username As String, ByVal Password As String) As User
        Return (From Usr In MagicBoxContext.Users
            Where Usr.Username = Username And Usr.Password = Password
        Select Usr).FirstOrDefault
    End Function
    Public Function GetAccountsSortByDesc() As IEnumerable(Of Accounting)
        Return From Acc In MagicBoxContext.Accountings
               Where Acc.Co_ID = CompanyID
               Select Acc Order By Acc.Segment_3_Desc
    End Function
    Public Function GetPaymentsRequests() As IEnumerable(Of PaymentsRequest)
        Return From PayReq In MagicBoxContext.PaymentsRequests
               Where PayReq.Co_ID = CompanyID And PayReq.RequestApproved = False _
               Select PayReq
    End Function
    Public Function GetPaymentsRequestsByDate(ByVal FromDate As Date, ByVal ToDate As Date, ByVal PayType As String) As IEnumerable(Of PaymentsRequest)
        Return From PayReq In MagicBoxContext.PaymentsRequests
               Where PayReq.Co_ID = CompanyID And PayReq.RequestApproved = False And PayReq.PaymentType = PayType _
                And PayReq.CapturedDate >= FromDate And PayReq.CapturedDate <= ToDate
               Select PayReq
    End Function
    Public Function GetMainTransID(ByVal InvoiceTransID As Integer) As Transaction
        Return (From Trans In MagicBoxContext.Transactions
                Where Trans.TransactionID = InvoiceTransID And Trans.Description_Code = "HAAAAA" _
                And Trans.DocType = "Invoice" And Trans.Accounting = True And Trans.Void = False
                Select Trans).FirstOrDefault
    End Function
    Public Function GetAccountTransactions(ByVal DescriptionCode As String, ByVal FromDate As Date, ByVal ToDate As Date) As IEnumerable(Of Transaction)
        Return From Trans In MagicBoxContext.Transactions
               Where Trans.Co_ID = CompanyID And Trans.Description_Code = DescriptionCode _
               And (Trans.Transaction_Date >= FromDate And Trans.Transaction_Date <= ToDate And Trans.Void = False And Trans.Accounting = True)
               Select Trans Order By Trans.Capture_Date Descending
    End Function
    Public Function GetAccountTransactionsByTxID(ByVal TxID As Long) As IEnumerable(Of Transaction)
        Return From Trans In MagicBoxContext.Transactions
               Where Trans.Co_ID = CompanyID And Trans.TransactionID = TxID _
                And Trans.Void = False And Trans.Accounting = True
               Select Trans Order By Trans.Capture_Date Descending
    End Function
    Public Function SupplierList(ByVal CompanyID As Integer) As IEnumerable(Of Supplier)

        Return (From MB_Suppliers In MagicBoxContext.Suppliers
               Where MB_Suppliers.Co_ID = CompanyID And MB_Suppliers.Active = "Active"
               Select MB_Suppliers Order By MB_Suppliers.SupplierName)
    End Function
    Public Function GetSupplierID(ByVal SuppName As String) As Integer

        Return (From MB_Suppliers In MagicBoxContext.Suppliers
               Where MB_Suppliers.Co_ID = CompanyID And MB_Suppliers.Active = "Active" _
               And MB_Suppliers.SupplierName = SuppName
               Select MB_Suppliers.SupplierID).FirstOrDefault

    End Function
    Public Function EmployeeList(ByVal CompanyID As Integer) As IEnumerable

        Return (From MB_Employees In MagicBoxContext.Employees
               Where MB_Employees.Co_ID = CompanyID And MB_Employees.Active = "Active" _
               Order By MB_Employees.First_Name
               Select New With {.EmployeeNames = MB_Employees.First_Name & " " & MB_Employees.Last_Name,
                                         .ID = MB_Employees.EmployeeID})
    End Function
    Public Function GetAccNumber(ByVal Segment4 As String) As String
        Return (From Trans In MagicBoxContext.Accountings
               Where Trans.Segment_4 = Segment4
               Select Trans.GL_ACCOUNT).FirstOrDefault
    End Function
    Public Function GetBankAcc(ByVal Segment3Code As String, ByVal Segment3Desc As String) As Accounting
        Dim BankAcc As Accounting
        BankAcc = (From Acc In MagicBoxContext.Accountings
                        Where Acc.Segment_3 = Segment3Code _
                        And Acc.Segment_3_Desc = Segment3Desc _
                        And Acc.Co_ID = CInt(My.Settings.Setting_CompanyID)
                        Select Acc).FirstOrDefault
        Return BankAcc
    End Function
    Public Sub UpdateBankAcc(ByVal BankAccID As Integer, ByVal AccNumber As String)
        Dim BankAccDetails As Accounting
        BankAccDetails = (From Acc In MagicBoxContext.Accountings
                        Where Acc.ID = BankAccID
                        Select Acc).FirstOrDefault
        BankAccDetails.BankAccountNo = AccNumber
        SaveChanges()
    End Sub
    Public Function GetCompanyID(ByVal CompanyName As String) As Integer
        Return (From Co In MagicBoxContext.Company_Details
                Where Co.CompanyName = CompanyName
                Select Co.ID).FirstOrDefault
    End Function
    Public Function CheckIFInvoiceHasRequest(ByVal InvoiceID As Integer) As PaymentsRequest
        Return (From PayReqs In MagicBoxContext.PaymentsRequests
                Where PayReqs.InvoiceTransactionID = InvoiceID
                Select PayReqs).FirstOrDefault
    End Function
    Public Function GetSupplierName(ByVal _SupplierID As Integer) As String
        Return (From s In MagicBoxContext.Suppliers
                Where s.SupplierID = _SupplierID
                Select s.SupplierName).FirstOrDefault
    End Function
    Public Function CheckSupplierRef(ByVal Ref As String) As String
        Return (From s In MagicBoxContext.Suppliers
                Where s.RefID = Ref And s.Co_ID = CInt(My.Settings.Setting_CompanyID)
                Select s.RefID).FirstOrDefault
    End Function

    Public Function CheckSupplierName(ByVal SupName As String) As String
        Return (From s In MagicBoxContext.Suppliers
                Where s.SupplierName = SupName And s.Co_ID = CInt(My.Settings.Setting_CompanyID)
                Select s.RefID).FirstOrDefault
    End Function

    Public Function SupplierNameIDPair(ByVal CompanyID As Integer)

        ' And MB_Suppliers.Active = "Active"
        Dim SupplierIDName = (From MB_Suppliers In MagicBoxContext.Suppliers
               Where MB_Suppliers.Co_ID = CompanyID And MB_Suppliers.Active = "Active"
               Select MB_Suppliers Order By MB_Suppliers.SupplierName)

        Dim SupplierIDNamePair As New Hashtable()

        For Each Supp In SupplierIDName
            SupplierIDNamePair.Add(Supp.SupplierID, Supp.SupplierName)
        Next

        Return SupplierIDNamePair

    End Function
#Region "User Control ON Processing"
    Public Sub CreateNewRequest(ByVal InvTransID As Integer, ByVal PaymentType As String, _
                            ByVal RequestedAmount As Decimal)

        Dim NewRequest As New PaymentsRequest
        NewRequest.InvoiceTransactionID = InvTransID
        NewRequest.CapturedDate = Date.Now.ToString("dd MMM yyyy") & " " & Date.Now.ToLongTimeString
        NewRequest.PaymentType = PaymentType
        NewRequest.RequestedAmount = RequestedAmount
        NewRequest.RequestApproved = False
        NewRequest.RequestOwner = My.Settings.LoggedInUser.ToString
        NewRequest.Co_ID = CompanyID

        MagicBoxContext.PaymentsRequests.InsertOnSubmit(NewRequest)
        SaveChanges()

    End Sub
    Public Function CheckRequestByTransID(ByVal InvoiceTransID As Integer) As PaymentsRequest
        Return (From Requests In MagicBoxContext.PaymentsRequests
                Where Requests.InvoiceTransactionID = InvoiceTransID And Requests.RequestApproved <> True
                Select Requests).FirstOrDefault
    End Function
    Public Function CheckIfRequestExist(ByVal InvoiceTransID As Integer, ByVal Amount As Decimal) As PaymentsRequest
        Return (From Requests In MagicBoxContext.PaymentsRequests
                Where Requests.InvoiceTransactionID = InvoiceTransID And Requests.RequestApproved <> True _
                And Requests.RequestedAmount = Amount And Requests.RequestOwner = CStr(My.Settings.LoggedInUser)
                Select Requests).FirstOrDefault
    End Function
    Public Function CheckIfRequestExist2(ByVal InvoiceTransID As Integer) As PaymentsRequest
        Return (From Requests In MagicBoxContext.PaymentsRequests
                Where Requests.InvoiceTransactionID = InvoiceTransID And Requests.RequestApproved <> True
                Select Requests).FirstOrDefault
    End Function
    Public Function GetInvoice(ByVal TransactionID As Integer) As Transaction
        Return (From Trans In MagicBoxContext.Transactions
               Where Trans.TransactionID = TransactionID And Trans.LinkID = 0 _
               And Trans.Description_Code = "HAAAAA" And Trans.DocType = "Invoice" _
               And Trans.Accounting = True And Trans.Void = False
               Select Trans).FirstOrDefault
    End Function
    Public Function GetInvoiceOriginalAmount(ByVal TransactionID As Integer) As Decimal
        Dim InvAmount As Decimal = 0
        InvAmount = (From Trans In MagicBoxContext.Transactions
                 Where Trans.TransactionID = TransactionID _
                 And Trans.LinkID = 0 And Trans.Description_Code = "HAAAAA" _
                 And Trans.DocType = "Invoice" _
                 Select Trans.Amount).FirstOrDefault

        Return InvAmount
    End Function
    Public Sub UpdateRequest(ByVal _RequestID As Integer, ByVal RequestApproved As Boolean, ByVal ApprovedAmount As Decimal, _
                             ByVal PaymentTransID As Integer, ByVal PayInAdvanceTID As Integer)

        Dim Request As PaymentsRequest = (From Reqs In MagicBoxContext.PaymentsRequests
                        Where Reqs.RequestID = _RequestID
                        Select Reqs).FirstOrDefault


        If RequestApproved = False And ApprovedAmount <> 0 Then
            Request.RequestApproved = RequestApproved
            Request.RequestedAmount = Request.RequestedAmount - ApprovedAmount

        ElseIf RequestApproved = True And ApprovedAmount <> 0 Then
            Request.RequestApproved = RequestApproved
            Request.RequestedAmount = Request.RequestedAmount - ApprovedAmount
            Request.PaymentTransactionID = PaymentTransID
            Request.RequestApprover = My.Settings.LoggedInUser.ToString
            Request.PayInAdvanceTransactionID = PayInAdvanceTID
        End If

        SaveChanges()

    End Sub

    Public Function ReturnSuppliersWithRequests()

        Dim Requests = From Reqs In MagicBoxContext.PaymentsRequests
                       Where Reqs.Co_ID = CInt(My.Settings.Setting_CompanyID) _
                       And Reqs.RequestApproved = False _
                       Select Reqs

        Dim SupplierIDNamePair As New Hashtable()

        If Not IsNothing(Requests) Then
            For Each request In Requests
                If LoggedUser_Roles.Contains(request.PaymentType) Or request.RequestOwner = LoggedInUser Then
                    Dim RequestTransaction = GetInvoice(request.InvoiceTransactionID)
                    If Not IsNothing(RequestTransaction) Then
                        If Not SupplierIDNamePair.Contains(RequestTransaction.SupplierName) Then
                            If RequestTransaction.SupplierName = "" Then
                                SupplierIDNamePair.Add(RequestTransaction.TransactionID, CStr(GetSupplierName(CInt(RequestTransaction.SupplierID))))
                            Else
                                SupplierIDNamePair.Add(RequestTransaction.TransactionID, RequestTransaction.SupplierName)
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return SupplierIDNamePair

    End Function
    
    Public Sub RemoveRequest(ByVal InvoiceTransID As Integer)
        Try
            Dim Request As PaymentsRequest = (From Reqs In MagicBoxContext.PaymentsRequests
                                    Where Reqs.InvoiceTransactionID = InvoiceTransID _
                                    And Reqs.Co_ID = CInt(My.Settings.Setting_CompanyID)
                                    Select Reqs).FirstOrDefault

            MagicBoxContext.PaymentsRequests.DeleteOnSubmit(Request)
            SaveChanges()
        Catch ex As Exception
            WriteToLog("error: " & Now().ToString & "-- " & ex.Message)
        End Try


    End Sub
    Public Sub RemoveRequestWithID(ByVal RequestID As Integer)

        Dim Request As PaymentsRequest = (From Reqs In MagicBoxContext.PaymentsRequests
                        Where Reqs.RequestID = RequestID
                        Select Reqs).FirstOrDefault
        MagicBoxContext.PaymentsRequests.DeleteOnSubmit(Request)
        SaveChanges()

    End Sub
    Public Function InvoiceHasPayment(ByVal TransID As Integer) As Boolean

        Dim BolHasPayment As Boolean = False

        Dim Results As Transaction = (From Tra In MagicBoxContext.Transactions
                       Where Tra.LinkID = TransID
                       Select Tra).FirstOrDefault

        If IsNothing(Results) Then
            BolHasPayment = False
        Else
            BolHasPayment = True
        End If

        Return BolHasPayment

    End Function
#End Region

    Public Sub UpdateGroupCompanies(ByVal CompName As String)

        Dim CheckName As Company_Detail = (From mbco In MagicBoxContext.Company_Details
                        Where mbco.CompanyName = CompName
                        Select mbco).FirstOrDefault

        If IsNothing(CheckName) Then
            Dim NewCompany As New Company_Detail
            With NewCompany
                .CompanyName = CompName
            End With
            MagicBoxContext.Company_Details.InsertOnSubmit(NewCompany)
            SaveChanges()
        End If

    End Sub
    Public Sub SaveChanges()
        MagicBoxContext.SubmitChanges()
    End Sub

#End Region
#Region "FrmPeriods"

#End Region
End Class
