﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_AddStore
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TB_StoreName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_ShortName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_Co_ID = New System.Windows.Forms.TextBox()
        Me.CB_GroupID = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_Add = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TB_POSID = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'TB_StoreName
        '
        Me.TB_StoreName.Location = New System.Drawing.Point(114, 12)
        Me.TB_StoreName.Name = "TB_StoreName"
        Me.TB_StoreName.Size = New System.Drawing.Size(189, 22)
        Me.TB_StoreName.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Store Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Short Name"
        '
        'TB_ShortName
        '
        Me.TB_ShortName.Location = New System.Drawing.Point(114, 40)
        Me.TB_ShortName.Name = "TB_ShortName"
        Me.TB_ShortName.Size = New System.Drawing.Size(189, 22)
        Me.TB_ShortName.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 17)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Company ID"
        '
        'TB_Co_ID
        '
        Me.TB_Co_ID.Location = New System.Drawing.Point(114, 68)
        Me.TB_Co_ID.Name = "TB_Co_ID"
        Me.TB_Co_ID.Size = New System.Drawing.Size(189, 22)
        Me.TB_Co_ID.TabIndex = 4
        '
        'CB_GroupID
        '
        Me.CB_GroupID.FormattingEnabled = True
        Me.CB_GroupID.Location = New System.Drawing.Point(114, 97)
        Me.CB_GroupID.Name = "CB_GroupID"
        Me.CB_GroupID.Size = New System.Drawing.Size(189, 24)
        Me.CB_GroupID.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Linked Group"
        '
        'btn_Add
        '
        Me.btn_Add.Location = New System.Drawing.Point(21, 154)
        Me.btn_Add.Name = "btn_Add"
        Me.btn_Add.Size = New System.Drawing.Size(282, 42)
        Me.btn_Add.TabIndex = 8
        Me.btn_Add.Text = "Add"
        Me.btn_Add.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 131)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 17)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "POS ID"
        '
        'TB_POSID
        '
        Me.TB_POSID.Location = New System.Drawing.Point(114, 126)
        Me.TB_POSID.Name = "TB_POSID"
        Me.TB_POSID.Size = New System.Drawing.Size(189, 22)
        Me.TB_POSID.TabIndex = 9
        '
        'Frm_AddStore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 208)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TB_POSID)
        Me.Controls.Add(Me.btn_Add)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CB_GroupID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TB_Co_ID)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TB_ShortName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TB_StoreName)
        Me.Name = "Frm_AddStore"
        Me.Text = "Add Store"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TB_StoreName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB_ShortName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_Co_ID As System.Windows.Forms.TextBox
    Friend WithEvents CB_GroupID As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btn_Add As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TB_POSID As System.Windows.Forms.TextBox
End Class
