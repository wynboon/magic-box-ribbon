﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_AddRole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_AddRole = New System.Windows.Forms.Button()
        Me.TB_RoleName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Ckb_MasterFiles = New System.Windows.Forms.CheckBox()
        Me.Ckb_Suppliers = New System.Windows.Forms.CheckBox()
        Me.Ckb_Approvals = New System.Windows.Forms.CheckBox()
        Me.Ckb_Intelligence = New System.Windows.Forms.CheckBox()
        Me.Ckb_AdditionalTools = New System.Windows.Forms.CheckBox()
        Me.Ckb_Accounting = New System.Windows.Forms.CheckBox()
        Me.Lbl_GrgID = New System.Windows.Forms.Label()
        Me.lbl_RoleID = New System.Windows.Forms.Label()
        Me.Ckb_isGroupAdmin = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Btn_AddRole
        '
        Me.Btn_AddRole.Location = New System.Drawing.Point(16, 287)
        Me.Btn_AddRole.Name = "Btn_AddRole"
        Me.Btn_AddRole.Size = New System.Drawing.Size(198, 41)
        Me.Btn_AddRole.TabIndex = 0
        Me.Btn_AddRole.Text = "Add Role"
        Me.Btn_AddRole.UseVisualStyleBackColor = True
        '
        'TB_RoleName
        '
        Me.TB_RoleName.Location = New System.Drawing.Point(107, 12)
        Me.TB_RoleName.Name = "TB_RoleName"
        Me.TB_RoleName.Size = New System.Drawing.Size(241, 22)
        Me.TB_RoleName.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Role Name"
        '
        'Ckb_MasterFiles
        '
        Me.Ckb_MasterFiles.AutoSize = True
        Me.Ckb_MasterFiles.Location = New System.Drawing.Point(16, 49)
        Me.Ckb_MasterFiles.Name = "Ckb_MasterFiles"
        Me.Ckb_MasterFiles.Size = New System.Drawing.Size(150, 21)
        Me.Ckb_MasterFiles.TabIndex = 4
        Me.Ckb_MasterFiles.Text = "Master Files Group"
        Me.Ckb_MasterFiles.UseVisualStyleBackColor = True
        '
        'Ckb_Suppliers
        '
        Me.Ckb_Suppliers.AutoSize = True
        Me.Ckb_Suppliers.Location = New System.Drawing.Point(16, 76)
        Me.Ckb_Suppliers.Name = "Ckb_Suppliers"
        Me.Ckb_Suppliers.Size = New System.Drawing.Size(133, 21)
        Me.Ckb_Suppliers.TabIndex = 5
        Me.Ckb_Suppliers.Text = "Suppliers Group"
        Me.Ckb_Suppliers.UseVisualStyleBackColor = True
        '
        'Ckb_Approvals
        '
        Me.Ckb_Approvals.AutoSize = True
        Me.Ckb_Approvals.Location = New System.Drawing.Point(16, 103)
        Me.Ckb_Approvals.Name = "Ckb_Approvals"
        Me.Ckb_Approvals.Size = New System.Drawing.Size(137, 21)
        Me.Ckb_Approvals.TabIndex = 6
        Me.Ckb_Approvals.Text = "Approvals Group"
        Me.Ckb_Approvals.UseVisualStyleBackColor = True
        '
        'Ckb_Intelligence
        '
        Me.Ckb_Intelligence.AutoSize = True
        Me.Ckb_Intelligence.Location = New System.Drawing.Point(16, 130)
        Me.Ckb_Intelligence.Name = "Ckb_Intelligence"
        Me.Ckb_Intelligence.Size = New System.Drawing.Size(145, 21)
        Me.Ckb_Intelligence.TabIndex = 7
        Me.Ckb_Intelligence.Text = "Intelligence Group"
        Me.Ckb_Intelligence.UseVisualStyleBackColor = True
        '
        'Ckb_AdditionalTools
        '
        Me.Ckb_AdditionalTools.AutoSize = True
        Me.Ckb_AdditionalTools.Location = New System.Drawing.Point(16, 184)
        Me.Ckb_AdditionalTools.Name = "Ckb_AdditionalTools"
        Me.Ckb_AdditionalTools.Size = New System.Drawing.Size(175, 21)
        Me.Ckb_AdditionalTools.TabIndex = 8
        Me.Ckb_AdditionalTools.Text = "Additional Tools Group"
        Me.Ckb_AdditionalTools.UseVisualStyleBackColor = True
        '
        'Ckb_Accounting
        '
        Me.Ckb_Accounting.AutoSize = True
        Me.Ckb_Accounting.Location = New System.Drawing.Point(16, 157)
        Me.Ckb_Accounting.Name = "Ckb_Accounting"
        Me.Ckb_Accounting.Size = New System.Drawing.Size(144, 21)
        Me.Ckb_Accounting.TabIndex = 9
        Me.Ckb_Accounting.Text = "Accounting Group"
        Me.Ckb_Accounting.UseVisualStyleBackColor = True
        '
        'Lbl_GrgID
        '
        Me.Lbl_GrgID.AutoSize = True
        Me.Lbl_GrgID.Location = New System.Drawing.Point(304, 248)
        Me.Lbl_GrgID.Name = "Lbl_GrgID"
        Me.Lbl_GrgID.Size = New System.Drawing.Size(16, 17)
        Me.Lbl_GrgID.TabIndex = 10
        Me.Lbl_GrgID.Text = "0"
        Me.Lbl_GrgID.Visible = False
        '
        'lbl_RoleID
        '
        Me.lbl_RoleID.AutoSize = True
        Me.lbl_RoleID.Location = New System.Drawing.Point(307, 228)
        Me.lbl_RoleID.Name = "lbl_RoleID"
        Me.lbl_RoleID.Size = New System.Drawing.Size(16, 17)
        Me.lbl_RoleID.TabIndex = 11
        Me.lbl_RoleID.Text = "0"
        Me.lbl_RoleID.Visible = False
        '
        'Ckb_isGroupAdmin
        '
        Me.Ckb_isGroupAdmin.AutoSize = True
        Me.Ckb_isGroupAdmin.Location = New System.Drawing.Point(16, 228)
        Me.Ckb_isGroupAdmin.Name = "Ckb_isGroupAdmin"
        Me.Ckb_isGroupAdmin.Size = New System.Drawing.Size(190, 21)
        Me.Ckb_isGroupAdmin.TabIndex = 12
        Me.Ckb_isGroupAdmin.Text = "Group Administrator Role"
        Me.Ckb_isGroupAdmin.UseVisualStyleBackColor = True
        '
        'Frm_AddRole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 340)
        Me.Controls.Add(Me.Ckb_isGroupAdmin)
        Me.Controls.Add(Me.lbl_RoleID)
        Me.Controls.Add(Me.Lbl_GrgID)
        Me.Controls.Add(Me.Ckb_Accounting)
        Me.Controls.Add(Me.Ckb_AdditionalTools)
        Me.Controls.Add(Me.Ckb_Intelligence)
        Me.Controls.Add(Me.Ckb_Approvals)
        Me.Controls.Add(Me.Ckb_Suppliers)
        Me.Controls.Add(Me.Ckb_MasterFiles)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TB_RoleName)
        Me.Controls.Add(Me.Btn_AddRole)
        Me.Name = "Frm_AddRole"
        Me.Text = "Add Role"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_AddRole As System.Windows.Forms.Button
    Friend WithEvents TB_RoleName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Ckb_MasterFiles As System.Windows.Forms.CheckBox
    Friend WithEvents Ckb_Suppliers As System.Windows.Forms.CheckBox
    Friend WithEvents Ckb_Approvals As System.Windows.Forms.CheckBox
    Friend WithEvents Ckb_Intelligence As System.Windows.Forms.CheckBox
    Friend WithEvents Ckb_AdditionalTools As System.Windows.Forms.CheckBox
    Friend WithEvents Ckb_Accounting As System.Windows.Forms.CheckBox
    Friend WithEvents Lbl_GrgID As System.Windows.Forms.Label
    Friend WithEvents lbl_RoleID As System.Windows.Forms.Label
    Friend WithEvents Ckb_isGroupAdmin As System.Windows.Forms.CheckBox
End Class
