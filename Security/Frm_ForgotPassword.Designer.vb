﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_ForgotPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_ForgotPassword))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tb_Email = New System.Windows.Forms.TextBox()
        Me.Btn_Validate = New System.Windows.Forms.Button()
        Me.Btn_Email = New System.Windows.Forms.Button()
        Me.lbl_TenantID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Email Address"
        '
        'tb_Email
        '
        Me.tb_Email.Location = New System.Drawing.Point(118, 13)
        Me.tb_Email.Name = "tb_Email"
        Me.tb_Email.Size = New System.Drawing.Size(316, 22)
        Me.tb_Email.TabIndex = 1
        '
        'Btn_Validate
        '
        Me.Btn_Validate.Location = New System.Drawing.Point(16, 47)
        Me.Btn_Validate.Name = "Btn_Validate"
        Me.Btn_Validate.Size = New System.Drawing.Size(159, 32)
        Me.Btn_Validate.TabIndex = 2
        Me.Btn_Validate.Text = "Validate"
        Me.Btn_Validate.UseVisualStyleBackColor = True
        '
        'Btn_Email
        '
        Me.Btn_Email.Location = New System.Drawing.Point(275, 47)
        Me.Btn_Email.Name = "Btn_Email"
        Me.Btn_Email.Size = New System.Drawing.Size(159, 32)
        Me.Btn_Email.TabIndex = 3
        Me.Btn_Email.Text = "e-Mail New Password"
        Me.Btn_Email.UseVisualStyleBackColor = True
        Me.Btn_Email.Visible = False
        '
        'lbl_TenantID
        '
        Me.lbl_TenantID.AutoSize = True
        Me.lbl_TenantID.Location = New System.Drawing.Point(181, 55)
        Me.lbl_TenantID.Name = "lbl_TenantID"
        Me.lbl_TenantID.Size = New System.Drawing.Size(16, 17)
        Me.lbl_TenantID.TabIndex = 4
        Me.lbl_TenantID.Text = "0"
        '
        'Frm_ForgotPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(446, 91)
        Me.Controls.Add(Me.lbl_TenantID)
        Me.Controls.Add(Me.Btn_Email)
        Me.Controls.Add(Me.Btn_Validate)
        Me.Controls.Add(Me.tb_Email)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Frm_ForgotPassword"
        Me.Text = "Reset Password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tb_Email As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Validate As System.Windows.Forms.Button
    Friend WithEvents Btn_Email As System.Windows.Forms.Button
    Friend WithEvents lbl_TenantID As System.Windows.Forms.Label
End Class
