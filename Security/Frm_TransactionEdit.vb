﻿Imports System.Windows.Forms

Public Class Frm_TransactionEdit
#Region "Variables"
    Dim ExtractedData As New DataExtractsClass
#End Region
    Private Sub Btn_Load_Click(sender As Object, e As EventArgs) Handles Btn_Load.Click
        TxBinding.DataSource = ExtractedData.GetAccountTransactionsByTxID(TB_TxID.Text)
        DGV_Tx.Refresh()
        SumRequests()
    End Sub

    Private Sub Save_Click(sender As Object, e As EventArgs) Handles Save.Click
        Dim ConfirmMove As DialogResult = MsgBox("Are you sure you want to save this transaction", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If ConfirmMove = Windows.Forms.DialogResult.Yes Then
            TxBinding.EndEdit()
            ExtractedData.SaveChanges()
            MsgBox("Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxBinding.DataSource = Nothing
            UpdateNZButton()
        End If
    End Sub
    Function SumRequests()
        Dim TRequests As Double = 0
        Dim row As DataGridViewRow
        For Each row In DGV_Tx.Rows
            If row.Cells("DRCRDataGridViewTextBoxColumn").Value = "Cr" Then
                TRequests = TRequests - row.Cells("AmountDataGridViewTextBoxColumn").Value
            Else
                TRequests = TRequests + row.Cells("AmountDataGridViewTextBoxColumn").Value
            End If
        Next
        TB_Total.Text = TRequests.ToString("c")
    End Function
    Private Sub DGV_Tx_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_Tx.CellEndEdit
        SumRequests()
    End Sub

    Private Sub Btn_NonBal_Click(sender As Object, e As EventArgs) Handles Btn_NonBal.Click
        TB_TxID.Text = NonZeroTransaction()
    End Sub
    Function UpdateNZButton()
        Btn_NonBal.Text = "Non Zero Transaction (" & NonZeroTransactionCount() & ")"
    End Function

    Private Sub Frm_TransactionEdit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        UpdateNZButton()
    End Sub
End Class