﻿Public Class Frm_ForgotPassword

    Private Sub Btn_Validate_Click(sender As Object, e As EventArgs) Handles Btn_Validate.Click
        If Len(ValidateAddress(tb_Email.Text)) > 0 Then
            Btn_Email.Visible = True
            lbl_TenantID.Text = ValidateAddress(tb_Email.Text)
        End If
    End Sub

    Private Sub Btn_Email_Click(sender As Object, e As EventArgs) Handles Btn_Email.Click
        Call NewPassword(lbl_TenantID.Text, tb_Email.Text)
        Me.Close()
    End Sub

    Private Sub Frm_ForgotPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class