﻿Imports System.Data
Imports System.Drawing
Imports System.Windows.Forms
Imports Microsoft.Win32

Public Class Frm_UserLogon
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        LogIntoSystem()
    End Sub

    Private Sub TB_Password_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles TB_Password.KeyDown
        If e.KeyCode = 13 Then LogIntoSystem()
    End Sub
    Function LogIntoSystem()
        lbl_Status.Text = "User login ...."

        Application.DoEvents()
        Globals.Ribbons.Ribbon1.DD_Group.Items.Clear()
        Dim DSGroups As New DataTable
        lbl_Status.Text = "Checking user credentials ...."
        Globals.Ribbons.Ribbon1.LoggedUsersDataSet = ValidateLogon(TB_Username.Text, TB_Password.Text).Tables(0)
        If Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows.Count = 0 Then
            MsgBox("Could not login due to invalid login credentials")
            Exit Function
        End If
        My.Settings.Username = Me.TB_Username.Text
        My.Settings.UserID = Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows(0).Item("TenantID")
        My.Settings.Password = TB_Password.Text

        If Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows(0).Item("ResetPassword") = "True" Then
            Dim frmReset As New Frm_ResetPassword
            frmReset.lbl_TenantID.Text = Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows(0).Item("TenantID")
            frmReset.ShowDialog()
            Exit Function
        End If


        lbl_Status.Text = "Check Global Admin ...."
        Application.DoEvents()
        '***********   Set For Global Administrator ********
        If Not IsDBNull(Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows(0).Item("IsGlobalAdmin")) Then
            If Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows(0).Item("IsGlobalAdmin") = True Then
                Globals.Ribbons.Ribbon1.GlobalAdmin = True
                Globals.Ribbons.Ribbon1.grpAdmin.Visible = True
                Globals.Ribbons.Ribbon1.GrpOrderX.Visible = True
            End If
        End If
        '*****************************************************
        Globals.Ribbons.Ribbon1.CurrentTenantID = Globals.Ribbons.Ribbon1.LoggedUsersDataSet.Rows(0).Item("TenantID")
        Globals.Ribbons.Ribbon1.btn_ChangeCo.Visible = True
        lbl_Status.Text = "Load Groups ...."
        Application.DoEvents()
        DSGroups = Globals.Ribbons.Ribbon1.LoggedUsersDataSet.DefaultView.ToTable(True, "GroupID", "GroupName", "LastGroupID")
        Dim dvg As DataView
        dvg = DSGroups.DefaultView
        dvg.Sort = "GroupName ASC"
        DSGroups = dvg.ToTable

        If DSGroups.Rows.Count = 0 Then
            MsgBox("This user has not been enabled for any groups")
            Exit Function
        End If
        Globals.Ribbons.Ribbon1.DD_Group.Visible = True

        For Each row As DataRow In DSGroups.Rows
            If Not IsDBNull(row("GroupName")) Then
                Dim DDitem = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem()
                DDitem.Label = row("GroupName")
                DDitem.Tag = row("GroupID")
                Globals.Ribbons.Ribbon1.DD_Group.Items.Add(DDitem)
            End If
        Next
        Globals.Ribbons.Ribbon1.DD_Companies.Visible = True

        If Globals.Ribbons.Ribbon1.DD_Group.Items.Count = 0 Then
            MsgBox("User is not mapped to any groups!" & vbNewLine & "Please contact your group administrator")
            Exit Function
        End If
        Globals.Ribbons.Ribbon1.ShouldLoadGroups = True
        Globals.Ribbons.Ribbon1.Group_Home.Visible = True
        Globals.Ribbons.Ribbon1.DD_Group.SelectedItemIndex = 0
        Globals.Ribbons.Ribbon1.btnOnOff.Label = "Active"
        '  Globals.Ribbons.Ribbon1.btn_LogOut.Visible = True
        Me.Close()
    End Function

    Private Sub LinkLabel1_Click(sender As Object, e As EventArgs) Handles LinkLabel1.Click
        Dim Frm_Reset As New Frm_ForgotPassword
        Frm_Reset.Show()
    End Sub

    Private Sub Frm_UserLogon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim img As Image = My.Resources.LogPic
        Dim bm As Bitmap = img
        Dim hIcon As IntPtr = bm.GetHicon
        Dim TheIcon As Icon = Icon.FromHandle(hIcon)
        Me.Icon = TheIcon

        Dim regStartUP As RegistryKey
        Dim LastUser As String
        Dim LastPass As String

        regStartUP = Registry.CurrentUser.OpenSubKey("SOFTWARE\\MagicBox\\StartUp", True)
        If regStartUP Is Nothing Then
            ' Key doesn't exist; create it.
            regStartUP = Registry.CurrentUser.CreateSubKey("SOFTWARE\\MagicBox\\StartUp")
        End If
        LastUser = regStartUP.GetValue("CurrentUser")
        LastPass = regStartUP.GetValue("CurrentPass")
        regStartUP.Close()
        TB_Username.Text = LastUser
        TB_Password.Text = LastPass

    End Sub
    Function LoadCompanySettings()
        Try
            lbl_Status.Text = "Loading Company Settings...."
            Dim dsStoreSettings As New DataTable
            If IsNothing(dsStoreSettings) Or dsStoreSettings.Rows.Count = 0 Then
                MsgBox("There are no company settings loaded on server, please update and then continue")
            Else
                For Each row As DataRow In dsStoreSettings.Rows
                    My.Settings.Setting_Year = GetFinYear(Now.ToString)
                    My.Settings.DBType = row("DBType")
                    My.Settings.Load_Checks = row("Load_Checks")
                    My.Settings.ID_Checks = row("ID_Checks")
                    My.Settings.Auto_Approve_CreditNotes = row("Auto_Approve_CreditNotes")
                    My.Settings.Auto_Approve_EFT = row("Auto_Approve_EFT")
                    My.Settings.EmailAll = row("EmailAll")
                    My.Settings.EmailDefault = row("EmailDefault")
                    My.Settings.PayInAdvance = row("PayInAdvance")
                    My.Settings.FutureInvoiceEnabled = row("FutureInvoiceEnabled")
                    My.Settings.Save()
                Next
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Me.Close()
    End Sub
End Class