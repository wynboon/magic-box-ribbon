﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_ResetPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TB_OldP = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_NewP = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_ConfirmP = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Reset = New System.Windows.Forms.Button()
        Me.lbl_TenantID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TB_OldP
        '
        Me.TB_OldP.Location = New System.Drawing.Point(139, 35)
        Me.TB_OldP.Name = "TB_OldP"
        Me.TB_OldP.Size = New System.Drawing.Size(206, 22)
        Me.TB_OldP.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Old Password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "New Password"
        '
        'TB_NewP
        '
        Me.TB_NewP.Location = New System.Drawing.Point(139, 63)
        Me.TB_NewP.Name = "TB_NewP"
        Me.TB_NewP.Size = New System.Drawing.Size(206, 22)
        Me.TB_NewP.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 17)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Confirm Password"
        '
        'TB_ConfirmP
        '
        Me.TB_ConfirmP.Location = New System.Drawing.Point(139, 91)
        Me.TB_ConfirmP.Name = "TB_ConfirmP"
        Me.TB_ConfirmP.Size = New System.Drawing.Size(206, 22)
        Me.TB_ConfirmP.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 25)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Label4"
        '
        'Btn_Reset
        '
        Me.Btn_Reset.Location = New System.Drawing.Point(15, 117)
        Me.Btn_Reset.Name = "Btn_Reset"
        Me.Btn_Reset.Size = New System.Drawing.Size(330, 41)
        Me.Btn_Reset.TabIndex = 7
        Me.Btn_Reset.Text = "Reset"
        Me.Btn_Reset.UseVisualStyleBackColor = True
        '
        'lbl_TenantID
        '
        Me.lbl_TenantID.AutoSize = True
        Me.lbl_TenantID.Location = New System.Drawing.Point(293, 13)
        Me.lbl_TenantID.Name = "lbl_TenantID"
        Me.lbl_TenantID.Size = New System.Drawing.Size(51, 17)
        Me.lbl_TenantID.TabIndex = 8
        Me.lbl_TenantID.Text = "Label5"
        Me.lbl_TenantID.Visible = False
        '
        'Frm_ResetPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 170)
        Me.Controls.Add(Me.lbl_TenantID)
        Me.Controls.Add(Me.Btn_Reset)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TB_ConfirmP)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TB_NewP)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TB_OldP)
        Me.Name = "Frm_ResetPassword"
        Me.Text = "Reset Password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TB_OldP As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB_NewP As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_ConfirmP As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Btn_Reset As System.Windows.Forms.Button
    Friend WithEvents lbl_TenantID As System.Windows.Forms.Label
End Class
