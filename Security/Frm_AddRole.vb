﻿Imports System.Data.SqlClient

Public Class Frm_AddRole

    Private Sub Frm_AddRole_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If lbl_RoleID.Text <> 0 Then
            'Load the role details
            LoadRole(lbl_RoleID.Text)
        End If
    End Sub
    Private Sub Btn_AddRole_Click(sender As Object, e As EventArgs) Handles Btn_AddRole.Click
        If lbl_RoleID.Text = 0 Then
            Call InsertRole()
        Else
            Call EditRole()
        End If
    End Sub
    Function InsertRole()
        Try
            'Do validations

            If TB_RoleName.Text = "" Then
                MsgBox("Role name may not be empty")
                Exit Function
            End If
            If ValidateRoleName(TB_RoleName.Text, Lbl_GrgID.Text) = False Then
                MsgBox("There is already a role that includes this name for the group")
                Exit Function
            End If


            Dim strSQL As String = ""
            strSQL = "Insert Into [Roles_Header] ([RoleName],[GroupID],[grpMasterFiles],[grpSuppliers],[grpApprovals],[grpIntelligence]" & _
                    ",[grpAccounting],[grpTools],[RoleCreateDate],[RoleCreatedBy],[RoleModifedDate],[RoleModifiedBy],[isGroupAdmin]) Values (" & _
                    "'" & TB_RoleName.Text & "'," & _
                    "'" & Lbl_GrgID.Text & "'," & _
                    "'" & Ckb_MasterFiles.CheckState & "'," & _
                    "'" & Ckb_Suppliers.CheckState & "'," & _
                    "'" & Ckb_Approvals.CheckState & "'," & _
                    "'" & Ckb_Intelligence.CheckState & "'," & _
                    "'" & Ckb_Accounting.CheckState & "'," & _
                    "'" & Ckb_AdditionalTools.CheckState & "'," & _
                    "'" & Now.ToString("dd MMM yyyy") & "'," & _
                    "'" & My.Settings.FirstName & "'," & _
                   "'" & Now.ToString("dd MMM yyyy") & "'," & _
                    "'" & My.Settings.FirstName & "'," & _
                    "'" & Ckb_isGroupAdmin.Checked & "')"

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            MsgBox("New role " & TB_RoleName.Text & " has been created!")
            connection.Close()
            Me.Close()
        Catch ex As Exception
            MsgBox("There was an error: " & ex.Message)
        End Try
    End Function
    Function EditRole()
        Try
            'Do validations

            If TB_RoleName.Text = "" Then
                MsgBox("Role name may not be empty")
                Exit Function
            End If


            Dim strSQL As String = ""
            strSQL = "Update [Roles_Header] Set [RoleName] = '" & TB_RoleName.Text & "',[grpMasterFiles] = '" & Ckb_MasterFiles.CheckState & "'," & _
                     "[grpSuppliers] = '" & Ckb_Suppliers.CheckState & "',[grpApprovals] = '" & Ckb_Approvals.CheckState & "'," & _
                     "[grpIntelligence] = '" & Ckb_Intelligence.CheckState & "',[grpAccounting] = '" & Ckb_Accounting.CheckState & "'," & _
                     "[grpTools] = '" & Ckb_AdditionalTools.CheckState & "',[RoleModifedDate] = " & Now.ToString("DD MMM YYYY") & "'," & _
                     "[RoleModifiedBy] = " & Globals.Ribbons.Ribbon1.CurrentTenantID & ",[isGroupAdmin] = '" & Ckb_isGroupAdmin.Checked & "' " & _
                     "Where RoleID = " & lbl_RoleID.Text

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(strSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteScalar()
            MsgBox("Role " & TB_RoleName.Text & " has been updated!")
            connection.Close()
            Me.Close()
        Catch ex As Exception
            MsgBox("There was an error: " & ex.Message)
        End Try
    End Function
    Function LoadRole(RoleID As Integer)
        Dim SecureEnc As New PasswordSecurity
        Try
            Dim sSQL As String

            sSQL = "SELECT Top 1 * FROM Roles_Header WHERE RoleID = " & RoleID

            Dim connection As New SqlConnection(MagicBox_CommonDB & ";Connect Timeout=180")

            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            While datareader.Read
                If Not datareader("RoleName").Equals(DBNull.Value) Then
                    TB_RoleName.Text = datareader("RoleName")
                Else
                    TB_RoleName.Text = ""
                End If
                If Not datareader("grpMasterFiles").Equals(DBNull.Value) Then
                    Ckb_MasterFiles.Checked = datareader("grpMasterFiles")
                Else
                    Ckb_MasterFiles.Text = False
                End If
                If Not datareader("grpSuppliers").Equals(DBNull.Value) Then
                    Ckb_Suppliers.Checked = datareader("grpSuppliers")
                Else
                    Ckb_Suppliers.Text = False
                End If
                If Not datareader("grpApprovals").Equals(DBNull.Value) Then
                    Ckb_Approvals.Checked = datareader("grpApprovals")
                Else
                    Ckb_Approvals.Text = False
                End If
                If Not datareader("grpIntelligence").Equals(DBNull.Value) Then
                    Ckb_Intelligence.Checked = datareader("grpIntelligence")
                Else
                    Ckb_Intelligence.Text = False
                End If
                If Not datareader("grpAccounting").Equals(DBNull.Value) Then
                    Ckb_Accounting.Checked = datareader("grpAccounting")
                Else
                    Ckb_Accounting.Text = False
                End If
                If Not datareader("grpTools").Equals(DBNull.Value) Then
                    Ckb_AdditionalTools.Checked = datareader("grpTools")
                Else
                    Ckb_AdditionalTools.Text = False
                End If
                If Not datareader("isGroupAdmin").Equals(DBNull.Value) Then
                    Ckb_isGroupAdmin.Checked = datareader("isGroupAdmin")
                Else
                    Ckb_isGroupAdmin.Text = False
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
End Class