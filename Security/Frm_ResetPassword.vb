﻿Imports System.Data

Public Class Frm_ResetPassword
    Dim DSTenant As New DataSet
    Private Sub Btn_Reset_Click(sender As Object, e As EventArgs) Handles Btn_Reset.Click
        Dim SecPas As New PasswordSecurity
        Dim OldPassword As String
        Dim NewPassword As String
        NewPassword = SecPas.EncryptString(TB_NewP.Text)

        '1 Are both passwords the same
        If TB_ConfirmP.Text <> TB_NewP.Text Then
            MsgBox("Passwords do not match")
        End If

        '2. Check Old Pass


        OldPassword = DSTenant.Tables(0).Rows(0).Item("Password")

        If OldPassword <> SecPas.EncryptString(TB_OldP.Text) Then
            MsgBox("Old password does not match")
        End If

        '3 Reset
        If ResetPassword(lbl_TenantID.Text, NewPassword) = True Then
            MsgBox("reset")
        End If
    End Sub

    Private Sub Frm_ResetPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DSTenant = GetTenantINFO(lbl_TenantID.Text)
        Label4.Text = DSTenant.Tables(0).Rows(0).Item("FirstName")
    End Sub
End Class