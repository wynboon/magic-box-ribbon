﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D


Public Class frmInvoiceEdit
#Region "Variables"
    Dim MBData As New DataExtractsClass
    Dim xSQLAdapter_Accounts As SqlDataAdapter
    Dim xSQLTable_Accounts As New DataTable
    Dim EditClickinProgress As Boolean
    Dim TaxPerc As Double
    Dim TaxCalc As Double
#End Region
    Public Sub oFormLoad()
        Try
            If My.Settings.Setting_CompanyID <> "" Then
                CB_SupplierName.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                CB_SupplierName.DisplayMember = "SupplierName"
                CB_SupplierName.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
            CB_Tax_Edit.SelectedIndex = 0
            If LoadTax() = False Then
                MsgBox("Error loading TaxTypes, please resolve before trying to enter invoices.", MsgBoxStyle.Critical, "Tax Types")
                Me.Close()
                Exit Sub
            End If
            Call LoadInvoice()
            Call AddTotals()
            Call Fill_DGV_Accounts()
            Call Fill_Category1_Combobox()
            DGV_InvLines.ClearSelection()
            DGV_InvLines.Rows(0).Selected = True
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Function LoadInvoice()
        Dim TempTable As New DataTable
        Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Dim VATAcc As String = Get_VATControlAcc()
        Dim CredAcc As String = Get_CreditorControlAcc()

        'Sql Statement
        Dim strSQL As String = "Select Case When Dr_Cr = 'Dr' then Amount else Amount *-1 End as CalcAmount,* " & _
                               "From Transactions Left Join Accounting on Accounting.Co_ID = Transactions.Co_ID and Accounting.[Segment 4] = Transactions.[Description Code] " & _
                               "Where TransactionID = " & Lbl_OrigTxID.Text & " and [Description Code] Not In ('" & VATAcc & "','" & CredAcc & "')"

        Dim adpBooks As New SqlDataAdapter(strSQL, conn)
        adpBooks.Fill(TempTable)
        Dim Desc As String
        Dim Seg1 As String
        Dim Seg2 As String
        Dim Seg3 As String
        Dim Amount As Double
        Dim TaxType As String
        Dim TaxAmount As Double
        Dim LineIncl As Double
        Dim SupplierID As Long
        DGV_InvLines.Rows.Clear()
        Dim dtDataTable As DataTable = TempTable
        Dim Count As Long
        Count = 0
        For Each row As DataRow In dtDataTable.Rows
            SupplierID = row("SupplierID")
            Desc = row("Description")
            Seg1 = row("Segment 1 Desc")
            Seg2 = row("Segment 2 Desc")
            Seg3 = row("Segment 3 Desc")
            Amount = row("CalcAmount")
            TaxType = row("TaxType")
            TaxAmount = row("Tax Amount")
            LineIncl = Amount + TaxAmount

            DGV_InvLines.Rows.Add(Count + 1, Desc, Seg1, Seg2, Seg3, Amount, TaxType, TaxAmount, LineIncl)
            Count = Count + 1
        Next row
        Lbl_SupplierID.Text = SupplierID
    End Function
    Private Sub CB_SupplierName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_SupplierName.SelectedIndexChanged
        Lbl_SupplierID.Text = CB_SupplierName.SelectedValue.ToString
    End Sub

    Private Sub DGV_InvLines_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_InvLines.CellClick
        EditClickinProgress = True
        Lbl_RowIndex.Text = e.RowIndex
        TB_Desc_Edit.Text = DGV_InvLines.Item("Description", e.RowIndex).Value
        CB_Seg1_Edit.Text = DGV_InvLines.Item("Segment1", e.RowIndex).Value
        CB_Seg2_Edit.Text = DGV_InvLines.Item("Segment2", e.RowIndex).Value
        CB_Seg3_Edit.Text = DGV_InvLines.Item("Segment3", e.RowIndex).Value
        TB_AmountExcl_Edit.Text = DGV_InvLines.Item("Amount", e.RowIndex).Value
        If DGV_InvLines.Item("TaxType", e.RowIndex).Value = 1 Then
            CB_Tax_Edit.SelectedIndex = 0
        Else
            CB_Tax_Edit.SelectedValue = DGV_InvLines.Item("TaxType", e.RowIndex).Value
        End If
        TB_TaxAmount.Text = DGV_InvLines.Item("TaxAmount", e.RowIndex).Value
        TB_AmountIncl_Edit.Text = DGV_InvLines.Item("Total", e.RowIndex).Value
        EditClickinProgress = False
    End Sub
    Private Sub DGV_InvLines_SelectionChanged(sender As Object, e As EventArgs) Handles DGV_InvLines.SelectionChanged
        Try
            If IsNothing(DGV_InvLines.CurrentRow) = True Then Exit Sub
            EditClickinProgress = True
            Dim RwIndex As Integer
            RwIndex = DGV_InvLines.CurrentRow.Index
            Lbl_RowIndex.Text = RwIndex
            TB_Desc_Edit.Text = DGV_InvLines.Item("Description", RwIndex).Value
            CB_Seg1_Edit.Text = DGV_InvLines.Item("Segment1", RwIndex).Value
            CB_Seg2_Edit.Text = DGV_InvLines.Item("Segment2", RwIndex).Value
            CB_Seg3_Edit.Text = DGV_InvLines.Item("Segment3", RwIndex).Value
            TB_AmountExcl_Edit.Text = DGV_InvLines.Item("Amount", RwIndex).Value
            If DGV_InvLines.Item("TaxType", RwIndex).Value = 1 Then
                CB_Tax_Edit.SelectedIndex = 0
            Else
                CB_Tax_Edit.SelectedValue = DGV_InvLines.Item("TaxType", RwIndex).Value
            End If
            TB_TaxAmount.Text = DGV_InvLines.Item("TaxAmount", RwIndex).Value
            TB_AmountIncl_Edit.Text = DGV_InvLines.Item("Total", RwIndex).Value
            EditClickinProgress = False
        Catch ex As Exception
            MsgBox("Error, the error was: " & ex.Message, MsgBoxStyle.Critical, "Selection Error")
        End Try
    End Sub
    Function AddTotals()
        Dim sum As Double = 0
        Dim TotalExcl As Double
        Dim TotalIncl As Double
        Dim TotalVAT As Double

        For i = 0 To DGV_InvLines.RowCount - 1
            sum += DGV_InvLines.Rows(i).Cells("Amount").Value()
        Next
        Lbl_Total_Excl.Text = Decimal.Parse(sum).ToString("C")
        TotalExcl = sum
        sum = 0


        For i = 0 To DGV_InvLines.RowCount - 1
            sum += DGV_InvLines.Rows(i).Cells("Total").Value() - DGV_InvLines.Rows(i).Cells("Amount").Value()
        Next
        Lbl_Total_VAT.Text = Decimal.Parse(sum).ToString("C")
        TotalVAT = sum
        sum = 0

        For i = 0 To DGV_InvLines.RowCount - 1
            sum += DGV_InvLines.Rows(i).Cells("Total").Value()
        Next
        Lbl_Total_Incl.Text = Decimal.Parse(sum).ToString("C")
        TotalIncl = sum
        sum = 0



        Lbl_Total_Excl.Text = TotalExcl.ToString("C")
        Lbl_Total_VAT.Text = TotalVAT.ToString("C")
        Lbl_Total_Incl.Text = TotalIncl.ToString("C")
        DGV_InvLines.Columns("Amount").DefaultCellStyle.Format = "c"
        DGV_InvLines.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DGV_InvLines.Columns("Total").DefaultCellStyle.Format = "c"
        DGV_InvLines.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        sum = 0
    End Function

    Private Sub BT_Edit_Click(sender As Object, e As EventArgs) Handles BT_Edit.Click
        Dim isSplit As Boolean = False
        Dim CalcExcl As Double
        Dim CalcTotal As Double
        If lbl_TaxSplit.Visible = True Then
            If MsgBox("VAT Split is about to be actioned. The existing transaction will be broken into two lines in order to handle the VAT correctly." & vbNewLine & _
                      "Are you happy to continue?", MsgBoxStyle.YesNo, "VAT Split") = MsgBoxResult.Yes Then
                isSplit = True
            Else
                Exit Sub
            End If
        End If
        Dim Lng_RowIndex As Integer
        Lng_RowIndex = Lbl_RowIndex.Text
        
        If isSplit = True Then
            'Get the rand amount for the tax Value
            CalcExcl = Math.Round(TB_TaxAmount.Text / (CB_Tax_Edit.SelectedValue.ToString / 100), 2)
            DGV_InvLines.Item("Description", Lng_RowIndex).Value = TB_Desc_Edit.Text
            DGV_InvLines.Item("Segment1", Lng_RowIndex).Value = CB_Seg1_Edit.Text
            DGV_InvLines.Item("Segment2", Lng_RowIndex).Value = CB_Seg2_Edit.Text
            DGV_InvLines.Item("Segment3", Lng_RowIndex).Value = CB_Seg3_Edit.Text
            DGV_InvLines.Item("Amount", Lng_RowIndex).Value = FormatCurrency(CalcExcl)
            DGV_InvLines.Item("TaxType", Lng_RowIndex).Value = CB_Tax_Edit.SelectedValue.ToString
            DGV_InvLines.Item("TaxAmount", Lng_RowIndex).Value = FormatCurrency(TB_TaxAmount.Text)
            DGV_InvLines.Item("Total", Lng_RowIndex).Value = FormatCurrency(CalcExcl + TB_TaxAmount.Text)

            CalcTotal = TB_AmountIncl_Edit.Text - (CalcExcl + TB_TaxAmount.Text)
            DGV_InvLines.Rows.Add(0, TB_Desc_Edit.Text, CB_Seg1_Edit.Text, CB_Seg2_Edit.Text, CB_Seg3_Edit.Text, _
                                  CalcTotal, 0, 0, CalcTotal)

        Else
            DGV_InvLines.Item("Description", Lng_RowIndex).Value = TB_Desc_Edit.Text
            DGV_InvLines.Item("Segment1", Lng_RowIndex).Value = CB_Seg1_Edit.Text
            DGV_InvLines.Item("Segment2", Lng_RowIndex).Value = CB_Seg2_Edit.Text
            DGV_InvLines.Item("Segment3", Lng_RowIndex).Value = CB_Seg3_Edit.Text
            DGV_InvLines.Item("Amount", Lng_RowIndex).Value = FormatCurrency(TB_AmountExcl_Edit.Text)
            DGV_InvLines.Item("TaxType", Lng_RowIndex).Value = CB_Tax_Edit.SelectedValue.ToString
            DGV_InvLines.Item("TaxAmount", Lng_RowIndex).Value = FormatCurrency(TB_TaxAmount.Text)
            DGV_InvLines.Item("Total", Lng_RowIndex).Value = FormatCurrency(TB_AmountIncl_Edit.Text)
        End If

        Call ClearFields()
        Call ResetOrder()
        Call AddTotals()
        DGV_InvLines.ClearSelection()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim isSplit As Boolean = False
        Dim CalcExcl As Double
        Dim CalcTotal As Double
        Dim NextRw As Long
        If lbl_TaxSplit.Visible = True Then
            If MsgBox("VAT Split is about to be actioned. The existing transaction will be broken into two lines in order to handle the VAT correctly." & vbNewLine & _
                      "Are you happy to continue?", MsgBoxStyle.YesNo, "VAT Split") = MsgBoxResult.Yes Then
                isSplit = True
            Else
                Exit Sub
            End If
        End If
        If lbl_TaxSplit.Visible = True Then
            'Get the rand amount for the tax Value
            NextRw = DGV_InvLines.Rows.Count + 1
            CalcExcl = Math.Round(TB_TaxAmount.Text / (CB_Tax_Edit.SelectedValue.ToString / 100), 2)
            DGV_InvLines.Rows.Add(NextRw, TB_Desc_Edit.Text, CB_Seg1_Edit.Text, CB_Seg2_Edit.Text, CB_Seg3_Edit.Text, _
                                  FormatCurrency(CalcExcl), CB_Tax_Edit.SelectedValue.ToString, _
                                  FormatCurrency(TB_TaxAmount.Text), FormatCurrency(CalcExcl + TB_TaxAmount.Text))

            CalcTotal = TB_AmountIncl_Edit.Text - (CalcExcl + TB_TaxAmount.Text)
            DGV_InvLines.Rows.Add(0, TB_Desc_Edit.Text, CB_Seg1_Edit.Text, CB_Seg2_Edit.Text, CB_Seg3_Edit.Text, _
                                  CalcTotal, 0, 0, CalcTotal)
        Else
            NextRw = DGV_InvLines.Rows.Count + 1
            DGV_InvLines.Rows.Add(NextRw, TB_Desc_Edit.Text.ToString, CB_Seg1_Edit.Text, CB_Seg2_Edit.Text, CB_Seg3_Edit.Text, _
                                  FormatCurrency(TB_AmountExcl_Edit.Text), CB_Tax_Edit.SelectedValue.ToString, FormatCurrency(TB_TaxAmount.Text), FormatCurrency(TB_AmountIncl_Edit.Text))
        End If

        Call ClearFields()
        Call ResetOrder()
        Call AddTotals()
        DGV_InvLines.ClearSelection()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        DGV_InvLines.Rows.Remove(DGV_InvLines.CurrentRow)
        Call ResetOrder()
        Call AddTotals()
        Call ClearFields()
        DGV_InvLines.ClearSelection()
    End Sub
    Function ClearFields()
        TB_AmountExcl_Edit.Text = ""
        TB_Desc_Edit.Text = ""
        TB_AmountIncl_Edit.Text = ""
        CB_Seg1_Edit.Text = ""
        CB_Seg2_Edit.Text = ""
        CB_Seg3_Edit.Text = ""

    End Function
    Function ResetOrder()
        Dim Count As Integer
        Count = 0
        For Each row As DataGridViewRow In DGV_InvLines.Rows
            Count = Count + 1
            row.Cells(0).Value = Count
        Next
    End Function
    Sub Fill_Category1_Combobox()
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If


            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader

            CB_Seg1_Edit.Items.Clear()

            While datareader.Read
                If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                    CB_Seg1_Edit.Items.Add(datareader("Segment 1 Desc"))
                End If
            End While
            connection.Close()
        Catch ex As Exception
            MsgBox(ex.Message & " 030")
        End Try
    End Sub

    Private Sub CB_Seg1_Edit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Seg1_Edit.SelectedIndexChanged
        If EditClickinProgress = False Then
            'First clear other two boxes
            CB_Seg2_Edit.Items.Clear()
            CB_Seg2_Edit.Text = ""
            CB_Seg3_Edit.Items.Clear()
            CB_Seg3_Edit.Text = ""
            'Then fill next box
        End If
        If CB_Seg1_Edit.Text = "" Then Exit Sub
        Call Fill_Category2_Combobox(CB_Seg1_Edit.Text)
    End Sub
    Sub Fill_Category2_Combobox(Category1 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Category1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                CB_Seg2_Edit.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        CB_Seg2_Edit.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'dTable.DefaultView.RowFilter = "SupplierID=12"  'or you can concatenate a variable instead of hardcoding ID

                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 1 Desc]='" & Category1 & "'"   'THIS IS ALREADY COMPANY SPECIFIC

                    'xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 1 Desc]='PURCHASES'"
                    With CB_Seg2_Edit.Items
                        .Clear()
                        For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                            If AlreadInCombo(CB_Seg2_Edit, xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 2 Desc")) = False Then
                                .Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 2 Desc"))
                            End If
                        Next
                    End With

                Else

                    'the following technique is supposed to speed up the data reader compared to the other boxes
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    With CB_Seg2_Edit.Items
                        .Clear()
                        While datareader.Read
                            .Add(datareader("Segment 2 Desc"))
                        End While
                    End With
                    connection.Close()
                End If
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 031")
        End Try
    End Sub
    Function AlreadInCombo(ByVal oComboBox As ComboBox, ByVal oText As String) As Boolean
        AlreadInCombo = False
        For i As Integer = 0 To oComboBox.Items.Count - 1
            If oComboBox.Items(i).ToString = oText Then
                AlreadInCombo = True
            End If
        Next
    End Function
    Function Fill_DGV_Accounts() As Boolean
        Try
            Dim sSQL As String

            sSQL = "Select * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Accounts.Fill(xSQLTable_Accounts)
            End If
            Fill_DGV_Accounts = True
        Catch ex As Exception
            MsgBox(ex.Message & " 011")
            Fill_DGV_Accounts = False
        End Try
    End Function

    Private Sub CB_Seg2_Edit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Seg2_Edit.SelectedIndexChanged
        If EditClickinProgress = False Then
            If CB_Seg2_Edit.Text = "" Then Exit Sub
            CB_Seg3_Edit.Text = ""
        End If
        Call Fill_Category3_Combobox(CB_Seg2_Edit.Text)
    End Sub
    Sub Fill_Category3_Combobox(Category2 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Category2) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.CB_Seg1_Edit.Text) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                CB_Seg3_Edit.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        CB_Seg3_Edit.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                    xSQLTable_Accounts.DefaultView.RowFilter = "[Segment 2 Desc]='" & Category2 & "'"


                    With Me.CB_Seg3_Edit.Items
                        .Clear()
                        For i As Integer = 0 To xSQLTable_Accounts.DefaultView.Count - 1
                            If AlreadInCombo(Me.CB_Seg3_Edit, xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 3 Desc")) = False Then
                                .Add(xSQLTable_Accounts.DefaultView.Item(i).Item("Segment 3 Desc"))
                            End If
                        Next
                    End With

                Else
                    Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    Dim cmd As New SqlCommand(sSQL, connection)
                    cmd.CommandTimeout = 300
                    connection.Open()
                    Dim datareader As SqlDataReader = cmd.ExecuteReader
                    Me.CB_Seg3_Edit.Items.Clear()
                    While datareader.Read
                        If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                            Me.CB_Seg3_Edit.Items.Add(datareader("Segment 3 Desc"))
                        End If
                    End While
                    connection.Close()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 033")
        End Try

    End Sub
    Private Sub Btn_UpdatePost_Click(sender As Object, e As EventArgs) Handles Btn_UpdatePost.Click
        ToolStripStatusLabel1.Text = "Updating Please Wait......"
        Dim connetionString As String
        Dim connection As SqlConnection

        Dim adapter As New SqlDataAdapter
        Dim ds As New DataSet
        Dim i As Integer
        Dim sql As String
        Dim TransactionID As Integer
        Dim BatchID As Integer
        Dim Period As Integer
        Dim FinYear As Integer
        Dim transaction As SqlTransaction
        Dim SupplierID As Integer
        Dim TaxType As Integer
        Dim HasTax As Boolean
        Dim DrAccount As String
        Dim PostAccount As String
        Dim LineExcl As Double
        Dim LineIncl As Double
        Dim TaxAmount As Double
        Dim Sign As String
        Dim TxSum As Double = 0

        Dim TaxAccount As String
        Dim TaxAccountSeg1 As String
        Dim TaxAccountSeg2 As String
        Dim TaxAccountSeg3 As String

        Try
            If Lbl_OrigTxID.Text = "" Then
                MsgBox("Original Invoice not Found!, Please close and try again", MsgBoxStyle.Critical, "ERROR")
                Exit Sub
            End If
            If Lbl_OrigTxID.Text = "0" Then
                MsgBox("Original Invoice not Found!, Please close and try again", MsgBoxStyle.Critical, "ERROR")
                Exit Sub
            End If

            Err.Number = 0
            'check that the historyHead & Lines are saved
            If TB_InvNo.Text = "" Then
                MsgBox("There is no document number associated with this document, please update the invoice number.")
                Exit Sub
            End If
            If DTP_InvoiceDate.Text = "" Then
                MsgBox("There is no Date number associated with this document, please ensure you select a date.")
                Exit Sub
            End If
            If CB_SupplierName.Text = "" Then
                MsgBox("There is no Supplier associated with this document, please ensure you select a supplier.")
                Exit Sub
            End If



            HasTax = False


            'Get TAX Account Info
            TaxAccount = AccountFromType("VAT Control")
            TaxAccountSeg1 = Get_AccountInfo_from_Seg4(TaxAccount, "Segment 1 Desc")
            TaxAccountSeg2 = Get_AccountInfo_from_Seg4(TaxAccount, "Segment 2 Desc")
            TaxAccountSeg3 = Get_AccountInfo_from_Seg4(TaxAccount, "Segment 3 Desc")

            DrAccount = AccountFromType("Creditors Control")
            If DrAccount = "" Then
                MsgBox("There is no creditor control account set, please contact a Magic Box wizard." & vbNewLine & "Posting aborted!", MsgBoxStyle.Critical, "Magic Box - Debtor Control Account Not Found")
                Exit Sub
            End If
            Period = GetPeriod(DTP_InvoiceDate.Text)
            FinYear = GetFinYear(DTP_InvoiceDate.Text)
            TransactionID = oCreateTransactionID()
            ChangedTxID = TransactionID
            BatchID = oCreateBatchID()

            SupplierID = Lbl_SupplierID.Text

            connetionString = My.Settings.CS_Setting
            connection = New SqlConnection(connetionString)
            Dim command As SqlCommand = connection.CreateCommand()
            command.CommandTimeout = 900


            connection.Open()

            transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted)

            'Update original Invoice
            command.Connection = connection
            command.Transaction = transaction
            command.CommandText = "Update Transactions Set Void = 1, Info3 = 'Edited Invoice: Original TxID = " & Lbl_OrigTxID.Text & "' Where TransactionID = " & Lbl_OrigTxID.Text
            command.ExecuteNonQuery()


            'Loop through lines

            For i = 0 To DGV_InvLines.RowCount - 1
                ' Income Statment Line
                LineExcl = DGV_InvLines.Rows(i).Cells("Amount").Value()
                LineIncl = DGV_InvLines.Rows(i).Cells("Total").Value()
                TaxAmount = LineIncl - LineExcl
                TaxType = DGV_InvLines.Rows(i).Cells("TaxType").Value()
                
                If LineExcl > 0 Then
                    Sign = "Dr"
                Else
                    Sign = "Cr"
                End If

                PostAccount = Get_Segment4(DGV_InvLines.Rows(i).Cells("Segment1").Value(), DGV_InvLines.Rows(i).Cells("Segment2").Value(), DGV_InvLines.Rows(i).Cells("Segment3").Value())
                If PostAccount = "" Then
                    MsgBox("The system was not able to validate an account for :" & DGV_InvLines.Rows(i).Cells("Segment1").Value() & "/" & DGV_InvLines.Rows(i).Cells("Segment2").Value() & "/" & DGV_InvLines.Rows(i).Cells("Segment3").Value() & _
                           "- Roling back transactions and aborting edit.", MsgBoxStyle.Critical, "Validation Error")
                    transaction.Rollback()
                    Exit Sub
                    Me.Close()
                End If
                sql = TransactionLine(BatchID, TransactionID, DTP_InvoiceDate.Text, Period, FinYear _
                , TB_InvNo.Text, DGV_InvLines.Rows(i).Cells("Description").Value() _
                , LineExcl _
                , TaxType, _
                 TaxAmount, SupplierID, _
                 DGV_InvLines.Rows(i).Cells("Segment2").Value().ToString, _
                DGV_InvLines.Rows(i).Cells("Segment3").Value().ToString, _
                DGV_InvLines.Rows(i).Cells("Description").Value(), _
                DGV_InvLines.Rows(i).Cells("Description").Value(), 12, Sign, _
                PostAccount, _
                "", "", "", "")
                command.Connection = connection
                command.Transaction = transaction
                command.CommandText = sql
                command.ExecuteNonQuery()
                TxSum = TxSum + LineExcl

                'Tax Line
                If CStr(DGV_InvLines.Rows(i).Cells("TaxType").Value()) <> 0 Then

                    TaxType = DGV_InvLines.Rows(i).Cells("TaxType").Value().ToString
                    HasTax = True

                    sql = TransactionLine(BatchID, TransactionID, DTP_InvoiceDate.Text, Period, FinYear _
                    , TB_InvNo.Text, DGV_InvLines.Rows(i).Cells("Description").Value() & "-VAT" _
                    , TaxAmount, 0, 0, SupplierID, _
                    TaxAccountSeg2, TaxAccountSeg3, DGV_InvLines.Rows(i).Cells("Description").Value() & "-VAT", _
                    TaxAccountSeg3, 12, Sign, TaxAccount, "", "", "", "")
                    command.Connection = connection
                    command.Transaction = transaction
                    command.CommandText = sql
                    command.ExecuteNonQuery()
                    TxSum = TxSum + TaxAmount
                Else
                    TaxType = 0
                End If
            Next
            'Crs Control Account
            If Sign = "Cr" Then
                Sign = "Dr"
            Else
                Sign = "Cr"
            End If
            sql = TransactionLine(BatchID, TransactionID, DTP_InvoiceDate.Text, Period, FinYear, _
                  TB_InvNo.Text, TB_InvNo.Text, Lbl_Total_Incl.Text, 0, 0, _
                SupplierID, _
                "Creditors Control " & TB_InvNo.Text, _
                "Creditors Control " & TB_InvNo.Text, _
                "Creditors Control " & TB_InvNo.Text, _
                "Creditors Control " & TB_InvNo.Text, _
                12, Sign, AccountFromType("Creditors Control"), "Invoice", "Invoice", "Invoice", "Invoice")
            command.Connection = connection
            command.Transaction = transaction
            command.CommandText = sql
            command.ExecuteNonQuery()

            TxSum = TxSum - Lbl_Total_Incl.Text
            If Math.Round(TxSum, 2) <> 0 Then
                MsgBox("The transaction did not balance by " & Math.Round(TxSum, 2) & ". Please report the issue to your wizard." & _
                           " Roling back transactions and aborting edit.", MsgBoxStyle.Critical, "Validation Error")
                transaction.Rollback()
                Exit Sub
            End If

            transaction.Commit()

            Dim MessagePay As String
            If Update_PaymentLinkID(Lbl_OrigTxID.Text, TransactionID) Then
                MessagePay = " with associated payments re-linked."
            End If

            '14 May 17 - Brad decision to rather remove request rather than update 
            '  Call UpdateRequestTransactionID(Lbl_OrigTxID.Text, TransactionID)
            Call MBData.RemoveRequest(TransactionID)

            Call AddTotals()

            If Err.Number <> 0 Then
                transaction.Rollback()
            End If
            MsgBox("Invoice: " & TB_InvNo.Text & " has been updated" & MessagePay, MsgBoxStyle.Information, "Completed")
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
            Try
                transaction.Rollback()
            Catch zz As Exception
                MsgBox(zz.Message)
            End Try

        End Try
    End Sub
    Function AccountFromType(TypeName As String) As String
        Try
            Dim strSQL As String
            strSQL = "Select Top 1 [Segment 4] from [Accounting] Where Type = '" & TypeName & "' and Co_ID = " & My.Settings.Setting_CompanyID
            Dim Conn As New SqlConnection(My.Settings.CS_Setting)
            Conn.Open()
            Dim cmd As New SqlCommand(strSQL, Conn)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                AccountFromType = reader("Segment 4")
            End While
            cmd.Dispose()
            Conn.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Function
    Function TransactionLine(BatchID As Integer, TransactionID As Integer, TxDate As String, Period As Integer, FinYear As Integer _
             , Reference As String, Description As String, Amount As Double, TaxType As Integer, TaxAmount As Double _
            , SupplierID As Integer, Description2 As String, Description3 As String, Description4 As String, Description5 As String _
           , TransactionType As Integer, Dr_Cr As String, DescriptionCode As String, DocType As String, Info1 As String, Info2 As String, Info3 As String) As String
        Try

            TransactionLine = "INSERT INTO [Transactions] ([BatchID],[TransactionID],[LinkID] ,[Capture Date]" & _
           ",[Transaction Date],[PPeriod],[Fin Year],[GDC],[Reference],[Description],[AccNumber],[LinkAcc]" & _
           ",[Amount],[TaxType],[Tax Amount],[UserID],[SupplierID],[EmployeeID],[Description 2]" & _
           ",[Description 3],[Description 4],[Description 5],[Posted],[Accounting],[Void],[Transaction Type]" & _
           ",[DR_CR],[Contra Account],[Description Code],[DocType],[SupplierName],[Co_ID],[Info],[Info2]" & _
           ",[Info3]) VALUES (" & BatchID & "," & TransactionID & ",0,'" & Now.ToString("dd MMM yyyy") & "','" & _
            TxDate & "','" & Period & "','" & FinYear & "','G','" & Reference & "','" & Description & "'" & _
            ",'',''," & Math.Abs(Amount) & "," & TaxType & "," & Math.Abs(TaxAmount) & "," & My.Settings.UserID & "," & SupplierID & ",0," & _
            "'" & Description2 & "','" & Description3 & "','" & Description4 & "','" & Description5 & _
            "',1,1,0," & TransactionType & ",'" & Dr_Cr & "','','" & DescriptionCode & "'," & _
            "'" & DocType & "',' " & SQLConvert(CB_SupplierName.Text) & "'," & My.Settings.Setting_CompanyID & ",'" & Info1 & "','" & Info2 & "','" & Info3 & "')"
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Function
    Private Sub frmInvoiceEdit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LockForm(Me)
        If Lbl_OrigTxID.Text <> 0 Then
            CB_SupplierName.Text = GetSupplierName(Lbl_SupplierID.Text)
        End If
        
    End Sub
    Function LoadTax() As Boolean

        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        '  cmbVATType.Items.Clear()
        '  VATType_ComboBox.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Tax_Edit.DataSource = ds.Tables(0)
            CB_Tax_Edit.ValueMember = "Value"
            CB_Tax_Edit.DisplayMember = "Display"
            CB_Tax_Edit.SelectedIndex = 0

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Private Sub TB_AmountIncl_Edit_KeyUp(sender As Object, e As KeyEventArgs) Handles TB_AmountIncl_Edit.KeyUp
        If IsNumeric(TB_AmountExcl_Edit.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        Dim oVatType As Integer
        Dim oVatAmount As Decimal

        TaxPerc = CB_Tax_Edit.SelectedValue.ToString

        AmountIncl = TB_AmountIncl_Edit.Text
        If CStr(TaxPerc) <> 0 Then
            TaxCalc = 1 + (TaxPerc / 100)
            AmountExcl = Math.Round(AmountIncl / TaxCalc, 2)
        Else
            AmountExcl = AmountIncl
        End If
        TB_AmountExcl_Edit.Text = AmountExcl
        TB_TaxAmount.Text = AmountIncl - AmountExcl
        lbl_TaxSplit.Visible = False
    End Sub
    Private Sub CB_Tax_Edit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Tax_Edit.SelectedIndexChanged
        If IsNumeric(TB_AmountExcl_Edit.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        Dim oVatType As Integer
        Dim oVatAmount As Decimal

        TaxPerc = CB_Tax_Edit.SelectedValue.ToString

        AmountExcl = TB_AmountExcl_Edit.Text
        If CStr(TaxPerc) <> 0 Then
            TaxCalc = 1 + (TaxPerc / 100)
            AmountIncl = Math.Round(AmountExcl * TaxCalc, 2)
        Else
            AmountIncl = AmountExcl
        End If
        TB_AmountIncl_Edit.Text = AmountIncl
        TB_TaxAmount.Text = AmountIncl - AmountExcl
        lbl_TaxSplit.Visible = False
    End Sub
    Private Sub TB_AmountExcl_Edit_KeyUp(sender As Object, e As KeyEventArgs) Handles TB_AmountExcl_Edit.KeyUp
        If IsNumeric(TB_AmountExcl_Edit.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        TaxPerc = CB_Tax_Edit.SelectedValue.ToString
        AmountExcl = TB_AmountExcl_Edit.Text

        If CStr(TaxPerc) <> 0 Then
            AmountIncl = Math.Round(AmountExcl * 1.14, 2)
        Else
            TaxCalc = 1 + (TaxPerc / 100)
            AmountIncl = Math.Round(AmountExcl * TaxCalc, 2)
        End If
        TB_AmountIncl_Edit.Text = AmountIncl
        TB_TaxAmount.Text = AmountIncl - AmountExcl
        lbl_TaxSplit.Visible = False
    End Sub

    Private Sub TB_TaxAmount_KeyUp(sender As Object, e As KeyEventArgs) Handles TB_TaxAmount.KeyUp
        If IsNumeric(TB_TaxAmount.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        Dim TaxAmount As Double

        TaxPerc = CB_Tax_Edit.SelectedValue.ToString

        AmountExcl = TB_AmountExcl_Edit.Text
        AmountIncl = TB_AmountIncl_Edit.Text
        TaxAmount = AmountIncl - AmountExcl
        
        If TB_TaxAmount.Text <> TaxAmount Then
            lbl_TaxSplit.Visible = True
        End If
    End Sub
    Private Sub Lbl_Total_Incl_TextChanged(sender As Object, e As EventArgs) Handles Lbl_Total_Incl.TextChanged
        Dim OriginalAmount As Double
        Dim TotalAmount As Double
        OriginalAmount = Lbl_OriginalAmount.Text
        TotalAmount = Lbl_Total_Incl.Text
        If OriginalAmount = 0 Then
            Btn_UpdatePost.Enabled = True
            Btn_VoidInvoice.Enabled = True
            ToolStripStatusLabel1.Text = "Available to Post"
        ElseIf OriginalPaid.Text = "Unpaid Invoice" Then
            Btn_UpdatePost.Enabled = True
            Btn_VoidInvoice.Enabled = True
            ToolStripStatusLabel1.Text = "Invoice is unpaid. Available to Post"
        ElseIf OriginalAmount <> TotalAmount Then
            Btn_UpdatePost.Enabled = False
            Btn_VoidInvoice.Enabled = False
            ToolStripStatusLabel1.Text = "The current amount of the invoice does not equal the original amount posted."
        ElseIf OriginalAmount = TotalAmount Then
            Btn_UpdatePost.Enabled = True
            Btn_VoidInvoice.Enabled = False
            ToolStripStatusLabel1.Text = "The current amount of the invoice equals the original amount posted. Available to Post"
        Else
            Btn_UpdatePost.Enabled = False
            Btn_VoidInvoice.Enabled = False
            ToolStripStatusLabel1.Text = "Unhandled criteria, unavailable to post"
        End If
    End Sub

    Private Sub Btn_VoidInvoice_Click(sender As Object, e As EventArgs) Handles Btn_VoidInvoice.Click
        Try
            Dim TranID As Long = Lbl_OrigTxID.Text
            If TranID = 0 Then
                MsgBox("There was a problem with the transaction ID, please try again when there is an active transaction ID", MsgBoxStyle.Information, "No Transaction ID found")
                Exit Sub
            End If

            Dim API As New MBAPI
            If API.VoidTransaction(TranID) = True Then
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox("There was a problem with the void: " & ex.Message, MsgBoxStyle.Critical, "Void Error")
        End Try
    End Sub
End Class
