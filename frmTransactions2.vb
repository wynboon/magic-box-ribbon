﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class frmTransactions2

    Dim xAccessAdapter_Suppliers As OleDbDataAdapter
    Dim xAccessTable_Suppliers As New DataTable
    Dim xSQLAdapter_Suppliers As SqlDataAdapter
    Dim xSQLTable_Suppliers As New DataTable


    Dim dbadp As OleDbDataAdapter
    Dim dTable As New DataTable
    Dim dbadp2 As SqlDataAdapter
    Dim dTable2 As New DataTable

    Private Sub Transactions2_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Transactions 2 -> Version : " & My.Settings.Setting_Version
        Call Fill_DGV_Suppliers()
        Call GetTransactions()
        Call Fill_ComboBox_Suppliers()

    End Sub


    Sub Fill_DGV_Suppliers()
        Try
            Dim sSQL As String

            sSQL = "Select * From Suppliers"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                xAccessAdapter_Suppliers = New OleDbDataAdapter(sSQL, connection)
                xAccessAdapter_Suppliers.Fill(xAccessTable_Suppliers)
                Me.dgvSuppliers.DataSource = xAccessTable_Suppliers
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                xSQLAdapter_Suppliers = New SqlDataAdapter(sSQL, connection)
                xSQLAdapter_Suppliers.Fill(xSQLTable_Suppliers)
                Me.dgvSuppliers.DataSource = xSQLTable_Suppliers
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 012")
        End Try
    End Sub

    Sub GetTransactions()

        Try

            'First clear DGV
            DataGridView1.DataSource = Nothing


            Dim sSQL As String

            sSQL = "Select * From Transactions Where Co_ID = " & My.Settings.Setting_CompanyID & " And TransactionID = " & Me.lblTransactionID.Text

            DataGridView1.DataSource = Nothing

            If My.Settings.DBType = "Access" Then
                dTable.Clear()
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                dbadp = New OleDbDataAdapter(sSQL, connection)
                dbadp.Fill(dTable)
                Me.DataGridView1.DataSource = dTable
            ElseIf My.Settings.DBType = "SQL" Then
                dTable2.Clear()
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                dbadp2 = New SqlDataAdapter(sSQL, connection)
                dbadp2.Fill(dTable2)
                Me.DataGridView1.DataSource = dTable2
            End If


            If DataGridView1.ColumnCount > 0 Then
                Me.DataGridView1.Columns(0).Visible = False

                Me.DataGridView1.Columns(3).DefaultCellStyle.Format = "n2"
                Me.DataGridView1.Columns(4).DefaultCellStyle.Format = "n2"
                Me.DataGridView1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                Me.DataGridView1.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 319")
        End Try

    End Sub

    Sub Fill_ComboBox_Suppliers()

        Try
            Dim sSQL As String
            sSQL = "SELECT Distinct SupplierName FROM Suppliers"

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            sSQL = sSQL & " Order By SupplierName"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.ComboBox_Supplier.Items.Clear()
                Me.ComboBox_Supplier.Items.Add("")
                While datareader.Read
                    Me.ComboBox_Supplier.Items.Add(datareader("SupplierName"))
                End While
                Me.ComboBox_Supplier.SelectedIndex = 0
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.ComboBox_Supplier.Items.Clear()
                Me.ComboBox_Supplier.Items.Add("")
                While datareader.Read
                    Me.ComboBox_Supplier.Items.Add(datareader("SupplierName"))
                End While
                Me.ComboBox_Supplier.SelectedIndex = 0
                connection.Close()
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 056")

        End Try

    End Sub

    Function Check_Integrity() As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try

            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To Me.DataGridView1.RowCount - 2 'extra row
                If Me.DataGridView1.Rows(i).Cells(27).Value = "Dr" Then
                    oDebits = oDebits + CDec(Me.DataGridView1.Rows(i).Cells(13).Value)
                ElseIf Me.DataGridView1.Rows(i).Cells(27).Value = "Cr" Then
                    oCredits = oCredits + CDec(Me.DataGridView1.Rows(i).Cells(13).Value)
                End If
            Next

            If oDebits = oCredits Then
                Check_Integrity = True
            Else
                Check_Integrity = False
            End If


        Catch ex As Exception
            blnCRITICAL_ERROR = True
            MsgBox("There was an error checking integrity " & ex.Message & " 024")
        End Try
    End Function

    Private Sub btnSaveChanges_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveChanges.Click
        Try
            If Check_Integrity() = False Then
                MsgBox("Cannot Save Changes! Balance out! Please make sure Dr Amounts equal Cr Amounts")
                Exit Sub
            End If

            If My.Settings.DBType = "Access" Then
                Call oSave_Access()
            ElseIf My.Settings.DBType = "SQL" Then
                Call oSave_SQL()
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Sub oSave_Access()
        Try
            Dim builder As New OleDbCommandBuilder(dbadp)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            dbadp.Update(dTable)
            dbadp.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub
    Sub oSave_SQL()
        Try
            Dim builder As New SqlCommandBuilder(dbadp2)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            dbadp2.Update(dTable2)
            dbadp2.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        Try
            If txtUserName.Text = "s" And txtPassword.Text = "x" Then
                'If txtUserName.Text = "SuperUser" And txtPassword.Text = "zxcvbnm,./" Then
                Me.lblLoggedIn.Visible = True
                Me.btnSaveChanges.Visible = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Fill_Category1_Combobox()
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 1 Desc] FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Me.Cat1.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                        Me.Cat1.Items.Add(datareader("Segment 1 Desc"))
                    End If
                End While
                connection.Close()
            Else

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.Cat1.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 1 Desc").Equals(DBNull.Value) Then
                        Me.Cat1.Items.Add(datareader("Segment 1 Desc"))
                    End If
                End While
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 030b")
        End Try

    End Sub

    Private Sub Cat1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Cat1.SelectedIndexChanged
        If Me.Cat1.Text = "" Then Exit Sub
        'First clear other two boxes and label
        Me.Cat2.Items.Clear()
        Me.Cat2.Text = ""
        Me.Cat3.Items.Clear()
        Me.Cat3.Text = ""
        Me.lblMagicBox_Account_Segment4.Text = ""
        'Then fill next box
        Call Fill_Cat2(Me.Cat1.Text)
    End Sub

    Sub Fill_Cat2(Cat1 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 2 Desc] FROM Accounting WHERE [Segment 1 Desc] = '" & SQLConvert(Cat1) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Me.Cat2.Items.Clear()

                While datareader.Read
                    If Not datareader("Segment 2 Desc").Equals(DBNull.Value) Then
                        Me.Cat2.Items.Add(datareader("Segment 2 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                'the following technique is supposed to speed up the data reader compared to the other boxes
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                With Me.Cat2.Items
                    .Clear()
                    While datareader.Read
                        .Add(datareader("Segment 2 Desc"))
                    End While
                End With
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 032b")
        End Try

    End Sub

    Private Sub Cat2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Cat2.SelectedIndexChanged
        If Me.Cat2.Text = "" Then Exit Sub
        Me.Cat3.Text = ""
        Call Fill_Cat3(Me.Cat2.Text)
    End Sub

    Sub Fill_Cat3(Cat2 As String)
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 3 Desc] FROM Accounting WHERE [Segment 2 Desc] = '" & SQLConvert(Cat2) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.Cat1.Text) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                Me.Cat3.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.Cat3.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                Me.Cat3.Items.Clear()
                While datareader.Read
                    If Not datareader("Segment 3 Desc").Equals(DBNull.Value) Then
                        Me.Cat3.Items.Add(datareader("Segment 3 Desc"))
                    End If
                End While
                connection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 034")
        End Try

    End Sub


    Private Sub InvoiceTotal_TextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles InvoiceTotal_TextBox.TextChanged
        Try

            If IsNumeric(Me.InvoiceTotal_TextBox.Text) = True Then
                If CDec(Me.InvoiceTotal_TextBox.Text) < 0 Then
                    MsgBox("Negative numbers cannot be entered here!")
                    Me.InvoiceTotal_TextBox.Text = ""
                    Exit Sub
                End If
            End If

            If IsNumeric(InvoiceTotal_TextBox.Text) = False Then
                MsgBox("Please enter a number!")
                InvoiceTotal_TextBox.Text = ""
                Exit Sub
            Else
                Call oCalc_Numbers()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 034g")
        End Try
    End Sub

    Sub oCalc_Numbers()

        On Error Resume Next

        Dim oGrossAmount As Decimal
        Dim oVatType As Integer
        Dim oVatAmount As Decimal


        If Me.InvoiceTotal_TextBox.Text = "" Then
            'do nothing
        Else
            oGrossAmount = CDec(Me.InvoiceTotal_TextBox.Text)
            oVatType = CInt(Me.cmbVATType.Text)
            If Me.cmbVATType.Text = "" Then
                'do nothing
            ElseIf oVatType = 0 Then
                Me.txtVATAmount.Text = Format(0, "###,##0.00")
                Me.txtNonVATAmount.Text = Format(oGrossAmount, "###,##0.00")
            ElseIf oVatType = 1 Then
                oVatAmount = Format((oGrossAmount * 14 / 114), "###,##0.00")
                Me.txtVATAmount.Text = oVatAmount
                Me.txtNonVATAmount.Text = Format(oGrossAmount - oVatAmount, "###,##0.00")
            Else
                oVatAmount = Format(0, "###,##0.00")
                Me.txtVATAmount.Text = oVatAmount
                Me.txtNonVATAmount.Text = Format(oGrossAmount - oVatAmount, "###,##0.00")
            End If
        End If
    End Sub

    Private Sub ComboBox_Supplier_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox_Supplier.SelectedIndexChanged
        Call oLoad_Defaults_Supplier()
    End Sub

    Sub oLoad_Defaults_Supplier()
        Try

            Get_Defaults_for_Supplier(Me.ComboBox_Supplier.Text)
            Call Fill_Cat2(Me.Cat1.Text)
            Call Fill_Cat3(Me.Cat2.Text)

            Me.lblMagicBox_Account_Segment4.Text = Get_Segment4_MB_Account(Me.Cat3.Text)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Get_Defaults_for_Supplier(ByVal oSupplierName As String)

        '////// NOTE - THE DEFAULT VIEWS ARE DRAWM FROM TABLES THAT ARE ALREADY COMPANY-SPECIFIC SO WE DON"T NEED TO FILTER BY Co_ID '##### NEW #####

        If My.Settings.DBType = "Access" Then
            'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
            Dim oResult As String
            xAccessTable_Suppliers.DefaultView.RowFilter = "SupplierName='" & SQLConvert(oSupplierName) & "'"
            '(1) Default for Category 1 ComboBox
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Category_Default").ToString
                Me.Cat1.Text = ""
                Me.Cat1.SelectedText = oResult
            Else
                Me.Cat1.Text = ""
            End If
            '(2) Default for Category 2 ComboBox
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Detail_Default").ToString
                Me.Cat2.Text = ""
                Me.Cat2.SelectedText = oResult
            Else
                Me.Cat2.Text = ""
            End If
            '(3) Default for Category 3 ComboBox
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Detail_Default2").ToString
                Me.Cat3.Text = ""
                Me.Cat3.SelectedText = oResult
            Else
                Me.Cat3.Text = ""
            End If
            '(4) VAT Default 
            If xAccessTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("VAT_Default").ToString
                Me.cmbVATType.Text = ""
                Me.cmbVATType.Text = oResult
            Else
                'Me.VATType_ComboBox.Text = "1"
            End If
            '(5) Payment Default 
            'If xAccessTable_Suppliers.DefaultView.Count > 0 Then
            'oResult = xAccessTable_Suppliers.DefaultView.Item(0).Item("Payment_Default").ToString
            'Me.PaymentType_ComboBox.Text = ""
            'Me.PaymentType_ComboBox.SelectedText = oResult
            'Else
            'Me.PaymentType_ComboBox.SelectedText = "CASH"
            'End If
        ElseIf My.Settings.DBType = "SQL" Then

            'If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then
            Dim oResult As String
            xSQLTable_Suppliers.DefaultView.RowFilter = "SupplierName='" & SQLConvert(oSupplierName) & "'"
            '(1) Default for Category 1 ComboBox
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Category_Default").ToString
                Me.Cat1.Text = ""
                Me.Cat1.SelectedText = oResult
            Else
                Me.Cat1.Text = ""
            End If
            '(2) Default for Category 2 ComboBox
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Detail_Default").ToString
                Me.Cat2.Text = ""
                Me.Cat2.SelectedText = oResult
            Else
                Me.Cat2.Text = ""
            End If
            '(3) Default for Category 3 ComboBox
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Detail_Default2").ToString
                Me.Cat3.Text = ""
                Me.Cat3.SelectedText = oResult
            Else
                Me.Cat3.Text = ""
            End If
            '(4) VAT Default 
            If xSQLTable_Suppliers.DefaultView.Count > 0 Then
                oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("VAT_Default").ToString
                Me.cmbVATType.Text = ""
                Me.cmbVATType.Text = oResult
            Else
                'Me.VATType_ComboBox.Text = "1"
            End If
            '(5) Payment Default 
            'If xSQLTable_Suppliers.DefaultView.Count > 0 Then
            'oResult = xSQLTable_Suppliers.DefaultView.Item(0).Item("Payment_Default").ToString
            'Me.PaymentType_ComboBox.Text = ""
            'Me.PaymentType_ComboBox.SelectedText = oResult
            'Else
            'Me.PaymentType_ComboBox.SelectedText = "CASH"
            'End If
        End If
    End Sub

    Function Get_Segment4_MB_Account(Category3 As String) As String 'Used for top new ComboBoxes
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Segment 3 Desc] = '" & SQLConvert(Category3) & "'" & " And [Segment 2 Desc] = '" & _
                SQLConvert(Me.Cat2.Text) & "' And [Segment 1 Desc] = '" & SQLConvert(Me.Cat1.Text) & "'"



            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4_MB_Account = datareader("Segment 4")
                    Else
                        Get_Segment4_MB_Account = "## NO ACCOUNT ##"
                    End If
                End While
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                While datareader.Read
                    If Not datareader("Segment 4").Equals(DBNull.Value) Then
                        Get_Segment4_MB_Account = datareader("Segment 4")
                    Else
                        Get_Segment4_MB_Account = "## NO ACCOUNT ##"
                    End If
                End While
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 035p")
        End Try


    End Function

    Private Sub Cat3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Cat3.SelectedIndexChanged
        If Me.Cat3.Text = "" Then Exit Sub
        Me.lblMagicBox_Account_Segment4.Text = Get_Segment4_MB_Account(Me.Cat3.Text)
        If Me.lblMagicBox_Account_Segment4.Text = "## NO ACCOUNT ##" Then
            MsgBox("There is no account for this category!")
        End If
    End Sub

    Private Sub chkSupplier_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSupplier.CheckedChanged
        If Me.chkSupplier.Checked = True Then
            Me.chkCategory.Checked = True
        End If
    End Sub

    Private Sub btnChangeCheckedItems_Click(sender As System.Object, e As System.EventArgs) Handles btnChangeCheckedItems.Click
        Try
            Me.lblUpdateSuccessful.Text = ""


            If chkTransactionDate.Checked = True Then
                Change_TransactionDate(Me.lblTransactionID.Text, Me.DateTimePicker1.Value.ToString("dd MMMM yyyy"))
            End If

            If Me.chkAmount.Checked = True Then
                If Me.DataGridView1.RowCount > 4 Then
                    MsgBox("Operation cannot be completed! Amount can only be changed for single libe invoices")
                    Exit Sub
                End If
                If Me.InvoiceTotal_TextBox.Text = "" Or Me.cmbVATType.Text = "" Then
                    MsgBox("To change amount, please enter the invoice total and VAT amount!")
                    Exit Sub
                End If
            End If

            If Me.chkCategory.Checked = True Then
                If Me.DataGridView1.RowCount > 4 Then
                    MsgBox("Operation cannot be completed! Categories can only be changed for single libe invoices")
                    Exit Sub
                End If
            End If

            Dim sSupplierID As String

            If Me.chkSupplier.Checked = True Then
                If ComboBox_Supplier.Text = "" Then
                    MsgBox("Please select a supplier.")
                    Exit Sub
                Else
                    Change_SupplierName(Me.lblTransactionID.Text, Me.ComboBox_Supplier.Text)
                    sSupplierID = GetSupplierID(ComboBox_Supplier.Text)
                    Change_SupplierID(Me.lblTransactionID.Text, sSupplierID)
                End If

            End If

            If Me.chkRef.Checked = True Then
                If Me.txtNewRef.Text = "" Then
                    MsgBox("Please select specify a new Reference.")
                    Exit Sub
                Else
                    Change_Reference(Me.lblTransactionID.Text, Me.txtNewRef.Text)
                End If
            End If

            If Me.chkCategory.Checked = True Then

                If Me.Cat1.Text = "" Or Me.Cat2.Text = "" Or Me.Cat3.Text = "" Then
                    MsgBox("Please select all three categories.")
                    Exit Sub
                Else
                    If Me.lblMagicBox_Account_Segment4.Text = "" Then
                        MsgBox("No Magic Box account code generated from Category selection. Please check this.")
                        Exit Sub
                    End If

                    Update_Category1(Me.lblTransactionID.Text, Me.Cat1.Text)
                    Update_Category2(Me.lblTransactionID.Text, Me.Cat2.Text)
                    Update_Category3(Me.lblTransactionID.Text, Me.Cat3.Text)
                    Update_DescriptionCode(Me.lblTransactionID.Text, Me.lblMagicBox_Account_Segment4.Text)
                End If
            End If

            If Me.chkAmount.Checked = True Then
                Update_Amount1(Me.lblTransactionID.Text, Me.InvoiceTotal_TextBox.Text)
                Update_Amount2(Me.lblTransactionID.Text, Me.txtNonVATAmount.Text)
                Update_Amount3(Me.lblTransactionID.Text, Me.txtVATAmount.Text)
            End If

            Call GetTransactions()

            Me.lblUpdateSuccessful.Text = "Update Successful"

        Catch ex As Exception

            Me.lblUpdateSuccessful.Text = "Update Failed"
            MsgBox(ex.Message & " 035r")

        End Try
    End Sub

    Function GetSupplierID(ByVal sSupplierName As String) As String
        Try
            Dim sSQL As String

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                sSQL = "SELECT DISTINCT SupplierID FROM Suppliers Where SupplierName = '" & sSupplierName & "'"
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                GetSupplierID = cmd.ExecuteScalar().ToString
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                sSQL = "SELECT DISTINCT SupplierID FROM Suppliers Where SupplierName = '" & sSupplierName & "'"
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                GetSupplierID = cmd.ExecuteScalar().ToString
                connection.Close()
                ''End If
            End If
        Catch ex As Exception
            MsgBox("There was an error getting a SupplierID! ")
            GetSupplierID = "NA"
        End Try
    End Function

    Sub Change_TransactionDate(ByVal oTransactionID As Integer, ByVal oNewTransactionDate As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Transaction Date] = '" & oNewTransactionDate & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450b")
        End Try
    End Sub

    Sub Change_SupplierID(ByVal oTransactionID As Integer, ByVal oNewSupplierID As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET SupplierID = " & oNewSupplierID & " WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450b")
        End Try
    End Sub

    Sub Change_SupplierName(ByVal oTransactionID As Integer, ByVal oNewSupplierName As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET SupplierName = '" & oNewSupplierName & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450cc")
        End Try
    End Sub


    Sub Change_Reference(ByVal oTransactionID As Integer, ByVal oNewReference As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET Reference = '" & oNewReference & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450ff")
        End Try
    End Sub

    Sub Update_Category1(ByVal oTransactionID As Integer, ByVal oCategory As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Description] = '" & oCategory & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] <> 'VAT CONTROL' And DR_CR = 'Dr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd")
        End Try
    End Sub

    Sub Update_Category2(ByVal oTransactionID As Integer, ByVal oCategory2 As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Description 2] = '" & oCategory2 & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] <> 'VAT CONTROL' And DR_CR = 'Dr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd")
        End Try
    End Sub

    Sub Update_Category3(ByVal oTransactionID As Integer, ByVal oCategory3 As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Description 3] = '" & oCategory3 & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] <> 'VAT CONTROL' And DR_CR = 'Dr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd")
        End Try
    End Sub

    Sub Update_DescriptionCode(ByVal oTransactionID As Integer, ByVal oDescriptionCode As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Description Code] = '" & oDescriptionCode & "' WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] <> 'VAT CONTROL' And DR_CR = 'Dr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd")
        End Try
    End Sub


    Sub Update_Amount1(ByVal oTransactionID As Integer, ByVal oAmount As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Amount] = " & oAmount & " WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] = 'CREDITORS CONTROL' And DR_CR = 'Cr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd2")
        End Try
    End Sub

    Sub Update_Amount2(ByVal oTransactionID As Integer, ByVal oAmount As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Amount] = " & oAmount & " WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] <> 'VAT CONTROL' And DR_CR = 'Dr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd")
        End Try
    End Sub

    Sub Update_Amount3(ByVal oTransactionID As Integer, ByVal oAmount As String)
        Try
            Dim sSQL As String
            sSQL = "UPDATE Transactions SET [Amount] = " & oAmount & " WHERE TransactionID = " & oTransactionID & ""
            sSQL = sSQL & " And [Description 2] = 'VAT CONTROL' And DR_CR = 'Dr'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450hd")
        End Try
    End Sub

    Sub Void_Transaction(ByVal oTransactionID As Integer)
        Try
            If oTransactionID = "" Then
                MsgBox("There was an issue with the TransactionID not being valid, please contact support", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            If oTransactionID = "0" Then
                MsgBox("There was an issue with the TransactionID not being valid, please contact support", MsgBoxStyle.Critical, "CALL SUPPORT")
                Exit Sub
            End If
            Dim sSQL As String
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET Void = True WHERE TransactionID = " & oTransactionID & ""
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET Void = 1 WHERE TransactionID = " & oTransactionID & ""
            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 450a")
        End Try
    End Sub

End Class