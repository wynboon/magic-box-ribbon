﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransactions2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransactions2))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTransactionID = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblLoggedIn = New System.Windows.Forms.Label()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUserName = New System.Windows.Forms.TextBox()
        Me.lblUserName = New System.Windows.Forms.Label()
        Me.btnSaveChanges = New System.Windows.Forms.Button()
        Me.ComboBox_Supplier = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Cat2 = New System.Windows.Forms.ComboBox()
        Me.lblCat2 = New System.Windows.Forms.Label()
        Me.Cat1 = New System.Windows.Forms.ComboBox()
        Me.lblCat1 = New System.Windows.Forms.Label()
        Me.Cat3 = New System.Windows.Forms.ComboBox()
        Me.lblCat3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNewRef = New System.Windows.Forms.TextBox()
        Me.txtNonVATAmount = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtVATAmount = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.cmbVATType = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.InvoiceTotal_TextBox = New System.Windows.Forms.TextBox()
        Me.dgvSuppliers = New System.Windows.Forms.DataGridView()
        Me.lblMagicBox_Account_Segment4 = New System.Windows.Forms.Label()
        Me.chkSupplier = New System.Windows.Forms.CheckBox()
        Me.chkRef = New System.Windows.Forms.CheckBox()
        Me.chkCategory = New System.Windows.Forms.CheckBox()
        Me.chkAmount = New System.Windows.Forms.CheckBox()
        Me.btnChangeCheckedItems = New System.Windows.Forms.Button()
        Me.lblUpdateSuccessful = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkTransactionDate = New System.Windows.Forms.CheckBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 189)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(962, 180)
        Me.DataGridView1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(25, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Transaction ID:"
        '
        'lblTransactionID
        '
        Me.lblTransactionID.AutoSize = True
        Me.lblTransactionID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblTransactionID.Location = New System.Drawing.Point(111, 23)
        Me.lblTransactionID.Name = "lblTransactionID"
        Me.lblTransactionID.Size = New System.Drawing.Size(10, 13)
        Me.lblTransactionID.TabIndex = 2
        Me.lblTransactionID.Text = "-"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblLoggedIn)
        Me.Panel1.Controls.Add(Me.btnLogin)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtUserName)
        Me.Panel1.Controls.Add(Me.lblUserName)
        Me.Panel1.Controls.Add(Me.btnSaveChanges)
        Me.Panel1.Location = New System.Drawing.Point(188, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(689, 37)
        Me.Panel1.TabIndex = 100
        '
        'lblLoggedIn
        '
        Me.lblLoggedIn.AutoSize = True
        Me.lblLoggedIn.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLoggedIn.Location = New System.Drawing.Point(494, 13)
        Me.lblLoggedIn.Name = "lblLoggedIn"
        Me.lblLoggedIn.Size = New System.Drawing.Size(55, 13)
        Me.lblLoggedIn.TabIndex = 103
        Me.lblLoggedIn.Text = "Logged In"
        Me.lblLoggedIn.Visible = False
        '
        'btnLogin
        '
        Me.btnLogin.ForeColor = System.Drawing.Color.Maroon
        Me.btnLogin.Location = New System.Drawing.Point(359, 6)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(100, 23)
        Me.btnLogin.TabIndex = 102
        Me.btnLogin.Text = "Log in"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(244, 8)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(100, 20)
        Me.txtPassword.TabIndex = 101
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Maroon
        Me.Label2.Location = New System.Drawing.Point(186, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 100
        Me.Label2.Text = "Password"
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(69, 8)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(100, 20)
        Me.txtUserName.TabIndex = 99
        '
        'lblUserName
        '
        Me.lblUserName.AutoSize = True
        Me.lblUserName.ForeColor = System.Drawing.Color.Maroon
        Me.lblUserName.Location = New System.Drawing.Point(3, 11)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(60, 13)
        Me.lblUserName.TabIndex = 98
        Me.lblUserName.Text = "User Name"
        '
        'btnSaveChanges
        '
        Me.btnSaveChanges.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSaveChanges.Location = New System.Drawing.Point(565, 8)
        Me.btnSaveChanges.Name = "btnSaveChanges"
        Me.btnSaveChanges.Size = New System.Drawing.Size(100, 23)
        Me.btnSaveChanges.TabIndex = 97
        Me.btnSaveChanges.Text = "Save Changes"
        Me.btnSaveChanges.UseVisualStyleBackColor = True
        Me.btnSaveChanges.Visible = False
        '
        'ComboBox_Supplier
        '
        Me.ComboBox_Supplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.ComboBox_Supplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ComboBox_Supplier.FormattingEnabled = True
        Me.ComboBox_Supplier.Location = New System.Drawing.Point(116, 76)
        Me.ComboBox_Supplier.Name = "ComboBox_Supplier"
        Me.ComboBox_Supplier.Size = New System.Drawing.Size(182, 21)
        Me.ComboBox_Supplier.TabIndex = 101
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(20, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 102
        Me.Label3.Text = "Change Supplier"
        '
        'Cat2
        '
        Me.Cat2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.Cat2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Cat2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cat2.FormattingEnabled = True
        Me.Cat2.Items.AddRange(New Object() {"ACCOUNTING & AUDITING", "BANK CHARGES", "CREDIT CARD CHARGES", "INSURANCE", "RENT + OPPS COSTS +RATES", "RENTALS AND LEASES", "FRANCHISE ROYALTY", "FRANCHISE MARKETING"})
        Me.Cat2.Location = New System.Drawing.Point(419, 112)
        Me.Cat2.Margin = New System.Windows.Forms.Padding(4)
        Me.Cat2.Name = "Cat2"
        Me.Cat2.Size = New System.Drawing.Size(212, 21)
        Me.Cat2.TabIndex = 104
        '
        'lblCat2
        '
        Me.lblCat2.AutoSize = True
        Me.lblCat2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat2.Location = New System.Drawing.Point(326, 115)
        Me.lblCat2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCat2.Name = "lblCat2"
        Me.lblCat2.Size = New System.Drawing.Size(85, 13)
        Me.lblCat2.TabIndex = 106
        Me.lblCat2.Text = "Category Type 2"
        '
        'Cat1
        '
        Me.Cat1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.Cat1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Cat1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cat1.FormattingEnabled = True
        Me.Cat1.Items.AddRange(New Object() {"PURCHASES", "STAFF COSTS & WELFARE", "OPERATING COSTS", "UTILITIES", "NON CONTROLLABLE COSTS"})
        Me.Cat1.Location = New System.Drawing.Point(419, 77)
        Me.Cat1.Margin = New System.Windows.Forms.Padding(4)
        Me.Cat1.Name = "Cat1"
        Me.Cat1.Size = New System.Drawing.Size(212, 21)
        Me.Cat1.TabIndex = 103
        '
        'lblCat1
        '
        Me.lblCat1.AutoSize = True
        Me.lblCat1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat1.Location = New System.Drawing.Point(326, 80)
        Me.lblCat1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCat1.Name = "lblCat1"
        Me.lblCat1.Size = New System.Drawing.Size(85, 13)
        Me.lblCat1.TabIndex = 105
        Me.lblCat1.Text = "Category Type 1"
        '
        'Cat3
        '
        Me.Cat3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.Cat3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Cat3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cat3.FormattingEnabled = True
        Me.Cat3.Items.AddRange(New Object() {"ACCOUNTING & AUDITING"})
        Me.Cat3.Location = New System.Drawing.Point(419, 149)
        Me.Cat3.Margin = New System.Windows.Forms.Padding(4)
        Me.Cat3.Name = "Cat3"
        Me.Cat3.Size = New System.Drawing.Size(212, 21)
        Me.Cat3.TabIndex = 107
        '
        'lblCat3
        '
        Me.lblCat3.AutoSize = True
        Me.lblCat3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.lblCat3.Location = New System.Drawing.Point(326, 152)
        Me.lblCat3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCat3.Name = "lblCat3"
        Me.lblCat3.Size = New System.Drawing.Size(85, 13)
        Me.lblCat3.TabIndex = 108
        Me.lblCat3.Text = "Category Type 3"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(20, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 109
        Me.Label4.Text = "Change Ref"
        '
        'txtNewRef
        '
        Me.txtNewRef.Location = New System.Drawing.Point(116, 114)
        Me.txtNewRef.Name = "txtNewRef"
        Me.txtNewRef.Size = New System.Drawing.Size(182, 20)
        Me.txtNewRef.TabIndex = 110
        '
        'txtNonVATAmount
        '
        Me.txtNonVATAmount.Enabled = False
        Me.txtNonVATAmount.Location = New System.Drawing.Point(869, 103)
        Me.txtNonVATAmount.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNonVATAmount.Name = "txtNonVATAmount"
        Me.txtNonVATAmount.Size = New System.Drawing.Size(70, 20)
        Me.txtNonVATAmount.TabIndex = 114
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label34.Location = New System.Drawing.Point(810, 106)
        Me.Label34.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(42, 13)
        Me.Label34.TabIndex = 118
        Me.Label34.Text = "ex VAT"
        '
        'txtVATAmount
        '
        Me.txtVATAmount.Location = New System.Drawing.Point(715, 102)
        Me.txtVATAmount.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVATAmount.Name = "txtVATAmount"
        Me.txtVATAmount.Size = New System.Drawing.Size(87, 20)
        Me.txtVATAmount.TabIndex = 113
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label32.Location = New System.Drawing.Point(658, 105)
        Me.Label32.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 13)
        Me.Label32.TabIndex = 117
        Me.Label32.Text = "VAT amt"
        '
        'cmbVATType
        '
        Me.cmbVATType.FormattingEnabled = True
        Me.cmbVATType.Items.AddRange(New Object() {"0", "1"})
        Me.cmbVATType.Location = New System.Drawing.Point(869, 74)
        Me.cmbVATType.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbVATType.Name = "cmbVATType"
        Me.cmbVATType.Size = New System.Drawing.Size(70, 21)
        Me.cmbVATType.TabIndex = 112
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label28.Location = New System.Drawing.Point(810, 77)
        Me.Label28.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(51, 13)
        Me.Label28.TabIndex = 116
        Me.Label28.Text = "VAT type"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label19.Location = New System.Drawing.Point(658, 76)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(49, 13)
        Me.Label19.TabIndex = 115
        Me.Label19.Text = "Inv Total"
        '
        'InvoiceTotal_TextBox
        '
        Me.InvoiceTotal_TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InvoiceTotal_TextBox.Location = New System.Drawing.Point(715, 73)
        Me.InvoiceTotal_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.InvoiceTotal_TextBox.Name = "InvoiceTotal_TextBox"
        Me.InvoiceTotal_TextBox.Size = New System.Drawing.Size(87, 19)
        Me.InvoiceTotal_TextBox.TabIndex = 111
        '
        'dgvSuppliers
        '
        Me.dgvSuppliers.BackgroundColor = System.Drawing.Color.MidnightBlue
        Me.dgvSuppliers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSuppliers.Location = New System.Drawing.Point(921, 8)
        Me.dgvSuppliers.Name = "dgvSuppliers"
        Me.dgvSuppliers.RowTemplate.Height = 24
        Me.dgvSuppliers.Size = New System.Drawing.Size(53, 41)
        Me.dgvSuppliers.TabIndex = 119
        Me.dgvSuppliers.Visible = False
        '
        'lblMagicBox_Account_Segment4
        '
        Me.lblMagicBox_Account_Segment4.AutoSize = True
        Me.lblMagicBox_Account_Segment4.ForeColor = System.Drawing.SystemColors.ScrollBar
        Me.lblMagicBox_Account_Segment4.Location = New System.Drawing.Point(113, 148)
        Me.lblMagicBox_Account_Segment4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMagicBox_Account_Segment4.Name = "lblMagicBox_Account_Segment4"
        Me.lblMagicBox_Account_Segment4.Size = New System.Drawing.Size(10, 13)
        Me.lblMagicBox_Account_Segment4.TabIndex = 120
        Me.lblMagicBox_Account_Segment4.Text = "-"
        '
        'chkSupplier
        '
        Me.chkSupplier.AutoSize = True
        Me.chkSupplier.Location = New System.Drawing.Point(304, 79)
        Me.chkSupplier.Name = "chkSupplier"
        Me.chkSupplier.Size = New System.Drawing.Size(15, 14)
        Me.chkSupplier.TabIndex = 121
        Me.chkSupplier.UseVisualStyleBackColor = True
        '
        'chkRef
        '
        Me.chkRef.AutoSize = True
        Me.chkRef.Location = New System.Drawing.Point(304, 114)
        Me.chkRef.Name = "chkRef"
        Me.chkRef.Size = New System.Drawing.Size(15, 14)
        Me.chkRef.TabIndex = 122
        Me.chkRef.UseVisualStyleBackColor = True
        '
        'chkCategory
        '
        Me.chkCategory.AutoSize = True
        Me.chkCategory.Location = New System.Drawing.Point(638, 152)
        Me.chkCategory.Name = "chkCategory"
        Me.chkCategory.Size = New System.Drawing.Size(15, 14)
        Me.chkCategory.TabIndex = 123
        Me.chkCategory.UseVisualStyleBackColor = True
        '
        'chkAmount
        '
        Me.chkAmount.AutoSize = True
        Me.chkAmount.Location = New System.Drawing.Point(946, 105)
        Me.chkAmount.Name = "chkAmount"
        Me.chkAmount.Size = New System.Drawing.Size(15, 14)
        Me.chkAmount.TabIndex = 124
        Me.chkAmount.UseVisualStyleBackColor = True
        '
        'btnChangeCheckedItems
        '
        Me.btnChangeCheckedItems.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnChangeCheckedItems.Location = New System.Drawing.Point(715, 147)
        Me.btnChangeCheckedItems.Name = "btnChangeCheckedItems"
        Me.btnChangeCheckedItems.Size = New System.Drawing.Size(224, 23)
        Me.btnChangeCheckedItems.TabIndex = 125
        Me.btnChangeCheckedItems.Text = "Change Checked Items"
        Me.btnChangeCheckedItems.UseVisualStyleBackColor = True
        '
        'lblUpdateSuccessful
        '
        Me.lblUpdateSuccessful.AutoSize = True
        Me.lblUpdateSuccessful.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblUpdateSuccessful.Location = New System.Drawing.Point(727, 131)
        Me.lblUpdateSuccessful.Name = "lblUpdateSuccessful"
        Me.lblUpdateSuccessful.Size = New System.Drawing.Size(10, 13)
        Me.lblUpdateSuccessful.TabIndex = 126
        Me.lblUpdateSuccessful.Text = "-"
        Me.lblUpdateSuccessful.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(20, 153)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 13)
        Me.Label5.TabIndex = 127
        Me.Label5.Text = "Transaction Date"
        '
        'chkTransactionDate
        '
        Me.chkTransactionDate.AutoSize = True
        Me.chkTransactionDate.Location = New System.Drawing.Point(304, 151)
        Me.chkTransactionDate.Name = "chkTransactionDate"
        Me.chkTransactionDate.Size = New System.Drawing.Size(15, 14)
        Me.chkTransactionDate.TabIndex = 129
        Me.chkTransactionDate.UseVisualStyleBackColor = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(116, 149)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(182, 20)
        Me.DateTimePicker1.TabIndex = 130
        '
        'frmTransactions2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1000, 381)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.chkTransactionDate)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblUpdateSuccessful)
        Me.Controls.Add(Me.btnChangeCheckedItems)
        Me.Controls.Add(Me.chkAmount)
        Me.Controls.Add(Me.chkCategory)
        Me.Controls.Add(Me.chkRef)
        Me.Controls.Add(Me.chkSupplier)
        Me.Controls.Add(Me.lblMagicBox_Account_Segment4)
        Me.Controls.Add(Me.dgvSuppliers)
        Me.Controls.Add(Me.txtNonVATAmount)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.txtVATAmount)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.cmbVATType)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.InvoiceTotal_TextBox)
        Me.Controls.Add(Me.txtNewRef)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Cat3)
        Me.Controls.Add(Me.lblCat3)
        Me.Controls.Add(Me.Cat2)
        Me.Controls.Add(Me.lblCat2)
        Me.Controls.Add(Me.Cat1)
        Me.Controls.Add(Me.lblCat1)
        Me.Controls.Add(Me.ComboBox_Supplier)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblTransactionID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmTransactions2"
        Me.Text = "Transactions2"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTransactionID As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblLoggedIn As System.Windows.Forms.Label
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents btnSaveChanges As System.Windows.Forms.Button
    Friend WithEvents ComboBox_Supplier As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Cat2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCat2 As System.Windows.Forms.Label
    Friend WithEvents Cat1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCat1 As System.Windows.Forms.Label
    Friend WithEvents Cat3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblCat3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNewRef As System.Windows.Forms.TextBox
    Friend WithEvents txtNonVATAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtVATAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents cmbVATType As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents InvoiceTotal_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents dgvSuppliers As System.Windows.Forms.DataGridView
    Friend WithEvents lblMagicBox_Account_Segment4 As System.Windows.Forms.Label
    Friend WithEvents chkSupplier As System.Windows.Forms.CheckBox
    Friend WithEvents chkRef As System.Windows.Forms.CheckBox
    Friend WithEvents chkCategory As System.Windows.Forms.CheckBox
    Friend WithEvents chkAmount As System.Windows.Forms.CheckBox
    Friend WithEvents btnChangeCheckedItems As System.Windows.Forms.Button
    Friend WithEvents lblUpdateSuccessful As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkTransactionDate As System.Windows.Forms.CheckBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
End Class
