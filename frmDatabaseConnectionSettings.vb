﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class frmDatabaseConnectionSettings
#Region "Variables"
    Public ConnString As String = ""
    Public ConnectionName As String = ""
    Public ExitMethod As String = ""
    Dim forEdit As Boolean = False
#End Region
    Private Sub frmDatabaseConnectionSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try

            If CStr(My.Settings.CS_Setting) = String.Empty And CStr(My.Settings.ListViewValues) = String.Empty Then
                Me.Text = "MagicBox -> Connection Builder -> Version : " & My.Settings.Setting_Version
                Exit Sub
            Else
                Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Connection Builder -> Version : " & My.Settings.Setting_Version
            End If

            If ConnString <> "" And ConnectionName <> "" Then

                forEdit = True
                ckbPassword.Visible = False

                Dim ConnStringSplit As String() = ConnString.Split(New Char() {"="c})

                Dim ServerSplit() As String = ConnStringSplit(1).Split(New Char() {";"c})
                Dim Servername As String = ServerSplit(0).ToString

                Dim DatabaseSplit() As String = ConnStringSplit(2).Split(New Char() {";"c})
                Dim DatabaseName As String = DatabaseSplit(0).ToString

                Dim UserSplit() As String = ConnStringSplit(3).Split(New Char() {";"c})
                Dim UserName As String = UserSplit(0).ToString

                Dim Password As String = ConnStringSplit(4).ToString

                txtConnName.Text = ConnectionName
                txtServerName.Text = Servername
                txtDatabaseName.Text = DatabaseName
                txtUserName.Text = UserName
                txtPassword.Text = Password

            End If
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Scheduler", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        'Server=HEROLD-LAPTOP;Database=MagicBox_DoppioSandBox;Trusted_Connection=True;
        'ConnString = "Server=HEROLD-LAPTOP;Database=MagicBox_DoppioSandBox;Trusted_Connection=True;"

        If txtConnName.Text.Length <> 0 Then
            ConnectionName = txtConnName.Text.Trim
        Else
            MessageBox.Show("Please provide the connection name.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtConnName.Focus()
            Exit Sub
        End If

        If txtServerName.Text.Length <> 0 Then
            ConnString = "Server=" & txtServerName.Text.ToString
        Else
            MessageBox.Show("Please supply server name.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtServerName.Focus()
            Exit Sub
        End If

        If txtDatabaseName.Text.Length <> 0 Then
            ConnString += ";Database=" & txtDatabaseName.Text.ToString
        Else
            MessageBox.Show("Please supply database name.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtDatabaseName.Focus()
            Exit Sub
        End If

        If txtUserName.Text.Length <> 0 Then
            ConnString += ";Uid=" & txtUserName.Text.ToString
        Else
            MessageBox.Show("Please supply login username.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtUserName.Focus()
            Exit Sub
        End If

        If txtPassword.Text.Length <> 0 Then
            ConnString += ";Pwd=" & txtPassword.Text.ToString
        Else
            MessageBox.Show("Please supply login password.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txtPassword.Focus()
            Exit Sub
        End If

        'Testing connection String
        Try
            Dim cn As New SqlConnection(ConnString)
            Using cn
                cn.Open()
                If cn.State = Data.ConnectionState.Open Then
                    lblTesting.Text = "Connection valid"
                    If forEdit Then
                        MessageBox.Show("Connection : '" & ConnectionName & "' succesfully edited, please save the changes on the main screen.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("New connection : '" & ConnectionName & "' succesfully added, please save the changes on the main screen.", "Connection builder", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                    ExitMethod = "Save"
                    lblTesting.Text = ""
                    Me.Close()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Scheduler", MessageBoxButtons.OK, MessageBoxIcon.Error)
        txtServerName.Text = ""
        txtDatabaseName.Text = ""
        txtUserName.Text = ""
        txtPassword.Text = ""
        txtServerName.Focus()
        End Try

    End Sub

    Private Sub ckbPassword_CheckedChanged(sender As Object, e As EventArgs) Handles ckbPassword.CheckedChanged
        If Me.ckbPassword.Checked = True Then
            txtPassword.PasswordChar = ""
        Else
            txtPassword.PasswordChar = "*"
        End If
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        ExitMethod = "Exit"
        Me.Close()
    End Sub

End Class