1. Disable Customer tab on the Ribbon.
2. Changed Invoice status to Default "UNPAID" on Invoice Capture Screen
3. Included actual Company(Company connected to) name in all Screens Caption
4. Legder accounts automatic refresh on updating and Creation(Account Maintanance), Disabling/Enabling of Code 1, 2, 3 text fields depending on the option
5. Supplier Payment/Invoice Date capture error fix
6. Invoice/reference validation fix
7. Remove multiple confirmation during multiple invoice payment fix
8. Allow paying of multiple invoices from different suppliers