---SCRIPT TO CREATE dbo.PaymentsRequests
---Author : Herold.
---Date	  : 23 Jan 2014


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PaymentsRequests](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceTransactionID] [int] NULL,
	[CapturedDate] [datetime] NULL,
	[PaymentType] [nvarchar](50) NULL,
	[RequestedAmount] [decimal](12, 2) NULL,
	[RequestApproved] [bit] NULL,
	[ApprovedAmount] [decimal](12, 2) NULL,
	[PaymentTransactionID] [int] NULL,
	[RequestOwner] [nvarchar](100) NULL,
	[RequestApprover] [nvarchar](100) NULL,
	[PayInAdvanceTransactionID] [int] NULL,
	[Co_ID] [int] NULL,
 CONSTRAINT [PK_PaymentsRequests] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO