﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Net.NetworkInformation
Imports System.Windows.Forms

Module mod_Application_Startup



    Function TestServer() As Boolean
        Try
            Dim mac As String = "41.72.128.100" ' use any other machine name
            Dim pingreq As Ping = New Ping()
            Dim rep As PingReply = pingreq.Send(mac)
            If rep.Status = IPStatus.Success Then
                'Successful server
                TestServer = True
                Globals.Ribbons.Ribbon1.NI_ServerStatus.BalloonTipTitle = "Server Test - OK"
                Globals.Ribbons.Ribbon1.NI_ServerStatus.BalloonTipText = rep.RoundtripTime.ToString
                Globals.Ribbons.Ribbon1.NI_ServerStatus.BalloonTipIcon = ToolTipIcon.Info
                Globals.Ribbons.Ribbon1.NI_ServerStatus.ShowBalloonTip(1000)
            Else
                TestServer = False
                Globals.Ribbons.Ribbon1.NI_ServerStatus.BalloonTipTitle = "Server Test - Warning"
                Globals.Ribbons.Ribbon1.NI_ServerStatus.BalloonTipText = "Server Error: Conneciton not found"
                Globals.Ribbons.Ribbon1.NI_ServerStatus.BalloonTipIcon = ToolTipIcon.Warning
                Globals.Ribbons.Ribbon1.NI_ServerStatus.ShowBalloonTip(2000)
            End If

            

        Catch ex As Exception
            MsgBox("Err in ServerTest: " & ex.Message)
        End Try
    End Function


    Sub oStartUp()

        If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Active" Then Exit Sub

        Try

            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            'Load checks governs the InvoiceHeader and InvoiceLines table checks
            If My.Settings.Load_Checks = "No" Then
                'do nothing
            Else
                Dim oNumberRowsInInvoiceHeader = Count_Lines_In_Invoice_Header_Table()
                If oNumberRowsInInvoiceHeader = "Error" Or oNumberRowsInInvoiceHeader = "0" Then
                    'Do nothing
                Else
                    If IsNumeric(oNumberRowsInInvoiceHeader) = True Then
                        'Herold's fix-Create new disposable form object
                        '  Dim frmImpExp As New frmImportExport
                        '  frmImpExp.Show()
                        '  frmImpExp.oCheckAll()
                        ''--Old Code
                        'Form_ImportExport.Show()
                        'Form_ImportExport.BringToFront()
                        'Call Form_ImportExport.oCheckAll()
                    End If
                End If
            End If


            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            If My.Settings.Load_Checks = "No" Then
                'do nothing
            Else

                If My.Settings.CS_Setting = "" Or My.Settings.CS_Setting = Nothing Then
                    MsgBox("No database path or connection string has been set up. Please do this now in 'Settings'.")
                    Beep()
                End If


                Dim oHighestTransactionID_in_Transactions As Long = oGetHighestTransactionID_in_Transactions()

                If oHighestTransactionID_in_Transactions = 0 Then Exit Sub

                Dim oHighestID_in_TransactionID_Table As Long = oGetHighestID_in_TransactionID()
                If oHighestTransactionID_in_Transactions > oHighestID_in_TransactionID_Table Then
                    If My.Settings.DBType = "Access" Then
                        Insert_New_ID_in_TransactionID(oHighestTransactionID_in_Transactions + 100)
                    ElseIf My.Settings.DBType = "SQL" Then
                        Drop_Transaction_ID_Table()
                        Create_New_Transaction_ID_Table(oHighestTransactionID_in_Transactions + 100)
                    End If
                End If
                Dim oHighestBatchID_in_Transactions As Long = oGetHighestBatchID_in_Transactions()
                Dim oHighestID_in_BatchID_Table As Long = oGetHighestID_in_BatchID()
                If oHighestBatchID_in_Transactions > oHighestID_in_BatchID_Table Then
                    If My.Settings.DBType = "Access" Then
                        Insert_New_ID_in_BatchID(oHighestBatchID_in_Transactions + 100)
                    ElseIf My.Settings.DBType = "SQL" Then
                        Drop_Batch_ID_Table()
                        Create_New_Batch_ID_Table(oHighestBatchID_in_Transactions + 100)
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message & "001")
        End Try
    End Sub

    Sub Drop_Transaction_ID_Table()
        Try
            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            cn.Open()
            Dim sSQL As String
            sSQL = "DROP TABLE TransactionID"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message & "002")
        End Try
    End Sub
    Sub Create_New_Transaction_ID_Table(ByVal oIdentitySeed As String)
        Try
            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            cn.Open()
            Dim sSQL As String
            sSQL = "CREATE TABLE TransactionID "
            sSQL = sSQL & "([ID] bigint identity(" & oIdentitySeed & ",1) not null primary key, [oUser] nvarchar(50), [oDate] nvarchar(50), [oCode] nvarchar(50))"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message & " 003")
        End Try
    End Sub

    Sub Drop_Batch_ID_Table()
        Try
            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            cn.Open()
            Dim sSQL As String
            sSQL = "DROP TABLE BatchID"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message & " 004")
        End Try
    End Sub
    Sub Create_New_Batch_ID_Table(ByVal oIdentitySeed As String)
        Try
            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            cn.Open()
            Dim sSQL As String
            sSQL = "CREATE TABLE BatchID "
            sSQL = sSQL & "([ID] bigint identity(" & oIdentitySeed & ",1) not null primary key, [oUser] nvarchar(50), [oDate] nvarchar(50), [oCode] nvarchar(50))"
            Dim objCmd As New SqlCommand(sSQL, cn)
            objCmd.CommandTimeout = 300
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message & " 005")
        End Try
    End Sub

    Public Sub Insert_New_ID_in_TransactionID(ByVal oNewAutoNumber As Long)
        Dim sSQL As String
        Try
            sSQL = "ALTER TABLE TransactionID ALTER COLUMN ID Counter(" & oNewAutoNumber & ",1)"

            Dim cn As New OleDbConnection(My.Settings.CS_Setting)
            Dim cmd As New OleDbCommand(sSQL, cn)
            cn.Open()
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox("There was a problem setting a new ID in TransactionID " & ex.Message & " 006")
        End Try
    End Sub


    Public Sub Insert_New_ID_in_BatchID(ByVal oNewAutoNumber As Long)
        Dim sSQL As String
        Try
            sSQL = "ALTER TABLE BatchID ALTER COLUMN ID Counter(" & oNewAutoNumber & ",1)"

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox("There was a problem setting a new ID in BatchID " & ex.Message & " 007")
        End Try
    End Sub

    Public Function oGetHighestTransactionID_in_Transactions() As Long

        Try
            Dim oResult As Object
            Dim sSQL As String
            sSQL = "Select Max(TransactionID) From Transactions"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestTransactionID_in_Transactions = 0
                Else
                    oGetHighestTransactionID_in_Transactions = oResult
                End If
                connection.Close()

            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestTransactionID_in_Transactions = 0
                Else
                    oGetHighestTransactionID_in_Transactions = oResult
                End If
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the highest TransactionID in Transactions! " & ex.Message & " 008")
        End Try
    End Function
    Public Function oGetHighestID_in_TransactionID() As Long
        Try
            Dim sSQL As String
            sSQL = "Select Max(ID) From TransactionID"
            Dim oResult As Object

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_TransactionID = 0
                Else
                    oGetHighestID_in_TransactionID = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_TransactionID = 0
                Else
                    oGetHighestID_in_TransactionID = oResult
                End If
                connection.Close()
            End If



        Catch ex As Exception
            MsgBox("There was an error getting the highest ID from the TRansactionID table! " & ex.Message & " 009")
        End Try
    End Function
    Public Function oGetHighestBatchID_in_Transactions() As Long
        Try
            Dim oResult As Object
            Dim sSQL As String
            sSQL = "Select Max(BatchID) From Transactions"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestBatchID_in_Transactions = 0
                Else
                    oGetHighestBatchID_in_Transactions = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestBatchID_in_Transactions = 0
                Else
                    oGetHighestBatchID_in_Transactions = oResult
                End If
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the highest BatchID in Transactions! " & ex.Message & " 010")
        End Try
    End Function



    Public Function oGetHighestID_in_BatchID() As Long
        Try
            Dim oResult As Object
            Dim sSQL As String
            sSQL = "Select Max(ID) From BatchID"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_BatchID = 0
                Else
                    oGetHighestID_in_BatchID = oResult
                End If
                connection.Close()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                oResult = cmd.ExecuteScalar()
                If IsDBNull(oResult) = True Then
                    oGetHighestID_in_BatchID = 0
                Else
                    oGetHighestID_in_BatchID = oResult
                End If
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error getting the highest ID from the BatchID table! " & ex.Message & " 011")
        End Try
    End Function

End Module
