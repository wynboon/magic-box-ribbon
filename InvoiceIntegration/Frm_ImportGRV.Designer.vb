﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_ImportGRV
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Browse = New System.Windows.Forms.Button()
        Me.Btn_Close = New System.Windows.Forms.Button()
        Me.LBL_ImportType = New System.Windows.Forms.Label()
        Me.Micros = New System.Windows.Forms.OpenFileDialog()
        Me.PB_Import = New System.Windows.Forms.ProgressBar()
        Me.BGW_Import = New System.ComponentModel.BackgroundWorker()
        Me.SuspendLayout()
        '
        'Btn_Browse
        '
        Me.Btn_Browse.Location = New System.Drawing.Point(213, 12)
        Me.Btn_Browse.Name = "Btn_Browse"
        Me.Btn_Browse.Size = New System.Drawing.Size(281, 60)
        Me.Btn_Browse.TabIndex = 1
        Me.Btn_Browse.Text = "Browse for file"
        Me.Btn_Browse.UseVisualStyleBackColor = True
        '
        'Btn_Close
        '
        Me.Btn_Close.Location = New System.Drawing.Point(12, 107)
        Me.Btn_Close.Name = "Btn_Close"
        Me.Btn_Close.Size = New System.Drawing.Size(487, 40)
        Me.Btn_Close.TabIndex = 3
        Me.Btn_Close.Text = "Close"
        Me.Btn_Close.UseVisualStyleBackColor = True
        '
        'LBL_ImportType
        '
        Me.LBL_ImportType.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_ImportType.Location = New System.Drawing.Point(12, 25)
        Me.LBL_ImportType.Name = "LBL_ImportType"
        Me.LBL_ImportType.Size = New System.Drawing.Size(195, 37)
        Me.LBL_ImportType.TabIndex = 4
        Me.LBL_ImportType.Text = "GAAP"
        '
        'PB_Import
        '
        Me.PB_Import.Location = New System.Drawing.Point(12, 78)
        Me.PB_Import.Name = "PB_Import"
        Me.PB_Import.Size = New System.Drawing.Size(485, 23)
        Me.PB_Import.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.PB_Import.TabIndex = 5
        '
        'BGW_Import
        '
        '
        'Frm_ImportGRV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(506, 154)
        Me.ControlBox = False
        Me.Controls.Add(Me.PB_Import)
        Me.Controls.Add(Me.LBL_ImportType)
        Me.Controls.Add(Me.Btn_Close)
        Me.Controls.Add(Me.Btn_Browse)
        Me.Name = "Frm_ImportGRV"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Btn_Browse As System.Windows.Forms.Button
    Friend WithEvents Btn_Close As System.Windows.Forms.Button
    Friend WithEvents LBL_ImportType As System.Windows.Forms.Label
    Friend WithEvents Micros As System.Windows.Forms.OpenFileDialog
    Friend WithEvents PB_Import As System.Windows.Forms.ProgressBar
    Friend WithEvents BGW_Import As System.ComponentModel.BackgroundWorker
End Class
