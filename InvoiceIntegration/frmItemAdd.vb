﻿Imports System.Data

Public Class frmItemAdd

    Private Sub Btn_ItemAdd_Click(sender As Object, e As EventArgs) Handles Btn_ItemAdd.Click
        If CheckItemExists(TB_ItemCode.Text) = True Then
            'Notify and exit
            MsgBox("The item code already exists, please select a new code", MsgBoxStyle.Critical, "Code Exists")
            Exit Sub
        End If

        Dim Account As String = Get_MB_GL_Account(CB_Cat1.Text, CB_Cat2.Text, CB_Cat3.Text)

        If Account = "NO ACCOUNT" Then
            MsgBox("The code categories selected did not result in a found account, please re-try", MsgBoxStyle.Critical, "Account Error")
            Exit Sub
        End If

        If InsertNewItemMaster(TB_ItemCode.Text, TB_ItemName.Text, TB_IntegrationCode.Text, Account, CB_HasVAT.Checked) = True Then
            Me.Close()
        End If

    End Sub

    Private Sub frmItemAdd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LockForm(Me)
        Call LoadCats1()
    End Sub
    Sub LoadCats1()
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        ds = ds.DefaultView.ToTable(True, "Segment 1 Desc")
        CB_Cat1.DataSource = ds
        CB_Cat1.DisplayMember = "Segment 1 Desc"
        ds = Nothing
    End Sub

    Private Sub CB_Cat1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Cat1.SelectedIndexChanged
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        If CB_Cat1.Text.ToString = "" Then Exit Sub
        ds.DefaultView.RowFilter = "[Segment 1 Desc] = '" & CB_Cat1.Text & "'"
        ds = ds.DefaultView.ToTable(True, "Segment 2 Desc")
        CB_Cat2.DataSource = ds
        CB_Cat2.DisplayMember = "Segment 2 Desc"
        ds = Nothing
    End Sub

    Private Sub CB_Cat2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Cat2.SelectedIndexChanged
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        If CB_Cat2.Text = "" Then Exit Sub
        ds.DefaultView.RowFilter = "[Segment 2 Desc] = '" & CB_Cat2.Text & "'"
        ds = ds.DefaultView.ToTable(True, "Segment 3 Desc")
        CB_Cat3.DataSource = ds
        CB_Cat3.DisplayMember = "Segment 3 Desc"
        ds = Nothing
    End Sub

    Private Sub Btn_Cancel_Click(sender As Object, e As EventArgs) Handles Btn_Cancel.Click
        Me.Close()
    End Sub
End Class