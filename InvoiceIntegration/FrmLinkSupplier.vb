﻿Imports System.Data
Imports System.Windows.Forms

Public Class FrmLinkSupplier

    Private Sub btn_Link_Click(sender As Object, e As EventArgs) Handles btn_Link.Click
        Dim SupplierID As Long
        Dim selectedCellCount As Integer = DataGridView1.GetCellCount(DataGridViewElementStates.Selected)
        If selectedCellCount > 0 Then
            SupplierID = DataGridView1.SelectedRows(0).Cells("SupplierID").Value.ToString
            If UpdateSupplierIntegrationCode(SupplierID, LBL_SupplierName.Text) = True Then
                MsgBox("Complete")
                Me.Close()
            End If
        Else
            MsgBox("No Supplier selected in below grid.", MsgBoxStyle.Information, "User Error")
            Exit Sub
        End If
    End Sub

    Private Sub btn_CreateNew_Click(sender As Object, e As EventArgs) Handles btn_CreateNew.Click
        Dim frmSupplierNew As New frmSupplierNew
        frmSupplierNew.SupplierName_TextBox.Text = LBL_SupplierName.Text
        frmSupplierNew.Lbl_IntegrationCode.Text = LBL_SupplierName.Text
        frmSupplierNew.AutoClose = True
        frmSupplierNew.ShowDialog()
        Me.Close()
    End Sub

    Private Sub FrmLinkSupplier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim dsSuppliers As DataTable
        dsSuppliers = GetSuppliers()
        DataGridView1.DataSource = dsSuppliers
    End Sub
End Class