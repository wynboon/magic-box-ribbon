﻿Imports System.Data
Imports System.Windows.Forms
Imports System.Data.SqlClient

Public Class frmInvoiceIntegrator
    Dim activatedAlready = False
    Dim ds_InvoiceList As DataTable
    Dim TaxPerc As Double
    Dim TaxCalc As Double
    Private Sub frmInvoiceIntegrator_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If activatedAlready Then Exit Sub
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        If Get_Default_Tax_Perc() < 0 Then
            MsgBox("There is no default tax type selected, please contact your administrator.", MsgBoxStyle.Information, "Tax Check")
            Me.Close()
        End If
        Call RunCheck()
    End Sub
    Function RunCheck()
        TSS_Note.Text = "Checking Suppliers"
        Dim CheckSupp As DataTable
        CheckSupp = Check_UnmappedSuppliers()
        If CheckSupp.Rows.Count > 0 Then
            TSS_Note.Text = "New Suppliers Require Mapping"

            If MsgBox("Add new supplier?" & vbNewLine & "If you select No, this process will cancel", MsgBoxStyle.YesNo, "Mapping Required") = MsgBoxResult.No Then
                CheckSupp = Nothing
                Me.Close()
                RunCheck = ""
                Exit Function
            End If
            For Each row As DataRow In CheckSupp.Rows
                Dim frmSupplierLink As New FrmLinkSupplier
                frmSupplierLink.LBL_SupplierName.Text = row("SupplierName").ToString
                frmSupplierLink.ShowDialog()
            Next
        Else
            TSS_Note.Text = "All Suppliers Mapped"
        End If
        CheckSupp = Nothing

        TSS_Note.Text = "Checking Items"
        Dim CheckItems As DataTable
        CheckItems = Check_UnmappedItems("Group")
        If CheckItems.Rows.Count > 0 Then
            TSS_Note.Text = "New Items Require Mapping"

            If MsgBox("Add new items?" & vbNewLine & "If you select No, this process will cancel", MsgBoxStyle.YesNo, "Mapping Required") = MsgBoxResult.No Then
                CheckItems = Nothing
                Me.Close()
                TSS_Note.Text = "Item mapping closed"
            Else
                For Each row As DataRow In CheckItems.Rows
                    Dim frmNewStock As New frmItemAdd
                    frmNewStock.TB_IntegrationCode.Text = row("CostGroup").ToString
                    frmNewStock.TB_ItemName.Text = row("CostGroup").ToString
                    frmNewStock.ShowDialog()
                Next
                TSS_Note.Text = "All Suppliers Mapped"
            End If

        Else

        End If
    End Function

    Private Sub frmInvoiceIntegrator_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.AppIcon
        Me.Text = "MagicBox -> " & CStr(My.Settings.CurrentCompany) & " -> Invoice Integrator -> Version : " & My.Settings.Setting_Version
        LoadCats1()
        LoadTax()
        LockForm(Me)
    End Sub

    Private Sub Btn_Refresh_Click(sender As Object, e As EventArgs) Handles Btn_Refresh.Click
        Call LoadInvoiceDate()
    End Sub
    Sub LoadInvoiceDate()
        DGV_InvoiceList.Enabled = False
        TSS_Progress.Style = Windows.Forms.ProgressBarStyle.Marquee
        TSS_Progress.Visible = True
        TSS_Progress.MarqueeAnimationSpeed = 30
        Application.DoEvents()
        TSS_Note.Text = "Loading Invoice Data"

        'If DGV_InvoiceList.RowCount > 0 Then
        ' DGV_InvoiceList.Rows.Clear()
        ' End If

        Dim ds_FullData As DataTable
        ds_FullData = InvoiceGRNData(DT_StartDate.Text, DT_EndDate.Text)
        If IsNothing(ds_FullData) Then Exit Sub
        Dim ds_InvList As DataTable
        Dim TobeDistinct As String() = {"Inv Date", "SupplierName", "SupplierID", "InvoiceNo"}
        ds_InvList = ds_FullData.DefaultView.ToTable(True, TobeDistinct)

        For i As Integer = ds_InvList.Rows.Count - 1 To 0 Step -1
            SS_Note2.Text = "Checking: " & ds_InvList.Rows(i).Item("InvoiceNo")
            Application.DoEvents()
            If InvoiceExsists(ds_InvList.Rows(i).Item("SupplierID"), ds_InvList.Rows(i).Item("InvoiceNo") & "~" & Date.Parse(ds_InvList.Rows(i).Item("Inv Date")).ToString("dd MMM yyyy")) = True Then
                ds_InvList.Rows(i).Delete()
            End If
        Next

        'clear
        lbl_InvDate.Text = ""
        lbl_SupplierName.Text = ""
        lbl_InvRef.Text = ""

        DGV_InvoiceList.DataSource = ds_InvList
        DGV_InvoiceList.Columns("SupplierID").Visible = False
        TSS_Progress.MarqueeAnimationSpeed = 0
        TSS_Note.Text = "Data loaded and ready"
        TSS_Progress.Visible = False
        DGV_InvoiceList.Enabled = True
        SS_Note2.Text = ""
        If DGV_InvoiceList.RowCount > 0 Then
            DGV_InvoiceList.Rows(0).Selected = True
        End If
    End Sub

    Private Sub DGV_InvoiceList_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DGV_InvoiceList.CellMouseDown
        Dim RwIndex As Long
        RwIndex = e.RowIndex
        If RwIndex = -1 Then Exit Sub
        Call LoadInvoice(DGV_InvoiceList.Rows(RwIndex).Cells("SupplierName").Value, DGV_InvoiceList.Rows(RwIndex).Cells("InvoiceNo").Value)
    End Sub
    Function LoadInvoice(SupplierName As String, InvoiceNo As String)
        Try
            Dim ds_Invoice As DataTable
            ds_Invoice = Get_InvoiceGRNLines(SupplierName, InvoiceNo)
            If ds_Invoice Is Nothing Then Exit Function
            lbl_InvDate.Text = Date.Parse(ds_Invoice.Rows(0).Item("InvDate")).ToString("dd MMM yyyy")
            lbl_SupplierName.Text = ds_Invoice.Rows(0).Item("SupplierName")
            lbl_InvRef.Text = ds_Invoice.Rows(0).Item("InvoiceNo")


            'Calculated Amount
            Dim dc As DataColumn
            dc = New DataColumn("AmountIncl")
            dc.Expression = "AmountExcl + AmountTax"
            ds_Invoice.Columns.Add(dc)
            DGV_InvoiceDetail.DataSource = ds_Invoice
            'Format
            DGV_InvoiceDetail.Columns("InvDate").Visible = False
            DGV_InvoiceDetail.Columns("TaxPerc").Visible = False
            DGV_InvoiceDetail.Columns("SupplierName").Visible = False
            DGV_InvoiceDetail.Columns("InvoiceNo").Visible = False
            DGV_InvoiceDetail.Columns("AmountExcl").DefaultCellStyle.Format = "c"
            DGV_InvoiceDetail.Columns("AmountIncl").DefaultCellStyle.Format = "c"
            DGV_InvoiceDetail.Columns("AmountTax").DefaultCellStyle.Format = "c"

            Call AddTotals()
        Catch ex As Exception
            MsgBox("Error loading invoice: " & ex.Message)
        End Try
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        
        Try
            If MsgBox("Are you sure you want to post this invoice?", MsgBoxStyle.YesNo, "Post Invoice") = MsgBoxResult.No Then Exit Sub
            Dim TxTable As DataTable = Globals.Ribbons.Ribbon1.TransactionsTable
            Dim HasVat As Boolean
            Dim BatchID As Long = oCreateBatchID()
            Dim TransactionID As Long = oCreateTransactionID()
            Dim TransactionDate As String
            Dim FinYear As Long
            Dim PPeriod As Long
            Dim Ref As String
            Dim Description As String
            Dim AmountExcl As Double
            Dim TaxAmount As Double
            Dim TaxPerc As Long
            Dim SupplierID As Long
            Dim SupplierName As String
            Dim DrCr As String
            Dim Account As String
            Dim TransType As String
            Dim DocType As String
            Dim Info As String
            Dim counter As Long
            Dim InvoiceSum As Double
            Dim CreditorControlAccount As String
            Dim VATControlAccount As String

            TxTable.Rows.Clear()

            SS_Note2.Text = "Prepping Load ..."
            Application.DoEvents()
            CreditorControlAccount = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
            VATControlAccount = Get_Segment4_Using_Type_in_Accounting("VAT Control")
            BatchID = oCreateBatchID()
            TransactionID = oCreateTransactionID()
            FinYear = GetFinYear(lbl_InvDate.Text)
            PPeriod = GetPeriod(lbl_InvDate.Text)
            TransactionDate = lbl_InvDate.Text
            Ref = lbl_InvRef.Text & "~" & lbl_InvDate.Text

            SupplierName = SQLConvert(lbl_SupplierName.Text)

            'Get the supplierID
            If SupplierName = "-" Then
                MsgBox("Please ensure you select an invoice. There was an validation error on the supplier name.", MsgBoxStyle.Information, "Validation")
                Exit Sub
            End If
            SupplierID = GetSupplierID_IntegrationCode(lbl_SupplierName.Text)
            If IsNothing(SupplierID) Then
                MsgBox("Please ensure you select an invoice. There was an validation error on the supplier name.", MsgBoxStyle.Information, "Validation")
                Exit Sub
            End If
            If SupplierID <= 0 Then
                MsgBox("Please ensure you select an invoice. There was an validation error on the supplier name.", MsgBoxStyle.Information, "Validation")
                Exit Sub
            End If

            Application.DoEvents()

            'Add Here
            InvoiceSum = Lbl_Total_Incl.Text
            If InvoiceSum < 0 Then
                'Create debit note if less than zero
                TransType = 1
                DocType = "Payment"
                Info = "Supplier Debit"
                TxDataTale_AddRow(BatchID, TransactionID, 0, TransactionDate, FinYear, PPeriod, Ref, "CREDITORS CONTROL", _
                         Math.Abs(InvoiceSum), 0, 0, "Dr", CreditorControlAccount, SupplierID, SupplierName, _
                          0, True, TransType, DocType, Info, Info, "")

            Else
                TransType = 12
                DocType = "Invoice"
                Info = "Invoice"
                TxDataTale_AddRow(BatchID, TransactionID, 0, TransactionDate, FinYear, PPeriod, Ref, "CREDITORS CONTROL", _
                         Math.Abs(InvoiceSum), 0, 0, "Cr", CreditorControlAccount, SupplierID, SupplierName, _
                          0, True, TransType, DocType, Info, Info, "")
            End If
            For Each Irw As DataGridViewRow In DGV_InvoiceDetail.Rows
                Description = Irw.Cells("ItemName").Value.ToString
                AmountExcl = Irw.Cells("AmountExcl").Value.ToString
                TaxAmount = Irw.Cells("AmountTax").Value.ToString
                TaxPerc = Irw.Cells("TaxPerc").Value.ToString
                Account = Get_Segment4(Irw.Cells("Segment 1 Desc").Value.ToString, Irw.Cells("Segment 2 Desc").Value.ToString, Irw.Cells("Segment 3 Desc").Value.ToString)

                If AmountExcl >= 0 Then
                    DrCr = "Dr"
                Else
                    DrCr = "Cr"
                End If
                TxDataTale_AddRow(BatchID, TransactionID, 0, TransactionDate, FinYear, PPeriod, Ref, Description, _
                         Math.Abs(AmountExcl), TaxPerc, Math.Abs(TaxAmount), DrCr, Account, SupplierID, SupplierName, _
                          0, True, TransType, DocType, Info, Info, "")

                If TaxPerc <> 0 Then
                    TxDataTale_AddRow(BatchID, TransactionID, 0, TransactionDate, FinYear, PPeriod, Ref, "VAT CONTROL", _
                         Math.Abs(TaxAmount), 0, 0, DrCr, VATControlAccount, SupplierID, SupplierName, _
                          0, True, TransType, DocType, Info, Info, "")
                End If
                SS_Note2.Text = "Adding " & Ref & " to batch"
            Next
            'end add


            SS_Note2.Text = "Posting Batch..."
            Application.DoEvents()
            If PostTxDataTable() = True Then
                MsgBox("Invoice(s) successfuly posted", MsgBoxStyle.Information, "Success")
                LoadInvoiceDate()
            Else
            End If
            SS_Note2.Text = "Ready"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub AddTotals()
        Dim AmtExcl As Double
        Dim AmtInc As Double
        Dim AmtVAT As Double
        AmtInc = 0
        AmtExcl = 0
        AmtVAT = 0
        For Each row As DataGridViewRow In DGV_InvoiceDetail.Rows
            AmtExcl = row.Cells("AmountExcl").Value + AmtExcl
            AmtInc = row.Cells("AmountIncl").Value + AmtInc
            AmtVAT = row.Cells("AmountTax").Value + AmtVAT
        Next

        If CB_IsPaid.Checked = True Then
            Lbl_PaidAmount.Text = Val(TB_PaidAmount.Text)
        End If

        Lbl_Total_Excl.Text = AmtExcl.ToString("c")
        Lbl_Total_Incl.Text = AmtInc.ToString("c")
        Lbl_Total_Vat.Text = AmtVAT.ToString("c")
        lbl_Outstanding.Text = (AmtInc - Val(Lbl_PaidAmount.Text)).ToString("c")
    End Sub
    Private Sub GAAPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GAAPToolStripMenuItem.Click
        Dim frmImport As New Frm_ImportGRV
        frmImport.LBL_ImportType.Text = "GAAP"
        frmImport.ShowDialog()
        Call RunCheck()
    End Sub

    Private Sub MyMicrosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MyMicrosToolStripMenuItem.Click
        Dim frmImport As New Frm_ImportGRV
        frmImport.LBL_ImportType.Text = "Micros"
        frmImport.ShowDialog()
        Call RunCheck()
    End Sub

    Private Sub EditItemsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditItemsToolStripMenuItem.Click
        Dim frmItemEdit As New frmItemEdit
        frmItemEdit.ShowDialog()
    End Sub
    Sub LoadCats1()
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        ds = ds.DefaultView.ToTable(True, "Segment 1 Desc")
        CB_Cat1.DataSource = ds
        CB_Cat1.DisplayMember = "Segment 1 Desc"
        ds = Nothing
    End Sub

    Private Sub CB_Cat1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Cat1.SelectedIndexChanged
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        If CB_Cat1.Text.ToString = "" Then Exit Sub
        ds.DefaultView.RowFilter = "[Segment 1 Desc] = '" & CB_Cat1.Text & "'"
        ds = ds.DefaultView.ToTable(True, "Segment 2 Desc")
        CB_Cat2.DataSource = ds
        CB_Cat2.DisplayMember = "Segment 2 Desc"
        ds = Nothing
    End Sub

    Private Sub CB_Cat2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Cat2.SelectedIndexChanged
        Dim ds As DataTable = Globals.Ribbons.Ribbon1.AccountsTable.Copy
        If CB_Cat2.Text = "" Then Exit Sub
        ds.DefaultView.RowFilter = "[Segment 2 Desc] = '" & CB_Cat2.Text & "'"
        ds = ds.DefaultView.ToTable(True, "Segment 3 Desc")
        CB_Cat3.DataSource = ds
        CB_Cat3.DisplayMember = "Segment 3 Desc"
        ds = Nothing
    End Sub

    Private Sub DGV_InvoiceDetail_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_InvoiceDetail.CellClick
        Dim RwIndex As Long = e.RowIndex
        If RwIndex = -1 Then Exit Sub
        CB_Vat.SelectedValue = DGV_InvoiceDetail.Item("TaxPerc", e.RowIndex).Value.ToString
        TB_LineDesc.Text = DGV_InvoiceDetail.Item("ItemName", e.RowIndex).Value.ToString
        CB_Cat1.Text = DGV_InvoiceDetail.Item("Segment 1 Desc", e.RowIndex).Value.ToString
        CB_Cat2.Text = DGV_InvoiceDetail.Item("Segment 2 Desc", e.RowIndex).Value.ToString
        CB_Cat3.Text = DGV_InvoiceDetail.Item("Segment 3 Desc", e.RowIndex).Value.ToString
        TB_ExclAmount.Text = CDbl(DGV_InvoiceDetail.Item("AmountExcl", e.RowIndex).Value).ToString("N2")
        TB_TaxAmount.Text = CDbl(DGV_InvoiceDetail.Item("AmountTax", e.RowIndex).Value).ToString("N2")
        TB_AmountIncl.Text = CDbl(DGV_InvoiceDetail.Item("AmountIncl", e.RowIndex).Value).ToString("N2")
    End Sub
    Function LoadTax() As Boolean

        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [TaxPerc] as Value, [TaxDesc] as Display FROM TaxTypes"

        If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        '  cmbVATType.Items.Clear()
        '  VATType_ComboBox.Items.Clear()

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Vat.DataSource = ds.Tables(0)
            CB_Vat.ValueMember = "Value"
            CB_Vat.DisplayMember = "Display"
            CB_Vat.SelectedIndex = 0

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Private Sub TB_ExclAmount_KeyUp(sender As Object, e As KeyEventArgs) Handles TB_ExclAmount.KeyUp
        If IsNumeric(TB_ExclAmount.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        TaxPerc = CB_Vat.SelectedValue.ToString
        AmountExcl = TB_ExclAmount.Text

        If CStr(TaxPerc) <> 0 Then
            AmountIncl = Math.Round(AmountExcl * 1.14, 2)
        Else
            TaxCalc = 1 + (TaxPerc / 100)
            AmountIncl = Math.Round(AmountExcl * TaxCalc, 2)
        End If
        TB_AmountIncl.Text = AmountIncl
        TB_TaxAmount.Text = AmountIncl - AmountExcl

    End Sub

    Private Sub TB_AmountIncl_KeyUp(sender As Object, e As KeyEventArgs) Handles TB_AmountIncl.KeyUp
        If IsNumeric(TB_ExclAmount.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        Dim oVatType As Integer
        Dim oVatAmount As Decimal

        TaxPerc = CB_Vat.SelectedValue.ToString

        AmountIncl = TB_AmountIncl.Text
        If CStr(TaxPerc) <> 0 Then
            TaxCalc = 1 + (TaxPerc / 100)
            AmountExcl = Math.Round(AmountIncl / TaxCalc, 2)
        Else
            AmountExcl = AmountIncl
        End If
        TB_ExclAmount.Text = AmountExcl
        TB_TaxAmount.Text = AmountIncl - AmountExcl
    End Sub

    Private Sub CB_Vat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Vat.SelectedIndexChanged
        If IsNumeric(TB_ExclAmount.Text) = False Then Exit Sub
        Dim AmountExcl As Double
        Dim AmountIncl As Double
        Dim oVatType As Integer
        Dim oVatAmount As Decimal

        TaxPerc = CB_Vat.SelectedValue.ToString

        AmountExcl = TB_ExclAmount.Text
        If CStr(TaxPerc) <> 0 Then
            TaxCalc = 1 + (TaxPerc / 100)
            AmountIncl = Math.Round(AmountExcl * TaxCalc, 2)
        Else
            AmountIncl = AmountExcl
        End If
        TB_AmountIncl.Text = AmountIncl
        TB_TaxAmount.Text = AmountIncl - AmountExcl
    End Sub

    Private Sub Btn_LineEdit_Click(sender As Object, e As EventArgs) Handles Btn_LineEdit.Click
        Dim RwIndex = DGV_InvoiceDetail.CurrentCell.RowIndex
        DGV_InvoiceDetail.Item("ItemName", RwIndex).Value = TB_LineDesc.Text
        DGV_InvoiceDetail.Item("Segment 1 Desc", RwIndex).Value = CB_Cat1.Text
        DGV_InvoiceDetail.Item("Segment 2 Desc", RwIndex).Value = CB_Cat2.Text
        DGV_InvoiceDetail.Item("Segment 3 Desc", RwIndex).Value = CB_Cat3.Text
        DGV_InvoiceDetail.Item("AmountExcl", RwIndex).Value = TB_ExclAmount.Text
        DGV_InvoiceDetail.Item("AmountTax", RwIndex).Value = TB_TaxAmount.Text
        DGV_InvoiceDetail.Item("AmountIncl", RwIndex).Value = TB_AmountIncl.Text
        DGV_InvoiceDetail.Item("TaxDesc", RwIndex).Value = CB_Vat.SelectedText
        DGV_InvoiceDetail.Item("TaxPerc", RwIndex).Value = CB_Vat.SelectedValue
    End Sub
    Private Sub TB_PaidAmount_KeyDown(sender As Object, e As KeyEventArgs) Handles TB_PaidAmount.KeyDown
        Call AddTotals()
    End Sub

    Private Sub CB_IsPaid_CheckedChanged(sender As Object, e As EventArgs) Handles CB_IsPaid.CheckedChanged
        Call AddTotals()
    End Sub

    Private Sub BTN_AddLine_Click(sender As Object, e As EventArgs) Handles BTN_AddLine.Click
    End Sub

    Private Sub DGV_InvoiceList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_InvoiceList.CellContentClick

    End Sub
End Class