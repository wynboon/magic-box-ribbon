﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemAdd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TB_ItemCode = New System.Windows.Forms.TextBox()
        Me.TB_ItemName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_IntegrationCode = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CB_HasVAT = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LBL_CostGroup = New System.Windows.Forms.Label()
        Me.CB_Cat1 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CB_Cat2 = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CB_Cat3 = New System.Windows.Forms.ComboBox()
        Me.Btn_ItemAdd = New System.Windows.Forms.Button()
        Me.Btn_Cancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Item Code"
        '
        'TB_ItemCode
        '
        Me.TB_ItemCode.Location = New System.Drawing.Point(146, 26)
        Me.TB_ItemCode.Name = "TB_ItemCode"
        Me.TB_ItemCode.Size = New System.Drawing.Size(127, 22)
        Me.TB_ItemCode.TabIndex = 1
        '
        'TB_ItemName
        '
        Me.TB_ItemName.Location = New System.Drawing.Point(146, 54)
        Me.TB_ItemName.Name = "TB_ItemName"
        Me.TB_ItemName.Size = New System.Drawing.Size(310, 22)
        Me.TB_ItemName.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Item Name"
        '
        'TB_IntegrationCode
        '
        Me.TB_IntegrationCode.Location = New System.Drawing.Point(146, 82)
        Me.TB_IntegrationCode.Name = "TB_IntegrationCode"
        Me.TB_IntegrationCode.ReadOnly = True
        Me.TB_IntegrationCode.Size = New System.Drawing.Size(310, 22)
        Me.TB_IntegrationCode.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(27, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Integration Code"
        '
        'CB_HasVAT
        '
        Me.CB_HasVAT.AutoSize = True
        Me.CB_HasVAT.Location = New System.Drawing.Point(146, 111)
        Me.CB_HasVAT.Name = "CB_HasVAT"
        Me.CB_HasVAT.Size = New System.Drawing.Size(18, 17)
        Me.CB_HasVAT.TabIndex = 6
        Me.CB_HasVAT.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 111)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Item Vatable"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 137)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 17)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Item Vatable"
        Me.Label5.Visible = False
        '
        'LBL_CostGroup
        '
        Me.LBL_CostGroup.AutoSize = True
        Me.LBL_CostGroup.Location = New System.Drawing.Point(143, 137)
        Me.LBL_CostGroup.Name = "LBL_CostGroup"
        Me.LBL_CostGroup.Size = New System.Drawing.Size(13, 17)
        Me.LBL_CostGroup.TabIndex = 9
        Me.LBL_CostGroup.Text = "-"
        Me.LBL_CostGroup.Visible = False
        '
        'CB_Cat1
        '
        Me.CB_Cat1.FormattingEnabled = True
        Me.CB_Cat1.Location = New System.Drawing.Point(146, 158)
        Me.CB_Cat1.Name = "CB_Cat1"
        Me.CB_Cat1.Size = New System.Drawing.Size(310, 24)
        Me.CB_Cat1.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 165)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 17)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Category 1"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(27, 195)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(77, 17)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Category 2"
        '
        'CB_Cat2
        '
        Me.CB_Cat2.FormattingEnabled = True
        Me.CB_Cat2.Location = New System.Drawing.Point(146, 188)
        Me.CB_Cat2.Name = "CB_Cat2"
        Me.CB_Cat2.Size = New System.Drawing.Size(310, 24)
        Me.CB_Cat2.TabIndex = 12
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 225)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 17)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Category 3"
        '
        'CB_Cat3
        '
        Me.CB_Cat3.FormattingEnabled = True
        Me.CB_Cat3.Location = New System.Drawing.Point(146, 218)
        Me.CB_Cat3.Name = "CB_Cat3"
        Me.CB_Cat3.Size = New System.Drawing.Size(310, 24)
        Me.CB_Cat3.TabIndex = 14
        '
        'Btn_ItemAdd
        '
        Me.Btn_ItemAdd.Location = New System.Drawing.Point(30, 264)
        Me.Btn_ItemAdd.Name = "Btn_ItemAdd"
        Me.Btn_ItemAdd.Size = New System.Drawing.Size(134, 54)
        Me.Btn_ItemAdd.TabIndex = 16
        Me.Btn_ItemAdd.Text = "Add Item"
        Me.Btn_ItemAdd.UseVisualStyleBackColor = True
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.Location = New System.Drawing.Point(322, 264)
        Me.Btn_Cancel.Name = "Btn_Cancel"
        Me.Btn_Cancel.Size = New System.Drawing.Size(134, 54)
        Me.Btn_Cancel.TabIndex = 17
        Me.Btn_Cancel.Text = "Cancel"
        Me.Btn_Cancel.UseVisualStyleBackColor = True
        '
        'frmItemAdd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(468, 329)
        Me.Controls.Add(Me.Btn_Cancel)
        Me.Controls.Add(Me.Btn_ItemAdd)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.CB_Cat3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.CB_Cat2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.CB_Cat1)
        Me.Controls.Add(Me.LBL_CostGroup)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CB_HasVAT)
        Me.Controls.Add(Me.TB_IntegrationCode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TB_ItemName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TB_ItemCode)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmItemAdd"
        Me.Text = "Item Add"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TB_ItemCode As System.Windows.Forms.TextBox
    Friend WithEvents TB_ItemName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB_IntegrationCode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CB_HasVAT As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents LBL_CostGroup As System.Windows.Forms.Label
    Friend WithEvents CB_Cat1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CB_Cat2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents CB_Cat3 As System.Windows.Forms.ComboBox
    Friend WithEvents Btn_ItemAdd As System.Windows.Forms.Button
    Friend WithEvents Btn_Cancel As System.Windows.Forms.Button
End Class
