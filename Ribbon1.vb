﻿Imports Microsoft.Office.Tools.Ribbon
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.Win32
Imports Microsoft.Reporting.WinForms
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Runtime.InteropServices

Public Class Ribbon1
    Public LoggedUsersDataSet As DataTable
    Public LoggedUsersRoles As DataTable
    Public TaxTable As DataTable
    Public AccountsTable As DataTable
    Public TransactionsTable As DataTable
    Public JournalTemplate As DataTable
    Public GlobalAdmin As Boolean = False
    Public CurrentTenantID As Long
    Public CurrentCompanyID As Long
    Public CurrentGroupID As Long
    Public ShouldLoadGroups As Boolean
    Public ActiveDatabase
    Public RemittanceTable As DataTable
    Public ExcludedPayTypes As String
    Public OrderX_token As String

    Private Sub Ribbon1_Close(sender As Object, e As EventArgs) Handles Me.Close
        Me.NI_ServerStatus.Dispose()
    End Sub

    Private Sub Ribbon1_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.NI_ServerStatus.Dispose()
    End Sub
    Private Sub Ribbon1_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load
        ' BackgroundWorker1.RunWorkerAsync()
        DB_BackgroundWorker.RunWorkerAsync()

        Try
            Globals.Ribbons.Ribbon1.Group_Home.Visible = False
            Call Hidegroups()
            'Tested this for a ribbon gallary - it works
            Call BuildGallary()
            Call BuildBIGallary()
            '  Tmr_ServerCheck.Interval = 30000
            '  Tmr_ServerCheck.Enabled = False


            TransactionsTable = CreateTransactionDataTable()
            RemittanceTable = CreateRemittanceDataTable()
            LoggedUsersDataSet = New DataTable
            LoggedUsersRoles = New DataTable
            Me.txtPassword.Visible = True

            ' If My.Settings.User_Control_On = "Yes" Then
            ' Me.GrpLogin.Visible = True
            ' Else
            ' Me.GrpLogin.Visible = False
            ' End If

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            ' If My.Settings.ListViewValues <> String.Empty Then

            '            If My.Settings.CS_Setting = String.Empty Then

            'MessageBox.Show("You have not specified the default connection / Company, please specify this details on the Settings screen that will apper after this dialog in order to continue with using the MagicBox. ", "Database Connectivity", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Dim frmS As New frmSettings
            'frmS.ShowDialog()

            'End If
            'Else
            'MessageBox.Show("Your system does not have database connectity settings, please specify this details on the Settings screen that will apper after this dialog in order to continue with using the MagicBox.", "Database Connectivity", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Dim frmS As New frmSettings
            'frmS.ShowDialog()
            'End If

        Catch ex As Exception
            MessageBox.Show("Please check if your current database connection details are specified  correctly" & vbNewLine & ex.Message, "Database Connections", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive"
            Globals.Ribbons.Ribbon1.btnOnOff.Image = My.Resources.Off2
        End Try
    End Sub
    Private Sub Button_CaptureInvoice_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button_CaptureInvoice.Click
        Try
            Dim frmInvCapture As New frmInvoice_Capture
            frmInvCapture.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Invoice Capture -> Version : " & My.Settings.Setting_Version
            frmInvCapture.ComboBox_Status.Text = "UNPAID"
            frmInvCapture.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Invoice Capture form " & Err.Description)
        End Try
    End Sub
    Private Sub Button_Suppliers_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)
        'DECLARED GLOBALLY
        Try
            Dim frmSup As New frmSuppliers
            frmSup.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Suppliers form " & Err.Description)
        End Try

    End Sub

    'Private Sub Button_Periods_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button_Periods.Click

    '    Try
    '        If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then
    '            MsgBox("The Magic Box application is inactive due to being turned off or lack of a network connection. Please turn it on first!")
    '            Exit Sub
    '        End If
    '        If My.Settings.User_Control_On = "Yes" Then
    '            If Me.lblUsername.Label = "-" Then
    '                MsgBox("Please login first!")
    '                Exit Sub
    '            End If
    '        End If
    '        Dim Prds As New frmPeriods
    '        Prds.ShowDialog()
    '    Catch ex As Exception
    '        MsgBox("There was an error viewing the Periods form " & Err.Description)
    '    End Try

    'End Sub

    Private Sub Button_Settings_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button_Settings.Click
        Try
            'DECLARED GLOBALLY
            'Dim Form_Supplier_Payments As New Supplier_Payments
            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then
                MsgBox("The Magic Box application is inactive due to being turned off or lack of a network connection. Please turn it on first!")
                Exit Sub
            End If
            Dim frmSettings As New frmSettings
            frmSettings.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Supplier Payments form " & Err.Description)
        End Try
    End Sub

    Private Sub Button_AccountingSync_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button_AccountingSync.Click
        Try
            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then
                MsgBox("The Magic Box application is inactive due to being turned off or lack of a network connection. Please turn it on first!")
                Exit Sub
            End If
            If My.Settings.User_Control_On = "Yes" Then
                If Me.lblUsername.Label = "-" Then
                    MsgBox("Please login first!")
                    Exit Sub
                End If
            End If
            Dim Form_AccountMaintenance As New frmAccountMaintenance
            Form_AccountMaintenance.Show()
            Form_AccountMaintenance.BringToFront()
        Catch ex As Exception
            MsgBox("There was an error viewing the Accounts form " & Err.Description)
        End Try
    End Sub

    Sub zzzAppendAccounting(ByVal oSegment1 As String, ByVal oSeg1Desc As String, ByVal oSegment2 As String, ByVal oSeg2Desc As String, _
             ByVal oSegment3 As String, ByVal oSeg3Desc As String, ByVal oSegment4 As String, ByVal GLACCOUNT As String, ByVal AccountType As String, oRowNumber As Integer)
        Try

            'Dim MYDOC_DIR As String = Environ("userprofile") & "\my documents"
            'Dim oFullPath As String = MYDOC_DIR & "\MagicBox.accdb"

            'Dim myConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"
            Dim sSQL As String

            sSQL = "INSERT INTO Accounting ( [Segment 1], [Segment 1 Desc], [Segment 2], [Segment 2 Desc], [Segment 3], [Segment 3 Desc], [Segment 4], [GL ACCOUNT], [Account Type] ) "
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & "'" & SQLConvert(oSegment1) & "' as Expr1, "
            sSQL = sSQL & "'" & SQLConvert(oSeg1Desc) & "' as Expr2, "
            sSQL = sSQL & "'" & SQLConvert(oSegment2) & "' as Expr3, "
            sSQL = sSQL & "'" & SQLConvert(oSeg2Desc) & "' as Expr4, "
            sSQL = sSQL & "'" & SQLConvert(oSegment3) & "' as Expr5, "
            sSQL = sSQL & "'" & SQLConvert(oSeg3Desc) & "' as Expr6, "
            sSQL = sSQL & "'" & SQLConvert(oSegment4) & "' as Expr7, "
            sSQL = sSQL & "'" & SQLConvert(GLACCOUNT) & "' as Expr8, "
            sSQL = sSQL & "'" & SQLConvert(AccountType) & "' as Expr9"

            Dim cn As New OleDbConnection(My.Settings.CS_Setting)

            '// define the sql statement to execute
            Dim cmd As New OleDbCommand(sSQL, cn)

            '    '// open the connection
            cn.Open()



            'the following line is for testing purposes - need to do something if 
            Dim xlWb As Excel.Workbook = Globals.ThisAddIn.Application.ActiveWorkbook
            Dim xlWs As Excel.Worksheet = xlWb.Sheets("Journal Reference")

            xlWs.Cells(oRowNumber, 10).value = "" 'this clears cell so that if not loaded will be empty

            cmd.ExecuteNonQuery()

            xlWs.Cells(oRowNumber, 10).value = "Loaded"

        Catch ex As Exception
            MsgBox(ex.Message & " 193")

        End Try
    End Sub

    Sub zzzzDelete_All_Accounting_Table()
        Try

            'Dim MYDOC_DIR As String = Environ("userprofile") & "\my documents"
            'Dim oFullPath As String = MYDOC_DIR & "\MagicBox.accdb"

            'Dim myConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & oFullPath & ";"
            Dim sSQL As String

            sSQL = "Delete * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim cn As New OleDbConnection(My.Settings.CS_Setting)

            '// define the sql statement to execute
            Dim cmd As New OleDbCommand(sSQL, cn)

            '    '// open the connection
            cn.Open()

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub


    Function SQLConvert(ByVal sString As String) As String
        SQLConvert = Replace(sString, "'", "''")
    End Function
    Private Sub Button_StockTake_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button_CreditNotes.Click

        Try
            Dim FCN As New frmCreditNotes
            FCN.Show()
        Catch ex As Exception
            MsgBox("Error opening the credit notes interface" & Err.Description)
        End Try
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)
        Try
            Dim Form_Queries As New Queries
            Form_Queries.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Invoice and Payments Queries " & Err.Description)
        End Try
    End Sub
    Private Sub Btn_RM_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)

        Try
            If Environment.Is64BitOperatingSystem Then
                LaunchReportManager()
            Else
                LaunchApp("ReportManager")
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub Btn_Reports_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Btn_Reports.Click
        If Environment.Is64BitOperatingSystem Then
            LaunchViewer()
        Else
            LaunchApp("Viewer")
        End If
    End Sub
    Sub LaunchApp(ByVal Name As String)
        Try
            Dim objIntegrator As Alchemex.NET.Integration.SDK.Integrator
            objIntegrator = New Alchemex.NET.Integration.SDK.Integrator
            objIntegrator.LoadLicenseIntegrationLibrary()


            Dim ConnString As String() = My.Settings.CS_Setting.Split(New Char() {"="c})

            Dim ServerSplit() As String = ConnString(1).Split(New Char() {";"c})
            Dim Servername As String = ServerSplit(0).ToString
            Dim DatabaseSplit() As String = ConnString(2).Split(New Char() {";"c})
            Dim DatabaseName As String = DatabaseSplit(0).ToString
            Dim UserSplit() As String = ConnString(3).Split(New Char() {";"c})
            Dim UserName As String = UserSplit(0).ToString
            Dim Password As String = ConnString(4).ToString

            With objIntegrator.DatabaseConnectionDetails
                .Catalog = DatabaseName
                .Server = Servername
                .UserName = UserName
                .Password = Password
            End With

            objIntegrator.SystemUser.User = "[Default]"
            objIntegrator.TenantCode = "BuildSmart"

            'Dim TenStoresNames As String = "(" & "'"
            'Dim TenStoresIDs As String = "("

            'For Each item In TenantEnabledStores
            '    TenStoresNames &= item.value & "', '" : TenStoresIDs &= item.key & ", "
            'Next

            'TenStoresNames = TenStoresNames.Substring(0, TenStoresNames.Length - 4) & ""
            'TenStoresIDs = TenStoresIDs.Substring(0, TenStoresIDs.Length - 2) & ""
            'TenStoresNames &= "')" : TenStoresIDs &= ")"

            objIntegrator.AddSessionVariable("@CurrentCompanyID@", My.Settings.Setting_CompanyID, True)
            objIntegrator.AddSessionVariable("@CurrentCompanyName@", CStr(GetCompanyName()), True)
            'objIntegrator.AddSessionVariable("@UserGroupID@", My.Settings.GroupID, True)
            'objIntegrator.AddSessionVariable("@UserFullNames", My.Settings.Firstname & " " & My.Settings.Surname, True)
            'objIntegrator.AddSessionVariable("@ListOfUserActiveStoresNames@", CStr(TenStoresNames), True)
            'objIntegrator.AddSessionVariable("@ListOfUserActiveStoresIDs@", CStr(TenStoresIDs), True)
            objIntegrator.AddSessionVariable("@CurrentFinancialYear@", GetFinYear(Now.Date.ToString("dd MMMM yyyy")), True)
            objIntegrator.AddSessionVariable("@CurrentPeriod@", GetPeriod(Now.Date.ToString("dd MMMM yyyy")), True)
            objIntegrator.AddSessionVariable("@CurrentUser@", UserName, True)
            objIntegrator.AddSessionVariable("@CurrentGroup@", CurrentGroupID, True)

            ' MsgBox(objIntegrator.MetaDataExists())

            If Name = "ReportManager" Then
                objIntegrator.Launcher.LaunchReportManager()
            End If

            If Name = "Administrator" Then
                objIntegrator.Launcher.LaunchAdministratorTool()
            End If

            If Name = "Viewer" Then
                objIntegrator.Launcher.LaunchViewer()
            End If

            If Name = "SecurityManager" Then
                objIntegrator.Launcher.LaunchSecurityManager()
            End If

            If Name = "LicenseManager" Then
                objIntegrator.Launcher.LaunchLicenseManager()
            End If


            objIntegrator = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, _
            "An Error Occurred", _
            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Btn_Admin_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)
        Try
            If Environment.Is64BitOperatingSystem Then
                LaunchAdministrator()
            Else
                LaunchApp("Administrator")
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub Btn_LicMan_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)

        Try
            If Environment.Is64BitOperatingSystem Then
                LaunchLicenseManager()
            Else
                LaunchApp("LicenseManager")
            End If

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button2.Click
        Try
            Dim frmSpaymentsV2 As New frmSupplierPaymentsV2
            frmSpaymentsV2.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Supplier Payments form " & Err.Description)
        End Try
    End Sub

    Private Sub Button_Company_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button_Company.Click
        Try
            Dim Form_Company As New frmCompanyDetails
            Form_Company.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Company form " & Err.Description)
        End Try
    End Sub

    Private Sub Button_Journals_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)
        Try
            Dim Form_Journal As New frmJournals
            Form_Journal.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Journal form " & Err.Description)
        End Try
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button3.Click
        Try
            ''Dim Form_Customer_Invoice As New Invoice_Customer
            '' Form_Customer_Invoice.Show()
        Catch ex As Exception
            MsgBox("There was an error opening the Customer Invoice " & Err.Description)
        End Try
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button4.Click
        Try
            Dim Form_Products As New Products
            Form_Products.Show()
        Catch ex As Exception
            MsgBox("There was an error opening the Products Form " & Err.Description)
        End Try
    End Sub
    Private Sub btnCreditNotes_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnCreditNotes.Click
        Try
            Dim frmCNA As New frmCreditNoteApprovals
            frmCNA.Show()
            frmCNA.BringToFront()
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnEFTApprovals_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnEFTApprovals.Click
        Try
            Dim frmEFTA As New frmEFT_Approvals
            frmEFTA.Show()
            frmEFTA.BringToFront()
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Button_Transactions_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)
        Try
            Dim frmTransactions As New frmTransactions
            frmTransactions.Show()
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button1.Click
        'DECLARED GLOBALLY
        'Dim Form_InvoiceCapture As New Invoice_Capture
        Try
            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then
                MsgBox("The Magic Box application is inactive due to being turned off or lack of a network connection. Please turn it on first!")
                Exit Sub
            End If

        Catch ex As Exception
            MsgBox("There was an error viewing the Import/Export form " & Err.Description)
        End Try
    End Sub

    Private Sub btnEmployees_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs)

        Try
            Dim Form_Employees As New frmEmployees
            Form_Employees.Show()
            Form_Employees.BringToFront()
        Catch ex As Exception
            MsgBox(ex.Message & " 196")
        End Try
    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles Button6.Click
        Try
            Dim IC As New frmInvoice_Capture
            IC.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Supplier Debit -> Version : " & My.Settings.Setting_Version
            IC.Show()
            IC.GroupBox3.Visible = False
            IC.chkCredit.Visible = False
            IC.EditInvoices_CheckBox.Visible = False
            IC.chkNotes.Visible = False
        Catch ex As Exception
            MsgBox("There was a problem opening the Supplier Debits form! " & ex.Message)
        End Try
    End Sub

    Private Sub btnOnOff_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnOnOff.Click
        Try


            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then
                'If is inactive then try to activate
                If TestServer() = False Then
                    Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive"
                    Globals.Ribbons.Ribbon1.btnOnOff.Image = My.Resources.Off2
                    MsgBox("No internet or server connection! The Magic Box add-in can not be used.")
                Else
                    ' Call oStartUp()
                    Globals.Ribbons.Ribbon1.btnOnOff.Label = "Active"
                    Globals.Ribbons.Ribbon1.btnOnOff.Image = My.Resources.On2
                End If
            Else
                'If is active then merely deactivate
                Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive"
                Globals.Ribbons.Ribbon1.btnOnOff.Image = My.Resources.Off2

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnAllocations_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnAllocations.Click
        Try
            Dim Form_Allocations As New frmAllocations
            Form_Allocations.Show()
            Form_Allocations.BringToFront()
        Catch ex As Exception
            MsgBox("Error opening Allocations form" & ex.Message)
        End Try
    End Sub

    Public Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnLogin.Click

        Dim oUserName As String
        Dim oPassword As String
        Dim oLevel As String
        Dim ApprovedPayments As String
        Dim cs As String = My.Settings.CS_Setting
        Dim blnIncorrectLoginInfo As Boolean


        If Globals.Ribbons.Ribbon1.btnLogin.Label = "Log out" Then
            Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
            Globals.Ribbons.Ribbon1.lblGroup.Label = "-"
            Globals.Ribbons.Ribbon1.btnLogin.Label = "Log in"
            Globals.Ribbons.Ribbon1.txtUserName.Text = ""
            Globals.Ribbons.Ribbon1.txtPassword.Text = ""
            Me.lblUsername.Label = "-"
            Exit Sub
        End If


        If My.Settings.DBType = "Access" Then


            Dim myConnection As OleDbConnection = New OleDbConnection(cs)
            Dim SQL As String = "SELECT * From Users Where Co_ID = " & My.Settings.Setting_CompanyID

            Dim command As OleDbCommand = New OleDbCommand(SQL, myConnection)
            Try
                Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                myConnection.Open()
                Dim reader As OleDbDataReader = command.ExecuteReader()
                If reader.HasRows Then
                    Do While reader.Read()
                        If Not reader("Username").Equals(DBNull.Value) Then
                            oUserName = reader("Username").ToString()
                        Else
                            oUserName = ""
                        End If
                        If Not reader("Password").Equals(DBNull.Value) Then
                            oPassword = reader("Password").ToString()
                        Else
                            oPassword = ""
                        End If

                        If Not reader("Level").Equals(DBNull.Value) Then
                            oLevel = reader("Level").ToString()
                        Else
                            oLevel = ""
                        End If

                        If oUserName <> "" Then

                            If oUserName = Me.txtUserName.Text Then
                                If oPassword = Me.txtPassword.Text Then
                                    Globals.Ribbons.Ribbon1.lblUsername.Label = oUserName
                                    Globals.Ribbons.Ribbon1.lblGroup.Label = oLevel
                                    Globals.Ribbons.Ribbon1.btnLogin.Label = "Log out"
                                    Me.txtPassword.Text = ""
                                    GoTo Jumps
                                Else
                                    Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                                    MsgBox("The username does not exist!")
                                    Exit Sub
                                End If
                            End If
                        End If
                    Loop
                Else


                End If

                If Globals.Ribbons.Ribbon1.lblUsername.Label = "-" Then
                    MsgBox("Username and password incorrect!")
                End If

                ''Globals.Ribbons.Ribbon1.lblUsername.Label = ""
Jumps:
                reader.Close()

                myConnection.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        Else

            Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim SQL As String = "SELECT * From Users Where Co_ID = " & My.Settings.Setting_CompanyID
            Dim command As New SqlCommand(SQL, myConnection)
            command.CommandTimeout = 300
            Try
                Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                myConnection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader()
                If reader.HasRows Then
                    Do While reader.Read()
                        If Not reader("Username").Equals(DBNull.Value) Then
                            oUserName = reader("Username").ToString()
                        Else
                            oUserName = ""
                        End If
                        If Not reader("Password").Equals(DBNull.Value) Then
                            oPassword = reader("Password").ToString()
                        Else
                            oPassword = ""
                        End If

                        If Not reader("Level").Equals(DBNull.Value) Then
                            oLevel = reader("Level").ToString()
                        Else
                            oLevel = ""
                        End If

                        If oUserName <> "" Then
                            LoggedInUser = oUserName
                            If oUserName = Me.txtUserName.Text Then
                                If oPassword = Me.txtPassword.Text Then

                                    Globals.Ribbons.Ribbon1.lblUsername.Label = oUserName
                                    Globals.Ribbons.Ribbon1.lblGroup.Label = oLevel
                                    Globals.Ribbons.Ribbon1.btnLogin.Label = "Log out"
                                    Me.txtPassword.Text = ""

                                    If Not reader("Approve_Payments_Types").Equals(DBNull.Value) Then
                                        ApprovedPayments = reader("Approve_Payments_Types").ToString
                                    Else
                                        ApprovedPayments = ""
                                    End If

                                    'Herold New section
                                    '15 January 2014
                                    If ApprovedPayments <> String.Empty Then
                                        LoggedUser_Roles = ApprovedPayments.Split(New Char() {","c})
                                    End If
                                    My.Settings.LoggedInUser = oUserName
                                    My.Settings.Save()
                                    GoTo Jump
                                Else
                                    Globals.Ribbons.Ribbon1.lblUsername.Label = "-"
                                    MsgBox("Password is incorrect!")
                                    Exit Sub
                                End If
                            End If
                        End If
                    Loop
                Else

                End If

                If Globals.Ribbons.Ribbon1.lblUsername.Label = "-" Then
                    MsgBox("The username does not exist!")
                End If

Jump:
                reader.Close()

                myConnection.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnCashSpotCheck_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnCashSpotCheck.Click
        Try
            Dim frmCSP As New frmCashSpotCheck
            frmCSP.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnAdmin_Click(ByVal sender As System.Object, ByVal e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnAdmin.Click
        Try
            Dim Frm_Administration As New Frm_Administration
            Frm_Administration.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnMaintenance_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles btnMaintenance.Click
        Try
            Dim frm_trasEdit As New Frm_TransactionEdit
            frm_trasEdit.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnBankStatement_Click(sender As Object, e As RibbonControlEventArgs) Handles btnBankStatement.Click
        Try
            Dim mbDllDirectory As String = AppDomain.CurrentDomain.BaseDirectory().ToString
            Dim startInfo As New ProcessStartInfo(mbDllDirectory & "\Bank Statement Importer.exe")
            Dim userid As Integer = 1
            startInfo.Arguments = My.Settings.CS_Setting & "|" & My.Settings.Setting_CompanyID & "|" & "Import"
            Process.Start(startInfo)
        Catch ex As Exception
            MessageBox.Show("An Error occured. Please report to System support with details :" & ex.Message, "MagicBox Cashbook Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnCashbook_Click(sender As Object, e As RibbonControlEventArgs) Handles btnCashbook.Click
        Try
            Dim mbDllDirectory As String = AppDomain.CurrentDomain.BaseDirectory().ToString
            Dim startInfo As New ProcessStartInfo(mbDllDirectory & "\Bank Statement Importer.exe")
            Dim userid As Integer = 1
            startInfo.Arguments = My.Settings.CS_Setting & "|" & My.Settings.Setting_CompanyID & "|" & "Cashbook"
            Process.Start(startInfo)
        Catch ex As Exception
            MessageBox.Show("An Error occured. Please report to System support with details :" & ex.Message, "MagicBox Cashbook Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button7_Click(sender As Object, e As RibbonControlEventArgs) Handles Button7.Click
        Try
            Dim Form_Journal As New frmJournals
            Form_Journal.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Journal form " & Err.Description)
        End Try
    End Sub

    Private Sub btnOtherModules_Click(sender As Object, e As RibbonControlEventArgs) Handles btnOtherModules.Click
        Try
            Dim frmOtherM As New frmOtherModules
            frmOtherM.Show()
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnMainLogIn_Click(sender As Object, e As RibbonControlEventArgs) Handles btnMainLogIn.Click
        Dim frmMainLogIn As New Frm_UserLogon
        frmMainLogIn.ShowDialog()
    End Sub
    Private Sub btnTransactions_Click(sender As Object, e As RibbonControlEventArgs) Handles btnTransactions.Click
        Try
            Dim frmTransactions As New frmTransactions
            frmTransactions.Show()
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnSupplierMaster_Click(sender As Object, e As RibbonControlEventArgs) Handles btnSupplierMaster.Click
        Try
            Dim frmSup As New frmSuppliers
            frmSup.Show()
        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnEmpMaster_Click(sender As Object, e As RibbonControlEventArgs) Handles btnEmpMaster.Click
        Try
            Dim frmEmployees As New frmEmployees
            frmEmployees.Show()

        Catch ex As Exception
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Btn_Migrate_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_Migrate.Click
        Try
            Dim Form_Migrage As New frmMigrateAccountTransactions
            Form_Migrage.Show()
        Catch ex As Exception
            MsgBox("There was an error viewing the Journal form " & Err.Description)
        End Try
    End Sub
    Private Sub Button9_Click(sender As Object, e As RibbonControlEventArgs) Handles Button9.Click
        Dim frmCashPayRemove As New frmCashPayRemove
        frmCashPayRemove.ShowDialog()
    End Sub
    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        'Dim startInfo As New ProcessStartInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\Magic Box\MagicBox_Update.exe")
        'Dim userid As Integer = 1
        'startInfo.Arguments = "Magic_Box_Ribbon~" & My.Settings.Setting_Version & "~"
        'Process.Start(startInfo)
    End Sub
    Private Sub DB_BackgroundWorker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles DB_BackgroundWorker.DoWork
        Call CheckFields()
    End Sub

    Private Sub Button10_Click(sender As Object, e As RibbonControlEventArgs) Handles Button10.Click
        Dim frmEnquire As New FrmEnquire
        frmEnquire.Show()
    End Sub
    Private Sub DD_Companies_ItemsLoading(sender As Object, e As RibbonControlEventArgs) Handles DD_Companies.ItemsLoading
        ' SetCompany()
    End Sub
    Private Sub btn_ChangeCo_Click(sender As Object, e As RibbonControlEventArgs) Handles btn_ChangeCo.Click
        SetCompany()
        Group_Home.Visible = False
    End Sub
    Function LoadCompanyList()
        If Globals.Ribbons.Ribbon1.DD_Group.Items.Count = 0 Then Exit Function
        If LoggedUsersDataSet.Rows.Count = 0 Then Exit Function
        Globals.Ribbons.Ribbon1.DD_Companies.Items.Clear()
        Dim DSCompanies As New DataTable
        DSCompanies = LoggedUsersDataSet.DefaultView.ToTable(True, "LinkCo_ID", "ShortName", "GroupID", "TenantStoreID", "LastCoID")
        Dim dvc As DataView
        dvc = DSCompanies.DefaultView
        dvc.Sort = "ShortName ASC"
        DSCompanies = dvc.ToTable

        For Each row As DataRow In DSCompanies.Rows
            If row("GroupID") = DD_Group.SelectedItem.Tag.ToString Then
                Dim DDitem = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem()
                DDitem.Label = row("ShortName")
                DDitem.Tag = row("TenantStoreID")
                Globals.Ribbons.Ribbon1.DD_Companies.Items.Add(DDitem)
            End If
        Next

        ' SetCompany()
    End Function
    Function SetCompany()
        Try
            Dim SecureP As New PasswordSecurity
            If DD_Group.Visible = True Then
                DD_Companies.Visible = True
            End If
            For Each row As DataRow In LoggedUsersDataSet.Rows
                If row("TenantStoreID") = DD_Companies.SelectedItem.Tag.ToString Then
                    My.Settings.GroupID = DD_Group.SelectedItem.Tag.ToString
                    My.Settings.Setting_CompanyID = row("LinkCo_ID")
                    My.Settings.CurrentCompany = row("StoreName")
                    My.Settings.TenantStoreID = row("TenantStoreID")
                    CurrentGroupID = DD_Group.SelectedItem.Tag.ToString
                    CurrentCompanyID = DD_Companies.SelectedItem.Tag.ToString
                    My.Settings.CS_Setting = "Server=" & row("GroupServerAddress") & ";Database=" & row("DatabaseName") & ";Uid=" & row("DatabaseUserName") & ";Pwd=" & SecureP.DecryptString(row("DatabasePassword")) & ""
                    NI_ServerStatus.BalloonTipText = "Company set to: " & My.Settings.CurrentCompany
                    NI_ServerStatus.BalloonTipTitle = ""
                    NI_ServerStatus.BalloonTipIcon = ToolTipIcon.None
                    NI_ServerStatus.ShowBalloonTip(500)
                    Globals.Ribbons.Ribbon1.TaxTable = GetTaxTables().Tables(0)
                    Globals.Ribbons.Ribbon1.AccountsTable = GetAccountsTables().Tables(0)
                End If
                If (Globals.Ribbons.Ribbon1.DD_Companies.SelectedItem.Tag.ToString = "") Or (Globals.Ribbons.Ribbon1.DD_Group.SelectedItem.Tag.ToString = "") Then
                Else
                    ' Call UpdateLastGroup(CurrentTenantID, Globals.Ribbons.Ribbon1.DD_Group.SelectedItem.Tag.ToString, Globals.Ribbons.Ribbon1.DD_Companies.SelectedItem.Tag.ToString)
                End If
            Next

            Globals.Ribbons.Ribbon1.LoggedUsersRoles = TenantRoles(CurrentTenantID).Tables(0)
            For Each row As DataRow In Globals.Ribbons.Ribbon1.LoggedUsersRoles.Rows
                If row("GroupID") = Globals.Ribbons.Ribbon1.CurrentGroupID Then
                    If IsDBNull(row.Item("PaymentExclusion")) Then
                        ExcludedPayTypes = ""
                    Else
                        ExcludedPayTypes = row.Item("PaymentExclusion")
                    End If
                End If
            Next

            Hidegroups()
            OpenGroups()
            SetReg(CurrentGroupID, CurrentCompanyID, My.Settings.Username)
            LBL_CoName.Label = My.Settings.CurrentCompany
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub Btn_Administration_Click(sender As Object, e As RibbonControlEventArgs)
        Dim Frm_Administration As New Frm_Administration
        Frm_Administration.Show()
    End Sub

    Private Sub DD_Group_SelectionChanged(sender As Object, e As RibbonControlEventArgs) Handles DD_Group.SelectionChanged
        LoadCompanyList()
        DD_Companies.Visible = True
    End Sub
    Function Hidegroups()
        Globals.Ribbons.Ribbon1.GrpLogin.Visible = False
        Globals.Ribbons.Ribbon1.grpDailyProcessing.Visible = False
        Globals.Ribbons.Ribbon1.grpMaintanance.Visible = False
        Globals.Ribbons.Ribbon1.grpSuppliers.Visible = False
        Globals.Ribbons.Ribbon1.grpApprovals.Visible = False
        Globals.Ribbons.Ribbon1.grpCustomers.Visible = False
        Globals.Ribbons.Ribbon1.grpGeneral.Visible = False
        Globals.Ribbons.Ribbon1.grpIntelligence.Visible = False
        Globals.Ribbons.Ribbon1.grpAdmin.Visible = False
        Globals.Ribbons.Ribbon1.GrpAccounting.Visible = False
        Globals.Ribbons.Ribbon1.grpExtaTools.Visible = False
        Globals.Ribbons.Ribbon1.grpModes.Visible = False
        Globals.Ribbons.Ribbon1.Group_Home.Visible = False
        Globals.Ribbons.Ribbon1.GrpOrderX.Visible = False
    End Function
    Function OpenGroups()
        Try
            Dim HasRole As Boolean = False
            Globals.Ribbons.Ribbon1.Group_Home.Visible = True
            For Each row As DataRow In LoggedUsersRoles.Rows
                If row("GroupID") = CurrentGroupID Then
                    HasRole = True
                    If row("grpMasterFiles") = "True" Then
                        Globals.Ribbons.Ribbon1.grpMaintanance.Visible = True
                    Else
                        Globals.Ribbons.Ribbon1.grpMaintanance.Visible = False
                    End If
                    If row("grpSuppliers") = "True" Then
                        Globals.Ribbons.Ribbon1.grpSuppliers.Visible = True
                    Else
                        Globals.Ribbons.Ribbon1.grpSuppliers.Visible = False
                    End If
                    If row("grpApprovals") = "True" Then
                        Globals.Ribbons.Ribbon1.grpApprovals.Visible = True
                    Else
                        Globals.Ribbons.Ribbon1.grpApprovals.Visible = False
                    End If
                    If row("grpIntelligence") = "True" Then
                        Globals.Ribbons.Ribbon1.grpIntelligence.Visible = True
                    Else
                        Globals.Ribbons.Ribbon1.grpIntelligence.Visible = False
                    End If
                    If row("grpAccounting") = "True" Then
                        Globals.Ribbons.Ribbon1.GrpAccounting.Visible = True
                    Else
                        Globals.Ribbons.Ribbon1.GrpAccounting.Visible = False
                    End If
                    If row("grpTools") = "True" Then
                        Globals.Ribbons.Ribbon1.grpExtaTools.Visible = True
                    Else
                        Globals.Ribbons.Ribbon1.grpExtaTools.Visible = False
                    End If
                    If GlobalAdmin = True Then
                        Globals.Ribbons.Ribbon1.grpAdmin.Visible = True
                    End If
                    Exit Function
                End If
            Next
            If HasRole = False Then
                MsgBox("The user logged in does not have any user roles assigned." & vbNewLine & "Please contact your group administrator")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Private Sub Tmr_ServerCheck_Tick(sender As Object, e As EventArgs) Handles Tmr_ServerCheck.Tick
        Dim thread As New Thread(AddressOf TestServer)
        thread.Start()
    End Sub
    Private Sub btn_LogOut_Click(sender As Object, e As RibbonControlEventArgs) Handles btn_LogOut.Click
        Call LogOut()
    End Sub
    Private Sub Btn_Cancel_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_Cancel.Click
        Group_Home.Visible = False
    End Sub
    Function LogOut()
        Globals.Ribbons.Ribbon1.ShouldLoadGroups = False
        'Globals.Ribbons.Ribbon1.btnMainLogIn.Visible = True
        ' Globals.Ribbons.Ribbon1.btn_LogOut.Visible = False
        Globals.Ribbons.Ribbon1.DD_Group.Visible = False
        Globals.Ribbons.Ribbon1.DD_Companies.Visible = False
        Globals.Ribbons.Ribbon1.btn_ChangeCo.Visible = False
        GlobalAdmin = False
        ShouldLoadGroups = False
        CurrentGroupID = 0
        CurrentTenantID = 0
        Call Hidegroups()
        Call BuildGallary()
    End Function
    Function BuildGallary()
        Gallery1.Items.Clear()

        Dim galleryItem_Welcome = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'Login Button
        'galleryItem_Welcome.Image = My.Resources.Settings
        If CurrentTenantID = 0 Then
            galleryItem_Welcome.Label = "Not Logged In (" & My.Settings.Setting_Version & ")"
        Else
            galleryItem_Welcome.Label = My.Settings.Username
        End If
        galleryItem_Welcome.Tag = "Log In"
        Gallery1.Items.Add(galleryItem_Welcome)

        Dim galleryItem = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'Login Button
        galleryItem.Image = My.Resources.Settings
        If CurrentTenantID = 0 Then
            galleryItem.Label = "Log In"
            galleryItem.Tag = "Log In"
        Else
            galleryItem.Label = "Log Out"
            galleryItem.Tag = "Log Out"
        End If

        Gallery1.Items.Add(galleryItem)
        'Change Company
        If CurrentTenantID <> 0 Then
            Dim galleryItem_ChangeCo = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
            galleryItem_ChangeCo.Image = My.Resources.Settings
            galleryItem_ChangeCo.Label = "Change Co"
            galleryItem_ChangeCo.Tag = "Change Co"
            Gallery1.Items.Add(galleryItem_ChangeCo)
        End If
    End Function
    Function BuildBIGallary()
        Gal_BI.Items.Clear()

        Dim galleryItem_RM = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'Report Manager
        galleryItem_RM.Image = My.Resources.Reports
        galleryItem_RM.Label = "Report Manager"
        galleryItem_RM.Tag = "Report Manager"
        Gal_BI.Items.Add(galleryItem_RM)

        Dim galleryItem_Adm = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'Administrator
        galleryItem_Adm.Image = My.Resources.Administrator
        galleryItem_Adm.Label = "Administrator"
        galleryItem_Adm.Tag = "Administrator"
        Gal_BI.Items.Add(galleryItem_Adm)

        Dim galleryItem_Lm = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'License Manager
        galleryItem_Lm.Image = My.Resources.License
        galleryItem_Lm.Label = "License Manager"
        galleryItem_Lm.Tag = "License Manager"
        Gal_BI.Items.Add(galleryItem_Lm)

        Dim galleryItem_Sm = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'License Manager
        galleryItem_Sm.Image = My.Resources.License
        galleryItem_Sm.Label = "Security Manager"
        galleryItem_Sm.Tag = "Security Manager"
        Gal_BI.Items.Add(galleryItem_Sm)

        '    Dim galleryItem_Om = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        '    'License Manager
        '    galleryItem_Om.Image = My.Resources.License
        '    galleryItem_Om.Label = "OLAP Manager"
        '    galleryItem_Om.Tag = "OLAP Manager"
        ''   Gal_BI.Items.Add(galleryItem_Om)

        Dim galleryItem_Cm = Globals.Ribbons.Ribbon1.Factory.CreateRibbonDropDownItem
        'License Manager
        galleryItem_Cm.Image = My.Resources.License
        galleryItem_Cm.Label = "Community"
        galleryItem_Cm.Tag = "Community"
        Gal_BI.Items.Add(galleryItem_Cm)

    End Function
    Private Sub Gallery1_Click(sender As Object, e As RibbonControlEventArgs) Handles Gallery1.Click
        Dim NavItem As String = Gallery1.SelectedItem.Tag.ToString
        If NavItem = "Change Co" Then
            Group_Home.Visible = True
            CheckCos()
        End If
        If NavItem = "Log In" Then
            Dim frmMainLogIn As New Frm_UserLogon
            frmMainLogIn.ShowDialog()
            Group_Home.Visible = True
            CheckCos()
        End If
        If NavItem = "Log Out" Then
            Call LogOut()
        End If
        Call BuildGallary()
        Gallery1.SelectedItemIndex = 0
    End Sub
    Function CheckCos()
        Dim CountR As Long
        CountR = 0
        For Each ditem As RibbonDropDownItem In DD_Group.Items
            If ditem.Tag.ToString = CurrentGroupID Then
                DD_Group.SelectedItemIndex = CountR
            End If
            CountR = CountR + 1
        Next
        LoadCompanyList()
        CountR = 0
        For Each ditem As RibbonDropDownItem In DD_Companies.Items
            ' MsgBox("Loop G = " & ditem.Tag.ToString & "   Current Group: " & CurrentCompanyID)
            If ditem.Tag.ToString = CurrentCompanyID Then
                DD_Companies.SelectedItemIndex = CountR
            End If
            CountR = CountR + 1
        Next
    End Function
#Region "LoginDefaults"
    Function CheckReg()
        Dim regStartUP As RegistryKey
        Dim intGroupID As Long
        Dim intCoID As Long

        regStartUP = Registry.CurrentUser.OpenSubKey("SOFTWARE\\MagicBox\\StartUp", True)
        If regStartUP Is Nothing Then
            ' Key doesn't exist; create it.
            regStartUP = Registry.CurrentUser.CreateSubKey("SOFTWARE\\MagicBox\\StartUp")
        End If
        intGroupID = 0
        If (Not regStartUP Is Nothing) Then
            intGroupID = regStartUP.GetValue("GroupID", 0)
        End If
        intCoID = 0
        If (Not regStartUP Is Nothing) Then
            intCoID = regStartUP.GetValue("Co_ID", 0)
        End If
        regStartUP.Close()
    End Function
    Function SetReg(GroupID As String, Co_ID As String, CurrentUser As String)
        Dim regStartUP As RegistryKey
        Dim intGroupID As Long
        Dim intCoID As Long

        regStartUP = Registry.CurrentUser.OpenSubKey("SOFTWARE\\MagicBox\\StartUp", True)
        If regStartUP Is Nothing Then
            ' Key doesn't exist; create it.
            regStartUP = Registry.CurrentUser.CreateSubKey("SOFTWARE\\MagicBox\\StartUp")
        End If
        regStartUP.SetValue("GroupID", GroupID)
        regStartUP.SetValue("Co_ID", Co_ID)
        regStartUP.SetValue("CurrentUser", CurrentUser)
        regStartUP.Close()
    End Function
#End Region

    Private Sub Gal_BI_Click(sender As Object, e As RibbonControlEventArgs) Handles Gal_BI.Click
        Dim NavItem As String = Gal_BI.SelectedItem.Tag.ToString
        If NavItem = "Report Manager" Then
            If Environment.Is64BitOperatingSystem Then

                LaunchReportManager()
            Else

                LaunchApp("ReportManager")
            End If
        End If
        If NavItem = "Administrator" Then
            If Environment.Is64BitOperatingSystem Then
                LaunchAlchemexModule("eAdministrator")
            Else
                LaunchApp("Administrator")
            End If
        End If
        If NavItem = "License Manager" Then
            If Environment.Is64BitOperatingSystem Then
                LaunchAlchemexModule("eLicenseManager")
            Else
                LaunchApp("License Manager")
            End If
        End If
        If NavItem = "Security Manager" Then
            If Environment.Is64BitOperatingSystem Then
                LaunchAlchemexModule("eSecurityManager")
            Else
                LaunchApp("License Manager")
            End If
        End If
        If NavItem = "OLAP Manager" Then
            If Environment.Is64BitOperatingSystem Then
                LaunchAlchemexModule("eOLAPManager")
            Else
                LaunchApp("License Manager")
            End If
        End If
        If NavItem = "Community" Then
            If Environment.Is64BitOperatingSystem Then
                LaunchAlchemexModule("eCommunity")
            Else
                LaunchApp("License Manager")
            End If
        End If
    End Sub


    Private Sub Btn_Verify_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_Verify.Click
        Dim Frm_Verified As New Frm_Verify
        Frm_Verified.Show()
    End Sub

    Private Sub Button11_Click(sender As Object, e As RibbonControlEventArgs) Handles Button11.Click
        Dim frmNewCo As New frm_NewCoStart
        frmNewCo.ShowDialog()
    End Sub

    Private Sub test_report_Click(sender As Object, e As RibbonControlEventArgs) Handles test_report.Click
        Dim frm_Report As New frmReportHome
        frm_Report.ReportViewer1.Clear()
        frm_Report.ReportViewer1.Reset()
        frm_Report.ReportViewer1.LocalReport.ReportEmbeddedResource = "MB.Suppliers_Master.rdlc"

        frm_Report.ReportViewer1.LocalReport.DataSources.Clear()
        Dim ds_SupplierMaster As New ReportDataSource

        Dim p1 As New ReportParameter("RPT_Name", "Supplier Master Report")
        Dim p2 As New ReportParameter("CompanyName", My.Settings.CurrentCompany)
        Dim p3 As New ReportParameter("UserName", My.Settings.Username)

        frm_Report.ReportViewer1.LocalReport.SetParameters(p1)
        frm_Report.ReportViewer1.LocalReport.SetParameters(p2)
        frm_Report.ReportViewer1.LocalReport.SetParameters(p3)
        ds_SupplierMaster.Value = ReportQueries.SupplierMaster.Tables("SupplierMasters")
        ds_SupplierMaster.Name = "SupplierMasters"
        frm_Report.ReportViewer1.LocalReport.DataSources.Add(ds_SupplierMaster)
        frm_Report.Show()

    End Sub

    Private Sub Btn_InvIntegration_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_InvIntegration.Click
        Dim frmInvoiceIntegrator As New frmInvoiceIntegrator
        frmInvoiceIntegrator.Show()
    End Sub

    Private Sub Btn_OrderX_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_OrderX.Click
        Try
            Dim mbDllDirectory As String = AppDomain.CurrentDomain.BaseDirectory().ToString
            Dim startInfo As New ProcessStartInfo(mbDllDirectory & "\OrderXApp.exe")
            Dim userid As Integer = 1
            startInfo.Arguments = "U:1|L:1"
            Process.Start(startInfo)
        Catch ex As Exception
            MessageBox.Show("An Error occured. Please report to System support with details :" & ex.Message, "MagicBox Cashbook Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Tasks_Click(sender As Object, e As RibbonControlEventArgs) Handles Tasks.Click
        Dim IssuesMain As New frm_IssuesMain
        IssuesMain.Show()
    End Sub

    Private Sub Btn_DBMaint_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_DBMaint.Click
        Dim Frm_DBMaint As New Frm_DBMaint
        Frm_DBMaint.Show()
    End Sub

    Private Sub btn_Update_Click(sender As Object, e As RibbonControlEventArgs) Handles btn_Update.Click
        Try
            Dim mbDllDirectory As String = AppDomain.CurrentDomain.BaseDirectory().ToString
            Dim startInfo As New ProcessStartInfo(mbDllDirectory & "\MBUpdate.exe")
            ' Dim userid As Integer = 1
            ' startInfo.Arguments = My.Settings.CS_Setting & "|" & My.Settings.Setting_CompanyID & "|" & "Import"
            Process.Start(startInfo)
        Catch ex As Exception
            MessageBox.Show("An Error occured. Please report to System support with details :" & ex.Message, "MagicBox Cashbook Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button13_Click(sender As Object, e As RibbonControlEventArgs) Handles Button13.Click
        Dim OXcls As New clsOrderX.Core

        OXcls.UserName = "wynboon@gmail.com"
        OXcls.Password = "B00nz@13r"

        OrderX_token = OXcls.ValidateUser2

        Dim XL As Object
        'Set to new instance of excel
        'XL = GetObject(, "Excel.Application")
        'XL.Visible = True
        'XL.WindowState = Excel.XlWindowState.xlMaximized
        'Create(New WrokBook)
        'Dim wkbk As Excel.Workbook = XL.Workbooks.Add(App_Path() & "/xlTemplates/GenericTemplate.xltx")
        Globals.ThisAddIn.AddOrderXTaskPane()
    End Sub

    Private Sub Btn_Tasks_Click(sender As Object, e As RibbonControlEventArgs) Handles Btn_Tasks.Click

    End Sub
End Class