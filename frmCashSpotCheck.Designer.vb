﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashSpotCheck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCashSpotCheck))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.txtYourCashCount = New System.Windows.Forms.RichTextBox()
        Me.btnRangeTotal = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtCashOnSystem = New System.Windows.Forms.RichTextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel_Action = New System.Windows.Forms.Panel()
        Me.lblExplanation = New System.Windows.Forms.Label()
        Me.btnDone = New System.Windows.Forms.Button()
        Me.txtExplanation = New System.Windows.Forms.RichTextBox()
        Me.rdbAlert = New System.Windows.Forms.RadioButton()
        Me.rdbExplain = New System.Windows.Forms.RadioButton()
        Me.rdbAdjust = New System.Windows.Forms.RadioButton()
        Me.rdbIgnore = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtVariance = New System.Windows.Forms.RichTextBox()
        Me.Panel_Result = New System.Windows.Forms.Panel()
        Me.lblVariance = New System.Windows.Forms.Label()
        Me.DateTimePicker_To = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker_From = New System.Windows.Forms.DateTimePicker()
        Me.DGV_Transactions = New System.Windows.Forms.DataGridView()
        Me.BatchID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Capture_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fin_Year = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reference = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAcc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tax_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accounting = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Void = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DR_CR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contra_Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label_Success = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel_Action.SuspendLayout()
        Me.Panel_Result.SuspendLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnOK)
        Me.GroupBox1.Controls.Add(Me.txtYourCashCount)
        Me.GroupBox1.Controls.Add(Me.btnRangeTotal)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(13, 36)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(497, 75)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnOK.Location = New System.Drawing.Point(387, 20)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(49, 43)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'txtYourCashCount
        '
        Me.txtYourCashCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYourCashCount.Location = New System.Drawing.Point(221, 20)
        Me.txtYourCashCount.Name = "txtYourCashCount"
        Me.txtYourCashCount.Size = New System.Drawing.Size(148, 43)
        Me.txtYourCashCount.TabIndex = 2
        Me.txtYourCashCount.Text = ""
        '
        'btnRangeTotal
        '
        Me.btnRangeTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRangeTotal.ForeColor = System.Drawing.Color.MidnightBlue
        Me.btnRangeTotal.Location = New System.Drawing.Point(442, 20)
        Me.btnRangeTotal.Name = "btnRangeTotal"
        Me.btnRangeTotal.Size = New System.Drawing.Size(49, 43)
        Me.btnRangeTotal.TabIndex = 1
        Me.btnRangeTotal.Text = "Range Total"
        Me.btnRangeTotal.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(163, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Your Cash Count"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtCashOnSystem)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(12, 117)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(498, 75)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'txtCashOnSystem
        '
        Me.txtCashOnSystem.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCashOnSystem.Location = New System.Drawing.Point(222, 19)
        Me.txtCashOnSystem.Name = "txtCashOnSystem"
        Me.txtCashOnSystem.Size = New System.Drawing.Size(148, 43)
        Me.txtCashOnSystem.TabIndex = 2
        Me.txtCashOnSystem.Text = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(157, 25)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cash on System"
        '
        'Panel_Action
        '
        Me.Panel_Action.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel_Action.Controls.Add(Me.lblExplanation)
        Me.Panel_Action.Controls.Add(Me.btnDone)
        Me.Panel_Action.Controls.Add(Me.txtExplanation)
        Me.Panel_Action.Controls.Add(Me.rdbAlert)
        Me.Panel_Action.Controls.Add(Me.rdbExplain)
        Me.Panel_Action.Controls.Add(Me.rdbAdjust)
        Me.Panel_Action.Controls.Add(Me.rdbIgnore)
        Me.Panel_Action.Location = New System.Drawing.Point(18, 307)
        Me.Panel_Action.Name = "Panel_Action"
        Me.Panel_Action.Size = New System.Drawing.Size(492, 100)
        Me.Panel_Action.TabIndex = 2
        Me.Panel_Action.Visible = False
        '
        'lblExplanation
        '
        Me.lblExplanation.AutoSize = True
        Me.lblExplanation.Location = New System.Drawing.Point(206, 30)
        Me.lblExplanation.Name = "lblExplanation"
        Me.lblExplanation.Size = New System.Drawing.Size(62, 13)
        Me.lblExplanation.TabIndex = 6
        Me.lblExplanation.Text = "Explanation"
        Me.lblExplanation.Visible = False
        '
        'btnDone
        '
        Me.btnDone.Location = New System.Drawing.Point(91, 70)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(81, 23)
        Me.btnDone.TabIndex = 5
        Me.btnDone.Text = "Done"
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'txtExplanation
        '
        Me.txtExplanation.Location = New System.Drawing.Point(206, 53)
        Me.txtExplanation.Name = "txtExplanation"
        Me.txtExplanation.Size = New System.Drawing.Size(266, 40)
        Me.txtExplanation.TabIndex = 4
        Me.txtExplanation.Text = ""
        Me.txtExplanation.Visible = False
        '
        'rdbAlert
        '
        Me.rdbAlert.AutoSize = True
        Me.rdbAlert.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbAlert.Location = New System.Drawing.Point(20, 53)
        Me.rdbAlert.Name = "rdbAlert"
        Me.rdbAlert.Size = New System.Drawing.Size(46, 17)
        Me.rdbAlert.TabIndex = 3
        Me.rdbAlert.TabStop = True
        Me.rdbAlert.Text = "Alert"
        Me.rdbAlert.UseVisualStyleBackColor = True
        '
        'rdbExplain
        '
        Me.rdbExplain.AutoSize = True
        Me.rdbExplain.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbExplain.Location = New System.Drawing.Point(19, 76)
        Me.rdbExplain.Name = "rdbExplain"
        Me.rdbExplain.Size = New System.Drawing.Size(59, 17)
        Me.rdbExplain.TabIndex = 2
        Me.rdbExplain.TabStop = True
        Me.rdbExplain.Text = "Explain"
        Me.rdbExplain.UseVisualStyleBackColor = True
        '
        'rdbAdjust
        '
        Me.rdbAdjust.AutoSize = True
        Me.rdbAdjust.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbAdjust.Location = New System.Drawing.Point(19, 30)
        Me.rdbAdjust.Name = "rdbAdjust"
        Me.rdbAdjust.Size = New System.Drawing.Size(54, 17)
        Me.rdbAdjust.TabIndex = 1
        Me.rdbAdjust.TabStop = True
        Me.rdbAdjust.Text = "Adjust"
        Me.rdbAdjust.UseVisualStyleBackColor = True
        '
        'rdbIgnore
        '
        Me.rdbIgnore.AutoSize = True
        Me.rdbIgnore.ForeColor = System.Drawing.Color.MidnightBlue
        Me.rdbIgnore.Location = New System.Drawing.Point(19, 7)
        Me.rdbIgnore.Name = "rdbIgnore"
        Me.rdbIgnore.Size = New System.Drawing.Size(55, 17)
        Me.rdbIgnore.TabIndex = 0
        Me.rdbIgnore.TabStop = True
        Me.rdbIgnore.Text = "Ignore"
        Me.rdbIgnore.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackgroundImage = Global.MB.My.Resources.Resources.CashSpotCheckTick
        Me.Panel2.Location = New System.Drawing.Point(19, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(83, 86)
        Me.Panel2.TabIndex = 4
        '
        'txtVariance
        '
        Me.txtVariance.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVariance.Location = New System.Drawing.Point(216, 26)
        Me.txtVariance.Name = "txtVariance"
        Me.txtVariance.Size = New System.Drawing.Size(148, 43)
        Me.txtVariance.TabIndex = 5
        Me.txtVariance.Text = ""
        '
        'Panel_Result
        '
        Me.Panel_Result.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Panel_Result.Controls.Add(Me.lblVariance)
        Me.Panel_Result.Controls.Add(Me.Panel2)
        Me.Panel_Result.Controls.Add(Me.txtVariance)
        Me.Panel_Result.Location = New System.Drawing.Point(18, 199)
        Me.Panel_Result.Name = "Panel_Result"
        Me.Panel_Result.Size = New System.Drawing.Size(492, 102)
        Me.Panel_Result.TabIndex = 6
        Me.Panel_Result.Visible = False
        '
        'lblVariance
        '
        Me.lblVariance.AutoSize = True
        Me.lblVariance.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVariance.ForeColor = System.Drawing.Color.Red
        Me.lblVariance.Location = New System.Drawing.Point(118, 35)
        Me.lblVariance.Name = "lblVariance"
        Me.lblVariance.Size = New System.Drawing.Size(90, 25)
        Me.lblVariance.TabIndex = 6
        Me.lblVariance.Text = "Variance"
        '
        'DateTimePicker_To
        '
        Me.DateTimePicker_To.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_To.Location = New System.Drawing.Point(167, 11)
        Me.DateTimePicker_To.Name = "DateTimePicker_To"
        Me.DateTimePicker_To.Size = New System.Drawing.Size(137, 20)
        Me.DateTimePicker_To.TabIndex = 55
        Me.DateTimePicker_To.Visible = False
        '
        'DateTimePicker_From
        '
        Me.DateTimePicker_From.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_From.Location = New System.Drawing.Point(37, 12)
        Me.DateTimePicker_From.Name = "DateTimePicker_From"
        Me.DateTimePicker_From.Size = New System.Drawing.Size(124, 20)
        Me.DateTimePicker_From.TabIndex = 54
        Me.DateTimePicker_From.Visible = False
        '
        'DGV_Transactions
        '
        Me.DGV_Transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Transactions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BatchID, Me.TransactionID, Me.LinkID, Me.Capture_Date, Me.Transaction_Date, Me.PPeriod, Me.Fin_Year, Me.GDC, Me.Reference, Me.Description, Me.AccNumber, Me.LinkAcc, Me.Amount, Me.TaxType, Me.Tax_Amount, Me.UserID, Me.SupplierID, Me.EmployeeID, Me.Description2, Me.Description3, Me.Description4, Me.Description5, Me.Posted, Me.Accounting, Me.Void, Me.Transaction_Type, Me.DR_CR, Me.Contra_Account, Me.Description_Code, Me.DocType, Me.SupplierName, Me.Info, Me.Info2, Me.Info3})
        Me.DGV_Transactions.Location = New System.Drawing.Point(18, 414)
        Me.DGV_Transactions.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_Transactions.Name = "DGV_Transactions"
        Me.DGV_Transactions.RowTemplate.Height = 24
        Me.DGV_Transactions.Size = New System.Drawing.Size(472, 75)
        Me.DGV_Transactions.TabIndex = 97
        '
        'BatchID
        '
        Me.BatchID.HeaderText = "BatchID"
        Me.BatchID.Name = "BatchID"
        Me.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TransactionID
        '
        Me.TransactionID.HeaderText = "TransactionID"
        Me.TransactionID.Name = "TransactionID"
        '
        'LinkID
        '
        Me.LinkID.HeaderText = "LinkID"
        Me.LinkID.Name = "LinkID"
        '
        'Capture_Date
        '
        Me.Capture_Date.HeaderText = "Capture_Date"
        Me.Capture_Date.Name = "Capture_Date"
        '
        'Transaction_Date
        '
        Me.Transaction_Date.HeaderText = "Transaction_Date"
        Me.Transaction_Date.Name = "Transaction_Date"
        '
        'PPeriod
        '
        Me.PPeriod.HeaderText = "PPeriod"
        Me.PPeriod.Name = "PPeriod"
        '
        'Fin_Year
        '
        Me.Fin_Year.HeaderText = "Fin_Year"
        Me.Fin_Year.Name = "Fin_Year"
        '
        'GDC
        '
        Me.GDC.HeaderText = "GDC"
        Me.GDC.Name = "GDC"
        '
        'Reference
        '
        Me.Reference.HeaderText = "Reference"
        Me.Reference.Name = "Reference"
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        '
        'AccNumber
        '
        Me.AccNumber.HeaderText = "AccNumber"
        Me.AccNumber.Name = "AccNumber"
        '
        'LinkAcc
        '
        Me.LinkAcc.HeaderText = "LinkAcc"
        Me.LinkAcc.Name = "LinkAcc"
        '
        'Amount
        '
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        '
        'TaxType
        '
        Me.TaxType.HeaderText = "TaxType"
        Me.TaxType.Name = "TaxType"
        '
        'Tax_Amount
        '
        Me.Tax_Amount.HeaderText = "Tax_Amount"
        Me.Tax_Amount.Name = "Tax_Amount"
        '
        'UserID
        '
        Me.UserID.HeaderText = "UserID"
        Me.UserID.Name = "UserID"
        '
        'SupplierID
        '
        Me.SupplierID.HeaderText = "SupplierID"
        Me.SupplierID.Name = "SupplierID"
        '
        'EmployeeID
        '
        Me.EmployeeID.HeaderText = "EmployeeID"
        Me.EmployeeID.Name = "EmployeeID"
        '
        'Description2
        '
        Me.Description2.HeaderText = "Description2"
        Me.Description2.Name = "Description2"
        '
        'Description3
        '
        Me.Description3.HeaderText = "Description3"
        Me.Description3.Name = "Description3"
        '
        'Description4
        '
        Me.Description4.HeaderText = "Description4"
        Me.Description4.Name = "Description4"
        '
        'Description5
        '
        Me.Description5.HeaderText = "Description5"
        Me.Description5.Name = "Description5"
        '
        'Posted
        '
        Me.Posted.HeaderText = "Posted"
        Me.Posted.Name = "Posted"
        '
        'Accounting
        '
        Me.Accounting.HeaderText = "Accounting"
        Me.Accounting.Name = "Accounting"
        '
        'Void
        '
        Me.Void.HeaderText = "Void"
        Me.Void.Name = "Void"
        '
        'Transaction_Type
        '
        Me.Transaction_Type.HeaderText = "Transaction_Type"
        Me.Transaction_Type.Name = "Transaction_Type"
        '
        'DR_CR
        '
        Me.DR_CR.HeaderText = "DR_CR"
        Me.DR_CR.Name = "DR_CR"
        '
        'Contra_Account
        '
        Me.Contra_Account.HeaderText = "Contra_Account"
        Me.Contra_Account.Name = "Contra_Account"
        '
        'Description_Code
        '
        Me.Description_Code.HeaderText = "Description_Code"
        Me.Description_Code.Name = "Description_Code"
        '
        'DocType
        '
        Me.DocType.HeaderText = "DocType"
        Me.DocType.Name = "DocType"
        '
        'SupplierName
        '
        Me.SupplierName.HeaderText = "SupplierName"
        Me.SupplierName.Name = "SupplierName"
        '
        'Info
        '
        Me.Info.HeaderText = "Info"
        Me.Info.Name = "Info"
        '
        'Info2
        '
        Me.Info2.HeaderText = "Info2"
        Me.Info2.Name = "Info2"
        '
        'Info3
        '
        Me.Info3.HeaderText = "Info3"
        Me.Info3.Name = "Info3"
        '
        'Label_Success
        '
        Me.Label_Success.AutoSize = True
        Me.Label_Success.ForeColor = System.Drawing.Color.MidnightBlue
        Me.Label_Success.Location = New System.Drawing.Point(334, 429)
        Me.Label_Success.Name = "Label_Success"
        Me.Label_Success.Size = New System.Drawing.Size(48, 13)
        Me.Label_Success.TabIndex = 98
        Me.Label_Success.Text = "Success"
        '
        'frmCashSpotCheck
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 512)
        Me.Controls.Add(Me.Label_Success)
        Me.Controls.Add(Me.DGV_Transactions)
        Me.Controls.Add(Me.DateTimePicker_To)
        Me.Controls.Add(Me.DateTimePicker_From)
        Me.Controls.Add(Me.Panel_Result)
        Me.Controls.Add(Me.Panel_Action)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCashSpotCheck"
        Me.Text = "Cash Spot Check"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel_Action.ResumeLayout(False)
        Me.Panel_Action.PerformLayout()
        Me.Panel_Result.ResumeLayout(False)
        Me.Panel_Result.PerformLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtYourCashCount As System.Windows.Forms.RichTextBox
    Friend WithEvents btnRangeTotal As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCashOnSystem As System.Windows.Forms.RichTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel_Action As System.Windows.Forms.Panel
    Friend WithEvents rdbAlert As System.Windows.Forms.RadioButton
    Friend WithEvents rdbExplain As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAdjust As System.Windows.Forms.RadioButton
    Friend WithEvents rdbIgnore As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtVariance As System.Windows.Forms.RichTextBox
    Friend WithEvents Panel_Result As System.Windows.Forms.Panel
    Friend WithEvents lblVariance As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents DGV_Transactions As System.Windows.Forms.DataGridView
    Friend WithEvents BatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Capture_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fin_Year As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Reference As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAcc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tax_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accounting As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Void As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DR_CR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contra_Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label_Success As System.Windows.Forms.Label
    Friend WithEvents txtExplanation As System.Windows.Forms.RichTextBox
    Friend WithEvents btnDone As System.Windows.Forms.Button
    Friend WithEvents lblExplanation As System.Windows.Forms.Label
End Class
