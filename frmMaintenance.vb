﻿

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO

Public Class frmMaintenance


    Dim oAccessAdapter As OleDbDataAdapter
    Dim oAccessTable As New DataTable
    Dim oSQLAdapter As SqlDataAdapter
    Dim oSQLTable As New DataTable


    Private Sub frmMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Maintanance -> Version : " & My.Settings.Setting_Version
    End Sub
    Private Sub btnLoadGrid_Click(sender As System.Object, e As System.EventArgs) Handles btnLoadGrid.Click
        Fill_DGV_SupplierID_Corrections()
    End Sub

    Sub Fill_DGV_SupplierID_Corrections()
        Try

            Me.DataGridView1.DataSource = Nothing

            Dim sSQL As String

            sSQL = "SELECT Distinct Transactions.SupplierName, Transactions.SupplierID, Suppliers.SupplierID, Transactions.Co_ID"
            sSQL = sSQL & " FROM Transactions LEFT JOIN Suppliers ON (Transactions.SupplierName = Suppliers.SupplierName) AND (Transactions.Co_ID = Suppliers.Co_ID)"
            sSQL = sSQL & " WHERE [Transactions].[Co_ID] = " & My.Settings.Setting_CompanyID

            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.DateTimePicker_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTimePicker_To.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If

            If Me.chkUseDates.Checked = True Then
                sSQL = sSQL & sDateCriteria
            End If

            If My.Settings.DBType = "Access" Then
                oAccessTable.Clear()
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                oAccessAdapter = New OleDbDataAdapter(sSQL, connection)
                oAccessAdapter.Fill(oAccessTable)
                Me.DataGridView1.DataSource = oAccessTable
            ElseIf My.Settings.DBType = "SQL" Then
                oSQLTable.Clear() 'MUST CLEAR DATA TABLE
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                oSQLAdapter = New SqlDataAdapter(sSQL, connection)
                oSQLAdapter.Fill(oSQLTable)
                Me.DataGridView1.DataSource = oSQLTable
            End If
            Me.DataGridView1.Refresh()
            'Me.DataGridView1.ColumnHeadersDefaultCellStyle.Font = System.Drawing.F
            'Me.DataGridView1.Columns(0).HeaderCell.


        Catch ex As Exception
            MsgBox(ex.Message & " x01")
        End Try
    End Sub

    Private Sub btnToExcel_Click(sender As System.Object, e As System.EventArgs) Handles btnToExcel.Click
        Send_to_Excel("A1")
    End Sub

    Public Sub Send_to_Excel(ByVal oAnchorRangeAddress As String)

        Try

            Globals.ThisAddIn.Application.Workbooks.Add()

            System.Threading.Thread.Sleep(1500)


            ' TODO: Add code here to save the current contents of the form to a file.

            Dim D As DataGridView = Me.DataGridView1

            Dim oResponse As MsgBoxResult
            If D.RowCount > 1000 Then
                oResponse = MsgBox("You are about to load " & CStr(D.RowCount) & " lines. Would you like to continue?", MsgBoxStyle.YesNo)
                If oResponse = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If

            'Globals.ThisAddIn.Application.Sheets.Add() = caused errors

            'No matter what the range, this will get the row and column of the first cell
            Dim oAnchorRow As Long = Globals.ThisAddIn.Application.Range(oAnchorRangeAddress).Row
            Dim oAnchorColumn As Long = Globals.ThisAddIn.Application.Range(oAnchorRangeAddress).Column

            Me.lblFromRow.Text = oAnchorRow.ToString
            Globals.ThisAddIn.Application.Cells(1, 999).value = CStr(oAnchorRow.ToString)

            Dim rowCounter As Long

            Dim c As Integer
            Dim r As Long

            'Populate column headers first
            For c = 0 To D.ColumnCount - 1
                Globals.ThisAddIn.Application.Cells(oAnchorRow, oAnchorColumn + c).value = D.Columns(c).Name
            Next

            For r = 0 To D.RowCount - 1 'Take care of rows and progess bar that relies on its count

                rowCounter = rowCounter + 1

                For c = 0 To D.ColumnCount - 1 'take care of columns for each row
                    Globals.ThisAddIn.Application.Cells(oAnchorRow + r + 1, oAnchorColumn + c).value = D.Rows(r).Cells(c).Value
                Next
            Next

            Me.lblExcelToRow.Text = CStr(oAnchorRow + r - 1)
            'ALSO PUT LAST ROW IN SPREADSHEET
            Globals.ThisAddIn.Application.Cells(1, 1000).value = CStr(oAnchorRow + r - 1)

            Globals.ThisAddIn.Application.Columns.AutoFit()

            Globals.ThisAddIn.Application.Range(Globals.ThisAddIn.Application.Cells(oAnchorRow, oAnchorColumn), Globals.ThisAddIn.Application.Cells(oAnchorRow + D.RowCount - 1, oAnchorColumn + D.ColumnCount - 1)).Select()


            Call oSave_Workbook()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub oSave_Workbook()
        Try
            Dim sFileAndPath As String
            Dim sPath As String
            Dim sFile As String
            Dim SaveFileDialog As New SaveFileDialog
            SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
            SaveFileDialog.FileName = Now.Date.ToString("dd MMM yyyy") & " " & "Change Log - SupplierID"
            SaveFileDialog.Filter = "Excel Files (*.xlsx)|*.xlsx"
            SaveFileDialog.ShowDialog(Me)
            sFileAndPath = SaveFileDialog.FileName
            sPath = Path.GetDirectoryName(sFileAndPath)
            sFile = Path.GetFileName(sFileAndPath)
            Globals.ThisAddIn.Application.ActiveWorkbook.SaveAs(sFileAndPath)

        Catch ex As Exception
            MsgBox(ex.Message & " Problem saving workbook")
        End Try
    End Sub


    Private Sub btnRunCorrections_Click(sender As System.Object, e As System.EventArgs) Handles btnRunCorrections.Click
        Call oLoop_Thru_Excel("Change")
    End Sub
    Private Sub btnRunReversals_Click(sender As System.Object, e As System.EventArgs) Handles btnRunReversals.Click
        Call oLoop_Thru_Excel("Reverse")
    End Sub

    Sub oLoop_Thru_Excel(ByVal oChangeOrReverse As String)
        Try
            Dim i As Long
            Dim oSupplierName As String
            Dim oSupplierID As String
            Dim oNewSupplierID As String
            Dim oRecordsAffected As Long
            Dim oLastRow As Long
            Dim oFirstRow As Integer  'Me.lblFromRow.Text


            If oChangeOrReverse = "Change" Then
                oFirstRow = Me.lblFromRow.Text
                If Me.lblExcelToRow.Text = "XL Row" Then
                    MsgBox("Please  load data to Excel first!")
                    Exit Sub
                End If
                oLastRow = CLng(Me.lblExcelToRow.Text)

            ElseIf oChangeOrReverse = "Reverse" Then

                oLastRow = Globals.ThisAddIn.Application.Cells(1, 1000).value()
                oFirstRow = Globals.ThisAddIn.Application.Cells(1, 999).value()

            End If

            If oChangeOrReverse = "Change" Then
                Globals.ThisAddIn.Application.Cells(oFirstRow, 5).value = "Records Affected"
            ElseIf oChangeOrReverse = "Reverse" Then
                Globals.ThisAddIn.Application.Cells(oFirstRow, 6).value = "Records Reversed"
            End If

            For i = oFirstRow + 1 To oLastRow

                If IsNumeric(Globals.ThisAddIn.Application.Cells(i, 2).value) = True And _
                    IsNumeric(Globals.ThisAddIn.Application.Cells(i, 3).value) = True Then

                    If Globals.ThisAddIn.Application.Cells(i, 2).value = "0" Then GoTo Jump
                    If Globals.ThisAddIn.Application.Cells(i, 3).value = "0" Then GoTo Jump

                    oSupplierName = CStr(Globals.ThisAddIn.Application.Cells(i, 1).value)
                    oSupplierID = CStr(Globals.ThisAddIn.Application.Cells(i, 2).value)
                    oNewSupplierID = CStr(Globals.ThisAddIn.Application.Cells(i, 3).value)

                    If oChangeOrReverse = "Change" Then

                        If oSupplierID <> oNewSupplierID Then
                            oRecordsAffected = oRunUpdate(oSupplierName, oSupplierID, oNewSupplierID)
                            Globals.ThisAddIn.Application.Cells(i, 5).value = oRecordsAffected
                        End If

                    ElseIf oChangeOrReverse = "Reverse" Then

                        If oSupplierID <> oNewSupplierID Then
                            'Check if there is a number in original records affected
                            If IsNumeric(Globals.ThisAddIn.Application.Cells(i, 5).value) = True Then
                                oRecordsAffected = oRunUpdate(oSupplierName, oNewSupplierID, oSupplierID)
                                Globals.ThisAddIn.Application.Cells(i, 6).value = oRecordsAffected
                            End If

                        End If
                    End If


                End If

Jump:
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function oRunUpdate(ByVal oSupplierName As String, ByVal oSupplierID As String, ByVal oNewSupplierID As String) As Long
        Try
            Dim ra As Integer

            Dim sSQL As String
            sSQL = "Update Transactions Set [SupplierID] = " & oNewSupplierID & " Where SupplierID = " & oSupplierID & " And SupplierName = '" & oSupplierName & "'"
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.DateTimePicker_From.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTimePicker_To.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If

            If Me.chkUseDates.Checked = True Then
                sSQL = sSQL & sDateCriteria
            End If

            If My.Settings.DBType = "Access" Then

                Dim myConnection As New OleDbConnection(My.Settings.CS_Setting)
                myConnection.Open()
                Dim myCommand As OleDbCommand
                myCommand = New OleDbCommand(sSQL, myConnection)
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                oRunUpdate = ra
                'MsgBox("Records Updated " & ra)
                myConnection.Close()

            ElseIf My.Settings.DBType = "SQL" Then

                Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                myConnection.Open()
                Dim myCommand As SqlCommand
                myCommand = New SqlCommand(sSQL, myConnection)
                myCommand.CommandTimeout = 300
                ra = myCommand.ExecuteNonQuery()
                'Since no value is returned we use ExecuteNonQuery
                'MsgBox("Records Updated " & ra)
                oRunUpdate = ra
                myConnection.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " a82")
        End Try
    End Function

  
    Private Sub DateTimePicker_From_ValueChanged(sender As System.Object, e As System.EventArgs) Handles DateTimePicker_From.ValueChanged
        Try
            Me.DateTimePicker_To.Value = Me.DateTimePicker_From.Value
            Me.chkUseDates.Checked = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

  
    Private Sub DateTimePicker_To_ValueChanged(sender As System.Object, e As System.EventArgs) Handles DateTimePicker_To.ValueChanged
        Me.chkUseDates.Checked = True
    End Sub

  
    Private Sub chkEffectReversals_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkEffectReversals.CheckedChanged
        If Me.chkEffectReversals.Checked = True Then
            Me.btnOpenChangeWorkbook.Visible = True
            Me.btnRunReversals.Visible = True
        Else
            Me.btnOpenChangeWorkbook.Visible = False
            Me.btnRunReversals.Visible = False
        End If
    End Sub

    Private Sub btnOpenChangeWorkbook_Click(sender As System.Object, e As System.EventArgs) Handles btnOpenChangeWorkbook.Click

        Try
            Dim sFileAndPath As String
            Dim sPath As String
            Dim sFile As String

            With OpenFileDialog1
                .FileName = ""
                '.InitialDirectory = My.Settings.Elements_Directory & "\Elements Projects"
                .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
                .Filter = "Excel (*.xlsx)|*.xlsx"
                .ShowDialog()
                sFileAndPath = .FileName
                sPath = Path.GetDirectoryName(sFileAndPath)
                sFile = Path.GetFileName(sFileAndPath)
                Globals.ThisAddIn.Application.Workbooks.Open(sFileAndPath)
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


End Class