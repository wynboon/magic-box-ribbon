﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSuppliers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSuppliers))
        Me.SupplierName_TextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RefID_label = New System.Windows.Forms.Label()
        Me.ContactNo_label = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.RefID_TextBox = New System.Windows.Forms.TextBox()
        Me.ContactNo_TextBox = New System.Windows.Forms.TextBox()
        Me.ContactPerson_TextBox = New System.Windows.Forms.TextBox()
        Me.Address_TextBox = New System.Windows.Forms.TextBox()
        Me.EmailAddress_TextBox = New System.Windows.Forms.TextBox()
        Me.Fax_TextBox = New System.Windows.Forms.TextBox()
        Me.Supplier_DataGridView = New System.Windows.Forms.DataGridView()
        Me.SupplierBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Delete_Button = New System.Windows.Forms.Button()
        Me.ConfirmChanges_Button = New System.Windows.Forms.Button()
        Me.Result_Label = New System.Windows.Forms.Label()
        Me.DetailDefault2_ComboBox = New System.Windows.Forms.ComboBox()
        Me.CategoryDefault_ComboBox = New System.Windows.Forms.ComboBox()
        Me.DetailDefault_ComboBox = New System.Windows.Forms.ComboBox()
        Me.VATDefault_ComboBox = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbVAT = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Edit_ID_Label = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnEditSupplier = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NudTerms = New System.Windows.Forms.NumericUpDown()
        Me.ckbIsActive = New System.Windows.Forms.CheckBox()
        Me.Lbl_Edit_SuppRef = New System.Windows.Forms.Label()
        Me.TB_IntegrationCode = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Btn_SuppBank = New System.Windows.Forms.Button()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Lbl_Edit_SuppName = New System.Windows.Forms.Label()
        Me.Cb_ShowDeactivated = New System.Windows.Forms.CheckBox()
        CType(Me.Supplier_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SupplierBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudTerms, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SupplierName_TextBox
        '
        Me.SupplierName_TextBox.Enabled = False
        Me.SupplierName_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SupplierName_TextBox.Location = New System.Drawing.Point(11, 26)
        Me.SupplierName_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.SupplierName_TextBox.Name = "SupplierName_TextBox"
        Me.SupplierName_TextBox.Size = New System.Drawing.Size(236, 23)
        Me.SupplierName_TextBox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(12, 4)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Supplier Name"
        '
        'RefID_label
        '
        Me.RefID_label.AutoSize = True
        Me.RefID_label.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefID_label.ForeColor = System.Drawing.Color.DarkBlue
        Me.RefID_label.Location = New System.Drawing.Point(12, 53)
        Me.RefID_label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RefID_label.Name = "RefID_label"
        Me.RefID_label.Size = New System.Drawing.Size(52, 18)
        Me.RefID_label.TabIndex = 2
        Me.RefID_label.Text = "Ref ID"
        '
        'ContactNo_label
        '
        Me.ContactNo_label.AutoSize = True
        Me.ContactNo_label.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactNo_label.ForeColor = System.Drawing.Color.DarkBlue
        Me.ContactNo_label.Location = New System.Drawing.Point(9, 102)
        Me.ContactNo_label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ContactNo_label.Name = "ContactNo_label"
        Me.ContactNo_label.Size = New System.Drawing.Size(87, 18)
        Me.ContactNo_label.TabIndex = 4
        Me.ContactNo_label.Text = "Contact No"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label9.Location = New System.Drawing.Point(12, 151)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(127, 18)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "Category Default"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(12, 201)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 18)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Detail Default"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label11.Location = New System.Drawing.Point(12, 432)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(66, 18)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "Address"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label12.Location = New System.Drawing.Point(12, 481)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(107, 18)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Email Address"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label13.Location = New System.Drawing.Point(12, 530)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 18)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Fax"
        '
        'RefID_TextBox
        '
        Me.RefID_TextBox.Enabled = False
        Me.RefID_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefID_TextBox.Location = New System.Drawing.Point(11, 75)
        Me.RefID_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.RefID_TextBox.Name = "RefID_TextBox"
        Me.RefID_TextBox.Size = New System.Drawing.Size(236, 23)
        Me.RefID_TextBox.TabIndex = 1
        '
        'ContactNo_TextBox
        '
        Me.ContactNo_TextBox.Enabled = False
        Me.ContactNo_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactNo_TextBox.Location = New System.Drawing.Point(12, 124)
        Me.ContactNo_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.ContactNo_TextBox.Name = "ContactNo_TextBox"
        Me.ContactNo_TextBox.Size = New System.Drawing.Size(235, 23)
        Me.ContactNo_TextBox.TabIndex = 2
        '
        'ContactPerson_TextBox
        '
        Me.ContactPerson_TextBox.Enabled = False
        Me.ContactPerson_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContactPerson_TextBox.Location = New System.Drawing.Point(11, 323)
        Me.ContactPerson_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.ContactPerson_TextBox.Name = "ContactPerson_TextBox"
        Me.ContactPerson_TextBox.Size = New System.Drawing.Size(236, 23)
        Me.ContactPerson_TextBox.TabIndex = 6
        '
        'Address_TextBox
        '
        Me.Address_TextBox.Enabled = False
        Me.Address_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Address_TextBox.Location = New System.Drawing.Point(10, 454)
        Me.Address_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.Address_TextBox.Name = "Address_TextBox"
        Me.Address_TextBox.Size = New System.Drawing.Size(236, 23)
        Me.Address_TextBox.TabIndex = 8
        '
        'EmailAddress_TextBox
        '
        Me.EmailAddress_TextBox.Enabled = False
        Me.EmailAddress_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailAddress_TextBox.Location = New System.Drawing.Point(12, 503)
        Me.EmailAddress_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.EmailAddress_TextBox.Name = "EmailAddress_TextBox"
        Me.EmailAddress_TextBox.Size = New System.Drawing.Size(237, 23)
        Me.EmailAddress_TextBox.TabIndex = 9
        '
        'Fax_TextBox
        '
        Me.Fax_TextBox.Enabled = False
        Me.Fax_TextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fax_TextBox.Location = New System.Drawing.Point(15, 552)
        Me.Fax_TextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.Fax_TextBox.Name = "Fax_TextBox"
        Me.Fax_TextBox.Size = New System.Drawing.Size(237, 23)
        Me.Fax_TextBox.TabIndex = 10
        '
        'Supplier_DataGridView
        '
        Me.Supplier_DataGridView.AllowUserToAddRows = False
        Me.Supplier_DataGridView.AllowUserToDeleteRows = False
        Me.Supplier_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Supplier_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Supplier_DataGridView.Location = New System.Drawing.Point(0, 0)
        Me.Supplier_DataGridView.Margin = New System.Windows.Forms.Padding(4)
        Me.Supplier_DataGridView.Name = "Supplier_DataGridView"
        Me.Supplier_DataGridView.ReadOnly = True
        Me.Supplier_DataGridView.RowTemplate.Height = 24
        Me.Supplier_DataGridView.Size = New System.Drawing.Size(1106, 763)
        Me.Supplier_DataGridView.TabIndex = 30
        '
        'SupplierBindingSource
        '
        Me.SupplierBindingSource.DataSource = GetType(MB.Supplier)
        '
        'Delete_Button
        '
        Me.Delete_Button.Location = New System.Drawing.Point(463, -13)
        Me.Delete_Button.Margin = New System.Windows.Forms.Padding(4)
        Me.Delete_Button.Name = "Delete_Button"
        Me.Delete_Button.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Delete_Button.Size = New System.Drawing.Size(10, 11)
        Me.Delete_Button.TabIndex = 50
        Me.Delete_Button.Text = "Delete Selected"
        Me.Delete_Button.UseVisualStyleBackColor = True
        Me.Delete_Button.Visible = False
        '
        'ConfirmChanges_Button
        '
        Me.ConfirmChanges_Button.Enabled = False
        Me.ConfirmChanges_Button.ForeColor = System.Drawing.Color.DarkBlue
        Me.ConfirmChanges_Button.Location = New System.Drawing.Point(10, 748)
        Me.ConfirmChanges_Button.Margin = New System.Windows.Forms.Padding(4)
        Me.ConfirmChanges_Button.Name = "ConfirmChanges_Button"
        Me.ConfirmChanges_Button.Size = New System.Drawing.Size(120, 28)
        Me.ConfirmChanges_Button.TabIndex = 56
        Me.ConfirmChanges_Button.Tag = "S_Allow_Supplier_Save"
        Me.ConfirmChanges_Button.Text = "&Save Changes"
        Me.ConfirmChanges_Button.UseVisualStyleBackColor = True
        '
        'Result_Label
        '
        Me.Result_Label.AutoSize = True
        Me.Result_Label.ForeColor = System.Drawing.Color.Maroon
        Me.Result_Label.Location = New System.Drawing.Point(1133, 661)
        Me.Result_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Result_Label.Name = "Result_Label"
        Me.Result_Label.Size = New System.Drawing.Size(0, 18)
        Me.Result_Label.TabIndex = 59
        '
        'DetailDefault2_ComboBox
        '
        Me.DetailDefault2_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DetailDefault2_ComboBox.Enabled = False
        Me.DetailDefault2_ComboBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DetailDefault2_ComboBox.FormattingEnabled = True
        Me.DetailDefault2_ComboBox.Location = New System.Drawing.Point(11, 273)
        Me.DetailDefault2_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.DetailDefault2_ComboBox.Name = "DetailDefault2_ComboBox"
        Me.DetailDefault2_ComboBox.Size = New System.Drawing.Size(234, 24)
        Me.DetailDefault2_ComboBox.TabIndex = 5
        '
        'CategoryDefault_ComboBox
        '
        Me.CategoryDefault_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CategoryDefault_ComboBox.Enabled = False
        Me.CategoryDefault_ComboBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CategoryDefault_ComboBox.FormattingEnabled = True
        Me.CategoryDefault_ComboBox.Location = New System.Drawing.Point(11, 173)
        Me.CategoryDefault_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.CategoryDefault_ComboBox.Name = "CategoryDefault_ComboBox"
        Me.CategoryDefault_ComboBox.Size = New System.Drawing.Size(234, 24)
        Me.CategoryDefault_ComboBox.TabIndex = 3
        '
        'DetailDefault_ComboBox
        '
        Me.DetailDefault_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DetailDefault_ComboBox.Enabled = False
        Me.DetailDefault_ComboBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DetailDefault_ComboBox.FormattingEnabled = True
        Me.DetailDefault_ComboBox.Location = New System.Drawing.Point(12, 223)
        Me.DetailDefault_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.DetailDefault_ComboBox.Name = "DetailDefault_ComboBox"
        Me.DetailDefault_ComboBox.Size = New System.Drawing.Size(234, 24)
        Me.DetailDefault_ComboBox.TabIndex = 4
        '
        'VATDefault_ComboBox
        '
        Me.VATDefault_ComboBox.FormattingEnabled = True
        Me.VATDefault_ComboBox.Location = New System.Drawing.Point(11, 404)
        Me.VATDefault_ComboBox.Margin = New System.Windows.Forms.Padding(4)
        Me.VATDefault_ComboBox.Name = "VATDefault_ComboBox"
        Me.VATDefault_ComboBox.Size = New System.Drawing.Size(140, 24)
        Me.VATDefault_ComboBox.TabIndex = 4
        Me.VATDefault_ComboBox.Text = "1"
        Me.VATDefault_ComboBox.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(12, 301)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(116, 18)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Contact Person"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label6.Location = New System.Drawing.Point(12, 350)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(92, 18)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "VAT Default"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label17.Location = New System.Drawing.Point(12, 251)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(116, 18)
        Me.Label17.TabIndex = 64
        Me.Label17.Text = "Detail Default 2"
        '
        'cmbVAT
        '
        Me.cmbVAT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbVAT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbVAT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbVAT.Enabled = False
        Me.cmbVAT.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbVAT.FormattingEnabled = True
        Me.cmbVAT.Location = New System.Drawing.Point(11, 372)
        Me.cmbVAT.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbVAT.Name = "cmbVAT"
        Me.cmbVAT.Size = New System.Drawing.Size(236, 24)
        Me.cmbVAT.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Button1.Location = New System.Drawing.Point(19, 8)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Button1.Size = New System.Drawing.Size(203, 41)
        Me.Button1.TabIndex = 85
        Me.Button1.Tag = "S_Allow_Supplier_Add"
        Me.Button1.Text = "&Add New Supplier"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Edit_ID_Label
        '
        Me.Edit_ID_Label.AutoSize = True
        Me.Edit_ID_Label.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.Edit_ID_Label.Location = New System.Drawing.Point(415, 20)
        Me.Edit_ID_Label.Name = "Edit_ID_Label"
        Me.Edit_ID_Label.Size = New System.Drawing.Size(0, 18)
        Me.Edit_ID_Label.TabIndex = 86
        Me.Edit_ID_Label.Visible = False
        '
        'btnExit
        '
        Me.btnExit.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnExit.Location = New System.Drawing.Point(876, 557)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(102, 28)
        Me.btnExit.TabIndex = 87
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnEditSupplier
        '
        Me.btnEditSupplier.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnEditSupplier.Location = New System.Drawing.Point(230, 8)
        Me.btnEditSupplier.Margin = New System.Windows.Forms.Padding(4)
        Me.btnEditSupplier.Name = "btnEditSupplier"
        Me.btnEditSupplier.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.btnEditSupplier.Size = New System.Drawing.Size(165, 41)
        Me.btnEditSupplier.TabIndex = 86
        Me.btnEditSupplier.Tag = "S_Allow_Supplier_Edit"
        Me.btnEditSupplier.Text = "&Edit Supplier"
        Me.btnEditSupplier.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label2.Location = New System.Drawing.Point(12, 579)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 18)
        Me.Label2.TabIndex = 90
        Me.Label2.Text = "Terms"
        '
        'NudTerms
        '
        Me.NudTerms.Enabled = False
        Me.NudTerms.Location = New System.Drawing.Point(15, 600)
        Me.NudTerms.Name = "NudTerms"
        Me.NudTerms.Size = New System.Drawing.Size(236, 23)
        Me.NudTerms.TabIndex = 91
        '
        'ckbIsActive
        '
        Me.ckbIsActive.AutoSize = True
        Me.ckbIsActive.Enabled = False
        Me.ckbIsActive.ForeColor = System.Drawing.Color.DarkBlue
        Me.ckbIsActive.Location = New System.Drawing.Point(15, 629)
        Me.ckbIsActive.Name = "ckbIsActive"
        Me.ckbIsActive.Size = New System.Drawing.Size(103, 22)
        Me.ckbIsActive.TabIndex = 92
        Me.ckbIsActive.Text = "Is Active ?"
        Me.ckbIsActive.UseVisualStyleBackColor = True
        '
        'Lbl_Edit_SuppRef
        '
        Me.Lbl_Edit_SuppRef.AutoSize = True
        Me.Lbl_Edit_SuppRef.Location = New System.Drawing.Point(290, 8)
        Me.Lbl_Edit_SuppRef.Name = "Lbl_Edit_SuppRef"
        Me.Lbl_Edit_SuppRef.Size = New System.Drawing.Size(0, 18)
        Me.Lbl_Edit_SuppRef.TabIndex = 93
        Me.Lbl_Edit_SuppRef.Visible = False
        '
        'TB_IntegrationCode
        '
        Me.TB_IntegrationCode.Enabled = False
        Me.TB_IntegrationCode.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_IntegrationCode.Location = New System.Drawing.Point(11, 717)
        Me.TB_IntegrationCode.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_IntegrationCode.Name = "TB_IntegrationCode"
        Me.TB_IntegrationCode.Size = New System.Drawing.Size(237, 23)
        Me.TB_IntegrationCode.TabIndex = 94
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(12, 695)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(127, 18)
        Me.Label4.TabIndex = 95
        Me.Label4.Text = "Integration Code"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.Btn_SuppBank)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.RefID_label)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TB_IntegrationCode)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ConfirmChanges_Button)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SupplierName_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label13)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label11)
        Me.SplitContainer1.Panel1.Controls.Add(Me.DetailDefault2_ComboBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label17)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Fax_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ContactNo_label)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CategoryDefault_ComboBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ckbIsActive)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EmailAddress_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.DetailDefault_ComboBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.NudTerms)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Address_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.VATDefault_ComboBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ContactPerson_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbVAT)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ContactNo_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.RefID_TextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label12)
        Me.SplitContainer1.Panel1MinSize = 250
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Lbl_Edit_SuppRef)
        Me.SplitContainer1.Size = New System.Drawing.Size(1386, 828)
        Me.SplitContainer1.SplitterDistance = 274
        Me.SplitContainer1.TabIndex = 96
        '
        'Btn_SuppBank
        '
        Me.Btn_SuppBank.Enabled = False
        Me.Btn_SuppBank.Location = New System.Drawing.Point(123, 660)
        Me.Btn_SuppBank.Name = "Btn_SuppBank"
        Me.Btn_SuppBank.Size = New System.Drawing.Size(130, 23)
        Me.Btn_SuppBank.TabIndex = 96
        Me.Btn_SuppBank.Tag = "S_Allow_Supplier_Bank"
        Me.Btn_SuppBank.Text = "Bank Details"
        Me.Btn_SuppBank.UseVisualStyleBackColor = True
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.Cb_ShowDeactivated)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Lbl_Edit_SuppName)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer2.Panel1.Controls.Add(Me.btnEditSupplier)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Edit_ID_Label)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.Supplier_DataGridView)
        Me.SplitContainer2.Size = New System.Drawing.Size(1106, 826)
        Me.SplitContainer2.SplitterDistance = 59
        Me.SplitContainer2.TabIndex = 94
        '
        'Lbl_Edit_SuppName
        '
        Me.Lbl_Edit_SuppName.AutoSize = True
        Me.Lbl_Edit_SuppName.Location = New System.Drawing.Point(534, 22)
        Me.Lbl_Edit_SuppName.Name = "Lbl_Edit_SuppName"
        Me.Lbl_Edit_SuppName.Size = New System.Drawing.Size(0, 18)
        Me.Lbl_Edit_SuppName.TabIndex = 87
        Me.Lbl_Edit_SuppName.Visible = False
        '
        'Cb_ShowDeactivated
        '
        Me.Cb_ShowDeactivated.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cb_ShowDeactivated.AutoSize = True
        Me.Cb_ShowDeactivated.Location = New System.Drawing.Point(947, 27)
        Me.Cb_ShowDeactivated.Name = "Cb_ShowDeactivated"
        Me.Cb_ShowDeactivated.Size = New System.Drawing.Size(156, 22)
        Me.Cb_ShowDeactivated.TabIndex = 88
        Me.Cb_ShowDeactivated.Text = "Show Deactivated"
        Me.Cb_ShowDeactivated.UseVisualStyleBackColor = True
        '
        'frmSuppliers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1386, 828)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Delete_Button)
        Me.Controls.Add(Me.Result_Label)
        Me.Font = New System.Drawing.Font("MS Reference Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(18, 626)
        Me.Name = "frmSuppliers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_SupplierMaster"
        Me.Text = "Suppliers"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Supplier_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SupplierBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudTerms, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SupplierName_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RefID_label As System.Windows.Forms.Label
    Friend WithEvents ContactNo_label As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents RefID_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContactNo_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContactPerson_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Address_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailAddress_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fax_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Supplier_DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Delete_Button As System.Windows.Forms.Button
    Friend WithEvents ConfirmChanges_Button As System.Windows.Forms.Button
    Friend WithEvents Result_Label As System.Windows.Forms.Label
    Friend WithEvents DetailDefault2_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CategoryDefault_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DetailDefault_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents VATDefault_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cmbVAT As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Edit_ID_Label As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnEditSupplier As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NudTerms As System.Windows.Forms.NumericUpDown
    Friend WithEvents ckbIsActive As System.Windows.Forms.CheckBox
    Friend WithEvents SupplierBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Lbl_Edit_SuppRef As System.Windows.Forms.Label
    Friend WithEvents TB_IntegrationCode As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents Btn_SuppBank As System.Windows.Forms.Button
    Friend WithEvents Lbl_Edit_SuppName As System.Windows.Forms.Label
    Friend WithEvents Cb_ShowDeactivated As System.Windows.Forms.CheckBox
End Class
