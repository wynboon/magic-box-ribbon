﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class Accounts

    Private Sub Accounts_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            e.Cancel = True
            Me.ShowInTaskbar = False
            Me.Hide()
        End If
    End Sub
    Private Sub Accounts_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Accounts -> Version : " & My.Settings.Setting_Version

        Try
            Me.TopMost = True
            Me.Label_Version.Text = "Version " & My.Settings.Setting_Version
            Call Fill_DGV()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try

    End Sub

    Sub Fill_DGV()
        Try
            Dim sSQL As String

            sSQL = "Select * From Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                oAccessAdapter_Accounts = New OleDbDataAdapter(sSQL, connection)
                oAccessAdapter_Accounts.Fill(oAccessTable_Accounts)
                Me.DataGridView1.DataSource = oAccessTable_Accounts
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                oSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
                oSQLAdapter_Accounts.Fill(oSQLTable_Accounts)
                Me.DataGridView1.DataSource = oSQLTable_Accounts
            End If

            Me.DataGridView1.Columns(0).Width = 40 'not editable
            Me.DataGridView1.Columns(0).DefaultCellStyle.BackColor = Color.LightGray
            Me.DataGridView1.Columns(0).ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message & " 083")
        End Try
    End Sub

    Public oAccessAdapter_Accounts As OleDbDataAdapter
    Public oAccessTable_Accounts As New DataTable
    Public oSQLAdapter_Accounts As SqlDataAdapter
    Public oSQLTable_Accounts As New DataTable

    Private Sub Button_Save_Click(sender As System.Object, e As System.EventArgs) Handles Button_Save.Click
        Try
            If My.Settings.DBType = "Access" Then
                Dim builder As New OleDbCommandBuilder(oAccessAdapter_Accounts)
                builder.QuotePrefix = "["
                builder.QuoteSuffix = "]"
                oAccessAdapter_Accounts.Update(oAccessTable_Accounts)
                oAccessAdapter_Accounts.UpdateCommand = builder.GetUpdateCommand()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim builder As New SqlCommandBuilder(oSQLAdapter_Accounts)
                builder.QuotePrefix = "["
                builder.QuoteSuffix = "]"
                oSQLAdapter_Accounts.Update(oSQLTable_Accounts)
                oSQLAdapter_Accounts.UpdateCommand = builder.GetUpdateCommand()
            End If
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub


    Private Sub Button_Close_Click(sender As System.Object, e As System.EventArgs) Handles Button_Close.Click
        Me.Close()
    End Sub

    Private Sub Label_Version_Click(sender As System.Object, e As System.EventArgs) Handles Label_Version.Click

    End Sub
End Class