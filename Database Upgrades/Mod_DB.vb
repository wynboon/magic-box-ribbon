﻿Imports System.Data.SqlClient

Module Mod_DB
    Private ModDB_CS As String
    Function CheckFields()

        Dim ConnString As String() = My.Settings.CS_Setting.Split(New Char() {"="c})
        If ConnString.Length = 1 Then Exit Function
        Dim ServerSplit() As String = ConnString(1).Split(New Char() {";"c})
        Dim DatabaseSplit() As String = ConnString(2).Split(New Char() {";"c})
        ModDB_CS = "Server=" & CStr(ServerSplit(0).ToString) & ";Database=" & CStr(DatabaseSplit(0).ToString) & ";Uid=sa; Pwd=Bu1ldSm@rt"
        Call BankPayType()
        Call Periods_Blocked()
        Call CheckTaxTable()
        Call Tax_Default()
        Call PayRequest_BankRequestID()
        Call PayRequest_BankExported()
        Call JournalTemplateHearerTable()
        Call JournalTemplateLineTable()
        Call JournalTemplateLineTable()
    End Function
#Region "LogFile"
    Private Sub WriteDebug(ByVal x As String)
        Try
            Dim path As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
            Dim FILE_NAME As String = path & "\Magic Box\DBLog.txt"
            If System.IO.File.Exists(FILE_NAME) = False Then
                System.IO.File.Create(FILE_NAME).Dispose()
            End If
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(x)
            objWriter.Close()
        Catch ex As Exception
            '   MsgBox("Error while adding to DB Log. Error: " & ex.Message)
        End Try
    End Sub
#End Region
#Region "DataFields"
    Function BankPayType() As String
        Try
            Dim sSQL As String
            sSQL = "IF COL_LENGTH('Accounting', 'BankLayout') IS NULL " & vbNewLine & _
            "BEGIN " & vbNewLine & _
            "ALTER TABLE Accounting " & vbNewLine & _
            "ADD BankLayout varchar(3) " & vbNewLine & _
            "End "
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Bank Layout to accounting Table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString & vbNewLine & ModDB_CS)
        End Try

    End Function
    Function Periods_Blocked() As String
        Try
            Dim sSQL As String
            sSQL = "IF COL_LENGTH('Periods', 'BLOCKED') IS NULL " & vbNewLine & _
            "BEGIN " & vbNewLine & _
            "ALTER TABLE Periods " & vbNewLine & _
            "ADD BLOCKED varchar(1) " & vbNewLine & _
            "End "
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding blocked to periods Table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try

    End Function
    Function PayRequest_BankRequestID() As String
        Try
            Dim sSQL As String
            sSQL = "IF COL_LENGTH('PaymentsRequests', 'BankRequestID') IS NULL " & vbNewLine & _
            "BEGIN " & vbNewLine & _
            "ALTER TABLE PaymentsRequests " & vbNewLine & _
            "ADD BankRequestID Int " & vbNewLine & _
            "End "
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding BankRequestID to PaymentsRequests Table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try
    End Function
    Function PayRequest_BankExported() As String
        Try
            Dim sSQL As String
            sSQL = "IF COL_LENGTH('PaymentsRequests', 'BankExported') IS NULL " & vbNewLine & _
            "BEGIN " & vbNewLine & _
            "ALTER TABLE PaymentsRequests " & vbNewLine & _
            "ADD BankExported Bit " & vbNewLine & _
            "End "
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding BankExported to PaymentsRequests Table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try
    End Function
    Function JournalTemplateHearerTable() As String
        Try
            Dim sSQL As String
            sSQL = "if not exists (select * from sysobjects where name='Journal_Header' and xtype='U') " & vbNewLine & _
                    "create table Journal_Header ([HeaderID] [int] IDENTITY(1,1) NOT NULL,[JournalName] [varchar](100) NULL," & _
                    "[JournalDescription] [varchar](500) NULL, [Co_ID] [int] NULL)"
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding Journal_Header table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try
    End Function
    Function JournalTemplateLineTable() As String
        Try
            Dim sSQL As String
            sSQL = "if not exists (select * from sysobjects where name='Journal_Lines' and xtype='U') " & vbNewLine & _
                    "create table Journal_Lines ([DetailID] [int] IDENTITY(1,1) NOT NULL,[HeaderID] [int] NOT NULL,[LineDesc] [varchar](200) NULL," & _
                    "[LineAccount] [varchar](6) NULL,[LineAccountDesc] [varchar](200) NULL,[LineAccountContra] [varchar](6) NULL," & _
                    "[LineAccountDescContra] [varchar](200) NULL,[LineDRCR] [varchar](2) NULL,[LineAmount] [decimal](18, 2) NULL,[LineHasTax] [bit] NULL)"
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding Journal_Header table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try
    End Function
#End Region
    Function CheckTaxTable() As String
        Try
            Dim sSQL As String
            sSQL = "if not exists (select * from sysobjects where name='TaxTypes' and xtype='U') " & vbNewLine & _
                    "CREATE TABLE [dbo].[TaxTypes]([ID] [int] IDENTITY(1,1) NOT NULL,[TaxDesc] [varchar](50) NULL,[TaxPerc] [float] NULL,[Co_ID] [int] NULL,[Default] [bit] NULL) ON [PRIMARY]"
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding Tax table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try
    End Function
    Function Tax_Default() As String
        Try
            Dim sSQL As String
            sSQL = "IF COL_LENGTH('TaxTypes', 'Default') IS NULL " & vbNewLine & _
            "BEGIN " & vbNewLine & _
            "ALTER TABLE TaxTypes " & vbNewLine & _
            "ADD Default [bit] NULL DEFAULT 0 WITH VALUES " & vbNewLine & _
            "End "
            Dim connection As New SqlConnection(ModDB_CS & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            WriteDebug("Error Adding Default to TaxTypes Table. Error: " & ex.Message & vbNewLine & Now.ToLongDateString & " " & Now.ToLongTimeString)
        End Try
    End Function
End Module
