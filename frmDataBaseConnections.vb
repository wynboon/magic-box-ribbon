﻿

Imports System.Windows.Forms
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class frmDataBaseConnections

    Private Sub btnAddCS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddCS.Click
        Try

            Dim frmConnBuilder As New frmDatabaseConnectionSettings
            frmConnBuilder.ShowDialog()

            If frmConnBuilder.ExitMethod <> "Exit" Then

                If CStr(frmConnBuilder.ConnectionName) <> "" And CStr(frmConnBuilder.ConnString) <> "" Then

                    Dim ConnName As String = CStr(frmConnBuilder.ConnectionName)
                    Dim databaseType As String = "SQL"
                    Dim ConnString As String = CStr(frmConnBuilder.ConnString)

                    Dim str(2) As String
                    Dim itm As ListViewItem

                    str(0) = ConnName
                    str(1) = databaseType
                    str(2) = ConnString

                    itm = New ListViewItem(str)
                    Me.ListViewCS.Items.Add(itm)

                    savings = oBuild_String_from_ListView()

                    Enabled = True
                    Cursor = Cursors.Arrow

                End If

            End If
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnEditCS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditCS.Click
        Try

            If ListViewCS.SelectedItems.Count = 0 Or ListViewCS.SelectedItems.Count > 1 Then
                MessageBox.Show("Please select one connection in order to continue", "Connection editing", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                ListViewCS.Focus()
                Exit Sub
            End If

            Dim ConnectionString As String = CStr(ListViewCS.FocusedItem.SubItems(2).Text)
            Dim ConnName As String = CStr(ListViewCS.FocusedItem.SubItems(0).Text)
            'cmbDatabaseType.Text = ListViewCS.FocusedItem.SubItems(1).Text

            Dim frmConnBuilder As New frmDatabaseConnectionSettings
            frmConnBuilder.ConnString = ConnectionString
            frmConnBuilder.ConnectionName = ConnName
            frmConnBuilder.ShowDialog()

            If frmConnBuilder.ExitMethod <> "Exit" Then

                Me.Enabled = False
                Me.Cursor = Cursors.AppStarting

                Dim oCol1 As String = CStr(frmConnBuilder.ConnectionName)
                Dim oCol2 As String = "SQL"
                Dim oCol3 As String = CStr(frmConnBuilder.ConnString)

                With Me.ListViewCS.SelectedItems(0)
                    If oCol1 <> String.Empty Then
                        .SubItems(0).Text = oCol1
                    End If
                    If oCol2 <> String.Empty Then
                        .SubItems(1).Text = oCol2
                    End If
                    If oCol3 <> String.Empty Then
                        .SubItems(2).Text = oCol3
                    End If
                End With

                savings = oBuild_String_from_ListView()

                Me.Enabled = True
                Me.Cursor = Cursors.Arrow

            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnDeleteCS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteCS.Click
        Try

            If Me.ListViewCS.SelectedItems.Count > 1 Or Me.ListViewCS.SelectedItems.Count < 1 Then
                MessageBox.Show("Please select one connection in order to continue", "Connection deleting", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                ListViewCS.Focus()
                Exit Sub
            End If

            Me.Refresh()

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim items(Me.ListViewCS.SelectedItems.Count - 1) As Object 'this code needed as multiselect is true
            Me.ListViewCS.SelectedItems.CopyTo(items, 0)

            For Each item As ListViewItem In items
                Me.ListViewCS.Items.Remove(item)
            Next

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Dim savings As String = ""
    Public NewItems As Boolean = False
    Function oBuild_String_from_ListView() As String

        Dim i As Integer
        Dim S1 As String
        Dim S2 As String
        Dim S3 As String
        Dim S As String = ""
        Dim oJoinedString As String = ""
        For i = 0 To Me.ListViewCS.Items.Count - 1
            S1 = Me.ListViewCS.Items(i).SubItems(2).Text
            S2 = Me.ListViewCS.Items(i).SubItems(1).Text
            S3 = Me.ListViewCS.Items(i).SubItems(0).Text

            S = S1 & "~" & S2 & "~" & S3
            If i = 0 Then
                oJoinedString = S
            Else
                oJoinedString = oJoinedString & "|" & S
            End If
        Next
        Dim f As String = ""
        oBuild_String_from_ListView = oJoinedString
        savings = My.Settings.ListViewValues
    End Function
    Private Sub frmDataBaseConnections_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Me.ListViewCS.Columns.Add("", 153, HorizontalAlignment.Left)
            Me.ListViewCS.Columns.Add("", 172, HorizontalAlignment.Left)

            If CStr(My.Settings.CS_Setting) = String.Empty And CStr(My.Settings.ListViewValues) = String.Empty Then
                Me.Text = "MagicBox -> Connections -> Version : " & My.Settings.Setting_Version
                btnEditCS.Enabled = False
                btnDeleteCS.Enabled = False
                Exit Sub
            Else
                Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Connections -> Version : " & My.Settings.Setting_Version
            End If

            If CStr(My.Settings.CS_Setting) <> String.Empty And CStr(My.Settings.ListViewValues) <> String.Empty Then


                Enabled = False
                Cursor = Cursors.AppStarting

                Dim arrSplit As Object = Split(CStr(My.Settings.ListViewValues), "|")
                Dim strListViewLine As String
                Dim arrSplit2 As Object 'used to split each line

                Dim str(2) As String
                Dim itm As ListViewItem

                For i As Integer = 0 To UBound(arrSplit)
                    strListViewLine = arrSplit(i)
                    arrSplit2 = Split(strListViewLine, "~")
                    str(0) = arrSplit2(2)
                    str(1) = arrSplit2(1)
                    str(2) = arrSplit2(0)
                    itm = New ListViewItem(str)
                    Me.ListViewCS.Items.Add(itm)
                Next

                btnDeleteCS.Enabled = True
                btnEditCS.Enabled = True

                Enabled = True
                Cursor = Cursors.Arrow

            End If
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Function oTest_Connection_String(ByVal strConnectionString As String, ByVal oDatabaseType As String) As Boolean
        Try

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim sSQL As String = "Select TOP 1 Co_ID From Periods"

            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(strConnectionString)
                Dim cmd As New OleDbCommand(sSQL, connection)
                connection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader
                connection.Close()
                oTest_Connection_String = True

            ElseIf My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(strConnectionString & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader
                connection.Close()
                oTest_Connection_String = True
            End If

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            oTest_Connection_String = False
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("Please double check and make sure that the connection string you provided is a valid database connection string", "Connection Test", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Function
    Private Sub btnSaveCS_Click(sender As Object, e As EventArgs) Handles btnSaveCS.Click
        Try
            Enabled = False
            Cursor = Cursors.AppStarting

            My.Settings.ListViewValues = oBuild_String_from_ListView()
            My.Settings.Save()
            If My.Settings.ListViewValues = savings Then
                MessageBox.Show("Connection strings succesfully saved", "Connection strings", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                NewItems = True
                MessageBox.Show("Changes to connection succesfully saved", "Connection strings", MessageBoxButtons.OK, MessageBoxIcon.Information)
                savings = CStr(My.Settings.ListViewValues)
            End If

            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmDataBaseConnections_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If savings <> "" Then

            If Not CStr(My.Settings.ListViewValues) = savings Then
                Dim ResponseDial As DialogResult = MessageBox.Show("there were changes detected on the connections, do you want to save the changes?", "Connections", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If ResponseDial = Windows.Forms.DialogResult.Yes Then
                    My.Settings.ListViewValues = oBuild_String_from_ListView()
                    My.Settings.Save()
                    NewItems = True
                    MessageBox.Show("Changes to connections succesfully saved", "Connection strings", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If

        End If
    End Sub
End Class