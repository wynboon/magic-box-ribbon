﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D




Public Class frmAllocations

#Region "Variables"
    Public ListIndex_on_Mousehover As Integer
    Dim MBData As New DataExtractsClass
    Dim FormLoaded As Boolean
#End Region
    Private Sub frmAllocations_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            LockForm(Me)
            Enabled = False
            Cursor = Cursors.AppStarting

            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Allocations -> Version : " & My.Settings.Setting_Version
            FillComboBoxSuppliers()
            Me.DateFrom1.Value = System.DateTime.Now.AddMonths(-1)
            Me.DateFrom2.Value = System.DateTime.Now.AddMonths(-1)
            Me.cmbAllocate.SelectedIndex = 0

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub FillComboBoxSuppliers()

        Try
            If My.Settings.Setting_CompanyID <> "" Then
                FormLoaded = False
                cmbSupplier1.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier2.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier1.DisplayMember = "SupplierName"
                cmbSupplier1.ValueMember = "SupplierID"
                cmbSupplier2.DisplayMember = "SupplierName"
                cmbSupplier2.ValueMember = "SupplierID"
                cmbSupplier1.SelectedIndex = 0
                cmbSupplier2.SelectedIndex = 0
                FormLoaded = True
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSupplier1.SelectedIndexChanged
        If Me.cmbAllocate.SelectedIndex = 0 Then
            Call Get_Supplier_Debits()
        ElseIf Me.cmbAllocate.SelectedIndex = 1 Then
            'Call GetInvoiceDetailPerSupplier()
        End If
        cmbSupplier2.Text = cmbSupplier1.Text

    End Sub
    Private Sub cmbStatus1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbStatus1.SelectedIndexChanged
        If Me.cmbAllocate.SelectedIndex = 0 Then
            Call Get_Supplier_Debits()
        ElseIf Me.cmbAllocate.SelectedIndex = 1 Then
            'Call GetInvoiceDetailPerSupplier()
        End If
    End Sub

    Sub GetInvoiceDetailPerSupplier()

        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            Dim oSupplier As String
            oSupplier = Me.cmbSupplier1.Text
            Dim sSQL_Supplier As String = ""

            If oSupplier = "[ALL]" Or oSupplier = "" Then
                'do nothing
            Else
                oSupplier = Me.cmbSupplier1.Text
                sSQL_Supplier = " And SupplierName = '" & SQLConvert(oSupplier) & "'"
            End If


            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.DateFrom1.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTo1.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If

            Dim sSQL As String

            If Me.cmbStatus1.Text = "[ALL]" Then
                sSQL = "Select Trans, SupplierName, Reference, Invoiced, Due, [Transaction Date] From [Step4b] Where Due >=0 And Invoiced > 0"
            ElseIf Me.cmbStatus1.Text = "PAID" Then
                sSQL = "Select Trans, SupplierName, Reference, Invoiced, Due, [Transaction Date] From [Step4b] Where Due =0 And Invoiced > 0"
            ElseIf Me.cmbStatus1.Text = "UNPAID" Then
                sSQL = "Select Trans, SupplierName, Reference, Invoiced, Due, [Transaction Date] From [Step4b] Where Due >0 And Invoiced > 0"
            End If

            sSQL = sSQL & sSQL_Supplier

            sSQL = sSQL & sDateCriteria

            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID

            'First clear DGV
            DataGridView1.DataSource = Nothing


            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            End If
            If DataGridView1.ColumnCount > 0 Then
                Me.DataGridView1.Columns(0).Visible = False

                Me.DataGridView1.Columns(3).DefaultCellStyle.Format = "n2"
                Me.DataGridView1.Columns(4).DefaultCellStyle.Format = "n2"
                Me.DataGridView1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                Me.DataGridView1.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 312")
        End Try

    End Sub


    Sub GetInvoiceDetailPerSupplier2()
        If FormLoaded = False Then Exit Sub
        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            Dim oSupplier As String
            Dim oSupplierID As Integer
            oSupplier = Me.cmbSupplier2.Text
            oSupplierID = cmbSupplier2.SelectedValue
            Dim sSQL_Supplier As String = ""

            If oSupplier = "[ALL]" Or oSupplier = "" Then
                'do nothing
            Else
                oSupplier = Me.cmbSupplier2.Text
                sSQL_Supplier = " And SupplierID = " & oSupplierID
            End If


            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom2 As String = Me.DateFrom2.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo2 As String = Me.DateTo2.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom2 & "# And [Transaction Date] <= #" & oTo2 & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom2 & "' And [Transaction Date] <= '" & oTo2 & "')"
            End If

            Dim sSQL As String
            If Me.cmbStatus1.Text = "[ALL]" Then
                sSQL = "Select Trans, SupplierName, Reference, Invoiced, Due, [Transaction Date] From [Func_SupplierInvoiceAmountAndDue](1," & My.Settings.Setting_CompanyID & "," & oSupplierID & ") Where Due >= 0 And Invoiced > 0"
            ElseIf Me.cmbStatus1.Text = "PAID" Then
                sSQL = "Select Trans, SupplierName, Reference, Invoiced, Due, [Transaction Date] From Func_SupplierBalanceDetailNoDate(1," & My.Settings.Setting_CompanyID & "," & oSupplierID & ") Where Due =0 And Invoiced > 0"
            ElseIf Me.cmbStatus1.Text = "UNPAID" Then
                sSQL = "Select TransactionID, SupplierName, Reference, Invoiced, Due, [Transaction Date] From Func_SupplierInvoiceAmountAndDue(" & My.Settings.Setting_CompanyID & "," & oSupplierID & ") Where Due >0 And Invoiced > 0"
            End If

            'sSQL = sSQL & sSQL_Supplier


            sSQL = sSQL & sDateCriteria

            'If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            'sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            'End If

            'First clear DGV
            DataGridView2.DataSource = Nothing


            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView2.DataSource = ds
                DataGridView2.DataMember = "Suppliers_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView2.DataSource = ds
                DataGridView2.DataMember = "Suppliers_table"
            End If
            If DataGridView2.ColumnCount > 0 Then
                Me.DataGridView2.Columns(0).Visible = False

                Me.DataGridView2.Columns(3).DefaultCellStyle.Format = "n2"
                Me.DataGridView2.Columns(4).DefaultCellStyle.Format = "n2"
                Me.DataGridView2.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                Me.DataGridView2.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 313")
        End Try

    End Sub

    Sub Get_Supplier_Debits()

        Try

            If Globals.Ribbons.Ribbon1.btnOnOff.Label = "Inactive" Then Exit Sub

            Dim oSupplier As String
            Dim oSupplierID As String
            Dim CreditorControlAccount As String
            CreditorControlAccount = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
            oSupplier = Me.cmbSupplier1.Text
            oSupplierID = cmbSupplier1.SelectedValue
            Dim sSQL_Supplier As String = ""

            If oSupplier = "[ALL]" Or oSupplier = "" Then
                'do nothing
            Else
                oSupplier = Me.cmbSupplier1.Text
                oSupplierID = cmbSupplier1.SelectedValue
                sSQL_Supplier = " And Transactions.SupplierID = " & oSupplierID

            End If


            'USE THE FOLLOWING TO ENSURE DATE SETTING DIFFERENCES ACROSS COMPUTERS
            Dim oFrom As String = Me.DateFrom1.Value.Date.ToString("dd MMMM yyyy")
            Dim oTo As String = Me.DateTo1.Value.Date.ToString("dd MMMM yyyy")

            Dim sDateCriteria As String
            If My.Settings.DBType = "Access" Then
                sDateCriteria = " And ([Transaction Date] >= #" & oFrom & "# And [Transaction Date] <= #" & oTo & "#)"
            ElseIf My.Settings.DBType = "SQL" Then
                sDateCriteria = " And ([Transaction Date] >= '" & oFrom & "' And [Transaction Date] <= '" & oTo & "')"
            End If

            Dim sSQL As String
            'SELECT CONVERT(VARCHAR,[Transaction Date],106) From Transactions
            If My.Settings.DBType = "Access" Then
                'sSQL = "Select Trans, SupplierName, Reference, Invoiced As Amount, [Transaction Date] From [Step8_SupplierDebits] Where Invoiced < 0"
                sSQL = "Select TransactionID, SupplierName, Reference, Amount, [Transaction Date] From TransactionsWhere Info = 'Supplier Debit' And Void = False"
                sSQL = sSQL & sSQL_Supplier
                sSQL = sSQL & sDateCriteria

            Else
                sSQL = "Select TransactionID , Suppliers.SupplierName, Reference, Amount, CONVERT(VARCHAR,[Transaction Date],106) As [Transaction Date] From Transactions " & _
                    "left join suppliers on Transactions.SupplierID = Suppliers.SupplierID " & _
                    "Where ((Info ='Supplier Debit' And Void = 0) or (Info2 ='Credit Note' And Void = 0)) and [Description Code] = '" & CreditorControlAccount & "' "
                sSQL = sSQL & sSQL_Supplier
                sSQL = sSQL & sDateCriteria

            End If

            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Transactions.Co_ID = " & My.Settings.Setting_CompanyID
            End If


            'First clear DGV
            DataGridView1.DataSource = Nothing


            If My.Settings.DBType = "Access" Then
                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            ElseIf My.Settings.DBType = "SQL" Then
                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim dataadapter As New SqlDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Suppliers_table")
                connection.Close()
                DataGridView1.DataSource = ds
                DataGridView1.DataMember = "Suppliers_table"
            End If
            If DataGridView1.ColumnCount > 0 Then
                Me.DataGridView1.Columns(0).Visible = False

                Me.DataGridView1.Columns(3).DefaultCellStyle.Format = "n2"

                Me.DataGridView1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

            End If


        Catch ex As Exception
            MsgBox(ex.Message & " 314")
        End Try

    End Sub


    Private Sub DataGridView1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseDown
        Try

            Dim info As DataGridView.HitTestInfo = Me.DataGridView1.HitTest(e.X, e.Y)
            Dim row As DataGridViewRow = Me.DataGridView1.Rows(info.RowIndex)

            Me.DataGridView1.DoDragDrop(row, DragDropEffects.Copy)

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView2.DoubleClick

    End Sub



    Private Sub DataGridView2_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles DataGridView2.DragDrop
        Try
            Dim row As DataGridViewRow = TryCast(e.Data.GetData(GetType(DataGridViewRow)), DataGridViewRow)
            'This is where the row from the ist DGV is effectively copied and added to the second DGV
            If row IsNot Nothing Then

                Dim oAmount As Decimal
                Dim oDue As Decimal
                '-------------------------------------------------------------------------------------------------
                'Dim newRow As New DataGridViewRow()
                'newRow.CreateCells(Me.DataGridView1)
                'Type of code used in Supplier Payments where DataGridView1 data is used to populate DataGridView2
                'newRow.Cells(0).Value = row.Cells(0).Value
                'newRow.Cells(1).Value = row.Cells(1).Value
                '-------------------------------------------------------------------------------------------------
                Try
                    Me.B0.Text = row.Cells(0).Value
                    Me.B1.Text = row.Cells(1).Value
                    Me.B2.Text = row.Cells(2).Value
                    Me.B3.Text = row.Cells(3).Value
                    Me.B4.Text = row.Cells(4).Value
                    Me.B5.Text = row.Cells(5).Value
                    Me.B6.Text = row.Cells(6).Value

                Catch ex As Exception
                    'no error as last few might be Nulls
                End Try

                If Me.DataGridView1.Rows(row.Index).DefaultCellStyle.ForeColor <> Color.OrangeRed Then
                    'Don't add the row if it is already owolintshi
                    'Me.DataGridView2.Rows.Add(newRow) 
                    '***Color line orange -on the drop
                    Me.DataGridView1.Rows(row.Index).DefaultCellStyle.ForeColor = Color.OrangeRed
                    Me.DataGridView1.ClearSelection()

                Else
                    MsgBox("This row has already been dragged")
                End If

            End If

        Catch
            MsgBox(Err.Description)
        End Try

        Try
            Call GetListIndexWithoutClicking()

            Me.Index2.Text = CStr(ListIndex_on_Mousehover)

            If ListIndex_on_Mousehover = -1 Then
                MsgBox("No row on drop. Please drop the data onto a valid row in the bottom grid")
            Else
                Call oAllocate_Supplier_Debits_to_Invoice(ListIndex_on_Mousehover)
            End If

            ListIndex_on_Mousehover = Nothing 'set this back to nothing

        Catch ex As Exception

        End Try
    End Sub


    Private Sub GetListIndexWithoutClicking()

        Dim LocalMousePosition As Point
        LocalMousePosition = Me.DataGridView2.PointToClient(Cursor.Position)

        Dim x As Integer = LocalMousePosition.X
        Dim y As Integer = LocalMousePosition.Y
        Dim hitTestInfo As DataGridView.HitTestInfo
        hitTestInfo = Me.DataGridView2.HitTest(x, y)
        If hitTestInfo.Type = DataGridViewHitTestType.Cell Then
            ListIndex_on_Mousehover = hitTestInfo.RowIndex
        Else
            ListIndex_on_Mousehover = -1
        End If

    End Sub


    Private Sub DataGridView2_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles DataGridView2.DragEnter
        Try
            e.Effect = DragDropEffects.All
        Catch

        End Try
    End Sub

    Private Sub cmbAllocate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAllocate.SelectedIndexChanged
        If cmbAllocate.SelectedIndex = 0 Then
            '0: Allocate Supplier Debits to Invoices
            Call FillComboBoxSuppliers()
            lblStatus1.Visible = False
            cmbStatus1.Visible = False
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSupplier2.SelectedIndexChanged
        Call GetInvoiceDetailPerSupplier2()
    End Sub

    Private Sub cmbStatus2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbStatus2.SelectedIndexChanged
        Call GetInvoiceDetailPerSupplier2()
    End Sub

    Private Sub DateFrom2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateFrom2.ValueChanged
        Call GetInvoiceDetailPerSupplier2()
    End Sub

    Private Sub DateTo2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTo2.ValueChanged
        Call GetInvoiceDetailPerSupplier2()
    End Sub

    Private Sub DateFrom1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateFrom1.ValueChanged
        If Me.cmbAllocate.SelectedIndex = 0 Then
            Call Get_Supplier_Debits()
        ElseIf Me.cmbAllocate.SelectedIndex = 1 Then
            'Call GetInvoiceDetailPerSupplier()
        End If
    End Sub

    Private Sub DateTo1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTo1.ValueChanged
        If Me.cmbAllocate.SelectedIndex = 0 Then
            Call Get_Supplier_Debits()
        ElseIf Me.cmbAllocate.SelectedIndex = 1 Then
            'Call GetInvoiceDetailPerSupplier()
        End If
    End Sub

    Sub oAllocate_Supplier_Debits_to_Invoice(ByVal sRowIndex2 As Integer)
        'fix 3
        Try
            'Use the reference from the invoice in DGV2 to update the reference in the Supplier in DGV1
            Dim oRowIndex1 As String = Me.Index1.Text
            Dim oTransactionID1 As String = Me.B0.Text
            Dim oSupplier1 As String = Me.B1.Text
            Dim oReference1 As String = Me.B2.Text
            'oAmount is the amount of the supplier debit so if larger than invoice need to split
            Dim oAmount As String = Me.B3.Text 'Remember this is a negative figure 
            Dim oSupplier_Debit_Amount As Decimal = CDec(oAmount)
            Dim oTransactionDate As String = Me.B4.Text

            Dim sTransactionID2 As String = Me.DataGridView2.Rows(sRowIndex2).Cells(0).Value
            Dim oTransactionID2 As Long = CLng(sTransactionID2)
            Dim sSupplier2 As String = Me.DataGridView2.Rows(sRowIndex2).Cells(1).Value
            Dim sReference2 As String = Me.DataGridView2.Rows(sRowIndex2).Cells(2).Value
            Dim oInvoice_Amount As Decimal = CDec(Me.DataGridView2.Rows(sRowIndex2).Cells(3).Value)
            'NOTE NEXT LINE USED TO GET TRANSACTION DATE OF INVOICE TO CHANGE THE TRANSATION DATE FOR SUPPLIER
            'DEBITS SO THAT TWO LINES ARE NOT SHOWN IN SUPPLIER PAYMENTS FOR THIS COMBINED ITEM !!!
            Dim sDATE2 As String = Me.DataGridView2.Rows(sRowIndex2).Cells(5).Value

            If oTransactionID1 = "0" Then
                MsgBox("Error with transactionID, aborting")
                Exit Sub
            End If
            If oTransactionID1 = "" Then
                MsgBox("Error with transactionID, aborting")
                Exit Sub
            End If

            If sSupplier2 <> oSupplier1 Then
                MsgBox("Suppliers do not match! The allocation cannot be completed!")
                Exit Sub
            End If

            'If Supplier Debit is greater than invoice then create a new transaction with difference
            If oSupplier_Debit_Amount > oInvoice_Amount Then

                '++++++++++++++++++++++++START OF 1 TO MANY:  SUPPLIER DEBIT TO INVOICE ++++++++++++++++++++++++++++

                'First get the VAT and exVAT amounts  for the Supplier debit and invoice
                Dim oSupplier_Debit_VAT_Amount As Decimal = _
                    oGET("Select Amount From Transactions Where TransactionID = " & oTransactionID1 _
                         & " And DocType <> 'Invoice' And [Description 2] = 'VAT CONTROL'" _
                         , "Error getting Amount for VAT line in Supplier Debit allocation larger than invoice HJ876")
                Dim oSupplier_Debit_exVAT_Amount As Decimal = _
              oGET("Select Amount From Transactions Where TransactionID = " & oTransactionID1 _
                   & " And DocType <> 'Invoice' And [Description 2] <> 'VAT CONTROL'" _
                   , "Error getting Amount for ex VAT line in Supplier Debit allocation that larger than invoice HJ877")

                Dim oInvoice_VAT_Amount As Decimal = _
                oGET("Select Amount From Transactions Where TransactionID = " & oTransactionID2 _
                & " And DocType <> 'Invoice' And [Description 2] = 'VAT CONTROL'" _
                 , "Error getting Amount for VAT line in Supplier Debit allocation larger than invoice HJ876")
                Dim oInvoice_exVAT_Amount As Decimal = _
             oGET("Select Amount From Transactions Where TransactionID = " & oTransactionID2 _
                   & " And DocType <> 'Invoice' And [Description 2] <> 'VAT CONTROL'" _
                   , "Error getting Amount for ex VAT line in Supplier Debit allocation that larger than invoice HJ877")

                'NOTE - no need for proportions as 3 lines, just use subtraction to keep in balance
                Dim oNewSupplierDebitAmount As Decimal = oSupplier_Debit_Amount - oInvoice_Amount
                Dim oNewVATAmount As Decimal = oSupplier_Debit_VAT_Amount * (oNewSupplierDebitAmount / oAmount)
                oNewVATAmount = Math.Round(oNewVATAmount, 2)
                Dim oNewExVAT_Amount As Decimal = oNewSupplierDebitAmount - oNewVATAmount
                Dim oNewBatchID As Long = CLng(oCreateBatchID())
                Dim oNewTransactionID As Long = CLng(oCreateTransactionID())
                Dim oNewTransactionID2 As Long = CLng(oCreateTransactionID())

                If oNewTransactionID = 0 Then
                    MsgBox("Error with transactionID, aborting")
                    Exit Sub
                End If
                If oNewTransactionID2 = 0 Then
                    MsgBox("Error with transactionID, aborting")
                    Exit Sub
                End If
                'So new Transaction for remainder of Supplier Debit has been generated.
                'Need to update This to have a new BatchID and Transaction ID
                'and then adjust the amounts of all three lines

                '(1a) First create a new duplicate transaction for Supplier Debits with remaining 
                '    amount after allocation
                Dim oDifference As Decimal = oSupplier_Debit_Amount - oInvoice_Amount
                Dim xSQL As String = "Insert Into Transactions "
                xSQL = "INSERT INTO Transactions ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                xSQL = xSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                xSQL = xSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                xSQL = xSQL & ", Co_ID"
                xSQL = xSQL & " ) "
                xSQL = xSQL & "SELECT " & oNewBatchID & " As Expr1, " & oNewTransactionID & " As Expr2, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], "
                xSQL = xSQL & "GDC, Reference, Description, AccNumber, LinkAcc, " & oDifference & " As Expr3 , TaxType, [Tax Amount], "
                xSQL = xSQL & "UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], [Description 5], "
                xSQL = xSQL & "Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], "
                xSQL = xSQL & "DocType, SupplierName, Info, Info2, Info3, Co_ID FROM Transactions Where TransactionID = " & oTransactionID1
                arrUpdate_SQL(0) = xSQL

                'Now that you have inserted the new Transaction above change all three amounts:

                '(1b) Set the total Amount of the Supplier Debit to the remainder
                arrUpdate_SQL(1) = "Update Transactions Set Amount = " & oNewSupplierDebitAmount & _
                             " Where TransactionID = " & oNewTransactionID & " And DocType = 'Invoice'"
                '(1c) Set the VAT Amount of the new Transaction to new amount
                arrUpdate_SQL(2) = "Update Transactions Set Amount = " & oNewVATAmount & _
                 " Where TransactionID = " & oNewTransactionID _
                  & " And DocType <> 'Invoice' And [Description 2] = 'VAT CONTROL'"

                '(1d) Set the ex VAT Amount of the new Transaction to new amount
                arrUpdate_SQL(3) = "Update Transactions Set Amount = " & oNewExVAT_Amount & _
               " Where TransactionID = " & oNewTransactionID _
                & " And DocType <> 'Invoice' And [Description 2] <> 'VAT CONTROL'"
                '(1d2) Set the ex VAT Amount [VAT Amount] field to have the VAT value
                arrUpdate_SQL(4) = "Update Transactions Set [Tax Amount] = " & oNewVATAmount & _
               " Where TransactionID = " & oNewTransactionID _
                & " And DocType <> 'Invoice' And [Description 2] <> 'VAT CONTROL'"


                'Next create another duplicate transaction like (1a) above but with invoice amounts

                '(2a) Now create duplicate transaction of the invoice and flip the Drs and Crs 
                '     and convert it into a supplier debit


                Dim ySQL As String = "Insert Into Transactions "
                ySQL = "INSERT INTO Transactions ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                ySQL = ySQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                ySQL = ySQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                ySQL = ySQL & ", Co_ID"
                ySQL = ySQL & " ) "
                ySQL = ySQL & "SELECT " & oNewBatchID & " As Expr1, " & oNewTransactionID2 & " As Expr2, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], "
                ySQL = ySQL & "GDC, Reference, Description, AccNumber, LinkAcc, Amount, TaxType, [Tax Amount], "
                ySQL = ySQL & "UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], [Description 5], "
                ySQL = ySQL & "Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], "
                ySQL = ySQL & "DocType, SupplierName, Info, Info2, Info3, Co_ID FROM Transactions Where TransactionID = " & oTransactionID2
                arrUpdate_SQL(5) = ySQL

                'NOTE = IN SWAPPING DRS AND CRS YOU HAVE TI CHANGE ONE TO SOMETHING TEMPORARLILY
                'OTHERWISE FIRST SWAP WILL CHANGE ALL DRS AND CRS TO SAME THING THEN SECOND WILL DO SAME

                '---------------- Dr and Cr swap in three steps ----------------------------------
                '(2b) Temporarily change the Drs to temporary name
                arrUpdate_SQL(6) = "Update Transactions Set DR_CR = 'Debit Temp'" & _
                " Where TransactionID = " & oNewTransactionID2 & " And DR_CR = 'Cr'"

                '(2c) Swap Drs with Crs
                arrUpdate_SQL(7) = "Update Transactions Set DR_CR = 'Cr'" & _
                " Where TransactionID = " & oNewTransactionID2 & " And DR_CR = 'Dr'"

                '(2d) Swap Drs with Crs
                arrUpdate_SQL(8) = "Update Transactions Set DR_CR = 'Dr'" & _
                " Where TransactionID = " & oNewTransactionID2 & " And DR_CR = 'Debit Temp'"
                '---------------- Dr and Cr swap in three steps ----------------------------------


                '(2e) Set the VAT Amount of the new Transaction to new amount
                arrUpdate_SQL(9) = "Update Transactions Set Info = 'Supplier Debit In Invoice'" & _
                 " Where DocType = 'Invoice' And TransactionID = " & oNewTransactionID2

                '(2f) Set the ex VAT Amount of the new Transaction to new amount
                arrUpdate_SQL(10) = "Update Transactions Set Info2 = 'Supplier Debit'" & _
               " Where TransactionID = " & oNewTransactionID2

                'Now void the original supplier debit
                If My.Settings.DBType = "Access" Then
                    arrUpdate_SQL(11) = "Update Transactions Set Void = True " & _
                                     " Where  TransactionID = " & oTransactionID1
                Else
                    arrUpdate_SQL(11) = "Update Transactions Set Void = 1 " & _
                            " Where  TransactionID = " & oTransactionID1
                End If


                'NOW RUN ALL ABOVE UPDATES TOGETHER IN TRANSACT ROLL BACK ...........
                oNumberQueries = 12

                Dim blnRunUpdates As Boolean = UPDATE_APPEND_MULTI()
                If blnRunUpdates = False Then
                    MsgBox("There was a problem splitting a suppier debit allocated to a smaller invoice. Error code X119K")
                    Exit Sub
                End If
                '++++++++++++++++++++++++END OF 1 TO MANY:  SUPPLIER DEBIT TO INVOICE ++++++++++++++++++++++++++++
            Else
                '++++++++++++++++++++++++START OF 1 TO 1:  SUPPLIER DEBIT TO INVOICE ++++++++++++++++++++++++++++

                Dim ra As Integer

                Dim sSQL As String
                Dim sSQLb As String
                Dim sSQL2 As String
                Dim sSQL3 As String
                Dim sSQL4 As String

                sSQL = "Update Transactions Set [Reference] = '" & sReference2 & "' Where TransactionID = " & oTransactionID1 '& " And Supplier = '" & oSupplier1 & "'"

                If My.Settings.DBType = "Access" Then

                    Dim myConnection As New OleDbConnection(My.Settings.CS_Setting)
                    myConnection.Open()
                    Dim myCommand As OleDbCommand
                    myCommand = New OleDbCommand(sSQL, myConnection)
                    ra = myCommand.ExecuteNonQuery()
                    'Since no value is returned we use ExecuteNonQuery
                    'MsgBox("Records Updated " & ra)
                    myConnection.Close()

                ElseIf My.Settings.DBType = "SQL" Then

                    Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    myConnection.Open()
                    Dim myCommand As SqlCommand
                    myCommand = New SqlCommand(sSQL, myConnection)
                    myCommand.CommandTimeout = 300
                    ra = myCommand.ExecuteNonQuery()
                    'Since no value is returned we use ExecuteNonQuery
                    'MsgBox("Records Updated " & ra)
                    myConnection.Close()

                End If
                'First change the reference/invoice number to match the invoice
                sSQL = "Update Transactions Set [Reference] = '" & sReference2 & "' Where TransactionID = " & oTransactionID1 '& " And Supplier = '" & oSupplier1 & "'"
                '============================================================================================================
                '***NOTE - ALSO CHANGE THE TRANSACTION DATE SO THAT DOESN'T SHOW AS 2 LINES IN SUPPLIER PAYMENTS !!!!
                Call Update_TransactionDate_to_match_another_Transaction(oTransactionID2, oTransactionID1)
                sSQLb = "Update Transactions Set [Transaction Date] = '" & sDATE2 & "' Where TransactionID = " & oTransactionID1
                '============================================================================================================
                'Now change Info2 from "Supplier Debit" to "Supplier Debit In Invoice"
                sSQL2 = "Update Transactions Set [Info] = 'Supplier Debit In Invoice' Where TransactionID = " & oTransactionID1 & " And Info = 'Supplier Debit'"
                If Me.txtUpdateOriginalRef.Text = "" Then
                    sSQL3 = "Update Transactions Set [Info2] = 'Supplier Dr| Ref: " & sReference2 & "| Orig Date:" & oTransactionDate & "' Where TransactionID = " & oTransactionID1
                Else
                    sSQL3 = "Update Transactions Set [Info2] = 'Supplier Dr| Ref: " & Me.txtUpdateOriginalRef.Text & "| Orig Date:" & oTransactionDate & "' Where TransactionID = " & oTransactionID1
                End If

                'Finally change the LinkID to the the TransactionID of the invoice
                sSQL4 = "Update Transactions Set [LinkID] = '" & sTransactionID2 & "' Where TransactionID = " & oTransactionID1

                If My.Settings.DBType = "Access" Then

                    Dim myConnection As New OleDbConnection(My.Settings.CS_Setting)
                    myConnection.Open()
                    Dim myCommand As OleDbCommand
                    myCommand = New OleDbCommand(sSQL, myConnection)
                    Dim myCommandb As OleDbCommand
                    myCommandb = New OleDbCommand(sSQLb, myConnection)
                    Dim myCommand2 As OleDbCommand
                    myCommand2 = New OleDbCommand(sSQL2, myConnection)
                    Dim myCommand3 As OleDbCommand
                    myCommand3 = New OleDbCommand(sSQL3, myConnection)
                    Dim myCommand4 As OleDbCommand
                    myCommand4 = New OleDbCommand(sSQL4, myConnection)

                    Try
                        ra = myCommand.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL expression " & sSQL)
                    End Try
                    Try
                        ra = myCommand2.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL2 expression " & sSQL2)
                    End Try

                    Try
                        ra = myCommand3.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL3 expression " & sSQL3)
                    End Try

                    Try
                        ra = myCommand4.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL4 expression " & sSQL4)
                    End Try
                    'Since no value is returned we use ExecuteNonQuery
                    'MsgBox("Records Updated " & ra)
                    myConnection.Close()

                ElseIf My.Settings.DBType = "SQL" Then

                    Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                    myConnection.Open()

                    Dim myCommand As SqlCommand
                    myCommand = New SqlCommand(sSQL, myConnection)
                    myCommand.CommandTimeout = 300

                    Dim myCommandb As SqlCommand
                    myCommandb = New SqlCommand(sSQLb, myConnection)
                    myCommandb.CommandTimeout = 300

                    Dim myCommand2 As SqlCommand
                    myCommand2 = New SqlCommand(sSQL2, myConnection)
                    myCommand2.CommandTimeout = 300

                    Dim myCommand3 As SqlCommand
                    myCommand3 = New SqlCommand(sSQL3, myConnection)
                    myCommand3.CommandTimeout = 300

                    Dim myCommand4 As SqlCommand
                    myCommand4 = New SqlCommand(sSQL4, myConnection)
                    myCommand4.CommandTimeout = 300

                    Try
                        ra = myCommand.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL expression " & sSQL)
                    End Try
                    Try
                        ra = myCommand2.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL2 expression " & sSQL2)
                    End Try

                    Try
                        ra = myCommand3.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL3 expression " & sSQL3)
                    End Try

                    Try
                        ra = myCommand4.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message & " 315_sSQL4 expression " & sSQL4)
                    End Try

                    'Since no value is returned we use ExecuteNonQuery
                    'MsgBox("Records Updated " & ra)
                    myConnection.Close()

                End If
                '++++++++++++++++++++++++END OF 1 TO 1 SUPPLIER DEBIT TO INVOICE ++++++++++++++++++++++++++++
            End If

            Call Get_Supplier_Debits()
            Call GetInvoiceDetailPerSupplier2()

            'MsgBox("Allocation Sccessful!")

            Me.lblDescription.Text = "Supplier Debit reference '" & oReference1 & "' successfully incorporated into invoice number '" & sReference2 & "' for supplier '" & oSupplier1 & "'"

            Me.txtUpdateOriginalRef.Text = ""

        Catch ex As Exception
            MsgBox("Error allocating a supplier debit to an invoice! " & ex.Message & " 315")
        End Try

    End Sub
    Sub Create_Duplicate_Transactions(ByVal oTransactionID As Long, ByVal oAmount As Long)

        Try
            Dim sSQL As String = "Insert Into Transactions "

            sSQL = sSQL & "SELECT BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], "
            sSQL = sSQL & "GDC, Reference, Description, AccNumber, LinkAcc, " & oAmount & " As Expr1 , TaxType, [Tax Amount], "
            sSQL = sSQL & "UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], [Description 5], "
            sSQL = sSQL & "Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], "
            sSQL = sSQL & "DocType, SupplierName, Co_ID, Info, Info2, Info3 FROM Transactions Where TransactionID = " & oTransactionID

            If My.Settings.DBType = "Access" Then
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 0cc28")
        End Try
    End Sub

    Sub Update_TransactionDate_to_match_another_Transaction(ByVal oSourceID As Long, ByVal oDestinationID As Long)

        Try
            Dim sSQL As String
            If My.Settings.DBType = "Access" Then
                sSQL = "UPDATE Transactions SET [Transaction Date] = "
                sSQL = sSQL & "(Select Distinct [Transaction Date] From Transactions Where TransactionID = " & oSourceID & ")"
                sSQL = sSQL & " From Transactions Where TransactionID = " & oDestinationID
            ElseIf My.Settings.DBType = "SQL" Then
                sSQL = "UPDATE Transactions SET [Transaction Date] = "
                sSQL = sSQL & "(Select Distinct [Transaction Date] From Transactions Where TransactionID = " & oSourceID & ")"
                sSQL = sSQL & " From Transactions Where TransactionID = " & oDestinationID
            End If
            sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID


            If My.Settings.DBType = "SQL" Then
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 028")
        End Try
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Update_TransactionDate_to_match_another_Transaction(57041, 57043)
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick

    End Sub
    
    Private Sub GroupBox3_Enter(sender As Object, e As EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class