﻿Imports System.IO
Imports System.Net

Public Class MBAPI
    Public Function VoidTransaction(TransactionID As Integer) As Boolean
        Try
            ' Get HTML data
            Dim url As String = ""
            url = "http://www.restaurantmagicbox.com/platform/api/MBTransactions/" & My.Settings.TenantStoreID & "/" & TransactionID
            Dim postReq As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)

            Dim Auth As String = My.Settings.Username & ":" & My.Settings.Password
            postReq.Headers.Add("Authorization", Auth)
            postReq.Method = "Delete"

            Dim WebRespo As HttpWebResponse
            WebRespo = postReq.GetResponse()
            If WebRespo.StatusCode = HttpStatusCode.OK Then
                MsgBox("Void Completed")
                Return True
            End If
            Return False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error Communicating with WebServer")
            Return False
        End Try
    End Function
End Class
