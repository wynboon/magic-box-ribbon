﻿

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS
Imports System.Drawing.Drawing2D

Public Class frmEFT_Approvals
#Region "Variables"
    Dim oBatchID As Integer
    Dim oTransactionID As Long
    Dim oLinkID As Integer
    Dim dTransactionDate As Date
    Dim dCaptureDate As Date
    Dim sPPeriod As String
    Dim sFinYear As String
    Dim sGDC As String
    Dim sInvoiceNumber As String
    Dim sDescription As String
    Dim sAccountNumber As String
    Dim sContraAccount As String
    Dim sLinkAcc As String
    Dim oAmount As Decimal
    Dim oTaxType As Integer
    Dim oTaxAmount As Decimal
    Dim sUserID As String
    Dim oSupplierID As Integer
    Dim oEmployeeID As Integer
    Dim sDescription2 As String
    Dim sDescription3 As String
    Dim sDescription4 As String
    Dim sDescription5 As String
    Dim blnPosted As Boolean
    Dim blnAccounting As Boolean
    Dim blnVoid As Boolean
    Dim TransactionType As Integer
    Dim oDocType As String
    Dim oSupplierName As String
    Dim sDRCR As String
    Dim sInfo As String
    Dim sInfo2 As String
    Dim sInfo3 As String

    Dim BatchID_Check As Integer
    Public oNumberPayments As Integer
    Public arrID(10000) As Integer '0
    Public arrSupplierName(10000) As String '1
    Public arrReference(10000) As String '2
    Public arrDateOfPayment(10000) As String '3
    Public arrPaymentAmount(10000) As Decimal '4
    Public arrPaymentType(10000) As String ' 5

    Dim sTransactionDate As String

    Dim xAccessAdapter_Accounts As OleDbDataAdapter
    Dim xAccessTable_Accounts As New DataTable
    Dim xSQLAdapter_Accounts As SqlDataAdapter
    Dim xSQLTable_Accounts As New DataTable

    Dim xAccessAdapter_Suppliers As OleDbDataAdapter
    Dim xAccessTable_Suppliers As New DataTable
    Dim xSQLAdapter_Suppliers As SqlDataAdapter
    Dim xSQLTable_Suppliers As New DataTable

    Dim xAccessAdapter_Periods As OleDbDataAdapter
    Dim xAccessTable_Periods As New DataTable
    Dim xSQLAdapter_Periods As SqlDataAdapter
    Dim xSQLTable_Periods As New DataTable

    Dim oProgressBar As ProgressBar

    Dim blnAt_Least_One_Payment_Request_Paid As Boolean

    Private Property blnFormLoading As Boolean
    Private blnFirstRun As Boolean
    Private blnForm_Hiding As Boolean
    Dim MBData As New DataExtractsClass
#End Region
    Dim blnSuccessOrFailure
    Private Sub frmEFT_Approvals_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Enabled = False
            Cursor = Cursors.AppStarting
            Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> EFT Approvals -> Version : " & My.Settings.Setting_Version
            LockForm(Me)
            FillComboBoxSuppliers()
            Enabled = True
            Cursor = Cursors.Arrow
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Sub FillComboBoxSuppliers()
        Try
            If My.Settings.Setting_CompanyID <> "" Then
                cmbSupplier.DataSource = MBData.SupplierList(CInt(My.Settings.Setting_CompanyID))
                cmbSupplier.DisplayMember = "SupplierName"
                cmbSupplier.ValueMember = "SupplierID"
            Else
                MessageBox.Show("No Company is set to default please double check and rectify", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub
    Sub BuildSQL()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting
            Dim sSQL As String = ""

            If ckbAllSuppliers.Checked = True Then
                sSQL = "Select ID, TransactionID, LinkID, SupplierName, Amount,Reference, [Transaction Date],DocType,Info from Transactions where Info like '%Invoice Payment Requested%' or INFO like '%EFT%' or INFO like '%Payout%' And Accounting=0"
            Else
                sSQL = "Select ID, TransactionID, LinkID, SupplierName, Amount,Reference, [Transaction Date],DocType,Info from Transactions where Info like '%Invoice Payment Requested%' or INFO like '%EFT%' or INFO like '%Payout%' And Accounting=0 AND SupplierName = '" & cmbSupplier.Text & "'"
            End If
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Call Load_DGV(sSQL)

            Dim ckbApprove As New DataGridViewCheckBoxColumn
            With ckbApprove
                .Name = "ckbApprove"
                .HeaderText = "Approve"
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            End With
            grdEFTApprovals.Columns.Insert(0, ckbApprove)
            grdEFTApprovals.Columns("ID").Visible = False
            grdEFTApprovals.Columns("LinkID").Visible = False

            Me.grdEFTApprovals.Columns("TransactionID").ReadOnly = True
            Me.grdEFTApprovals.Columns("SupplierName").ReadOnly = True
            Me.grdEFTApprovals.Columns("Amount").ReadOnly = True
            Me.grdEFTApprovals.Columns("Reference").ReadOnly = True
            Me.grdEFTApprovals.Columns("Transaction Date").ReadOnly = True
            Me.grdEFTApprovals.Columns("DocType").ReadOnly = True
            Me.grdEFTApprovals.Columns("Info").ReadOnly = True

            Call Calc_Totals()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Load_DGV(ByVal sSQL As String)
            'First clear DGV
            grdEFTApprovals.DataSource = Nothing
            grdEFTApprovals.Columns.Clear() 'in case of extra columns

            If My.Settings.DBType = "Access" Then

                Dim connection As New OleDbConnection(My.Settings.CS_Setting)
                Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
                Dim ds As New DataSet()
                connection.Open()
                dataadapter.Fill(ds, "Data_table")
                connection.Close()
                grdEFTApprovals.DataSource = ds
                grdEFTApprovals.DataMember = "Data_table"

            ElseIf My.Settings.DBType = "SQL" Then

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Data_table")
            connection.Close()
            grdEFTApprovals.DataSource = ds
            grdEFTApprovals.DataMember = "Data_table"

        End If

        Me.grdEFTApprovals.Columns("Amount").DefaultCellStyle.Format = "c" 'note extra credit box hasn't been added yet

    End Sub

    Private Sub DateTimePicker_From_ValueChanged(sender As System.Object, e As System.EventArgs)
        Call BuildSQL()
    End Sub

    Private Sub DateTimePicker_To_ValueChanged(sender As System.Object, e As System.EventArgs) Handles DateTimePicker_To.ValueChanged
        Call BuildSQL()
    End Sub
    Private Sub btnApprove_Click(sender As System.Object, e As System.EventArgs) Handles btnApproveSelected.Click
        oApprove()
    End Sub
    Sub oApprove()
        Try

            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            For Each row In grdEFTApprovals.Rows
                If row.Cells(0).Value = True Then

                    'Pay(CInt(row.Cells("ID").Value), CStr(row.Cells("SupplierName").Value), CStr(row.Cells("Amount").Value), CDate(row.Cells("Transaction Date").Value))
                    Nullify_RequestPayment(CInt(row.Cells("TransactionID").Value))
                    Set_Accounting_to_True(CInt(row.Cells("TransactionID").Value), CInt(row.Cells("LinkID").Value))

                    'If oCheck_Integrity() = False Then
                    'MsgBox("The numbers are zero or out of balance and cannot be saved!")
                    'blnCRITICAL_ERROR = True
                    'Else
                    '    If My.Settings.DBType = "Access" Then
                    'Call AppendTransactions()
                    'ElseIf My.Settings.DBType = "SQL" Then
                    '   Call AppendTransactions_SQL()
                    'End If
                    'End If

                End If
            Next
            MsgBox("Approval succeeded!")
            Call BuildSQL()
            Call Calc_Totals()

            Me.Enabled = True
            Me.Cursor = Cursors.Arrow

        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub Set_Accounting_to_True(ByVal oTransactionID As String, ByVal oLinkID As String)

        Try
            If My.Settings.DBType = "Access" Then

                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = -1 WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If

                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                '// define the sql statement to execute
                Dim cmd As New OleDbCommand(sSQL, cn)
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
            ElseIf My.Settings.DBType = "SQL" Then

                Dim sSQL As String
                sSQL = "Update Transactions Set Accounting = 1 WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                '// define the sql statement to execute
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                '    '// open the connection
                cn.Open()
                cmd.ExecuteNonQuery()
                cn.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " 097")
        End Try

    End Sub
    Private Sub DataGridView1_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdEFTApprovals.CellEndEdit


        Dim oRowIndex As Integer = e.RowIndex
        Dim oColumnIndex As Integer = e.ColumnIndex
        Dim strCellValue As String = Me.grdEFTApprovals.Rows(oRowIndex).Cells(oColumnIndex).Value
        Dim oAmount As Decimal
        Dim oTransactionID As String
        Dim oLinkID As String 'this is the link to the invoice

        If oColumnIndex = 3 Then

            oAmount = CDec(strCellValue)
            oTransactionID = Me.grdEFTApprovals.Rows(oRowIndex).Cells(oColumnIndex + 1).Value 'col index = 4
            oLinkID = Me.grdEFTApprovals.Rows(oRowIndex).Cells(oColumnIndex + 2).Value 'col index = 5

            Dim oResponse As MsgBoxResult
            oResponse = MsgBox("Would you like to change the EFT payment to " & strCellValue & "?", MsgBoxStyle.YesNo)
            If oResponse = MsgBoxResult.Yes Then
                Call Change_Amount(oAmount, oLinkID, oTransactionID)
            End If
            Call Calc_Totals()
        Else
            Exit Sub
        End If

    End Sub

    Sub Change_Amount(ByVal oAmount As Decimal, ByVal oLinkID As String, ByVal oTransactionID As String)
        Try
            Dim sSQL As String

            If My.Settings.DBType = "Access" Then

                sSQL = "Update Transactions Set Amount = " & oAmount & " WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, cn)
                cn.Open()
                cmd.ExecuteNonQuery()

            ElseIf My.Settings.DBType = "SQL" Then

                sSQL = "Update Transactions Set Amount = " & oAmount & " WHERE TransactionID = " & oTransactionID & " And LinkID = " & oLinkID & " And Accounting = 0"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                    sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                End If
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()

            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 098")
        End Try
    End Sub
    Private Sub DataGridView1_CurrentCellDirtyStateChanged(sender As Object, e As System.EventArgs) Handles grdEFTApprovals.CurrentCellDirtyStateChanged
        Try
            If grdEFTApprovals.IsCurrentCellDirty Then
                grdEFTApprovals.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub Calc_Totals()
        Try
            Dim oTotal As Decimal = 0
            For Each row As DataGridViewRow In grdEFTApprovals.Rows
                oTotal = oTotal + CDec(row.Cells("Amount").Value)
            Next
            txtBalance.Text = oTotal.ToString("c")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BuildSQL()
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub ckbAllSuppliers_CheckedChanged(sender As Object, e As EventArgs) Handles ckbAllSuppliers.CheckedChanged
        If ckbAllSuppliers.Checked = True Then
            cmbSupplier.Enabled = False
        Else
            cmbSupplier.Enabled = True
        End If
    End Sub

    Private Sub ckbSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles ckbSelectAll.CheckedChanged
        If ckbSelectAll.Checked = True Then
            For Each r As DataGridViewRow In grdEFTApprovals.Rows
                r.Cells("ckbApprove").Value = CBool(True)
            Next
        Else
            For Each r As DataGridViewRow In grdEFTApprovals.Rows
                r.Cells("ckbApprove").Value = CBool(False)
            Next
        End If
    End Sub
    Private Sub btnApproveAll_Click(sender As Object, e As EventArgs) Handles btnApproveAll.Click
        ckbSelectAll.Checked = True
        oApprove()
    End Sub
    Public Sub Nullify_RequestPayment(ByVal oTransID As String)

        Try

            If My.Settings.DBType = "SQL" Then
                Dim sSQL As String
                sSQL = "Update Transactions Set Info = 'Invoice' WHERE SubString(Info,1,25) = 'Invoice Payment Requested' And TransactionID = " & oTransID
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
                Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cn.Open()
                cmd.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MsgBox(ex.Message & " 326")
        End Try

    End Sub

    Public Sub Pay(ByVal T_ID As Integer, ByVal SupplierName As String, ByVal Amount As Decimal, ByVal TranDate As Date)

        Try

            Dim Info As String
            Dim Info2 As String
            Dim Info3 As String

            oBatch_or_Transaction_generation_Failed = False

            oBatchID = CLng(oCreateBatchID())

            Dim oPaymentType As String

            Dim sCaptureDate As String = Now.Date.ToString("dd MMMM yyyy")
            Dim oTransID As Integer
            Dim sDescriptionCode As String
            Dim oDocType As String
            Dim oNewTransactionID As Integer


            Info = ""
            Info2 = ""
            Info3 = ""
            'MDB

            oPaymentType = "EFT"
            oLinkID = T_ID
            Call Load_Invoice_Data_From_ID_to_Variables(oLinkID)

            oNewTransactionID = CLng(oCreateTransactionID())

            If CStr(arrDateOfPayment(0)) <> "" Then
                dTransactionDate = CDate(arrDateOfPayment(0)) 'if there is a date here then use otherwise use general one on form
            Else
                dTransactionDate = Now.Date
            End If

            dCaptureDate = Now.Date
            sPPeriod = GetPeriod(dTransactionDate.ToString("dd MMMM yyyy"))
            sGDC = "G" 'General/Debtors/Creditors

            sDescription = SupplierName
            oAmount = Amount

            oTaxType = 0
            oTaxAmount = 0

            blnAccounting = True

            Info = oPaymentType
            'blnVoid - same as original invoice
            TransactionType = 1
            sDRCR = "Dr"

            Dim sCreditorsControl_Segment4 As String
            sCreditorsControl_Segment4 = Get_Segment4_Using_Type_in_Accounting("Creditors Control")
            sDescriptionCode = sCreditorsControl_Segment4
            If sDescriptionCode = "" Or IsDBNull(sDescriptionCode) Then
                MsgBox("No Magic Box Segment 4 code for 'Creditors Control'!")
                Exit Sub
            End If

            oDocType = "Payment"
            Info2 = "Payment" 'This is so all line items can be manipulated at once

            sFinYear = GetFinYear(TranDate)

            Call Add_Transaction_DGV(oBatchID, oNewTransactionID, oLinkID, TranDate, sCaptureDate, sPPeriod, sFinYear, sGDC, cInvoiceNumber, _
sDescription, sAccountNumber, cBatchID, oAmount, oTaxType, oTaxAmount, sUserID, _
cSupplierID, oEmployeeID, cDescription2, cDescription3, cDescription4, cDescription5, blnPosted, _
blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, oDocType, cSupplierName, Info, Info2, Info3)


            'Now just change these few things
            'oTransactionID = oTransactionID + 1 'just unique - insert a new transaction number to settings table at end of this procedure

            sDescription = oPaymentType
            TransactionType = 1
            oTaxType = 0
            oTaxAmount = 0
            sDRCR = "Cr"
            'Dim sEFT_ControlAccount As String
            'sEFT_ControlAccount = GetSetting("EFT CONTROL")
            'sDescriptionCode = ""
            'Get_DisplayName_In_Accounting()

            sDescription = Get_Segm1Desc_From_DisplayName_in_Accounting(oPaymentType)
            sDescription2 = Get_Segm2Desc_From_DisplayName_in_Accounting(oPaymentType)
            sDescription3 = Get_Segm3Desc_From_DisplayName_in_Accounting(oPaymentType)
            sDescriptionCode = Get_Segment4_from_DisplayName_in_Accounting(oPaymentType)
            sAccountNumber = Get_GLACCOUNT_from_DisplayName_in_Accounting(oPaymentType)

            'set oDocType to nothing
            oDocType = ""
            Info = ""
            Info2 = "Payment" 'This is so all line items can be manipulated at once


            Call Add_Transaction_DGV(oBatchID, oNewTransactionID, oLinkID, sTransactionDate, sCaptureDate, sPPeriod, sFinYear, sGDC, cInvoiceNumber, _
sDescription, sAccountNumber, cBatchID, oAmount, oTaxType, oTaxAmount, sUserID, _
cSupplierID, oEmployeeID, sDescription2, sDescription3, sDescription4, sDescription5, blnPosted, _
blnAccounting, blnVoid, TransactionType, sDRCR, sContraAccount, sDescriptionCode, oDocType, cSupplierName, Info, Info2, Info3)




            If My.Settings.EmailAll = "Yes" Or My.Settings.DefaultEmailAddress = "Yes" Then
                Call oEmail_Payment_Info()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub Add_Transaction_DGV(ByVal oBatchID As Integer, ByVal oTransactionID As Long, ByVal oLinkID As Integer, ByVal sTransactionDate As String, ByVal sCaptureDate As String, _
                         ByVal sPPeriod As String, ByVal sFinYear As String, ByVal sGDC As String, ByVal sReference As String, ByVal sDescription As String, ByVal sAccountNumber As String, _
                         ByVal sLinkAcc As String, ByVal oAmount As String, ByVal oTaxType As Integer, ByVal oTaxAmount As Decimal, ByVal sUserID As String, _
                         ByVal oSupplierID As Integer, ByVal oEmployeeID As String, ByVal sDescription2 As String, ByVal sDescription3 As String, _
                         ByVal sDescription4 As String, ByVal sDescription5 As String, ByVal blnPosted As Boolean, ByVal blnAccounting As Boolean, _
                         ByVal blnVoid As Boolean, ByVal sTransactionType As String, ByVal oDR_CR As String, ByVal oContraAccount As String, ByVal oDescriptionCode As String, _
                         ByVal oDocType As String, ByVal oSupplierName As String, ByVal oInfo As String, ByVal oInfo2 As String, ByVal oInfo3 As String)

        Try

            Dim newRow As New DataGridViewRow()

            newRow.CreateCells(Me.DGV_Transactions)

            newRow.Cells(0).Value = CStr(oBatchID)
            newRow.Cells(1).Value = CStr(oTransactionID)
            newRow.Cells(2).Value = CStr(oLinkID)
            'newRow.Cells(3).Value = CStr(Format(dTransactionDate, "yyy-MMM-dd"))
            'newRow.Cells(4).Value = CStr(Format(dCaptureDate, "yyy-MMM-dd"))
            'newRow.Cells(3).Value = CStr(dTransactionDate.Year) & "-" & CStr(dTransactionDate.Month) & "-" & CStr(dTransactionDate.Day)
            'newRow.Cells(4).Value = CStr(dCaptureDate.Year) & "-" & CStr(dCaptureDate.Month) & "-" & CStr(dCaptureDate.Day)
            newRow.Cells(3).Value = sTransactionDate
            newRow.Cells(4).Value = sCaptureDate
            newRow.Cells(5).Value = sPPeriod
            newRow.Cells(6).Value = sFinYear
            newRow.Cells(7).Value = sGDC
            newRow.Cells(8).Value = sReference
            newRow.Cells(9).Value = sDescription
            newRow.Cells(10).Value = sAccountNumber
            newRow.Cells(11).Value = sLinkAcc
            newRow.Cells(12).Value = oAmount
            newRow.Cells(13).Value = CStr(oTaxType)
            newRow.Cells(14).Value = CStr(oTaxAmount)
            newRow.Cells(15).Value = sUserID
            newRow.Cells(16).Value = CStr(oSupplierID)
            newRow.Cells(17).Value = CStr(oEmployeeID)
            newRow.Cells(18).Value = sDescription2
            newRow.Cells(19).Value = sDescription3
            newRow.Cells(20).Value = sDescription4
            newRow.Cells(21).Value = sDescription5
            If My.Settings.DBType = "Access" Then
                newRow.Cells(22).Value = CStr(blnPosted)
                newRow.Cells(23).Value = CStr(blnAccounting)
                newRow.Cells(24).Value = CStr(blnVoid)
            ElseIf My.Settings.DBType = "SQL" Then
                If blnPosted = True Then
                    newRow.Cells(22).Value = "1"
                Else
                    newRow.Cells(22).Value = "0"
                End If
                If blnAccounting = True Then
                    newRow.Cells(23).Value = "1"
                Else
                    newRow.Cells(23).Value = "0"
                End If
                If blnVoid = True Then
                    newRow.Cells(24).Value = "1"
                Else
                    newRow.Cells(24).Value = "0"
                End If
            End If
            'newRow.Cells(22).Value = CStr(blnPosted)
            'newRow.Cells(23).Value = CStr(blnAccounting)
            'newRow.Cells(24).Value = CStr(blnVoid)
            newRow.Cells(25).Value = CStr(sTransactionType)
            newRow.Cells(26).Value = oDR_CR
            newRow.Cells(27).Value = oContraAccount
            newRow.Cells(28).Value = oDescriptionCode
            newRow.Cells(29).Value = oDocType
            newRow.Cells(30).Value = oSupplierName
            newRow.Cells(31).Value = oInfo
            newRow.Cells(32).Value = oInfo2
            newRow.Cells(33).Value = oInfo3

            Me.DGV_Transactions.Rows.Add(newRow) '

        Catch ex As Exception
            MsgBox(ex.Message & " 058")
        End Try


    End Sub

    Function Get_Segment4_from_DisplayName_in_Accounting(ByVal sString As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Segment 4] FROM Accounting WHERE [Display Name] = '" & SQLConvert(sString) & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " AND Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then
                ' If Globals.Ribbons.Ribbon1.DropDown1.SelectedItemIndex = 0 Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Segment4_from_DisplayName_in_Accounting = cmd.ExecuteScalar().ToString
                connection.Close()
                'End If
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the Magic Box Code fro the Display name in Accounting!")
        End Try
    End Function
    Sub oEmail_Payment_Info()
        Try
            Dim oNextSupplier As String
            Dim oSupplierName As String
            Dim oTo As String
            Dim oCc As String
            Dim oSubject As String
            Dim oBody As String
            Dim oAttachment As String
            oBody = "This is a notification email to confirm that the following payments have been made to your organisation:" & "    " & vbCrLf

            For i As Integer = 0 To oNumberPayments - 1

                'oNumberPayments = oNumberPayments + 1

                oSupplierName = arrSupplierName(i)

                oBody = oBody & vbCrLf & "Invoice Number: " & arrReference(i) & ", Amount: " & arrPaymentAmount(i) & ", Payment Type: " & arrPaymentType(i) & "    " & vbCrLf

                If i > 0 And i < oNumberPayments - 1 Then
                    oNextSupplier = arrSupplierName(i + 1)
                    If oSupplierName = oNextSupplier Then
                        'Do nothing - keep building oBody
                    Else
                        oBody = oBody & vbCrLf & "Thank you"
                        oSubject = "Invoices Paid for: " & oSupplierName
                        If My.Settings.EmailAll = "Yes" Then
                            oTo = Get_Supplier_Email(oSupplierName)
                            oCc = CStr(My.Settings.DefaultEmailAddress)
                        End If

                        If My.Settings.EmailDefault = "Yes" Then
                            oTo = Get_Supplier_Email(oSupplierName)
                        End If
                        oSendEmail(oTo, oCc, oSubject, oBody, "")
                        'Set body back to original
                        oBody = "This is a notification email to confirm that the following payments have been made to your organisation:" & "    " & vbCrLf
                    End If
                ElseIf i = oNumberPayments - 1 Then 'Last one in list
                    oBody = oBody & vbCrLf & "Thank you"
                    oSubject = "Invoices Paid for: " & oSupplierName
                    If My.Settings.EmailAll = "Yes" Then
                        oTo = Get_Supplier_Email(oSupplierName)
                        oCc = CStr(My.Settings.DefaultEmailAddress)
                    End If

                    If My.Settings.EmailDefault = "Yes" Then
                        oTo = Get_Supplier_Email(oSupplierName)
                    End If

                    oSendEmail(oTo, oCc, oSubject, oBody, "")
                End If

            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function Get_Supplier_Email(ByVal sSupplier As String) As String
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Email_Address] FROM Suppliers WHERE [SupplierName] = '" & SQLConvert(sSupplier) & "'"

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                Get_Supplier_Email = cmd.ExecuteScalar().ToString
                connection.Close()
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up a Supplier column email address! " & ex.Message & "kl90s")
        End Try
    End Function
    Function oCheck_Integrity() As Boolean
        '12 = Amount
        '26 = DR or Cr
        Try


            Dim oDebits As Decimal
            Dim oCredits As Decimal
            Dim i As Integer

            oDebits = 0
            oCredits = 0

            For i = 0 To Me.DGV_Transactions.RowCount - 2 'extra row
                If Me.DGV_Transactions.Rows(i).Cells(26).Value = "Dr" Then
                    oDebits = oDebits + CDec(Me.DGV_Transactions.Rows(i).Cells(12).Value)
                ElseIf Me.DGV_Transactions.Rows(i).Cells(26).Value = "Cr" Then
                    oCredits = oCredits + CDec(Me.DGV_Transactions.Rows(i).Cells(12).Value)
                End If
            Next



            If oDebits = oCredits Then
                If oDebits = 0 Or oCredits = 0 Then
                    oCheck_Integrity = False
                Else
                    oCheck_Integrity = True
                End If
            Else
                oCheck_Integrity = False
            End If


        Catch ex As Exception
            MsgBox("There was an error checking integrity " & ex.Message & " 061")
        End Try
    End Function
    Sub AppendTransactions()

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As Boolean
        Dim blnAccounting As Boolean
        Dim blnVoid As Boolean
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String


        Dim i As Integer
        Dim cmd As OleDbCommand

        Dim cn As New OleDbConnection(My.Settings.CS_Setting)
        Dim trans As OleDb.OleDbTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To Me.DGV_Transactions.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = Me.DGV_Transactions.Rows(i).Cells(0).Value
                oTransactionID = Me.DGV_Transactions.Rows(i).Cells(1).Value
                oLinkID = Me.DGV_Transactions.Rows(i).Cells(2).Value

                strTransactionDate = Me.DGV_Transactions.Rows(i).Cells(3).Value
                strCaptureDate = Me.DGV_Transactions.Rows(i).Cells(4).Value

                sPPeriod = Me.DGV_Transactions.Rows(i).Cells(5).Value
                sFinYear = Me.DGV_Transactions.Rows(i).Cells(6).Value
                sGDC = Me.DGV_Transactions.Rows(i).Cells(7).Value
                sReference = Me.DGV_Transactions.Rows(i).Cells(8).Value
                sDescription = Me.DGV_Transactions.Rows(i).Cells(9).Value
                sAccountNumber = Me.DGV_Transactions.Rows(i).Cells(10).Value
                sLinkAcc = Me.DGV_Transactions.Rows(i).Cells(11).Value
                oAmount = Me.DGV_Transactions.Rows(i).Cells(12).Value
                oTaxType = Me.DGV_Transactions.Rows(i).Cells(13).Value
                oTaxAmount = Me.DGV_Transactions.Rows(i).Cells(14).Value
                sUserID = Me.DGV_Transactions.Rows(i).Cells(15).Value
                oSupplierID = Me.DGV_Transactions.Rows(i).Cells(16).Value
                oEmployeeID = Me.DGV_Transactions.Rows(i).Cells(17).Value
                sDescription2 = Me.DGV_Transactions.Rows(i).Cells(18).Value
                sDescription3 = Me.DGV_Transactions.Rows(i).Cells(19).Value
                sDescription4 = Me.DGV_Transactions.Rows(i).Cells(20).Value
                sDescription5 = Me.DGV_Transactions.Rows(i).Cells(21).Value
                blnPosted = Me.DGV_Transactions.Rows(i).Cells(22).Value
                blnAccounting = Me.DGV_Transactions.Rows(i).Cells(23).Value
                blnVoid = Me.DGV_Transactions.Rows(i).Cells(24).Value
                sTransactionType = Me.DGV_Transactions.Rows(i).Cells(25).Value
                oDR_CR = Me.DGV_Transactions.Rows(i).Cells(26).Value
                oContraAccount = Me.DGV_Transactions.Rows(i).Cells(27).Value
                oDescriptionCode = Me.DGV_Transactions.Rows(i).Cells(28).Value
                oDocType = Me.DGV_Transactions.Rows(i).Cells(29).Value
                oSupplierName = Me.DGV_Transactions.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO Transactions ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "#" & strCaptureDate & "# As MyDate1, #" & strTransactionDate & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"
                End If


                '// define the sql statement to execute
                'Dim cmd As New OleDbCommand(sSQL, cn)
                cmd = New OleDbCommand(sSQL, cn)
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next



            trans.Commit()  '+++++++ Transaction and rollback ++++++++

            cmd = Nothing



            'System.Threading.Thread.Sleep(1200) 'A little append to make sure it hits the database before any further code is written

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++


            MsgBox(ex.Message & " 059")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True


        Finally

            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub AppendTransactions_SQL()

        Dim oBatchID As Integer
        Dim oTransactionID As Long
        Dim oLinkID As Integer
        Dim strTransactionDate As String
        Dim strCaptureDate As String
        Dim sPPeriod As String
        Dim sFinYear As String
        Dim sGDC As String
        Dim sReference As String
        Dim sDescription As String
        Dim sAccountNumber As String
        Dim sLinkAcc As String
        Dim oAmount As String
        Dim oTaxType As Integer
        Dim oTaxAmount As Decimal
        Dim sUserID As String
        Dim oSupplierID As Integer
        Dim oEmployeeID As String
        Dim sDescription2 As String
        Dim sDescription3 As String
        Dim sDescription4 As String
        Dim sDescription5 As String
        Dim blnPosted As String 'Must be string - If Boolean then will turns a -1 into True
        Dim blnAccounting As String
        Dim blnVoid As String
        Dim sTransactionType As String
        Dim oDR_CR As String
        Dim oContraAccount As String
        Dim oDescriptionCode As String
        Dim oDocType As String
        Dim oSupplierName As String
        Dim oCompanyID As String = My.Settings.Setting_CompanyID.ToString  '##### NEW #####
        Dim oInfo As String
        Dim oInfo2 As String
        Dim oInfo3 As String



        Dim i As Integer
        Dim cmd As SqlCommand


        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
        Dim trans As SqlTransaction '+++++++ Transaction and rollback ++++++++

        Try
            '    '// open the connection
            cn.Open()

            ' Make the transaction.
            trans = cn.BeginTransaction(IsolationLevel.ReadCommitted)   '+++++++ Transaction and rollback ++++++++

            For i = 0 To Me.DGV_Transactions.RowCount - 2 'Remember that it there is an extra ghost row in the DataGridView

                blnAppend_Failed = False
                Dim sSQL As String

                oBatchID = Me.DGV_Transactions.Rows(i).Cells(0).Value
                oTransactionID = Me.DGV_Transactions.Rows(i).Cells(1).Value
                oLinkID = Me.DGV_Transactions.Rows(i).Cells(2).Value


                strTransactionDate = Me.DGV_Transactions.Rows(i).Cells(3).Value
                strCaptureDate = Me.DGV_Transactions.Rows(i).Cells(4).Value

                sPPeriod = Me.DGV_Transactions.Rows(i).Cells(5).Value
                sFinYear = Me.DGV_Transactions.Rows(i).Cells(6).Value
                sGDC = Me.DGV_Transactions.Rows(i).Cells(7).Value
                sReference = Me.DGV_Transactions.Rows(i).Cells(8).Value
                sDescription = Me.DGV_Transactions.Rows(i).Cells(9).Value
                sAccountNumber = Me.DGV_Transactions.Rows(i).Cells(10).Value
                sLinkAcc = Me.DGV_Transactions.Rows(i).Cells(11).Value
                oAmount = Me.DGV_Transactions.Rows(i).Cells(12).Value
                oTaxType = Me.DGV_Transactions.Rows(i).Cells(13).Value
                oTaxAmount = Me.DGV_Transactions.Rows(i).Cells(14).Value
                sUserID = Me.DGV_Transactions.Rows(i).Cells(15).Value
                oSupplierID = Me.DGV_Transactions.Rows(i).Cells(16).Value
                oEmployeeID = Me.DGV_Transactions.Rows(i).Cells(17).Value
                sDescription2 = Me.DGV_Transactions.Rows(i).Cells(18).Value
                sDescription3 = Me.DGV_Transactions.Rows(i).Cells(19).Value
                sDescription4 = Me.DGV_Transactions.Rows(i).Cells(20).Value
                sDescription5 = Me.DGV_Transactions.Rows(i).Cells(21).Value
                blnPosted = Me.DGV_Transactions.Rows(i).Cells(22).Value
                blnAccounting = Me.DGV_Transactions.Rows(i).Cells(23).Value
                blnVoid = Me.DGV_Transactions.Rows(i).Cells(24).Value
                sTransactionType = Me.DGV_Transactions.Rows(i).Cells(25).Value
                oDR_CR = Me.DGV_Transactions.Rows(i).Cells(26).Value
                oContraAccount = Me.DGV_Transactions.Rows(i).Cells(27).Value
                oDescriptionCode = Me.DGV_Transactions.Rows(i).Cells(28).Value
                oDocType = Me.DGV_Transactions.Rows(i).Cells(29).Value
                oSupplierName = Me.DGV_Transactions.Rows(i).Cells(30).Value
                oInfo = Me.DGV_Transactions.Rows(i).Cells(31).Value
                oInfo2 = Me.DGV_Transactions.Rows(i).Cells(32).Value
                oInfo3 = Me.DGV_Transactions.Rows(i).Cells(33).Value

                sSQL = "INSERT INTO Transactions ( BatchID, TransactionID, LinkID, [Capture Date], [Transaction Date], PPeriod, [Fin Year], GDC, Reference, Description, AccNumber, "
                sSQL = sSQL & "LinkAcc, Amount, TaxType, [Tax Amount], UserID, SupplierID, EmployeeID, [Description 2], [Description 3], [Description 4], "
                sSQL = sSQL & "[Description 5], Posted, Accounting, Void, [Transaction Type], DR_CR, [Contra Account], [Description Code], [DocType], [SupplierName], Info, Info2, Info3"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", Co_ID"
                End If
                sSQL = sSQL & " ) "
                sSQL = sSQL & "SELECT " & CStr(oBatchID) & " as Expr1, " & CStr(oTransactionID) & " as Expr2, " & CStr(oLinkID) & " as Expr3, "
                sSQL = sSQL & "'" & strCaptureDate & "' As MyDate1, '" & strTransactionDate & "' As MyDate2, "
                'sSQL = sSQL & "#" & CYear & "-" & CMonth & "-" & CDay & "# As MyDate1, "
                'sSQL = sSQL & "#" & TYear & "-" & TMonth & "-" & TDay & "# As MyDate2, "
                sSQL = sSQL & "" & sPPeriod & " as Expr4, " & sFinYear & " As Expr5, " & "'" & sGDC & "' as Expr5a, "
                sSQL = sSQL & "'" & sReference & "' as Expr6, '" & SQLConvert(sDescription) & "' As Expr7, "
                sSQL = sSQL & "'" & sAccountNumber & "' as Expr8, '" & sLinkAcc & "' As Expr9, "
                sSQL = sSQL & "" & CStr(oAmount) & " as Expr10, " & CStr(oTaxType) & " As Expr11, "
                sSQL = sSQL & "" & CStr(oTaxAmount) & " as Expr12, '" & sUserID & "' As Expr13, "
                sSQL = sSQL & "" & CStr(oSupplierID) & " as ExprNew, " & CStr(oEmployeeID) & " As Expr15, "
                sSQL = sSQL & "'" & SQLConvert(sDescription2) & "' as Expr16, '" & SQLConvert(sDescription3) & "' As Expr17, "
                sSQL = sSQL & "'" & SQLConvert(sDescription4) & "' as Expr18, '" & SQLConvert(sDescription5) & "' As Expr19, "
                sSQL = sSQL & "" & CStr(blnPosted) & " as Expr20, " & CStr(blnAccounting) & " As Expr21, "
                sSQL = sSQL & "" & CStr(blnVoid) & " as Expr22, '" & CStr(sTransactionType) & "' As Expr23, "
                sSQL = sSQL & "'" & oDR_CR & "' as Expr24, '" & oContraAccount & "' As Expr25, '" & oDescriptionCode & "' As Expr26, "
                sSQL = sSQL & "'" & oDocType & "' as Expr27, '" & SQLConvert(oSupplierName) & "' As Expr28,"
                sSQL = sSQL & "'" & SQLConvert(oInfo) & "' as Expr29, '" & SQLConvert(oInfo2) & "' as Expr30, '" & SQLConvert(oInfo3) & "' As Expr31"
                If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                    sSQL = sSQL & ", " & oCompanyID & " as Expr32"
                End If


                '// define the sql statement to execute
                'Dim cmd As New OleDbCommand(sSQL, cn)
                cmd = New SqlCommand(sSQL, cn)
                cmd.CommandTimeout = 300
                cmd.Transaction = trans '+++++++ Transaction and rollback ++++++++
                cmd.ExecuteNonQuery()

            Next

            trans.Commit()  '+++++++ Transaction and rollback ++++++++
            cmd = Nothing

            'System.Threading.Thread.Sleep(1200) 'A little append to make sure it hits the database before any further code is written

        Catch ex As Exception
            trans.Rollback() '+++++++ Transaction and rollback ++++++++


            MsgBox(ex.Message & " 060vv")
            blnAppend_Failed = True
            blnCRITICAL_ERROR = True


        Finally

            If Not IsNothing(cmd) Then
                cmd.Dispose()
            End If

            If Not IsNothing(cn) Then
                cn.Dispose()
            End If

        End Try
    End Sub

    Sub Load_InvoiceVariables(ByVal oID As Integer)

        Try

            Dim sSQL As String

            sSQL = "SELECT * FROM Transactions WHERE ID = " & oID

            If My.Settings.DBType = "Access" Then
                Dim xConnection As New OleDbConnection(My.Settings.CS_Setting)
                Dim cmd As New OleDbCommand(sSQL, xConnection)
                xConnection.Open()
                Dim datareader As OleDbDataReader = cmd.ExecuteReader

                Try

                    While datareader.Read

                        oBatchID = datareader("BatchID")
                        oTransactionID = datareader("TransactionID")
                        oLinkID = datareader("LinkID")
                        cTransactionDate = datareader("Transaction Date")
                        cCaptureDate = datareader("Capture Date")
                        cPPeriod = datareader("PPeriod")
                        cFinYear = datareader("Fin Year")
                        cGDC = datareader("GDC")
                        cInvoiceNumber = datareader("Reference")
                        cDescription = datareader("Description")
                        cAccountNumber = datareader("AccNumber")
                        cContraAccount = datareader("Contra Account")
                        cLinkAcc = datareader("LinkAcc")
                        cAmount = datareader("Amount")
                        cTaxType = datareader("TaxType")
                        cTaxAmount = datareader("Tax Amount")
                        cUserID = datareader("UserID")
                        cSupplierID = datareader("SupplierID")
                        cEmployeeID = datareader("EmployeeID")
                        cDescription2 = datareader("Description 2")
                        cDescription3 = datareader("Description 3")
                        cDescription4 = datareader("Description 4")
                        cDescription5 = datareader("Description 5")
                        cPosted = datareader("Posted")
                        cAccounting = datareader("Accounting")
                        cVoid = datareader("Void")
                        cTransactionType = datareader("Transaction Type")
                        cDRCR = datareader("DR_CR")
                        cDescriptionCode = datareader("Description Code")
                        cDocType = datareader("DocType")
                        cSupplierName = datareader("SupplierName")

                    End While

                    xConnection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message)
                    xConnection.Close()
                End Try

            ElseIf My.Settings.DBType = "SQL" Then

                Dim xConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, xConnection)
                cmd.CommandTimeout = 300
                xConnection.Open()
                Dim datareader As SqlDataReader = cmd.ExecuteReader

                Try

                    While datareader.Read

                        oLinkID = datareader("LinkID")
                        dTransactionDate = datareader("Transaction Date")
                        dCaptureDate = datareader("Capture Date")
                        sPPeriod = datareader("PPeriod")
                        sFinYear = datareader("Fin Year")
                        sGDC = datareader("GDC")
                        sInvoiceNumber = datareader("Reference")
                        sDescription = datareader("Description")
                        sAccountNumber = datareader("AccNumber")
                        sContraAccount = datareader("Contra Account")
                        sLinkAcc = datareader("LinkAcc")
                        oAmount = datareader("Amount")
                        oTaxType = datareader("TaxType")
                        oTaxAmount = datareader("Tax Amount")
                        sUserID = datareader("UserID")
                        oSupplierID = datareader("SupplierID")
                        oEmployeeID = datareader("EmployeeID")
                        sDescription2 = datareader("Description 2")
                        sDescription3 = datareader("Description 3")
                        sDescription4 = datareader("Description 4")
                        sDescription5 = datareader("Description 5")
                        blnPosted = datareader("Posted")
                        blnAccounting = datareader("Accounting")
                        blnVoid = datareader("Void")
                        TransactionType = datareader("Transaction Type")
                        sDRCR = datareader("DR_CR")
                        oSupplierName = datareader("SupplierName")


                    End While

                    xConnection.Close()

                Catch ex As Exception
                    MsgBox(ex.Message)
                    xConnection.Close()
                End Try

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class

