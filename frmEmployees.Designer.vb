﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployees
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployees))
        Me.dgvEmployees = New System.Windows.Forms.DataGridView()
        Me.EmployeeIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FirstNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LastNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WaiterNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActiveDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CommissionPercDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WageSalaryDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContactNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contact2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StartDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContractEndDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LoanDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpBankNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpBankAccHolderDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpBankAccNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpBankBranchDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IsSchedulerContactDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PayrollNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ctmEmpBankingDetails = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EmpBankDetails = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnAddEmp = New System.Windows.Forms.Button()
        Me.btnEditEmp = New System.Windows.Forms.Button()
        Me.btnTerminateEmp = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Chk_ShowInactive = New System.Windows.Forms.CheckBox()
        CType(Me.dgvEmployees, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctmEmpBankingDetails.SuspendLayout()
        CType(Me.EmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvEmployees
        '
        Me.dgvEmployees.AllowUserToAddRows = False
        Me.dgvEmployees.AllowUserToDeleteRows = False
        Me.dgvEmployees.AutoGenerateColumns = False
        Me.dgvEmployees.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvEmployees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmployees.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EmployeeIDDataGridViewTextBoxColumn, Me.FirstNameDataGridViewTextBoxColumn, Me.LastNameDataGridViewTextBoxColumn, Me.WaiterNumberDataGridViewTextBoxColumn, Me.EmployeeTypeDataGridViewTextBoxColumn, Me.ActiveDataGridViewTextBoxColumn, Me.CommissionPercDataGridViewTextBoxColumn, Me.WageSalaryDataGridViewTextBoxColumn, Me.ContactNumberDataGridViewTextBoxColumn, Me.Contact2DataGridViewTextBoxColumn, Me.AddressDataGridViewTextBoxColumn, Me.PaymentTypeDataGridViewTextBoxColumn, Me.StartDateDataGridViewTextBoxColumn, Me.ContractEndDataGridViewTextBoxColumn, Me.LoanDataGridViewTextBoxColumn, Me.EmpBankNameDataGridViewTextBoxColumn, Me.EmpBankAccHolderDataGridViewTextBoxColumn, Me.EmpBankAccNoDataGridViewTextBoxColumn, Me.EmpBankBranchDataGridViewTextBoxColumn, Me.IsSchedulerContactDataGridViewTextBoxColumn, Me.PayrollNumberDataGridViewTextBoxColumn, Me.CoIDDataGridViewTextBoxColumn, Me.IDNumberDataGridViewTextBoxColumn})
        Me.dgvEmployees.ContextMenuStrip = Me.ctmEmpBankingDetails
        Me.dgvEmployees.DataSource = Me.EmployeeBindingSource
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Century Gothic", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEmployees.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEmployees.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployees.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployees.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvEmployees.Name = "dgvEmployees"
        Me.dgvEmployees.ReadOnly = True
        Me.dgvEmployees.RowTemplate.Height = 24
        Me.dgvEmployees.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmployees.Size = New System.Drawing.Size(1295, 708)
        Me.dgvEmployees.TabIndex = 31
        '
        'EmployeeIDDataGridViewTextBoxColumn
        '
        Me.EmployeeIDDataGridViewTextBoxColumn.DataPropertyName = "EmployeeID"
        Me.EmployeeIDDataGridViewTextBoxColumn.HeaderText = "EmployeeID"
        Me.EmployeeIDDataGridViewTextBoxColumn.Name = "EmployeeIDDataGridViewTextBoxColumn"
        Me.EmployeeIDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FirstNameDataGridViewTextBoxColumn
        '
        Me.FirstNameDataGridViewTextBoxColumn.DataPropertyName = "First_Name"
        Me.FirstNameDataGridViewTextBoxColumn.HeaderText = "First name"
        Me.FirstNameDataGridViewTextBoxColumn.Name = "FirstNameDataGridViewTextBoxColumn"
        Me.FirstNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LastNameDataGridViewTextBoxColumn
        '
        Me.LastNameDataGridViewTextBoxColumn.DataPropertyName = "Last_Name"
        Me.LastNameDataGridViewTextBoxColumn.HeaderText = "Last name"
        Me.LastNameDataGridViewTextBoxColumn.Name = "LastNameDataGridViewTextBoxColumn"
        Me.LastNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'WaiterNumberDataGridViewTextBoxColumn
        '
        Me.WaiterNumberDataGridViewTextBoxColumn.DataPropertyName = "Waiter_Number"
        Me.WaiterNumberDataGridViewTextBoxColumn.HeaderText = "Waiter number"
        Me.WaiterNumberDataGridViewTextBoxColumn.Name = "WaiterNumberDataGridViewTextBoxColumn"
        Me.WaiterNumberDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EmployeeTypeDataGridViewTextBoxColumn
        '
        Me.EmployeeTypeDataGridViewTextBoxColumn.DataPropertyName = "Employee_Type"
        Me.EmployeeTypeDataGridViewTextBoxColumn.HeaderText = "Employee type"
        Me.EmployeeTypeDataGridViewTextBoxColumn.Name = "EmployeeTypeDataGridViewTextBoxColumn"
        Me.EmployeeTypeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ActiveDataGridViewTextBoxColumn
        '
        Me.ActiveDataGridViewTextBoxColumn.DataPropertyName = "Active"
        Me.ActiveDataGridViewTextBoxColumn.HeaderText = "Active"
        Me.ActiveDataGridViewTextBoxColumn.Name = "ActiveDataGridViewTextBoxColumn"
        Me.ActiveDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CommissionPercDataGridViewTextBoxColumn
        '
        Me.CommissionPercDataGridViewTextBoxColumn.DataPropertyName = "Commission_Perc"
        Me.CommissionPercDataGridViewTextBoxColumn.HeaderText = "Commission percentage"
        Me.CommissionPercDataGridViewTextBoxColumn.Name = "CommissionPercDataGridViewTextBoxColumn"
        Me.CommissionPercDataGridViewTextBoxColumn.ReadOnly = True
        '
        'WageSalaryDataGridViewTextBoxColumn
        '
        Me.WageSalaryDataGridViewTextBoxColumn.DataPropertyName = "Wage_Salary"
        Me.WageSalaryDataGridViewTextBoxColumn.HeaderText = "Wage salary"
        Me.WageSalaryDataGridViewTextBoxColumn.Name = "WageSalaryDataGridViewTextBoxColumn"
        Me.WageSalaryDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ContactNumberDataGridViewTextBoxColumn
        '
        Me.ContactNumberDataGridViewTextBoxColumn.DataPropertyName = "Contact_Number"
        Me.ContactNumberDataGridViewTextBoxColumn.HeaderText = "Contact number"
        Me.ContactNumberDataGridViewTextBoxColumn.Name = "ContactNumberDataGridViewTextBoxColumn"
        Me.ContactNumberDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Contact2DataGridViewTextBoxColumn
        '
        Me.Contact2DataGridViewTextBoxColumn.DataPropertyName = "Contact_2"
        Me.Contact2DataGridViewTextBoxColumn.HeaderText = "Contact 2"
        Me.Contact2DataGridViewTextBoxColumn.Name = "Contact2DataGridViewTextBoxColumn"
        Me.Contact2DataGridViewTextBoxColumn.ReadOnly = True
        '
        'AddressDataGridViewTextBoxColumn
        '
        Me.AddressDataGridViewTextBoxColumn.DataPropertyName = "Address"
        Me.AddressDataGridViewTextBoxColumn.HeaderText = "Address"
        Me.AddressDataGridViewTextBoxColumn.Name = "AddressDataGridViewTextBoxColumn"
        Me.AddressDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PaymentTypeDataGridViewTextBoxColumn
        '
        Me.PaymentTypeDataGridViewTextBoxColumn.DataPropertyName = "Payment_Type"
        Me.PaymentTypeDataGridViewTextBoxColumn.HeaderText = "Payment_Type"
        Me.PaymentTypeDataGridViewTextBoxColumn.Name = "PaymentTypeDataGridViewTextBoxColumn"
        Me.PaymentTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.PaymentTypeDataGridViewTextBoxColumn.Visible = False
        '
        'StartDateDataGridViewTextBoxColumn
        '
        Me.StartDateDataGridViewTextBoxColumn.DataPropertyName = "Start_Date"
        Me.StartDateDataGridViewTextBoxColumn.HeaderText = "Start_Date"
        Me.StartDateDataGridViewTextBoxColumn.Name = "StartDateDataGridViewTextBoxColumn"
        Me.StartDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.StartDateDataGridViewTextBoxColumn.Visible = False
        '
        'ContractEndDataGridViewTextBoxColumn
        '
        Me.ContractEndDataGridViewTextBoxColumn.DataPropertyName = "Contract_End"
        Me.ContractEndDataGridViewTextBoxColumn.HeaderText = "Contract_End"
        Me.ContractEndDataGridViewTextBoxColumn.Name = "ContractEndDataGridViewTextBoxColumn"
        Me.ContractEndDataGridViewTextBoxColumn.ReadOnly = True
        Me.ContractEndDataGridViewTextBoxColumn.Visible = False
        '
        'LoanDataGridViewTextBoxColumn
        '
        Me.LoanDataGridViewTextBoxColumn.DataPropertyName = "Loan"
        Me.LoanDataGridViewTextBoxColumn.HeaderText = "Loan"
        Me.LoanDataGridViewTextBoxColumn.Name = "LoanDataGridViewTextBoxColumn"
        Me.LoanDataGridViewTextBoxColumn.ReadOnly = True
        Me.LoanDataGridViewTextBoxColumn.Visible = False
        '
        'EmpBankNameDataGridViewTextBoxColumn
        '
        Me.EmpBankNameDataGridViewTextBoxColumn.DataPropertyName = "Emp_BankName"
        Me.EmpBankNameDataGridViewTextBoxColumn.HeaderText = "Bank name"
        Me.EmpBankNameDataGridViewTextBoxColumn.Name = "EmpBankNameDataGridViewTextBoxColumn"
        Me.EmpBankNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EmpBankAccHolderDataGridViewTextBoxColumn
        '
        Me.EmpBankAccHolderDataGridViewTextBoxColumn.DataPropertyName = "Emp_BankAccHolder"
        Me.EmpBankAccHolderDataGridViewTextBoxColumn.HeaderText = "Acc holder"
        Me.EmpBankAccHolderDataGridViewTextBoxColumn.Name = "EmpBankAccHolderDataGridViewTextBoxColumn"
        Me.EmpBankAccHolderDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EmpBankAccNoDataGridViewTextBoxColumn
        '
        Me.EmpBankAccNoDataGridViewTextBoxColumn.DataPropertyName = "Emp_BankAccNo"
        Me.EmpBankAccNoDataGridViewTextBoxColumn.HeaderText = "Acc No"
        Me.EmpBankAccNoDataGridViewTextBoxColumn.Name = "EmpBankAccNoDataGridViewTextBoxColumn"
        Me.EmpBankAccNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EmpBankBranchDataGridViewTextBoxColumn
        '
        Me.EmpBankBranchDataGridViewTextBoxColumn.DataPropertyName = "Emp_BankBranch"
        Me.EmpBankBranchDataGridViewTextBoxColumn.HeaderText = "Bank branch"
        Me.EmpBankBranchDataGridViewTextBoxColumn.Name = "EmpBankBranchDataGridViewTextBoxColumn"
        Me.EmpBankBranchDataGridViewTextBoxColumn.ReadOnly = True
        '
        'IsSchedulerContactDataGridViewTextBoxColumn
        '
        Me.IsSchedulerContactDataGridViewTextBoxColumn.DataPropertyName = "IsSchedulerContact"
        Me.IsSchedulerContactDataGridViewTextBoxColumn.HeaderText = "Is Scheduler active?"
        Me.IsSchedulerContactDataGridViewTextBoxColumn.Name = "IsSchedulerContactDataGridViewTextBoxColumn"
        Me.IsSchedulerContactDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PayrollNumberDataGridViewTextBoxColumn
        '
        Me.PayrollNumberDataGridViewTextBoxColumn.DataPropertyName = "PayrollNumber"
        Me.PayrollNumberDataGridViewTextBoxColumn.HeaderText = "Payroll number"
        Me.PayrollNumberDataGridViewTextBoxColumn.Name = "PayrollNumberDataGridViewTextBoxColumn"
        Me.PayrollNumberDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CoIDDataGridViewTextBoxColumn
        '
        Me.CoIDDataGridViewTextBoxColumn.DataPropertyName = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.HeaderText = "Co_ID"
        Me.CoIDDataGridViewTextBoxColumn.Name = "CoIDDataGridViewTextBoxColumn"
        Me.CoIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.CoIDDataGridViewTextBoxColumn.Visible = False
        '
        'IDNumberDataGridViewTextBoxColumn
        '
        Me.IDNumberDataGridViewTextBoxColumn.DataPropertyName = "IDNumber"
        Me.IDNumberDataGridViewTextBoxColumn.HeaderText = "ID No"
        Me.IDNumberDataGridViewTextBoxColumn.Name = "IDNumberDataGridViewTextBoxColumn"
        Me.IDNumberDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ctmEmpBankingDetails
        '
        Me.ctmEmpBankingDetails.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmpBankDetails})
        Me.ctmEmpBankingDetails.Name = "ctmEmpBankingDetails"
        Me.ctmEmpBankingDetails.Size = New System.Drawing.Size(252, 28)
        '
        'EmpBankDetails
        '
        Me.EmpBankDetails.Name = "EmpBankDetails"
        Me.EmpBankDetails.Size = New System.Drawing.Size(251, 24)
        Me.EmpBankDetails.Text = "Employee Banking Details"
        '
        'EmployeeBindingSource
        '
        Me.EmployeeBindingSource.DataSource = GetType(MB.Employee)
        '
        'btnAddEmp
        '
        Me.btnAddEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddEmp.Location = New System.Drawing.Point(7, 30)
        Me.btnAddEmp.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAddEmp.Name = "btnAddEmp"
        Me.btnAddEmp.Size = New System.Drawing.Size(179, 30)
        Me.btnAddEmp.TabIndex = 32
        Me.btnAddEmp.Text = "&Add Employee"
        Me.btnAddEmp.UseVisualStyleBackColor = True
        '
        'btnEditEmp
        '
        Me.btnEditEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditEmp.Location = New System.Drawing.Point(191, 30)
        Me.btnEditEmp.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEditEmp.Name = "btnEditEmp"
        Me.btnEditEmp.Size = New System.Drawing.Size(179, 30)
        Me.btnEditEmp.TabIndex = 33
        Me.btnEditEmp.Text = "&Edit Employee"
        Me.btnEditEmp.UseVisualStyleBackColor = True
        '
        'btnTerminateEmp
        '
        Me.btnTerminateEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTerminateEmp.Location = New System.Drawing.Point(375, 30)
        Me.btnTerminateEmp.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnTerminateEmp.Name = "btnTerminateEmp"
        Me.btnTerminateEmp.Size = New System.Drawing.Size(179, 30)
        Me.btnTerminateEmp.TabIndex = 36
        Me.btnTerminateEmp.Text = "&Deactivated Employee"
        Me.btnTerminateEmp.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvEmployees)
        Me.Panel1.Location = New System.Drawing.Point(8, 91)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1295, 708)
        Me.Panel1.TabIndex = 37
        '
        'btnExit
        '
        Me.btnExit.ForeColor = System.Drawing.Color.Navy
        Me.btnExit.Location = New System.Drawing.Point(1185, 806)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(117, 28)
        Me.btnExit.TabIndex = 38
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Chk_ShowInactive)
        Me.GroupBox1.Controls.Add(Me.btnAddEmp)
        Me.GroupBox1.Controls.Add(Me.btnEditEmp)
        Me.GroupBox1.Controls.Add(Me.btnTerminateEmp)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Navy
        Me.GroupBox1.Location = New System.Drawing.Point(8, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1295, 69)
        Me.GroupBox1.TabIndex = 39
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Employee Editing Options"
        '
        'Chk_ShowInactive
        '
        Me.Chk_ShowInactive.AutoSize = True
        Me.Chk_ShowInactive.Location = New System.Drawing.Point(1149, 41)
        Me.Chk_ShowInactive.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Chk_ShowInactive.Name = "Chk_ShowInactive"
        Me.Chk_ShowInactive.Size = New System.Drawing.Size(116, 21)
        Me.Chk_ShowInactive.TabIndex = 37
        Me.Chk_ShowInactive.Text = "Show Inactive"
        Me.Chk_ShowInactive.UseVisualStyleBackColor = True
        '
        'frmEmployees
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1316, 827)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximumSize = New System.Drawing.Size(1334, 874)
        Me.MinimumSize = New System.Drawing.Size(1334, 874)
        Me.Name = "frmEmployees"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Employees&MasterFile"
        Me.Text = "Employees"
        CType(Me.dgvEmployees, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctmEmpBankingDetails.ResumeLayout(False)
        CType(Me.EmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvEmployees As System.Windows.Forms.DataGridView
    Friend WithEvents btnAddEmp As System.Windows.Forms.Button
    Friend WithEvents btnEditEmp As System.Windows.Forms.Button
    Friend WithEvents btnTerminateEmp As System.Windows.Forms.Button
    Friend WithEvents ctmEmpBankingDetails As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EmpBankDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents EmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmployeeIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FirstNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LastNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WaiterNumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActiveDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CommissionPercDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WageSalaryDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContactNumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contact2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StartDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContractEndDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LoanDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmpBankNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmpBankAccHolderDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmpBankAccNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmpBankBranchDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsSchedulerContactDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PayrollNumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDNumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Chk_ShowInactive As System.Windows.Forms.CheckBox
End Class
