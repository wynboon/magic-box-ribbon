﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows.Forms
Imports System.Diagnostics

'############################################################
'----Implementation of Paying selected suppliers invoices----
'------------Using ACB File (with FNB format)----------------
'---Gets Data Required to create ACB file from the inteface--
#Region "CLASSES that source data from frmSupplierPayments Interface into a ACB_Creation Class that create ACB FILE"

'############################################################
'----------Class that takes the needed data field, ----------
'-----------according to the slelected   invoices------------
Public Class MBAdapterToACB_File

    'Properties With Data From DGV PaymentsList "
    Private DGV_Payment As New DGV_PaymentData
    'Properties With Data Not Found in DGV Payments List but Needed For ACB"
    Private SuppliersBanking As New BankDetailsFor_ACB()
    Private CompanyBanking As New BankDetailsFor_ACB()
    'Propertise Needed To Create ACB File"
    Private BulkTransaction_(MAX_PER_BULK) As STD_BulkTransactin 'Bulk Details
    Private PaymentsDate_ As String

    Const MAX_PER_BULK As Integer = 200 'maximum invoices
    Private MaxInv As Integer
    Private SupTable As New DataTable

    Public Sub New(ByRef dgvPaymentList As DataGridView, ByVal CoID As Integer, ByVal paymentDate As Date)

        FillSupplierTable()
        Dim SupID
        CompanyBanking.GetBanking(CoID, "Co_ID")
        MaxInv = dgvPaymentList.RowCount
        If MaxInv > MAX_PER_BULK Then
            MsgBox("Maximum Invoice Per Bulk Payment " & vbNewLine & "FNB-ACB file Max Invoices is 200")
            Exit Sub
        End If

        For eachInv = 0 To MaxInv - 2
            DGV_Payment.GetDGV_PaymentList(dgvPaymentList, eachInv)
            SupID = getSupIDfromDGV_Data(DGV_Payment.SupplierName)
            SuppliersBanking.GetBanking("SupplierBank", SupID, "SupplierID") 'supID for each


            BulkTransaction_(eachInv).UserAccountNumber = CompanyBanking.BankAcc
            BulkTransaction_(eachInv).UserAccountBranch = CompanyBanking.BankBranch
            BulkTransaction_(eachInv).UserRef = DGV_Payment.SupplierName 'CompanyBanking.BankAccRef

            BulkTransaction_(eachInv).CreditorAccountNumber = SuppliersBanking.BankAcc
            BulkTransaction_(eachInv).CreditorBranch = SuppliersBanking.BankBranch
            BulkTransaction_(eachInv).CreditorAccountType = SuppliersBanking.BankAccType
            BulkTransaction_(eachInv).CreditorRef = SuppliersBanking.BankAccRef

            BulkTransaction_(eachInv).PayAmount = DGV_Payment.Due
        Next
        PaymentsDate_ = paymentDate.ToString

    End Sub

    Sub FillSupplierTable()

        Dim SQLAdapter As SqlDataAdapter
        Try
            Dim sSQL As String

            sSQL = "Select SupplierID,SupplierName From Suppliers" ' Where SupplierName = " & SupName
            ' sSQL = "Select DISTICNT SupID From Suppliers Where SupplierNAme = " & SupName

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            SQLAdapter = New SqlDataAdapter(sSQL, connection)
            SQLAdapter.Fill(SupTable)
        Catch ex As Exception
            MsgBox(ex.Message & " getSupIDfromDGV_Data")
        End Try

    End Sub

    Sub ShowDetails()
        Dim tabl As String = "[SupplierBank]"
        Dim xSQLTable As New DataTable
        Try
            Dim sSQL As String

            sSQL = "Select * From " & tabl
            'If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
            '    sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            'End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim xSQLAdapter_Accounts = New SqlDataAdapter(sSQL, connection)
            xSQLAdapter_Accounts.Fill(xSQLTable)
            ' Me.dgvAccounts.DataSource = xSQLTable_Accounts

            Dim accFrm As New Form()
            accFrm.Text = tabl
            accFrm.SetBounds(20, 20, 800, 500)
            accFrm.Show()

            Dim dgvAcc As New DataGridView()
            dgvAcc.DataSource = xSQLTable
            accFrm.Controls.Add(dgvAcc)
            dgvAcc.Dock = DockStyle.Fill


        Catch ex As Exception
            MessageBox.Show("An error occured with details :" & ex.Message)
        End Try
    End Sub

    Function getSupIDfromDGV_Data(ByVal SupName As String) As Integer
        'since no SupID on DV Payment list
        'SupID is needed to access bank info
        Dim SupID_ As Integer
        '  MsgBox()
        Dim row As DataRow = SupTable.Select("SupplierName = '" & SupName & "' ").FirstOrDefault()
        If Not row Is Nothing Then
            SupID_ = row.Item("SupplierID")
        End If
        'SupID_ = Integer.Parse(SupTable.Rows(0).Item("SupplierID").ToString())
        'MsgBox("supplier id found" & SupID_)
        Return SupID_
    End Function

    '############################################################
    '----------ACB Properties Details-------------

    Public Property BulkTransaction() As STD_BulkTransactin()
        Get
            Return BulkTransaction_
        End Get
        Set(ByVal value As STD_BulkTransactin())
            BulkTransaction_ = value
        End Set
    End Property

    Public Property PaymentsDate() As String
        Get
            Return PaymentsDate_
        End Get
        Set(ByVal value As String)
            PaymentsDate_ = value
        End Set
    End Property

End Class

'############################################################
'----------Implementation ACB Banking Details-------------
Class BankDetailsFor_ACB

    Private BankAcc_ As String
    Private BankBranch_ As String
    Private BankAccType_ As String
    Private BankAccRef_ As String

    Private ID As Integer
    Private DB_ID As String
    Private BankTable As New DataTable
    Private SQLAdapter As SqlDataAdapter

    Public Sub GetBanking(ByVal ID As Integer, ByVal ID_TypeInDB As String)
        Dim SQLstr As String = "SELECT * " & _
                               "FROM [Accounting] left Join [Company Details] on [Company Details].ID = Accounting.Co_ID "
        SQLstr = SQLstr & "WHERE BankAccountNo <> '' and [Accounting].Co_ID = " & ID

        Me.ID = ID
        DB_ID = ID_TypeInDB
        FillCompanyBankInto(SQLstr, BankTable)
        'MsgBox("Bank Infomation from " & BankTable.ToString & " with " & BankTable.Rows.Count & " rows. Co_ID = " & ID) ''''''''''''''''''''''
        BankAcc_ = getStringField(BankTable, "BankAccountNo")
        BankBranch_ = getStringField(BankTable, "BranchCode")
        BankAccType_ = getStringField(BankTable, "Account Type")
        BankAccRef_ = getStringField(BankTable, "CompanyName")
        'MsgBox("Some Company info found is " & getStringField(BankTable, "Account Type"))
        BankTable.Clear()
    End Sub
    Public Sub GetBanking(ByVal DB_Table As String, ByVal ID As Integer, ByVal ID_TypeInDB As String)
        Me.ID = ID
        DB_ID = ID_TypeInDB
        FillBankInto(DB_Table, BankTable)
        'MsgBox("Bank Infomation from " & BankTable.ToString & " with " & BankTable.Rows.Count & " rows") ''''''''''''''''''''''
        BankAcc_ = getStringField(BankTable, "AccountNumber")
        BankBranch_ = getStringField(BankTable, "Branch")
        'BankAccType_ = getStringField(BankTable, "[Account Type]")
        BankAccRef_ = getStringField(BankTable, "DefaultRef")
        ' MsgBox("Some info found is " & getStringField(BankTable, "ID"))
        BankTable.Clear()
    End Sub


    Public Property BankAcc() As String
        Get
            Return BankAcc_
        End Get
        Set(ByVal value As String)
            BankAcc_ = value
        End Set
    End Property

    Public Property BankBranch() As String
        Get
            Return BankBranch_
        End Get
        Set(ByVal value As String)
            BankBranch_ = value
        End Set
    End Property

    Public Property BankAccType() As String
        Get
            Return BankAccType_
        End Get
        Set(ByVal value As String)
            BankAccType_ = value
        End Set
    End Property

    Public Property BankAccRef() As String
        Get
            Return BankAccRef_
        End Get
        Set(ByVal value As String)
            BankAccRef_ = value
        End Set
    End Property

    '############################################################
    '------------Implementation General Procedures-------------
    Function getStringField(ByVal fromDatatable As DataTable, ByVal fieldNeeded As String) As String
        Dim StringValue As String = " "

        For Each row As DataRow In fromDatatable.Rows
            'If row.Item(DB_ID).Equals(ID) Then
            StringValue = row.Item(fieldNeeded).ToString
            Exit For
            'End If
        Next


        'Dim row As DataRow = fromDatatable.Select(DB_ID & " = " & ID & " ").FirstOrDefault()
        'If Not row Is Nothing Then
        '    StringValue = row.Item(fieldNeeded)
        'End If

        Return StringValue
    End Function


    Private Sub FillCompanyBankInto(ByVal SQLstr As String, ByVal intoDataTable As DataTable)

        Try
            Dim sSQL As String

            sSQL = SQLstr

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            SQLAdapter = New SqlDataAdapter(sSQL, connection)
            SQLAdapter.Fill(intoDataTable)

            'MsgBox("Bank Table called  Company-Accounting  fills this number of rows " & intoDataTable.Rows.Count)

        Catch ex As Exception
            MsgBox(ex.Message & " 051b")

        End Try

    End Sub

    Private Sub FillBankInto(ByVal fromDB_Table As String, ByVal intoDataTable As DataTable)

        Try
            Dim sSQL As String

            sSQL = "Select * From " & fromDB_Table
            ' If ID <> "" And ID <> Nothing Then '##### NEW #####
            sSQL = sSQL & " Where " & DB_ID & " = " & ID
            '  End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            SQLAdapter = New SqlDataAdapter(sSQL, connection)
            SQLAdapter.Fill(intoDataTable)

            ' MsgBox("Bank Table called " & fromDB_Table & " fills this number of rows " & intoDataTable.Rows.Count)

        Catch ex As Exception
            MsgBox(ex.Message & " 051 - fill int bank")

        End Try

    End Sub

End Class

'############################################################
'--------Implementation ACB - Data form DGV Payments---------
Class DGV_PaymentData

    Public Sub GetDGV_PaymentList(ByRef DGV_PaymentsList_Table As DataGridView, ByVal InvoiceIndex As Integer)
        'MsgBox("Number of invoice from dgvPaymentList " & InvoiceIndex & " of " & DGV_PaymentsList_Table.Rows.Count)
        ' TransactionID_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("TransactionID").Value.ToString
        SupplierName_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("SupplierName").Value.ToString
        Reference_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("Reference").Value.ToString
        Due_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("Amount").Value.ToString
        TransactionDate_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("TransactionDate").Value.ToString
        PaymentType_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("PaymentType").Value.ToString
        SupplierID_ = DGV_PaymentsList_Table.Rows(InvoiceIndex).Cells("SuppID").Value
    End Sub

    Private TransactionID_ As String
    Public Property TransactionID() As String
        Get
            Return TransactionID_
        End Get
        Set(ByVal value As String)
            TransactionID_ = value
        End Set
    End Property

    Private SupplierName_ As String
    Public Property SupplierName() As String
        Get
            Return SupplierName_
        End Get
        Set(ByVal value As String)
            SupplierName_ = value
        End Set
    End Property

    Private Reference_ As String
    Public Property Reference() As String
        Get
            Return Reference_
        End Get
        Set(ByVal value As String)
            Reference_ = value
        End Set
    End Property

    Private Due_ As String
    Public Property Due() As String
        Get
            Return Due_
        End Get
        Set(ByVal value As String)
            Due_ = value
        End Set
    End Property

    Private TransactionDate_ As String
    Public Property TransactionDate() As String
        Get
            Return TransactionDate_
        End Get
        Set(ByVal value As String)
            TransactionDate_ = value
        End Set
    End Property

    Private PaymentType_ As String
    Public Property NewPaymentTypeProperty() As String
        Get
            Return PaymentType_
        End Get
        Set(ByVal value As String)
            PaymentType_ = value
        End Set
    End Property

    Private SupplierID_ As Integer
    Public Property SupplierID() As Integer
        Get
            Return SupplierID_
        End Get
        Set(ByVal value As Integer)
            SupplierID_ = value
        End Set
    End Property

End Class

'############################################################
'-----Data structure used just to hold each invoive data-----
Public Structure STD_BulkTransactin

    Public UserAccountBranch As String
    Public UserAccountNumber As String
    Public UserRef As String
    Public CreditorBranch As String
    Public CreditorAccountNumber As String
    Public CreditorAccountType As String
    Public CreditorRef As String
    Public PayAmount As String

End Structure

#End Region


'############################################################
'----Implementation ACB File ready to b imported by FNB------
Public Class ACB_Creation
    ''' <summary>
    ''' Class that create a ACB file
    ''' </summary>
    ''' <remarks></remarks>

    Private Instal_ As Installation
    Private User_ As User_Payer
    Private STD_Transaction As Payment_Transaction
    Private Bulk_STD_Transaction As String ' save multiples of STD_Transaction
    Private Contra As Contract_Date

    Private requiredField As Boolean
    Private alertOn As Boolean
    Const SIZE_ As Integer = 20
    Private missedField(SIZE_) As Boolean
    Private FilePath_ As String

    Public Sub New(Optional alertOn As Boolean = False, Optional FilePath_ As String = "C:\Users\MagicBoxProgramming\Desktop\Creditor_FNB_ACB_00.txt")

        Me.alertOn = alertOn
        requiredField = False
        For index = 0 To SIZE_ - 1
            missedField(index) = False
        Next
        Me.FilePath_ = FilePath_

    End Sub

    'implements the INSTALLATION HEADER AND TRAILER RECORD
    Private Structure Installation

        Private ID_Header As Integer ' 2 ID characters 
        Private ID_Trailer As Integer ' 2 ID characters
        Private Blanks_Header As String '179 blanks = 181 caharaters
        Private Blanks_Trailer As String '178 blanks = 181 caharaters

        'get records identifier for header and trailer 
        Public Sub New(ByVal Header_RecordID As Integer, ByVal Trailer_RecordID As Integer)
            ID_Header = Header_RecordID
            ID_Trailer = Trailer_RecordID
        End Sub

        'complete installation header to be posted on ACD File with 181 characters
        Public ReadOnly Property I_Header As String
            Get
                SetBlanks()
                Return ID_Header.ToString("D2") + Blanks_Header
            End Get
        End Property

        'complete installation trailler to be posted on ACD File with 181 characters
        Public ReadOnly Property I_Trailer As String
            Get
                SetBlanks()
                Return ID_Trailer.ToString("D2") + Blanks_Trailer
            End Get
        End Property

        Private Sub SetBlanks()
            Blanks_Header = StrDup(178, "0") '178 blanks for header
            Blanks_Trailer = StrDup(178, "0") '178 blanks for trailer
        End Sub

    End Structure ' the end of installation 

    'implements the USER HEADER AND TRAILER RECORD
    Private Structure User_Payer

        Private ID_Header As Integer ' 2 ID characters 
        Private ID_Trailer As Integer ' 2 ID characters
        Private HashTotal As Integer ' 12 digits
        Private H_BlankSize As Integer '178 zeros to fill blanks 
        Private BlanksBeforeHash As Integer  '70 zeros to fill blanks
        Private BlanksAfterHash As Integer  '96 zeros to fill blanks

        'get records identifier for header and trailer 
        Public Sub New(ByVal zerosFillsBlanks As Boolean, Optional ByVal Header_RecordID As Integer = 4, Optional ByVal Trailer_RecordID As Integer = 92)
            ID_Header = Header_RecordID
            ID_Trailer = Trailer_RecordID

            H_BlankSize = 178 '178 zeros to fill blanks 
            BlanksBeforeHash = 70 '70 zeros to fill blanks
            BlanksAfterHash = 96 '96 zeros to fill blanks
        End Sub

        'complete installation header to be posted on ACD File with 181 characters
        Public ReadOnly Property Header As String
            Get
                Return ID_Header.ToString("D2") + addBlanks(H_BlankSize)
            End Get
        End Property

        'complete installation trailler to be posted on ACD File with 181 characters
        Public ReadOnly Property Trailer As String
            Get
                Return ID_Trailer.ToString("D2") + addBlanks(BlanksBeforeHash) _
                    + generate_HashTotal(0).ToString("D12") + addBlanks(BlanksAfterHash)
            End Get
        End Property

        Private Function generate_HashTotal(ByRef hashTot As Long) As Long
            Return hashTot '47861444007 for now, no calculation
        End Function

        Private Function addBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, "0") 'zeros to fiil up the blanks
        End Function

    End Structure ' the end of user 

    'implements the STANDARD TRANSACTION RECORD
    Private Structure Payment_Transaction

        Private ID As Integer
        Private Branch As Integer
        Private Account As ULong
        Private ZerosBeforeUSN As Integer
        Private USN As Integer
        Private HBranch As Integer
        Private HAccount As ULong
        Private HAccType As Char
        Private Amount As Double
        Private ZerosBeforeTax As Integer
        Private Tax As Integer
        Private ZerosAfterTax As Integer
        Private ZerosBeforeReci_Ref As Integer
        Private Recipient_Ref As String
        Private SpacesBtwnRefs As Integer
        Private Owner_Ref As String
        Private SpacesBeforeNonSTD_Acc As Integer
        Private NonSTD_Acc As ULong
        Private ZerosAfterNonSTD_Acc As Integer

        'get transaction details 1--3---3----2-----2-----1-->>>12 parameters
        Public Sub New(ByVal tranID As Integer _
            , ByVal Branch As Integer, ByVal Account As ULong, ByVal USN As Integer _
            , ByVal HBranch As Integer, ByVal HAccount As ULong, ByVal HAccType As Char _
            , ByVal Amount As Double, ByVal Tax As Integer _
            , ByVal Recipient_Ref As String, ByVal Owner_Ref As String _
            , ByVal NonSTD_Acc As ULong) 'parameters


            Me.ID = tranID
            Me.Branch = Branch
            Me.Account = Account
            Me.USN = USN
            Me.HBranch = HBranch
            Me.HAccount = HAccount
            Me.HAccType = HAccType
            Me.Amount = Amount
            Me.Tax = Tax
            Me.Recipient_Ref = Recipient_Ref
            Me.Owner_Ref = Owner_Ref
            Me.NonSTD_Acc = NonSTD_Acc

            ZerosBeforeUSN = 4 'number of zeros to fill blanks
            ZerosBeforeTax = 8 'number of zeros to fill blanks
            ZerosAfterTax = 2 'number of zeros to fill blanks
            ZerosBeforeReci_Ref = 1 'number of zeros to fill blanks
            SpacesBtwnRefs = 10 'number of spaces to fill blanks
            SpacesBeforeNonSTD_Acc = 15 'number of spaces to fill blanks
            ZerosAfterNonSTD_Acc = 30 'number of zeros to fill blanks

        End Sub

        'keep the size a fixed length
        Private Function strSizeControl(ByRef setance As String, ByVal size As Integer) As String

            If setance.Length > size Then ' cut other charater above given size
                setance = setance.Remove(size, setance.Length - size)
            Else 'fill the extra space with blank space character
                setance = setance & StrDup(size - setance.Length, " ")
            End If

            Return setance
        End Function

        'it generates Zeros (0) to fill up blanks
        Private Function addZerosBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, "0") 'zeros to fiil up the blanks
        End Function

        'it generates spaces to fill up blanks
        Private Function addSpaceBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, " ") 'space character to fiil up the blanks
        End Function

        'removes the dot in a double number and returns a string with both
        Private Function removeDotForCent(ByRef AmountDbl As Double) As String
            Dim parts As String() = (AmountDbl.ToString("f2")).Split(New Char() {"."c})
            Return (CInt(parts(0))).ToString("D9") & parts(1)
        End Function

        'complete installation contra details to be posted on ACD File with 180 characters
        Public Overrides Function ToString() As String
            '-------
            'Size***2***6,11,[4],6***6,11,1***11***[8],1,[2]*****[1],20*****[10],"15******[15],"20,[30]
            '-------1-------4------3--------1--------3---------2--------2-------3
            Return ID.ToString("D2") &
               strSizeControl(Branch.ToString("D6"), 6) & Account.ToString("D11") & addZerosBlanks(ZerosBeforeUSN) & USN.ToString("D6") &
                HBranch.ToString("D6") & HAccount.ToString("D11") & HAccType.ToString() &
                removeDotForCent(Amount) &
                addZerosBlanks(ZerosBeforeTax) & Tax.ToString("D1") & addZerosBlanks(ZerosAfterTax) &
                addZerosBlanks(ZerosBeforeReci_Ref) & strSizeControl(Recipient_Ref, 20) &
                addSpaceBlanks(SpacesBtwnRefs) & strSizeControl(Owner_Ref, 15) &
                addSpaceBlanks(SpacesBeforeNonSTD_Acc) & NonSTD_Acc.ToString("D20") & addZerosBlanks(ZerosAfterNonSTD_Acc)
        End Function

    End Structure ' the end of transaction

    'implements the CONTRA RECORD
    Private Structure Contract_Date

        Private ID As Integer ' 2 ID characters 
        Private Action_Date As Date ' 6 date characters 
        Private BlanksBeforeDate As Integer  '56 zeros to fill blanks
        Private BlanksAfterDate As Integer  '116 zeros to fill blanks

        'get records identifier and action date 
        Public Sub New(ByVal zerosFillsBlanks As Boolean, ByVal TransDate As Date, Optional ByVal RecordID As Integer = 12)
            ID = RecordID
            Action_Date = TransDate

            BlanksBeforeDate = 56 '56 zeros to fill blanks
            BlanksAfterDate = 116 '116 zeros to fill blanks
        End Sub

        'it generates Zeros (0) to fill up blanks
        Private Function addBlanks(ByRef numOfZeros As Integer) As String
            Return StrDup(numOfZeros, "0") 'zeros to fiil up the blanks
        End Function

        'convert date into numeric yyMMdd -> 991231
        Private Function strDate_yymmdd(ByRef dateFormat As Date) As String
            If dateFormat.ToString = "" Then
                Return (Date.Now).ToString("yyMMdd") 'if no transcation date, then use today
            End If
            Return dateFormat.ToString("yyMMdd") 'yy & mm & dd 'with a required date format

        End Function

        'complete installation contra details to be posted on ACD File with 180 characters
        Public Overrides Function ToString() As String
            Return ID.ToString("D2") + addBlanks(BlanksBeforeDate) +
            strDate_yymmdd(Action_Date) + addBlanks(BlanksAfterDate)
        End Function
    End Structure ' the end of contra 

    'create the ACD text Document
    Private Sub createTextFile()

        SetAlertOn(alertOn) 'alert for any missing banking field

        Dim FILE_NAME As String = FilePath_
        Dim objWriter 'As System.IO.StreamWriter
        Try

            objWriter = New System.IO.StreamWriter(FILE_NAME)

            objWriter.WriteLine(Instal_.I_Header)

            objWriter.WriteLine(User_.Header)

            objWriter.WriteLine(Bulk_STD_Transaction)

            objWriter.WriteLine(Contra)

            objWriter.WriteLine(User_.Trailer)

            objWriter.WriteLine(Instal_.I_Trailer)


        Catch ex As Exception
            MsgBox("ACB File Accessing Error:  " & ex.Message)
        Finally
            objWriter.Close()
            OpenACD_File()
        End Try

    End Sub

    'check for empty fields and make sure is an empty string for text file creation 
    'if alert is set on then give a masg of fields that are empty
    Private Function forVBNullMakeEmptyString(ByVal txtBx As String, ByVal index As Integer) As String
        If txtBx = vbNullString Or txtBx = Nothing Or txtBx = " " Then
            missedField(index) = True
            requiredField = True
            Return " "
        End If
        Return txtBx
    End Function

    'check for empty fields and make sure is zero for text file creation and alet of those missing
    Private Function forVBNullSetZeroAndAlert(ByVal txtBx As String, ByVal index As Integer) As String
        If txtBx = vbNullString Or txtBx = Nothing Or txtBx = " " Then
            missedField(index) = True
            requiredField = True
            Return "0"
        End If
        Return txtBx
    End Function

    'display  a message that alert about missing banking filed
    Private Sub SetAlertOn(ByVal status As Boolean)
        Dim AlertMsg As String

        If status And requiredField Then
            AlertMsg = "One or More Required Banking Fields Cause The Error" & vbNewLine _
                     & "___________________________________________________" & vbNewLine
            For Each FieldStatus In missedField
                Select Case Array.IndexOf(missedField, FieldStatus)
                    Case 0
                        AlertMsg = AlertMsg & "Company Account Number Is Missing" & vbNewLine
                    Case 1
                        AlertMsg = AlertMsg & "Company Branch Code Is Missing" & vbNewLine
                    Case 2
                        AlertMsg = AlertMsg & "Company Reference Is Missing" & vbNewLine
                    Case 3
                        AlertMsg = AlertMsg & "Supplier Account Number Is Missing" & vbNewLine
                    Case 4
                        AlertMsg = AlertMsg & "Supplier Branch Code Is Missing" & vbNewLine
                    Case 5
                        AlertMsg = AlertMsg & "Supplier Reference Is Missing" & vbNewLine
                    Case 6
                        AlertMsg = AlertMsg & "Supplier Account Type Is Missing" & vbNewLine
                    Case 7
                        AlertMsg = AlertMsg & "Invoice Amount Is Missing" & vbNewLine
                    Case Else
                        AlertMsg = AlertMsg & "Error >> With requiredFields in ACB_Creation Class" & vbNewLine
                End Select
            Next
            AlertMsg = AlertMsg & "After FNB Import; Check Bulk Payment File Report sent by FNB into you email, for the affected Invoices"
            MsgBox(AlertMsg)
        End If
    End Sub

    'Interface to create an ACB file
    Public Sub GenerateACB_File(ByVal banch() As STD_BulkTransactin, ByVal ActionDate As Date)

        'INSTALLATION HEADER n TRAILER RECORDs - fill it with record ID for both header and trailler
        Instal_ = New Installation(2, 94)
        'USER HEADER n TRAILER RECORDs - fill it with record ID for both header and trailler
        User_ = New User_Payer(True)
        'CONTRA RECORD - fill it with action date
        Contra = New Contract_Date(False, ActionDate)
        'STANDARD TRANSACTION RECORD - fill it with each transaction/invoices/payments
        For Each trans In banch
            If Not trans.Equals(banch.First) Then
                Bulk_STD_Transaction = Bulk_STD_Transaction & STD_Transaction.ToString
            End If
            STD_Transaction = New Payment_Transaction(10, CInt(forVBNullSetZeroAndAlert(trans.UserAccountBranch, 1)), CULng(forVBNullSetZeroAndAlert(trans.UserAccountNumber, 0)), 1, _
                            CInt(forVBNullSetZeroAndAlert(trans.CreditorBranch, 4)), CULng(forVBNullSetZeroAndAlert(trans.CreditorAccountNumber, 3)), "1", _
                            CDbl(forVBNullSetZeroAndAlert(trans.PayAmount, 5)), 0, _
                            forVBNullMakeEmptyString(trans.CreditorRef, 6), forVBNullMakeEmptyString(trans.UserRef, 7), 0L)
            'MsgBox("Co_Acc  " & trans.UserAccountNumber & vbNewLine _
            '       & "Co_Bra  " & trans.UserAccountBranch & vbNewLine _
            '       & "Sup_Acc " & trans.CreditorAccountNumber & vbNewLine _
            '       & "Sup_Bra " & trans.CreditorBranch & vbNewLine
            '      )
            'STD_Transaction = New Payment_Transaction(10, CInt(forVBNullSetZeroAndAlert(0, 1)), CULng(forVBNullSetZeroAndAlert(0, 0)), 1, _
            '                CInt(forVBNullSetZeroAndAlert(0, 4)), CULng(forVBNullSetZeroAndAlert(0, 3)), "1", _
            '                CDbl(forVBNullSetZeroAndAlert(trans.PayAmount, 5)), 0, _
            '                forVBNullMakeEmptyString(trans.CreditorRef, 6), forVBNullMakeEmptyString(trans.UserRef, 7), 0L)
            If trans.Equals(banch.Last) Then
                Exit For
            ElseIf Not trans.Equals(banch.First) Then
                Bulk_STD_Transaction = Bulk_STD_Transaction & vbNewLine
            End If
        Next

        '#########################################################################
        createTextFile() 'now create an ACB file as a text file
        '#########################################################################
    End Sub

    'if want to give an option to open the file
    Public Sub OpenACD_File()

        If MsgBox("Do you want to open an ACB file" & " at: " & FilePath_ & vbNewLine, _
                  MsgBoxStyle.YesNo) = MsgBoxResult.Yes And System.IO.File.Exists(FilePath_) Then
            Process.Start(FilePath_)
        ElseIf Not System.IO.File.Exists(FilePath_) Then
            MsgBox("File Does Not Exist Yet! Click Payment Transaction then enter required datails then Click Create ACB file")
        End If
    End Sub
End Class
