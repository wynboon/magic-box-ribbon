﻿
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS


Public Class frmUserAuthorization

#Region "Variables"
    Dim oAccessAdapter_Users As OleDbDataAdapter
    Dim oAccessTable_Users As New DataTable
    Dim oSQLAdapter_Users As SqlDataAdapter
    Dim oSQLTable_Users As New DataTable
    Dim MBData As New DataExtractsClass
    Dim AuthourisationTypes As String = ""
    Dim oLevel As String = ""
#End Region

#Region "Methods"
    Public Function GetUserDetails(ByVal sUserName As String, ByVal sPassword As String) As String 'returns "Not User", "Wrong password", or the Level



        Dim sSQL As String
        sSQL = "SELECT * FROM Users"
        If My.Settings.Setting_CompanyID <> "" Or Not IsNothing(My.Settings.Setting_CompanyID) Then
            sSQL &= " Where Co_ID = " & CInt(My.Settings.Setting_CompanyID)
        End If

        Dim oUsername, oPassword As String

        'Set to this in case no user found
        GetUserDetails = "Not User"
        If My.Settings.DBType = "SQL" Then

            Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            Try

                cn.Open()
                Using reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                    While reader.Read()
                        oUsername = reader("Username").ToString()
                        oPassword = reader("Password").ToString()
                        oLevel = reader("Level").ToString()

                        If sUserName = oUsername Then

                            If sPassword = oPassword Then
                                GetUserDetails = oLevel
                            Else
                                GetUserDetails = "Wrong Password"
                            End If

                        End If

                    End While
                End Using
            Catch ex As Exception
                MsgBox(ex.Message & " 173")
            Finally
                If cn.State <> ConnectionState.Closed Then
                    cn.Close()
                End If
            End Try
        End If

    End Function
#End Region

    Private Sub frmUserAuthorization_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> User Authorisation -> Version : " & My.Settings.Setting_Version
        Call Fill_Payment_Types_Checklistbox()

    End Sub

    Sub GetUsers()
        Try
            Dim sSQL As String
            sSQL = "Select * From Users"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & " Where Co_ID = " & My.Settings.Setting_CompanyID
            End If
            'CLEAR THE DATA TABLES
            dgvUsers.DataSource = Nothing

            oSQLTable_Users.Clear()
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            oSQLAdapter_Users = New SqlDataAdapter(sSQL, connection)
            oSQLAdapter_Users.Fill(oSQLTable_Users)
            dgvUsers.DataSource = oSQLTable_Users

            dgvUsers.Columns(0).Width = 140
            dgvUsers.Columns(2).Width = 140
            ''Me.DataGridView1.Columns(0).ReadOnly = True
            dgvUsers.Columns(0).DefaultCellStyle.BackColor = Color.LightBlue
            dgvUsers.Columns(1).DefaultCellStyle.BackColor = Color.LightBlue

        Catch ex As Exception
            MsgBox(ex.Message & " 322")
        End Try
    End Sub


    Sub Fill_Payment_Types_Checklistbox()
        Try

            Dim sSQL As String
            sSQL = "SELECT DISTINCT [Display Name] FROM Accounting Where Type = 'Tender'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim datareader As SqlDataReader = cmd.ExecuteReader
            Me.clbPaymentTypes.Items.Clear()
            While datareader.Read
                If Not datareader("Display Name").Equals(DBNull.Value) And Not datareader("Display Name").ToString = "" Then
                    Me.clbPaymentTypes.Items.Add(datareader("Display Name"))
                End If
            End While
            connection.Close()

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub Button_Login_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Login.Click
        Try
            Me.Label_Level.Text = CheckUserLevel(Me.txtUserName.Text, Me.txtPassword.Text)

            If oLevel = "Basic User" Then
                MessageBox.Show("The user account you logging in with doesn't have rights to manage users, in order to continue your user type must be 'Super User'.", "User Login", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtPassword.Text = ""
                txtPassword.Focus()
                Exit Sub
            End If

            If Me.Label_Level.Text = "Super" Then

                Enabled = False
                Cursor = Cursors.AppStarting

                GetUsers()

                Enabled = True
                Cursor = Cursors.Arrow

            End If
        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details : " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Label_Level.Text <> "Super" Then
                MsgBox("Please sign in under the appropriate level to save changes")
                Exit Sub
            End If

            Enabled = False
            Cursor = Cursors.AppStarting

            Dim builder As New SqlCommandBuilder(oSQLAdapter_Users)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"
            oSQLAdapter_Users.Update(oSQLTable_Users)
            oSQLAdapter_Users.UpdateCommand = builder.GetUpdateCommand()

            Enabled = True
            Cursor = Cursors.Arrow

            MessageBox.Show("Changes to user(s) succesfully updated")

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnAddNewUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNewUser.Click

        Try
            If Me.Label_Level.Text.Trim = "" Then
                MessageBox.Show("Please log in in order to continue with adding users in the system", "Users maintanance", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtUserName.Focus()
                Exit Sub
            End If

            If txtUserName_New.Text.Trim = "" Then
                MessageBox.Show("Please enter a Username for the new user you want to create", "Username", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtUserName_New.Focus()
                Exit Sub
            End If

            If txtPassword_New.Text.Trim = "" Then
                MessageBox.Show("Please enter a Password for the new user in order to continue", "Password", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txtPassword_New.Focus()
                Exit Sub
            End If

            If cmbUserLevel.SelectedIndex = -1 Then
                MessageBox.Show("Please select the user level in order to continue", "User level", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                cmbUserLevel.Focus()
                Exit Sub
            End If

            Dim Username As String = CStr(txtUserName_New.Text.Trim)
            Dim Password As String = CStr(txtPassword_New.Text.Trim)

            Dim User = MBData.GetUser(Username, Password)

            If Not IsNothing(User) Then
                MessageBox.Show("The user you're attempting to create already exist, please supply different details", "New user", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtUserName_New.Text = ""
                txtPassword_New.Text = ""
                txtUserName_New.Focus()
                Exit Sub
            End If


            Enabled = False
            Cursor = Cursors.AppStarting


            If cmbUserLevel.Text = "Super User" Then

                AuthourisationTypes = ""

                For Each itm As String In clbPaymentTypes.CheckedItems
                    If AuthourisationTypes = "" Then
                        AuthourisationTypes = itm
                    Else
                        AuthourisationTypes &= "," & itm
                    End If
                Next
            End If


            Dim oCompanyID As Integer = CInt(My.Settings.Setting_CompanyID)

            Dim sSQL As String
            sSQL = "Insert Into Users ([Username],[Password],[Level],[Approve_Payments_Types]"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & ", Co_ID"
            End If
            sSQL = sSQL & " ) "
            sSQL = sSQL & " VALUES ('" & txtUserName_New.Text & "', '" & txtPassword_New.Text & "', '" & cmbUserLevel.Text & "',"
            sSQL = sSQL & "'" & AuthourisationTypes & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then
                sSQL = sSQL & ", " & oCompanyID
            End If
            sSQL = sSQL & ") "

            Dim ra As Integer

            Dim myConnection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            myConnection.Open()
            Dim myCommand As SqlCommand
            myCommand = New SqlCommand(sSQL, myConnection)
            myCommand.CommandTimeout = 300
            ra = myCommand.ExecuteNonQuery()
            'Since no value is returned we use ExecuteNonQuery
            MessageBox.Show("New user successfully created.", "New User", MessageBoxButtons.OK, MessageBoxIcon.Information)
            myConnection.Close()

            GetUsers()

            Enabled = True
            Cursor = Cursors.Arrow

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An Error occured with details " & ex.Message, "Error", MessageBoxButtons.OK)
        End Try
    End Sub
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles ckbPassword.CheckedChanged
        If Me.ckbPassword.Checked = True Then
            txtPassword_New.PasswordChar = ""
        Else
            txtPassword_New.PasswordChar = "*"
        End If
    End Sub
    Private Sub cmbUserLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUserLevel.SelectedIndexChanged
        If cmbUserLevel.Text = "Basic User" Then
            AuthourisationTypes = "CASH"
            clbPaymentTypes.SetItemChecked(0, True)
            clbPaymentTypes.Enabled = False
        ElseIf cmbUserLevel.Text = "Super User" Then
            AuthourisationTypes = ""
            clbPaymentTypes.Enabled = True
        End If
    End Sub
    Private Sub btnDeleteSelected_Click(sender As Object, e As EventArgs) Handles btnDeleteSelected.Click

        Try

            If Me.Label_Level.Text = "Super" Then

                If dgvUsers.SelectedRows.Count <> 1 Then
                    MessageBox.Show("Please select the user you want to delete", "Delete User", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    dgvUsers.Focus()
                    Exit Sub
                End If

                Enabled = False
                Cursor = Cursors.AppStarting

                Dim UserID As Integer = CInt(dgvUsers.Rows(dgvUsers.CurrentRow.Index).Cells(0).Value)
                MBData.DeleteUser(UserID)

                GetUsers()

                MessageBox.Show("User succesfully deleted", "Delete User", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Enabled = True
                Cursor = Cursors.Arrow

            Else

                MessageBox.Show("Please log-in in order to delete a user", "User Login", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtUserName.Focus()
            End If

        Catch ex As Exception
            Enabled = True
            Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class