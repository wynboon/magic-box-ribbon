﻿


Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Windows.Forms  '*** NOTE - MUST HAVE THIS FOR ADD-INS

Public Class Products


    Dim dbadp As OleDbDataAdapter
    Dim dTable As New DataTable


    Private Sub Products_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Products -> Version : " & My.Settings.Setting_Version
        Try
            Me.TopMost = True
            Me.Label_Version.Text = "Version " & My.Settings.Setting_Version
            Call Fill_DGV()
            Call Fill_DGV2()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Sub Fill_DGV()
        Try
            Dim sSQL As String

            sSQL = "SELECT * FROM Products"
            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            dbadp = New OleDbDataAdapter(sSQL, connection)

            dbadp.Fill(dTable)
            Me.DataGridView1.DataSource = dTable
            'Me.DataGridView1.Columns(0).Width = 0
        Catch ex As Exception
            MsgBox(ex.Message & " 188")
        End Try
    End Sub


    Private Sub Button_Save_Click(sender As System.Object, e As System.EventArgs) Handles Button_Save.Click
        Try
            Dim builder As New OleDbCommandBuilder(dbadp)
            builder.QuotePrefix = "["
            builder.QuoteSuffix = "]"

            dbadp.Update(dTable)
            dbadp.UpdateCommand = builder.GetUpdateCommand()
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
    End Sub

    Sub Fill_DGV2()
        Try


            Dim sSQL As String

            sSQL = "SELECT * FROM Product_Groups"

            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "ProdGroup_table")
            connection.Close()


            Me.DataGridView2.DataSource = Nothing
            Me.DataGridView2.DataSource = ds
            Me.DataGridView2.DataMember = "ProdGroup_table"

            Me.DataGridView2.Refresh()

        Catch ex As Exception
            MsgBox(ex.Message & " 189")
        End Try

    End Sub

End Class