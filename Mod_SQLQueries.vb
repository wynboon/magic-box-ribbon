﻿Imports System.Data
Imports System.Data.SqlClient

Module Mod_SQLQueries
    Function PaymentRequests() As DataTable
        Try

            Dim sSQL As String = "SELECT Distinct RequestID,InvoiceTransactionID,PaymentType,RequestedAmount,Suppliers.SupplierName, Transactions.Reference" & _
            " FROM [PaymentsRequests] Left join Transactions on Transactions.transactionID = PaymentsRequests.InvoiceTransactionID" & _
            " Left Join Suppliers on Transactions.SupplierID = Suppliers.SupplierID Where RequestApproved = 0 and " & _
            " [PaymentsRequests].Co_ID =" & My.Settings.Setting_CompanyID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function GetSupplierBankID(SupplierID As Long) As Long
        Try
            Dim sSQL As String
            sSQL = "Select ID From SupplierBank WHERE SupplierID = " & SupplierID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim ES As Object = cmd.ExecuteScalar()
            If IsNothing(ES) Then
                GetSupplierBankID = 0
            Else
                GetSupplierBankID = ES.ToString
            End If
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error looking up an account! " & ex.Message)
        End Try
    End Function
    Function UpdateRequest2Exported(RequestID As Long) As Long
        Try
            Dim sSQL As String
            sSQL = "Update [PaymentsRequests] Set [BankExported] = 'TRUE' Where RequestID = " & RequestID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            MsgBox("There was an error looking up an account! " & ex.Message)
        End Try
    End Function
    Function UpdateRequestTransactionID(OldTransactionID As Long, NewTransactionID As Long) As Long
        Try
            Dim sSQL As String
            sSQL = "Update [PaymentsRequests] Set [InvoiceTransactionID] = " & NewTransactionID & " Where InvoiceTransactionID = " & OldTransactionID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            MsgBox("There was an error looking up an account! " & ex.Message)
        End Try
    End Function
    Function BankAccNo(ByVal oSegment4 As String) As String
        Try

            Dim sSQL As String

            sSQL = "Select BankAccountNo from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                BankAccNo = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function BankBranch(ByVal oSegment4 As String) As String
        Try

            Dim sSQL As String

            sSQL = "Select BranchCode from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                BankBranch = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function BankLayout(ByVal oSegment4 As String) As String
        Try

            Dim sSQL As String

            sSQL = "Select BankLayout from Accounting Where [Segment 4] = '" & oSegment4 & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                BankLayout = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Bank Layout' type in for the ledger account " & oSegment4 & "!")
        End Try
    End Function
    Function BankLayout_fromAccCode(ByVal BankAccNumber As String) As String
        Try

            Dim sSQL As String

            sSQL = "Select BankLayout from Accounting Where [BankAccountNo] = '" & BankAccNumber & "'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            If My.Settings.DBType = "SQL" Then

                Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
                Dim cmd As New SqlCommand(sSQL, connection)
                cmd.CommandTimeout = 300
                connection.Open()
                BankLayout_fromAccCode = cmd.ExecuteScalar().ToString
                connection.Close()

            End If
        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function NonZeroTransaction() As String
        Try

            Dim sSQL As String = "SELECT Top 1 TransactionID " & _
            "FROM [Transactions] where Transactions.Co_ID =" & My.Settings.Setting_CompanyID & " and Void = 0 and Accounting = 1 " & _
            "Group By TransactionID Having SUM(Case When Dr_Cr = 'Cr' then Amount * -1 else Amount End) <> 0 order by TransactionID desc"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            NonZeroTransaction = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error looking up the 'Report Category' in Accounting!")
        End Try
    End Function
    Function NonZeroTransactionCount() As Double
        Try
            Dim sSQL As String = "SELECT Count(TransactionID) " & _
            "FROM [Transactions] where Transactions.Co_ID =" & My.Settings.Setting_CompanyID & " and Void = 0 and Accounting = 1 " & _
            "Group By TransactionID Having SUM(Case When Dr_Cr = 'Cr' then Amount * -1 else Amount End) <> 0  order by TransactionID desc"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            NonZeroTransactionCount = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error :" & ex.Message)
        End Try
    End Function
    Function Period_Delete(PeriodID As Long)
        Try
            Dim sSQL As String = "Delete from periods where ID = " & PeriodID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            MsgBox("There was an error removing the PeriodID: " & PeriodID & vbNewLine & ex.Message)
        End Try
    End Function
    Function Period_Add(FinYear As Long, Period As Long, StartDate As String, EndDate As String, Blocked As Boolean)
        Try
            Dim isBlocked As String
            If Blocked = True Then
                isBlocked = "Y"
            Else
                isBlocked = "N"
            End If
            Dim sSQL As String = "Insert into periods (FinYear,Period,[Start Date],[End Date],Blocked,Co_ID) " & _
                "Values (" & FinYear & "," & Period & ",'" & StartDate & "','" & EndDate & "','" & isBlocked & "'," & My.Settings.Setting_CompanyID & ")"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            MsgBox("There was an error adding the Period: " & Period & vbNewLine & ex.Message)
        End Try
    End Function
    Function Period_Edit(PeriodID As Long, FinYear As Long, Period As Long, StartDate As String, EndDate As String, Blocked As Boolean)
        Try
            Dim isBlocked As String
            If Blocked = True Then
                isBlocked = "Y"
            Else
                isBlocked = "N"
            End If
            Dim sSQL As String = "Update periods Set " & _
                "FinYear = " & FinYear & ", Period = " & Period & "," & _
                "[Start Date] = '" & StartDate & "',[End Date]='" & EndDate & "',Blocked='" & isBlocked & "' " & _
                "Where Co_ID = " & My.Settings.Setting_CompanyID & " and FinYear = " & FinYear & " and Period = " & Period

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            MsgBox("There was an error updating the Period: " & Period & vbNewLine & ex.Message)
        End Try
    End Function
    Function CheckClosedPeriod(SelectedDate As String) As Boolean
        Try
            Dim LastClosedDate As String = String.Empty

            Dim sSQL As String = "SELECT TOP 1 Convert(VARCHAR(12),[End Date] ,106) FROM [Periods] Where Co_ID = " & My.Settings.Setting_CompanyID & " and BLOCKED = 'Y' " & _
            "Order by [End Date] Desc"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim ODate As Object = cmd.ExecuteScalar()
            connection.Close()
            If ODate Is Nothing Then
                Return True
                Exit Function
            End If
            LastClosedDate = ODate.ToString
            If Date.Parse(SelectedDate) <= Date.Parse(LastClosedDate) Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            MsgBox("There was an error :" & ex.Message)
        End Try
    End Function
    Function CashupDates() As DataTable
        Try
            Dim sSQL As String = "SELECT Distinct [Transaction Date] " & _
            " FROM [Transactions]  Where Co_ID =" & My.Settings.Setting_CompanyID & " and [Transaction Type] in (144,145) and Void = 0 Order by [Transaction Date] Desc"
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function Tax_Edit(TaxPeriodID As Long, Desc As String, Perc As Double, IsDefault As Boolean)
        Try
            Dim sSQL As String

            If IsDefault = True Then
                sSQL = "Update TaxTypes Set " & _
                "[Default] = 'FALSE' " & _
                "Where Co_ID = " & My.Settings.Setting_CompanyID & vbNewLine & _
                "Update TaxTypes Set " & _
                "TaxDesc = '" & Desc & "', TaxPerc = " & Perc & "," & _
                "[Default] = 'TRUE' " & _
                "Where Co_ID = " & My.Settings.Setting_CompanyID & " and ID = " & TaxPeriodID
            Else
                sSQL = "Update TaxTypes Set " & _
                "TaxDesc = '" & Desc & "', TaxPerc = '" & Perc & "'," & _
                "[Default] = 'FALSE' " & _
                "Where Co_ID = " & My.Settings.Setting_CompanyID & " and ID = " & TaxPeriodID
            End If


            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error updating tax recort:" & vbNewLine & ex.Message)
        End Try
    End Function
    Function TaxRecord_Delete(TaxID As Long)
        Try
            Dim sSQL As String = "Delete from TaxTypes where ID = " & TaxID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error removing the record: " & TaxID & vbNewLine & ex.Message)
        End Try
    End Function
    Function Get_CreditorControlAcc() As String
        Try

            Dim sSQL As String

            sSQL = "Select Top 1 [Segment 4] from Accounting Where [Type] = 'Creditors Control'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Get_CreditorControlAcc = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error looking up the 'Creditor Control' in Accounting!")
            Return ""
        End Try
    End Function
    Function Get_VATControlAcc() As String
        Try

            Dim sSQL As String

            sSQL = "Select Top 1 [Segment 4] from Accounting Where [Type] = 'VAT Control'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Get_VATControlAcc = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error looking up the 'VAT Control' in Accounting!")
            Return ""
        End Try
    End Function
    Function Get_Default_Tax_Perc() As Double
        Try

            Dim sSQL As String

            sSQL = "Select Top 1 [TaxPerc] FROM [TaxTypes] Where [Default] = 'True'"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim TaxPerc As Object = Nothing
            TaxPerc = cmd.ExecuteScalar()
            connection.Close()
            If TaxPerc Is Nothing Then
                Return -1
            Else
                Get_Default_Tax_Perc = TaxPerc.ToString
            End If

        Catch ex As Exception
            MsgBox("There was an error looking up the 'VAT Default', the error was:" & vbNewLine & ex.Message)
            Return ""
        End Try
    End Function
    Function Get_TransactionID_from_Reference(ByVal Reference As String) As Integer
        Try
            Dim sSQL As String

            sSQL = "Select Top 1 TransactionID from Transactions Where [Reference] = '" & Reference & "' and Void = 0"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " And Co_ID = " & My.Settings.Setting_CompanyID
            End If

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Get_TransactionID_from_Reference = cmd.ExecuteScalar().ToString
            connection.Close()

        Catch ex As Exception
            MsgBox("There was an error getting the TransactionID. Error:'" & ex.Message)
            Return 0
        End Try
    End Function
    Function Update_PaymentLinkID(OldLinkID As Integer, NewLinkID As Integer) As Boolean
        Try
            Dim sSQL As String
            sSQL = "Update Transactions Set LinkID = " & NewLinkID & " Where LinkID = " & OldLinkID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error updating LinkID:" & vbNewLine & ex.Message)
            Return False
        End Try
    End Function
    Function VerifiedSummary(FromDate As String, ToDate As String) As DataTable
        Try
            Dim CreditorControlAccount As String
            CreditorControlAccount = Get_CreditorControlAcc()
            Dim sSQL As String = "SELECT Distinct [vw_Transactions].[TransactionID],[Transaction Date],[Reference],[SupplierID],[SupplierName],Verified " &
                                 "FROM [vw_Transactions] Left Join Verification On Verification.TransactionID = [vw_Transactions].[TransactionID] " &
                                 "Where [Void] = 0 and Accounting = 1 And [Transaction Date] >= '" & FromDate & "' And [Transaction Date] <= '" & ToDate & "'" &
                                 " and [transaction Type] = 12 and [Segment 4] = '" & CreditorControlAccount & "' and Co_ID =" & My.Settings.Setting_CompanyID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=360")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function VerifiedSummary2(FromDate As String, ToDate As String) As DataTable
        Try
            Dim CreditorControlAccount As String
            CreditorControlAccount = Get_CreditorControlAcc()
            Dim sSQL As String = "SELECT * FROM [vw_VerifyTree] Where [Transaction Date] >= '" & FromDate & "' And [Transaction Date] <= '" & ToDate & "' and " &
                                 " [Description Code] = '" & CreditorControlAccount & "' and Co_ID =" & My.Settings.Setting_CompanyID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=360")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function VerifiedDetail(TransactionID As String) As DataTable
        Try
            Dim CreditorControlAccount As String
            CreditorControlAccount = Get_CreditorControlAcc()
            Dim VatControlAccount As String
            VatControlAccount = Get_VATControlAcc()
            Dim sSQL As String = "SELECT vw_Transactions.[TransactionID],[BatchID] ,[Transaction Date] ,[Description],[Reference]" & _
                                ",[SupplierName],[Segment 3 Desc],[Segment 2 Desc],[Segment 1 Desc]," & _
                                "[Amount],[Tax Amount],([Amount]+[Tax Amount]) as Total,Verified,VerificationNotes FROM [vw_Transactions] Left Join Verification On Verification.TransactionID = [vw_Transactions].[TransactionID] " & _
                                "Where [transaction type] = 12 and [Segment 4] not in ('" & CreditorControlAccount & "','" & VatControlAccount & "') " & _
                                "and accounting = 1 and void = 0 " & _
                                "and vw_Transactions.[TransactionID] =" & TransactionID & "  and vw_Transactions.Co_ID =" & My.Settings.Setting_CompanyID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function InsertVerifivation(TxID As Long, Verified As Boolean, VerificationNotes As String)
        Try
            Dim sSQL As String
            sSQL = "Insert Into Verification (TransactionID,Verified,VerificationNotes) Values (" & _
            TxID & ",'" & Verified & "','" & VerificationNotes & "')"
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error updating verification notes:" & vbNewLine & ex.Message)
        End Try
    End Function
    Function UpdateVerifivation(TxID As Long, Verified As Boolean, VerificationNotes As String)
        Try
            Dim sSQL As String


            sSQL = "Update Verification Set Verified = '" & Verified & "',VerificationNotes = '" & VerificationNotes & "'" & _
            " Where TransactionID = " & TxID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error inserting verification notes:" & vbNewLine & ex.Message)
        End Try
    End Function
    Function InsertNewCompany(GroupID As Long, CompanyName As String, CoReg As String, CoVat As String, _
                              Address1 As String, Address2 As String, Address3 As String, Address4 As String, _
                              Tel1 As String, Tel2 As String, Email1 As String, Email2 As String, _
                              Directors As String, InvoicePrefix As String) As Long
        Try
            Dim sSQL As String

            sSQL = "INSERT INTO [Company Details] ([CompanyName],[LegalEntityName],[CoReg],[CoVat],[Address1]" & _
           ",[Address2],[Address3],[Address4],[TelephoneLine1],[TelephoneLine2],[EmailAddress1],[EmailAddress2]" & _
           ",[Directors],[PfxDrsInv]) VALUES ('" & _
           CompanyName & " ','" & CompanyName & "','" & CoReg & "','" & CoVat & "','" & _
           Address1 & " ','" & Address2 & "','" & Address3 & "','" & Address4 & "','" & _
           Tel1 & " ','" & Tel2 & "','" & Email1 & "','" & Email2 & "','" & _
           Directors & " ','" & InvoicePrefix & "')"


            Dim query2 As String = "Select @@Identity"
            Dim ID As Integer

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            cmd.CommandText = query2
            ID = cmd.ExecuteScalar()
            Return ID
            conn.Close()
            MsgBox("Complete")
        Catch ex As Exception
            MsgBox("There was an error inserting new company:" & vbNewLine & ex.Message)
        End Try
    End Function
    Function GetSuppliers() As DataTable
        Try
            Dim sSQL As String = "SELECT * from [Suppliers] Where Co_ID =" & My.Settings.Setting_CompanyID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function Get_Supplier_Email(ByVal SupplierID As Integer) As String

        Dim SupplierName As String = ""
        Dim cn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")

        Try

            Dim sSQL As String
            sSQL = "Select Email_Address from Suppliers WHERE SupplierID=" & SupplierID

            Dim cmd As New SqlCommand(sSQL, cn)
            cmd.CommandTimeout = 300
            cn.Open()
            SupplierName = CStr(cmd.ExecuteScalar())
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If

            Return SupplierName

        Catch ex As Exception
            MsgBox("An error occured with details : " & ex.Message)
            If cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try
    End Function
    Function UpdateSupplierIntegrationCode(SupplierID As Long, IntegrationCode As String) As Boolean
        Try
            Dim sSQL As String


            sSQL = "Update Suppliers Set IntegrationCode = '" & IntegrationCode & "' Where SupplierID = " & SupplierID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("There was an error inserting verification notes:" & vbNewLine & ex.Message)
        End Try
    End Function
End Module
Module Mod_InoiveIntegrate_SQLQueries
    Function Check_UnmappedItems(IntegrationLevel As String) As DataTable
        Try
            Dim sSQL As String = ""
            If IntegrationLevel = "Item" Then
                sSQL = "Select Distinct POS_GRN_Detail.ItemName from " & _
                                     "POS_GRN_Detail Left Join ItemMaster " & _
                                     "On POS_GRN_Detail.ItemName = ItemMaster.IntegrationCode and " & _
                                    "POS_GRN_Detail.Co_ID = ItemMaster.Co_ID " & _
                                    "Where isNull(Account,'True') = 'True' and POS_GRN_Detail.Co_ID =" & My.Settings.Setting_CompanyID
            Else
                sSQL = "Select Distinct POS_GRN_Detail.CostGroup from " & _
                                     "POS_GRN_Detail Left Join ItemMaster " & _
                                     "On POS_GRN_Detail.CostGroup = ItemMaster.IntegrationCode and " & _
                                    "POS_GRN_Detail.Co_ID = ItemMaster.Co_ID " & _
                                    "Where isNull(Account,'True') = 'True' and POS_GRN_Detail.Co_ID =" & My.Settings.Setting_CompanyID
            End If


            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error getting itemcheck occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function Check_UnmappedSuppliers() As DataTable
        Try
            Dim sSQL As String = "Select Distinct POS_GRN_Detail.SupplierName from " & _
                                 "POS_GRN_Detail Left Join Suppliers " & _
                                 "On POS_GRN_Detail.SupplierName = Suppliers.IntegrationCode and " & _
                                 "POS_GRN_Detail.Co_ID = Suppliers.Co_ID " & _
                                 "Where isNull(SupplierID,1) = 1 and POS_GRN_Detail.Co_ID = " & My.Settings.Setting_CompanyID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error getting Supplier Check occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function CashUpDays(StartDate As String, EndDate As String) As DataTable
        Try

            Dim sSQL As String = "Select Distinct [Transactions].[Transaction Date] From Transactions Where " & _
                "[Transaction date] >= '" & StartDate & "' and [Transaction date] <= '" & EndDate & "' and Transactions.Co_ID = " & My.Settings.Setting_CompanyID & _
"and [Transaction Type] = 144"

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim DT As New DataTable()
            connection.Open()
            dataadapter.Fill(DT)
            connection.Close()
            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function CheckItemExists(ItemCode As String) As Boolean
        Try
            Dim sSQL As String = "Select Top 1 ItemCode  From ItemMaster Where [ItemCode] = '" & ItemCode & "' and Co_ID = " & My.Settings.Setting_CompanyID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim OItem As Object = cmd.ExecuteScalar()
            connection.Close()

            If OItem Is Nothing Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            MsgBox("There was an error checking item:" & ex.Message)
        End Try
    End Function
    Function InsertNewItemMaster(ItemCode As String, ItemName As String, IntegrationCode As String, _
                           Account As String, HasVAT As Boolean) As Boolean
        Try
            Dim sSQL As String

            sSQL = "Insert Into ItemMaster ([ItemCode],[ItemName],[IntegrationCode],[Account],[HasVAT],[Co_ID]) Values (" & _
                "'" & ItemCode & "'," & _
                "'" & ItemName & "'," & _
                "'" & IntegrationCode & "'," & _
                "'" & Account & "'," & _
                "'" & HasVAT & "'," & _
                "'" & My.Settings.Setting_CompanyID & "')"

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Item Added Complete")
            Return True
        Catch ex As Exception
            MsgBox("There was an error inserting new item:" & vbNewLine & ex.Message)
            Return False
        End Try
    End Function
    Function EditItemMaster(ID As Long, ItemCode As String, ItemName As String, _
                       Account As String, HasVAT As Boolean) As Boolean
        Try
            Dim sSQL As String

            sSQL = "Update ItemMaster Set " & _
                "[ItemCode] ='" & ItemCode & "'," & _
                "[ItemName] ='" & ItemName & "'," & _
                "[Account] = '" & Account & "'," & _
                "[HasVAT] = '" & HasVAT & "'" & _
                " Where ID = " & ID

            Dim conn As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Item Edit Complete")
            Return True
        Catch ex As Exception
            MsgBox("There was an error editing item:" & vbNewLine & ex.Message)
            Return False
        End Try
    End Function
    Function InvoiceGRNData(StartDate As String, EndDate As String) As DataTable
        Try

            Dim sSQL As String = "Select CONVERT( VARCHAR, InvDate, 113) as [Inv Date],POS_GRN_Detail.SupplierName,Suppliers.SupplierID,InvoiceNo " & _
                "FROM POS_GRN_Detail " & _
                "Left Join [Suppliers] On [Suppliers].IntegrationCode = POS_GRN_Detail.SupplierName and [Suppliers].Co_ID = POS_GRN_Detail.Co_ID " & _
                "Where  InvDate >= '" & StartDate & "' and InvDate <='" & EndDate & "' and POS_GRN_Detail.Co_ID =" & My.Settings.Setting_CompanyID
            Dim builder As New SqlConnectionStringBuilder
            builder.ConnectionString = My.Settings.CS_Setting
            builder.ConnectTimeout = 900
            builder.Pooling = False

            Dim connection As New SqlConnection(builder.ToString())
            connection.Open()
            Dim SQL_Command As New SqlCommand()
            SQL_Command.CommandTimeout = 900
            SQL_Command.Connection = connection
            SQL_Command.CommandText = sSQL

            Dim myreader As SqlDataReader = SQL_Command.ExecuteReader()

            Dim DT As New DataTable()

            DT.Load(myreader)
            connection.Close()

            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function ItemList(Search) As DataTable
        Try

            Dim sSQL As String = "Select [ID],[ItemCode],[ItemName],[IntegrationCode],[HasVAT],[Co_ID]," & _
                "[Account] FROM ItemMaster Where ItemName like '%" & Search & "%' and " & _
                "Co_ID = " & My.Settings.Setting_CompanyID
            Dim builder As New SqlConnectionStringBuilder
            builder.ConnectionString = My.Settings.CS_Setting
            builder.ConnectTimeout = 900
            builder.Pooling = False

            Dim connection As New SqlConnection(builder.ToString())
            connection.Open()
            Dim SQL_Command As New SqlCommand()
            SQL_Command.CommandTimeout = 900
            SQL_Command.Connection = connection
            SQL_Command.CommandText = sSQL

            Dim myreader As SqlDataReader = SQL_Command.ExecuteReader()

            Dim DT As New DataTable()

            DT.Load(myreader)
            connection.Close()

            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function InvoiceExsists(SupplierID As String, Reference As String) As Boolean
        Try
            Dim sSQL As String = "Select Top 1 TransactionID  From Transactions " & _
                "Where [SupplierID] = " & SupplierID & " and Reference = '" & Reference & "'" & _
                "And [Void] = 0 and Co_ID = " & My.Settings.Setting_CompanyID

            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim OItem As Object = cmd.ExecuteScalar()
            connection.Close()

            If OItem Is Nothing Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            MsgBox("There was an error checking Invoice:" & ex.Message)
        End Try
    End Function
    Function Get_InvoiceGRNLines(SupplierName As String, InvoiceNo As String) As DataTable
        Try
            Dim sSQL As String = "Select InvDate,POS_GRN_Detail.SupplierName,InvoiceNo,POS_GRN_Detail.ItemName," & _
            "Accounting.[Segment 1 Desc],Accounting.[Segment 2 Desc],Accounting.[Segment 3 Desc]," & _
            "Case when HasVat = 'True' then TaxTypes.[TaxDesc] else ZeroT.[TaxDesc]  End as TaxDesc ," & _
            "Case when HasVat = 'True' then TaxTypes.[TaxPerc] else ZeroT.[TaxPerc]  End as TaxPerc ," & _
            "Total as [AmountExcl]," & _
            "Case When HasVat = 1 then Round((Total * (1 + (TaxTypes.[TaxPerc]/100)))-Total,2) else 0 end as [AmountTax],CostGroup " & _
            "FROM POS_GRN_Detail Left Join [Suppliers] On [Suppliers].IntegrationCode = POS_GRN_Detail.SupplierName and [Suppliers].Co_ID = POS_GRN_Detail.Co_ID " & _
            "Left Join [ItemMaster] on ItemMaster.IntegrationCode = POS_GRN_Detail.CostGroup and ItemMaster.Co_ID = POS_GRN_Detail.Co_ID " & _
            "Left Join [Accounting] on ItemMaster.Account = [Accounting].[Segment 4] and ItemMaster.Co_ID = [Accounting].Co_ID " & _
            "left join TaxTypes On TaxTypes.ID = TaxTypes.ID and TaxTypes.[Default] = 1 and TaxTypes.[Co_ID] = " & My.Settings.Setting_CompanyID & _
            "left join TaxTypes as ZeroT On ZeroT.ID = ZeroT.ID and ZeroT.[TaxPerc] = 0 and ZeroT.[Co_ID] = " & My.Settings.Setting_CompanyID & _
            "Where  InvoiceNo = '" & InvoiceNo & "' and POS_GRN_Detail.SupplierName = '" & SupplierName & "' and POS_GRN_Detail.Co_ID =" & My.Settings.Setting_CompanyID
            Dim builder As New SqlConnectionStringBuilder
            builder.ConnectionString = My.Settings.CS_Setting
            builder.ConnectTimeout = 900
            builder.Pooling = False

            Dim connection As New SqlConnection(builder.ToString())
            connection.Open()
            Dim SQL_Command As New SqlCommand()
            SQL_Command.CommandTimeout = 900
            SQL_Command.Connection = connection
            SQL_Command.CommandText = sSQL

            Dim myreader As SqlDataReader = SQL_Command.ExecuteReader()

            Dim DT As New DataTable()

            DT.Load(myreader)
            connection.Close()

            Return DT
        Catch ex As Exception
            MsgBox("An error occured with details :" & ex.Message, MsgBoxStyle.Critical, "ERROR")
            Return Nothing
        End Try
    End Function
    Function Get_PaymentAmount_by_TransactionID(TransactionID As Integer) As Double
        Try
            Dim sSQL As String = "Select SUM(Case When Dr_Cr = 'Cr'then -1 * Amount  else amount end) as Amount  From Transactions " & _
                "Where [LinkID] = '" & TransactionID & "' and [Transaction Type] = 1 and [Description Code] = '" & Get_CreditorControlAcc() & "' and Co_ID = " & My.Settings.Setting_CompanyID
            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim cmd As New SqlCommand(sSQL, connection)
            cmd.CommandTimeout = 300
            connection.Open()
            Dim OItem As Object = cmd.ExecuteScalar()
            connection.Close()

            If IsDBNull(OItem) Then
                Return 0
            Else
                Return OItem
            End If

        Catch ex As Exception
            MsgBox("There was an error checking item:" & ex.Message)
        End Try
    End Function
End Module