﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Windows.Forms '*** NOTE - MUST HAVE THIS FOR ADD-INS


Public Class Queries

    Private Sub Button_All_Supplier_Invoices_Click(sender As System.Object, e As System.EventArgs) Handles Button_All_Supplier_Invoices.Click
        Call GetALLInvoiceAmounts()
    End Sub
    Private Sub Queries_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Queries -> Version : " & My.Settings.Setting_Version
        Me.DateTimePicker_From.Value = System.DateTime.Now.AddYears(-2)
    End Sub
    Sub GetALLInvoiceAmounts()

        Try

            Dim sSQL As String
            'sSQL = "SELECT Transactions.ID, Suppliers.SupplierName, Transactions.Reference, Transactions.[Transaction Date], Transactions.Amount, IIf(DCount('Amount','Transactions','[Transaction Type] = " & TransT_Payment & "' & ' And ' & '[DR_CR] = ''Dr''')=0,[Transactions].[Amount],Transactions.Amount-DSum('Amount','Transactions','[Transaction Type] = " & TransT_Payment & "' & ' And ' & '[DR_CR] = ''Dr''')) As Due "
            sSQL = "SELECT Suppliers.SupplierName, [Invoiced]-[Paid] AS Balance, DSum('Amount','Transactions','[Transaction Type] = " & TransT_Invoice & " And SupplierID =' & [Suppliers].[SupplierID] & ' And ' & '[DR_CR] = ''Cr''') AS Invoiced, IIF(DCount('Amount','Transactions','[Transaction Type] = " & TransT_Payment & " And SupplierID =' & [Suppliers].[SupplierID] & ' And ' & '[DR_CR] = ''Dr''')=0,0,DSum('Amount','Transactions','[Transaction Type] = " & TransT_Payment & " And SupplierID =' & [Suppliers].[SupplierID] & ' And ' & '[DR_CR] = ''Dr''')) AS Paid"
            sSQL = sSQL & " FROM Suppliers INNER JOIN Transactions ON Suppliers.SupplierID = Transactions.SupplierID"



            sSQL = sSQL & " GROUP BY Suppliers.SupplierName, Suppliers.SupplierID"

            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Suppliers_table")
            connection.Close()
            Me.DataGridView1.DataSource = ds
            Me.DataGridView1.DataMember = "Suppliers_table"

            Total_Supplier_Boxes()

        Catch ex As Exception
            MsgBox(ex.Message & " 190")
        End Try

    End Sub

    Sub GetALLPaymentAmounts()

        Try

            Dim sSQL As String

            sSQL = "SELECT Suppliers.SupplierName, [Invoiced]-[Paid] AS Balance, DSum('Amount','Transactions','[Transaction Type] = " & TransT_Invoice & " And SupplierID =' & [Suppliers].[SupplierID] & ' And ' & '[DR_CR] = ''Cr''') AS Invoiced, IIF(DCount('Amount','Transactions','[Transaction Type] = " & TransT_Payment & " And SupplierID =' & [Suppliers].[SupplierID] & ' And ' & '[DR_CR] = ''Dr''')=0,0,DSum('Amount','Transactions','[Transaction Type] = " & TransT_Payment & " And SupplierID =' & [Suppliers].[SupplierID] & ' And ' & '[DR_CR] = ''Dr''')) AS Paid"
            sSQL = sSQL & " FROM Suppliers INNER JOIN Transactions ON Suppliers.SupplierID = Transactions.SupplierID"


            sSQL = sSQL & " GROUP BY Suppliers.SupplierName, Suppliers.SupplierID"

            Dim connection As New OleDbConnection(My.Settings.CS_Setting)
            Dim dataadapter As New OleDbDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "Suppliers_table")
            connection.Close()
            Me.DataGridView1.DataSource = ds
            Me.DataGridView1.DataMember = "Suppliers_table"

            Total_Supplier_Boxes()

        Catch ex As Exception
            MsgBox(ex.Message & " 191")
        End Try

    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        GetALLPaymentAmounts()
    End Sub


    Sub Total_Supplier_Boxes()
        Call TotalSuppliersPaidBox()
        Call TotalSuppliersInvBox()
        Call TotalSuppliersBalBox()
    End Sub

    Sub TotalSuppliersBalBox()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            oTotal = oTotal + Me.DataGridView1.Rows(i).Cells(1).Value
        Next
        Me.TextBox_TotBalanced.Text = CStr(oTotal)
    End Sub

    Sub TotalSuppliersInvBox()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            oTotal = oTotal + Me.DataGridView1.Rows(i).Cells(2).Value
        Next
        Me.TextBox_TotInvoiced.Text = CStr(oTotal)
    End Sub

    Sub TotalSuppliersPaidBox()
        On Error Resume Next
        Dim oTotal As Decimal = 0
        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            oTotal = oTotal + Me.DataGridView1.Rows(i).Cells(3).Value
        Next

        Me.TextBox_TotPaid.Text = CStr(oTotal)
    End Sub
End Class