﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSupplierPaymentsV2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSupplierPaymentsV2))
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ComboBox_SupplierStatus = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtBalance = New System.Windows.Forms.TextBox()
        Me.dgvSupplierSum = New System.Windows.Forms.DataGridView()
        Me.dgvPaymentsList = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Reference = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TranDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Action = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.TextBox_Total = New System.Windows.Forms.TextBox()
        Me.btnRemoveRequest = New System.Windows.Forms.Button()
        Me.Button_Delete = New System.Windows.Forms.Button()
        Me.cmbPaymentType = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnUpdatePayments = New System.Windows.Forms.Button()
        Me.dtpPaymentDate = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox_Paid = New System.Windows.Forms.TextBox()
        Me.Paid_Label = New System.Windows.Forms.Label()
        Me.Button_Pay = New System.Windows.Forms.Button()
        Me.Label_PaymentProcessed = New System.Windows.Forms.Label()
        Me.DGV_Transactions = New System.Windows.Forms.DataGridView()
        Me.BatchID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Capture_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fin_Year = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAcc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tax_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accounting = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Void = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DR_CR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contra_Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvPeriods = New System.Windows.Forms.DataGridView()
        Me.dgvSuppliers = New System.Windows.Forms.DataGridView()
        Me.dgvAccounts = New System.Windows.Forms.DataGridView()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btnViewDetail = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TrackBar2 = New System.Windows.Forms.TrackBar()
        Me.txtRequested = New System.Windows.Forms.TextBox()
        Me.chkTotalRequested = New System.Windows.Forms.CheckBox()
        Me.chkSupplierDetail = New System.Windows.Forms.CheckBox()
        Me.btnSuppDetail = New System.Windows.Forms.Button()
        Me.txtReferenceSearch = New System.Windows.Forms.TextBox()
        Me.btnSearchRef = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cmbStatus = New System.Windows.Forms.ComboBox()
        Me.btnFilter = New System.Windows.Forms.Button()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.cmbSupplier = New System.Windows.Forms.ComboBox()
        Me.SupplierBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnViewPaymentsRequests = New System.Windows.Forms.Button()
        Me.txtPaymentsRequests = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnViewCreditNotes = New System.Windows.Forms.Button()
        Me.txtUnapprovedCNotes = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtAsPerSheet = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ckbAllBalances = New System.Windows.Forms.CheckBox()
        Me.rdbSupplier = New System.Windows.Forms.RadioButton()
        Me.rdbBalance = New System.Windows.Forms.RadioButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.DGVResults = New System.Windows.Forms.DataGridView()
        Me.dgvInvoiceList = New System.Windows.Forms.DataGridView()
        Me.PtypeEqualiser = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TB_Pay = New System.Windows.Forms.TabPage()
        Me.TB_Requests = New System.Windows.Forms.TabPage()
        Me.btn_DeselectAll = New System.Windows.Forms.Button()
        Me.Btn_Bank = New System.Windows.Forms.Button()
        Me.Btn_all = New System.Windows.Forms.Button()
        Me.lbl_RequestStatus = New System.Windows.Forms.Label()
        Me.TB_TotalRequests = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DGV_Requests = New System.Windows.Forms.DataGridView()
        Me.Ignor = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Lbl_Status = New System.Windows.Forms.Label()
        Me.lbl_ExcludedPayTypes = New System.Windows.Forms.Label()
        CType(Me.dgvSupplierSum, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPaymentsList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.SupplierBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.DGVResults, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvInvoiceList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TB_Pay.SuspendLayout()
        Me.TB_Requests.SuspendLayout()
        CType(Me.DGV_Requests, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Brush Script MT", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(0, 11)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(133, 37)
        Me.Label10.TabIndex = 75
        Me.Label10.Text = "Magic Box "
        '
        'ComboBox_SupplierStatus
        '
        Me.ComboBox_SupplierStatus.FormattingEnabled = True
        Me.ComboBox_SupplierStatus.Items.AddRange(New Object() {"[ALL]", "UNPAID", "PAID"})
        Me.ComboBox_SupplierStatus.Location = New System.Drawing.Point(227, 166)
        Me.ComboBox_SupplierStatus.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboBox_SupplierStatus.Name = "ComboBox_SupplierStatus"
        Me.ComboBox_SupplierStatus.Size = New System.Drawing.Size(164, 24)
        Me.ComboBox_SupplierStatus.TabIndex = 74
        Me.ComboBox_SupplierStatus.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label6.Location = New System.Drawing.Point(111, 148)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 17)
        Me.Label6.TabIndex = 73
        Me.Label6.Text = "Supplier Status :"
        Me.Label6.Visible = False
        '
        'txtBalance
        '
        Me.txtBalance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtBalance.ForeColor = System.Drawing.Color.DarkBlue
        Me.txtBalance.Location = New System.Drawing.Point(183, 16)
        Me.txtBalance.Margin = New System.Windows.Forms.Padding(4)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(239, 23)
        Me.txtBalance.TabIndex = 0
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvSupplierSum
        '
        Me.dgvSupplierSum.AllowUserToAddRows = False
        Me.dgvSupplierSum.AllowUserToDeleteRows = False
        Me.dgvSupplierSum.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSupplierSum.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSupplierSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSupplierSum.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvSupplierSum.Location = New System.Drawing.Point(7, 105)
        Me.dgvSupplierSum.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvSupplierSum.MultiSelect = False
        Me.dgvSupplierSum.Name = "dgvSupplierSum"
        Me.dgvSupplierSum.RowTemplate.Height = 24
        Me.dgvSupplierSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSupplierSum.Size = New System.Drawing.Size(431, 696)
        Me.dgvSupplierSum.TabIndex = 1
        '
        'dgvPaymentsList
        '
        Me.dgvPaymentsList.AllowDrop = True
        Me.dgvPaymentsList.AllowUserToAddRows = False
        Me.dgvPaymentsList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvPaymentsList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPaymentsList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvPaymentsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPaymentsList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.SupplierName, Me.Reference, Me.Amount, Me.TransactionDate, Me.PaymentType, Me.TranDate, Me.Action})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPaymentsList.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvPaymentsList.Location = New System.Drawing.Point(3, 3)
        Me.dgvPaymentsList.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvPaymentsList.Name = "dgvPaymentsList"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPaymentsList.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvPaymentsList.RowTemplate.Height = 24
        Me.dgvPaymentsList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPaymentsList.Size = New System.Drawing.Size(1014, 367)
        Me.dgvPaymentsList.TabIndex = 6
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'SupplierName
        '
        Me.SupplierName.HeaderText = "SupplierName"
        Me.SupplierName.Name = "SupplierName"
        Me.SupplierName.ReadOnly = True
        '
        'Reference
        '
        Me.Reference.HeaderText = "Reference"
        Me.Reference.Name = "Reference"
        Me.Reference.ReadOnly = True
        '
        'Amount
        '
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Blue
        Me.Amount.DefaultCellStyle = DataGridViewCellStyle4
        Me.Amount.HeaderText = "Payment Amount"
        Me.Amount.Name = "Amount"
        '
        'TransactionDate
        '
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Blue
        Me.TransactionDate.DefaultCellStyle = DataGridViewCellStyle5
        Me.TransactionDate.HeaderText = "Date of Payment"
        Me.TransactionDate.Name = "TransactionDate"
        '
        'PaymentType
        '
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.PaymentType.DefaultCellStyle = DataGridViewCellStyle6
        Me.PaymentType.HeaderText = "Payment Type"
        Me.PaymentType.Name = "PaymentType"
        Me.PaymentType.ReadOnly = True
        Me.PaymentType.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'TranDate
        '
        Me.TranDate.HeaderText = "TransactionDate"
        Me.TranDate.Name = "TranDate"
        Me.TranDate.Visible = False
        '
        'Action
        '
        Me.Action.HeaderText = "Action"
        Me.Action.Name = "Action"
        Me.Action.Text = "Remove"
        Me.Action.UseColumnTextForButtonValue = True
        '
        'TextBox_Total
        '
        Me.TextBox_Total.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_Total.Enabled = False
        Me.TextBox_Total.Location = New System.Drawing.Point(722, 374)
        Me.TextBox_Total.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox_Total.Name = "TextBox_Total"
        Me.TextBox_Total.Size = New System.Drawing.Size(175, 22)
        Me.TextBox_Total.TabIndex = 58
        '
        'btnRemoveRequest
        '
        Me.btnRemoveRequest.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveRequest.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnRemoveRequest.Location = New System.Drawing.Point(664, 365)
        Me.btnRemoveRequest.Margin = New System.Windows.Forms.Padding(4)
        Me.btnRemoveRequest.Name = "btnRemoveRequest"
        Me.btnRemoveRequest.Size = New System.Drawing.Size(144, 28)
        Me.btnRemoveRequest.TabIndex = 5
        Me.btnRemoveRequest.Tag = "S_Requests_Remove"
        Me.btnRemoveRequest.Text = "R&emove Request"
        Me.btnRemoveRequest.UseVisualStyleBackColor = True
        '
        'Button_Delete
        '
        Me.Button_Delete.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_Delete.ForeColor = System.Drawing.Color.DarkBlue
        Me.Button_Delete.Location = New System.Drawing.Point(4, 199)
        Me.Button_Delete.Margin = New System.Windows.Forms.Padding(4)
        Me.Button_Delete.Name = "Button_Delete"
        Me.Button_Delete.Size = New System.Drawing.Size(207, 200)
        Me.Button_Delete.TabIndex = 6
        Me.Button_Delete.Text = "Remove Loaded Payments"
        Me.Button_Delete.UseVisualStyleBackColor = True
        Me.Button_Delete.Visible = False
        '
        'cmbPaymentType
        '
        Me.cmbPaymentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbPaymentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbPaymentType.FormattingEnabled = True
        Me.cmbPaymentType.Location = New System.Drawing.Point(129, 23)
        Me.cmbPaymentType.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbPaymentType.Name = "cmbPaymentType"
        Me.cmbPaymentType.Size = New System.Drawing.Size(148, 24)
        Me.cmbPaymentType.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label7.Location = New System.Drawing.Point(8, 26)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 17)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Payment Type :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.btnUpdatePayments)
        Me.GroupBox1.Controls.Add(Me.dtpPaymentDate)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.cmbPaymentType)
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkBlue
        Me.GroupBox1.Location = New System.Drawing.Point(446, 448)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1029, 55)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Payment Details"
        '
        'btnUpdatePayments
        '
        Me.btnUpdatePayments.Location = New System.Drawing.Point(555, 20)
        Me.btnUpdatePayments.Margin = New System.Windows.Forms.Padding(4)
        Me.btnUpdatePayments.Name = "btnUpdatePayments"
        Me.btnUpdatePayments.Size = New System.Drawing.Size(168, 31)
        Me.btnUpdatePayments.TabIndex = 2
        Me.btnUpdatePayments.Text = "&Update Payments"
        Me.btnUpdatePayments.UseVisualStyleBackColor = True
        '
        'dtpPaymentDate
        '
        Me.dtpPaymentDate.CustomFormat = "dd MMM yyyy"
        Me.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPaymentDate.Location = New System.Drawing.Point(401, 23)
        Me.dtpPaymentDate.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpPaymentDate.Name = "dtpPaymentDate"
        Me.dtpPaymentDate.Size = New System.Drawing.Size(144, 22)
        Me.dtpPaymentDate.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(287, 28)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(105, 17)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Payment Date :"
        '
        'TextBox_Paid
        '
        Me.TextBox_Paid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_Paid.Location = New System.Drawing.Point(1320, 426)
        Me.TextBox_Paid.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox_Paid.Name = "TextBox_Paid"
        Me.TextBox_Paid.Size = New System.Drawing.Size(155, 22)
        Me.TextBox_Paid.TabIndex = 65
        Me.TextBox_Paid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Paid_Label
        '
        Me.Paid_Label.AutoSize = True
        Me.Paid_Label.ForeColor = System.Drawing.Color.MediumBlue
        Me.Paid_Label.Location = New System.Drawing.Point(900, 57)
        Me.Paid_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Paid_Label.Name = "Paid_Label"
        Me.Paid_Label.Size = New System.Drawing.Size(0, 17)
        Me.Paid_Label.TabIndex = 71
        Me.Paid_Label.Visible = False
        '
        'Button_Pay
        '
        Me.Button_Pay.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_Pay.ForeColor = System.Drawing.Color.DarkBlue
        Me.Button_Pay.Location = New System.Drawing.Point(901, 371)
        Me.Button_Pay.Margin = New System.Windows.Forms.Padding(4)
        Me.Button_Pay.Name = "Button_Pay"
        Me.Button_Pay.Size = New System.Drawing.Size(115, 27)
        Me.Button_Pay.TabIndex = 10
        Me.Button_Pay.Tag = "S_Payments_Pay"
        Me.Button_Pay.Text = "&Pay"
        Me.Button_Pay.UseVisualStyleBackColor = True
        '
        'Label_PaymentProcessed
        '
        Me.Label_PaymentProcessed.AutoSize = True
        Me.Label_PaymentProcessed.ForeColor = System.Drawing.Color.Blue
        Me.Label_PaymentProcessed.Location = New System.Drawing.Point(919, 722)
        Me.Label_PaymentProcessed.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label_PaymentProcessed.Name = "Label_PaymentProcessed"
        Me.Label_PaymentProcessed.Size = New System.Drawing.Size(0, 17)
        Me.Label_PaymentProcessed.TabIndex = 77
        Me.Label_PaymentProcessed.Visible = False
        '
        'DGV_Transactions
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Transactions.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.DGV_Transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Transactions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BatchID, Me.TransactionID, Me.LinkID, Me.Capture_Date, Me.Transaction_Date, Me.PPeriod, Me.Fin_Year, Me.GDC, Me.DataGridViewTextBoxColumn1, Me.Description, Me.AccNumber, Me.LinkAcc, Me.DataGridViewTextBoxColumn2, Me.TaxType, Me.Tax_Amount, Me.UserID, Me.SupplierID, Me.EmployeeID, Me.Description2, Me.Description3, Me.Description4, Me.Description5, Me.Posted, Me.Accounting, Me.Void, Me.Transaction_Type, Me.DR_CR, Me.Contra_Account, Me.Description_Code, Me.DocType, Me.DataGridViewTextBoxColumn3, Me.Info, Me.Info2, Me.Info3})
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_Transactions.DefaultCellStyle = DataGridViewCellStyle10
        Me.DGV_Transactions.Location = New System.Drawing.Point(572, 770)
        Me.DGV_Transactions.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_Transactions.Name = "DGV_Transactions"
        Me.DGV_Transactions.RowTemplate.Height = 24
        Me.DGV_Transactions.Size = New System.Drawing.Size(13, 12)
        Me.DGV_Transactions.TabIndex = 97
        Me.DGV_Transactions.Visible = False
        '
        'BatchID
        '
        Me.BatchID.HeaderText = "BatchID"
        Me.BatchID.Name = "BatchID"
        Me.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TransactionID
        '
        Me.TransactionID.HeaderText = "TransactionID"
        Me.TransactionID.Name = "TransactionID"
        '
        'LinkID
        '
        Me.LinkID.HeaderText = "LinkID"
        Me.LinkID.Name = "LinkID"
        '
        'Capture_Date
        '
        Me.Capture_Date.HeaderText = "Capture_Date"
        Me.Capture_Date.Name = "Capture_Date"
        '
        'Transaction_Date
        '
        Me.Transaction_Date.HeaderText = "Transaction_Date"
        Me.Transaction_Date.Name = "Transaction_Date"
        '
        'PPeriod
        '
        Me.PPeriod.HeaderText = "PPeriod"
        Me.PPeriod.Name = "PPeriod"
        '
        'Fin_Year
        '
        Me.Fin_Year.HeaderText = "Fin_Year"
        Me.Fin_Year.Name = "Fin_Year"
        '
        'GDC
        '
        Me.GDC.HeaderText = "GDC"
        Me.GDC.Name = "GDC"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Reference"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        '
        'AccNumber
        '
        Me.AccNumber.HeaderText = "AccNumber"
        Me.AccNumber.Name = "AccNumber"
        '
        'LinkAcc
        '
        Me.LinkAcc.HeaderText = "LinkAcc"
        Me.LinkAcc.Name = "LinkAcc"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'TaxType
        '
        Me.TaxType.HeaderText = "TaxType"
        Me.TaxType.Name = "TaxType"
        '
        'Tax_Amount
        '
        Me.Tax_Amount.HeaderText = "Tax_Amount"
        Me.Tax_Amount.Name = "Tax_Amount"
        '
        'UserID
        '
        Me.UserID.HeaderText = "UserID"
        Me.UserID.Name = "UserID"
        '
        'SupplierID
        '
        Me.SupplierID.HeaderText = "SupplierID"
        Me.SupplierID.Name = "SupplierID"
        '
        'EmployeeID
        '
        Me.EmployeeID.HeaderText = "EmployeeID"
        Me.EmployeeID.Name = "EmployeeID"
        '
        'Description2
        '
        Me.Description2.HeaderText = "Description2"
        Me.Description2.Name = "Description2"
        '
        'Description3
        '
        Me.Description3.HeaderText = "Description3"
        Me.Description3.Name = "Description3"
        '
        'Description4
        '
        Me.Description4.HeaderText = "Description4"
        Me.Description4.Name = "Description4"
        '
        'Description5
        '
        Me.Description5.HeaderText = "Description5"
        Me.Description5.Name = "Description5"
        '
        'Posted
        '
        Me.Posted.HeaderText = "Posted"
        Me.Posted.Name = "Posted"
        '
        'Accounting
        '
        Me.Accounting.HeaderText = "Accounting"
        Me.Accounting.Name = "Accounting"
        '
        'Void
        '
        Me.Void.HeaderText = "Void"
        Me.Void.Name = "Void"
        '
        'Transaction_Type
        '
        Me.Transaction_Type.HeaderText = "Transaction_Type"
        Me.Transaction_Type.Name = "Transaction_Type"
        '
        'DR_CR
        '
        Me.DR_CR.HeaderText = "DR_CR"
        Me.DR_CR.Name = "DR_CR"
        '
        'Contra_Account
        '
        Me.Contra_Account.HeaderText = "Contra_Account"
        Me.Contra_Account.Name = "Contra_Account"
        '
        'Description_Code
        '
        Me.Description_Code.HeaderText = "Description_Code"
        Me.Description_Code.Name = "Description_Code"
        '
        'DocType
        '
        Me.DocType.HeaderText = "DocType"
        Me.DocType.Name = "DocType"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "SupplierName"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Info
        '
        Me.Info.HeaderText = "Info"
        Me.Info.Name = "Info"
        '
        'Info2
        '
        Me.Info2.HeaderText = "Info2"
        Me.Info2.Name = "Info2"
        '
        'Info3
        '
        Me.Info3.HeaderText = "Info3"
        Me.Info3.Name = "Info3"
        '
        'dgvPeriods
        '
        Me.dgvPeriods.BackgroundColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPeriods.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvPeriods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPeriods.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvPeriods.Location = New System.Drawing.Point(56, 151)
        Me.dgvPeriods.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvPeriods.Name = "dgvPeriods"
        Me.dgvPeriods.RowTemplate.Height = 24
        Me.dgvPeriods.Size = New System.Drawing.Size(53, 41)
        Me.dgvPeriods.TabIndex = 120
        Me.dgvPeriods.Visible = False
        '
        'dgvSuppliers
        '
        Me.dgvSuppliers.BackgroundColor = System.Drawing.Color.MidnightBlue
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSuppliers.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvSuppliers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSuppliers.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgvSuppliers.Location = New System.Drawing.Point(207, 151)
        Me.dgvSuppliers.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvSuppliers.Name = "dgvSuppliers"
        Me.dgvSuppliers.RowTemplate.Height = 24
        Me.dgvSuppliers.Size = New System.Drawing.Size(53, 41)
        Me.dgvSuppliers.TabIndex = 119
        Me.dgvSuppliers.Visible = False
        '
        'dgvAccounts
        '
        Me.dgvAccounts.BackgroundColor = System.Drawing.Color.Teal
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAccounts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAccounts.DefaultCellStyle = DataGridViewCellStyle16
        Me.dgvAccounts.Location = New System.Drawing.Point(148, 138)
        Me.dgvAccounts.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvAccounts.Name = "dgvAccounts"
        Me.dgvAccounts.RowTemplate.Height = 24
        Me.dgvAccounts.Size = New System.Drawing.Size(53, 41)
        Me.dgvAccounts.TabIndex = 118
        Me.dgvAccounts.Visible = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(7, 942)
        Me.ProgressBar1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ProgressBar1.Maximum = 200
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(194, 21)
        Me.ProgressBar1.TabIndex = 123
        Me.ProgressBar1.Visible = False
        '
        'TrackBar1
        '
        Me.TrackBar1.BackColor = System.Drawing.SystemColors.Control
        Me.TrackBar1.Location = New System.Drawing.Point(100, 142)
        Me.TrackBar1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TrackBar1.Maximum = 6
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TrackBar1.Size = New System.Drawing.Size(84, 56)
        Me.TrackBar1.TabIndex = 124
        Me.TrackBar1.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.SlateGray
        Me.Label9.Location = New System.Drawing.Point(83, 114)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(19, 13)
        Me.Label9.TabIndex = 126
        Me.Label9.Text = "14"
        Me.Label9.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SlateGray
        Me.Label13.Location = New System.Drawing.Point(135, 126)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(19, 13)
        Me.Label13.TabIndex = 129
        Me.Label13.Text = "30"
        Me.Label13.Visible = False
        '
        'btnViewDetail
        '
        Me.btnViewDetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewDetail.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnViewDetail.Location = New System.Drawing.Point(1193, 423)
        Me.btnViewDetail.Margin = New System.Windows.Forms.Padding(4)
        Me.btnViewDetail.Name = "btnViewDetail"
        Me.btnViewDetail.Size = New System.Drawing.Size(119, 28)
        Me.btnViewDetail.TabIndex = 7
        Me.btnViewDetail.Text = "&Invoice Detail"
        Me.btnViewDetail.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.SlateGray
        Me.Label14.Location = New System.Drawing.Point(121, 114)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 13)
        Me.Label14.TabIndex = 131
        Me.Label14.Text = "60"
        Me.Label14.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.SlateGray
        Me.Label15.Location = New System.Drawing.Point(68, 114)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(13, 13)
        Me.Label15.TabIndex = 132
        Me.Label15.Text = "0"
        Me.Label15.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.SlateGray
        Me.Label8.Location = New System.Drawing.Point(159, 114)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(13, 13)
        Me.Label8.TabIndex = 137
        Me.Label8.Text = "0"
        Me.Label8.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.SlateGray
        Me.Label11.Location = New System.Drawing.Point(289, 142)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 13)
        Me.Label11.TabIndex = 136
        Me.Label11.Text = "60"
        Me.Label11.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.SlateGray
        Me.Label12.Location = New System.Drawing.Point(264, 138)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(19, 13)
        Me.Label12.TabIndex = 135
        Me.Label12.Text = "30"
        Me.Label12.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.SlateGray
        Me.Label16.Location = New System.Drawing.Point(204, 126)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(19, 13)
        Me.Label16.TabIndex = 134
        Me.Label16.Text = "14"
        Me.Label16.Visible = False
        '
        'TrackBar2
        '
        Me.TrackBar2.BackColor = System.Drawing.SystemColors.Control
        Me.TrackBar2.Location = New System.Drawing.Point(116, 151)
        Me.TrackBar2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TrackBar2.Maximum = 6
        Me.TrackBar2.Name = "TrackBar2"
        Me.TrackBar2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TrackBar2.Size = New System.Drawing.Size(84, 56)
        Me.TrackBar2.TabIndex = 133
        Me.TrackBar2.Visible = False
        '
        'txtRequested
        '
        Me.txtRequested.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRequested.Enabled = False
        Me.txtRequested.Location = New System.Drawing.Point(894, 940)
        Me.txtRequested.Margin = New System.Windows.Forms.Padding(4)
        Me.txtRequested.Name = "txtRequested"
        Me.txtRequested.Size = New System.Drawing.Size(175, 22)
        Me.txtRequested.TabIndex = 140
        Me.txtRequested.Visible = False
        '
        'chkTotalRequested
        '
        Me.chkTotalRequested.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkTotalRequested.AutoSize = True
        Me.chkTotalRequested.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTotalRequested.ForeColor = System.Drawing.Color.Black
        Me.chkTotalRequested.Location = New System.Drawing.Point(445, 944)
        Me.chkTotalRequested.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTotalRequested.Name = "chkTotalRequested"
        Me.chkTotalRequested.Size = New System.Drawing.Size(119, 19)
        Me.chkTotalRequested.TabIndex = 141
        Me.chkTotalRequested.Text = "Total Requested"
        Me.chkTotalRequested.UseVisualStyleBackColor = True
        Me.chkTotalRequested.Visible = False
        '
        'chkSupplierDetail
        '
        Me.chkSupplierDetail.AutoSize = True
        Me.chkSupplierDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSupplierDetail.ForeColor = System.Drawing.Color.DarkGray
        Me.chkSupplierDetail.Location = New System.Drawing.Point(7, 148)
        Me.chkSupplierDetail.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSupplierDetail.Name = "chkSupplierDetail"
        Me.chkSupplierDetail.Size = New System.Drawing.Size(110, 19)
        Me.chkSupplierDetail.TabIndex = 143
        Me.chkSupplierDetail.Text = "Supplier Detail"
        Me.chkSupplierDetail.UseVisualStyleBackColor = True
        Me.chkSupplierDetail.Visible = False
        '
        'btnSuppDetail
        '
        Me.btnSuppDetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSuppDetail.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnSuppDetail.Location = New System.Drawing.Point(1053, 423)
        Me.btnSuppDetail.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSuppDetail.Name = "btnSuppDetail"
        Me.btnSuppDetail.Size = New System.Drawing.Size(132, 28)
        Me.btnSuppDetail.TabIndex = 8
        Me.btnSuppDetail.Text = "&Supplier Detail"
        Me.btnSuppDetail.UseVisualStyleBackColor = True
        '
        'txtReferenceSearch
        '
        Me.txtReferenceSearch.Location = New System.Drawing.Point(421, 17)
        Me.txtReferenceSearch.Margin = New System.Windows.Forms.Padding(4)
        Me.txtReferenceSearch.Name = "txtReferenceSearch"
        Me.txtReferenceSearch.Size = New System.Drawing.Size(291, 22)
        Me.txtReferenceSearch.TabIndex = 0
        '
        'btnSearchRef
        '
        Me.btnSearchRef.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.btnSearchRef.Location = New System.Drawing.Point(129, 15)
        Me.btnSearchRef.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSearchRef.Name = "btnSearchRef"
        Me.btnSearchRef.Size = New System.Drawing.Size(264, 28)
        Me.btnSearchRef.TabIndex = 1
        Me.btnSearchRef.Text = "&Search Inv No"
        Me.btnSearchRef.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(4, 20)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(103, 17)
        Me.Label17.TabIndex = 147
        Me.Label17.Text = "Total Balance :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.txtReferenceSearch)
        Me.GroupBox2.Controls.Add(Me.btnSearchRef)
        Me.GroupBox2.ForeColor = System.Drawing.Color.DarkBlue
        Me.GroupBox2.Location = New System.Drawing.Point(445, 6)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(1037, 49)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Search by invoice"
        Me.GroupBox2.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.GroupBox6)
        Me.GroupBox3.Controls.Add(Me.btnFilter)
        Me.GroupBox3.Controls.Add(Me.dtpTo)
        Me.GroupBox3.Controls.Add(Me.cmbSupplier)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.dtpFrom)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.ForeColor = System.Drawing.Color.DarkBlue
        Me.GroupBox3.Location = New System.Drawing.Point(446, 63)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(1037, 102)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Filter options"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.cmbStatus)
        Me.GroupBox6.ForeColor = System.Drawing.Color.DarkBlue
        Me.GroupBox6.Location = New System.Drawing.Point(421, 10)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox6.Size = New System.Drawing.Size(301, 52)
        Me.GroupBox6.TabIndex = 157
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Invoice Status"
        '
        'cmbStatus
        '
        Me.cmbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbStatus.FormattingEnabled = True
        Me.cmbStatus.Items.AddRange(New Object() {"[ALL]", "UNPAID", "PAID"})
        Me.cmbStatus.Location = New System.Drawing.Point(8, 18)
        Me.cmbStatus.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbStatus.Name = "cmbStatus"
        Me.cmbStatus.Size = New System.Drawing.Size(283, 24)
        Me.cmbStatus.TabIndex = 0
        '
        'btnFilter
        '
        Me.btnFilter.Location = New System.Drawing.Point(580, 63)
        Me.btnFilter.Margin = New System.Windows.Forms.Padding(4)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(143, 31)
        Me.btnFilter.TabIndex = 3
        Me.btnFilter.Text = "&Filter"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'dtpTo
        '
        Me.dtpTo.CustomFormat = "dd MMM yyyy"
        Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTo.Location = New System.Drawing.Point(421, 66)
        Me.dtpTo.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(149, 22)
        Me.dtpTo.TabIndex = 2
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.DataSource = Me.SupplierBindingSource
        Me.cmbSupplier.DisplayMember = "SupplierName"
        Me.cmbSupplier.FormattingEnabled = True
        Me.cmbSupplier.Location = New System.Drawing.Point(129, 26)
        Me.cmbSupplier.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(263, 24)
        Me.cmbSupplier.TabIndex = 0
        Me.cmbSupplier.ValueMember = "SupplierID"
        '
        'SupplierBindingSource
        '
        Me.SupplierBindingSource.DataSource = GetType(MB.Supplier)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(12, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 17)
        Me.Label1.TabIndex = 153
        Me.Label1.Text = "Supplier name :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(359, 70)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 17)
        Me.Label5.TabIndex = 156
        Me.Label5.Text = "To :"
        '
        'dtpFrom
        '
        Me.dtpFrom.Checked = False
        Me.dtpFrom.CustomFormat = "dd MMM yyyy"
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFrom.Location = New System.Drawing.Point(189, 66)
        Me.dtpFrom.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(143, 22)
        Me.dtpFrom.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(125, 70)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 17)
        Me.Label3.TabIndex = 155
        Me.Label3.Text = "From :"
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox4.Controls.Add(Me.btnViewPaymentsRequests)
        Me.GroupBox4.Controls.Add(Me.txtPaymentsRequests)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.btnViewCreditNotes)
        Me.GroupBox4.Controls.Add(Me.txtUnapprovedCNotes)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.txtAsPerSheet)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.txtBalance)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.White
        Me.GroupBox4.Location = New System.Drawing.Point(7, 808)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Size = New System.Drawing.Size(431, 127)
        Me.GroupBox4.TabIndex = 9
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Balances"
        '
        'btnViewPaymentsRequests
        '
        Me.btnViewPaymentsRequests.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewPaymentsRequests.BackColor = System.Drawing.Color.Lime
        Me.btnViewPaymentsRequests.ForeColor = System.Drawing.Color.Red
        Me.btnViewPaymentsRequests.Location = New System.Drawing.Point(353, 94)
        Me.btnViewPaymentsRequests.Margin = New System.Windows.Forms.Padding(4)
        Me.btnViewPaymentsRequests.Name = "btnViewPaymentsRequests"
        Me.btnViewPaymentsRequests.Size = New System.Drawing.Size(69, 27)
        Me.btnViewPaymentsRequests.TabIndex = 1
        Me.btnViewPaymentsRequests.Text = "View"
        Me.btnViewPaymentsRequests.UseVisualStyleBackColor = False
        '
        'txtPaymentsRequests
        '
        Me.txtPaymentsRequests.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtPaymentsRequests.ForeColor = System.Drawing.Color.Red
        Me.txtPaymentsRequests.Location = New System.Drawing.Point(183, 94)
        Me.txtPaymentsRequests.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPaymentsRequests.Name = "txtPaymentsRequests"
        Me.txtPaymentsRequests.ReadOnly = True
        Me.txtPaymentsRequests.Size = New System.Drawing.Size(165, 23)
        Me.txtPaymentsRequests.TabIndex = 3
        Me.txtPaymentsRequests.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(4, 101)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(137, 17)
        Me.Label19.TabIndex = 154
        Me.Label19.Text = "Payments requests :"
        '
        'btnViewCreditNotes
        '
        Me.btnViewCreditNotes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewCreditNotes.BackColor = System.Drawing.Color.Lime
        Me.btnViewCreditNotes.ForeColor = System.Drawing.Color.Red
        Me.btnViewCreditNotes.Location = New System.Drawing.Point(353, 68)
        Me.btnViewCreditNotes.Margin = New System.Windows.Forms.Padding(4)
        Me.btnViewCreditNotes.Name = "btnViewCreditNotes"
        Me.btnViewCreditNotes.Size = New System.Drawing.Size(69, 27)
        Me.btnViewCreditNotes.TabIndex = 0
        Me.btnViewCreditNotes.Text = "View"
        Me.btnViewCreditNotes.UseVisualStyleBackColor = False
        '
        'txtUnapprovedCNotes
        '
        Me.txtUnapprovedCNotes.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtUnapprovedCNotes.ForeColor = System.Drawing.Color.Red
        Me.txtUnapprovedCNotes.Location = New System.Drawing.Point(183, 68)
        Me.txtUnapprovedCNotes.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUnapprovedCNotes.Name = "txtUnapprovedCNotes"
        Me.txtUnapprovedCNotes.ReadOnly = True
        Me.txtUnapprovedCNotes.Size = New System.Drawing.Size(165, 23)
        Me.txtUnapprovedCNotes.TabIndex = 2
        Me.txtUnapprovedCNotes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(4, 73)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(172, 17)
        Me.Label18.TabIndex = 151
        Me.Label18.Text = "Unapproved credit notes :"
        '
        'txtAsPerSheet
        '
        Me.txtAsPerSheet.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtAsPerSheet.ForeColor = System.Drawing.Color.DarkBlue
        Me.txtAsPerSheet.Location = New System.Drawing.Point(183, 42)
        Me.txtAsPerSheet.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAsPerSheet.Name = "txtAsPerSheet"
        Me.txtAsPerSheet.ReadOnly = True
        Me.txtAsPerSheet.Size = New System.Drawing.Size(239, 23)
        Me.txtAsPerSheet.TabIndex = 1
        Me.txtAsPerSheet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(4, 47)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 17)
        Me.Label2.TabIndex = 149
        Me.Label2.Text = "As per balance sheet :"
        '
        'ckbAllBalances
        '
        Me.ckbAllBalances.AutoSize = True
        Me.ckbAllBalances.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.ckbAllBalances.ForeColor = System.Drawing.Color.DarkBlue
        Me.ckbAllBalances.Location = New System.Drawing.Point(7, 71)
        Me.ckbAllBalances.Margin = New System.Windows.Forms.Padding(4)
        Me.ckbAllBalances.Name = "ckbAllBalances"
        Me.ckbAllBalances.Size = New System.Drawing.Size(143, 21)
        Me.ckbAllBalances.TabIndex = 0
        Me.ckbAllBalances.Text = "Show all balances"
        Me.ckbAllBalances.UseVisualStyleBackColor = True
        '
        'rdbSupplier
        '
        Me.rdbSupplier.AutoSize = True
        Me.rdbSupplier.ForeColor = System.Drawing.Color.DarkBlue
        Me.rdbSupplier.Location = New System.Drawing.Point(33, 18)
        Me.rdbSupplier.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSupplier.Name = "rdbSupplier"
        Me.rdbSupplier.Size = New System.Drawing.Size(81, 21)
        Me.rdbSupplier.TabIndex = 0
        Me.rdbSupplier.Text = "Supplier"
        Me.rdbSupplier.UseVisualStyleBackColor = True
        '
        'rdbBalance
        '
        Me.rdbBalance.AutoSize = True
        Me.rdbBalance.ForeColor = System.Drawing.Color.DarkBlue
        Me.rdbBalance.Location = New System.Drawing.Point(143, 18)
        Me.rdbBalance.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbBalance.Name = "rdbBalance"
        Me.rdbBalance.Size = New System.Drawing.Size(80, 21)
        Me.rdbBalance.TabIndex = 1
        Me.rdbBalance.Text = "Balance"
        Me.rdbBalance.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rdbSupplier)
        Me.GroupBox5.Controls.Add(Me.rdbBalance)
        Me.GroupBox5.ForeColor = System.Drawing.Color.DarkBlue
        Me.GroupBox5.Location = New System.Drawing.Point(164, 54)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Size = New System.Drawing.Size(273, 48)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Sort order"
        '
        'DGVResults
        '
        Me.DGVResults.AllowUserToAddRows = False
        Me.DGVResults.AllowUserToDeleteRows = False
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVResults.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.DGVResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVResults.DefaultCellStyle = DataGridViewCellStyle18
        Me.DGVResults.Location = New System.Drawing.Point(7, -12)
        Me.DGVResults.Margin = New System.Windows.Forms.Padding(4)
        Me.DGVResults.Name = "DGVResults"
        Me.DGVResults.ReadOnly = True
        Me.DGVResults.Size = New System.Drawing.Size(13, 12)
        Me.DGVResults.TabIndex = 145
        Me.DGVResults.Visible = False
        '
        'dgvInvoiceList
        '
        Me.dgvInvoiceList.AllowUserToAddRows = False
        Me.dgvInvoiceList.AllowUserToDeleteRows = False
        Me.dgvInvoiceList.AllowUserToOrderColumns = True
        Me.dgvInvoiceList.AllowUserToResizeRows = False
        Me.dgvInvoiceList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvInvoiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoiceList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.dgvInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvInvoiceList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PtypeEqualiser})
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvInvoiceList.DefaultCellStyle = DataGridViewCellStyle20
        Me.dgvInvoiceList.Location = New System.Drawing.Point(446, 173)
        Me.dgvInvoiceList.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvInvoiceList.Name = "dgvInvoiceList"
        Me.dgvInvoiceList.ReadOnly = True
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoiceList.RowHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.dgvInvoiceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInvoiceList.Size = New System.Drawing.Size(1037, 245)
        Me.dgvInvoiceList.TabIndex = 4
        '
        'PtypeEqualiser
        '
        Me.PtypeEqualiser.HeaderText = ""
        Me.PtypeEqualiser.Name = "PtypeEqualiser"
        Me.PtypeEqualiser.ReadOnly = True
        Me.PtypeEqualiser.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TB_Pay)
        Me.TabControl1.Controls.Add(Me.TB_Requests)
        Me.TabControl1.Location = New System.Drawing.Point(446, 508)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1028, 429)
        Me.TabControl1.TabIndex = 146
        '
        'TB_Pay
        '
        Me.TB_Pay.Controls.Add(Me.dgvPaymentsList)
        Me.TB_Pay.Controls.Add(Me.Button_Pay)
        Me.TB_Pay.Controls.Add(Me.TextBox_Total)
        Me.TB_Pay.Controls.Add(Me.Button_Delete)
        Me.TB_Pay.Location = New System.Drawing.Point(4, 25)
        Me.TB_Pay.Name = "TB_Pay"
        Me.TB_Pay.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_Pay.Size = New System.Drawing.Size(1020, 400)
        Me.TB_Pay.TabIndex = 0
        Me.TB_Pay.Text = "Payments"
        Me.TB_Pay.UseVisualStyleBackColor = True
        '
        'TB_Requests
        '
        Me.TB_Requests.Controls.Add(Me.btn_DeselectAll)
        Me.TB_Requests.Controls.Add(Me.Btn_Bank)
        Me.TB_Requests.Controls.Add(Me.Btn_all)
        Me.TB_Requests.Controls.Add(Me.lbl_RequestStatus)
        Me.TB_Requests.Controls.Add(Me.TB_TotalRequests)
        Me.TB_Requests.Controls.Add(Me.Button2)
        Me.TB_Requests.Controls.Add(Me.DGV_Requests)
        Me.TB_Requests.Controls.Add(Me.btnRemoveRequest)
        Me.TB_Requests.Location = New System.Drawing.Point(4, 25)
        Me.TB_Requests.Name = "TB_Requests"
        Me.TB_Requests.Padding = New System.Windows.Forms.Padding(3)
        Me.TB_Requests.Size = New System.Drawing.Size(1020, 400)
        Me.TB_Requests.TabIndex = 1
        Me.TB_Requests.Text = "Requests"
        Me.TB_Requests.UseVisualStyleBackColor = True
        '
        'btn_DeselectAll
        '
        Me.btn_DeselectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_DeselectAll.Location = New System.Drawing.Point(121, 368)
        Me.btn_DeselectAll.Name = "btn_DeselectAll"
        Me.btn_DeselectAll.Size = New System.Drawing.Size(111, 23)
        Me.btn_DeselectAll.TabIndex = 13
        Me.btn_DeselectAll.Text = "Deselect All"
        Me.btn_DeselectAll.UseVisualStyleBackColor = True
        '
        'Btn_Bank
        '
        Me.Btn_Bank.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_Bank.ForeColor = System.Drawing.Color.DarkBlue
        Me.Btn_Bank.Location = New System.Drawing.Point(491, 365)
        Me.Btn_Bank.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Bank.Name = "Btn_Bank"
        Me.Btn_Bank.Size = New System.Drawing.Size(165, 28)
        Me.Btn_Bank.TabIndex = 12
        Me.Btn_Bank.Tag = "S_Requests_Waiting_Bank"
        Me.Btn_Bank.Text = "Awaiting Bank Export"
        Me.Btn_Bank.UseVisualStyleBackColor = True
        '
        'Btn_all
        '
        Me.Btn_all.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Btn_all.Location = New System.Drawing.Point(4, 368)
        Me.Btn_all.Name = "Btn_all"
        Me.Btn_all.Size = New System.Drawing.Size(111, 23)
        Me.Btn_all.TabIndex = 11
        Me.Btn_all.Text = "Select All"
        Me.Btn_all.UseVisualStyleBackColor = True
        '
        'lbl_RequestStatus
        '
        Me.lbl_RequestStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_RequestStatus.Location = New System.Drawing.Point(238, 368)
        Me.lbl_RequestStatus.Name = "lbl_RequestStatus"
        Me.lbl_RequestStatus.Size = New System.Drawing.Size(246, 23)
        Me.lbl_RequestStatus.TabIndex = 10
        Me.lbl_RequestStatus.Text = "Ready"
        '
        'TB_TotalRequests
        '
        Me.TB_TotalRequests.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TB_TotalRequests.Location = New System.Drawing.Point(816, 365)
        Me.TB_TotalRequests.Name = "TB_TotalRequests"
        Me.TB_TotalRequests.Size = New System.Drawing.Size(121, 22)
        Me.TB_TotalRequests.TabIndex = 9
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(943, 363)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 28)
        Me.Button2.TabIndex = 8
        Me.Button2.Tag = "S_Requests_Pay"
        Me.Button2.Text = "Pay"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'DGV_Requests
        '
        Me.DGV_Requests.AllowDrop = True
        Me.DGV_Requests.AllowUserToAddRows = False
        Me.DGV_Requests.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGV_Requests.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Requests.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.DGV_Requests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Requests.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Ignor})
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_Requests.DefaultCellStyle = DataGridViewCellStyle23
        Me.DGV_Requests.Location = New System.Drawing.Point(3, 3)
        Me.DGV_Requests.Margin = New System.Windows.Forms.Padding(4)
        Me.DGV_Requests.Name = "DGV_Requests"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Requests.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.DGV_Requests.RowTemplate.Height = 24
        Me.DGV_Requests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_Requests.Size = New System.Drawing.Size(1014, 355)
        Me.DGV_Requests.TabIndex = 7
        '
        'Ignor
        '
        Me.Ignor.HeaderText = "Ignor"
        Me.Ignor.Name = "Ignor"
        '
        'Lbl_Status
        '
        Me.Lbl_Status.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Lbl_Status.AutoSize = True
        Me.Lbl_Status.Location = New System.Drawing.Point(207, 946)
        Me.Lbl_Status.Name = "Lbl_Status"
        Me.Lbl_Status.Size = New System.Drawing.Size(13, 17)
        Me.Lbl_Status.TabIndex = 147
        Me.Lbl_Status.Text = "-"
        '
        'lbl_ExcludedPayTypes
        '
        Me.lbl_ExcludedPayTypes.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbl_ExcludedPayTypes.AutoSize = True
        Me.lbl_ExcludedPayTypes.Location = New System.Drawing.Point(575, 945)
        Me.lbl_ExcludedPayTypes.Name = "lbl_ExcludedPayTypes"
        Me.lbl_ExcludedPayTypes.Size = New System.Drawing.Size(131, 17)
        Me.lbl_ExcludedPayTypes.TabIndex = 148
        Me.lbl_ExcludedPayTypes.Text = "Excluded Paytypes:"
        '
        'frmSupplierPaymentsV2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1486, 972)
        Me.Controls.Add(Me.lbl_ExcludedPayTypes)
        Me.Controls.Add(Me.Lbl_Status)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.dgvInvoiceList)
        Me.Controls.Add(Me.DGVResults)
        Me.Controls.Add(Me.ckbAllBalances)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.btnSuppDetail)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.btnViewDetail)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.chkSupplierDetail)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkTotalRequested)
        Me.Controls.Add(Me.txtRequested)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.TrackBar2)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TrackBar1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dgvPeriods)
        Me.Controls.Add(Me.dgvSuppliers)
        Me.Controls.Add(Me.dgvAccounts)
        Me.Controls.Add(Me.DGV_Transactions)
        Me.Controls.Add(Me.Label_PaymentProcessed)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ComboBox_SupplierStatus)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Paid_Label)
        Me.Controls.Add(Me.TextBox_Paid)
        Me.Controls.Add(Me.dgvSupplierSum)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmSupplierPaymentsV2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_SupplierPayments"
        Me.Text = "Ready"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvSupplierSum, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPaymentsList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPeriods, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSuppliers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.SupplierBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.DGVResults, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvInvoiceList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TB_Pay.ResumeLayout(False)
        Me.TB_Pay.PerformLayout()
        Me.TB_Requests.ResumeLayout(False)
        Me.TB_Requests.PerformLayout()
        CType(Me.DGV_Requests, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_SupplierStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents dgvSupplierSum As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPaymentsList As System.Windows.Forms.DataGridView
    Friend WithEvents TextBox_Total As System.Windows.Forms.TextBox
    Friend WithEvents btnRemoveRequest As System.Windows.Forms.Button
    Friend WithEvents Button_Delete As System.Windows.Forms.Button
    Friend WithEvents cmbPaymentType As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox_Paid As System.Windows.Forms.TextBox
    Friend WithEvents Paid_Label As System.Windows.Forms.Label
    Friend WithEvents Button_Pay As System.Windows.Forms.Button
    Friend WithEvents Label_PaymentProcessed As System.Windows.Forms.Label
    Friend WithEvents DGV_Transactions As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPeriods As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSuppliers As System.Windows.Forms.DataGridView
    Friend WithEvents dgvAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents BatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Capture_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fin_Year As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAcc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tax_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accounting As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Void As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DR_CR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contra_Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnViewDetail As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TrackBar2 As System.Windows.Forms.TrackBar
    Friend WithEvents txtRequested As System.Windows.Forms.TextBox
    Friend WithEvents chkTotalRequested As System.Windows.Forms.CheckBox
    Friend WithEvents chkSupplierDetail As System.Windows.Forms.CheckBox
    Friend WithEvents btnSuppDetail As System.Windows.Forms.Button
    Friend WithEvents txtReferenceSearch As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchRef As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbSupplier As System.Windows.Forms.RadioButton
    Friend WithEvents rdbBalance As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents ckbAllBalances As System.Windows.Forms.CheckBox
    Friend WithEvents dtpPaymentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents txtUnapprovedCNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtAsPerSheet As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnViewCreditNotes As System.Windows.Forms.Button
    Friend WithEvents DGVResults As System.Windows.Forms.DataGridView
    Friend WithEvents dgvInvoiceList As System.Windows.Forms.DataGridView
    Friend WithEvents PtypeEqualiser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnViewPaymentsRequests As System.Windows.Forms.Button
    Friend WithEvents txtPaymentsRequests As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnUpdatePayments As System.Windows.Forms.Button
    Friend WithEvents SupplierBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TB_Pay As System.Windows.Forms.TabPage
    Friend WithEvents TB_Requests As System.Windows.Forms.TabPage
    Friend WithEvents DGV_Requests As System.Windows.Forms.DataGridView
    Friend WithEvents TB_TotalRequests As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents lbl_RequestStatus As System.Windows.Forms.Label
    Friend WithEvents Btn_all As System.Windows.Forms.Button
    Friend WithEvents Btn_Bank As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Reference As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TranDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Action As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Ignor As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Lbl_Status As System.Windows.Forms.Label
    Friend WithEvents btn_DeselectAll As System.Windows.Forms.Button
    Friend WithEvents lbl_ExcludedPayTypes As System.Windows.Forms.Label
End Class
