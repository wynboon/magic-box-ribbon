﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSupplierBank
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSupplierBank))
        Me.LBL_SupplierID = New System.Windows.Forms.Label()
        Me.LBL_SupplierName = New System.Windows.Forms.Label()
        Me.TB_AccountNumber = New System.Windows.Forms.TextBox()
        Me.TB_AccountName = New System.Windows.Forms.TextBox()
        Me.TB_Branch = New System.Windows.Forms.TextBox()
        Me.TB_DefaultRef = New System.Windows.Forms.TextBox()
        Me.Btn_Add = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BankID = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LBL_SupplierID
        '
        Me.LBL_SupplierID.AutoSize = True
        Me.LBL_SupplierID.Location = New System.Drawing.Point(137, 11)
        Me.LBL_SupplierID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LBL_SupplierID.Name = "LBL_SupplierID"
        Me.LBL_SupplierID.Size = New System.Drawing.Size(51, 17)
        Me.LBL_SupplierID.TabIndex = 0
        Me.LBL_SupplierID.Text = "Label1"
        '
        'LBL_SupplierName
        '
        Me.LBL_SupplierName.AutoSize = True
        Me.LBL_SupplierName.Location = New System.Drawing.Point(137, 32)
        Me.LBL_SupplierName.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LBL_SupplierName.Name = "LBL_SupplierName"
        Me.LBL_SupplierName.Size = New System.Drawing.Size(51, 17)
        Me.LBL_SupplierName.TabIndex = 1
        Me.LBL_SupplierName.Text = "Label2"
        '
        'TB_AccountNumber
        '
        Me.TB_AccountNumber.Location = New System.Drawing.Point(141, 52)
        Me.TB_AccountNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_AccountNumber.Name = "TB_AccountNumber"
        Me.TB_AccountNumber.Size = New System.Drawing.Size(139, 22)
        Me.TB_AccountNumber.TabIndex = 3
        '
        'TB_AccountName
        '
        Me.TB_AccountName.Location = New System.Drawing.Point(141, 84)
        Me.TB_AccountName.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_AccountName.Name = "TB_AccountName"
        Me.TB_AccountName.Size = New System.Drawing.Size(243, 22)
        Me.TB_AccountName.TabIndex = 4
        '
        'TB_Branch
        '
        Me.TB_Branch.Location = New System.Drawing.Point(141, 116)
        Me.TB_Branch.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_Branch.MaxLength = 6
        Me.TB_Branch.Name = "TB_Branch"
        Me.TB_Branch.Size = New System.Drawing.Size(107, 22)
        Me.TB_Branch.TabIndex = 5
        '
        'TB_DefaultRef
        '
        Me.TB_DefaultRef.Location = New System.Drawing.Point(141, 148)
        Me.TB_DefaultRef.Margin = New System.Windows.Forms.Padding(4)
        Me.TB_DefaultRef.Name = "TB_DefaultRef"
        Me.TB_DefaultRef.Size = New System.Drawing.Size(243, 22)
        Me.TB_DefaultRef.TabIndex = 6
        '
        'Btn_Add
        '
        Me.Btn_Add.Location = New System.Drawing.Point(141, 176)
        Me.Btn_Add.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Add.Name = "Btn_Add"
        Me.Btn_Add.Size = New System.Drawing.Size(111, 28)
        Me.Btn_Add.TabIndex = 8
        Me.Btn_Add.Text = "Add"
        Me.Btn_Add.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 60)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 17)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Account Number"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 92)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 17)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Account Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 124)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 17)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Account Branch"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 156)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 17)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Default Reference"
        '
        'BankID
        '
        Me.BankID.AutoSize = True
        Me.BankID.Location = New System.Drawing.Point(17, 188)
        Me.BankID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.BankID.Name = "BankID"
        Me.BankID.Size = New System.Drawing.Size(53, 17)
        Me.BankID.TabIndex = 14
        Me.BankID.Text = "BankID"
        Me.BankID.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 11)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 17)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Supplier ID"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 32)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 17)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Supplier Name"
        '
        'FrmSupplierBank
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(401, 280)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.BankID)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btn_Add)
        Me.Controls.Add(Me.TB_DefaultRef)
        Me.Controls.Add(Me.TB_Branch)
        Me.Controls.Add(Me.TB_AccountName)
        Me.Controls.Add(Me.TB_AccountNumber)
        Me.Controls.Add(Me.LBL_SupplierName)
        Me.Controls.Add(Me.LBL_SupplierID)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FrmSupplierBank"
        Me.Text = "Please wait while screen loads ..."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LBL_SupplierID As System.Windows.Forms.Label
    Friend WithEvents LBL_SupplierName As System.Windows.Forms.Label
    Friend WithEvents TB_AccountNumber As System.Windows.Forms.TextBox
    Friend WithEvents TB_AccountName As System.Windows.Forms.TextBox
    Friend WithEvents TB_Branch As System.Windows.Forms.TextBox
    Friend WithEvents TB_DefaultRef As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Add As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BankID As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
