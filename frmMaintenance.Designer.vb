﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintenance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintenance))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblFromRow = New System.Windows.Forms.Label()
        Me.lblExcelToRow = New System.Windows.Forms.Label()
        Me.btnRunCorrections = New System.Windows.Forms.Button()
        Me.btnToExcel = New System.Windows.Forms.Button()
        Me.btnLoadGrid = New System.Windows.Forms.Button()
        Me.DateTimePicker_To = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DateTimePicker_From = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkUseDates = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnRunReversals = New System.Windows.Forms.Button()
        Me.btnOpenChangeWorkbook = New System.Windows.Forms.Button()
        Me.chkEffectReversals = New System.Windows.Forms.CheckBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(35, 18)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(564, 243)
        Me.DataGridView1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFromRow)
        Me.GroupBox1.Controls.Add(Me.lblExcelToRow)
        Me.GroupBox1.Controls.Add(Me.btnRunCorrections)
        Me.GroupBox1.Controls.Add(Me.btnToExcel)
        Me.GroupBox1.Controls.Add(Me.btnLoadGrid)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox1.Location = New System.Drawing.Point(400, 276)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(199, 138)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Update Supplier IDs in Transactions to match Suppliers Table"
        '
        'lblFromRow
        '
        Me.lblFromRow.AutoSize = True
        Me.lblFromRow.Location = New System.Drawing.Point(155, 62)
        Me.lblFromRow.Name = "lblFromRow"
        Me.lblFromRow.Size = New System.Drawing.Size(54, 13)
        Me.lblFromRow.TabIndex = 4
        Me.lblFromRow.Text = "From Row"
        Me.lblFromRow.Visible = False
        '
        'lblExcelToRow
        '
        Me.lblExcelToRow.AutoSize = True
        Me.lblExcelToRow.Location = New System.Drawing.Point(155, 75)
        Me.lblExcelToRow.Name = "lblExcelToRow"
        Me.lblExcelToRow.Size = New System.Drawing.Size(44, 13)
        Me.lblExcelToRow.TabIndex = 3
        Me.lblExcelToRow.Text = "XL Row"
        '
        'btnRunCorrections
        '
        Me.btnRunCorrections.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunCorrections.Location = New System.Drawing.Point(19, 99)
        Me.btnRunCorrections.Name = "btnRunCorrections"
        Me.btnRunCorrections.Size = New System.Drawing.Size(134, 23)
        Me.btnRunCorrections.TabIndex = 2
        Me.btnRunCorrections.Text = "Run Corrections"
        Me.btnRunCorrections.UseVisualStyleBackColor = True
        '
        'btnToExcel
        '
        Me.btnToExcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnToExcel.Location = New System.Drawing.Point(19, 70)
        Me.btnToExcel.Name = "btnToExcel"
        Me.btnToExcel.Size = New System.Drawing.Size(134, 23)
        Me.btnToExcel.TabIndex = 1
        Me.btnToExcel.Text = "To Excel"
        Me.btnToExcel.UseVisualStyleBackColor = True
        '
        'btnLoadGrid
        '
        Me.btnLoadGrid.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoadGrid.Location = New System.Drawing.Point(19, 41)
        Me.btnLoadGrid.Name = "btnLoadGrid"
        Me.btnLoadGrid.Size = New System.Drawing.Size(134, 23)
        Me.btnLoadGrid.TabIndex = 0
        Me.btnLoadGrid.Text = "Load Grid"
        Me.btnLoadGrid.UseVisualStyleBackColor = True
        '
        'DateTimePicker_To
        '
        Me.DateTimePicker_To.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_To.Location = New System.Drawing.Point(9, 110)
        Me.DateTimePicker_To.Name = "DateTimePicker_To"
        Me.DateTimePicker_To.Size = New System.Drawing.Size(124, 19)
        Me.DateTimePicker_To.TabIndex = 66
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(6, 91)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "To Date"
        '
        'DateTimePicker_From
        '
        Me.DateTimePicker_From.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker_From.Location = New System.Drawing.Point(9, 72)
        Me.DateTimePicker_From.Name = "DateTimePicker_From"
        Me.DateTimePicker_From.Size = New System.Drawing.Size(124, 19)
        Me.DateTimePicker_From.TabIndex = 65
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(6, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 67
        Me.Label3.Text = "From Date"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkUseDates)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker_To)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker_From)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox2.Location = New System.Drawing.Point(214, 276)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(171, 138)
        Me.GroupBox2.TabIndex = 69
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Date Range"
        '
        'chkUseDates
        '
        Me.chkUseDates.AutoSize = True
        Me.chkUseDates.Location = New System.Drawing.Point(9, 26)
        Me.chkUseDates.Name = "chkUseDates"
        Me.chkUseDates.Size = New System.Drawing.Size(75, 17)
        Me.chkUseDates.TabIndex = 69
        Me.chkUseDates.Text = "Use Dates"
        Me.chkUseDates.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(31, 13)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(629, 446)
        Me.TabControl1.TabIndex = 70
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(621, 420)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Supplier IDs"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnRunReversals)
        Me.GroupBox3.Controls.Add(Me.btnOpenChangeWorkbook)
        Me.GroupBox3.Controls.Add(Me.chkEffectReversals)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.MidnightBlue
        Me.GroupBox3.Location = New System.Drawing.Point(35, 276)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(171, 138)
        Me.GroupBox3.TabIndex = 70
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Reversals"
        '
        'btnRunReversals
        '
        Me.btnRunReversals.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunReversals.Location = New System.Drawing.Point(9, 81)
        Me.btnRunReversals.Name = "btnRunReversals"
        Me.btnRunReversals.Size = New System.Drawing.Size(156, 23)
        Me.btnRunReversals.TabIndex = 71
        Me.btnRunReversals.Text = "Run Reversals"
        Me.btnRunReversals.UseVisualStyleBackColor = True
        Me.btnRunReversals.Visible = False
        '
        'btnOpenChangeWorkbook
        '
        Me.btnOpenChangeWorkbook.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenChangeWorkbook.Location = New System.Drawing.Point(9, 52)
        Me.btnOpenChangeWorkbook.Name = "btnOpenChangeWorkbook"
        Me.btnOpenChangeWorkbook.Size = New System.Drawing.Size(156, 23)
        Me.btnOpenChangeWorkbook.TabIndex = 70
        Me.btnOpenChangeWorkbook.Text = "Open Change Workbook"
        Me.btnOpenChangeWorkbook.UseVisualStyleBackColor = True
        Me.btnOpenChangeWorkbook.Visible = False
        '
        'chkEffectReversals
        '
        Me.chkEffectReversals.AutoSize = True
        Me.chkEffectReversals.Location = New System.Drawing.Point(9, 26)
        Me.chkEffectReversals.Name = "chkEffectReversals"
        Me.chkEffectReversals.Size = New System.Drawing.Size(72, 17)
        Me.chkEffectReversals.TabIndex = 69
        Me.chkEffectReversals.Text = "Reversals"
        Me.chkEffectReversals.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(621, 420)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Other"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'frmMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 471)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMaintenance"
        Me.Text = "Maintenance"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRunCorrections As System.Windows.Forms.Button
    Friend WithEvents btnToExcel As System.Windows.Forms.Button
    Friend WithEvents btnLoadGrid As System.Windows.Forms.Button
    Friend WithEvents lblExcelToRow As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_To As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker_From As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkUseDates As System.Windows.Forms.CheckBox
    Friend WithEvents lblFromRow As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkEffectReversals As System.Windows.Forms.CheckBox
    Friend WithEvents btnOpenChangeWorkbook As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnRunReversals As System.Windows.Forms.Button
End Class
