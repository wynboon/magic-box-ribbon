﻿Module Module5

    Function BuildSQL(ByVal oStep As String) As String

        Dim Step1 As String = ""
        Step1 = Step1 & "SELECT Transactions.SupplierName, Transactions.Reference, Transactions.Amount, Transactions.DocType, Transactions.Void, IIf([DocType]='Invoice',[TransactionID],[LinkID]) AS Trans, Transactions.[Transaction Date]"
        Step1 = Step1 & " FROM Transactions"
        Step1 = Step1 & " WHERE (((Transactions.DocType)='Invoice' Or (Transactions.DocType)='Payment') AND ((Transactions.Void)=False))"

        Dim Step2 As String = ""
        Step2 = Step2 & "SELECT SupplierName, Sum(Amount) AS Amount, DocType, Trans"
        Step2 = Step2 & " FROM (" & Step1 & ")"
        Step2 = Step2 & " GROUP BY SupplierName, DocType, Trans"

        Dim Step3 As String = ""
        Step3 = Step3 & "SELECT SupplierName, IIf([DocType]='Invoice',[Amount],0) AS Invoiced, IIf([DocType]='Invoice',[Amount],-[Amount]) AS DueCalc, Trans"
        Step3 = Step3 & " FROM Step2"

        Dim Step4 As String = ""
        Step4 = Step4 & "SELECT Step3.Trans, Step3.SupplierName, Step1.Reference, Sum(Step3.Invoiced) AS Invoiced, Sum(Step3.DueCalc) AS Due, Step1.[Transaction Date]"
        Step4 = Step4 & " FROM (" & Step1 & ") As Step1 INNER JOIN (" & Step3 & ") As Step3 ON Step1.Trans = Step3.Trans"
        Step4 = Step4 & " GROUP BY Step3.Trans, Step3.SupplierName, Step1.Reference, Step1.[Transaction Date], Step1.DocType"
        Step4 = Step4 & " HAVING (((Step1.DocType)='Invoice'))"

        Dim Step5 As String = ""
        Step5 = Step5 & "SELECT Step4.SupplierName, Sum(Step4.Invoiced) AS Invoiced, Sum(Step4.Due) AS Due"
        Step5 = Step5 & " FROM (" & Step4 & ") As Step4"
        Step5 = Step5 & " GROUP BY Step4.SupplierName"

        If oStep = "Step1" Then
            BuildSQL = Step1
        ElseIf oStep = "Step2" Then
            BuildSQL = Step2
        ElseIf oStep = "Step3" Then
            BuildSQL = Step3
        ElseIf oStep = "Step3" Then
            BuildSQL = Step3
        ElseIf oStep = "Step4" Then
            BuildSQL = Step4
        ElseIf oStep = "Step5" Then
            BuildSQL = Step4
        End If

    End Function
End Module
