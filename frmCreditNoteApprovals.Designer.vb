﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreditNoteApprovals
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCreditNoteApprovals))
        Me.grdCrNApprovals = New System.Windows.Forms.DataGridView()
        Me.cmbSupplier = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnApprove = New System.Windows.Forms.Button()
        Me.lblSuccessorFailure = New System.Windows.Forms.Label()
        Me.btnApproveAll = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtBalance = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.ckbAllSuppliers = New System.Windows.Forms.CheckBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.ckbSelectAll = New System.Windows.Forms.CheckBox()
        Me.DGV_Transactions = New System.Windows.Forms.DataGridView()
        Me.BatchID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Capture_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fin_Year = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GDC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinkAcc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TaxType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tax_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmployeeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accounting = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Void = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transaction_Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DR_CR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contra_Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Info3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.grdCrNApprovals, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdCrNApprovals
        '
        Me.grdCrNApprovals.AllowUserToAddRows = False
        Me.grdCrNApprovals.AllowUserToDeleteRows = False
        Me.grdCrNApprovals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCrNApprovals.Location = New System.Drawing.Point(7, 150)
        Me.grdCrNApprovals.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.grdCrNApprovals.Name = "grdCrNApprovals"
        Me.grdCrNApprovals.RowTemplate.Height = 24
        Me.grdCrNApprovals.Size = New System.Drawing.Size(881, 422)
        Me.grdCrNApprovals.TabIndex = 1
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.FormattingEnabled = True
        Me.cmbSupplier.Location = New System.Drawing.Point(97, 31)
        Me.cmbSupplier.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(243, 24)
        Me.cmbSupplier.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(21, 34)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 17)
        Me.Label1.TabIndex = 69
        Me.Label1.Text = "Supplier :"
        '
        'btnApprove
        '
        Me.btnApprove.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnApprove.Location = New System.Drawing.Point(7, 577)
        Me.btnApprove.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.Size = New System.Drawing.Size(127, 31)
        Me.btnApprove.TabIndex = 73
        Me.btnApprove.Text = "&Approve ticked"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'lblSuccessorFailure
        '
        Me.lblSuccessorFailure.AutoSize = True
        Me.lblSuccessorFailure.Location = New System.Drawing.Point(291, 587)
        Me.lblSuccessorFailure.Name = "lblSuccessorFailure"
        Me.lblSuccessorFailure.Size = New System.Drawing.Size(12, 17)
        Me.lblSuccessorFailure.TabIndex = 74
        Me.lblSuccessorFailure.Text = "."
        '
        'btnApproveAll
        '
        Me.btnApproveAll.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnApproveAll.Location = New System.Drawing.Point(137, 577)
        Me.btnApproveAll.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnApproveAll.Name = "btnApproveAll"
        Me.btnApproveAll.Size = New System.Drawing.Size(127, 31)
        Me.btnApproveAll.TabIndex = 75
        Me.btnApproveAll.Text = "&Approve All"
        Me.btnApproveAll.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 17)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "Balance :"
        '
        'txtBalance
        '
        Me.txtBalance.Location = New System.Drawing.Point(97, 64)
        Me.txtBalance.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(243, 22)
        Me.txtBalance.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnSearch)
        Me.GroupBox1.Controls.Add(Me.ckbAllSuppliers)
        Me.GroupBox1.Controls.Add(Me.cmbSupplier)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtBalance)
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkBlue
        Me.GroupBox1.Location = New System.Drawing.Point(7, 12)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(881, 102)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filter Options"
        '
        'btnSearch
        '
        Me.btnSearch.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnSearch.Location = New System.Drawing.Point(349, 60)
        Me.btnSearch.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(144, 30)
        Me.btnSearch.TabIndex = 3
        Me.btnSearch.Text = "&Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'ckbAllSuppliers
        '
        Me.ckbAllSuppliers.AutoSize = True
        Me.ckbAllSuppliers.ForeColor = System.Drawing.Color.DarkBlue
        Me.ckbAllSuppliers.Location = New System.Drawing.Point(349, 33)
        Me.ckbAllSuppliers.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ckbAllSuppliers.Name = "ckbAllSuppliers"
        Me.ckbAllSuppliers.Size = New System.Drawing.Size(140, 21)
        Me.ckbAllSuppliers.TabIndex = 2
        Me.ckbAllSuppliers.Text = "View all Suppliers"
        Me.ckbAllSuppliers.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnClose.Location = New System.Drawing.Point(777, 577)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(111, 31)
        Me.btnClose.TabIndex = 90
        Me.btnClose.Text = "&Exit"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ckbSelectAll
        '
        Me.ckbSelectAll.AutoSize = True
        Me.ckbSelectAll.ForeColor = System.Drawing.Color.DarkBlue
        Me.ckbSelectAll.Location = New System.Drawing.Point(729, 122)
        Me.ckbSelectAll.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ckbSelectAll.Name = "ckbSelectAll"
        Me.ckbSelectAll.Size = New System.Drawing.Size(143, 21)
        Me.ckbSelectAll.TabIndex = 92
        Me.ckbSelectAll.Text = "Select All Invoices"
        Me.ckbSelectAll.UseVisualStyleBackColor = True
        '
        'DGV_Transactions
        '
        Me.DGV_Transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Transactions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BatchID, Me.TransactionID, Me.LinkID, Me.Capture_Date, Me.Transaction_Date, Me.PPeriod, Me.Fin_Year, Me.GDC, Me.DataGridViewTextBoxColumn1, Me.Description, Me.AccNumber, Me.LinkAcc, Me.DataGridViewTextBoxColumn2, Me.TaxType, Me.Tax_Amount, Me.UserID, Me.SupplierID, Me.EmployeeID, Me.Description2, Me.Description3, Me.Description4, Me.Description5, Me.Posted, Me.Accounting, Me.Void, Me.Transaction_Type, Me.DR_CR, Me.Contra_Account, Me.Description_Code, Me.DocType, Me.DataGridViewTextBoxColumn3, Me.Info, Me.Info2, Me.Info3})
        Me.DGV_Transactions.Location = New System.Drawing.Point(729, 12)
        Me.DGV_Transactions.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DGV_Transactions.Name = "DGV_Transactions"
        Me.DGV_Transactions.RowTemplate.Height = 24
        Me.DGV_Transactions.Size = New System.Drawing.Size(13, 12)
        Me.DGV_Transactions.TabIndex = 99
        Me.DGV_Transactions.Visible = False
        '
        'BatchID
        '
        Me.BatchID.HeaderText = "BatchID"
        Me.BatchID.Name = "BatchID"
        Me.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TransactionID
        '
        Me.TransactionID.HeaderText = "TransactionID"
        Me.TransactionID.Name = "TransactionID"
        '
        'LinkID
        '
        Me.LinkID.HeaderText = "LinkID"
        Me.LinkID.Name = "LinkID"
        '
        'Capture_Date
        '
        Me.Capture_Date.HeaderText = "Capture_Date"
        Me.Capture_Date.Name = "Capture_Date"
        '
        'Transaction_Date
        '
        Me.Transaction_Date.HeaderText = "Transaction_Date"
        Me.Transaction_Date.Name = "Transaction_Date"
        '
        'PPeriod
        '
        Me.PPeriod.HeaderText = "PPeriod"
        Me.PPeriod.Name = "PPeriod"
        '
        'Fin_Year
        '
        Me.Fin_Year.HeaderText = "Fin_Year"
        Me.Fin_Year.Name = "Fin_Year"
        '
        'GDC
        '
        Me.GDC.HeaderText = "GDC"
        Me.GDC.Name = "GDC"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Reference"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        '
        'AccNumber
        '
        Me.AccNumber.HeaderText = "AccNumber"
        Me.AccNumber.Name = "AccNumber"
        '
        'LinkAcc
        '
        Me.LinkAcc.HeaderText = "LinkAcc"
        Me.LinkAcc.Name = "LinkAcc"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'TaxType
        '
        Me.TaxType.HeaderText = "TaxType"
        Me.TaxType.Name = "TaxType"
        '
        'Tax_Amount
        '
        Me.Tax_Amount.HeaderText = "Tax_Amount"
        Me.Tax_Amount.Name = "Tax_Amount"
        '
        'UserID
        '
        Me.UserID.HeaderText = "UserID"
        Me.UserID.Name = "UserID"
        '
        'SupplierID
        '
        Me.SupplierID.HeaderText = "SupplierID"
        Me.SupplierID.Name = "SupplierID"
        '
        'EmployeeID
        '
        Me.EmployeeID.HeaderText = "EmployeeID"
        Me.EmployeeID.Name = "EmployeeID"
        '
        'Description2
        '
        Me.Description2.HeaderText = "Description2"
        Me.Description2.Name = "Description2"
        '
        'Description3
        '
        Me.Description3.HeaderText = "Description3"
        Me.Description3.Name = "Description3"
        '
        'Description4
        '
        Me.Description4.HeaderText = "Description4"
        Me.Description4.Name = "Description4"
        '
        'Description5
        '
        Me.Description5.HeaderText = "Description5"
        Me.Description5.Name = "Description5"
        '
        'Posted
        '
        Me.Posted.HeaderText = "Posted"
        Me.Posted.Name = "Posted"
        '
        'Accounting
        '
        Me.Accounting.HeaderText = "Accounting"
        Me.Accounting.Name = "Accounting"
        '
        'Void
        '
        Me.Void.HeaderText = "Void"
        Me.Void.Name = "Void"
        '
        'Transaction_Type
        '
        Me.Transaction_Type.HeaderText = "Transaction_Type"
        Me.Transaction_Type.Name = "Transaction_Type"
        '
        'DR_CR
        '
        Me.DR_CR.HeaderText = "DR_CR"
        Me.DR_CR.Name = "DR_CR"
        '
        'Contra_Account
        '
        Me.Contra_Account.HeaderText = "Contra_Account"
        Me.Contra_Account.Name = "Contra_Account"
        '
        'Description_Code
        '
        Me.Description_Code.HeaderText = "Description_Code"
        Me.Description_Code.Name = "Description_Code"
        '
        'DocType
        '
        Me.DocType.HeaderText = "DocType"
        Me.DocType.Name = "DocType"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "SupplierName"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Info
        '
        Me.Info.HeaderText = "Info"
        Me.Info.Name = "Info"
        '
        'Info2
        '
        Me.Info2.HeaderText = "Info2"
        Me.Info2.Name = "Info2"
        '
        'Info3
        '
        Me.Info3.HeaderText = "Info3"
        Me.Info3.Name = "Info3"
        '
        'frmCreditNoteApprovals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(893, 603)
        Me.Controls.Add(Me.DGV_Transactions)
        Me.Controls.Add(Me.ckbSelectAll)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnApproveAll)
        Me.Controls.Add(Me.lblSuccessorFailure)
        Me.Controls.Add(Me.btnApprove)
        Me.Controls.Add(Me.grdCrNApprovals)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(911, 650)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(911, 650)
        Me.Name = "frmCreditNoteApprovals"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "S_Credit&Note&Approvals"
        Me.Text = "Credit Note Approvals"
        CType(Me.grdCrNApprovals, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGV_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdCrNApprovals As System.Windows.Forms.DataGridView
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnApprove As System.Windows.Forms.Button
    Friend WithEvents lblSuccessorFailure As System.Windows.Forms.Label
    Friend WithEvents btnApproveAll As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents ckbAllSuppliers As System.Windows.Forms.CheckBox
    Friend WithEvents ckbSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DGV_Transactions As System.Windows.Forms.DataGridView
    Friend WithEvents BatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransactionID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Capture_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fin_Year As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GDC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkAcc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TaxType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tax_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SupplierID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmployeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accounting As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Void As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transaction_Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DR_CR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contra_Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Info3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
