﻿Imports System.Windows.Forms


Public Class Reports

    Private Sub LoadReportsMenu()
        Try
            Dim objApp As New Alchemex.NET.Application
            Dim oReposCln As Alchemex.NET.Interfaces.RepositoryCollection
            Dim oRepos As Alchemex.NET.Repository
            Dim oReports As Alchemex.NET.Report
            Dim objIntegrator As Alchemex.NET.Integration.SDK.Integrator
            objIntegrator = New Alchemex.NET.Integration.SDK.Integrator

            objIntegrator.TenantCode = My.Computer.Name
            objIntegrator.SystemUser.User = My.Computer.Name

            '1. Clear the Menu List in cas already populated
            Me.DataGridView1.Rows.Clear()
            '2. Get the Repository collection (usually only one entry in collection)
            oReposCln = objApp.Repositories
            '3. Get the Repository out of the collection
            oRepos = oReposCln.Item(0)
            '4. Add Each report wth its name and ID to a new menu item
            'and add a click event error handler to handle launching
            For Each oReports In oRepos.PublishedReports

                Dim row0 As String() = {oReports.ID, oReports.Name, oReports.Description}
                DataGridView1.Rows.Add(row0)
            Next
            '5. Clean Up resources
            objApp = Nothing
            oReposCln = Nothing
            oRepos = Nothing
            oReports = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Occurred", MessageBoxButtons.OK)
        End Try
    End Sub
    Private Sub RunReport(ByVal ReportID As Integer)
        Dim objApp As New Alchemex.NET.Application
        Dim oRepos As Alchemex.NET.Repository
        Dim oRep As Alchemex.NET.Report
        Dim Index As Integer
        Index = 0

        Try
            '1. Get the Repository Object
            oRepos = objApp.Repositories.Item(0)
            '2. Set the Tenant - this should be a unique identifier
            ' per set of data (e.g. Company Code)
            oRepos.TenantCode = "1"
            '3. Set the Application logged on user
            oRepos.User = "Wyndham"
            '4. Set the Application Database connection credentials
            With oRepos.AutoConnection
                .Catalog = My.Settings.Setting_Path & "\MagicBox.accdb"
                .Server = ""
                .User = ""
                .Password = ""
            End With

            For Each oReports In oRepos.Reports

                If oReports.ID = ReportID Then
                    '5. get the selected report
                    MsgBox(oReports.Name)
                    oRep = oRepos.Reports(Index)
                    '6. Run the report
                    oRep.Run()
                End If
                Index = Index + 1
            Next



            '7. Release Resources
            objApp = Nothing
            oRepos = Nothing
            oRep = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Occurred", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub Reports_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Reports -> Version : " & My.Settings.Setting_Version
        Call LoadReportsMenu()
    End Sub
    Private Sub DataGridView1_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentDoubleClick

        Dim RepID As Object = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        RunReport(Val(RepID))

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class