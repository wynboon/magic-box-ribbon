﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frm_NewCoStart

    Private Sub Btn_Cancel_Click(sender As Object, e As EventArgs) Handles Btn_Cancel.Click
        Me.Close()
    End Sub

    Private Sub Btn_New_Click(sender As Object, e As EventArgs) Handles Btn_New.Click
        'Get GroupID

        'Add Company into the MB DB
        Dim NewCoID As Long = InsertNewCompany(CB_Group.SelectedValue.ToString, TB_CompanyName.Text, TB_CoRegNo.Text, TB_CoVATNo.Text, TB_Address1.Text, TB_Address2.Text, _
                                               TB_Address3.Text, TB_Address4.Text, TB_Phone1.Text, TB_Phone2.Text, TB_eMail1.Text, TB_eMail2.Text, _
                                                TB_Directors.Text, TB_InvPrefix.Text)

        'Add store to the Central DB
        If AddGroupStore(TB_CompanyName.Text, TB_CoShortName.Text, NewCoID, CB_Group.SelectedItem.Value.ToString, TB_POS_ID.Text) = True Then

        End If

        'Build Periods
        Dim x As Long
        Dim PeriodNo As Long
        Dim PeriodStartDate As Date = DT_StartDate.Value.AddMonths(-1)
        Dim PeriodEndDate As Date
        Dim FinYear As Long = TB_FinYear.Text
        For x = 1 To 60
            PeriodNo = x
            PeriodStartDate = PeriodStartDate.AddMonths(x)
            PeriodEndDate = PeriodStartDate.AddDays(-1)
            If x >= 13 And x <= 24 Then
                PeriodNo = x - 12
                FinYear = TB_FinYear.Text + 1
            End If
            If x >= 25 And x <= 36 Then
                PeriodNo = x - 24
                FinYear = TB_FinYear.Text + 2
            End If
            If x >= 37 And x <= 48 Then
                PeriodNo = x - 36
                FinYear = TB_FinYear.Text + 3
            End If
            If x >= 49 And x <= 60 Then
                PeriodNo = x - 48
                FinYear = TB_FinYear.Text + 4
            End If
            Period_Add(FinYear, PeriodNo, PeriodStartDate, PeriodEndDate, False)
        Next
        MsgBox("Done")
        'Insert Ledgers

    End Sub
    Function LoadGroups() As Boolean

        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String

        sSQL = "SELECT [GroupID],[GroupName] FROM [Groups] Where [GroupName] <> '' Order By [GroupName] Asc"

        Dim i As Integer = 0
        Dim sql As String = Nothing
        sql = sSQL
        connection = New SqlConnection(MagicBox_CommonDB)

        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Group.DataSource = ds.Tables(0)
            CB_Group.ValueMember = "GroupID"
            CB_Group.DisplayMember = "GroupName"
            CB_Group.SelectedIndex = 0

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Private Sub frm_NewCoStart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadGroups()
    End Sub
End Class