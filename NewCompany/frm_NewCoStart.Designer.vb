﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_NewCoStart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CB_Group = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_CompanyName = New System.Windows.Forms.TextBox()
        Me.TB_CoShortName = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DT_StartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TB_FinYear = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CB_Ledger = New System.Windows.Forms.ComboBox()
        Me.Btn_New = New System.Windows.Forms.Button()
        Me.Btn_Cancel = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TB_InvPrefix = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TB_Directors = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TB_eMail2 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TB_eMail1 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TB_Phone2 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TB_Phone1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TB_Address4 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TB_Address3 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TB_Address2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TB_Address1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TB_CoVATNo = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TB_CoRegNo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TB_POS_ID = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(223, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Creation of a new company wizard"
        '
        'CB_Group
        '
        Me.CB_Group.FormattingEnabled = True
        Me.CB_Group.Location = New System.Drawing.Point(44, 89)
        Me.CB_Group.Name = "CB_Group"
        Me.CB_Group.Size = New System.Drawing.Size(361, 24)
        Me.CB_Group.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(41, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(364, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Select the group where the new company will be located:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(41, 126)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(170, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Enter the company name:"
        '
        'TB_CompanyName
        '
        Me.TB_CompanyName.Location = New System.Drawing.Point(41, 147)
        Me.TB_CompanyName.Name = "TB_CompanyName"
        Me.TB_CompanyName.Size = New System.Drawing.Size(220, 22)
        Me.TB_CompanyName.TabIndex = 4
        '
        'TB_CoShortName
        '
        Me.TB_CoShortName.Location = New System.Drawing.Point(281, 147)
        Me.TB_CoShortName.Name = "TB_CoShortName"
        Me.TB_CoShortName.Size = New System.Drawing.Size(84, 22)
        Me.TB_CoShortName.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(278, 127)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Short name:"
        '
        'DT_StartDate
        '
        Me.DT_StartDate.CustomFormat = "dd MMM yyyy"
        Me.DT_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DT_StartDate.Location = New System.Drawing.Point(41, 207)
        Me.DT_StartDate.Name = "DT_StartDate"
        Me.DT_StartDate.Size = New System.Drawing.Size(152, 22)
        Me.DT_StartDate.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(41, 187)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(152, 17)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Fisrt Period Start Date:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(218, 189)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Financial Year:"
        '
        'TB_FinYear
        '
        Me.TB_FinYear.Location = New System.Drawing.Point(221, 209)
        Me.TB_FinYear.Name = "TB_FinYear"
        Me.TB_FinYear.Size = New System.Drawing.Size(99, 22)
        Me.TB_FinYear.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(41, 246)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 17)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Ledger Structure"
        '
        'CB_Ledger
        '
        Me.CB_Ledger.FormattingEnabled = True
        Me.CB_Ledger.Location = New System.Drawing.Point(44, 266)
        Me.CB_Ledger.Name = "CB_Ledger"
        Me.CB_Ledger.Size = New System.Drawing.Size(149, 24)
        Me.CB_Ledger.TabIndex = 11
        '
        'Btn_New
        '
        Me.Btn_New.Location = New System.Drawing.Point(44, 313)
        Me.Btn_New.Name = "Btn_New"
        Me.Btn_New.Size = New System.Drawing.Size(361, 46)
        Me.Btn_New.TabIndex = 13
        Me.Btn_New.Text = "Create New"
        Me.Btn_New.UseVisualStyleBackColor = True
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.Location = New System.Drawing.Point(136, 365)
        Me.Btn_Cancel.Name = "Btn_Cancel"
        Me.Btn_Cancel.Size = New System.Drawing.Size(184, 36)
        Me.Btn_Cancel.TabIndex = 14
        Me.Btn_Cancel.Text = "Cancel"
        Me.Btn_Cancel.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TB_InvPrefix)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.TB_Directors)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.TB_eMail2)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.TB_eMail1)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.TB_Phone2)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.TB_Phone1)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.TB_Address4)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.TB_Address3)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.TB_Address2)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.TB_Address1)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.TB_CoVATNo)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TB_CoRegNo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(476, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(389, 391)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Company Details"
        '
        'TB_InvPrefix
        '
        Me.TB_InvPrefix.Location = New System.Drawing.Point(161, 343)
        Me.TB_InvPrefix.Name = "TB_InvPrefix"
        Me.TB_InvPrefix.Size = New System.Drawing.Size(205, 22)
        Me.TB_InvPrefix.TabIndex = 28
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 348)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(149, 17)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "Debtors Invoice Prefix:"
        '
        'TB_Directors
        '
        Me.TB_Directors.Location = New System.Drawing.Point(88, 315)
        Me.TB_Directors.Name = "TB_Directors"
        Me.TB_Directors.Size = New System.Drawing.Size(278, 22)
        Me.TB_Directors.TabIndex = 26
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 320)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(69, 17)
        Me.Label18.TabIndex = 25
        Me.Label18.Text = "Directors:"
        '
        'TB_eMail2
        '
        Me.TB_eMail2.Location = New System.Drawing.Point(88, 287)
        Me.TB_eMail2.Name = "TB_eMail2"
        Me.TB_eMail2.Size = New System.Drawing.Size(278, 22)
        Me.TB_eMail2.TabIndex = 24
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 292)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(57, 17)
        Me.Label16.TabIndex = 23
        Me.Label16.Text = "eMail 2:"
        '
        'TB_eMail1
        '
        Me.TB_eMail1.Location = New System.Drawing.Point(88, 259)
        Me.TB_eMail1.Name = "TB_eMail1"
        Me.TB_eMail1.Size = New System.Drawing.Size(278, 22)
        Me.TB_eMail1.TabIndex = 22
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 264)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 17)
        Me.Label17.TabIndex = 21
        Me.Label17.Text = "eMail 1:"
        '
        'TB_Phone2
        '
        Me.TB_Phone2.Location = New System.Drawing.Point(88, 231)
        Me.TB_Phone2.Name = "TB_Phone2"
        Me.TB_Phone2.Size = New System.Drawing.Size(278, 22)
        Me.TB_Phone2.TabIndex = 20
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 236)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 17)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Phone 2:"
        '
        'TB_Phone1
        '
        Me.TB_Phone1.Location = New System.Drawing.Point(88, 203)
        Me.TB_Phone1.Name = "TB_Phone1"
        Me.TB_Phone1.Size = New System.Drawing.Size(278, 22)
        Me.TB_Phone1.TabIndex = 18
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 208)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(65, 17)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "Phone 1:"
        '
        'TB_Address4
        '
        Me.TB_Address4.Location = New System.Drawing.Point(88, 175)
        Me.TB_Address4.Name = "TB_Address4"
        Me.TB_Address4.Size = New System.Drawing.Size(278, 22)
        Me.TB_Address4.TabIndex = 16
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 180)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 17)
        Me.Label13.TabIndex = 15
        Me.Label13.Text = "Address 4:"
        '
        'TB_Address3
        '
        Me.TB_Address3.Location = New System.Drawing.Point(88, 150)
        Me.TB_Address3.Name = "TB_Address3"
        Me.TB_Address3.Size = New System.Drawing.Size(278, 22)
        Me.TB_Address3.TabIndex = 14
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 155)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 17)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Address 3:"
        '
        'TB_Address2
        '
        Me.TB_Address2.Location = New System.Drawing.Point(88, 122)
        Me.TB_Address2.Name = "TB_Address2"
        Me.TB_Address2.Size = New System.Drawing.Size(278, 22)
        Me.TB_Address2.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 127)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 17)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Address 2:"
        '
        'TB_Address1
        '
        Me.TB_Address1.Location = New System.Drawing.Point(88, 94)
        Me.TB_Address1.Name = "TB_Address1"
        Me.TB_Address1.Size = New System.Drawing.Size(278, 22)
        Me.TB_Address1.TabIndex = 10
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 99)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(76, 17)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Address 1:"
        '
        'TB_CoVATNo
        '
        Me.TB_CoVATNo.Location = New System.Drawing.Point(146, 66)
        Me.TB_CoVATNo.Name = "TB_CoVATNo"
        Me.TB_CoVATNo.Size = New System.Drawing.Size(220, 22)
        Me.TB_CoVATNo.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 71)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(135, 17)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Company VAT Num:"
        '
        'TB_CoRegNo
        '
        Me.TB_CoRegNo.Location = New System.Drawing.Point(146, 38)
        Me.TB_CoRegNo.Name = "TB_CoRegNo"
        Me.TB_CoRegNo.Size = New System.Drawing.Size(220, 22)
        Me.TB_CoRegNo.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(134, 17)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Company Reg Num:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(218, 246)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 17)
        Me.Label20.TabIndex = 17
        Me.Label20.Text = "POS ID:"
        '
        'TB_POS_ID
        '
        Me.TB_POS_ID.Location = New System.Drawing.Point(221, 266)
        Me.TB_POS_ID.Name = "TB_POS_ID"
        Me.TB_POS_ID.Size = New System.Drawing.Size(99, 22)
        Me.TB_POS_ID.TabIndex = 16
        '
        'frm_NewCoStart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 429)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.TB_POS_ID)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Btn_Cancel)
        Me.Controls.Add(Me.Btn_New)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.CB_Ledger)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TB_FinYear)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.DT_StartDate)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TB_CoShortName)
        Me.Controls.Add(Me.TB_CompanyName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CB_Group)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_NewCoStart"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CB_Group As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TB_CompanyName As System.Windows.Forms.TextBox
    Friend WithEvents TB_CoShortName As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DT_StartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TB_FinYear As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CB_Ledger As System.Windows.Forms.ComboBox
    Friend WithEvents Btn_New As System.Windows.Forms.Button
    Friend WithEvents Btn_Cancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TB_InvPrefix As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TB_Directors As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TB_eMail2 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TB_eMail1 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TB_Phone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TB_Phone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TB_Address4 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TB_Address3 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TB_Address2 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TB_Address1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TB_CoVATNo As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TB_CoRegNo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TB_POS_ID As System.Windows.Forms.TextBox
End Class
