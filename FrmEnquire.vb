﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data

Public Class FrmEnquire

    Private Sub FrmEnquire_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "MagicBox -> " & CStr(GetCompanyName()) & " -> Transaction Enquiry -> Version : " & My.Settings.Setting_Version
        LockForm(Me)
        DT_StartDate.Value = DateAdd(DateInterval.Month, -1, Now)
        DT_EndDate.Value = Now
        LoadFinCat()
        LoadSupp()
        LoadEmp()
        CB_Accounts.Text = ""
    End Sub
    Function LoadAccount()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String
        Dim oSegment1 As String
        Dim oSegment1Desc As String


        If RB_ByCode.Checked = True Then
            sSQL = "SELECT [Segment 4] as Value,[Segment 4] + ' :~: ' + [Segment 3 Desc] as Display FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
                sSQL = sSQL & " Order By [Segment 4] "
            End If
        Else
            sSQL = "SELECT [Segment 4] as Value,[Segment 3 Desc] + ' :~: ' + [Segment 4] as Display FROM Accounting"
            If My.Settings.Setting_CompanyID <> "" And My.Settings.Setting_CompanyID <> Nothing Then '##### NEW #####
                sSQL = sSQL & " WHERE Co_ID = " & My.Settings.Setting_CompanyID
                sSQL = sSQL & " Order By [Segment 3 Desc]"
            End If
        End If

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Accounts.DataSource = ds.Tables(0)
            CB_Accounts.ValueMember = "Value"
            CB_Accounts.DisplayMember = "Display"
            CB_Accounts.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            CB_Accounts.Text = ""
        Catch ex As Exception
            MsgBox("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Function LoadFinCat()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String
        Dim oSegment1 As String
        Dim oSegment1Desc As String

        sSQL = "SELECT LTrim([FinDesc]) as Value,[FinDesc] as Display FROM [FinancialCategories] Order By FinDesc"

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_FinCat.DataSource = ds.Tables(0)
            CB_FinCat.ValueMember = "Value"
            CB_FinCat.DisplayMember = "Display"
            CB_FinCat.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            CB_FinCat.Text = ""
        Catch ex As Exception
            MessageBox.Show("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Function LoadSupp()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String
        Dim oSegment1 As String
        Dim oSegment1Desc As String

        sSQL = "SELECT SupplierID as Value,[SupplierName] as Display FROM [Suppliers]  Where Co_ID = " & My.Settings.Setting_CompanyID & "  Order By SupplierName"

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Suppliers.DataSource = ds.Tables(0)
            CB_Suppliers.ValueMember = "Value"
            CB_Suppliers.DisplayMember = "Display"
            CB_Suppliers.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            CB_Suppliers.Text = ""
        Catch ex As Exception
            MessageBox.Show("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Function LoadEmp()
        Dim connetionString As String = Nothing
        Dim connection As SqlConnection
        Dim command As SqlCommand
        Dim adapter As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim sSQL As String
        Dim oSegment1 As String
        Dim oSegment1Desc As String

        sSQL = "SELECT EmployeeID as Value,[First_Name] + ' ' + [Last_Name] as Display FROM [Employees]  Where Co_ID = " & My.Settings.Setting_CompanyID & " Order By [First_Name]"

        Dim i As Integer = 0
        Dim sql As String = Nothing
        connetionString = My.Settings.CS_Setting
        sql = sSQL
        connection = New SqlConnection(connetionString)
        Try
            connection.Open()
            command = New SqlCommand(sql, connection)
            adapter.SelectCommand = command
            adapter.Fill(ds)
            adapter.Dispose()
            command.Dispose()
            connection.Close()
            CB_Employees.DataSource = ds.Tables(0)
            CB_Employees.ValueMember = "Value"
            CB_Employees.DisplayMember = "Display"
            CB_Employees.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            CB_Employees.Text = ""
        Catch ex As Exception
            MessageBox.Show("Can not open connection ! " & ex.Message)
        End Try
    End Function
    Private Sub RB_ByCode_CheckedChanged(sender As Object, e As EventArgs) Handles RB_ByCode.CheckedChanged
        LoadAccount()
        CB_Accounts.Text = ""
    End Sub

    Private Sub RB_ByName_CheckedChanged(sender As Object, e As EventArgs) Handles RB_ByName.CheckedChanged
        LoadAccount()
        CB_Accounts.Text = ""
    End Sub

    Private Sub Btn_Execute_Click(sender As Object, e As EventArgs) Handles Btn_Execute.Click
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Dim sSQL As String = ""
            sSQL = CStr("Select [Transaction Date] as TxDate,Reference as Ref," & _
                "Description, Debit,Credit,Amount, [Description 4] as [Alt Desc]," & _
                "[Segment 3 Desc] as [Acc Name],[Segment 4] as [Acc Code],[Capture Date] as Captured," & _
                "[SupplierName],[Employee],[TransactionID],[BatchID],[Transaction Type]" & _
                " from vw_Transactions Where Co_ID = " & My.Settings.Setting_CompanyID & " and [transaction Date] >= '" & DT_StartDate.Text & "' and [Transaction Date] <= '" & DT_EndDate.Text & "' and void = 0 and accounting = 1")

            If CB_Accounts.Text <> "" Then
                sSQL = sSQL & " and [Segment 4] = '" & CB_Accounts.SelectedValue & "' "
            End If
            If CB_FinCat.Text <> "" Then
                sSQL = sSQL & " and [FinCat] = '" & Trim(CB_FinCat.SelectedValue) & "' "
            End If
            If RB_Ref.Checked = True Then
                sSQL = sSQL & " and [Reference] Like '%" & TB_RefDesc.Text & "%' "
            Else
                sSQL = sSQL & " and ([Description] like '%" & TB_RefDesc.Text & "%' or [Description 4] like '%" & TB_RefDesc.Text & "%') "
            End If
            If CB_Suppliers.Text <> "" Then
                sSQL = sSQL & " and [SupplierID] = '" & CB_Suppliers.SelectedValue & "' "
            End If
            If CB_Employees.Text <> "" Then
                sSQL = sSQL & " and [EmployeeID] = '" & CB_Employees.SelectedValue & "' "
            End If


            Dim connection As New SqlConnection(My.Settings.CS_Setting & ";Connect Timeout=180")
            Dim dataadapter As New SqlDataAdapter(sSQL, connection)
            Dim ds As New DataSet()
            connection.Open()
            dataadapter.Fill(ds, "CashPay")
            connection.Close()
            DGV_Enquire.DataSource = ds
            DGV_Enquire.DataMember = "CashPay"
            DGV_Enquire.Columns("TxDate").DefaultCellStyle.Format = "dd MMM yyyy"
            DGV_Enquire.Columns("Debit").DefaultCellStyle.Format = "c"
            DGV_Enquire.Columns("Credit").DefaultCellStyle.Format = "c"
            DGV_Enquire.Columns("Amount").DefaultCellStyle.Format = "c"
            DGV_Enquire.Columns("Captured").DefaultCellStyle.Format = "dd MMM yyyy"
            LBL_Movement.Text = QueryMovement.ToString("c")
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            Me.Enabled = True
            Me.Cursor = Cursors.Arrow
            MessageBox.Show("An error occured with details :" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Function QueryMovement() As Double
        If DGV_Enquire.RowCount > 1 Then
            Dim movement As Double = 0

            'if you have the other column to get the result you  could add a new one like these above 
            For index As Integer = 0 To DGV_Enquire.RowCount - 1
                movement = movement + Convert.ToDouble(DGV_Enquire.Rows(index).Cells("Amount").Value)
                'if you have the other column to get the result you  could add a new one like these above (just change Cells(2) to the one you added)
            Next
            Return movement

        End If

    End Function

    Private Sub CB_Accounts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Accounts.SelectedIndexChanged

    End Sub
End Class